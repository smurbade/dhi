appPrefix="DHI"
STG_DIR=C:/staging
home=`pwd`

# Defining the path for the application package data
APP_EXEC_DIR=../appExecData

if [ "$1" = "" ]
then
	while [ "$ver" = "" ]
	do
		echo "Please enter the version no for applicatioon"
		read ver
	done
else
	ver=$1
fi

#ostNamePref=$4

if [ "$4" = RCHI ]
then
	 hostNamePref="RCHI"
elif [ "$4" = CPHI ]
then
	hostNamePref="CPHI"
elif [ "$4" = EHI ]
then
	hostNamePref="EHI"
fi
	
cleanup()
{
	echo "Removing directories if present"

	if [ -d APP_PKG ]; then
		echo "Removing APP_PKG"
		rm -f -r APP_PKG
	fi
	
	if [ -d DHIFiles_PKG ]; then
		echo "Removing DHIFiles_PKG"
		rm -f -r DHIFiles_PKG
	fi
	
	if [ -d PKG ]; then
		echo "Removing PKG"
		rm -f -r PKG
	fi
}

APP_EXEC_DIR=$2
DHIFiles=$3

if ! [ -d "$APP_EXEC_DIR" ]; then
	echo "ERROR: Invalid Path given: $APP_EXEC_DIR.. Exiting..."
	return 1
fi

if ! [ -d "$DHIFiles" ]; then
	echo "ERROR: Invalid Path given: $DHIFiles.. Exiting..."
	return 1
fi

today=$(date +%d%m%y)
appName="$appPrefix-$ver-$today"
hostName="$hostNamePref-$ver-$today"

cleanup

echo "***********************************************************************"
echo "Creating the application bundle"
echo "***********************************************************************"
mkdir -p APP_PKG/lib APP_PKG/CONTROL

#Copy the data into the folder for the application
#Added this so that it could be easily restarted after 
#using Secins_Start_App
chmod 777 $APP_EXEC_DIR/output/*.exe

cp $APP_EXEC_DIR/output/* APP_PKG
cp $APP_EXEC_DIR/lib/*.so APP_PKG/lib
cp $APP_EXEC_DIR/CONFIG/$hostNamePref/config.usr1 APP_PKG/CONTROL
cp $APP_EXEC_DIR/CONFIG/$hostNamePref/.profile APP_PKG/CONTROL

if [ "$hostNamePref" = RCHI ]
then
	rm APP_PKG/lib/libpscphi* 
	rm APP_PKG/lib/libpselvnhi*
elif [ "$hostNamePref" = CPHI ]
then
	rm APP_PKG/lib/libpsrchi*
	rm APP_PKG/lib/libpselvnhi*
elif [ "$hostNamePref" = EHI ]
then
	rm APP_PKG/lib/libpsrchi*
	rm APP_PKG/lib/libpscphi*
fi

touch control
echo "Package: $appPrefix-App" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control

mv control APP_PKG/CONTROL

#create the tar of the application
cd APP_PKG
tar -cvf ../$appPrefix-App.tar ./*

cd $home
rm -f -r APP_PKG

echo "***********************************************************************"
echo "Creating the DHI Files bundle"
echo "***********************************************************************"
mkdir -p DHIFiles_PKG/CONTROL DHIFiles_PKG/crt

touch control
echo "Package: $appPrefix-DHIFiles" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control
echo "Type: userflash" >> control

mv control DHIFiles_PKG/CONTROL
cp ./appsignerA1.crt DHIFiles_PKG/crt
cp -R $DHIFiles/* DHIFiles_PKG

cd DHIFiles_PKG
#create the tar of the DHIFiles deck
tar -cvf ../$appPrefix-DHIFiles.tar ./*

cd $home
rm -f -r DHIFiles_PKG

echo "***********************************************************************"
echo "Creating the download bundle"
echo "***********************************************************************"
mkdir -p PKG/CONTROL PKG/crt
touch control
echo "Package: $appPrefix" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control

mv control PKG/CONTROL
cp ./appsignerA1.crt PKG/crt
mv $appPrefix-App.tar PKG
mv $appPrefix-DHIFiles.tar PKG

cd PKG

echo
echo "Signing the application tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../appsignerA1.crt --key ../appsignerA1.key $appPrefix-App.tar

echo
echo "Signing the DHIFiles tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../appsignerA1.crt --key ../appsignerA1.key $appPrefix-DHIFiles.tar

tar -cvzf ../USR1.$appName.tgz ./*

cd $home

echo
echo "Signing the usr1 package tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ./appsignerA1.crt --key ./appsignerA1.key USR1.$appName.tgz

rm -f -r PKG

echo
echo "Creating the download package dl-$appName.tgz"
tar -cvzf dl-$hostName.tgz USR1.$appName.tgz*

rm -f -r USR1.$appName.tgz*

echo "***********************************************************************"
echo "Bundling the application successful"
echo "***********************************************************************"