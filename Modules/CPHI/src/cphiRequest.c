/*
 * cphiRequest.c
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */

#include <time.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <svc.h>
#include <errno.h>

#include "CPHI/cphicommon.h"
#include "DHI_App/appLog.h"
#include "CPHI/cphiGroups.h"
#include "CPHI/cphi.h"
#include "CPHI/cphiCfg.h"
#include "CPHI/cphiResp.h"
#include "CPHI/utils.h"


static int processVSPRegistration(TRANDTLS_PTYPE);
static int frameCPRequest(CPTRANDTLS_PTYPE, char *);
static CPHI_BOOL isHostConnected();
static pthread_mutex_t gpConfigParamsAcessMutex;
static CPHI_BOOL isCPHIDevRegAllowed();

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		cphiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         cphistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  cphistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	cphiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			cphiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)




/*
 * ============================================================================
 * Function Name: processCreditTransaction
 *
 * Description	:
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	processCreditTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int 					rv					                    = SUCCESS;
	int						iCmd									= -1;
	int						iAppLogEnabled							= 0;
	int						iReqSize								= 0;
	int						iCPRespSize								= 0;
	int						iWaitTime								= 0;
	char 					szRespMsg[256]							= "";
	char					pszCPRequest[2048]						= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszCPResp								= NULL;
	CPHI_BOOL				bTORRequest								= CPHI_FALSE;
	CPHI_BOOL				bvspregistration						= CPHI_FALSE;
	CPHI_BOOL				bTORFirstTime							= CPHI_TRUE;
	CPHI_BOOL				bSAFTran								= CPHI_FALSE;
	time_t					tStartEpochTime  						= 0;
	time_t 					tEndEpochTime  							= 0;
	time_t 					tDiffDeconds       						= 0;
	CPTRANDTLS_STYPE		pstCPReqDetails[CP_MAX_ELEMENTS]		= {{""}};
	CPTRANDTLS_STYPE		pstCPRespDetails[CP_MAX_RESP_FIELDS]	= {{""}};

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Resetting all the fields in the structure for the new transaction*/
	memset(pstCPReqDetails, 0x00, sizeof(pstCPReqDetails));
	memset(pstCPRespDetails, 0x00, sizeof(pstCPRespDetails));
	memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
	memset(szRespMsg, 0x00, sizeof(szRespMsg));

	while(1)
	{

		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		/* Obtaining Transaction Type */
		iCmd = getTransCode(pstCPReqDetails[CP_A_TXN_CODE].elementValue);
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction code !!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if((pstDHIDetails[eSAF_TRAN].elementValue != NULL) && (atoi(pstDHIDetails[eSAF_TRAN].elementValue) == 1))
		{
			bSAFTran = CPHI_TRUE;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Credit %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);

			if((bSAFTran == CPHI_TRUE) && (strcmp(pstDHIDetails[eCOMMAND].elementValue, "SALE") == SUCCESS) && (iCmd == CP_POST_AUTH))
			{
				strcpy(szAppLogData, "Sending Transaction As a Force Sale, Because Its SAF(SALE) Transaction");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

		}

		switch(iCmd)
		{
		case CP_SALE:
		case CP_AUTH:
		case CP_RETURN:
		case CP_POST_AUTH:

			if(pstDHIDetails[eVSP_REG].elementValue != NULL)
			{
				if(atoi(pstDHIDetails[eVSP_REG].elementValue) == 1 && iCmd == CP_SALE)
				{
					bvspregistration = CPHI_TRUE;
					rv = processVSPRegistration(pstDHIDetails);
					if(rv != SUCCESS)
					{
						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "VSP Registration Failed");
							addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

					}
					else
					{
						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "VSP Registration SUCCESS");
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}
					}

					break;
				}
			}
			rv = fillCPReqDtls(pstDHIDetails, pstCPReqDetails, szRespMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Get CP Values", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			break;

		case CP_EMV_DOWNLOAD:
			rv = fillCPDtlsforEMVParamDwnld(pstDHIDetails,pstCPReqDetails);
			break;
		case CP_COMPLETION:
		case CP_VOID:
		case CP_REVERSAL:

			rv = fillCPReqDtlsforFollowOnTran(pstDHIDetails, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill CP Values for follow on", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			break;

		default:
			rv =ERR_UNSUPP_CMD;
			break;
		}
		/* Check if VSP Registration was performed and break, since all the request and response has been performed
		 * in the processDeviceRegistration function
		 */
		if (rv != SUCCESS || bvspregistration == CPHI_TRUE)
		{
			break;
		}
		while(1)
		{

			rv  = frameCPRequest(pstCPReqDetails, pszCPRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame CP request", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to frame the request For CPHI");
					strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_REQUEST, pszCPRequest, NULL);
			}

			/* Posting data to the CP Host */
			iReqSize = strlen(pszCPRequest);
			debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
			CPHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that CP lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			rv = postDataToCPHost(pszCPRequest, iReqSize, &pszCPResp, &iCPRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				CPHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_RESPONSE, pszCPResp, NULL);
				}

				if(iCmd == CP_EMV_DOWNLOAD)
				{
					rv = parseCPEMVDwldResp(pszCPResp,pstDHIDetails,pstCPRespDetails);
					rv = frameSSIRespForEMVAdmin(pstDHIDetails);
				}
				else
				{
					rv = parseCPResponse(pszCPResp, pstDHIDetails, pstCPRespDetails, CREDIT);
				}
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the CP Response", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
				else
				{
					/* Updating config variables values in File */
					rv = updateCPConfigParams(pstCPRespDetails[CP_RET_REF_NUM].elementValue);
					if(rv != SUCCESS)
					{
						strcpy(szAppLogData, "Failed to Update CPHI config Parameters to File");
						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
					}
				}
			}

			if(rv != SUCCESS)
			{
				if(iCmd == CP_SALE || iCmd == CP_AUTH || iCmd == CP_RETURN || iCmd == CP_COMPLETION || iCmd == CP_POST_AUTH)
				{
					if(rv == ERR_RESP_TO)
					{
						/* Setting the end time*/
						tEndEpochTime = time(NULL);
						debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
						CPHI_LIB_TRACE(szDbgMsg);
						tDiffDeconds  = tEndEpochTime - tStartEpochTime;
						debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
						CPHI_LIB_TRACE(szDbgMsg);
						iWaitTime = 40 - tDiffDeconds;
						debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
						CPHI_LIB_TRACE(szDbgMsg);

						if(bTORFirstTime == CPHI_TRUE)
						{
							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Received Response Time Out, Waiting For %d Seconds To Send Time Out Reversal Request", iWaitTime);
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Sending Time Out Reversal Request For 1st time");
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							if(iWaitTime > 0)
							{
								svcWait(iWaitTime*1000);
							}

							if(strlen(pszCPRequest) > 0)
							{
								memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
							}

							/* fill CP details and frame request */
							rv = fillCPDtlsForTOR(pstDHIDetails, pstCPReqDetails);
							debug_sprintf(szDbgMsg, "%s: Filled CP Tor details", __FUNCTION__);
							CPHI_LIB_TRACE(szDbgMsg);
							if(rv == SUCCESS)
							{
								bTORRequest = CPHI_TRUE;
								bTORFirstTime = CPHI_FALSE;
								continue;
							}
						}
						else
						{
							/* Retrying the timeout reversal for another two times*/
							retryTimeoutReversal(pszCPRequest, iWaitTime);

							rv = ERR_HOST_TRAN_TIMEOUT;
							break;
						}
					}
					else if(bTORRequest == CPHI_TRUE && rv == ERR_CONN_FAIL)
					{
						/* Retrying the timeout reversal for another two times*/
						retryTimeoutReversal(pszCPRequest, iWaitTime);

						rv = ERR_HOST_TRAN_TIMEOUT;
						break;
					}
				}
			}
			break;			//coming out of the inner loop
		}
		break;				// coming out of Outer Loop
	}

	if((bSAFTran == CPHI_TRUE) && (bTORRequest == CPHI_TRUE))
	{
		rv = ERR_HOST_NOT_AVAILABLE;
	}
	else if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
	{
		if(bTORRequest == CPHI_TRUE)
		{
			free(pstDHIDetails[eRESULT_CODE].elementValue);
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("22");

			if(pstDHIDetails[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstDHIDetails[eRESPONSE_TEXT].elementValue);
				pstDHIDetails[eRESPONSE_TEXT].elementValue =  strdup("Transaction Reversed");
			}
		}
		else
		{
			if(iCmd != CP_EMV_DOWNLOAD)
			{
				saveTranDetailsForFollowOnTxn(pstDHIDetails, pstCPReqDetails, pstCPRespDetails);
			}
		}
	}

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstDHIDetails, szRespMsg);
	}

	//Checking for Update CP_R_DOWNLAOD_IND is for Autoclose
	if((strcmp(pstCPRespDetails[CP_R_DOWNLOAD_IND].elementValue, "5")) == 0)
	{
		gstEMVParamDwnld.iKeyUpdAvl = 1;
		debug_sprintf(szDbgMsg, "%s: Do EMV Initialization since update is available", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		putEnvFile(SECTION_DCT, EMV_SETUP_REQ, "2");
		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "EMV UPDATE IS AVIALABLE");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Value of EMV Update[%d]", __FUNCTION__,gstEMVParamDwnld.iKeyUpdAvl);
	CPHI_LIB_TRACE(szDbgMsg);

	if(gstEMVParamDwnld.iKeyUpdAvl)
	{
		switch(iCmd)
		{
		case CP_SALE:
		case CP_RETURN:
			pstDHIDetails[eEMV_UPDATE].elementValue = strdup("Y");
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Not a valid command to return the Key Update Flag", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
	}

	if(pszCPResp != NULL)
	{
		free(pszCPResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}



/*
 * ============================================================================
 * Function Name : processDebitTransaction
 *
 * Description	:
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	processDebitTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int 					rv					                    = SUCCESS;
	int						iCmd									= -1;
	int						iAppLogEnabled							= 0;
	int						iReqSize								= 0;
	int						iCPRespSize								= 0;
	int						iWaitTime								= 0;
	char 					szRespMsg[256]							= "";
	char					pszCPRequest[2048]						= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszCPResp								= NULL;
	CPHI_BOOL				bTORRequest								= CPHI_FALSE;
	CPHI_BOOL				bTORFirstTime							= CPHI_TRUE;
	time_t					tStartEpochTime  						= 0;
	time_t 					tEndEpochTime  							= 0;
	time_t 					tDiffDeconds       						= 0;
	CPTRANDTLS_STYPE		pstCPReqDetails[CP_MAX_ELEMENTS]		= {{""}};
	CPTRANDTLS_STYPE		pstCPRespDetails[CP_MAX_RESP_FIELDS]	= {{""}};

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Resetting all the fields in the structure for the new transaction*/
	memset(pstCPReqDetails, 0x00, sizeof(pstCPReqDetails));
	memset(pstCPRespDetails, 0x00, sizeof(pstCPRespDetails));
	memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
	memset(szRespMsg, 0x00, sizeof(szRespMsg));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		/* Obtaining Transaction Type */
		iCmd = getTransCode(pstCPReqDetails[CP_A_TXN_CODE].elementValue);
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction code !!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing DEBIT %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case CP_SALE:
		case CP_RETURN:

			rv = fillCPReqDtls(pstDHIDetails, pstCPReqDetails, szRespMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Get CP Values", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			break;
		case CP_REVERSAL:
			rv = fillCPReqDtlsforFollowOnTran(pstDHIDetails, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill CP Values for follow on", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			break;

		default:
			rv =ERR_UNSUPP_CMD;
			break;
		}

		if (rv != SUCCESS)
		{
			break;
		}


		while(1)
		{

			rv  = frameCPRequest(pstCPReqDetails, pszCPRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame CP request", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to frame the request For CPHI");
					strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_REQUEST, pszCPRequest, NULL);
			}

			/* Posting data to the CP Host */
			iReqSize = strlen(pszCPRequest);

			debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
			CPHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that CP lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			rv = postDataToCPHost(pszCPRequest, iReqSize, &pszCPResp, &iCPRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				CPHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_RESPONSE, pszCPResp, NULL);
				}

				rv = parseCPResponse(pszCPResp, pstDHIDetails, pstCPRespDetails, DEBIT);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the CP Response", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
				else
				{
					/* Updating config variables values in File */
					rv = updateCPConfigParams(pstCPRespDetails[CP_RET_REF_NUM].elementValue);
					if(rv != SUCCESS)
					{
						strcpy(szAppLogData, "Failed to Update CPHI config Parameters to File");
						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
					}
				}
			}

			if(rv != SUCCESS)
			{
				if(iCmd == CP_SALE || iCmd == CP_RETURN)
				{
					if(rv == ERR_RESP_TO)
					{
						/* Setting the end time*/
						tEndEpochTime = time(NULL);
						debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
						CPHI_LIB_TRACE(szDbgMsg);
						tDiffDeconds  = tEndEpochTime - tStartEpochTime;
						debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
						CPHI_LIB_TRACE(szDbgMsg);
						iWaitTime = 40 - tDiffDeconds;
						debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
						CPHI_LIB_TRACE(szDbgMsg);

						if(bTORFirstTime == CPHI_TRUE)
						{
							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Received Response Time Out, Waiting For %d Seconds To Send Time Out Reversal Request", iWaitTime);
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Sending Time Out Reversal Request For 1st time");
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							if(iWaitTime > 0)
							{
								svcWait(iWaitTime*1000);
							}

							if(strlen(pszCPRequest) > 0)
							{
								memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
							}

							/* fill CP details and frame request */
							rv = fillCPDtlsForTOR(pstDHIDetails, pstCPReqDetails);
							debug_sprintf(szDbgMsg, "%s: Filled CP Tor details", __FUNCTION__);
							CPHI_LIB_TRACE(szDbgMsg);
							if(rv == SUCCESS)
							{
								bTORRequest = CPHI_TRUE;
								bTORFirstTime = CPHI_FALSE;
								continue;
							}
						}
						else
						{
							/* Retrying the timeout reversal for another two times*/
							retryTimeoutReversal(pszCPRequest, iWaitTime);

							rv = ERR_HOST_TRAN_TIMEOUT;
							break;
						}
					}
					else if(bTORRequest == CPHI_TRUE && rv == ERR_CONN_FAIL)
					{
						/* Retrying the timeout reversal for another two times*/
						retryTimeoutReversal(pszCPRequest, iWaitTime);

						rv = ERR_HOST_TRAN_TIMEOUT;
						break;
					}
				}
			}
			break;   //coming out of inner loop
		}
		break;       //coming out of outer loop
	}

	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
	{
		if(bTORRequest == CPHI_TRUE)
		{
			free(pstDHIDetails[eRESULT_CODE].elementValue);
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("22");

			if(pstDHIDetails[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstDHIDetails[eRESPONSE_TEXT].elementValue);
				pstDHIDetails[eRESPONSE_TEXT].elementValue =  strdup("Transaction Reversed");
			}
		}
		else
		{
			saveTranDetailsForFollowOnTxn(pstDHIDetails, pstCPReqDetails, pstCPRespDetails);
		}
	}

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstDHIDetails, szRespMsg);
	}

	//Checking for Update CP_R_DOWNLAOD_IND is for Autoclose
	if((strcmp(pstCPRespDetails[CP_R_DOWNLOAD_IND].elementValue, "5")) == 0)
	{
		gstEMVParamDwnld.iKeyUpdAvl = 1;
		debug_sprintf(szDbgMsg, "%s: Do EMV Initialization since update is available", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		putEnvFile(SECTION_DCT, EMV_SETUP_REQ, "2");
		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "EMV UPDATE IS AVIALABLE");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Value of EMV Update[%d]", __FUNCTION__,gstEMVParamDwnld.iKeyUpdAvl);
	CPHI_LIB_TRACE(szDbgMsg);

	if(gstEMVParamDwnld.iKeyUpdAvl)
	{
		switch(iCmd)
		{
		case CP_SALE:
		case CP_RETURN:
			pstDHIDetails[eEMV_UPDATE].elementValue = strdup("Y");
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Not a valid command to return the Key Update Flag", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
	}

	if(pszCPResp != NULL)
	{
		free(pszCPResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name : processEBTTransaction
 *
 * Description	:
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	processEBTTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int 					rv					                    = SUCCESS;
	int						iCmd									= -1;
	int						iAppLogEnabled							= 0;
	int						iReqSize								= 0;
	int						iCPRespSize								= 0;
	int						iWaitTime								= 0;
	char 					szRespMsg[256]							= "";
	char					pszCPRequest[2048]						= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszCPResp								= NULL;
	CPHI_BOOL				bTORRequest								= CPHI_FALSE;
	CPHI_BOOL				bTORFirstTime							= CPHI_TRUE;
	time_t					tStartEpochTime  						= 0;
	time_t 					tEndEpochTime  							= 0;
	time_t 					tDiffDeconds       						= 0;
	CPTRANDTLS_STYPE		pstCPReqDetails[CP_MAX_ELEMENTS]		= {{""}};
	CPTRANDTLS_STYPE		pstCPRespDetails[CP_MAX_RESP_FIELDS]	= {{""}};

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Resetting all the fields in the structure for the new transaction*/
	memset(pstCPReqDetails, 0x00, sizeof(pstCPReqDetails));
	memset(pstCPRespDetails, 0x00, sizeof(pstCPRespDetails));
	memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
	memset(szRespMsg, 0x00, sizeof(szRespMsg));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		/* Obtaining Transaction Type */
		iCmd = getTransCode(pstCPReqDetails[CP_A_TXN_CODE].elementValue);
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction code !!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing EBT %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case CP_SALE:
		case CP_RETURN:
		case CP_BALANCE:
			rv = fillCPReqDtls(pstDHIDetails, pstCPReqDetails, szRespMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Get CP Values", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			break;
		case CP_REVERSAL:
			rv = fillCPReqDtlsforFollowOnTran(pstDHIDetails, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill CP Values for follow on", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			break;

		default:
			rv =ERR_UNSUPP_CMD;
			break;
		}

		if (rv != SUCCESS)
		{
			break;
		}

		while(1)
		{

			rv  = frameCPRequest(pstCPReqDetails, pszCPRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame CP request", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to frame the request For CPHI");
					strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_REQUEST, pszCPRequest, NULL);
			}

			/* Posting data to the CP Host */
			iReqSize = strlen(pszCPRequest);

			debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
			CPHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that CP lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			rv = postDataToCPHost(pszCPRequest, iReqSize, &pszCPResp, &iCPRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				CPHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_RESPONSE, pszCPResp, NULL);
				}

				rv = parseCPResponse(pszCPResp, pstDHIDetails, pstCPRespDetails, EBT);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the CP Response", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
				else
				{
					/* Updating config variables values in File */
					rv = updateCPConfigParams(pstCPRespDetails[CP_RET_REF_NUM].elementValue);
					if(rv != SUCCESS)
					{
						strcpy(szAppLogData, "Failed to Update CPHI config Parameters to File");
						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
					}
				}
			}

			if(rv != SUCCESS)
			{
				if(iCmd == CP_SALE || iCmd == CP_RETURN)
				{
					if(rv == ERR_RESP_TO)
					{
						/* Setting the end time*/
						tEndEpochTime = time(NULL);
						debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
						CPHI_LIB_TRACE(szDbgMsg);
						tDiffDeconds  = tEndEpochTime - tStartEpochTime;
						debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
						CPHI_LIB_TRACE(szDbgMsg);
						iWaitTime = 40 - tDiffDeconds;
						debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
						CPHI_LIB_TRACE(szDbgMsg);

						if(bTORFirstTime == CPHI_TRUE)
						{
							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Received Response Time Out, Waiting For %d Seconds To Send Time Out Reversal Request", iWaitTime);
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Sending Time Out Reversal Request For 1st time");
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							if(iWaitTime > 0)
							{
								svcWait(iWaitTime*1000);
							}

							if(strlen(pszCPRequest) > 0)
							{
								memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
							}

							/* fill CP details and frame request */
							rv = fillCPDtlsForTOR(pstDHIDetails, pstCPReqDetails);
							debug_sprintf(szDbgMsg, "%s: Filled CP Tor details", __FUNCTION__);
							CPHI_LIB_TRACE(szDbgMsg);
							if(rv == SUCCESS)
							{
								bTORRequest = CPHI_TRUE;
								bTORFirstTime = CPHI_FALSE;
								continue;
							}
						}
						else
						{
							/* Retrying the timeout reversal for another two times*/
							retryTimeoutReversal(pszCPRequest, iWaitTime);

							rv = ERR_HOST_TRAN_TIMEOUT;
							break;
						}
					}
					else if(bTORRequest == CPHI_TRUE && rv == ERR_CONN_FAIL)
					{
						/* Retrying the timeout reversal for another two times*/
						retryTimeoutReversal(pszCPRequest, iWaitTime);

						rv = ERR_HOST_TRAN_TIMEOUT;
						break;
					}
				}
			}
			break;   //coming out of inner loop
		}
		break;       //coming out of outer loop
	}

	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
	{
		if(bTORRequest == CPHI_TRUE)
		{
			free(pstDHIDetails[eRESULT_CODE].elementValue);
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("22");

			if(pstDHIDetails[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstDHIDetails[eRESPONSE_TEXT].elementValue);
				pstDHIDetails[eRESPONSE_TEXT].elementValue =  strdup("Transaction Reversed");
			}
		}
		else
		{
			saveTranDetailsForFollowOnTxn(pstDHIDetails, pstCPReqDetails, pstCPRespDetails);
		}
	}

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstDHIDetails, szRespMsg);
	}

	//Checking for Update CP_R_DOWNLAOD_IND is for Autoclose
	if((strcmp(pstCPRespDetails[CP_R_DOWNLOAD_IND].elementValue, "5")) == 0)
	{
		gstEMVParamDwnld.iKeyUpdAvl = 1;
		debug_sprintf(szDbgMsg, "%s: Do EMV Initialization since update is available", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		putEnvFile(SECTION_DCT, EMV_SETUP_REQ, "2");
		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "EMV UPDATE IS AVIALABLE");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Value of EMV Update[%d]", __FUNCTION__,gstEMVParamDwnld.iKeyUpdAvl);
	CPHI_LIB_TRACE(szDbgMsg);

	if(gstEMVParamDwnld.iKeyUpdAvl)
	{
		switch(iCmd)
		{
		case CP_SALE:
		case CP_RETURN:
			pstDHIDetails[eEMV_UPDATE].elementValue = strdup("Y");
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Not a valid command to return the Key Update Flag", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
	}

	if(pszCPResp != NULL)
	{
		free(pszCPResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name : processGiftTransaction
 *
 * Description	:
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int	processGiftTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int 					rv					                    = SUCCESS;
	int						iCmd									= -1;
	int						iAppLogEnabled							= 0;
	int						iReqSize								= 0;
	int						iCPRespSize								= 0;
	int						iWaitTime								= 0;
	char 					szRespMsg[256]							= "";
	char					pszCPRequest[2048]						= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszCPResp								= NULL;
	CPHI_BOOL				bTORRequest								= CPHI_FALSE;
	CPHI_BOOL				bTORFirstTime							= CPHI_TRUE;
	CPHI_BOOL				bSAFTran								= CPHI_FALSE;
	time_t					tStartEpochTime  						= 0;
	time_t 					tEndEpochTime  							= 0;
	time_t 					tDiffDeconds       						= 0;
	CPTRANDTLS_STYPE		pstCPReqDetails[CP_MAX_ELEMENTS]		= {{""}};
	CPTRANDTLS_STYPE		pstCPRespDetails[CP_MAX_RESP_FIELDS]	= {{""}};

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Resetting all the fields in the structure for the new transaction*/
	memset(pstCPReqDetails, 0x00, sizeof(pstCPReqDetails));
	memset(pstCPRespDetails, 0x00, sizeof(pstCPRespDetails));
	memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
	memset(szRespMsg, 0x00, sizeof(szRespMsg));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		/* Obtaining Transaction Type */
		iCmd = getTransCode(pstCPReqDetails[CP_A_TXN_CODE].elementValue);
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction code !!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if((pstDHIDetails[eSAF_TRAN].elementValue != NULL) && (atoi(pstDHIDetails[eSAF_TRAN].elementValue) == 1))
		{
			bSAFTran = CPHI_TRUE;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Stored Value %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case CP_AUTH:
		case CP_REDEMPTION:
		case CP_ACTIVATE:
		case CP_BALANCE:
		case CP_ADDVALUE:
		case CP_DEACTIVATE:
		case CP_POST_AUTH:

			rv = fillCPReqDtls(pstDHIDetails, pstCPReqDetails, szRespMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Get CP Values", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			break;

		case CP_COMPLETION:
		case CP_VOID:

			rv = fillCPReqDtlsforFollowOnTran(pstDHIDetails, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill DHI or CP Values", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			break;

		default:
			rv = ERR_UNSUPP_CMD;
			break;
		}
		/* Check if VSP Registration was performed and break, since all the request and response has been performed
		 * in the processDeviceRegistration function
		 */
		if (rv != SUCCESS || pstDHIDetails[eVSP_REG].elementValue != NULL)
		{
			break;
		}

		while(1)
		{

			rv  = frameCPRequest(pstCPReqDetails, pszCPRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame CP request", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to frame the request For CPHI");
					strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_REQUEST, pszCPRequest, NULL);
			}

			/* Posting data to the CP Host */
			iReqSize = strlen(pszCPRequest);

			debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
			CPHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that CP lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			rv = postDataToCPHost(pszCPRequest, iReqSize, &pszCPResp, &iCPRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				CPHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_RESPONSE, pszCPResp, NULL);
				}

				rv = parseCPResponse(pszCPResp, pstDHIDetails, pstCPRespDetails, STOREDVALUE);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the CP Response", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
				else
				{
					/* Updating config variables values in File */
					rv = updateCPConfigParams(pstCPRespDetails[CP_RET_REF_NUM].elementValue);
					if(rv != SUCCESS)
					{
						strcpy(szAppLogData, "Failed to Update CPHI config Parameters to File");
						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
					}
				}
			}

			if(rv != SUCCESS)
			{
				if(iCmd == CP_REDEMPTION || iCmd == CP_ACTIVATE || iCmd == CP_ADDVALUE || iCmd == CP_DEACTIVATE || iCmd == CP_COMPLETION)
				{
					if(rv == ERR_RESP_TO)
					{
						/* Setting the end time*/
						tEndEpochTime = time(NULL);
						debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
						CPHI_LIB_TRACE(szDbgMsg);
						tDiffDeconds  = tEndEpochTime - tStartEpochTime;
						debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
						CPHI_LIB_TRACE(szDbgMsg);
						iWaitTime = 40 - tDiffDeconds;
						debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
						CPHI_LIB_TRACE(szDbgMsg);

						if(bTORFirstTime == CPHI_TRUE)
						{
							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Received Response Time Out, Waiting For %d Seconds To Send Time Out Reversal Request", iWaitTime);
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Sending Time Out Reversal Request For 1st time");
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							if(iWaitTime > 0)
							{
								svcWait(iWaitTime*1000);
							}

							if(strlen(pszCPRequest) > 0)
							{
								memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
							}

							/* fill CP details and frame request */
							rv = fillCPDtlsForTOR(pstDHIDetails, pstCPReqDetails);
							debug_sprintf(szDbgMsg, "%s: Filled CP Tor details", __FUNCTION__);
							CPHI_LIB_TRACE(szDbgMsg);
							if(rv == SUCCESS)
							{
								bTORRequest = CPHI_TRUE;
								bTORFirstTime = CPHI_FALSE;
								continue;
							}
						}
						else
						{
							/* Retrying the timeout reversal for another two times*/
							retryTimeoutReversal(pszCPRequest, iWaitTime);

							rv = ERR_HOST_TRAN_TIMEOUT;
							break;
						}
					}
					else if(bTORRequest == CPHI_TRUE && rv == ERR_CONN_FAIL)
					{
						/* Retrying the timeout reversal for another two times*/
						retryTimeoutReversal(pszCPRequest, iWaitTime);

						rv = ERR_HOST_TRAN_TIMEOUT;
						break;
					}
				}
			}
			break;			//coming out of the inner loop
		}
		break;				// coming out of Outer Loop
	}

	if((bSAFTran == CPHI_TRUE) && (bTORRequest == CPHI_TRUE))
	{
		rv = ERR_HOST_NOT_AVAILABLE;
	}
	else if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
	{
		if(bTORRequest == CPHI_TRUE)
		{
			free(pstDHIDetails[eRESULT_CODE].elementValue);
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("22");

			if(pstDHIDetails[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstDHIDetails[eRESPONSE_TEXT].elementValue);
				pstDHIDetails[eRESPONSE_TEXT].elementValue =  strdup("Transaction Reversed");
			}
		}
		else
		{
			saveTranDetailsForFollowOnTxn(pstDHIDetails, pstCPReqDetails, pstCPRespDetails);
		}
	}

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstDHIDetails, szRespMsg);
	}

	//Checking for Update CP_R_DOWNLAOD_IND is for Autoclose
	if((strcmp(pstCPRespDetails[CP_R_DOWNLOAD_IND].elementValue, "5")) == 0)
	{
		gstEMVParamDwnld.iKeyUpdAvl = 1;
		debug_sprintf(szDbgMsg, "%s: Do EMV Initialization since update is available", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		putEnvFile(SECTION_DCT, EMV_SETUP_REQ, "2");
		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "EMV UPDATE IS AVIALABLE");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Value of EMV Update[%d]", __FUNCTION__,gstEMVParamDwnld.iKeyUpdAvl);
	CPHI_LIB_TRACE(szDbgMsg);

	if(gstEMVParamDwnld.iKeyUpdAvl)
	{
		switch(iCmd)
		{
		case CP_REDEMPTION:
			pstDHIDetails[eEMV_UPDATE].elementValue = strdup("Y");
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Not a valid command to return the Key Update Flag", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
	}

	if(pszCPResp != NULL)
	{
		free(pszCPResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processVSPRegistration
 *
 * Description	:
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	processVSPRegistration(TRANDTLS_PTYPE pstDHIDetails)
{
	int 					rv					                    = SUCCESS;
	int						iAppLogEnabled							= 0;
	int						iReqSize								= 0;
	int						iCPRespSize								= 0;
	char 					szRespMsg[256]							= "";
	char					pszCPRequest[2048]						= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszCPResp								= NULL;
	CPTRANDTLS_STYPE		pstCPReqDetails[CP_MAX_ELEMENTS]		= {{""}};
	CPTRANDTLS_STYPE 		stCPRespDtls[CP_MAX_RESP_FIELDS]		= {{""}};

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Resetting all the fields in the structure for the new transaction*/
	memset(pstCPReqDetails, 0x00, sizeof(pstCPReqDetails));
	memset(stCPRespDtls, 0x00, sizeof(stCPRespDtls));
	memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
	memset(szRespMsg, 0x00, sizeof(szRespMsg));

	while(1)
	{
		/* Step 1: Fill and Frame Request for VSP registration */
		rv = fillCPDtlsForVSPReg(pstDHIDetails, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Get CP Values for VSP Registration", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
			}
			break;
		}
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing VSP  Registration");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}


		/* Hardcoding Sequence Number to 000001 as suggested by the document */
		strcpy(pstCPReqDetails[CP_A_SEQ_NUM].elementValue, "000001");

		rv  = frameCPRequest(pstCPReqDetails, pszCPRequest);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Frame CP request", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "FAILED to frame the request For CPHI");
				strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_REQUEST, pszCPRequest, NULL);
		}

		/* Posting data to the CP Host */
		iReqSize = strlen(pszCPRequest);

		debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
		CPHI_LIB_TRACE(szDbgMsg);

		/* Setting the parameter that CP lib hit the host*/
		pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

		rv = postDataToCPHost(pszCPRequest, iReqSize, &pszCPResp, &iCPRespSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
		else
		{
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_RESPONSE, pszCPResp, NULL);
			}
			rv = parseCPResponseForDevAndVSPReg(pszCPResp, pstDHIDetails, stCPRespDtls);
		}

		break;
	}


	if(rv != SUCCESS)
	{
		if(rv == ERR_HOST_NOT_AVAILABLE || rv == ERR_RESP_TO)
		{
			pstDHIDetails[eVSP_CODE].elementValue = strdup("327");
			pstDHIDetails[eVSP_RESULTDESC].elementValue = strdup("VSP Registration Required");
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid response[%d]", __FUNCTION__, rv);
			CPHI_LIB_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: VSP Registration Failure", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Successfully parsed the response[%d]", __FUNCTION__, rv);
		CPHI_LIB_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: VSP Registration Successful", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstDHIDetails, szRespMsg);
	}

	if(pszCPResp != NULL)
	{
		free(pszCPResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
============================================================================
 * Function Name: processReportCommand
 *
 * Description	:
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	processReportCommand(TRANDTLS_PTYPE pstDHIDetails)
{
	int 					rv					                    = SUCCESS;
	int						iCmd									= 0;
	int						iAppLogEnabled							= 0;
	int						iReqSize								= 0;
	int						iCPRespSize								= 0;
	char 					szRespMsg[256]							= "";
	char					pszCPRequest[2048]						= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszCPResp								= NULL;
	CPTRANDTLS_STYPE		pstCPReqDetails[CP_MAX_ELEMENTS]		= {{""}};
	CPTRANDTLS_STYPE 		stCPRespDtls[CP_MAX_RESP_FIELDS]		= {{""}};
	VAL_PAYMENTTYPEHEADNODE_STYPE stPaymentHeadNode					= {0};

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	memset(pstCPReqDetails, 0x00, sizeof(pstCPReqDetails));
	memset(stCPRespDtls, 0x00, sizeof(stCPRespDtls));
	memset(szRespMsg, 0x00, sizeof(szRespMsg));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		/* Obtaining Transaction Type */
		iCmd = getTransCode(pstCPReqDetails[CP_A_TXN_CODE].elementValue);
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction code !!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Report %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case CP_BATCH_INQUIRY:
		case CP_BATCH_CLOSE:

			rv = fillCPDtlsForBatchMgmt(pstDHIDetails, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Get CP Values", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			break;

		default:
			rv =ERR_UNSUPP_CMD;
			break;
		}

		if (rv != SUCCESS)
		{
			break;
		}

		while(1)
		{

			rv  = frameCPRequest(pstCPReqDetails, pszCPRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame CP request", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to frame the request For CPHI");
					strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_REQUEST, pszCPRequest, NULL);
			}

			/* Posting data to the CP Host */
			iReqSize = strlen(pszCPRequest);

			debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
			CPHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that CP lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			/* Setting the start time*/
			//tStartEpochTime = time(NULL);

			rv = postDataToCPHost(pszCPRequest, iReqSize, &pszCPResp, &iCPRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				CPHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_RESPONSE, pszCPResp, NULL);
				}
				stPaymentHeadNode.iMaxHostTotalGroup = 0;
				stPaymentHeadNode.pstHead = NULL;
				memset(stPaymentHeadNode.szNetSettlemntAmt,0x00, sizeof(stPaymentHeadNode.szNetSettlemntAmt));
				rv = parseCPResponseForBatchMgmt(pszCPResp, pstDHIDetails, stCPRespDtls, &stPaymentHeadNode);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: No response to parse", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
				}
				rv = frameSSIRespForReportCommand(pstDHIDetails, &stPaymentHeadNode);

				if(strcmp(stCPRespDtls[CP_ACTION_CODE].elementValue, "A") == SUCCESS && (iCmd == CP_BATCH_CLOSE) )
				{
					resetBatchParams();
				}
			}
			break;
		}
		break;
	}

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstDHIDetails, szRespMsg);
	}

	if((strcmp(stCPRespDtls[CP_DOWNLD_FLG].elementValue, "5") == 0))
	{
		gstEMVParamDwnld.iKeyUpdAvl = 1;
		debug_sprintf(szDbgMsg, "%s: Do EMV Initialization since update is available", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		putEnvFile(SECTION_DCT, EMV_SETUP_REQ, "2");
	}

	if(pszCPResp != NULL)
	{
		free(pszCPResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processTokenQueryCommand
 *
 * Description	:
 *
 * Input Params	: Structure
 *
 * Output Params:
 * ============================================================================
 */
int	processTokenQueryCommand(TRANDTLS_PTYPE pstDHIDetails)
{
	int 					rv					                    = SUCCESS;
	int						iAppLogEnabled							= 0;
	int						iReqSize								= 0;
	int						iCPRespSize								= 0;
	char 					szRespMsg[256]							= "";
	char					pszCPRequest[2048]						= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszCPResp								= NULL;
	CPTRANDTLS_STYPE		pstCPReqDetails[CP_MAX_ELEMENTS]		= {{""}};
	CPTRANDTLS_STYPE 		stCPRespDtls[CP_MAX_RESP_FIELDS]		= {{""}};

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Resetting all the fields in the structure for the new transaction*/
	memset(pstCPReqDetails, 0x00, sizeof(pstCPReqDetails));
	memset(pszCPRequest, 0x00, sizeof(pszCPRequest));
	memset(szRespMsg, 0x00, sizeof(szRespMsg));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed To get Command !!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		/* Step 1: Fill and Frame Request for VSP registration */
		rv = fillCPDtlsForTokenQuery(pstDHIDetails, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Get CP Values for Token Query", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Get Required Details For CPHI : rv = [%d]", rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
			}
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Token Query Command");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}


		rv  = frameCPRequest(pstCPReqDetails, pszCPRequest);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Frame CP request", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "FAILED to frame the request For CPHI");
				strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_REQUEST, pszCPRequest, NULL);
		}

		/* Posting data to the CP Host */
		iReqSize = strlen(pszCPRequest);

		debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
		CPHI_LIB_TRACE(szDbgMsg);

		/* Setting the parameter that CP lib hit the host*/
		pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

		rv = postDataToCPHost(pszCPRequest, iReqSize, &pszCPResp, &iCPRespSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_RESPONSE, pszCPResp, NULL);
			}

			rv = parseCPResponse(pszCPResp, pstDHIDetails, stCPRespDtls, CREDIT);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid response[%d]", __FUNCTION__, rv);
				CPHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Successfully parsed the response[%d]", __FUNCTION__, rv);
				CPHI_LIB_TRACE(szDbgMsg);
			}
		}
		break;
	}
	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstDHIDetails, szRespMsg);
	}

	if(pszCPResp != NULL)
	{
		free(pszCPResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}


/*
 * ============================================================================
 * Function Name: FrameCPRequest
 *
 * Description	: This function will frame the request
 *
 * Input Params	: Structure containing the details,
 *
 * 				  frameCPRequest buffer to be filled
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int frameCPRequest(CPTRANDTLS_PTYPE pstCPReqDetails, char* pszCPRequest)
{
	int 	rv             = SUCCESS;
	int 	iCnt 	       = 0;
	int 	iFcnt		   = 0;
	char	szTemp[1+1]    = "";
	char*   pszTemp		   = NULL;


#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	sprintf(szTemp, "%c", STX);
	strcpy(pszCPRequest,szTemp);

	sprintf(szTemp, "%c", FS);

	for(iCnt = 0; iCnt < CP_F_INDUSTRY_CODE; iCnt++ )
	{
		strcat(pszCPRequest, pstCPReqDetails[iCnt].elementValue);
		strcat(pszCPRequest, pstCPReqDetails[iCnt].fieldSeparator);
	}

	if(atoi(pstCPReqDetails[CP_A_TXN_CODE].elementValue) == 78)			//78 == GIFT VOID TRANSACTION
	{

		for(iCnt = CP_G_FIELD_SEP; iCnt <= CP_G_ACC_NUM; iCnt++ )
		{
			strcat(pszCPRequest, pstCPReqDetails[iCnt].elementValue);
			strcat(pszCPRequest, pstCPReqDetails[iCnt].fieldSeparator);
		}
		for(iCnt = CP_F_INDUSTRY_CODE; iCnt <= CP_F_TOT_NUM_CARD; iCnt++ )
		{
			strcat(pszCPRequest, pstCPReqDetails[iCnt].elementValue);
			strcat(pszCPRequest, pstCPReqDetails[iCnt].fieldSeparator);
		}

		iCnt = CP_J_TOKEN_START;
	}
	else
	{
		for(; iCnt < CP_J_TOKEN_START; iCnt++ )					// using the  continuation value of iCnt
		{
			strcat(pszCPRequest, pstCPReqDetails[iCnt].elementValue);
			strcat(pszCPRequest, pstCPReqDetails[iCnt].fieldSeparator);
		}
	}

	if(iCnt == CP_J_TOKEN_START )												//checking for 12 field separator before start of token data
	{
		pszTemp = pszCPRequest;
		while((pszTemp = strchr(pszTemp, FS)) != NULL)
		{
			pszTemp++;
			iFcnt ++;
		}

		if(iFcnt < 13)
		{
			for(; iFcnt < 12 ; iFcnt++)									       //adding Field separator to make count  to 12 before adding token data
			{
				strcat(pszCPRequest, szTemp);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No of field separators are: %d  Should Not be the case ", __FUNCTION__,iFcnt);
			CPHI_LIB_TRACE(szDbgMsg);
			rv	= ERR_SYSTEM;
			return rv;
		}

	}

	for(; iCnt < CP_LRC; iCnt++ )											// using the  continuation value of iCnt
	{
		strcat(pszCPRequest, pstCPReqDetails[iCnt].elementValue);
		strcat(pszCPRequest, pstCPReqDetails[iCnt].fieldSeparator);
	}


	sprintf(szTemp, "%c", ETX);
	strcat(pszCPRequest,szTemp);

	pszTemp = pszCPRequest + 1;
	sprintf(pstCPReqDetails[CP_LRC].elementValue,"%c", findLrc(pszTemp, strlen(pszCPRequest)-1));

	strcat(pszCPRequest, pstCPReqDetails[CP_LRC].elementValue);

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: processDeviceRegistration
 *
 * Description	:
 *
 * Input Params	: Structure
 *
 * Output Params:
 * ============================================================================
 */
int	processDeviceRegistration(TRANDTLS_PTYPE pstDHIDetails)
{
	int 	rv             = SUCCESS;
	char    szTemp[4+1]	   = "";

	if(isCPHIDevRegAllowed() != CPHI_TRUE)
	{
		// Return ERR_DEV_REG_DATA_INSUFF since Dev Registration Should not go through without valid Merchant ID or Terminal ID.
		pstDHIDetails[eRESULT_CODE].elementValue = strdup("31");
		rv = SUCCESS;
	}
	else
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		getClientNum(szTemp);
		pstDHIDetails[eDEVICEKEY].elementValue 		= strdup("DUMMY DEVICE KEY");
		pstDHIDetails[eCLIENT_ID].elementValue 		= strdup(szTemp);
		pstDHIDetails[eCREDIT_PROC_ID].elementValue = strdup("PYMT");
		pstDHIDetails[eDEBIT_PROC_ID].elementValue 	= strdup("PYMT");
		pstDHIDetails[eGIFT_PROC_ID].elementValue 	= strdup("PYMT");
	}
	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: updateCPConfigParams
 *
 * Description	: Updating CP Config Parameters in cpparams.txt file
 *
 * Input Params	: Null
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int updateCPConfigParams(char* pszLastRefNum)
{
	extern			int		    errno;
	int				rv						       	 = SUCCESS;
	char			szSeqNumTknRefNumCustData[256]	 = "";
	char			szTemp[20]						 = "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	putLastRefNum(pszLastRefNum);

	acquireCPMutexLock(&gpConfigParamsAcessMutex, "Updating config params");

	getSequencenum(szTemp);
	strcpy(szSeqNumTknRefNumCustData, szTemp);

	memset(szTemp, 0x00, sizeof(szTemp));
	getTokenRefNum(szTemp);
	strcat(szSeqNumTknRefNumCustData, "|");
	strcat(szSeqNumTknRefNumCustData, szTemp);

	memset(szTemp, 0x00, sizeof(szTemp));
	getCusDefData(szTemp);
	strcat(szSeqNumTknRefNumCustData, "|");
	strcat(szSeqNumTknRefNumCustData, szTemp);

	memset(szTemp, 0x00, sizeof(szTemp));
	getLastRefNum(szTemp);
	strcat(szSeqNumTknRefNumCustData, "|");
	strcat(szSeqNumTknRefNumCustData, szTemp);
	strcat(szSeqNumTknRefNumCustData, "|");

	debug_sprintf(szDbgMsg, "%s - writing key=[%s]", __FUNCTION__, szSeqNumTknRefNumCustData);
	CPHI_LIB_TRACE(szDbgMsg);

	/* Writing into the file */
	rv = writeCPParamsToFile(szSeqNumTknRefNumCustData);
	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully written",__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - Write unsuccessful in file [%s]!",
				__FUNCTION__, CP_PARAMS_FILE);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	releaseCPMutexLock(&gpConfigParamsAcessMutex, "Updating Config params");

	debug_sprintf(szDbgMsg, "%s Returning", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
* ============================================================================
* Function Name: checkHostConnection
*
* Description	:
*
* Input Params	: NONE
*
* Output Params:  1/0
* ============================================================================
*/
int checkHostConnection(void *pData) //Actually nothing will be passed to this function
{
	int		rv 					= 0;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
	iAppLogEnabled = isAppLogEnabled();

	if(isHostConnected() == CPHI_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: CP Host connection is available",__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Chase Paymentech Host Connection is Available");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		rv = 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: CP Host connection is NOT available",__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Chase Paymentech  Host Connection is Not Available");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		rv = 0;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: isHostConnected
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
static CPHI_BOOL isHostConnected()
{
	CPHI_BOOL	bRv				= CPHI_TRUE;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(SUCCESS == checkHostConn())
	{
		bRv = CPHI_TRUE;
	}
	else
	{
		bRv = CPHI_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__,
					(bRv == CPHI_TRUE)? "TRUE" : "FALSE");
	CPHI_LIB_TRACE(szDbgMsg);

	return  bRv;
}

/*
* ============================================================================
* Function Name: isCPHIDevRegAllowed
*
* Description	: Checks whether we must go ahead with device registration or return
* 				  Result code 31 in SSI response , so that SCA requests for checking MID/TID and other
* 				  required configuration for Device Registration
*
* Input Params	:
*
* Output Params:
* ============================================================================
*/
CPHI_BOOL isCPHIDevRegAllowed()
{
	int 	rv 			= 	CPHI_TRUE;
	char 	termId[50]	= 	"";
	char 	merchId[50]	=	"";

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/* Get Configured Merchant ID and Terminal ID.
	 * And verify whether we can go ahead with Device Registration.
	 */
	getTerminalNum(termId);
	getMerchantNum(merchId);

	if((strlen(termId) == 0) || (strlen(merchId) == 0))
	{
		debug_sprintf(szDbgMsg, "%s: --- Dev Reg Not allowed ---", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv =  CPHI_FALSE;
	}
	if((atoi(termId) == 0) || (atoi(merchId) == 0))
	{
		debug_sprintf(szDbgMsg, "%s: --- Dev Reg Not allowed --- termID:%s MerchId:%s", __FUNCTION__,termId,merchId);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = CPHI_FALSE;
	}
	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
	return rv;
}


