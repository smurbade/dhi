/*
 * cphiResponseParse.c
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */


#include <libxml/parser.h>
#include <libxml/tree.h>
#include <time.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <svc.h>

#include "DHI_APP/tranDef.h"
#include "CPHI/cphicommon.h"
#include "CPHI/cphiResp.h"
#include "DHI_App/appLog.h"
#include "CPHI/cphiGroups.h"
#include "CPHI/cphi.h"
#include "CPHI/cphiCfg.h"
#include "CPHI/cphiResp.h"
#include "CPHI/cphash.h"
#include "CPHI/cphiTokenResp.inc"
#include "CPHI/utils.h"

#define NUMBER_CONVERION 10

VAL_EMVFLHEADNODE_STYPE gstEMVFLlst;
VAL_EMVFIHEADNODE_STYPE gstEMVFIlst;
VAL_EMVPKLSTHEADNODE_STYPE gstEMVPkLst;
VAL_EMVPARAMDWNLDNODE_STYPE gstEMVParamDwnld;

void display(VAL_PAYMENTTYPENODE_PTYPE head);

static int buildGenRespDtlsForSSI(xmlDocPtr, xmlNode * , TRANDTLS_PTYPE );
static VAL_PAYMENTTYPENODE_PTYPE addNode(VAL_PAYMENTTYPENODE_PTYPE ,VAL_PAYMENTTYPENODE_PTYPE );

#undef strdup			// we are doing undef as we are defining our own function

#define malloc(size)		cphiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         cphistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  cphistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	cphiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			cphiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)




/*
 * ============================================================================
 * Function Name: parseCPResponse
 *
 * Description	: This function parses the CP Response and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int parseCPResponse(char* pszCPResp, TRANDTLS_PTYPE pstDHIReqResFields, CPTRANDTLS_PTYPE pstCPRespFields, int iPaymentType)
{
	int		 				rv 										= SUCCESS;
	int 					iLen									= 0;
	int 					iDiffTemp								= 0;
	int						iAppLogEnabled							= 0;
	char					szTemp[50]								= "";
//	char 					szlrcValue[3]							= "";
	char*					szErrTemp								= NULL;
	char*					pszStartTemp							= NULL;
//	char*					pszEndTemp								= NULL;
	char					pszApprovedAmt[50]						= "";
	char					szAppLogData[256]						= "";
	float  					fAmount       							= 0.00;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(pszCPResp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		return ERR_INV_RESP;
	}

#if 0
#ifdef DEVDEBUG
	if(strlen(pszRCResp) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s: %s", __FUNCTION__, pszCPResp);
		CPHI_LIB_TRACE(szDbgMsg);
	}
#endif
#endif

	iAppLogEnabled = isAppLogEnabled();

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsing CPHI Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	if(pszCPResp != NULL)
	{
		iLen = strlen(pszCPResp);
	}

	debug_sprintf(szDbgMsg, "%s:Length of the string:[%d]",
			__FUNCTION__, iLen);
	CPHI_LIB_TRACE(szDbgMsg);


	pszStartTemp = pszCPResp;


	//Parse details from 1 to 10.

	if(*pszStartTemp == STX)
	{
		debug_sprintf(szDbgMsg, "%s:Start of Response", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	pszStartTemp += 1;

	//Action Code
	memset(szTemp, 0x00, sizeof(szTemp));

	strncpy(szTemp, pszStartTemp, 1);
	strcpy(pstCPRespFields[CP_ACTION_CODE].elementValue, szTemp);
	if(strcmp(pstCPRespFields[CP_ACTION_CODE].elementValue, "A")==0)
	{
		pstDHIReqResFields[eRESULT].elementValue = strdup("APPROVED");
	}
	else
	{
		pstDHIReqResFields[eRESULT].elementValue = strdup("ERROR");
	}

	pszStartTemp += 1;
	debug_sprintf(szDbgMsg, "%s:Parsed Action Code", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Address Verification Response Code
	memset(szTemp, 0x00, sizeof(szTemp));

	strncpy(szTemp, pszStartTemp, 1);
	strcpy(pstCPRespFields[CP_ADDR_VER_RESP_CODE].elementValue, szTemp);
	if(szTemp[0] != ' ')
	{
		pstDHIReqResFields[eAVS_CODE].elementValue = strdup(szTemp);
	}
	pszStartTemp += 1;
	debug_sprintf(szDbgMsg, "%s:Parsed Address Verification Response Code",
			__FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Authorization code
	memset(szTemp, 0x00,sizeof(szTemp));

	strncpy(szTemp, pszStartTemp, 6);
	strcpy(pstCPRespFields[CP_AUTH_ERROR_CODE].elementValue, szTemp);

	if(pstDHIReqResFields[eAUTH_CODE].elementValue != NULL)
	{
		free(pstDHIReqResFields[eAUTH_CODE].elementValue);					//for POST_AUTH(PRIOR SALE) we are getting auth code, so we are updating with auth code  received from the host.
	}

	if(strcmp(pstCPRespFields[CP_ACTION_CODE].elementValue, "A") == 0)
	{
		pstDHIReqResFields[eAUTH_CODE].elementValue = strdup(szTemp);
	}
	else
	{
		szErrTemp = strchr(szTemp,' ');
		iDiffTemp = szErrTemp - szTemp;
		pstDHIReqResFields[eHOST_RESPCODE].elementValue = strndup(szTemp, iDiffTemp);
	}

	pszStartTemp += 6;
	debug_sprintf(szDbgMsg, "%s:Parsed Authorization Code", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Batch Number
	memset(szTemp, 0x00, sizeof(szTemp));

	strncpy(szTemp, pszStartTemp ,6);
	strcpy(pstCPRespFields[CP_BATCH_NUMBER].elementValue, szTemp);

	pszStartTemp += 6;
	debug_sprintf(szDbgMsg, "%s:Parsed Batch Number", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//parsing Retrieval Reference Number
	memset(szTemp, 0x00, sizeof(szTemp));

	strncpy(szTemp, pszStartTemp, 8);
	strcpy(pstCPRespFields[CP_RET_REF_NUM].elementValue, szTemp);


	pszStartTemp += 8;
	debug_sprintf(szDbgMsg, "%s:Parsed Ret Reference Number", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);


	//Sequence Number
	memset(szTemp, 0x00, sizeof(szTemp));

	strncpy(szTemp, pszStartTemp, 6);

	strcpy(pstCPRespFields[CP_SEQ_NUM].elementValue, szTemp);
	pstDHIReqResFields[eTRANS_SEQ_NUM].elementValue = strdup(szTemp);
	if(pstDHIReqResFields[eCTROUTD].elementValue != NULL)
	{
		free(pstDHIReqResFields[eCTROUTD].elementValue);					// for follow on transactions we will get ctroutd from POS,we need update it with current ctroutd.
	}
	pstDHIReqResFields[eCTROUTD].elementValue = strdup(szTemp);

	pszStartTemp += 6;
	debug_sprintf(szDbgMsg, "%s:Parsed Sequence Number", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Response Message
	memset(szTemp, 0x00, sizeof(szTemp));
	//check here
	strncpy(szTemp, pszStartTemp, 32);
	strcpy(pstCPRespFields[CP_RESP_MSG].elementValue, szTemp);

	pstDHIReqResFields[eRESPONSE_TEXT].elementValue = strdup(szTemp);

	pszStartTemp += 32;
	debug_sprintf(szDbgMsg, "%s:Parsed Response Message", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Card type
	memset(szTemp, 0x00, sizeof(szTemp));
	strncpy(szTemp, pszStartTemp, 2);

	if(pstDHIReqResFields[ePAYMENT_MEDIA].elementValue != NULL)
	{
		free(pstDHIReqResFields[ePAYMENT_MEDIA].elementValue);					// Replacing SSI payment type (Replaced with host payment type value)
	}
	if(strcasecmp(szTemp, "DC") == SUCCESS)
	{
		/* According to Processing and interchange compliance document table 24.
		 * Effective 10/16/09, Chase Paymentech treat all 36 BIN cards as Discover, however, DC continues to be returned in the Host response as the Card Type.
		 * Applications should be updated to reflect this range as a Discover so that POS receipts and reports match the host and settlement.
		 * The POS should not interrogate the host response for this field.*/
		strcpy(szTemp, "DS");
	}
	strcpy(pstCPRespFields[CP_CARD_TYPE].elementValue, szTemp);
	pstDHIReqResFields[ePAYMENT_MEDIA].elementValue = strdup(szTemp);


	pszStartTemp += 2;

	debug_sprintf(szDbgMsg, "%s:Parsed Card Type", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(*pszStartTemp == FS)
	{
		pszStartTemp += 1;
	}

	switch(iPaymentType)
	{
	case CREDIT:
		rv = parseCreditDetails(pstDHIReqResFields, pstCPRespFields, &pszStartTemp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to parse the Credit response:[%d]", __FUNCTION__, rv);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		break;

	case DEBIT:
		rv  = parseDebitDetails(pstDHIReqResFields, pstCPRespFields, &pszStartTemp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to parse the Debit response:[%d]", __FUNCTION__, rv);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		break;

	case EBT:
		rv = parseEBTDetails(pstDHIReqResFields, pstCPRespFields, &pszStartTemp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to parse the EBT response:[%d]", __FUNCTION__, rv);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		break;

	case STOREDVALUE:
		rv = parseStoredValueDetails(pstDHIReqResFields, pstCPRespFields, &pszStartTemp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to parse the StoredValue response:[%d]", __FUNCTION__, rv);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		break;

	case CHECK:
		rv = parseCheckDetails(pstDHIReqResFields, pstCPRespFields, &pszStartTemp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to parse the Check response:[%d]", __FUNCTION__, rv);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		break;
	}

	while(1)
	{
		if(*pszStartTemp != FS)
		{

			rv = parseTokenDetails(pstDHIReqResFields, pstCPRespFields, &pszStartTemp);

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:Error in parsing Token Details", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}
			break;
		}
		else
		{
			pszStartTemp += 1;
		}
	}

	//Apporved Amount
	if(strlen(pstCPRespFields[CP_R_ENHANCED_AUTH_RESP].elementValue) > 0)
	{
		strncpy(pszApprovedAmt, pstCPRespFields[CP_R_ENHANCED_AUTH_RESP].elementValue, 2);
		pszApprovedAmt[2] = '\0';
		//pszApprovedAmt = strndup(pszApprovedAmt,2);
		if (strcmp(pszApprovedAmt,"00") == 0)
		{
			if(pstDHIReqResFields[eTRANS_AMOUNT].elementValue != NULL)
			{
				pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue = (char *)malloc(strlen(pstDHIReqResFields[eTRANS_AMOUNT].elementValue) + 1);
				strcpy(pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue, pstDHIReqResFields[eTRANS_AMOUNT].elementValue);

				debug_sprintf(szDbgMsg, "%s: Transaction approved for full amount, Approved Amount :[%s]", __FUNCTION__, pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue);
				CPHI_LIB_TRACE(szDbgMsg);
			}
		}
		else if(strcmp(pszApprovedAmt,"01") == 0)
		{
			strcpy(pszApprovedAmt, pstCPRespFields[CP_R_ENHANCED_AUTH_RESP].elementValue + 3 );
			pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue = (char *)malloc(strlen(pszApprovedAmt) + 1);

			fAmount = atof(pszApprovedAmt);
			fAmount = fAmount/100;
			sprintf(pszApprovedAmt, "%.02f", fAmount);
			strcpy(pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue,pszApprovedAmt);

			debug_sprintf(szDbgMsg, "%s: Transaction approved for partial amount, Approved Amount :[%s]", __FUNCTION__, pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Q8 Token Value is not valid Q8:[%s]", __FUNCTION__,pstCPRespFields[CP_R_ENHANCED_AUTH_RESP].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Response LRC value:[%s]", __FUNCTION__, pstCPRespFields[CP_R_LRC].elementValue);
	CPHI_LIB_TRACE(szDbgMsg);

#if 0
	//LRC Calculation
	memset(szlrcValue, 0x00, sizeof(szlrcValue));

	pszCPResp = pszCPResp + 1;
	pszEndTemp = strchr(pszCPResp, ETX);
	pszEndTemp += 1;
	iDiffTemp = pszEndTemp - pszCPResp;
	pszCPResp = strndup(pszCPResp, iDiffTemp);

	sprintf(szlrcValue,"%c", findLrc(pszCPResp, strlen(pszCPResp)-1));

	debug_sprintf(szDbgMsg, "%s: Calculated LRC value:[%s]", __FUNCTION__, szlrcValue);
	CPHI_LIB_TRACE(szDbgMsg);

	if(strcmp(pstCPRespFields[CP_R_LRC].elementValue, szlrcValue) == 0)
	{
		debug_sprintf(szDbgMsg, "%s:Calculated LRC Value is matched with Response LRC Value", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Calculated LRC Value is not matched with Response LRC Value", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Calculated LRC Value is not matched with Response LRC Value");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

	}
#endif
	correctDHIResponseFields(pstDHIReqResFields);

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsed CPHI Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);


	return rv;
}

/*
 * ============================================================================
 * Function Name: parseCreditDetails
 *
 * Description	: This function parses the Credit Details and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

int parseCreditDetails(TRANDTLS_PTYPE pstDHIReqResFields, CPTRANDTLS_PTYPE pstCPRespFields,char** pszStartTemp)
{

	int		 	rv 					= SUCCESS;
	int         iDiffTemp 			= 0;
	char 		szTemp[64]			= "";
	char*       pszEndTemp			= NULL;

#if DEBUG
	char			szDbgMsg[1536]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter  ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Interchange Compliance
	if(**pszStartTemp != FS)
	{
		pszEndTemp = strchr(*pszStartTemp, FS);
		if(pszEndTemp != NULL)
		{
			iDiffTemp = pszEndTemp - *pszStartTemp;
			strncpy(pstCPRespFields[CP_INTER_COMPLI].elementValue, *pszStartTemp, iDiffTemp);
			*pszStartTemp += iDiffTemp;
			debug_sprintf(szDbgMsg, "%s: ---  Parsed Interchange Compliance [%s]---", __FUNCTION__, pstCPRespFields[CP_INTER_COMPLI].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}
	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

//	//Authorizing Network ID
//	if(**pszStartTemp != FS)
//	{
//		memset(szTemp, 0x00, sizeof(szTemp));
//		strncpy(szTemp, *pszStartTemp, 2);
//		strcpy(pstCPRespFields[CP_AUTH_NETW_ID].elementValue,szTemp);
//		*pszStartTemp += 2;
//		debug_sprintf(szDbgMsg, "%s: ---  Parsed Auth Network ID ---", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}
//
//	//Authorization Source
//	if(**pszStartTemp != FS)
//	{
//		memset(szTemp, 0x00, sizeof(szTemp));
//		strncpy(szTemp, *pszStartTemp, 1);
//		strcpy(pstCPRespFields[CP_AUTH_SOURCE].elementValue, szTemp);
//		*pszStartTemp += 1;
//		debug_sprintf(szDbgMsg, "%s: ---  Parsed Auth Source ---", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}


	if(**pszStartTemp != FS)
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(szTemp, *pszStartTemp, iDiffTemp);

		if(iDiffTemp == 3)
		{
			strncpy(pstCPRespFields[CP_AUTH_NETW_ID].elementValue, szTemp, 2);
			debug_sprintf(szDbgMsg, "%s: Parsed Authorizer Network ID [%s]", __FUNCTION__, pstCPRespFields[CP_AUTH_NETW_ID].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
			strncpy(pstCPRespFields[CP_AUTH_SOURCE].elementValue, szTemp + 2, 1);
			debug_sprintf(szDbgMsg, "%s: Parsed Authorization source [%s]", __FUNCTION__, pstCPRespFields[CP_AUTH_SOURCE].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		*pszStartTemp += iDiffTemp;
	}
	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}
	else
	{
		/* This check is meant to skip if there is any data between <FS> & <FS>, actually there should not be any data in between, if there is some data we will skip it*/
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		*pszStartTemp += iDiffTemp;
	}

	//optional data
	if(**pszStartTemp != FS)
	{
		pszEndTemp = strchr(*pszStartTemp, FS);
		if(pszEndTemp != NULL)
		{
			iDiffTemp = pszEndTemp - *pszStartTemp;
			memset(szTemp, 0x00, sizeof(szTemp));
			strncpy(szTemp, *pszStartTemp, iDiffTemp);
			*pszStartTemp += iDiffTemp;
			debug_sprintf(szDbgMsg, "%s: ---  Parsed Optional Data [%s]---", __FUNCTION__, szTemp);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}

	while(**pszStartTemp == FS)
	{
		*pszStartTemp +=1;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: parseDebitDetails
 *
 * Description	: This function parses the Debit Details and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

int parseDebitDetails(TRANDTLS_PTYPE pstDHIReqResFields, CPTRANDTLS_PTYPE pstCPRespFields, char** pszStartTemp)
{

	int		 	rv 					= SUCCESS;
	int         iDiffTemp 			= 0;
	char 		szTemp[64]			= "";
	char*       pszEndTemp			= NULL;

#if DEBUG
	char			szDbgMsg[1536]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Trace Number
	if(**pszStartTemp != FS)
	{
	/*	memset(szTemp, 0x00, sizeof(szTemp));
		strncpy(szTemp, *pszStartTemp, 8);
		strcpy(pstCPRespFields[CP_TRACE_NUM].elementValue, szTemp);
		pstDHIReqResFields[eTRACE_NUM].elementValue = strdup(szTemp);
		*pszStartTemp += 8;*/

		/*T_POLISETTYG1 : In stead of copying 8 byte trace number blindly, we are checking for next field separator, and if the size is 8 bytes then we are copying the data to TRACE NUMBER,
		 * by this way we can avoid filling the junk in to TRACE NUMBER */
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		if(iDiffTemp == 8)
		{
			strncpy(pstCPRespFields[CP_TRACE_NUM].elementValue, *pszStartTemp, iDiffTemp);
			pstDHIReqResFields[eTRACE_NUM].elementValue = strdup(pstCPRespFields[CP_TRACE_NUM].elementValue);
		}
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: Parsed Trace Number[%s]", __FUNCTION__, pstCPRespFields[CP_TRACE_NUM].elementValue);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	if(**pszStartTemp == FS)
	{
		*pszStartTemp+=1;
	}

//	//Authorizing Network ID
//	if(**pszStartTemp != FS)
//	{
//		memset(szTemp, 0x00, sizeof(szTemp));
//		strncpy(szTemp, *pszStartTemp, 2);
//		strcpy(pstCPRespFields[CP_AUTH_NETW_ID].elementValue, szTemp);
//		*pszStartTemp += 2;
//		debug_sprintf(szDbgMsg, "%s: Parsed Authorizer Network ID", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}
//
//	//Authorization Source
//	if(**pszStartTemp != FS)
//	{
//		memset(szTemp, 0x00, sizeof(szTemp));
//		strncpy(szTemp, *pszStartTemp, 1);
//		strcpy(pstCPRespFields[CP_AUTH_SOURCE].elementValue, szTemp);
//		*pszStartTemp += 1;
//		debug_sprintf(szDbgMsg, "%s: Parsed Authorization source", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}
//
//	//Surchage Amount
//	if(**pszStartTemp != FS)
//	{
//		pszEndTemp = strchr(*pszStartTemp, FS);
//		iDiffTemp = pszEndTemp - *pszStartTemp;
//		strncpy(pstCPRespFields[CP_SURCH_FEE_AMT].elementValue, *pszStartTemp, iDiffTemp);
//		*pszStartTemp += iDiffTemp;
//		debug_sprintf(szDbgMsg, "%s: Parsed surcharge amount", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}

	if(**pszStartTemp != FS)
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(szTemp, *pszStartTemp, iDiffTemp);

		if(iDiffTemp >= 3)
		{
			strncpy(pstCPRespFields[CP_AUTH_NETW_ID].elementValue, szTemp, 2);
			debug_sprintf(szDbgMsg, "%s: Parsed Authorizer Network ID : [%s]", __FUNCTION__, pstCPRespFields[CP_AUTH_NETW_ID].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
			strncpy(pstCPRespFields[CP_AUTH_SOURCE].elementValue, szTemp + 2, 1);
			debug_sprintf(szDbgMsg, "%s: Parsed Authorization source : [%s]", __FUNCTION__, pstCPRespFields[CP_AUTH_SOURCE].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
			strncpy(pstCPRespFields[CP_SURCH_FEE_AMT].elementValue, szTemp + 3, iDiffTemp - 3);
			debug_sprintf(szDbgMsg, "%s: Parsed surcharge amount : [%s]", __FUNCTION__, pstCPRespFields[CP_SURCH_FEE_AMT].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		*pszStartTemp += iDiffTemp;
	}
	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

	//for Additional Data
	if(**pszStartTemp != FS)
	{
		//*pszStartTemp+=16;
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		memset(szTemp, 0x00, sizeof(szTemp));
		strncpy(szTemp, *pszStartTemp, iDiffTemp);
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: ---  Parsed Optional Data [%s]---", __FUNCTION__, szTemp);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	while(**pszStartTemp == FS)
	{
		*pszStartTemp +=1;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseStoredValueDetails
 *
 * Description	: This function parses the Stored Value Details and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

int parseStoredValueDetails(TRANDTLS_PTYPE pstDHIReqResFields, CPTRANDTLS_PTYPE pstCPRespFields, char** pszStartTemp)
{
	int		 	rv 				= SUCCESS;
	int         iDiffTemp 		= 0;
	char 		szTemp[64]		= "";
	char*       pszEndTemp		= NULL;

#if DEBUG
	char			szDbgMsg[1536]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Trace Number
	if(**pszStartTemp != FS)
	{
	/*	memset(szTemp, 0x00, sizeof(szTemp));
		strncpy(szTemp, *pszStartTemp, 8);
		strcpy(pstCPRespFields[CP_TRACE_NUM].elementValue, szTemp);
		pstDHIReqResFields[eTRACE_NUM].elementValue = strdup(szTemp);
		*pszStartTemp += 8;*/

		/*T_POLISETTYG1 : In stead of copying 8 byte trace number blindly, we are checking for next field separator, and if the size is 8 bytes then we are copying the data to TRACE NUMBER,
		 * by this way we can avoid filling the junk in to TRACE NUMBER */
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		if(iDiffTemp == 8)
		{
			strncpy(pstCPRespFields[CP_TRACE_NUM].elementValue, *pszStartTemp, iDiffTemp);
			pstDHIReqResFields[eTRACE_NUM].elementValue = strdup(pstCPRespFields[CP_TRACE_NUM].elementValue);
		}
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: Parsed Trace Number :[%s]", __FUNCTION__, pstCPRespFields[CP_TRACE_NUM].elementValue);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	if(**pszStartTemp == FS)
	{
		*pszStartTemp+=1;
	}

//	//Authorizing Network ID
//	if(**pszStartTemp != FS)
//	{
//		memset(szTemp, 0x00, sizeof(szTemp));
//		strncpy(szTemp, *pszStartTemp, 2);
//		strcpy(pstCPRespFields[CP_AUTH_NETW_ID].elementValue, szTemp);
//		*pszStartTemp += 2;
//		debug_sprintf(szDbgMsg, "%s: Parsed Authorizer Network ID", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}
//
//	//Authorization Source
//	if(**pszStartTemp != FS)
//	{
//		memset(szTemp, 0x00, sizeof(szTemp));
//		strncpy(szTemp, *pszStartTemp, 1);
//		strcpy(pstCPRespFields[CP_AUTH_SOURCE].elementValue, szTemp);
//		*pszStartTemp += 1;
//		debug_sprintf(szDbgMsg, "%s: Parsed Authorization source", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}
//
//	//Surchage Amount
//	if(**pszStartTemp != FS)
//	{
//		pszEndTemp = strchr(*pszStartTemp, FS);
//		iDiffTemp = pszEndTemp - *pszStartTemp;
//		strncpy(pstCPRespFields[CP_SURCH_FEE_AMT].elementValue, *pszStartTemp, iDiffTemp);
//		*pszStartTemp += iDiffTemp;
//		debug_sprintf(szDbgMsg, "%s: Parsed surcharge amount", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}

	if(**pszStartTemp != FS)
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(szTemp, *pszStartTemp, iDiffTemp);

		if(iDiffTemp >= 3)
		{
			strncpy(pstCPRespFields[CP_AUTH_NETW_ID].elementValue, szTemp, 2);
			debug_sprintf(szDbgMsg, "%s: Parsed Authorizer Network ID : [%s]", __FUNCTION__, pstCPRespFields[CP_AUTH_NETW_ID].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
			strncpy(pstCPRespFields[CP_AUTH_SOURCE].elementValue, szTemp + 2, 1);
			debug_sprintf(szDbgMsg, "%s: Parsed Authorization source :[%s]", __FUNCTION__, pstCPRespFields[CP_AUTH_SOURCE].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
			strncpy(pstCPRespFields[CP_SURCH_FEE_AMT].elementValue, szTemp + 3, iDiffTemp - 3);
			debug_sprintf(szDbgMsg, "%s: Parsed surcharge amount: [%s]", __FUNCTION__, pstCPRespFields[CP_SURCH_FEE_AMT].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		*pszStartTemp += iDiffTemp;
	}
	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

	//for Additional Data
	if(**pszStartTemp != FS)
	{
		//*pszStartTemp+=16;
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		memset(szTemp, 0x00, sizeof(szTemp));
		strncpy(szTemp, *pszStartTemp, iDiffTemp);
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: ---  Parsed Optional Data [%s]---", __FUNCTION__, szTemp);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	while(**pszStartTemp == FS)
	{
		*pszStartTemp +=1;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseEBTDetails
 *
 * Description	: This function parses the EBT Details and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

int parseEBTDetails(TRANDTLS_PTYPE pstDHIReqResFields, CPTRANDTLS_PTYPE pstCPRespFields, char** pszStartTemp)
{

	int		 	rv 				= SUCCESS;
	int         iDiffTemp 		= 0;
	char 		szTemp[64]		= "";
	char*       pszEndTemp		= NULL;

#if DEBUG
	char			szDbgMsg[1536]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Trace Number
	if(**pszStartTemp != FS)
	{
	/*	memset(szTemp, 0x00, sizeof(szTemp));
		strncpy(szTemp, *pszStartTemp, 8);
		strcpy(pstCPRespFields[CP_TRACE_NUM].elementValue, szTemp);
		pstDHIReqResFields[eTRACE_NUM].elementValue = strdup(szTemp);
		*pszStartTemp += 8;*/

		/*T_POLISETTYG1 : In stead of copying 8 byte trace number blindly, we are checking for next field separator, and if the size is 8 bytes then we are copying the data to TRACE NUMBER,
		 * by this way we can avoid filling the junk in to TRACE NUMBER */
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		if(iDiffTemp == 8)
		{
			strncpy(pstCPRespFields[CP_TRACE_NUM].elementValue, *pszStartTemp, iDiffTemp);
			pstDHIReqResFields[eTRACE_NUM].elementValue = strdup(pstCPRespFields[CP_TRACE_NUM].elementValue);
		}
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: Parsed Trace Number [%s]", __FUNCTION__, pstCPRespFields[CP_TRACE_NUM].elementValue);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(**pszStartTemp == FS)
	{
		*pszStartTemp+=1;
	}

//	//Authorizing Network ID
//	if(**pszStartTemp != FS)
//	{
//		memset(szTemp, 0x00, sizeof(szTemp));
//		strncpy(szTemp, *pszStartTemp, 2);
//		strcpy(pstCPRespFields[CP_AUTH_NETW_ID].elementValue, szTemp);
//		*pszStartTemp += 2;
//		debug_sprintf(szDbgMsg, "%s: Parsed Authorizer Network ID", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}
//
//	//Authorization Source
//	if(**pszStartTemp != FS)
//	{
//		memset(szTemp, 0x00, sizeof(szTemp));
//		strncpy(szTemp, *pszStartTemp, 1);
//		strcpy(pstCPRespFields[CP_AUTH_SOURCE].elementValue, szTemp);
//		*pszStartTemp += 1;
//		debug_sprintf(szDbgMsg, "%s: Parsed Authorization source", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}
//
//	//Surchage Amount
//	if(**pszStartTemp != FS)
//	{
//		pszEndTemp = strchr(*pszStartTemp, FS);
//		iDiffTemp = pszEndTemp - *pszStartTemp;
//		strncpy(pstCPRespFields[CP_SURCH_FEE_AMT].elementValue, *pszStartTemp, iDiffTemp);
//		*pszStartTemp += iDiffTemp;
//		debug_sprintf(szDbgMsg, "%s: Parsed surcharge amount", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//	}

	if(**pszStartTemp != FS)
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(szTemp, *pszStartTemp, iDiffTemp);

		if(iDiffTemp >= 3)
		{
			strncpy(pstCPRespFields[CP_AUTH_NETW_ID].elementValue, szTemp, 2);
			debug_sprintf(szDbgMsg, "%s: Parsed Authorizer Network ID : [%s]", __FUNCTION__, pstCPRespFields[CP_AUTH_NETW_ID].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
			strncpy(pstCPRespFields[CP_AUTH_SOURCE].elementValue, szTemp + 2, 1);
			debug_sprintf(szDbgMsg, "%s: Parsed Authorization source : [%s]", __FUNCTION__, pstCPRespFields[CP_AUTH_SOURCE].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
			strncpy(pstCPRespFields[CP_SURCH_FEE_AMT].elementValue, szTemp + 3, iDiffTemp - 3);
			debug_sprintf(szDbgMsg, "%s: Parsed surcharge amount :[%s]", __FUNCTION__, pstCPRespFields[CP_SURCH_FEE_AMT].elementValue);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		*pszStartTemp += iDiffTemp;

	}

	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

	//for Additional Data
	if(**pszStartTemp != FS)
	{
		//*pszStartTemp+=16;
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		memset(szTemp, 0x00, sizeof(szTemp));
		strncpy(szTemp, *pszStartTemp, iDiffTemp);
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: ---  Parsed Optional Data [%s]---", __FUNCTION__, szTemp);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		*pszStartTemp+=1;
	}

	//F/S Ledger Balance
	if(**pszStartTemp != FS)
	{
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(pstCPRespFields[CP_FS_LEG_BAL].elementValue, *pszStartTemp, iDiffTemp);
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: Parsed F/S Ledger Balance", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

	//F/S Available Balance
	if(**pszStartTemp != FS)
	{
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(pstCPRespFields[CP_FS_AVAL_BAL].elementValue, *pszStartTemp, iDiffTemp);
		pstDHIReqResFields[eFS_AVAIL_BALANCE].elementValue = strdup(pstCPRespFields[CP_FS_AVAL_BAL].elementValue);
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: Parsed F/S Available Balance", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

	//F/S Beginning Balance
	if(**pszStartTemp != FS)
	{
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(pstCPRespFields[CP_FS_BEGI_BAL].elementValue, *pszStartTemp, iDiffTemp);
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: Parsed F/S Beginning Balance", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

	//Cash Ledger Balance
	if(**pszStartTemp != FS)
	{
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(pstCPRespFields[CP_CASH_LEG_BAL].elementValue, *pszStartTemp, iDiffTemp);
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: Parsed Cash Ledger Balance", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

	//Cash Available Balance
	if(**pszStartTemp != FS)
	{
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(pstCPRespFields[CP_CASH_AVAL_BAL].elementValue, *pszStartTemp, iDiffTemp);
		pstDHIReqResFields[eCB_AVAIL_BALANCE].elementValue = strdup(pstCPRespFields[CP_CASH_AVAL_BAL].elementValue);
		*pszStartTemp += iDiffTemp;
		debug_sprintf(szDbgMsg, "%s: Parsed Cash Available Balance", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(**pszStartTemp == FS)
	{
		*pszStartTemp += 1;
	}

	//Cash Beginning Balance
	if(**pszStartTemp != FS)
	{
		pszEndTemp = strchr(*pszStartTemp, FS);
		iDiffTemp = pszEndTemp - *pszStartTemp;
		strncpy(pstCPRespFields[CP_CASH_BEGI_BAL].elementValue, *pszStartTemp, iDiffTemp);
		*pszStartTemp += iDiffTemp ;
		debug_sprintf(szDbgMsg, "%s: Parsed Cash Beginning Balance", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		*pszStartTemp += 1;
	}

	debug_sprintf(szDbgMsg, "%s: CP_FS_LEG_BAL [%s]   ", __FUNCTION__, pstCPRespFields[CP_FS_LEG_BAL].elementValue);
	CPHI_LIB_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: CP_FS_AVAL_BAL [%s]   ", __FUNCTION__, pstCPRespFields[CP_FS_AVAL_BAL].elementValue);
	CPHI_LIB_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: CP_FS_BEGI_BAL [%s]   ", __FUNCTION__, pstCPRespFields[CP_FS_BEGI_BAL].elementValue);
	CPHI_LIB_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: CP_CASH_LEG_BAL [%s]   ", __FUNCTION__, pstCPRespFields[CP_CASH_LEG_BAL].elementValue);
	CPHI_LIB_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: CP_CASH_AVAL_BAL [%s]   ", __FUNCTION__, pstCPRespFields[CP_CASH_AVAL_BAL].elementValue);
	CPHI_LIB_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: CP_CASH_BEGI_BAL [%s]   ", __FUNCTION__, pstCPRespFields[CP_CASH_BEGI_BAL].elementValue);
	CPHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: parseCheckDetails
 *
 * Description	: This function parses the Check Details and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

int parseCheckDetails(TRANDTLS_PTYPE pstDHIReqResFields, CPTRANDTLS_PTYPE pstCPRespFields, char** pszStartTemp)
{

	int		 	rv 				= SUCCESS;
	char 		szTemp[20]		= "";

#if DEBUG
	char			szDbgMsg[1024+1]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Field seperators
	if(**pszStartTemp == FS)
	{
		pszStartTemp += 1;
	}

	if(**pszStartTemp != FS)
	{
		//Authorizing Network ID
		memset(szTemp,0x00,sizeof(szTemp));
		strncpy(szTemp,*pszStartTemp,2);
		strcpy(pstCPRespFields[CP_AUTH_NETW_ID].elementValue,szTemp);
		*pszStartTemp += 2;
	}
	else
	{
		*pszStartTemp +=1;
	}

	while(**pszStartTemp == FS)
	{
		*pszStartTemp +=1;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: parseCPResponseForDevAndVSPReg
 *
 * Description	: This function parses the CP Response for Device and VSP Reg.
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

int parseCPResponseForDevAndVSPReg(char *pszCPResp,TRANDTLS_PTYPE pstDHIReqResFields,CPTRANDTLS_PTYPE stCPRespDtls)
{
	int 		rv									= SUCCESS;
	int 		iCnt								= 0;
	int			iDiffTemp							= 0;
	int			iAppLogEnabled						= 0;
	char		szAppLogData[256]					= "";
//	char		szlrcValue[3]						= "";
	char*		pszStart							= NULL;
	char*		pszEnd								= NULL;
	char*		pszErrTemp							= NULL;


#ifdef DEVDEBUG
	char			szDbgMsg[4700]		= "";
#elif DEBUG
	char			szDbgMsg[5326]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsing CPHI Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	pszStart = pszCPResp;

	if(*pszStart != '\0')
	{
		/*	case CP_ACTION_CODE:*/
		pszStart = pszStart + 1;
		memset(stCPRespDtls[CP_ACTION_CODE].elementValue,0x00,sizeof(stCPRespDtls[CP_ACTION_CODE].elementValue));
		strncpy(stCPRespDtls[CP_ACTION_CODE].elementValue, pszStart, 1);
		pszStart = pszStart + 1;
		if(strcmp(stCPRespDtls[CP_ACTION_CODE].elementValue, "A")==0)
		{
			pstDHIReqResFields[eRESULT].elementValue = strdup("APPROVED");
		}
		else
		{
			pstDHIReqResFields[eRESULT].elementValue = strdup("ERROR");
		}

		/*	case CP_ADDR_VER_RESP_CODE: */
		memset(stCPRespDtls[CP_ADDR_VER_RESP_CODE].elementValue,0x00,sizeof(stCPRespDtls[CP_ADDR_VER_RESP_CODE].elementValue));
		strncpy(stCPRespDtls[CP_ADDR_VER_RESP_CODE].elementValue, pszStart, 1);
		pszStart = pszStart + 1;

		/* case CP_AUTH_ERROR_CODE */
		memset(stCPRespDtls[CP_AUTH_ERROR_CODE].elementValue,0x00,sizeof(stCPRespDtls[CP_AUTH_ERROR_CODE].elementValue));
		strncpy(stCPRespDtls[CP_AUTH_ERROR_CODE].elementValue, pszStart, 6);
		if(strcmp(stCPRespDtls[CP_ACTION_CODE].elementValue, "A") == 0)
		{
			pstDHIReqResFields[eAUTH_CODE].elementValue = strdup(stCPRespDtls[CP_AUTH_ERROR_CODE].elementValue);
		}
		else
		{
			pszErrTemp = strchr(stCPRespDtls[CP_AUTH_ERROR_CODE].elementValue,' ');
			iDiffTemp = pszErrTemp - stCPRespDtls[CP_AUTH_ERROR_CODE].elementValue;
			pstDHIReqResFields[eHOST_RESPCODE].elementValue = strndup(stCPRespDtls[CP_AUTH_ERROR_CODE].elementValue, iDiffTemp);
		}
		pszStart = pszStart + 6;


		/*	case CP_BATCH_NUMBER:*/
		memset(stCPRespDtls[CP_BATCH_NUMBER].elementValue,0x00,sizeof(stCPRespDtls[CP_BATCH_NUMBER].elementValue));
		strncpy(stCPRespDtls[CP_BATCH_NUMBER].elementValue, pszStart, 6);

		pszStart = pszStart + 6;

		/*	case CP_RET_REF_NUM:*/
		memset(stCPRespDtls[CP_RET_REF_NUM].elementValue,0x00,sizeof(stCPRespDtls[CP_RET_REF_NUM].elementValue));
		strncpy(stCPRespDtls[CP_RET_REF_NUM].elementValue, pszStart, 8);
//		pstDHIReqResFields[eCTROUTD].elementValue = strdup(stCPRespDtls[CP_RET_REF_NUM].elementValue);
		pszStart = pszStart + 8;

		/*	case CP_SEQ_NUM: */
		memset(stCPRespDtls[CP_SEQ_NUM].elementValue,0x00,sizeof(stCPRespDtls[CP_SEQ_NUM].elementValue));
		strncpy(stCPRespDtls[CP_SEQ_NUM].elementValue, pszStart, 6);
		pstDHIReqResFields[eTRANS_SEQ_NUM].elementValue = strdup(stCPRespDtls[CP_SEQ_NUM].elementValue);
		pstDHIReqResFields[eCTROUTD].elementValue = strdup(stCPRespDtls[CP_SEQ_NUM].elementValue);
		pszStart = pszStart + 6;

		/*	case CP_RESP_MSG: */
		memset(stCPRespDtls[CP_RESP_MSG].elementValue,0x00,sizeof(stCPRespDtls[CP_RESP_MSG].elementValue));
		strncpy(stCPRespDtls[CP_RESP_MSG].elementValue, pszStart, 32);
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue = strdup(stCPRespDtls[CP_RESP_MSG].elementValue);
		pszStart = pszStart + 32;

		/*	case CP_MERCH_CONTROL_IND: */
		pszEnd = pszStart;
		while(*pszEnd != FS)
		{
			pszEnd++;
		}
		strncpy(stCPRespDtls[CP_MERCH_CONTROL_IND].elementValue, pszStart, pszEnd - pszStart);
		pszStart = pszEnd;
//
//		/*	case CP_MERCH_CONTROL_IND: */
//		memset(stCPRespDtls[CP_MERCH_CONTROL_IND].elementValue,0x00,sizeof(stCPRespDtls[CP_MERCH_CONTROL_IND].elementValue));
//		strncpy(stCPRespDtls[CP_MERCH_CONTROL_IND].elementValue, pszStart, 2);
//		pszStart = pszStart + 2;

		/*	case CP_DOWNLD_FLG: */
		memset(stCPRespDtls[CP_DOWNLD_FLG].elementValue,0x00,sizeof(stCPRespDtls[CP_DOWNLD_FLG].elementValue));
		pszStart = pszStart + 1;
		strncpy(stCPRespDtls[CP_DOWNLD_FLG].elementValue, pszStart, 1);
		pszStart = pszStart + 1;

		/*	case CP_MUL_MSG_FLG: */
		memset(stCPRespDtls[CP_MUL_MSG_FLG].elementValue,0x00,sizeof(stCPRespDtls[CP_MUL_MSG_FLG].elementValue));
		strncpy(stCPRespDtls[CP_MUL_MSG_FLG].elementValue, pszStart, 1);
		pszStart = pszStart + 1;

		/*	case CP_BATCH_OPEN_DATE_TIME: */
		memset(stCPRespDtls[CP_BATCH_OPEN_DATE_TIME].elementValue,0x00,sizeof(stCPRespDtls[CP_BATCH_OPEN_DATE_TIME].elementValue));
		strncpy(stCPRespDtls[CP_BATCH_OPEN_DATE_TIME].elementValue, pszStart, 10);
		pszStart = pszStart + 10;

		/*	case CP_BATCH_CLOSE_DATE_TIME: */
		memset(stCPRespDtls[CP_BATCH_CLOSE_DATE_TIME].elementValue,0x00,sizeof(stCPRespDtls[CP_BATCH_CLOSE_DATE_TIME].elementValue));
		strncpy(stCPRespDtls[CP_BATCH_CLOSE_DATE_TIME].elementValue, pszStart, 10);
		pszStart = pszStart + 10;

		/*	case CP_BATCH_TRAN_COUNT:*/
		memset(stCPRespDtls[CP_BATCH_TRAN_COUNT].elementValue,0x00,sizeof(stCPRespDtls[CP_BATCH_TRAN_COUNT].elementValue));
		strncpy(stCPRespDtls[CP_BATCH_TRAN_COUNT].elementValue, pszStart, 6);
		pszStart = pszStart + 6;

		/*case CP_BATCH_NET_AMT:*/
		memset(stCPRespDtls[CP_BATCH_NET_AMT].elementValue,0x00,sizeof(stCPRespDtls[CP_BATCH_NET_AMT].elementValue));
		pszEnd = pszStart;
		while(*pszEnd != FS)
		{
			pszEnd++;
		}
		strncpy(stCPRespDtls[CP_BATCH_NET_AMT].elementValue, pszStart, pszEnd - pszStart);
		pszStart = pszEnd;
		pszStart = pszStart + 1;

		/* case CP_ADDTL_DATA:*/
		memset(stCPRespDtls[CP_ADDTL_DATA].elementValue,0x00,sizeof(stCPRespDtls[CP_ADDTL_DATA].elementValue));
		pszEnd = pszStart;
		iCnt = 1;
		while(iCnt < 18)
		{
			if(*pszEnd != FS)
			{
				iCnt++;
				pszEnd++;
			}
			else
			{
				break;
			}
		}
		if(iCnt <= 17)
		{
			strncpy(stCPRespDtls[CP_ADDTL_DATA].elementValue, pszStart, pszEnd - pszStart);
			pszStart = pszEnd;
		}

		debug_sprintf(szDbgMsg, "%s: Additional Data [%s]", __FUNCTION__, stCPRespDtls[CP_ADDTL_DATA].elementValue);
		CPHI_LIB_TRACE(szDbgMsg);

		pszStart = pszStart + 1;

		while(1)
		{
			if(*pszStart != FS)
			{

				rv = parseTokenDetails(pstDHIReqResFields, stCPRespDtls,  &pszStart);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s:Error in parsing Token Details", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
					break;
				}
				break;
			}
			else
			{
				pszStart = pszStart + 1;
			}

		}
	}
	else
	{
		rv = ERR_INV_RESP;
		return rv;
	}
//	for(iCnt = 0; iCnt < CP_MAX_RESP_FIELDS; iCnt++)
//	{
//		debug_sprintf(szDbgMsg, "%s: CP[%d]: %s", __FUNCTION__,iCnt,stCPRespDtls[iCnt].elementValue );
//		CPHI_LIB_TRACE(szDbgMsg);
//	}

#if 0
	//LRC Calculation
	memset(szlrcValue, 0x00, sizeof(szlrcValue));

	pszCPResp = pszCPResp + 1;
	pszEnd = strchr(pszCPResp, ETX);
	pszEnd += 1;
	iDiffTemp = pszEnd - pszCPResp;
	pszCPResp = strndup(pszCPResp, iDiffTemp);

	sprintf(szlrcValue,"%c", findLrc(pszCPResp, strlen(pszCPResp)-1));

	debug_sprintf(szDbgMsg, "%s: Calculated LRC value:[%s]", __FUNCTION__, szlrcValue);
	CPHI_LIB_TRACE(szDbgMsg);

	if(strcmp(stCPRespDtls[CP_R_LRC].elementValue, szlrcValue) == 0)
	{
		debug_sprintf(szDbgMsg, "%s:Calculated LRC Value is matched with Response LRC Value", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Calculated LRC Value is not matched with Response LRC Value", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
#endif

	correctDHIResponseFields(pstDHIReqResFields);

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsed CPHI Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv );
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: parseCPResponseForBatchMgmt
 *
 * Description	: This function parses the CP Response for Batch Mgmt.
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

int parseCPResponseForBatchMgmt(char* pszCPResp, TRANDTLS_PTYPE pstDHIReqResFields, CPTRANDTLS_PTYPE stCPRespDtls, VAL_PAYMENTTYPEHEADNODE_PTYPE stPaymentHeadNode)
{
	int 						rv									= SUCCESS;
	int 						iCnt								= 0;
	int 						iDiffTemp 							= 0;
	int							iAppLogEnabled						= 0;
	char						szTemp[50]							= "";
	char						szAppLogData[256]					= "";
	//char 						szlrcValue[3]						= "";
	char*						szErrTemp							= NULL;
	char*						pszStart							= NULL;
	char*						pszEnd				= NULL;
	VAL_PAYMENTTYPENODE_PTYPE 	pstTempPaymentType	= NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[4700]		= "";
#elif DEBUG
	char			szDbgMsg[5326]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsing CPHI Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	pszStart = pszCPResp;
	if(pszStart == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL PARAMS PARSED", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}
	else
	{
		/*	case CP_ACTION_CODE:*/
		pszStart = pszStart + 1;
		strncpy(stCPRespDtls[CP_ACTION_CODE].elementValue, pszStart, 1);
		pszStart = pszStart + 1;
		if(strcmp(stCPRespDtls[CP_ACTION_CODE].elementValue, "A") == SUCCESS)
		{
			pstDHIReqResFields[eRESULT].elementValue = strdup("APPROVED");
		}
		else
		{
			pstDHIReqResFields[eRESULT].elementValue = strdup("ERROR");
		}
		/*	case CP_ADDR_VER_RESP_CODE: */
		strncpy(stCPRespDtls[CP_ADDR_VER_RESP_CODE].elementValue, pszStart, 1);
		pstDHIReqResFields[eAVS_CODE].elementValue = strdup(stCPRespDtls[CP_ADDR_VER_RESP_CODE].elementValue);
		pszStart = pszStart + 1;

		memset(szTemp, 0x00, sizeof(szTemp));
		/* case CP_AUTH_ERROR_CODE */
		strncpy(stCPRespDtls[CP_AUTH_ERROR_CODE].elementValue, pszStart, 6);
		strcpy(szTemp, stCPRespDtls[CP_AUTH_ERROR_CODE].elementValue);
		if(strcmp(stCPRespDtls[CP_ACTION_CODE].elementValue, "A") == 0)
		{
			pstDHIReqResFields[eAUTH_CODE].elementValue = strdup(szTemp);
		}
		else
		{
			szErrTemp = strchr(szTemp,' ');
			iDiffTemp = szErrTemp - szTemp;
			pstDHIReqResFields[eHOST_RESPCODE].elementValue = strndup(szTemp, iDiffTemp);
		}
		pszStart = pszStart + 6;

		/*	case CP_BATCH_NUMBER:*/
		strncpy(stCPRespDtls[CP_BATCH_NUMBER].elementValue, pszStart, 6);
		pszStart = pszStart + 6;

		/*	case CP_RET_REF_NUM:*/
		strncpy(stCPRespDtls[CP_RET_REF_NUM].elementValue, pszStart, 8);
		pszStart = pszStart + 8;

		/*	case CP_SEQ_NUM: */
		strncpy(stCPRespDtls[CP_SEQ_NUM].elementValue, pszStart, 6);
		pstDHIReqResFields[eTRANS_SEQ_NUM].elementValue = strdup(szTemp);
		pstDHIReqResFields[eCTROUTD].elementValue = strdup(szTemp);
		pszStart = pszStart + 6;

		/*	case CP_RESP_MSG: */
		strncpy(stCPRespDtls[CP_RESP_MSG].elementValue, pszStart, 32);
		pszStart = pszStart + 32;
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue = strdup(stCPRespDtls[CP_RESP_MSG].elementValue);

		/*	case CP_MERCH_CONTROL_IND: */
		pszEnd = pszStart;
		while(*pszEnd != FS)
		{
			pszEnd++;
		}
		strncpy(stCPRespDtls[CP_MERCH_CONTROL_IND].elementValue, pszStart, pszEnd - pszStart);
		pszStart = pszEnd;

		/* **********CHECK IF THE RESPONSE IS ERROR AND THEN PROCEED************** */
		if(!strcmp(stCPRespDtls[CP_MERCH_CONTROL_IND].elementValue, "MC") && !strcmp(stCPRespDtls[CP_ACTION_CODE].elementValue, "E") )
		{
			/* Extracting Auth Network ID */
			pszStart += 2;
			strncpy(stCPRespDtls[CP_AUTH_NETW_ID].elementValue, pszStart, 2);
			pszStart += 2;
			/* Extracting Auth source */
			strncpy(stCPRespDtls[CP_AUTH_SOURCE].elementValue, pszStart, 1);
			pszStart += 1;
			/* Do we need to do this?? Extracting tokens */
			pszStart += 2;

			pszEnd = pszStart;
			while(*pszEnd != FS)
			{
				pszEnd++;
			}

			pszStart = pszEnd;
			CPHI_LIB_TRACE("Returning because of Error condition");
			return rv;
		}
		/*	case CP_DOWNLD_FLG: */
		//pszStart += 1;
		pszStart = pszStart + 1;
		strncpy(stCPRespDtls[CP_DOWNLD_FLG].elementValue, pszStart, 1);
		pszStart = pszStart + 1;

		/*	case CP_MUL_MSG_FLG: */
		strncpy(stCPRespDtls[CP_MUL_MSG_FLG].elementValue, pszStart, 1);
		pszStart = pszStart + 1;

		/*	case CP_BATCH_OPEN_DATE_TIME: */
		strncpy(stCPRespDtls[CP_BATCH_OPEN_DATE_TIME].elementValue, pszStart, 10);
		pszStart = pszStart + 10;

		/*	case CP_BATCH_CLOSE_DATE_TIME: */
		strncpy(stCPRespDtls[CP_BATCH_CLOSE_DATE_TIME].elementValue, pszStart, 10);
		pszStart = pszStart + 10;

		/*	case CP_BATCH_TRAN_COUNT:*/
		strncpy(stCPRespDtls[CP_BATCH_TRAN_COUNT].elementValue, pszStart, 6);
		pszStart = pszStart + 6;

		/*case CP_BATCH_NET_AMT:*/
		pszEnd = pszStart;
		while(*pszEnd != FS)
		{
			pszEnd++;
		}
		strncpy(stCPRespDtls[CP_BATCH_NET_AMT].elementValue, pszStart, pszEnd - pszStart);
		strcpy(stPaymentHeadNode->szNetSettlemntAmt, stCPRespDtls[CP_BATCH_NET_AMT].elementValue);
		pszStart = pszEnd;

		//case CP_ADDTL_DATA:
		pszStart += 1;
		pszEnd = pszStart;
		iCnt = 1;
		while(iCnt < 18)
		{
			if(*pszEnd != FS)
			{
				iCnt++;
				pszEnd++;
			}
			else
			{
				break;
			}
		}
		if(iCnt <= 17)
		{
			strncpy(stCPRespDtls[CP_ADDTL_DATA].elementValue, pszStart, pszEnd - pszStart);
			pszStart = pszEnd;
		}


		debug_sprintf(szDbgMsg, "%s: Additional Data [%s] ", __FUNCTION__, stCPRespDtls[CP_ADDTL_DATA].elementValue);
		CPHI_LIB_TRACE(szDbgMsg);

		//pszStart += 1;
		for(iCnt = 0; iCnt < 4; iCnt++)
		{
			if(*pszStart == ETX)
			{
				break;
			}
			pszStart += 1;
			pstTempPaymentType = (VAL_PAYMENTTYPENODE_PTYPE)malloc(sizeof(VAL_PAYMENTTYPENODE_STYPE));
			if(pstTempPaymentType == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				return rv;
			}
			strncpy(pstTempPaymentType->szPayType, pszStart, 3);
			pstTempPaymentType->szPayType[3] = '\0';
			pszStart += 3;
			debug_sprintf(szDbgMsg, "%s: Card Tag[%d]: [%s]", __FUNCTION__, iCnt, pstTempPaymentType->szPayType);
			CPHI_LIB_TRACE(szDbgMsg);

			strncpy(pstTempPaymentType->szTransCnt, pszStart, 6);
			pszStart += 6;
			pstTempPaymentType->szTransCnt[6] = '\0';
			debug_sprintf(szDbgMsg, "%s: Trans Cnt[%d]: [%s]", __FUNCTION__, iCnt, pstTempPaymentType->szTransCnt);
			CPHI_LIB_TRACE(szDbgMsg);

			pszEnd = pszStart;
			while(*pszEnd != FS && *pszEnd != ETX )
			{
				pszEnd++;
			}
			strncpy(pstTempPaymentType->szNetAmt, pszStart, pszEnd - pszStart);
			*(pstTempPaymentType->szNetAmt + (pszEnd - pszStart)) = '\0';
			pszStart = pszEnd;

			debug_sprintf(szDbgMsg, "%s: Net Amt[%d]: [%s]", __FUNCTION__, iCnt, pstTempPaymentType->szNetAmt);
			CPHI_LIB_TRACE(szDbgMsg);
			pstTempPaymentType->next = NULL;

			stPaymentHeadNode->pstHead = addNode(stPaymentHeadNode->pstHead, pstTempPaymentType);
		}

		stPaymentHeadNode->iMaxHostTotalGroup = iCnt;
	}
//	/* Need to remove this later */
//	display(stPaymentHeadNode->pstHead);
//	for(iCnt = 0; iCnt < CP_MAX_RESP_FIELDS; iCnt++)
//	{
//		debug_sprintf(szDbgMsg, "%s: CP[%d]: %s", __FUNCTION__,iCnt,stCPRespDtls[iCnt].elementValue );
//		CPHI_LIB_TRACE(szDbgMsg);
//	}
//

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsed CPHI Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv );
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: addNode
 *
 * Description	: Getter function for the result code
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
VAL_PAYMENTTYPENODE_PTYPE addNode(VAL_PAYMENTTYPENODE_PTYPE pstHead,VAL_PAYMENTTYPENODE_PTYPE node)
{
	VAL_PAYMENTTYPENODE_PTYPE temp = NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif


	temp = pstHead;
	if(temp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Adding first node", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		temp = node;
		return temp;
	}
	while(temp->next != NULL)
	{
		temp = temp->next;
	}

	temp->next = node;

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	return pstHead;
}
/* NEED TO REMOVE THIS */
void display(VAL_PAYMENTTYPENODE_PTYPE head)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(head == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Empty LL", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

	}
	else
	{
		while(head != NULL)
		{
			debug_sprintf(szDbgMsg, "%s:Card Tag[%s], TransCount[%s] and TransAmt[%s]", __FUNCTION__,head->szPayType, head->szTransCnt, head->szNetAmt);
			CPHI_LIB_TRACE(szDbgMsg);
			head = head->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s:Returning", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

}
/*
 * ============================================================================
 * Function Name: frameSSIRespForCutOverOrSiteTotals
 *
 * Description	: Getter function for the result code
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int frameSSIRespForReportCommand(TRANDTLS_PTYPE pstSSIReqResFields, VAL_PAYMENTTYPEHEADNODE_PTYPE pstPaymentHead)
{
	int							rv					= SUCCESS;
	int							iAppLogEnabled		= 0;
	int							iCnt				= 0;
	int							iSize				= 0;
	xmlDocPtr					docPtr				= NULL;
	xmlNodePtr					rootPtr				= NULL;
	xmlNodePtr					recordsPtr			= NULL;
	xmlNodePtr					recordPtr			= NULL;
	xmlNodePtr					elemNodePtr			= NULL;
	char						szAppLogData[256]	= "";
	char						szAppLogDiag[256]	= "";
	char*						pszCPHIResp			= NULL;
	VAL_PAYMENTTYPENODE_PTYPE	pstTempHead 		= NULL;
	VAL_PAYMENTTYPENODE_PTYPE	pstTemp 			= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Host Total Command Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtlsForSSI(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstSSIReqResFields[eCOMMAND].elementValue != NULL)
		{
			CPHI_LIB_TRACE("Adding Command"); //Praveen_P1: testing purpose please remove it

			elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "COMMAND", BAD_CAST pstSSIReqResFields[eCOMMAND].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding COMMAND field", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
			}
		}
		else
		{
			CPHI_LIB_TRACE("Not Adding Command"); //Praveen_P1: testing purpose please remove it
		}

		/* If the XML message tree has formed correctly, dump this message into
		 * the string provided as parameter */
		//Create the root node
		//recordsPtr = xmlNewNode(NULL, BAD_CAST "RECORDS");
		/* HOST TOTAL DETAILS GROUP */
		/* NEED TO REVIEW THIS */

		pszCPHIResp = (char *)malloc(NUMBER_CONVERION);
		if(pszCPHIResp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;

			return rv;
		}

		memset(pszCPHIResp,0x00,NUMBER_CONVERION);
		sprintf(pszCPHIResp,"%d", pstPaymentHead->iMaxHostTotalGroup);

		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "MAX_NUM_RECORDS_RETURNED", BAD_CAST pszCPHIResp);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMINATION_STATUS field", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		/* Free the temporary memory used for storing the number as string */
		free(pszCPHIResp);

		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "NET_SETTLE_AMT", BAD_CAST pstPaymentHead->szNetSettlemntAmt);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMINATION_STATUS field", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		/* Creating RECORDS tree */
		recordsPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "RECORDS", NULL);
		if(recordsPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create records", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			return rv;
		}
		pstTempHead = pstPaymentHead->pstHead;
		if(pstTempHead != NULL)
		{
			for(iCnt = 0; iCnt < pstPaymentHead->iMaxHostTotalGroup;iCnt++)

			{
				recordPtr = xmlNewChild(recordsPtr, NULL, BAD_CAST "RECORD",NULL);
				if(recordPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create record", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					return rv;
				}

				elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "CARDTAG", BAD_CAST pstTempHead->szPayType);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create CARDTAG", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					return rv;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: CARDTAG: %s", __FUNCTION__, pstTempHead->szPayType);
					CPHI_LIB_TRACE(szDbgMsg);
				}
				elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "ITEMCOUNT", BAD_CAST pstTempHead->szTransCnt);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create ITEMCOUNT", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					return rv;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: ITEMCOUNT: %s", __FUNCTION__,pstTempHead->szTransCnt);
					CPHI_LIB_TRACE(szDbgMsg);
				}
				elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "ITEMAMOUNT", BAD_CAST pstTempHead->szNetAmt);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create ITEMAMOUNT", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					return rv;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Amount : %s", __FUNCTION__, pstTempHead->szNetAmt);
					CPHI_LIB_TRACE(szDbgMsg);
				}
				pstTempHead = pstTempHead->next;

			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Records not present", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

		}
		xmlDocDumpFormatMemory(docPtr, (xmlChar **)&pstSSIReqResFields[eREPORT_RESPONSE].elementValue, &iSize, 1);

		iCnt = strlen(pstSSIReqResFields[eREPORT_RESPONSE].elementValue);

		debug_sprintf(szDbgMsg, "%s: Length is: %d", __FUNCTION__, iCnt );
		CPHI_LIB_TRACE(szDbgMsg);

		if(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue == NULL)
		{
			pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue = (char *)malloc(NUMBER_CONVERION);
			if(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;

				return rv;
			}

			memset(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue,0x00,NUMBER_CONVERION);
			sprintf(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue,"%d", iSize);

		}

		CPHI_LIB_TRACE(pstSSIReqResFields[eREPORT_RESPONSE].elementValue);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);

		}

		/*if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);

		}*/

		break;
	}

	if(pstPaymentHead->pstHead != NULL)
	{
		pstTempHead = pstPaymentHead->pstHead;

		while(pstTempHead != NULL)
		{
			pstTemp = pstTempHead ->next;
			free(pstTempHead);
			pstTempHead = pstTemp;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: buildGenRespDtls
 *
 * Description	: This function adds the general response details to the
 * 				  xml doc
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildGenRespDtlsForSSI(xmlDocPtr docPtr, xmlNode * rootNode, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv				= SUCCESS;
	char 			szTmpVal[21+1]	= "";
	xmlNodePtr		elemNodePtr		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(docPtr == NULL || rootNode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param(docPtr/rootNode) is NULL", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(szTmpVal,0x00,sizeof(szTmpVal));
	getMerchantNum(szTmpVal);
	pstSSIReqResFields[eMERCHID].elementValue = strdup(szTmpVal);

	/* Get merchant ID */
	memset(szTmpVal,0x00,sizeof(szTmpVal));
	getTerminalNum(szTmpVal);
	pstSSIReqResFields[eTERMID].elementValue = strdup(szTmpVal);

	if(pstSSIReqResFields[eRESULT].elementValue != NULL)			//Action Code
	{
		if(strcmp(pstSSIReqResFields[eRESULT].elementValue, "APPROVED") == 0)
		{
			pstSSIReqResFields[eTERMINATION_STATUS].elementValue = strdup("SUCCESS");

			pstSSIReqResFields[eRESULT_CODE].elementValue	= strdup("5");

			free(pstSSIReqResFields[eRESULT].elementValue);
			pstSSIReqResFields[eRESULT].elementValue		= strdup("OK");
		}
		else if(strcmp(pstSSIReqResFields[eRESULT].elementValue, "ERROR") == 0)
		{
			pstSSIReqResFields[eTERMINATION_STATUS].elementValue = strdup("SUCCESS");

			pstSSIReqResFields[eRESULT_CODE].elementValue 	= strdup("6");
			free(pstSSIReqResFields[eRESULT].elementValue);
			pstSSIReqResFields[eRESULT].elementValue 		= strdup("DECLINED");
		}
		else
		{
			pstSSIReqResFields[eTERMINATION_STATUS].elementValue = strdup("NOT_PROCESSED");
			pstSSIReqResFields[eRESULT_CODE].elementValue 	= strdup("9000");
			free(pstSSIReqResFields[eRESULT].elementValue);
			pstSSIReqResFields[eRESULT].elementValue		= strdup("Error");
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error while adding RESULT", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	/* Adding TERMINATION_STATUS field */
	if(pstSSIReqResFields[eTERMINATION_STATUS].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST pstSSIReqResFields[eTERMINATION_STATUS].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMINATION_STATUS field", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding RESULT field */
	if(pstSSIReqResFields[eRESULT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT", BAD_CAST pstSSIReqResFields[eRESULT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT field", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding RESULT_CODE field */
	if(pstSSIReqResFields[eRESULT_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT_CODE", BAD_CAST pstSSIReqResFields[eRESULT_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT_CODE field", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding RESPONSE_TEXT field */
	if(pstSSIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST pstSSIReqResFields[eRESPONSE_TEXT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESPONSE_TEXT field", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding eHOST_RESPCODE field */
	if(pstSSIReqResFields[eHOST_RESPCODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST pstSSIReqResFields[eHOST_RESPCODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding HOST_RESPCODE field", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: fillErrorDetails
 *
 * Description	: This function is responsible for filling error details to DHI on occurence
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void fillErrorDetails(int iErrCode, TRANDTLS_PTYPE pstDHIReqResFields, char* szRespMsg)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	char 	szTmpVal[50];

	int iRetHostInfo = 0;

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	switch(iErrCode)
	{
	case ERR_UNSUPP_CMD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59006");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Command Not Supported by Processor");
		break;

	case ERR_RESP_TO:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("23");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Response Timeout");
		iRetHostInfo = 1;
		break;

	case ERR_CONN_TO:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Connection Timeout");
		iRetHostInfo = 1;
		break;

	case ERR_CONN_FAIL:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Connection Disrupted");
		iRetHostInfo = 1;
		break;

	case ERR_HOST_TRAN_REVERSED:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("22");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Transaction Reversed");
		iRetHostInfo = 1;
		break;

	case ERR_HOST_TRAN_TIMEOUT:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("23");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Transaction Timed out");
		iRetHostInfo = 1;
		break;

	case ERR_HOST_NOT_AVAILABLE:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable");
		iRetHostInfo = 1;
		break;

	case ERR_CFG_PARAMS:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Configuration Error");
		break;

	case ERR_INV_COMBI:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59041");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Invalid Combination");
		break;

	case ERR_FLD_REQD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59041");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Field is Required");
		break;

	case ERR_INV_FLD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59043");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Field is Invalid");
		break;

	case ERR_NO_FLD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59045");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Field Does Not Exist");
		break;

	case ERR_INV_FLD_VAL:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59045");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Value Not Valid For Field");
		break;

	case ERR_INV_RESP:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("HOST_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Invalid Response From Host");
		break;

	case ERR_HOST_ERROR:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		//pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup(szStagMsg);
		break;

	case ERR_INV_CTROUTD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("CTROUTD Not Found in the Current Batch");
		break;

	case ERR_SYSTEM:
	case FAILURE:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59020");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("INTERNAL_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("System Error");
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Invalid error code ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	/* below piece of code is for the response text(for the text other than text mention above)*/
	if(strlen(szRespMsg)> 0)
	{
		if(pstDHIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
		{
			free(pstDHIReqResFields[eRESPONSE_TEXT].elementValue);
		}

		pstDHIReqResFields[eRESPONSE_TEXT].elementValue= strdup(szRespMsg);
	}

	/* It is required to return Merchant ID and terminal ID even in case of error.
	 * Hence populating the same from the configured values.
	 */
	/* Get Terminal ID */
	if(iRetHostInfo)
	{
		memset(szTmpVal,0x00,sizeof(szTmpVal));
		getMerchantNum(szTmpVal);
		pstDHIReqResFields[eMERCHID].elementValue = strdup(szTmpVal);

		/* Get merchant ID */
		memset(szTmpVal,0x00,sizeof(szTmpVal));
		getTerminalNum(szTmpVal);
		pstDHIReqResFields[eTERMID].elementValue = strdup(szTmpVal);
	}
	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
}



/*
 * ================================================================================
 * Function Name: parsingTokenResponse
 *
 * Description	: This function parses the Token Details and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 *
 *==================================================================================
 */

int parseTokenDetails(TRANDTLS_PTYPE pstDHIReqResFields, CPTRANDTLS_PTYPE pstCPRespFields, char** pszFSTemp)
{
	int		 			rv 					= SUCCESS;
	//	int 				iCmd			= 0;
	int 				iDHIIndex			= -1;
	int 				iCPIndex			= -1;
	int					iDiffTemp			= 0;
	char				szLRCTemp[5]		= "";
	char				szTemp[19+1]		= "";
	char*				pszTagName			= NULL;
	char*    			pszTagValue			= NULL;
	char*				pszStartTemp		= NULL;
	char*				pszEndTemp			= NULL;
	char*				pszEMVTags			= NULL;
	char*				pszTemp				= NULL;
	float 				fAmount				= 0.00;
	HASHLST_PTYPE 		pstHashLst			= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ----Enter----", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	pszStartTemp = *pszFSTemp;

	while(1)
	{
		if(*pszStartTemp == ETX)
		{
			debug_sprintf(szDbgMsg, "%s: End of the Token Response", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			pszStartTemp += 1;

			memset(szLRCTemp, 0x00, sizeof(szLRCTemp));
			strncpy(szLRCTemp, pszStartTemp, 1);
			strcpy(pstCPRespFields[CP_R_LRC].elementValue, szLRCTemp);
			debug_sprintf(szDbgMsg, "%s:LRC Value:%c", __FUNCTION__, *pszStartTemp);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
		else
		{
			pszTagName = strndup(pszStartTemp,2);
			pstHashLst = lookupCPHashTable(pszTagName);

			debug_sprintf(szDbgMsg, "%s: Executed LookupCPhashtable Function", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			if(pstHashLst == NULL )
			{
				free(pszTagName);
				debug_sprintf(szDbgMsg, "%s: Did not find entry for this Tag[%s] in the Hash Table!!! So ignoring this tag", __FUNCTION__, pszTagName);
				CPHI_LIB_TRACE(szDbgMsg);
				debug_sprintf(szDbgMsg, "%s: Checking for the Next Token", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				pszEndTemp = strchr(pszStartTemp, FS);
				pszEndTemp += 1;
				if(pszEndTemp != NULL)
				{
					pszStartTemp = pszEndTemp;
					continue;
				}
				else
				{
					break;
				}
			}

			debug_sprintf(szDbgMsg, "%s: Index for this Tag in the structure is [%d]", __FUNCTION__, pstHashLst->iTagIndexVal);
			CPHI_LIB_TRACE(szDbgMsg);

			//
			//		if(pstHashLst->iTagIndexVal > eELEMENT_INDEX_MAX) //Safety Check
			//		{
			//			debug_sprintf(szDbgMsg, "%s: Invalid Index Value!!!", __FUNCTION__);
			//			CPHI_LIB_TRACE(szDbgMsg);
			//			rv = FAILURE;
			//			break;
			//			//Should not be the case, something wrong
			//		}

			iDHIIndex = genCPRespLst[pstHashLst->iTagIndexVal].iDHIIndex;
			iCPIndex  = genCPRespLst[pstHashLst->iTagIndexVal].iCPIndex;

			pszStartTemp += 2;												//Moving the pointer to the start of the token value

			pszEndTemp = pszStartTemp;										//Assigning the Start pointer address to End pointer address

			if(((pszEndTemp = strchr(pszStartTemp, FS)) != NULL) || (pszEndTemp = strchr(pszStartTemp, ETX)) != NULL)
			{
				iDiffTemp = pszEndTemp - pszStartTemp;						//Diiference of End and Start Pointer

				debug_sprintf(szDbgMsg, "%s: Length of the TagValue is %d", __FUNCTION__, iDiffTemp);
				CPHI_LIB_TRACE(szDbgMsg);

				pszTagValue = strndup(pszStartTemp,iDiffTemp);


				if(iDHIIndex != NO_ENUM)
				{

					pstDHIReqResFields[iDHIIndex].elementValue = strdup((char*)pszTagValue);
					debug_sprintf(szDbgMsg, "%s: Parsed to DHI ENUM", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
				}

				if(iCPIndex != NO_ENUM)
				{

					if (strcmp(pszTagName,"EM") == 0)
					{
						pszEMVTags = pszTagValue + 3;
						strcpy(pstCPRespFields[CP_R_CHIP_CARD_DATA].elementValue, pszEMVTags);

						if(pstDHIReqResFields[eEMV_TAGS_RESP].elementValue != NULL)
						{
							free(pstDHIReqResFields[eEMV_TAGS_RESP].elementValue);					// for follow on transactions this field will be filled in framing request, so updating with latest response tags.
						}

						pstDHIReqResFields[eEMV_TAGS_RESP].elementValue = strdup(pszEMVTags);

						debug_sprintf(szDbgMsg, "%s: Parsed to EM Tags", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
					}
					else if(strcmp(pszTagName,"CV") == 0)
					{
						pszTemp = pszTagValue + 2;
						strcpy(pstCPRespFields[CP_R_ENHANCED_CARD].elementValue, pszTemp);
						pstDHIReqResFields[eCVV2_CODE].elementValue = strdup(pszTemp);
						debug_sprintf(szDbgMsg, "%s: Parsed to CV Token", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
					}
					else if(strcmp(pszTagName,"CR") == 0)
					{
						memset(szTemp, 0x00, sizeof(szTemp));
						strcpy(szTemp, pszTagName);
						strcat(szTemp, pszTagValue);
						strcpy(pstCPRespFields[CP_R_CARD_AUTH].elementValue, szTemp);
						debug_sprintf(szDbgMsg, "%s: Parsed to CR Token", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
					}
					else if(strcmp(pszTagName, "T4") == 0)
					{
						pszTemp = pszTagValue + 19;
						memset(szTemp, 0x00, sizeof(szTemp));
						strncpy(szTemp, pszTemp, 3);
						if(!(strcmp(szTemp, "000")))
						{
							/*After Discussion with Chase in call Dated 02/JUNE/2016 : Chase Recommended to send the token as it is received from Host*/
//							memset(szTemp, 0x00, sizeof(szTemp));
//							strncpy(szTemp, pszTagValue, 19);
//
//							if((pszTemp = (strchr(szTemp, ' '))) != NULL)
//							{
//								pstDHIReqResFields[eCARD_TOKEN].elementValue = strndup(szTemp, pszTemp-szTemp);
//							}
//							else
//							{
								pstDHIReqResFields[eCARD_TOKEN].elementValue = strndup(pszTagValue, 19);
//							}
						}
						debug_sprintf(szDbgMsg, "%s: Parsed to T4 Token", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
					}
					else if(strcmp(pszTagName, "SR") == 0)
					{
						strcpy(pstCPRespFields[CP_R_SEMTEK_CODE].elementValue, pszTagValue);
						pstDHIReqResFields[eVSP_CODE].elementValue = strndup(pszTagValue, 3);
						pszTemp = pszTagValue + 3;
						pstDHIReqResFields[eVSP_RESULTDESC].elementValue = strdup(pszTemp);
						debug_sprintf(szDbgMsg, "%s: Parsed to SR Token", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
					}
					else if(strcmp(pszTagName, "RN") == 0)
					{
						memset(szTemp, 0x00, sizeof(szTemp));
						strcpy(szTemp, pszTagName);
						strcat(szTemp, pszTagValue);
						strcpy(pstCPRespFields[CP_R_POS_RET_REF_NUM].elementValue, szTemp);
						debug_sprintf(szDbgMsg, "%s: Parsed to RN Token", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
					}
					else if(strcmp(pszTagName, "B6") == 0 )
					{
						/* We have avaliable balances in B6 and B7 tokens, As B6 token is for Merchant,Parsed B6 only.*/
						/*As B7 is for Cardholder, we are not Parsing*/
						/*Considering the Currency Code in US Dollers*/
						pszTemp = pszTagValue + 3;
						fAmount = atof(pszTemp);
						fAmount = fAmount/100;
						memset(szTemp, 0x00, sizeof(szTemp));
						sprintf(szTemp, "%.02f", fAmount);
						pstDHIReqResFields[eAVAIL_BALANCE].elementValue = strdup(szTemp);
						debug_sprintf(szDbgMsg, "%s: Parsed to B6 Token", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
					}
					else
					{
						strcpy(pstCPRespFields[iCPIndex].elementValue, pszTagValue);
						debug_sprintf(szDbgMsg, "%s: Parsed to CPHI ENUM", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);
					}
				}


				free(pszTagName);
				free(pszTagValue);

				pszStartTemp = pszEndTemp;									//Assigning the End pointer address to Start pointer address

				if(*pszStartTemp == ETX)
				{
					debug_sprintf(szDbgMsg, "%s: End of the Token Response", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
					pszStartTemp += 1;

					memset(szLRCTemp, 0x00, sizeof(szLRCTemp));
					strncpy(szLRCTemp, pszStartTemp, 1);
					strcpy(pstCPRespFields[CP_R_LRC].elementValue, szLRCTemp);

					debug_sprintf(szDbgMsg, "%s:LRC Value:%c", __FUNCTION__,*pszStartTemp);

					CPHI_LIB_TRACE(szDbgMsg);

					*pszFSTemp = pszStartTemp;
					break;
				}
				while(*pszStartTemp == FS)
				{
					pszStartTemp += 1;
				}

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Response is Incorrect[%d]", __FUNCTION__, pstHashLst->iTagIndexVal);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);


	return rv;
}

/*
 * ============================================================================
 * Function Name: correctDHIResponseFields
 *
 * Description	: This function  parses the CP response and then fills the transaction
 * 				  details structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int correctDHIResponseFields(TRANDTLS_PTYPE pstDHIReqResFields)
{
	int			rv 			 		  = SUCCESS;
	char        szTmpVal[12+1]		  = "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szTmpVal,0x00,sizeof(szTmpVal));
	getMerchantNum(szTmpVal);
	pstDHIReqResFields[eMERCHID].elementValue = strdup(szTmpVal);

	/* Get merchant ID */
	memset(szTmpVal,0x00,sizeof(szTmpVal));
	getTerminalNum(szTmpVal);
	pstDHIReqResFields[eTERMID].elementValue = strdup(szTmpVal);

	while(1)
	{
		if(pstDHIReqResFields[eVSP_REG].elementValue != NULL && atoi(pstDHIReqResFields[eVSP_REG].elementValue) == 1)
		{
			pstDHIReqResFields[eTERMINATION_STATUS].elementValue = strdup("SUCCESS");

			if(strcmp(pstDHIReqResFields[eVSP_CODE].elementValue, "935") == 0)
			{
				pstDHIReqResFields[eRESULT_CODE].elementValue 	= strdup("935");
			}
			else
			{
				pstDHIReqResFields[eRESULT_CODE].elementValue 	= strdup("6");
			}
			break;
		}

		if(pstDHIReqResFields[eRESULT].elementValue != NULL)			//Action Code
		{
			if(strcmp(pstDHIReqResFields[eRESULT].elementValue, "APPROVED") == 0)
			{
				pstDHIReqResFields[eTERMINATION_STATUS].elementValue = strdup("SUCCESS");

				if (pstDHIReqResFields[eCOMMAND].elementValue != NULL &&
						!strcmp(pstDHIReqResFields[eCOMMAND].elementValue, "VOID")) //For VOID we have send 7
				{
					pstDHIReqResFields[eRESULT_CODE].elementValue 	= strdup("7");

					free(pstDHIReqResFields[eRESULT].elementValue);
					pstDHIReqResFields[eRESULT].elementValue 		= strdup("VOIDED");
				}
				else
				{
					pstDHIReqResFields[eRESULT_CODE].elementValue	= strdup("5");

					free(pstDHIReqResFields[eRESULT].elementValue);
					pstDHIReqResFields[eRESULT].elementValue		= strdup("SUCCESS");
				}
			}
			else if(strcmp(pstDHIReqResFields[eRESULT].elementValue, "ERROR") == 0)
			{
				pstDHIReqResFields[eTERMINATION_STATUS].elementValue = strdup("SUCCESS");

				pstDHIReqResFields[eRESULT_CODE].elementValue 	= strdup("6");

				free(pstDHIReqResFields[eRESULT].elementValue);
				pstDHIReqResFields[eRESULT].elementValue 		= strdup("DECLINED");
			}
			else
			{
				pstDHIReqResFields[eTERMINATION_STATUS].elementValue = strdup("NOT_PROCESSED");

				pstDHIReqResFields[eRESULT_CODE].elementValue 	= strdup("9000");

				free(pstDHIReqResFields[eRESULT].elementValue);
				pstDHIReqResFields[eRESULT].elementValue		= strdup("Error");
			}
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: ParseEMVParamDwldToken
 *
 * Description	: This function  parses the EMVParmaeterDownloadTokens and then fills the transaction
 * 				  details structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

int parseEMVParamDwldToken(char **pszFSTemp)
{
	int		 			rv 				= SUCCESS;
	int					iDiffTemp		= 0;
	char				pszTagName[3]	= "";
	//char*    			pszTagValue		= "";
	char*				pszStartTemp	= "";
	char*				pszEndTemp		= "";
	VAL_EMVFI_PTYPE		pstFINode		= NULL;
	VAL_EMVFL_PTYPE		pstFLLmtNode	= NULL;
	VAL_EMVPK_PTYPE		pstPkNode		= NULL;

#ifdef DEBUG
	char			szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	pszStartTemp = *pszFSTemp;

	while(1)
	{
		strncpy(pszTagName,pszStartTemp,2);

		pszStartTemp+=2;

		pszEndTemp = pszStartTemp;

		if((pszEndTemp = strchr(pszStartTemp, FS)) != NULL)
		{
			iDiffTemp = pszEndTemp - pszStartTemp;

			debug_sprintf(szDbgMsg, "%s: Length of the TagValue is %d", __FUNCTION__, iDiffTemp);
			CPHI_LIB_TRACE(szDbgMsg);

			if(!strncmp(pszTagName,"FI",2))
			{
				pstFINode = (VAL_EMVFI_PTYPE)malloc(sizeof(VAL_EMVFI_STYPE));
				memset(pstFINode,0x00,sizeof(VAL_EMVFI_STYPE));
				pstFINode->next = NULL;

				strncpy(pstFINode->szAID,pszStartTemp,CPHI_EMVAID_LEN);
				pstFINode->szPkFbInd[0] = pszStartTemp[CPHI_EMVAID_LEN];

				pszStartTemp = pszStartTemp + CPHI_EMVAID_LEN + 1; //10 Bytes of AID + 1 Byte for Fallback Indicator
				if(gstEMVFIlst.pstFILst != NULL)
				{
					pstFINode->next = gstEMVFIlst.pstFILst;
				}
				debug_sprintf(szDbgMsg, "%s: AID %s FI1 :%s ", __FUNCTION__, pstFINode->szAID,pstFINode->szPkFbInd);
				CPHI_LIB_TRACE(szDbgMsg);
				gstEMVFIlst.pstFILst = pstFINode;
				gstEMVFIlst.iNumFbNodes++;
				pstFINode = NULL;
			}
			else if(!strncmp(pszTagName,"FL",2))
			{
				pstFLLmtNode = (VAL_EMVFL_PTYPE)malloc(sizeof(VAL_EMVFL_STYPE));
				memset(pstFLLmtNode,0x00,sizeof(VAL_EMVFL_STYPE));
				pstFLLmtNode->next = NULL;

				strncpy(pstFLLmtNode->szAID,pszStartTemp,CPHI_EMVAID_LEN);
				pszStartTemp = pszStartTemp + CPHI_EMVAID_LEN;

				strncpy(pstFLLmtNode->szOfflineFlrLmt,pszStartTemp,CPHI_EMVFLOOR_LMT_LEN);
				pszStartTemp = pszStartTemp + CPHI_EMVFLOOR_LMT_LEN;

				strncpy(pstFLLmtNode->szRandSelThresh,pszStartTemp,CPHI_EMVFLOOR_LMT_LEN);
				pszStartTemp = pszStartTemp + CPHI_EMVFLOOR_LMT_LEN;

				if(gstEMVFLlst.pstFLLst != NULL)
				{
					pstFLLmtNode->next = gstEMVFLlst.pstFLLst;
				}
				debug_sprintf(szDbgMsg, "%s: AID %s FL1 :%s FL2 :%s ", __FUNCTION__, pstFLLmtNode->szAID,pstFLLmtNode->szOfflineFlrLmt,pstFLLmtNode->szRandSelThresh);
				CPHI_LIB_TRACE(szDbgMsg);
				gstEMVFLlst.pstFLLst = pstFLLmtNode;
				gstEMVFLlst.iNumFlrLmtNodes++;
				pstFLLmtNode = NULL;
			}
			else if(!strncmp(pszTagName,"PK",2))
			{

				pstPkNode = (VAL_EMVPK_PTYPE)malloc(sizeof(VAL_EMVPK_STYPE));
				memset(pstPkNode,0x00,sizeof(VAL_EMVPK_STYPE));
				pstPkNode->next = NULL;

				//Extracting AID
				strncpy(pstPkNode->szAID,pszStartTemp,CPHI_EMVAID_LEN);
				pszStartTemp = pszStartTemp + CPHI_EMVAID_LEN;
				debug_sprintf(szDbgMsg, "%s: AID %s  ", __FUNCTION__, pstPkNode->szAID);
				CPHI_LIB_TRACE(szDbgMsg);

				//Extracting key Index
				strncpy(pstPkNode->szPkIndex,pszStartTemp,2);
				pszStartTemp = pszStartTemp + 2;
				debug_sprintf(szDbgMsg, "%s: Index %s  ", __FUNCTION__, pstPkNode->szPkIndex);
				CPHI_LIB_TRACE(szDbgMsg);

				//Extracting Modulus Length
				strncpy(pstPkNode->szPkModLen,pszStartTemp,4);
				pszStartTemp = pszStartTemp + 4;
				debug_sprintf(szDbgMsg, "%s: Mod Len %s  ", __FUNCTION__, pstPkNode->szPkModLen);
				CPHI_LIB_TRACE(szDbgMsg);

				//Extracting Public Key Modulus Value
				pstPkNode->pszPkMod = (char *)malloc(atoi(pstPkNode->szPkModLen) + 1);
				memset(pstPkNode->pszPkMod,0x00,atoi(pstPkNode->szPkModLen) + 1);
				strncpy(pstPkNode->pszPkMod ,pszStartTemp,atoi(pstPkNode->szPkModLen));
				pszStartTemp = pszStartTemp + atoi(pstPkNode->szPkModLen);
				debug_sprintf(szDbgMsg, "%s: Mod %s  ", __FUNCTION__, pstPkNode->pszPkMod);
				CPHI_LIB_TRACE(szDbgMsg);

				//Extracting PUblic Key Exponent Length
				strncpy(pstPkNode->szPkExpLen,pszStartTemp,2);
				pszStartTemp = pszStartTemp + 2;
				debug_sprintf(szDbgMsg, "%s: Exp Len %s  ", __FUNCTION__, pstPkNode->szPkExpLen);
				CPHI_LIB_TRACE(szDbgMsg);

				//Extracting PUblic Key Exponent Value
				pstPkNode->pszPkExp = (char *)malloc(atoi(pstPkNode->szPkExpLen) + 1);
				memset(pstPkNode->pszPkExp,0x00,atoi(pstPkNode->szPkExpLen) + 1);
				strncpy(pstPkNode->pszPkExp ,pszStartTemp,atoi(pstPkNode->szPkExpLen));
				pszStartTemp = pszStartTemp + atoi(pstPkNode->szPkExpLen);
				debug_sprintf(szDbgMsg, "%s: Exp  %s  ", __FUNCTION__, pstPkNode->pszPkExp);
				CPHI_LIB_TRACE(szDbgMsg);

				//Extracting PUblic Key Checksum Length
				strncpy(pstPkNode->szPkCkSumLen,pszStartTemp,2);
				pszStartTemp = pszStartTemp + 2;
				debug_sprintf(szDbgMsg, "%s: CKSUM Len  %s  ", __FUNCTION__, pstPkNode->szPkCkSumLen);
				CPHI_LIB_TRACE(szDbgMsg);

				//Extracting PUblic Key Checksum Value
				pstPkNode->pszPkCkSum = (char *)malloc(atoi(pstPkNode->szPkCkSumLen) + 1);
				memset(pstPkNode->pszPkCkSum,0x00,atoi(pstPkNode->szPkCkSumLen) + 1);
				strncpy(pstPkNode->pszPkCkSum ,pszStartTemp,atoi(pstPkNode->szPkCkSumLen));
				pszStartTemp = pszStartTemp + atoi(pstPkNode->szPkCkSumLen);
				debug_sprintf(szDbgMsg, "%s: CKSUM %s  ", __FUNCTION__, pstPkNode->pszPkCkSum);
				CPHI_LIB_TRACE(szDbgMsg);


				if(gstEMVPkLst.pstKeysLst == NULL)
				{
					gstEMVPkLst.pstKeysLst = pstPkNode;
					gstEMVPkLst.iMaxPublicKeys++;
				}
				else
				{
					pstPkNode->next = gstEMVPkLst.pstKeysLst;
					gstEMVPkLst.pstKeysLst = pstPkNode;
					gstEMVPkLst.iMaxPublicKeys++;
				}
				pstPkNode = NULL;
			}
			else if(!strncmp(pszTagName,"DS",2))
			{
				strncpy(gstEMVParamDwnld.szDwnldStat,pszStartTemp,1);
			}

			pszEndTemp ++;
			pszStartTemp = pszEndTemp;
			if(*pszStartTemp == ETX)
			{
				debug_sprintf(szDbgMsg, "%s: End of the Token Response", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				pszStartTemp += 1;

				*pszFSTemp = pszStartTemp;
				break;
			}

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Response is Incorrect", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: DelEMVFlIndNode
 *
 * Description	: This function deletes the FloorLimit Indicators
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

static void delEMVFlIndNode(VAL_EMVFI_PTYPE pstNode)
{

#ifdef DEBUG
	char            szDbgMsg[4096]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
	if(pstNode)
	{
		free(pstNode);
	}

	debug_sprintf(szDbgMsg, "%s: --- returning ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: DelEMVFlLmtNode
 *
 * Description	: This function deletes the FloorLimit Indicators
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

static void delEMVFlLmtNode(VAL_EMVFL_PTYPE pstNode)
{

#ifdef DEBUG
	char            szDbgMsg[4096]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
	if(pstNode)
	{
		free(pstNode);
	}

	debug_sprintf(szDbgMsg, "%s: --- returning ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: DelEMVPkNode
 *
 * Description	: This function deletes the Public Key Indicators
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void delEMVPkNode(VAL_EMVPK_PTYPE pstNode)
{

#ifdef DEBUG
	char            szDbgMsg[4096]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
	if(pstNode)
	{
		if(pstNode->pszPkCkSum)
			free(pstNode->pszPkCkSum);
		if(pstNode->pszPkExp)
			free(pstNode->pszPkExp);
		if(pstNode->pszPkMod)
			free(pstNode->pszPkMod);

		free(pstNode);
	}

	debug_sprintf(szDbgMsg, "%s: --- returning ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: frameSSIRespForEMVAdmin
 *
 * Description	: This function frames the SSI Response for EMV Admin Request.
 *
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int frameSSIRespForEMVAdmin(TRANDTLS_PTYPE pstDHIReqResFields)
{


	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	int				iCnt				= 0;
	int				iSize				= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	xmlNodePtr		pKeysLstPtr			= NULL;
	xmlNodePtr		recordPtr			= NULL;
	xmlNodePtr		elemNodePtr			= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";
	VAL_EMVFI_PTYPE		pstFINode		= gstEMVFIlst.pstFILst;
	VAL_EMVFL_PTYPE		pstFLLmtNode	= gstEMVFLlst.pstFLLst;
	VAL_EMVPK_PTYPE		pstPkNode		= gstEMVPkLst.pstKeysLst;
//	void *pstTempHead 	= NULL;
	void *pstDelNode = NULL;

	char*			pkFieldNames[6] 	=  {"PUBLIC_KEY_0","PUBLIC_KEY_1","PUBLIC_KEY_2","PUBLIC_KEY_3","PUBLIC_KEY_4","PUBLIC_KEY_5"};
	char*			flLmtFieldNames[6] 	=  {"OFFLINE_FLOOR_LIMIT_0","OFFLINE_FLOOR_LIMIT_1","OFFLINE_FLOOR_LIMIT_2","OFFLINE_FLOOR_LIMIT_3","OFFLINE_FLOOR_LIMIT_4","OFFLINE_FLOOR_LIMIT_5"};
	char*			fbIndFieldNames[6] 	=  {"FALLBACK_INDICATOR_0","FALLBACK_INDICATOR_1","FALLBACK_INDICATOR_2","FALLBACK_INDICATOR_3","FALLBACK_INDICATOR_4","FALLBACK_INDICATOR_5"};

#ifdef DEBUG
	char			szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing EMV Admin Command Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtlsForSSI(docPtr, rootPtr, pstDHIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

//		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "TROUTD", BAD_CAST "123");
//		if(elemNodePtr == NULL)
//		{
//			debug_sprintf(szDbgMsg, "%s: Error while adding TROUTD field", __FUNCTION__);
//			CPHI_LIB_TRACE(szDbgMsg);
//		}

		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "DOWNLOAD_STATUS", BAD_CAST gstEMVParamDwnld.szDwnldStat);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create DOWNLOAD_STATUS", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			return rv;
		}
		if(gstEMVPkLst.iMaxPublicKeys > 0)
		{
			/* Creating PUBLIC_KEYS Tree */
			pKeysLstPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "PUBLIC_KEY", NULL);
			if(pKeysLstPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				return rv;
			}

			if(pstPkNode != NULL)
			{
				for(iCnt = 0;pstPkNode != NULL && iCnt < 6 ;iCnt++)
				{
					recordPtr = xmlNewChild(pKeysLstPtr, NULL, BAD_CAST pkFieldNames[iCnt],NULL);
					if(recordPtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_CHECKSUM", BAD_CAST pstPkNode->pszPkCkSum);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_CHECKSUM", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_EXP_LEN", BAD_CAST pstPkNode->szPkExpLen);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_EXP_LEN", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "APP_ID", BAD_CAST pstPkNode->szAID);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create APP_ID", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_MOD", BAD_CAST pstPkNode->pszPkMod);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_MOD", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_CHECKSUM_LEN", BAD_CAST pstPkNode->szPkCkSumLen);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_CHECKSUM_LEN", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_IND", BAD_CAST "PK");
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_IND", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_MOD_LEN", BAD_CAST pstPkNode->szPkModLen);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_MOD_LEN", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_EXP", BAD_CAST pstPkNode->pszPkExp);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_EXP", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}


					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_INDEX", BAD_CAST pstPkNode->szPkIndex);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_INDEX", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					pstDelNode = (VAL_EMVPK_PTYPE)pstPkNode;
					gstEMVPkLst.pstKeysLst = pstPkNode = pstPkNode->next;
					delEMVPkNode(pstDelNode);
					pstDelNode = NULL;

					gstEMVPkLst.iMaxPublicKeys--;
				}
				if(!gstEMVPkLst.iMaxPublicKeys)
					gstEMVPkLst.pstKeysLst = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Public Keys  not present", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}

		if(gstEMVFLlst.iNumFlrLmtNodes > 0)
		{
			/* Creating PUBLIC_KEYS Tree */
			pKeysLstPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "OFFLINE_FLOOR_LIMIT", NULL);
			if(pKeysLstPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				return rv;
			}

			if(pstFLLmtNode != NULL)
			{
				for(iCnt = 0;pstFLLmtNode != NULL && iCnt < 6;iCnt++)
				{
					recordPtr = xmlNewChild(pKeysLstPtr, NULL, BAD_CAST flLmtFieldNames[iCnt],NULL);
					if(recordPtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "OFFLINE_FLOOR_LIMIT_IND", BAD_CAST "FL");
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_CHECKSUM", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "APP_ID", BAD_CAST pstFLLmtNode->szAID);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create APP_ID", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "OFFLINE_FLOOR_LIMIT", BAD_CAST pstFLLmtNode->szOfflineFlrLmt);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_MOD", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "FLOOR_LIMIT_THRESHOLD", BAD_CAST pstFLLmtNode->szRandSelThresh);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_CHECKSUM_LEN", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					pstDelNode = (VAL_EMVFL_PTYPE)pstFLLmtNode;
					gstEMVFLlst.pstFLLst = pstFLLmtNode = pstFLLmtNode->next;
					delEMVFlLmtNode(pstDelNode);
					pstDelNode = NULL;
					gstEMVFLlst.iNumFlrLmtNodes--;
				}

				if(!gstEMVFLlst.iNumFlrLmtNodes)
					gstEMVFLlst.pstFLLst = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Floor Limit Nodes Not Present", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		if(gstEMVFIlst.iNumFbNodes > 0)
		{
			/* Creating PUBLIC_KEYS Tree */
			pKeysLstPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "FALLBACK_INDICATOR", NULL);
			if(pKeysLstPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				return rv;
			}

			if(pstFINode != NULL)
			{
				for(iCnt = 0;pstFINode != NULL  && iCnt < 6;iCnt++)
				{
					recordPtr = xmlNewChild(pKeysLstPtr, NULL, BAD_CAST fbIndFieldNames[iCnt],NULL);
					if(recordPtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "FALLBACK_VALUE", BAD_CAST pstFINode->szPkFbInd);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_CHECKSUM", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "APP_ID", BAD_CAST pstFINode->szAID);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create APP_ID", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "FALLBACK_IND", BAD_CAST "FI");
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_MOD", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					pstDelNode = (VAL_EMVFI_PTYPE)pstFINode;
					gstEMVFIlst.pstFILst = pstFINode = pstFINode->next;
					delEMVFlIndNode(pstDelNode);
					pstDelNode = NULL;
					gstEMVFIlst.iNumFbNodes--;
				}
				if(!gstEMVFIlst.iNumFbNodes)
					gstEMVFIlst.pstFILst = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Fallback Indicator Nodes Not Present", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		break;
	}

	xmlDocDumpFormatMemory(docPtr, (xmlChar **)&pstDHIReqResFields[eEMVADMIN_RESPONSE].elementValue, &iSize, 1);

	iCnt = strlen(pstDHIReqResFields[eEMVADMIN_RESPONSE].elementValue);

	debug_sprintf(szDbgMsg, "%s: Length is: %d", __FUNCTION__, iCnt );
	CPHI_LIB_TRACE(szDbgMsg);

	if(pstDHIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue == NULL)
	{
		pstDHIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue = (char *)malloc(10);
		if(pstDHIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;

			return rv;
		}

		memset(pstDHIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue,0x00,10);
		sprintf(pstDHIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue,"%d", iSize);

	}
	CPHI_LIB_TRACE(pstDHIReqResFields[eEMVADMIN_RESPONSE].elementValue);

	if(!strcmp(gstEMVParamDwnld.szDwnldStat,"F"))
	{
		if(gstEMVFIlst.pstFILst != NULL || gstEMVFIlst.iNumFbNodes != 0 || gstEMVFLlst.pstFLLst != NULL || gstEMVFLlst.iNumFlrLmtNodes != 0 || gstEMVPkLst.pstKeysLst != NULL || gstEMVPkLst.iMaxPublicKeys != 0)
		{
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		initEMVDwnloadParams();
	}

	/* Free the memory allocated for the xml document tree */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsed EMV Admin Command Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	return rv;

}

/*
 * ============================================================================
 * Function Name: parseCPResponse
 *
 * Description	: This function parses the CP Response and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int parseCPEMVDwldResp(char* pszCPResp, TRANDTLS_PTYPE pstDHIReqResFields, CPTRANDTLS_PTYPE pstCPRespDetails)
{

	int		 				rv 										= SUCCESS;
	int 					iCnt									= 0;
//	int						iDiffTemp								= 0;
	int						iAppLogEnabled							= 0;
	char					szTemp[64]								= "";
//	char 					szlrcValue[5]							= "";
	char*					pszFSTemp								= "";
	char*					pszStartTemp							= "";
//	char*					pszEndTemp								= "";
	char					szAppLogData[256]						= "";

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

#if 0
#ifdef DEVDEBUG
	if(strlen(pszRCResp) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s: %s", __FUNCTION__, pszCPResp);
		CPHI_LIB_TRACE(szDbgMsg);
	}
#endif
#endif

	iAppLogEnabled = isAppLogEnabled();

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsing CPHI Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	pszStartTemp = pszCPResp;
	pszFSTemp	 = pszCPResp;							//For Field Seperator
	//Parse details from 1 to 10.
	if(*pszStartTemp == STX)
	{
		debug_sprintf(szDbgMsg, "%s:Start of Response", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	pszStartTemp += 1;

	//Action Code
	memset(szTemp ,0x00 ,sizeof(szTemp));
	strncpy(szTemp ,pszStartTemp ,1);
	strcpy(pstCPRespDetails[CP_ACTION_CODE].elementValue, szTemp);
	if(strcmp(pstCPRespDetails[CP_ACTION_CODE].elementValue,"A") == 0)
	{
		pstDHIReqResFields[eRESULT].elementValue = strdup("APPROVED");
	}
	else
	{
		pstDHIReqResFields[eRESULT].elementValue = strdup("ERROR");
	}
	pszStartTemp+=1;

	debug_sprintf(szDbgMsg, "%s:Parsed Action Code", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Batch Number
	memset(szTemp, 0x00, sizeof(szTemp));
	strncpy(szTemp, pszStartTemp ,6);
	strcpy(pstCPRespDetails[CP_BATCH_NUMBER].elementValue, szTemp);
	pszStartTemp+=6;

	debug_sprintf(szDbgMsg, "%s:Parsed Batch Number", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//parsing Retrieval Reference Number
	memset(szTemp, 0x00, sizeof(szTemp));
	strncpy(szTemp, pszStartTemp, 8);
	strcpy(pstCPRespDetails[CP_RET_REF_NUM].elementValue, szTemp);
	strcpy(gstEMVParamDwnld.szLastRetRef,szTemp);
	pszStartTemp+=8;

	debug_sprintf(szDbgMsg, "%s:Parsed Ret Reference Number", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Sequence Number
	memset(szTemp, 0x00, sizeof(szTemp));
	strncpy(szTemp, pszStartTemp, 6);
	strcpy(pstCPRespDetails[CP_SEQ_NUM].elementValue, szTemp);
	pstDHIReqResFields[eTRANS_SEQ_NUM].elementValue = strdup(pstCPRespDetails[CP_SEQ_NUM].elementValue);
	pstDHIReqResFields[eCTROUTD].elementValue = strdup(pstCPRespDetails[CP_SEQ_NUM].elementValue);
	pszStartTemp += 6;

	debug_sprintf(szDbgMsg, "%s:Parsed Sequence Number", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	//Response Message    //check here
	memset(szTemp, 0x00, sizeof(szTemp));
	strncpy(szTemp, pszStartTemp, 32);
	strcpy(pstCPRespDetails[CP_RESP_MSG].elementValue, szTemp);
	pstDHIReqResFields[eRESPONSE_TEXT].elementValue = strdup(pstCPRespDetails[CP_RESP_MSG].elementValue);
	pszStartTemp += 32;

	debug_sprintf(szDbgMsg, "%s:Parsed Response Message", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	while((pszFSTemp = strchr(pszFSTemp, FS)) != NULL)
	{
		pszFSTemp++;
		iCnt ++;

		//TODO - This is not very clean
		if(iCnt == 12)
		{
			debug_sprintf(szDbgMsg, "%s:Encounter 12 field Seperator", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = parseEMVParamDwldToken(&pszFSTemp);
			break;
		}
	}


	strncpy(pstCPRespDetails[CP_R_LRC].elementValue, pszFSTemp, 1);
	debug_sprintf(szDbgMsg, "%s:LRC Value:%s", __FUNCTION__, pstCPRespDetails[CP_R_LRC].elementValue);
	CPHI_LIB_TRACE(szDbgMsg);

#if 0
//	pszStartTemp += 1;
//	if(*pszStartTemp == ETX)
//	{
//		//End of Response
//		pszStartTemp += 1;
//	}

	//LRC Calculation
	memset(szlrcValue, 0x00, sizeof(szlrcValue));

	pszCPResp = pszCPResp + 1;
	pszEndTemp = strchr(pszCPResp, ETX);
	pszEndTemp += 1;
	iDiffTemp = pszEndTemp - pszCPResp;
	pszCPResp = strndup(pszCPResp, iDiffTemp);


	sprintf(szlrcValue,"%c", findLrc(pszCPResp, strlen(pszCPResp)-1));

	debug_sprintf(szDbgMsg, "%s: Calculated LRC value:[%s]", __FUNCTION__, szlrcValue);
	CPHI_LIB_TRACE(szDbgMsg);

	if(strcmp(pstCPRespDetails[CP_R_LRC].elementValue, szlrcValue) == 0)
	{
		debug_sprintf(szDbgMsg, "%s:Calculated LRC Value is matched with Response LRC Value", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Calculated LRC Value is not matched with Response LRC Value", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Calculated LRC Value is not matched with Response LRC Value");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

	}
#endif
	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsed CPHI Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}
