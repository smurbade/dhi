
/*
 * cphiConfigParam.c
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <svc.h>
#include <errno.h>

//#include <iniparser.h>

#include "CPHI/cphiHostCfg.h"
#include "CPHI/cphicommon.h"
#include "CPHI/cphiCfg.h"
#include "CPHI/utils.h"
#include "CPHI/cphiGroups.h"

#include "DHI_App/appLog.h"
#include "DHI_App/tranDef.h"


static int 	getCPHostDefinition();
static int 	getCPValues();
static int  getCPParamsFromFile();
static int  setDataEntrySrc();
static int  setTxnCodeValue();
static int 	getP2tokenDtls(char*, char*);
static int  getUsernamenPassword(char*, char*);


static CPHI_CFG_STYPE cpSettings;

static signed char szDataEntrySrc[3][7];
static signed char szTransactionCodes[MAX_PAYMENT_TYPE][MAX_COMMANDS];

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		cphiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         cphistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  cphistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	cphiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			cphiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)


/*
 * ============================================================================
 * Function Name: loadCPHIConfigParams
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.usr1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int loadCPHIConfigParams()
{
	int		iLen 		= 0;
	int		rv 			= 0;
	int		iVal		= 0;
	int		iValLen		= 0;
	char    szVal[20]	= "";
	char	szTemp[50] 	= "";
	//FILE*	fptr	 	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);
	iValLen = sizeof(szVal);

	while(1)
	{

		rv = getCPValues();
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: CP Values found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: CP Values not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		memset(szTemp,0x00,iLen);
		rv = getEnvFile(SECTION_CPHI, ROUTING_IND, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Routing Indicator",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			strncpy(cpSettings.szRoutingInd, szTemp, sizeof(cpSettings.szRoutingInd) - 1);

			debug_sprintf(szDbgMsg, "%s: Routing Indicator = [%s]", __FUNCTION__, cpSettings.szRoutingInd);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Routing Indicator not found", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_CFG_PARAMS;
			break;

		}


		/* Getting Terminal ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_CPHI, TERMINAL_NUM, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Terminal ID",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			strncpy(cpSettings.szTerminalNum, szTemp, sizeof(cpSettings.szTerminalNum) - 1);

			debug_sprintf(szDbgMsg, "%s: Terminal ID = [%s]",__FUNCTION__, cpSettings.szTerminalNum);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Terminal ID not found", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_CFG_PARAMS;
			break;
		}


		/* Getting Merchant ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_CPHI, MERCHANT_NUM, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Merchant ID",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			strncpy(cpSettings.szMerchantNum, szTemp, sizeof(cpSettings.szMerchantNum) - 1);

			debug_sprintf(szDbgMsg, "%s: Merchant ID = [%s]",__FUNCTION__, cpSettings.szMerchantNum);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Merchant ID not found", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_CFG_PARAMS;
			break;
		}

		/* Getting Lane ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_CPHI, LANE_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Lane ID",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			strncpy(cpSettings.szLaneId, szTemp, sizeof(cpSettings.szLaneId) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Lane ID is not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Client Number */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_CPHI, CLIENT_NUM, szTemp, iLen);
		if(rv > 0)
		{
			/*Value Found*/
			debug_sprintf(szDbgMsg, "%s: Setting Client Number",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			strncpy(cpSettings.szClientNum,szTemp,sizeof(cpSettings.szClientNum)-1);

			debug_sprintf(szDbgMsg, "%s: Client Number = [%s]",__FUNCTION__, cpSettings.szClientNum);
			CPHI_LIB_TRACE(szDbgMsg);

		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : Client ID is not found", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_CFG_PARAMS;
			break;
		}

		//		/* Getting username */
		//		memset(szTemp, 0x00, iLen);
		//		rv = getEnvFile(SECTION_CPHI, USER_NAME, szTemp, iLen);
		//		if(rv > 0)
		//		{
		//			/*Value Found*/
		//			debug_sprintf(szDbgMsg, "%s: Setting Username",__FUNCTION__);
		//			CPHI_LIB_TRACE(szDbgMsg);
		//
		//			strncpy(cpSettings.szUsername,szTemp,sizeof(cpSettings.szUsername)-1);
		//
		//		}
		//		else
		//		{
		//			/*Value not found*/
		//			debug_sprintf(szDbgMsg,"%s : Username is not found",__FUNCTION__);
		//			CPHI_LIB_TRACE(szDbgMsg);
		//			break;
		//		}
		//
		//		/* Getting Password */
		//		memset(szTemp, 0x00, iLen);
		//		rv = getEnvFile(SECTION_CPHI, PASSWORD, szTemp, iLen);
		//		if(rv > 0)
		//		{
		//			/*Value Found*/
		//			debug_sprintf(szDbgMsg, "%s: Setting Password",__FUNCTION__);
		//			CPHI_LIB_TRACE(szDbgMsg);
		//
		//			strncpy(cpSettings.szPassword,szTemp,sizeof(cpSettings.szPassword)-1);
		//
		//		}
		//		else
		//		{
		//			/*Value not found*/
		//			debug_sprintf(szDbgMsg,"%s : Password is not found",__FUNCTION__);
		//			CPHI_LIB_TRACE(szDbgMsg);
		//			rv = ERR_CFG_PARAMS;
		//			break;
		//		}

		/* Getting software identifier */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_CPHI, SW_IDENTIFIER, szTemp, iLen);
		if(rv > 0)
		{
			/*Value Found*/
			debug_sprintf(szDbgMsg, "%s: Setting software identifier",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			strncpy(cpSettings.szSWIdentifier, szTemp, sizeof(cpSettings.szSWIdentifier)-1);

			debug_sprintf(szDbgMsg, "%s: Software Identifier = [%s]",__FUNCTION__,cpSettings.szSWIdentifier);
			CPHI_LIB_TRACE(szDbgMsg);

		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : software identifier is not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_CFG_PARAMS;
			break;
		}

		/* Getting software identifier */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_CPHI, HW_IDENTIFIER, szTemp, iLen);
		if(rv > 0)
		{
			/*Value Found*/
			debug_sprintf(szDbgMsg, "%s: Setting hardware identifier",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			strncpy(cpSettings.szHWIdentifier, szTemp, sizeof(cpSettings.szHWIdentifier)-1);

			debug_sprintf(szDbgMsg, "%s: HardWare Identifier = [%s]", __FUNCTION__, cpSettings.szHWIdentifier);
			CPHI_LIB_TRACE(szDbgMsg);

		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : hardware identifier is not found", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_CFG_PARAMS;
			break;
		}

		/* Getting Gift type setup by merchant */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_CPHI, THIRDPARTYGIFT_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				/*Value Found*/
				debug_sprintf(szDbgMsg, "%s:Third Party gift program Not Enabled",__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				cpSettings.iThirdpartygiftenabled = 0;
			}
			else
			{
				cpSettings.iThirdpartygiftenabled = 1;

				debug_sprintf(szDbgMsg, "%s:Third Party gift program Enabled",__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
			}
		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : Third Party gift program Not Found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			cpSettings.iThirdpartygiftenabled = 0;

		}

		/* Getting Token Mode */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_CPHI, TOKEN_MODE, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				/*Value Found*/
				debug_sprintf(szDbgMsg, "%s:Token Mode is Not Enabled",__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				cpSettings.iTokenMode = 0;
			}
			else
			{
				cpSettings.iTokenMode = 1;

				debug_sprintf(szDbgMsg, "%s:Token Mode is Enabled",__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
			}
		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : Token Mode is Not Found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			cpSettings.iTokenMode = 1;

		}

		/* EMV Enabled*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, EMVENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				/*Value Found*/
				debug_sprintf(szDbgMsg, "%s:EMV  Not Enabled",__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				cpSettings.iemvEnabled = 0;
			}
			else
			{
				cpSettings.iemvEnabled = 1;
			}
		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : Emv Enabled is not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			cpSettings.iemvEnabled = 0;

		}



		/* Contact less Enabled*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, CONTACTLESSENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				/*Value Found*/
				debug_sprintf(szDbgMsg, "%s: Setting Contact less  Not Enabled",__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				cpSettings.iContactessEnabled = 0;
			}
			else
			{
				cpSettings.iContactessEnabled = 1;
			}
		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : Contact less Enabled is not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			cpSettings.iContactessEnabled = 0;
		}

		/* vsp Enabled*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, VSPENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				/*Value Found*/
				debug_sprintf(szDbgMsg, "%s: vsp  Not Enabled",__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				cpSettings.ivspenabled = 0;
			}
			else
			{
				cpSettings.ivspenabled = 1;
			}
		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : vsp Enabled is not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			cpSettings.ivspenabled = 1;
		}

		/* Get CPHI host urls and other communication parameters */
		rv = getCPHostDefinition();
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in getting the cp host definition",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Got cp host definition",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

		}

		/* By Default the Number of Hosts available will be 2. But incase
		 * both the URL's are the same, then we set iNumUniqueHosts = 1.
		 * During Posting the data, if both URL's are same then we will
		 * avoid unnecessarily posting to same URL Twice.
		 */
		cpSettings.iNumUniqueHosts = 2;
		if(cpSettings.hostDef[0].url && cpSettings.hostDef[1].url )
		{
			if(!strcmp(cpSettings.hostDef[0].url,cpSettings.hostDef[1].url))
			{
				debug_sprintf(szDbgMsg, "%s: Both URL's match %s , So 1 Unique Host",__FUNCTION__,cpSettings.hostDef[0].url);
				CPHI_LIB_TRACE(szDbgMsg);
				cpSettings.iNumUniqueHosts = 1;
			}
		}
		debug_sprintf(szDbgMsg, "%s: cp Num Hosts:%d",__FUNCTION__,cpSettings.iNumUniqueHosts);
		CPHI_LIB_TRACE(szDbgMsg);

		/* ------------- Check for keep Alive settings ------------------- */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_HDT, KEEP_ALIVE, szTemp, iLen);
		if(rv > 0)
		{
			iVal = atoi(szTemp);
			cpSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
			cpSettings.iKeepAliveInt = 0;

			if((iVal >= KEEP_ALIVE_DISABLED) && (iVal <= KEEP_ALIVE_DEFINED_TIME))
			{
				cpSettings.iKeepAlive = iVal;
			}
			else
			{
				/* keep Alive parameter value not in range*/
				debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive parameter, Setting Default - Disabled", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				cpSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
				memset(szVal, 0x00, iValLen);
				sprintf(szVal, "%d", KEEP_ALIVE_DISABLED);
				putEnvFile(SECTION_HDT, KEEP_ALIVE, szVal);
				rv = SUCCESS;
			}

			if(cpSettings.iKeepAlive == 2)
			{
				memset(szTemp, 0x00, iLen);
				rv = getEnvFile(SECTION_HDT, KEEP_ALIVE_INTERVAL, szTemp, iLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal >= 1) && (iVal <= 60))
					{
						cpSettings.iKeepAliveInt = iVal;
					}
					else
					{
						/* No value found for keep Alive Interval parameter */
						debug_sprintf(szDbgMsg,"%s: Value found for Keep Alive Interval parameter is not in range ;Setting to Default", __FUNCTION__);
						CPHI_LIB_TRACE(szDbgMsg);

						cpSettings.iKeepAliveInt = DFLT_KEEP_ALIVE_INTERVAL;
						memset(szVal, 0x00, iValLen);
						sprintf(szVal, "%d", DFLT_KEEP_ALIVE_INTERVAL);
						putEnvFile(SECTION_HDT, KEEP_ALIVE_INTERVAL, szVal);
					}
				}
				else
				{
					/* No value found for keep Alive Interval parameter */
					debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive Interval parameter, Setting to Default Value", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					cpSettings.iKeepAliveInt = DFLT_KEEP_ALIVE_INTERVAL;
					memset(szVal, 0x00, iValLen);
					sprintf(szVal, "%d", DFLT_KEEP_ALIVE_INTERVAL);
					putEnvFile(SECTION_HDT, KEEP_ALIVE_INTERVAL, szVal);
				}
			}
			rv = SUCCESS;
		}
		else
		{
			/* No value found for keep Alive parameter */
			debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive parameter, Setting to Default - Disabled", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			cpSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
			memset(szVal, 0x00, iValLen);
			sprintf(szVal, "%d", KEEP_ALIVE_DISABLED);
			putEnvFile(SECTION_HDT, KEEP_ALIVE, szVal);
			rv = SUCCESS;
		}

		/*Getting P2 token  Information */
		rv = getP2tokenDtls(cpSettings.szP2TokenDetailsEncrypted, cpSettings.szP2TokenDetailsNonEncrypted);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully got the p2 token details",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get P2 token Details",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*Getting Username and password  Information */
		rv = getUsernamenPassword(cpSettings.szUsername, cpSettings.szPassword);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully got the Username and Password details for CPHI",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get username and password Details for CPHI",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* setting dataEntrysource value */
		rv = setDataEntrySrc();
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Data Entry Source Values are Successfully set ",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to set Data Entry source values",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* setting Transaction Code value */
		rv = setTxnCodeValue();
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Transaction code Values are Successfully set ",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to set Transaction Code Values",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}


		break;

	}

	debug_sprintf(szDbgMsg, "%s: returning [%d]",__FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);
	return rv;
}




/*
* ============================================================================
* Function Name: getTerminalNUM
*
*
* Description	: This function will fill the iTerminalNUM buffer
*
*
* Input Params	: TerminalNUM Buffer to be filled
*
*
* Output Params: None
* ============================================================================
*/

void getTerminalNum(char *pszTerminalNum)
{
	int 	iLen		=	0;
	char 	szTemp[3+1] = 	"";

	memset(szTemp, 0x00, sizeof(szTemp));

	iLen = strlen(cpSettings.szTerminalNum);
	memset(szTemp, '0', 3-iLen);
	strcat(szTemp, cpSettings.szTerminalNum);
	strcpy(pszTerminalNum, szTemp);
}


/*
* ============================================================================
* Function Name: getRoutingIndicator
*
*
* Description	: This function will fill the iRoutingind buffer
*
*
* Input Params	: TerminalID Buffer to be filled
*
*
* Output Params: None
* ============================================================================
*/

void getRoutingInd(char *pszRoutingInd)
{
	strcpy(pszRoutingInd, cpSettings.szRoutingInd);
}

/*
 * ============================================================================
 * Function Name: getMerchantNUM
 *
 *
 * Description	: This function will fill the iMerchantNUM buffer
 *
 *
 * Input Params	: MerchantNUM Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getMerchantNum(char *pszMerchantNum)
{
	int 	iLen		=	0;
	char 	szTemp[12+1] = 	"";

	memset(szTemp, 0x00, sizeof(szTemp));

	iLen = strlen(cpSettings.szMerchantNum);
	memset(szTemp, '0', 12-iLen);
	strcat(szTemp, cpSettings.szMerchantNum);
	strcpy(pszMerchantNum, szTemp);

}

/*
 * ============================================================================
 * Function Name: getLaneID
 *
 *
 * Description	: This function will fill the LaneID buffer
 *
 *
 * Input Params	: LaneID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getConfigLaneId(char *pszLaneID)
{
	strcpy(pszLaneID, cpSettings.szLaneId);
}
/*
 * ============================================================================
 * Function Name: getClientNumber
 *
 *
 * Description	: This function will fill the iClientNum buffer
 *
 *
 * Input Params	: ClientNum Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getClientNum(char *pszClientNum)
{
	int 	iLen		=	0;
	char 	szTemp[4+1] = 	"";

	memset(szTemp, 0x00, sizeof(szTemp));

	iLen = strlen(cpSettings.szClientNum);
	memset(szTemp, '0', 4-iLen);
	strcat(szTemp, cpSettings.szClientNum);
	strcpy(pszClientNum, szTemp);
}

/*
 * ============================================================================
 * Function Name: getUserName
 *
 *
 * Description	: This function will fill the UserName buffer
 *
 *
 * Input Params	: UserName Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getUserName(char *pszUserName)
{
	char sztemp[20]= "";
	int maxLen  = 0;
	maxLen = sizeof(sztemp);
	memset(sztemp,0x00,maxLen);

	strcpy(sztemp,"Auth-User:");
	strcat(sztemp,cpSettings.szUsername);
	strcpy(pszUserName,sztemp);

}

/*
 * ============================================================================
 * Function Name: getPassword
 *
 *
 * Description	: This function will fill the Password buffer
 *
 *
 * Input Params	: Password Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getPassword(char *pszPassword)
{
	char sztemp[100]= "";
	int maxLen  = 0;
	maxLen = sizeof(sztemp);
	memset(sztemp,0x00,maxLen);

	strcpy(sztemp,"Auth-Password:");
	strcat(sztemp,cpSettings.szPassword);
	strcpy(pszPassword,sztemp);
}


/*
 * ============================================================================
 * Function Name: getSWIdentifier
 *
 *
 * Description	: This function will fill the pszSWIdentifier
 *
 *
 * Input Params	: pszSWIdentifier Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getSWIdentifier(char *pszSWIdentifier)
{
	strcpy(pszSWIdentifier, cpSettings.szSWIdentifier);
}

/*
 * ============================================================================
 * Function Name: getHWIdentifier
 *
 *
 * Description	: This function will fill the pszHWIdentifier
 *
 *
 * Input Params	: pszHWIdentifier Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getHWIdentifier(char *pszHWIdentifier)
{
	strcpy(pszHWIdentifier, cpSettings.szHWIdentifier);
}

/*
 * ============================================================================
 * Function Name: getSystemInd
 *
 *
 * Description	: This function will fill the szSystemInd buffer
 *
 *
 * Input Params	: SystemInd Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getSystemInd(char *szSystemInd)
{
	strcpy(szSystemInd, SYSTEM_IND);
}



/*
 * ============================================================================
 * Function Name: getTxnClass
 *
 *
 * Description	: This function will fill the szTxnClass buffer
 *
 *
 * Input Params	: szTxnClass Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTxnClass(char *pszTxnClass)
{
	strcpy(pszTxnClass,TXN_CLASS);
}


/*
 * ============================================================================
 * Function Name: getTxnSqnFlag
 *
 *
 * Description	: This function will fill the szTxnSqnFlag buffer
 *
 *
 * Input Params	: szTxnSqnFlag Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTxnSqnFlag(char *pszTxnSqnFlag)
{
	strcpy(pszTxnSqnFlag, TXN_SQN_FLAG);
}

/*
 * ============================================================================
 * Function Name: getPinCapabCode
 *
 *
 * Description	: This function will fill the iPinCapabCode buffer
 *
 *
 * Input Params	: iPinCapabCode Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getPinCapabCode(char *pszPinCapabCode)
{
	strcpy(pszPinCapabCode, PIN_CAPBLTY_CODE);
}

/*
 * ============================================================================
 * Function Name: getTransInfoFiller
 *
 *
 * Description	: This function will fill the iTransInfoFiller buffer
 *
 *
 * Input Params	: iTransInfoFiller Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTransInfoFiller(char *pszTransInfoFiller, char * pszfieldseparator)
{
	strcpy(pszTransInfoFiller, TXN_FILLER);
	sprintf(pszfieldseparator, "%c",FS);
}


/*
 * ============================================================================
 * Function Name: getIndusCode
 *
 *
 * Description	: This function will fill the iIndusCode buffer
 *
 *
 * Input Params	: iIndusCode Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getIndusCode(char *pszIndusCode, char *pszPaymentType)
{
	if(strcmp(pszPaymentType, "GIFT") == 0)
	{
		strcpy(pszIndusCode, SV_INDUSTRY_CODE);
	}
	else
	{
		strcpy(pszIndusCode, INDUSTRY_CODE);
	}

}

/*
 * ============================================================================
 * Function Name: getItemCode
 *
 *
 * Description	: This function will fill the ItemCode buffer
 *
 *
 * Input Params	: ItemCode Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getItemCode(char *pszItemCode)
{
	strcpy(pszItemCode, ITEM_CODE);
}


/*
 * ============================================================================
 * Function Name: getExternal Transaction Identifier
 *
 *
 * Description	: This function will fill the External Transaction Identifier buffer
 *
 *
 * Input Params	: External Transaction Identifier Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getExtTransId(char *pszExtTransId ,char *pszLaneNum)
{
	int 	iTemp 				 	= 0;
	char    szTemp[15+1]		 	= "";
	char	szLaneTemp[5+1]			= "";

	if(pszLaneNum != NULL)
	{
		//Lane id from POS
		iTemp = strlen(pszLaneNum);

		memset(szTemp, '0', 15-iTemp);
		strcat(szTemp, pszLaneNum);

		strcpy(pszExtTransId, szTemp);
	}
	else if (cpSettings.szLaneId != NULL)
	{
		//Land id From Config parameters
		getConfigLaneId(szLaneTemp);
		iTemp = strlen(szLaneTemp);
		memset(szTemp, '0', 15-iTemp);
		strcat(szTemp, szLaneTemp);

		strcpy(pszExtTransId, szTemp);
	}
	else
	{
		strcpy(pszExtTransId, EXT_TRANS_ID);
	}
}

/*
 * ============================================================================
 * Function Name: getEmployee Number
 *
 *
 * Description	: This function will fill the Employee Number buffer
 *
 *
 * Input Params	: Employee Number Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

int getEmployeeNum(char *pszEmployeeNum , char *pszEmpNum)
{
	int   	 	rv					 	= SUCCESS;
	int 		iLen 					= 0	;
	char  		szTemp[10+1] 			= "";

	if(pszEmpNum != NULL)
	{
		iLen = strlen(pszEmpNum);
		strcpy(szTemp, pszEmpNum);

		memset(szTemp + iLen , ' ', sizeof(szTemp)-iLen-1);
		strcpy(pszEmployeeNum, szTemp);
		return rv;
	}
	else
	{
		rv = ERR_FLD_REQD;
		return rv;
	}
}

/*
 * ============================================================================
 * Function Name: getCash out Indicator
 *
 *
 * Description	: This function will fill the Cash out Indicator buffer
 *
 *
 * Input Params	: Cash out Indicator Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getCashOutInd(char *pszCashOutInd)
{
	strcpy(pszCashOutInd, "N");
}

/*
 * ============================================================================
 * Function Name: getSeqNumCards
 *
 *
 * Description	: This function will fill the SeqNumCards buffer
 *
 *
 * Input Params	: SeqNumCards Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getSeqNumCards(char *pszSeqNumCards ,char *pszCommand)
{
	if( (strcmp(pszCommand,"ACTIVATE") == 0 ) || (strcmp(pszCommand,"ADD_VALUE") == 0 ) )
	{
		strcpy(pszSeqNumCards, SEQ_NUM_CARDS);
	}
	else
	{
		strcpy(pszSeqNumCards, "00");
	}
}

/*
 * ============================================================================
 * Function Name: getTotalNumCards
 *
 *
 * Description	: This function will fill the TotalNumCards buffer
 *
 *
 * Input Params	: TotalNumCards Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */


void getTotalNumCards(char* pszTotalNumCards, char* pszCommand)
{
 if( (strcmp(pszCommand,"ACTIVATE") == 0 ) || ( strcmp(pszCommand,"ADD_VALUE") == 0 ) )
 	{
 		strcpy(pszTotalNumCards, TOTAL_NUM_CARDS);
 	}
 	else
 	{
 		strcpy(pszTotalNumCards,"00");
 	}
}

/*
 * ============================================================================
 * Function Name: getPosCapblty1Dtls
 *
 *
 * Description	: This function will fill the pos vaiable capabilities 1
 *
 *
 * Input Params	: ItemCode Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getPosCapblty1Dtls(char *pszDHIserialNum, char *pszPosCapbleDtls1)
{
	char    szTemp[20+1]  			 = "";
	char	szHardId[4+1]			 = "";
	char    szSoftId[4+1]			 = "";

	/*getting software identifier*/
	getSWIdentifier(szSoftId);

	/*getting hardware identifier*/
	getHWIdentifier(szHardId);

	memset(szTemp ,' ', sizeof(szTemp)-1);

	if(pszDHIserialNum == NULL)
	{
		CPHI_LIB_TRACE("Failed in getPosCapbltyDtls");
		strcpy(szTemp, "123456");
	}
	else
	{
		strncpy(szTemp, pszDHIserialNum,9);				// Copying 9 bytes of serial number
	}

	strcpy(pszPosCapbleDtls1, "P1");
	strcat(pszPosCapbleDtls1, szHardId);
	strcat(pszPosCapbleDtls1, szSoftId);
	strcat(pszPosCapbleDtls1, szTemp);

}

/*
 * ============================================================================
 * Function Name: getPosCapblty2Dtls
 *
 *
 * Description	: This function will fill the pos vaiable capabilities 1
 *
 *
 * Input Params	: ItemCode Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getPosCapblty2Dtls(char * pszPosCapbleDtls2, TRANDTLS_PTYPE pstDHIDtls)
{
	strcpy(pszPosCapbleDtls2, "P2");

	if(strcmp(pstDHIDtls[ePAYMENT_TYPE].elementValue, "GIFT") == SUCCESS)
	{
		strcat(pszPosCapbleDtls2,cpSettings.szP2TokenDetailsNonEncrypted);
	}
	else
	{
		strcat(pszPosCapbleDtls2,cpSettings.szP2TokenDetailsEncrypted);
	}

}

/*
 * ============================================================================
 * Function Name: getKeepAliveParameter
 *
 * Description	: This function returns the keep Alive parameter value
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getKeepAliveParameter()
{
	return cpSettings.iKeepAlive;
}

/*
 * ============================================================================
 * Function Name: getKeepAliveInterval
 *
 * Description	: This function returns the keep Alive parameter value
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getKeepAliveInterval()
{
	return cpSettings.iKeepAliveInt;
}

/*
 * ============================================================================
 * Function Name: isThirdPartyGiftEnabled
 *
 * Description	: This function Tells whether Third Party Gift is enabled or not.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int  isThirdPartyGiftEnabled()
{
	return cpSettings.iThirdpartygiftenabled;
}

/*
 * ============================================================================
 * Function Name: isTokenModeEnabled
 *
 * Description	: This function Tells whether Token Mode is enabled or not.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int  isTokenModeEnabled()
{
	return cpSettings.iTokenMode;
}

/*
 * ============================================================================
 * Function Name: getCPHostDefinition
 *
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.usr1 file and store them in the static structure.
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
 int getCPHostDefinition()
{
	int		rv				= 0;
	int		len				= 0;
	int		iCnt			= 0;
	int		jCnt			= 0;
	int		iVal			= 0;
	int		maxLen			= 0;
	char *	tempPtr			= NULL;
	char	szTemp[100]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (iCnt < 2))
	{
		switch(iCnt)
		{
		case PRIMARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_CPHI, PRIM_HOST_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Primary cp Host URL = [%s]",
						__FUNCTION__, szTemp);
				CPHI_LIB_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					cpSettings.hostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing primary URL */
					debug_sprintf(szDbgMsg,
							"%s - memory allocation failed for Primary cp URL",
							__FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Primary cp URL not found",
						__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);


			}

			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the cp host port number ---- */
				rv = getEnvFile(SECTION_CPHI, PRIM_HOST_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						cpSettings.hostDef[iCnt].portNum = iVal;
						debug_sprintf(szDbgMsg, "%s: Primary host Port Number = [%d]",__FUNCTION__, iVal);
						CPHI_LIB_TRACE(szDbgMsg);

					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid prim CPHI port [%d]"
								, __FUNCTION__, iVal);
						cpSettings.hostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Primary host port not found",
							__FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					cpSettings.hostDef[iCnt].portNum = 0;
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Connection Timeout ---- */
				rv = getEnvFile(SECTION_CPHI, PRIM_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					cpSettings.hostDef[iCnt].conTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Primary Conn TimeOut = [%d]",
							__FUNCTION__, iVal);
					CPHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
							"%s: Primary conn timeout not found, setting default",
							__FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					cpSettings.hostDef[iCnt].conTimeOut =
							DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_CPHI, PRIM_CONN_TO, szTemp);

				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_CPHI, PRIM_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					cpSettings.hostDef[iCnt].respTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Primary Resp TimeOut = [%d]",
							__FUNCTION__, iVal);
					CPHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
							"%s: Primary resp timeout not found, setting default",
							__FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					cpSettings.hostDef[iCnt].respTimeOut =DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_CPHI, PRIM_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		case SECONDARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_CPHI, SCND_HOST_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Secondary cp Host URL = [%s]",
						__FUNCTION__, szTemp);
				CPHI_LIB_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					cpSettings.hostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing secondary URL */
					debug_sprintf(szDbgMsg,
							"%s - memory allocation failed for Secondary PWC URL",
							__FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Secondary PWC URL not found",
						__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

			}


			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the PWC host port number ---- */
				rv = getEnvFile(SECTION_CPHI, SCND_HOST_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						cpSettings.hostDef[iCnt].portNum = iVal;
						debug_sprintf(szDbgMsg, "%s: Secondary host port number = [%d]",
													__FUNCTION__, iVal);
											CPHI_LIB_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid scnd cp port [%d]"
								, __FUNCTION__, iVal);
						cpSettings.hostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Secondary host port not found",
							__FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					cpSettings.hostDef[iCnt].portNum = 0;
				}

				/* ---- Get Connection Timeout ---- */
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				rv = getEnvFile(SECTION_CPHI, SCND_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					cpSettings.hostDef[iCnt].conTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Secondary Conn TimeOut = [%d]",
							__FUNCTION__, iVal);
					CPHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
							"%s - Secndary conn timeout not found, setting default",
							__FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					cpSettings.hostDef[iCnt].conTimeOut =
							DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_CPHI, SCND_CONN_TO, szTemp);
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_CPHI, SCND_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					cpSettings.hostDef[iCnt].respTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Secondary Resp TimeOut = [%d]",
							__FUNCTION__, iVal);
					CPHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
							"%s - Secondary resp timeout not found, setting default",
							__FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					cpSettings.hostDef[iCnt].respTimeOut =
							DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_CPHI, SCND_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		default:
			break;
		}

		iCnt++;
	}

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: cp Host definitions load failed",
				__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		/* Free any allocated resources */
		for(jCnt = 0; jCnt <= iCnt; jCnt++)
		{
			free(cpSettings.hostDef[jCnt].url);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getcpHostDetails
 *
 * Description	: This function fills up the cp Host Details for a connection to
 * 				  be setup
 *
 * Input Params	:	HOST_URL_ENUM
 * 					HOSTDEF_PTR_TYPE
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCPHostDetails(HOST_URL_ENUM hostType, HOSTDEF_PTR_TYPE hostDefPtr)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(hostDefPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No place holder provided for host details",
				__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		memset(hostDefPtr, 0x00, sizeof(HOSTDEF_STRUCT_TYPE));

		switch(hostType)
		{
		case PRIMARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting PRIMARY cp Host details",
					__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(cpSettings.hostDef[PRIMARY_URL]), sizeof(HOSTDEF_STRUCT_TYPE));
			break;

		case SECONDARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting SECONDARY cp Host details",
					__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(cpSettings.hostDef[SECONDARY_URL]), sizeof(HOSTDEF_STRUCT_TYPE));

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Incorrect Host Type given",
					__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNumHosts
 *
 *
 * Description	: This function will return number of unique hosts
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */

int getNumUniqueHosts()
{
	return cpSettings.iNumUniqueHosts;
}





/*
 * ============================================================================
 * Function Name: getSequenceNum
 *
 * Description	:
 *
 * Input Params	: Current sequence number to be filled Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getSequencenum(char *pszCurrentSeqNum)
{

	sprintf(pszCurrentSeqNum,"%d", ++cpSettings.iSequenceNum);
}


/*
 * ============================================================================
 * Function Name: getCurrentSequenceNum
 *
 * Description	: This function will gives the current Sequence Number
 *
 *
 * Input Params	: current Sequence Number Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getCurrentSequenceNum(char *szCurrentSequenceNum)
{
	sprintf(szCurrentSequenceNum, "%06d", cpSettings.iSequenceNum);
}

/*
 * ============================================================================
 * Function Name: getCurrentTknRefNum
 *
 * Description	: This function will gives the current token Ref Num
 *
 *
 * Input Params	: current token Ref Num Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTokenRefNum(char *szCurrentTknRefNum)
{
	sprintf(szCurrentTknRefNum, "%d", ++cpSettings.iTokenRefNum);
}

/*
 * ============================================================================
 * Function Name: getCurrentCustDefData
 *
 * Description	: This function will gives the getCurrentCustDefData
 *
 *
 * Input Params	: getCurrentCustDefData Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getCusDefData(char *szCustDefData)
{
	sprintf(szCustDefData, "%d", ++cpSettings.iCusDefData);
}

/* ============================================================================
 * Function Name: getRefNum
 *
 * Description	:
 *
 *
 * Input Params	: Current reference number to Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getCurrentTknRefNum(char *pszCurrentRefNum)
{
	char	szTemp[12+1]	= "";
	char    szTempVal[12+1]	= "";

	memset(szTemp, ' ', sizeof(szTemp)-1);
	sprintf(szTempVal, "%d", cpSettings.iTokenRefNum);
	strncpy(szTemp, szTempVal, strlen(szTempVal));

	strcpy(pszCurrentRefNum,"RN");
	strcat(pszCurrentRefNum,szTemp);
}

/*
 * ============================================================================
 * Function Name: getCusDefData
 *
 * Description	:
 *
 *
 * Input Params	: Current customer defined data to be filled Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getCurrentCustDefData(TRANDTLS_PTYPE pstDHIDtls, char *pszCusDefData)
{

	if((pstDHIDtls[eUSER_DEFINED1].elementValue != NULL) && (strlen(pstDHIDtls[eUSER_DEFINED1].elementValue) > 0))
	{
		sprintf(pszCusDefData, "CD%s", pstDHIDtls[eUSER_DEFINED1].elementValue);
	}
	else
	{
		sprintf(pszCusDefData, "CD%d",cpSettings.iCusDefData);
	}
}

/*
 * ============================================================================
 * Function Name: putLastRefNum
 *
 * Description	: it will update the last reference number
 *
 *
 * Input Params	: buffer containing the last reference number
 *
 *
 * Output Params: None
 * ============================================================================
 */
void putLastRefNum(char *pszLRR)
{
	cpSettings.iLastRefNum = atoi(pszLRR);
}

/*
 * ============================================================================
 * Function Name: getLastRefNum
 *
 * Description	: it will give the last reference number
 *
 *
 * Input Params	: buffer containing the last reference number
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getLastRefNum(char *pszLRR)
{
	sprintf(pszLRR, "%08d", cpSettings.iLastRefNum);
}


/*
 * ============================================================================
 * Function Name: getCPValues
 *
 *
 * Description	: This function will get the cp params
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCPValues()
{
	int 					rv 							= SUCCESS;
	char					szTemp[50] 			    	= "";
	char					szSeqRefCustData[53+1]   	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/* Step 1: Getting values from the file cpparams.txt */
	rv = doesFileExist(CP_PARAMS_FILE);
	if(rv == SUCCESS)
	{
		rv = getCPParamsFromFile();
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully read from the file", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Unable to read from the file", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}
	else
	{
		/* Setting Values */

		/* Setting Sequnce Number */
		cpSettings.iSequenceNum = 1;
		sprintf(szTemp,"%d", cpSettings.iSequenceNum);
		strcpy(szSeqRefCustData, szTemp);

		/* Setting Token Ref Number */
		cpSettings.iTokenRefNum = 1;
		sprintf(szTemp,"%d", cpSettings.iTokenRefNum);
		strcat(szSeqRefCustData,"|");
		strcat(szSeqRefCustData, szTemp);

		/* Setting Customer Defined Data */
		cpSettings.iCusDefData = 1;
		sprintf(szTemp,"%d", cpSettings.iCusDefData);
		strcat(szSeqRefCustData,"|");
		strcat(szSeqRefCustData, szTemp);

		/* Setting Last retrival Reference Number */
		sprintf(szTemp,"%d", cpSettings.iLastRefNum);
		strcat(szSeqRefCustData,"|");
		strcat(szSeqRefCustData, szTemp);
		strcat(szSeqRefCustData,"|");

		rv = writeCPParamsToFile(szSeqRefCustData);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully written",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s - Write unsuccessful in file [%s]!",
					__FUNCTION__, CP_PARAMS_FILE);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning %d---", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCPParamsFromFile
 *
 *
 * Description	: This function will get the details from the file
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCPParamsFromFile()
{
	int						rv						= SUCCESS;
	char*					pszTempSeqenceNum		= NULL;
	char*					pszTempTknRefNum		= NULL;
	char*					pszTempCustDefData   	= NULL;
	char*					pszTempLastRefNum   	= NULL;
	char 					szCPParams[53+1]		= "";
	FILE*					fptr					= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);


	memset(szCPParams,0x00,sizeof(szCPParams));
	fptr = fopen(CP_PARAMS_FILE, "r");
	if(fptr != NULL)
	{
		fgets(szCPParams, sizeof(szCPParams), fptr);
		debug_sprintf(szDbgMsg, "%s: szCPParams is :%s",__FUNCTION__, szCPParams);
		CPHI_LIB_TRACE(szDbgMsg);
		fclose(fptr);

		pszTempSeqenceNum	 = 	strtok(szCPParams,"|");
		pszTempTknRefNum 	 = 	strtok(NULL,"|");
		pszTempCustDefData 	 = 	strtok(NULL,"|");
		pszTempLastRefNum 	 = 	strtok(NULL,"|");

		/* Value found*/

		debug_sprintf(szDbgMsg, "%s: Checking for SequenceNum, TknRefNum and  Customer Defined Data and last reference Number", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		/* Check for the value of SequenceNum */
		if(pszTempSeqenceNum != NULL)
		{
			/* Value found ... Setting SequenceNum accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting SequenceNumber",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			cpSettings.iSequenceNum = atoi(pszTempSeqenceNum);
			if(cpSettings.iSequenceNum <= 0)
			{
				cpSettings.iSequenceNum = 1;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SequenceNumber not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}

		/* Checking for Token Reference Number */

		if(pszTempTknRefNum != NULL)
		{
			/* Value found ... Setting Token Reference Number accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting Token Reference Number",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			cpSettings.iTokenRefNum = atoi(pszTempTknRefNum);
			if(cpSettings.iTokenRefNum <= 0)
			{
				cpSettings.iTokenRefNum = 1;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:Token Reference Number not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}

		/* Checking for Customer Defined Data */

		if(pszTempCustDefData != NULL)
		{
			/* Value found ... Setting Customer Defined Data accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting Customer Defined Data",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			cpSettings.iCusDefData = atoi(pszTempCustDefData);
			if(cpSettings.iCusDefData <= 0)
			{
				cpSettings.iCusDefData = 1;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Customer Defined Data not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}

		/* Checking for last reference number */

		if(pszTempLastRefNum != NULL)
		{
			/* Value found ... Setting last reference number accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting last reference number",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			cpSettings.iLastRefNum = atoi(pszTempLastRefNum);
			if(cpSettings.iLastRefNum < 0)
			{
				cpSettings.iLastRefNum = 0;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: last reference number not found",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - fopen error; file [%s] could not be opened!",
				__FUNCTION__, CP_PARAMS_FILE);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: returning", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: writeCPParamsToFile
 *
 *
 * Description	: This function will fill details in to the file
 *
 *
 * Input Params	: string to be filled in to file
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int writeCPParamsToFile(char *pszSeqRefCustData)
{
	int		rv 		= SUCCESS;
	FILE*	fptr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	fptr = fopen(CP_PARAMS_FILE, "w");
	if(fptr != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Writing into the file",__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		/* Writing the parameters in the file in format: Sequence Number|Token Ref.Num|Customer Def.Data */
		fwrite(pszSeqRefCustData, strlen(pszSeqRefCustData) + 1, 1, fptr);
		fclose(fptr);
		rv = SUCCESS;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - fopen error; file [%s] could not be opened!",
				__FUNCTION__, CP_PARAMS_FILE);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: resetBatchParams
 *
 * Description	: This function resets all the close batch related params
 *				  Resets the refnum, clientrefnum, delete the saved records
 * Input Params	:
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void	resetBatchParams()
{
	FILE*		fptr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	cpSettings.iCusDefData			= 1;
	cpSettings.iSequenceNum			= 1;
	cpSettings.iTokenRefNum			= 1;
	cpSettings.iLastRefNum			= 0;


	if(doesFileExist(CP_PARAMS_FILE) == SUCCESS)
	{
		fptr = fopen(CP_PARAMS_FILE, "w");
		if(fptr != NULL)
		{
			fclose(fptr);
		}
	}

	if(doesFileExist(CP_TRAN_RECORDS) == SUCCESS)
		{
			fptr = fopen(CP_TRAN_RECORDS, "w");
			if(fptr != NULL)
			{
				fclose(fptr);
			}
		}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getP2tokenDtls
 *
 *
 * Description	: This function will get the P2 Token details
 *
 *
 * Input Params	: char buffer to be filled with p2 token details
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getP2tokenDtls(char* szTokenP2DtlsEncrptd, char* szTokenP2DtlsNonEncrptd)
{
	extern	int					errno;
	int				rv								= SUCCESS;
	int 			iValue							= 0;
	int 			iCnt							= 0;
	int 			iLen							= 0;
	int				iEnc							= 1;
	int 			iMaxAIDs						= 6;
	int				iAppLogEnabled					= 0;
	char 			szBitmap[64+1]					= "";
	char			szKey[100]						= "";
	char			szVal[100]						= "";
	char            szDataField[64+1]				= "";
	char			szFinalP2Bin[128+1]				= "";
	char            szTemp[8+1]						= "";
	char			szTempHex[2+1]					= "";
	dictionary *	dictEMVTables					= NULL;
	char			szAppLogData[256]				= "";
	char			szAppLogDiag[256]				= "";
	char* 			szvalue							= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	iAppLogEnabled = isAppLogEnabled();


	while(iEnc < 3)
	{
		memset(szBitmap, '0', sizeof(szBitmap)-1);
		memset(szDataField, 0x00, sizeof(szDataField));
		memset(szFinalP2Bin, 0x00, sizeof(szFinalP2Bin));


		/*CREATING BIT MAP*/
		szBitmap[1] = '1';				// indicating host platform
		szBitmap[2] = '1';  			//message format

		if(cpSettings.iemvEnabled == 1)
		{
			szBitmap[3] = '1'; 			 //emv Support present
		}
		else
		{
			szBitmap[3] = '0';  		//emv Support absent
		}

		szBitmap[4]  = '1'; 			 // peripheral information
		szBitmap[6]  = '1';			     //communication information
		szBitmap[8]  = '1'; 			 //industry information
		szBitmap[10] = '1'; 			 //class and certification

		if(cpSettings.ivspenabled == 1 && iEnc == 1)
		{
			szBitmap[11] = '1';  			//device security features enabled
		}
		else
		{
			szBitmap[11] = '0';  			//device security features diasbled
		}

		/*Host Processing Platform*/
		memset(szTemp, '0', sizeof(szTemp)-1);
		szTemp[4] = '1';				//HCS Auth/Settle Credit
		szTemp[5] = '1';				//Host Extended Product Support (Debit, SV, EBT)
		strcpy(szDataField, szTemp);


		/*Message Format Support 1*/
		memset(szTemp, '0', sizeof(szTemp)-1);
		szTemp[1] = '1';						//UTF Message Format
		strcat(szDataField, szTemp);


		/*EMV Support*/

		if(szBitmap[3] == '1')
		{
			memset(szTemp, '0', sizeof(szTemp)-1);

			rv = doesFileExist(EMV_TABLES_FILE_PATH);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: File %sPresent", __FUNCTION__, EMV_TABLES_FILE_PATH);
				CPHI_LIB_TRACE(szDbgMsg);

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: File %s Not Present", __FUNCTION__, EMV_TABLES_FILE_PATH);
				CPHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				return rv;
			}

			dictEMVTables = iniparser_load(EMV_TABLES_FILE_PATH);

			if(dictEMVTables == NULL)
			{
				/* Unable to load the ini file */
				debug_sprintf(szDbgMsg, "%s: iniparser_load error; file [%s] could not be opened!, errno=%d [%s]", __FUNCTION__, EMV_TABLES_FILE_PATH, errno, strerror(errno));
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error while Loading EMV Config Files");
					strcpy(szAppLogDiag, "Internal DHI-CPHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				rv = FAILURE;
				return rv;
			}

			for(iCnt = 1; iCnt <= iMaxAIDs; iCnt++)
			{

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s%d", "supportedschemes", "scheme", iCnt);

				szvalue = iniparser_getstring(dictEMVTables, szKey, NULL);
				if(szvalue != NULL)
				{
					memset(szVal, 0x00, sizeof(szVal));
					strcpy(szVal, szvalue);
					debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
					CPHI_LIB_TRACE(szDbgMsg);

					if(strcasecmp(szVal, "VISA") == 0)
					{
						szTemp[0] = '1';						//ChaseNet and Visa Credit EMV
						szTemp[1] = '1';						//Visa Debit EMV
					}
					else if(strcasecmp(szVal, "MASTERCARD") == 0)
					{
						szTemp[2] = '1';						//MasterCard Credit EMV
						szTemp[3] = '1';						//MaestroCard Credit EMV

					}
					else if(strcasecmp(szVal, "INTERAC") == 0)
					{
						szTemp[6] = '1';						//Interac EMV
					}
					else if(strcasecmp(szVal, "AMEX") == 0)
					{
						szTemp[4] = '1';						//American Express EMV
					}
					else if(strcasecmp(szVal, "JCB") == 0)
					{
						szTemp[5] = '1';						//JCB EMV
					}
					else if(strcasecmp(szVal, "DISCOVER") == 0)
					{
						szTemp[7] = '1';						//Discover EMV
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: ERROR: Got Unknown Scheme [%s] Increasing MAXAID variable by one",__FUNCTION__, szVal);
						CPHI_LIB_TRACE(szDbgMsg);
						iMaxAIDs++;
					}
				}
				else
				{
					break;
				}
			}


			strcat(szDataField, szTemp);
		}

		/*Peripheral Information 1*/
		memset(szTemp, '0', sizeof(szTemp)-1);
		szTemp[0] = '1';						//Internal PIN Pad Enabled

		if(cpSettings.iContactessEnabled == 1)
		{
			szTemp[2] = '1'; 			 //Internal Contactless Reader Enabled
		}
		else
		{
			szTemp[2] = '0';  			//Internal Contactless Reader disabled
		}

		if(szBitmap[3] == '1')
		{
			szTemp[4] = '1'; 			 // Internal Contact Chip Reader Enabled
		}
		else
		{
			szTemp[4] = '0'; 			 //Internal Contact Chip Reader disabled
		}

		strcat(szDataField, szTemp);

		/*Communication Information 1*/
		memset(szTemp, '0', sizeof(szTemp)-1);
		szTemp[3] = '1'; 				 //NetConnect
		strcat(szDataField, szTemp);

		/*Industry Information 1*/
		memset(szTemp, '0', sizeof(szTemp)-1);
		szTemp[5] = '1'; 				 //Restaurant Enabled
		szTemp[6] = '1'; 				 //Retail Enabled
		strcat(szDataField, szTemp);

		/*Class & Compliance Certification*/
		memset(szTemp, '0', sizeof(szTemp)-1);
		szTemp[1] = '1';				 // class B (Suggested by chase)
		szTemp[3] = '1'; 				 //Merchant Application
		strcat(szDataField, szTemp);

		/*Device Security Features*/
		if(szBitmap[11] == '1')
		{
			memset(szTemp, '0', sizeof(szTemp)-1);
			szTemp[0] = '1'; 				 //semtek
			strcat(szDataField, szTemp);
		}

		strcpy(szFinalP2Bin, szBitmap);
		strcat(szFinalP2Bin, szDataField);

		iLen = strlen(szFinalP2Bin);

		if(iEnc == 1)
		{
			for(iCnt = 0; iCnt < iLen; iCnt += 8)
			{
				memset(szTemp, 0x00, sizeof(szTemp)-1);
				strncpy(szTemp, &szFinalP2Bin[iCnt], 8);
				iValue = strtol(szTemp, NULL, 2);
				sprintf(szTempHex ,"%02X", iValue);
				strcat(szTokenP2DtlsEncrptd, szTempHex);
			}
		}
		else if(iEnc == 2)
		{
			for(iCnt = 0; iCnt < iLen; iCnt += 8)
			{
				memset(szTemp, 0x00, sizeof(szTemp)-1);
				strncpy(szTemp, &szFinalP2Bin[iCnt], 8);
				iValue = strtol(szTemp, NULL, 2);
				sprintf(szTempHex ,"%02X", iValue);
				strcat(szTokenP2DtlsNonEncrptd, szTempHex);
			}
		}
		else
		{
			break;
		}

		iEnc++;
	}
	debug_sprintf(szDbgMsg, "%s:P2 TOKEN FOR ENCRYPTED :[%s]", __FUNCTION__, szTokenP2DtlsEncrptd);
	CPHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s:P2 TOKEN FOR NON ENCRYPTED :[%s]", __FUNCTION__, szTokenP2DtlsNonEncrptd);
	CPHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getUsernamenPassword
 *
 *
 * Description	: This function will get the user name and password from the CPUsernameAndPassword.txt file.
 *
 *
 * Input Params	: buffer to be filled with user name,
 * 				  buffer to be filled with password.
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getUsernamenPassword(char* szUsername, char* szPassword)
{
	int 	rv 						= SUCCESS;
	char 	szTempUserName[32+1]	= "";
	char 	szTemPassword[32+1]     ="";
	char 	szTemp[48+1]			="";
	char*	pszTemp               	= NULL;
	FILE*	fp						= NULL;


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szTempUserName, 0x00, sizeof(szTempUserName));
	memset(szTemPassword, 0x00, sizeof(szTemPassword));

	/* Step 1: Getting values from the file cpparams.txt */
	rv = doesFileExist(CP_USERNAME_PASSWORD);
	if(rv == SUCCESS)
	{
		fp = fopen(CP_USERNAME_PASSWORD, "r");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while opening [%s] File ", __FUNCTION__, CP_USERNAME_PASSWORD);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			return rv;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: [%s] File is Missing ", __FUNCTION__, CP_USERNAME_PASSWORD);
		CPHI_LIB_TRACE(szDbgMsg);
		return rv;
	}


	while(!feof(fp))
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		if(fgets(szTemp, sizeof(szTemp), fp) != NULL)
		{
			if(szTemp[strlen(szTemp)-1] == LF)
			{
				szTemp[strlen(szTemp)-1] = '\0';
			}

			if(strncasecmp(szTemp, "Username", 8) == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its a User Name ", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				 pszTemp = strchr(szTemp, ':');
				 strncpy(szTempUserName, pszTemp+1, strlen(pszTemp));
				 strcpy(szUsername,szTempUserName);
			}
			else if(strncasecmp(szTemp, "Password", 8) == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its a Password ", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				pszTemp = strchr(szTemp, ':');
				strncpy(szTemPassword, pszTemp+1, strlen(pszTemp));
				strcpy(szPassword,szTemPassword);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Its Neither User Name Nor Password, so Neglecting It ", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
	}

	if(fp != NULL)
	{
		fclose(fp);
	}

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s: User Name :[%s] ", __FUNCTION__, szUsername);
	CPHI_LIB_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: Password :[%s] ", __FUNCTION__, szPassword);
	CPHI_LIB_TRACE(szDbgMsg);
#endif

	debug_sprintf(szDbgMsg, "%s: returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: setDataEntrySrc
 *
 *
 * Description	: This function set the Data Entry Source Value Based on Configuration
 *
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setDataEntrySrc()
{
	int		rv 		= SUCCESS;
	int 	iiCnt 	= 0;
	int     ijCnt	= 0;

	/*Assigning data entry Source Value Based row(Track Indicator) column(Present Flag) track indicator Zero refers Manual Entry
	 * different Arrays Based on device capability (EMV, Contactless, Both EMV& Contact less or None)*/

	signed char szEmvnContactless[3][7]	 = 		 {{46, 46, -1, -1, -1, -1, -1},
											 	 {-1, -1, 44, 42, -1, 40, 37},
											 	 {-1, -1, 45, 43, 36, 41, 38}};

	 signed  char szOnlyEmv[3][7] 			=    {{54, 54, -1, -1, -1, -1, -1},
			 	 	 	 	 	 	 	 	 	 {-1, -1, 52, -1, -1, -1, 49},
			 	 	 	 	 	 	 	 	 	 {-1, -1, 53, -1, 48, -1, 50}};

	  signed char szOnlyContactless[3][7]   = 	 {{35, 35, -1, -1, -1, -1, -1},
			  	  	  	  	  	  	  	  	  	  {-1, -1, 33, 31, -1, -1, -1},
			  	  	  	  	  	  	  	  	  	  {-1, -1, 34, 32, -1, -1, -1}};

	 signed  char szMagStripenKeyed[3][7]   = 	 {{02, 02, -1, -1, -1, -1, -1},
			 	 	 	 	 	 	 	 	 	 {-1, -1, 04, -1, -1, -1, -1},
			 	 	 	 	 	 	 	 	 	 {-1, -1, 03, -1, -1, -1, -1}};

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

    if(cpSettings.iemvEnabled == 1 && cpSettings.iContactessEnabled == 1)
    {
    	memcpy(szDataEntrySrc, szEmvnContactless, 21);
    }
    else if(cpSettings.iemvEnabled == 1 && cpSettings.iContactessEnabled == 0)
    {
    	memcpy(szDataEntrySrc, szOnlyEmv, 21);
    }
    else if(cpSettings.iemvEnabled == 0 && cpSettings.iContactessEnabled == 1)
    {
    	memcpy(szDataEntrySrc, szOnlyContactless, 21);
    }
    else
    {
    	memcpy(szDataEntrySrc, szMagStripenKeyed, 21);
    }
    for(iiCnt = 0; iiCnt < 3; iiCnt++)
    {
    	for(ijCnt = 0; ijCnt < 7; ijCnt++)
    	{
    		debug_sprintf(szDbgMsg, "%s: DataEntrysouce of TrackInd :[%d] PresentFlag :[%d] == [%d]", __FUNCTION__, iiCnt, ijCnt, szDataEntrySrc[iiCnt][ijCnt]);
    		CPHI_LIB_TRACE(szDbgMsg);
    	}
    }

	debug_sprintf(szDbgMsg, "%s: returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getDESValue
 *
 *
 * Description	: This function will give the data entry source value for corresponding present flag and
 *
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
char getDESValue(int iTrackInd, int iPresentFlag)
{

	return szDataEntrySrc[iTrackInd][iPresentFlag-1];

}

/*
 * ============================================================================
 * Function Name: setTxnCodeValue
 *
 *
 * Description	: This function set the transaction code Value
 *
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setTxnCodeValue()
{
	int		rv 		= SUCCESS;
	int 	iiCnt 	= 0;
	int     ijCnt	= 0;

/*ROW0: FOR FUNCTION TYPE OTHER THAN PAYMENT EX: REPORT*/
/*ROW1: CREDIT PAYMENT TYPE*/
/*ROW2: DEBIT PAYMENT TYPE*/
/*ROW3: DEBIT CASH BACK PAYMENT TYPE*/
/*ROW4: GIFT PAYMENT TYPE*/
/*ROW5: CHECK PAYMENT TYPE*/
/*ROW6: EBT PAYMENT TYPE*/
/*COLOUMN 0: CP_AUTH, COLOUMN 1: CP_SALE, COLOUMN 2: CP_COMPLETION, COLOUMN 3: CP_POST_AUTH,  COLOUMN 4: CP_RETURN,  COLOUMN 5: CP_VOID,  COLOUMN 6: CP_ACTIVATE,  COLOUMN 7: CP_REDEMPTION,
 *COLOUMN 8: CP_ADDVALUE,  COLOUMN 9: CP_BALANCE,  COLOUMN 10: CP_DEACTIVATE, COLOUMN 11:CP_EMV_DOWNLOAD,  COLOUMN 12: CP_BATCH_ENQURY,  COLOUMN 13: CP_BATCH_CLOSE, COLOUMN 14: CP_TOKEN_QUERY,  COLOUMN 15: CP_REVERSAL*/

	signed char szTempTransactionCodes[MAX_PAYMENT_TYPE][MAX_COMMANDS]	  =  {{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 50, 51, -1, -1},
																			  {02, 01, 03, 03, 06, 41, -1, -1, -1, -1, -1, 57, -1, -1, -1, 46},
																			  {-1, 21, -1, -1, 24, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 46},
																			  {-1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
																			  {80, -1, 81, 77, -1, 78, 71, 73, 70, 79, 82, -1, -1, -1, -1, -1},
																			  {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
																			  {-1, 61, -1, -1, 65, -1, -1, -1, -1, 66, -1, -1, -1, -1, -1, 46}};

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);


	if(MAX_PAYMENT_TYPE != 7 || MAX_COMMANDS != 16)
	{
		debug_sprintf(szDbgMsg, "%s: No of Payment types or commands changed", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	memcpy(szTransactionCodes, szTempTransactionCodes, 112);

	for(iiCnt = 0; iiCnt < MAX_PAYMENT_TYPE; iiCnt++)
	{
		for(ijCnt = 0; ijCnt < MAX_COMMANDS; ijCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: Transaction code for PAYMENT_TYPE:[%d] COMMAND :[%d] == [%d]", __FUNCTION__, iiCnt, ijCnt, szTransactionCodes[iiCnt][ijCnt]);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: getTxnCode
 *
 *
 * Description	: This function will give the transaction code
 *
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
char getTxnCode(int ipaymenttype, int icommand)
{

	return szTransactionCodes[ipaymenttype][icommand];

}
