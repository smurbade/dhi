
/*
 * cphiReqDataCtrl.c
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */

#include<stdio.h>
#include<string.h>
#include <stdlib.h>

#include "DHI_App/appLog.h"
#include "CPHI/cphicommon.h"
#include "CPHI/cphiGroups.h"
#include "CPHI/cphiCfg.h"
#include "CPHI/cphiResp.h"
#include "CPHI/csv.h"

static int fillHeaderDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static int fillAccDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE, char*);
static int fillTxnInfoDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE );
static int fillMarketDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static int fillMisceInfoDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE );
static int fillIndustryDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static int fillAvsInfoDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static int fillPurchseCardDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE );
static int fillTokenDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static int fillPinBlock(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);

static void 	getEnhacedAuthReq(char*, char*);
//static void 	getInstoreFlag(char*);				// use it When Required
static void 	getPaymentMethod(char*, char*);
static void 	getPinblock(char*, char*);
static void 	getKeySerialNum(char*, char*);
static void 	getThirdPartySV(char*, char*);
static void 	getCVV(char*, char*);
static void  	getTokenAllowed(char*);
static void   	getDupTxnCheck(char*, char*);
static void  	getPanEncryptDtls(char*, char*);
static void     getTaxAmt(char*, char*);
static void   	getDataEntrysrcforFollowOn(char*);
static void   	changeDESBySrvcCodeCheck(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static void     getAccnExpFromTrackData(char*, int, char*);
static int      convertTrackData2Manual(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static int	 	getHealthCareDtls(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static int 	    getHeaderDtlsforFollowOn(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static int 		getTagsForCompletion(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
static int   	getDataEntrysrc(TRANDTLS_PTYPE, char*);
static int 		parseEMVTags(CPTRANDTLS_PTYPE, char*, int);
static int   	getInvoiceNum(char*, char*);
//static int  	getLaneId(char*, char*);
static int 	    custRefNum(char*, char*);
static int 	    destZipCode(char*, char*);
static int 	    getCashBackAmt(char*, char*);
static int 		getCreditCashBackAmt(char*, char*);
static int   	getEMVData(CPTRANDTLS_PTYPE, TRANDTLS_PTYPE);


static  CPPYMTNCMD_STYPE 	stPymtncmd;

static void  Char2Hex(char *, char *, int );
static int Hex2Bin (unsigned char *, int , unsigned char *);

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		cphiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         cphistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  cphistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	cphiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			cphiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)





/*
 * ============================================================================
 * Function Name: setPymtnCmdValue
 *
 * Description	: This function will set the command value and payment type value for every transaction
 *
 * Input Params	: DHI structure containing the details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setPymtnCmdValue(TRANDTLS_PTYPE pstDHIDetails)
{
	int					rv		       = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --------enter--------", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(&stPymtncmd, 0x00, sizeof(stPymtncmd));

	/*Checking For Payment type*/
	if(pstDHIDetails[ePAYMENT_TYPE].elementValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:Payment Type Not Present", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		stPymtncmd.iPaymentType = 0;	// making it as zero for FUNCTION_TYPE other than PAYMENT
	}
	else
	{
		if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "CREDIT") == SUCCESS)
		{
			stPymtncmd.iPaymentType = CREDIT;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "DEBIT") == SUCCESS)
		{
			if(pstDHIDetails[eCASHBACK_AMNT].elementValue != NULL)
			{
				stPymtncmd.iPaymentType = DEBIT_CASHBACK;
			}
			else
			{
				stPymtncmd.iPaymentType = DEBIT;
			}
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "GIFT") == SUCCESS)
		{
			stPymtncmd.iPaymentType = STOREDVALUE;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "EBT") == SUCCESS)
		{
			stPymtncmd.iPaymentType = EBT;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "CHECK") == SUCCESS)
		{
			stPymtncmd.iPaymentType = CHECK;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "ADMIN") == SUCCESS)
		{
			stPymtncmd.iPaymentType = 0;		// for token query
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:Invalid Payment Type", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_INV_FLD_VAL;
			return rv;
		}
	}

	/*checking for Command*/
	if(pstDHIDetails[eCOMMAND].elementValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:Command Not Present Cannot Process Further", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = ERR_NO_FLD;
		return rv;
	}
	if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "PRE_AUTH") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_AUTH;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "COMPLETION") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_COMPLETION;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "SALE") == SUCCESS)
	{
		if(stPymtncmd.iPaymentType == STOREDVALUE)
		{
			stPymtncmd.iCommand = CP_REDEMPTION;
		}
		else
		{
			stPymtncmd.iCommand = CP_SALE;
		}
		/*In Case of SAF, Chase recommended to send SALE as a POST_AUTH(force sale) Transactions: Mail with subject: Chase SAF Dequeue, dated 04-OCT-2016*/
		if(((pstDHIDetails[eSAF_TRAN].elementValue != NULL) && (atoi(pstDHIDetails[eSAF_TRAN].elementValue) == 1)) && (stPymtncmd.iPaymentType == CREDIT))
		{
			stPymtncmd.iCommand = CP_POST_AUTH;
		}
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "CREDIT") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_RETURN;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "VOID") == SUCCESS)
	{
		if(stPymtncmd.iPaymentType == DEBIT || stPymtncmd.iPaymentType == EBT || pstDHIDetails[eEMV_REVERSAL_TYPE].elementValue != NULL)
		{
			stPymtncmd.iCommand = CP_REVERSAL;
		}
		else
		{
			stPymtncmd.iCommand = CP_VOID;
		}
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "POST_AUTH") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_POST_AUTH;
	}
//	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "VOICE_AUTH") == SUCCESS)
//	{
//		stPymtncmd.iCommand = CP_VOICE_AUTH;
//	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "ACTIVATE") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_ACTIVATE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "ADD_VALUE") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_ADDVALUE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "BALANCE") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_BALANCE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "DEACTIVATE") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_DEACTIVATE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "DAYSUMMARY") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_BATCH_INQUIRY;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "CUTOVER") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_BATCH_CLOSE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "TOKEN_QUERY") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_TOKEN_QUERY;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "EMVADMIN") == SUCCESS)
	{
		stPymtncmd.iCommand = CP_EMV_DOWNLOAD;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s:Invalid Command", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = ERR_UNSUPP_CMD;
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: getTransType
 *
 * Description	: This function will get the transaction code and puts in the CPTRANDTLS_STYPE structure
 *
 * Input Params	: buffer containing the transaction type
 * 				  buffer to be filled with the TransCode
 * 				  integer indicating payment type
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getTransCode(char* szCpTransCode)
{
	int				rv		      				 = SUCCESS;
	signed	char    cTxnCode					 = 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --------enter--------", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);


	cTxnCode = getTxnCode(stPymtncmd.iPaymentType, stPymtncmd.iCommand);

	if(cTxnCode < 0)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Transaction code ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = ERR_INV_FLD;
		return rv;
	}
	else
	{
		sprintf(szCpTransCode, "%02d", cTxnCode);
	}

	rv = stPymtncmd.iCommand;

	debug_sprintf(szDbgMsg, "%s: The converted command is %s", __FUNCTION__, szCpTransCode);
	CPHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillCPDtls
 *
 * Description	: Fills up the required chase details structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillCPReqDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails, char* szRespMsg)
{

	int		rv		       = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		/* step 1:Field(A): fill  Header Info details*/
		rv = fillHeaderDtls(pstDHIDtls, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill Header details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* step 2:Field(B):fill  Account Info details*/
		rv = fillAccDtls(pstDHIDtls, pstCPReqDetails, szRespMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill account Details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* step 3:Field(C): fill Transaction amount details*/
		rv = fillTxnInfoDtls(pstDHIDtls, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill the Transaction Information", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		if(!(stPymtncmd.iPaymentType == DEBIT || stPymtncmd.iPaymentType == DEBIT_CASHBACK || stPymtncmd.iPaymentType == EBT))
		{
			/* step 4:Field(D): fill Market Specific Details details*/
			rv = fillMarketDtls(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill Market Specific Details", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}

			/* Field Interchange Compliance Data (E) not Required For The Retail*/
			/* step 5:Field(F): fill Industry Specific Data details*/
			rv = fillIndustryDtls(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Industry Specific Details", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}
			/* step  6:Field(G): fill Miscellaneous Info  details*/
			rv = fillMisceInfoDtls(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Miscellaneous Information", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}
			if(stPymtncmd.iPaymentType != STOREDVALUE)
			{
				/* step  7:Field(H): fill Address Verification Info details*/
				rv = fillAvsInfoDtls(pstDHIDtls, pstCPReqDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to Get Address Verification Service", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
					break;
				}
				/* step 8:Field(I): fill Purchasing Card Info details*/
				rv = fillPurchseCardDtls(pstDHIDtls, pstCPReqDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to Get Purchase Card Details", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}

		}
		else
		{
			rv = fillPinBlock(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Get Pin Details", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}

			if(stPymtncmd.iPaymentType == EBT)
			{
				if(strcmp(pstDHIDtls[eEBT_TYPE].elementValue, "FOOD_STAMP") == 0)
				{
					strcpy(pstCPReqDetails[CP_G_EBT_TYPE].elementValue, "F");
				}
				else if(strcmp(pstDHIDtls[eEBT_TYPE].elementValue, "CASH_BENEFITS") == 0)
				{
					strcpy(pstCPReqDetails[CP_G_EBT_TYPE].elementValue, "C");
				}
				else
				{
					rv = ERR_INV_FLD_VAL;
					break;
				}
			}

		}
		/* step 9:Field(J): fill Token details*/
		rv = fillTokenDtls(pstDHIDtls, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill the token details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}
/*
 * ============================================================================
 * Function Name: fillCPDtlsForBatchMgmt
 *
 * Description	: Fills up the required chase details structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillCPDtlsForBatchMgmt(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int		rv		       = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	while(1)
	{


		/* step 1:Field(A): fill  Header Info details*/
		rv = fillHeaderDtls(pstDHIDtls, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill Header details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
		/* step 2:Field(J): fill Token details*/
		rv = fillTokenDtls(pstDHIDtls, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill the token details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}
/*
 * ============================================================================
 * Function Name: fillCPDtlsForVSPReg
 *
 * Description	: Fills up the required cp details datastructure for device reg.
 *
 * Input Params	:
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
int fillCPDtlsForVSPReg(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int				rv 									= SUCCESS;
	char			szTemp[512]							= "";
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		/* step 1:Field(A): fill  Header Info details*/
		rv = fillHeaderDtls(pstDHIDtls,pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill Header details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* As per the document, Hardcoding Transaction code to 02 for VSP activation */
		strcpy(pstCPReqDetails[CP_A_TXN_CODE].elementValue,"02");

		/* step 2:Field(B):fill  Account Info details*/

		/* Need to find out how to obtain this track 2 data */
		/* Obtain track2 data to szTemp */
		if(pstDHIDtls[eTRACK_DATA].elementValue != NULL && atoi(pstDHIDtls[eTRACK_INDICATOR].elementValue) == 2)
		{
			strcpy(szTemp, pstDHIDtls[eTRACK_DATA].elementValue);
			szTemp[strlen(szTemp) - 1] = '\0';
			strcpy(pstCPReqDetails[CP_B_FULL_MAG_STRIPE_INFO].elementValue,szTemp + 1);
		}
		else
		{
			rv =ERR_FLD_REQD;
			break;
		}
		sprintf(pstCPReqDetails[CP_B_FULL_MAG_STRIPE_INFO].fieldSeparator,"%c", FS);

		/* step 3:Field(C): fill Transaction amount details*/
		/* Hard coding Transaction Amount as given in the document */
		if(pstDHIDtls[eTRANS_AMOUNT].elementValue != NULL)
		{
			strcpy(pstCPReqDetails[CP_C_TXN_AMT].elementValue, pstDHIDtls[eTRANS_AMOUNT].elementValue);
		}
		else
		{
			strcpy(pstCPReqDetails[CP_C_TXN_AMT].elementValue, "0.00");
		}
		sprintf(pstCPReqDetails[CP_C_TXN_AMT].fieldSeparator,"%c", FS);

		getTransInfoFiller(pstCPReqDetails[CP_C_TXN_FILLER].elementValue, pstCPReqDetails[CP_C_TXN_FILLER].fieldSeparator);

		/* step 4:Field(J): fill P1, P2 and PE Token data  details*/

		getPosCapblty1Dtls(pstDHIDtls[eSERIAL_NUM].elementValue,pstCPReqDetails[CP_J_POS_CAPBLTY1].elementValue);
		sprintf(pstCPReqDetails[CP_J_POS_CAPBLTY1].fieldSeparator,"%c", FS);

		getPosCapblty2Dtls(pstCPReqDetails[CP_J_POS_CAPBLTY2].elementValue, pstDHIDtls);
		sprintf(pstCPReqDetails[CP_J_POS_CAPBLTY2].fieldSeparator,"%c", FS);

		if(pstDHIDtls[eENCRYPTION_PAYLOAD].elementValue != NULL)
		{
			getPanEncryptDtls(pstDHIDtls[eENCRYPTION_PAYLOAD].elementValue,pstCPReqDetails[CP_J_PAN_ENCRYPT].elementValue);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:PE Token Is Missing", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_FLD_REQD;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: fillCPDtlsForTokenQuery
 *
 * Description	: Fills up the required cp details datastructure for  token query
 *
 * Input Params	:
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
int fillCPDtlsForTokenQuery(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int				rv 									= SUCCESS;
	int 			ipresentFlag						= 0;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/* step 1:Field(A): fill  Header Info details*/
	rv = fillHeaderDtls(pstDHIDtls,pstCPReqDetails);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to fill Header details", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return rv;
	}

	/* As per the document, credit payment type token query txn code is 91 under assumption that SCA is not supporting token query for debit and ebt*/
	strcpy(pstCPReqDetails[CP_A_TXN_CODE].elementValue,"91");

	/* step 2:Field(B):fill  Account Info details*/
	if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
	{

		ipresentFlag =atoi(pstDHIDtls[ePRESENT_FLAG].elementValue);
		if(ipresentFlag <= 2)
		{
			if(pstDHIDtls[eACCT_NUM].elementValue == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Account number Not Present", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				rv = ERR_FLD_REQD;
				return rv;
			}
			else
			{
				strcpy(pstCPReqDetails[CP_B_ACC_NUM].elementValue, pstDHIDtls[eACCT_NUM].elementValue);
				sprintf(pstCPReqDetails[CP_B_ACC_NUM].fieldSeparator, "%c", FS);
			}

			if(pstDHIDtls[eEXP_MONTH].elementValue == NULL || pstDHIDtls[eEXP_YEAR].elementValue == NULL)
			{
				rv = ERR_FLD_REQD;
				return rv;
			}
			else
			{
				strcpy(pstCPReqDetails[CP_B_EXP_MMYY].elementValue,pstDHIDtls[eEXP_MONTH].elementValue);
				strcat(pstCPReqDetails[CP_B_EXP_MMYY].elementValue,pstDHIDtls[eEXP_YEAR].elementValue);
				sprintf(pstCPReqDetails[CP_B_EXP_MMYY].fieldSeparator, "%c", FS);
			}

		}
		else if(pstDHIDtls[eTRACK_DATA].elementValue != NULL && pstDHIDtls[eTRACK_INDICATOR].elementValue != NULL)
		{
			rv = convertTrackData2Manual(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to convert from track data to manual entry", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				return rv;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: track data or track indicator missing cannot process further", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_FLD_REQD;
			return rv;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Present flag is missing ,cannot process further", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = ERR_FLD_REQD;
		return rv;
	}

	/* step 3:Field(C): fill Transaction amount details*/
	/* Hard coding Transaction Amount as 0.00 (as per document) */

	strcpy(pstCPReqDetails[CP_C_TXN_AMT].elementValue, "0.00");

	sprintf(pstCPReqDetails[CP_C_TXN_AMT].fieldSeparator,"%c", FS);

	getTransInfoFiller(pstCPReqDetails[CP_C_TXN_FILLER].elementValue, pstCPReqDetails[CP_C_TXN_FILLER].fieldSeparator);

	/* step 9:Field(J): fill Token details*/
	rv = fillTokenDtls(pstDHIDtls, pstCPReqDetails);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to fill the token details", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}


/*
 * ============================================================================
 * Function Name: fillCPDtlsForTOR
 *
 * Description	: Fills up the required cp details datastructure
 *
 * Input Params	:
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */


int fillCPDtlsForTOR(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int				rv 									= SUCCESS;
	int 			iCPTorIndices[]						= {
			CP_B_FULL_MAG_STRIPE_INFO,CP_C_TXN_AMT,CP_C_TXN_FILLER, CP_D_MARKET_DATA_IND,
			CP_D_DUKPT_KSN, CP_D_DUKPT_PIN_BLOCK,
			CP_D_CASHBACK_AMT, CP_F_INDUSTRY_CODE,	CP_F_INVOICE_NUMBER,
			CP_F_ITEM_CODE, CP_F_EXT_TRANS_ID, CP_F_EMP_NUM, CP_F_CASHOUT_IND,
			CP_F_SEQ_NUM_CARD, CP_F_TOT_NUM_CARD, CP_G_AUTH_CODE, CP_G_EBT_TYPE,
			CP_G_REF_NUM, CP_H_CRDHLDR_ST_ADD,
			CP_H_EXTD_CRDHLDR_ST_ADD, CP_H_CARDHLDR_ZIP_CODE,
			CP_I_CUST_REFERENCE_NUM, CP_I_LOCAL_TAX_FLAG,
			CP_I_PURCHASE_FILLER, CP_I_DEST_ZIP,
			CP_I_SALES_TAX_AMT, CP_J_DUP_CHECK, CP_J_ENHANCED_AUTH, CP_J_CVV, CP_J_PIN_BLOCK, CP_J_KEY_SERIAL_NUM };
	int 			iCnt							 	= 0;
	int				iMaxSize							= 0;
	int				iIndex								= 0;
	float			famt								= 0.00;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/* Field A and B would be same for the TOR request*/

	/* step 1:Field(C): fill Transaction amount details*/

	if(strlen(pstCPReqDetails[CP_B_FULL_MAG_STRIPE_INFO].elementValue) > 0)
	{
		rv = convertTrackData2Manual(pstDHIDtls, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to convert from track data to manual entry", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}

	famt= atof(pstCPReqDetails[CP_C_TXN_AMT].elementValue);
	sprintf(pstCPReqDetails[CP_C_TXN_AMT_TOR].elementValue, "%09.2f", famt);

	//	strcpy(pstCPReqDetails[CP_C_TXN_AMT_TOR].elementValue, pstCPReqDetails[CP_C_TXN_AMT].elementValue);
	memset(pstCPReqDetails[CP_C_TXN_AMT].elementValue,0x00,sizeof(pstCPReqDetails[CP_C_TXN_AMT].elementValue));

	iMaxSize = sizeof(iCPTorIndices) / sizeof(int);

	for(iCnt = 0; iCnt < iMaxSize; iCnt++)
	{
		iIndex = iCPTorIndices[iCnt];
		memset(pstCPReqDetails[iIndex].elementValue, 0x00, sizeof(pstCPReqDetails[iIndex].elementValue));
		memset(pstCPReqDetails[iIndex].fieldSeparator, 0x00, sizeof(pstCPReqDetails[iIndex].fieldSeparator));
	}

	/* Copying Original Transaction code for the reversal transaction code token */
	strcpy(pstCPReqDetails[CP_J_TRANS_CODE].elementValue, "TC") ;             /*Reversal Advice Transaction Code Indicator*/
	strcat(pstCPReqDetails[CP_J_TRANS_CODE].elementValue,pstCPReqDetails[CP_A_TXN_CODE].elementValue);
	sprintf(pstCPReqDetails[CP_J_TRANS_CODE].fieldSeparator, "%c", FS);

	memset(pstCPReqDetails[CP_A_TXN_CODE].elementValue, 0x00, sizeof(pstCPReqDetails[CP_A_TXN_CODE].elementValue));
	strcpy(pstCPReqDetails[CP_A_TXN_CODE].elementValue, "46");

	/* Filling Reversal Reason Indicator token */
	strcpy(pstCPReqDetails[CP_J_REVERSL_REASON].elementValue, "RR");     /*Reversal Reason Code Indicator*/
	strcat(pstCPReqDetails[CP_J_REVERSL_REASON].elementValue, "1"); 	/* 1: No Response From Host */
	sprintf(pstCPReqDetails[CP_J_REVERSL_REASON].fieldSeparator, "%c", FS);

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillHeaderDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillCPReqDtlsforFollowOnTran(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{
	int			rv									    = SUCCESS;
	int			iFound				 					= CPHI_FALSE;
	int			iCount								    = 0;
	int			iIndex							        = 0;
	int			iTotal								    = 0;
	char 		szRespMsg[256]							= "";
	char        szTemp[400]								= "";
	char*		pszBuffer						    	= NULL;
	float	    famt					    			= 0.00;
//	char 		szIssuerScripRes[10] = "";				//size need to be checked
	FILE*		fptr				 					= NULL;
	int			iCPHIIndices[]						    = { CP_G_REF_NUM, CP_TXN_CODE_FOR_REVERSAL, CP_A_DATA_ENTRY_SRC, CP_B_ACC_NUM, CP_B_EXP_MMYY, CP_C_TXN_AMT, CP_I_CUST_REFERENCE_NUM,
														    CP_I_LOCAL_TAX_FLAG, CP_I_DEST_ZIP, CP_I_SALES_TAX_AMT, CP_G_AUTH_CODE, CP_J_REF_NUM, CP_J_CARD_AUTH_RESLT_CODE, CP_J_CARD_TOKEN};

	int         iCPHIIndicesnotrqdforCompletion[] 	    = { CP_G_REF_NUM, CP_TXN_CODE_FOR_REVERSAL, CP_C_TXN_AMT, CP_J_REF_NUM};

	int         iCPHIIndicesnotrqdforVoid[] 			= { CP_TXN_CODE_FOR_REVERSAL, CP_A_DATA_ENTRY_SRC, CP_B_ACC_NUM, CP_B_EXP_MMYY, CP_C_TXN_AMT,CP_I_CUST_REFERENCE_NUM,
															CP_I_LOCAL_TAX_FLAG, CP_I_DEST_ZIP, CP_I_SALES_TAX_AMT, CP_G_AUTH_CODE, CP_J_REF_NUM, CP_J_CARD_AUTH_RESLT_CODE};

	int         iCPHIIndicesnotrqdforReversal[] 		= { CP_G_REF_NUM, CP_TXN_CODE_FOR_REVERSAL, CP_C_TXN_AMT, CP_I_CUST_REFERENCE_NUM,
															CP_I_LOCAL_TAX_FLAG, CP_I_DEST_ZIP, CP_I_SALES_TAX_AMT, CP_G_AUTH_CODE, CP_J_CARD_AUTH_RESLT_CODE};

	int			iDHIIndices[]							= {eEMV_TAGS, eEMV_TAGS_RESP, eENCRYPTION_PAYLOAD};

#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szRespMsg, 0x00, sizeof(szRespMsg));

	while(1)
	{
		if(pstDHIDtls[eCTROUTD].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: CTROUTD not found", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD_VAL;
			break;
		}

		fptr = fopen(CP_TRAN_RECORDS, "r");
		if(fptr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: --- File not available --- ", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_CTROUTD;

			break;
		}

		while((pszBuffer = csvgetline(fptr)) != NULL)
		{
			pszBuffer = csvfield(iCount);
			if(pszBuffer != NULL)
			{
				if(strcmp(pszBuffer, pstDHIDtls[eCTROUTD].elementValue) == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Found the CTROUTD in the records", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);

					iFound = CPHI_TRUE;
					iCount++;
					break;
				}
			}
		}

		if(iFound == CPHI_FALSE)
		{
			debug_sprintf(szDbgMsg, "%s: CTROUTD not found in the records", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_CTROUTD;

			break;
		}

		iTotal = sizeof(iCPHIIndices)/sizeof(int);
		for(iIndex = 0; iIndex < iTotal; iIndex++)
		{
			pszBuffer = csvfield(iCount++);
			debug_sprintf(szDbgMsg, "%s: CP Field %d - %s", __FUNCTION__, iIndex, pszBuffer);
			CPHI_LIB_TRACE(szDbgMsg);

			if(pszBuffer && strlen(pszBuffer) > 0)
			{
				strcpy(pstCPReqDetails[iCPHIIndices[iIndex]].elementValue, pszBuffer);
			}
		}

//		if(pstDHIDtls[eEMV_TAGS].elementValue)
//		{
//		//	strcpy(szIssuerScripRes,pstDHIDtls[eEMV_TAGS].elementValue);
//			free(pstDHIDtls[eEMV_TAGS].elementValue);
//			pstDHIDtls[eEMV_TAGS].elementValue = NULL;
//		}

		if(pstDHIDtls[eEMV_TAGS_RESP].elementValue)
		{
			free(pstDHIDtls[eEMV_TAGS_RESP].elementValue);
			pstDHIDtls[eEMV_TAGS_RESP].elementValue = NULL;
		}

		if(pstDHIDtls[eENCRYPTION_PAYLOAD].elementValue)
		{
			memset(szTemp, 0x00, sizeof(szTemp));
			strcpy(szTemp, pstDHIDtls[eENCRYPTION_PAYLOAD].elementValue);
			free(pstDHIDtls[eENCRYPTION_PAYLOAD].elementValue);
			pstDHIDtls[eENCRYPTION_PAYLOAD].elementValue = NULL;
		}

		/*Copying DHI Fields from the File into DHI Structure*/
		iTotal = sizeof(iDHIIndices)/sizeof(int);
		for(iIndex = 0; iIndex < iTotal; iIndex++)
		{
			pszBuffer = csvfield(iCount++);
			debug_sprintf(szDbgMsg, "%s: DHI Field %d - %s", __FUNCTION__, iIndex, pszBuffer);
			CPHI_LIB_TRACE(szDbgMsg);

			if(pszBuffer && strlen(pszBuffer) > 0 && !pstDHIDtls[iDHIIndices[iIndex]].elementValue)
			{
				pstDHIDtls[iDHIIndices[iIndex]].elementValue = (char *)malloc(strlen(pszBuffer)+1);
				memset(pstDHIDtls[iDHIIndices[iIndex]].elementValue, 0x00, strlen(pszBuffer)+1);
				strcpy(pstDHIDtls[iDHIIndices[iIndex]].elementValue, pszBuffer);
			}
		}

		/*for CREDIT PRE_AUTH if we void we need to send it as a REVERSAL so changing command if it is PRE_AUTH VOID*/
		if((stPymtncmd.iCommand == CP_VOID || stPymtncmd.iCommand == CP_REVERSAL) && !(strcmp(pstCPReqDetails[CP_TXN_CODE_FOR_REVERSAL].elementValue, "02")))
		{
			strcpy(pstCPReqDetails[CP_A_TXN_CODE].elementValue, "46");  		// 46 - Transaction code for PRE_AUTH VOID (REVERSAL)
			stPymtncmd.iCommand = CP_REVERSAL;
		}

		/*Filling CP Details For Follow On*/

		/* step 1:Field(A): fill  Header Info details*/
		rv = fillHeaderDtls(pstDHIDtls, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill Header details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		if(stPymtncmd.iCommand == CP_COMPLETION)
		{

			iTotal = sizeof(iCPHIIndicesnotrqdforCompletion)/sizeof(int);
			for(iIndex = 0; iIndex < iTotal; iIndex++)
			{
				memset(pstCPReqDetails[iCPHIIndicesnotrqdforCompletion[iIndex]].elementValue, 0x00, sizeof(pstCPReqDetails[iCPHIIndicesnotrqdforCompletion[iIndex]].elementValue));
			}

			/*step 2:Field(B):processing account information*/
			sprintf(pstCPReqDetails[CP_B_ACC_NUM].fieldSeparator, "%c", FS);		 // as element values filled already
			sprintf(pstCPReqDetails[CP_B_EXP_MMYY].fieldSeparator, "%c", FS);

			/* step 3:Field(C): fill Transaction amount details*/
			rv = fillTxnInfoDtls(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill the Transaction Information", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}

			/*step 4:Field(D):processing market specific details*/
			sprintf(pstCPReqDetails[CP_D_MARKET_DATA_IND].fieldSeparator, "%c%c", FS, FS);

			/* step 5:Field(F): fill Industry Specific Data details*/
			rv = fillIndustryDtls(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Industry Specific Details", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}

			/* step  6:Field(G): fill Miscellaneous Info  details*/
			if(stPymtncmd.iPaymentType == CREDIT)
			{
				sprintf(pstCPReqDetails[CP_G_FIELD_SEP].fieldSeparator, "%c", FS);
				sprintf(pstCPReqDetails[CP_G_AUTH_CODE].fieldSeparator, "%c%c%c%c", FS, FS, FS, FS); // as element value filled already
			}
			else
			{
				sprintf(pstCPReqDetails[CP_G_FIELD_SEP].fieldSeparator, "%c%c", FS, FS);			// as G_AUTH_CODE element Value already filled
			}
			// need to be checked//
			/* step 8:Field(I): fill Purchasing Card Info details*/
			if(stPymtncmd.iPaymentType == CREDIT)
			{
				rv = fillPurchseCardDtls(pstDHIDtls, pstCPReqDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to Get Purchase Card Details", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}
			/* step 9:Field(J): fill Token details*/
			rv = fillTokenDtls(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill the token details", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}

			/*below is for the CP_J_CARD_AUTH_RESLT_CODE token which is Already filled*/
			if(strlen(pstCPReqDetails[CP_J_CARD_AUTH_RESLT_CODE].elementValue) > 0)
			{
				sprintf(pstCPReqDetails[CP_J_CARD_AUTH_RESLT_CODE].fieldSeparator, "%c", FS);
			}
		}
		else if(stPymtncmd.iCommand == CP_VOID)
		{
			strcpy(pstCPReqDetails[CP_G_ACC_NUM].elementValue, pstCPReqDetails[CP_B_ACC_NUM].elementValue);
			sprintf(pstCPReqDetails[CP_G_ACC_NUM].fieldSeparator, "%c", FS);

			iTotal = sizeof(iCPHIIndicesnotrqdforVoid)/sizeof(int);
			for(iIndex = 0; iIndex < iTotal; iIndex++)
			{
				memset(pstCPReqDetails[iCPHIIndicesnotrqdforVoid[iIndex]].elementValue, 0x00, sizeof(pstCPReqDetails[iCPHIIndicesnotrqdforVoid[iIndex]].elementValue));
			}

			/* step 5:Field(F): fill Industry Specific Data details*/
			if(stPymtncmd.iPaymentType == STOREDVALUE)
			{
				rv = fillIndustryDtls(pstDHIDtls, pstCPReqDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to get Industry Specific Details", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}

			/* step  6:Field(G): fill Miscellaneous Info  details*/
			sprintf(pstCPReqDetails[CP_G_REF_NUM].fieldSeparator, "%c", FS);


			/* step 9:Field(J): fill Token details*/
			rv = fillTokenDtls(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill the token details", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}

		}
		else if(stPymtncmd.iCommand == CP_REVERSAL)
		{
			/*step 2:Field(B):processing account information*/
			if(pstDHIDtls[eTRACK_DATA].elementValue != NULL || pstDHIDtls[eACCT_NUM].elementValue != NULL)
			{
				memset(pstCPReqDetails[CP_B_ACC_NUM].elementValue, 0x00, strlen(pstCPReqDetails[CP_B_ACC_NUM].elementValue));
				memset(pstCPReqDetails[CP_B_EXP_MMYY].elementValue, 0x00, strlen(pstCPReqDetails[CP_B_EXP_MMYY].elementValue));
				memset(pstCPReqDetails[CP_J_CARD_TOKEN].elementValue, 0x00, strlen(pstCPReqDetails[CP_J_CARD_TOKEN].elementValue));
				rv= fillAccDtls(pstDHIDtls, pstCPReqDetails, szRespMsg);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to fill Account details", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
					break;
				}
				pstDHIDtls[eENCRYPTION_PAYLOAD].elementValue = strdup(szTemp);
			}
			else
			{
				sprintf(pstCPReqDetails[CP_B_ACC_NUM].fieldSeparator, "%c", FS);							 // as element values filled already
				sprintf(pstCPReqDetails[CP_B_EXP_MMYY].fieldSeparator, "%c", FS);
			}

			famt= atof(pstCPReqDetails[CP_C_TXN_AMT].elementValue);
			sprintf(pstCPReqDetails[CP_C_TXN_AMT_TOR].elementValue, "%09.2f", famt);

			/* Copying Original Transaction code for the reversal transaction code token */
			strcpy(pstCPReqDetails[CP_J_TRANS_CODE].elementValue, "TC") ;            				 /*Reversal Advice Transaction Code Indicator*/
			strcat(pstCPReqDetails[CP_J_TRANS_CODE].elementValue,pstCPReqDetails[CP_TXN_CODE_FOR_REVERSAL].elementValue);
			sprintf(pstCPReqDetails[CP_J_TRANS_CODE].fieldSeparator, "%c", FS);

			/* Filling Reversal Reason Indicator token */
			if(pstDHIDtls[eEMV_REVERSAL_TYPE].elementValue != NULL)
			{
				strcpy(pstCPReqDetails[CP_J_REVERSL_REASON].elementValue, "RR");    					 /*Reversal Reason Code Indicator*/
				strcat(pstCPReqDetails[CP_J_REVERSL_REASON].elementValue, "4"); 						/* 4= Card declined the transaction (applicable only to EMV transactions) */
				sprintf(pstCPReqDetails[CP_J_REVERSL_REASON].fieldSeparator, "%c", FS);
			}
			else
			{
				strcpy(pstCPReqDetails[CP_J_REVERSL_REASON].elementValue, "RR");    					 /*Reversal Reason Code Indicator*/
				strcat(pstCPReqDetails[CP_J_REVERSL_REASON].elementValue, "0"); 						/* 0 = DEFAULT */
				sprintf(pstCPReqDetails[CP_J_REVERSL_REASON].fieldSeparator, "%c", FS);
			}

			/* step 9:Field(J): fill Token details*/
			rv = fillTokenDtls(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill the token details", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				break;
			}

			iTotal = sizeof(iCPHIIndicesnotrqdforReversal)/sizeof(int);
			for(iIndex = 0; iIndex < iTotal; iIndex++)
			{
				memset(pstCPReqDetails[iCPHIIndicesnotrqdforReversal[iIndex]].elementValue, 0x00, sizeof(pstCPReqDetails[iCPHIIndicesnotrqdforReversal[iIndex]].elementValue));
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: should not come here ", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: --- returning %d ---", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillHeaderDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int fillHeaderDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int 		rv  = SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/*getting system indicator */
	getSystemInd(pstCPReqDetails[CP_A_SYSTEM_IND].elementValue);

	/*getting Routing Indicator */
	getRoutingInd(pstCPReqDetails[CP_A_ROUTING_IND].elementValue);

	/*getting clientNumber  */
	getClientNum(pstCPReqDetails[CP_A_CLIENT_NUM].elementValue);

	/*getting Merchant Number */
	getMerchantNum(pstCPReqDetails[CP_A_MERCHANT_NUM].elementValue);

	/*getting Terminal Number */
	getTerminalNum(pstCPReqDetails[CP_A_TERMINAL_NUM].elementValue);

	/*getting transaction sequence flag */
	getTxnSqnFlag(pstCPReqDetails[CP_A_TXN_SQN_FLAG].elementValue);

	/*getting sequence number */
	getCurrentSequenceNum(pstCPReqDetails[CP_A_SEQ_NUM].elementValue);

	/*getting Transaction class */
	getTxnClass(pstCPReqDetails[CP_A_TXN_CLASS].elementValue);

	/* Filling details, if it is a batch management command */
	if(!strcmp(pstCPReqDetails[CP_A_TXN_CODE].elementValue, "50") || !strcmp(pstCPReqDetails[CP_A_TXN_CODE].elementValue, "51"))
	{
		/* 1: As per the document, batch number needs to be hardcoded to 000000 = The host will only
		 * send back totals for the open batch. */
		strcpy(pstCPReqDetails[CP_A_BATCH_NUM].elementValue, "000000");

		/* 2: As per the document, batch sequence number needs to be hardcoded to 000  */
		strcpy(pstCPReqDetails[CP_A_BATCH_SEQ_NUM].elementValue, "000");

		/* 3: As per the document, batch offset needs to be hardcoded to 0 = current batch */
		strcpy(pstCPReqDetails[CP_A_BATCH_OFFSET].elementValue, "0");

		/* 4: As per the document, transaction count can be set to 000000 */
		strcpy(pstCPReqDetails[CP_A_TRAN_COUNT].elementValue, "000000");

		/* 5: As per the document, net amount can be set to 000000000.00 */
		strcpy(pstCPReqDetails[CP_A_NET_AMT].elementValue, "00000000.00");
		sprintf(pstCPReqDetails[CP_A_NET_AMT].fieldSeparator, "%c", FS);

		/* 6: As per the document, application name needs to be sent  */
		strcpy(pstCPReqDetails[CP_A_APP_NAME].elementValue, "CPHI   ");								// chase expects Application name as 7 characters so appended with spaces

		/* 6: As per the document, application release date needs to be sent  */
		strcpy(pstCPReqDetails[CP_A_RELEASE_DATE].elementValue, CPHI_RELEASE_DATE);

		/* 7: As per the document, application version number needs to be sent  */
		/* From where do we obtain this */
		sprintf(pstCPReqDetails[CP_A_EPROM_VER_NUM].elementValue, "%s ", CPHI_LIB_VERSION);			// chase expects VERSION as 10 characters so appended with spaces
		sprintf(pstCPReqDetails[CP_A_EPROM_VER_NUM].fieldSeparator, "%c",FS);

		/* 8: Fill filler values */
		strcpy(pstCPReqDetails[CP_A_FILLER].elementValue, TXN_FILLER);
	}
	else if((stPymtncmd.iCommand == CP_VOID) || (stPymtncmd.iCommand == CP_COMPLETION) || (stPymtncmd.iCommand == CP_REVERSAL))
	{
		getHeaderDtlsforFollowOn(pstDHIDtls, pstCPReqDetails);
	}
	else
	{
		/*getting Pin capability code */
		getPinCapabCode(pstCPReqDetails[CP_A_PIN_CAPBLTY_CODE].elementValue);

		/*getting DataEntry Source */
		rv = getDataEntrysrc(pstDHIDtls,pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: failed to get data entry source", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillAccDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillAccDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails, char* szRespMsg)
{
	int   	  rv				 = SUCCESS;
	int       iPresentFlag		 =   0;

#ifdef DEBUG
	char	   szDbgMsg[256]	 = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
	{
		iPresentFlag = atoi(pstDHIDtls[ePRESENT_FLAG].elementValue);
		if(iPresentFlag < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Present Flag !! Cannot proceed further!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_INV_FLD_VAL;
			return rv;
		}
	}
	else if(pstDHIDtls[eCARD_TOKEN].elementValue != NULL)
	{
		iPresentFlag    = 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Present Flag missing !! Cannot proceed further!!", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = ERR_FLD_REQD;
		return rv;
	}

	if((((pstDHIDtls[eSAF_TRAN].elementValue != NULL) && (atoi(pstDHIDtls[eSAF_TRAN].elementValue) == 1)) || stPymtncmd.iCommand == CP_REVERSAL) && iPresentFlag > 2)
	{
		if(pstDHIDtls[eTRACK_DATA].elementValue != NULL && pstDHIDtls[eTRACK_INDICATOR].elementValue != NULL)
		{
			rv = convertTrackData2Manual(pstDHIDtls, pstCPReqDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to convert from track data to manual entry", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
			}
		}
		else
		{

			debug_sprintf(szDbgMsg, "%s: track data or track indicator missing cannot process further", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_FLD_REQD;
		}
		return rv;
	}


	switch(iPresentFlag)
	{
	case 1:
	case 2:


		if(pstDHIDtls[eACCT_NUM].elementValue == NULL && pstDHIDtls[eCARD_TOKEN].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Account number or card token Not Present", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_FLD_REQD;
			break;
		}
		else if(pstDHIDtls[eACCT_NUM].elementValue != NULL)
		{
			strcpy(pstCPReqDetails[CP_B_ACC_NUM].elementValue, pstDHIDtls[eACCT_NUM].elementValue);
			sprintf(pstCPReqDetails[CP_B_ACC_NUM].fieldSeparator, "%c", FS);
		}
		else
		{
			strcpy(pstCPReqDetails[CP_B_ACC_NUM].elementValue, pstDHIDtls[eCARD_TOKEN].elementValue);
			sprintf(pstCPReqDetails[CP_B_ACC_NUM].fieldSeparator, "%c", FS);
		}

			if(pstDHIDtls[eEXP_MONTH].elementValue == NULL || pstDHIDtls[eEXP_YEAR].elementValue == NULL)
			{
				rv = ERR_FLD_REQD;
			}
			else
			{
				strcpy(pstCPReqDetails[CP_B_EXP_MMYY].elementValue,pstDHIDtls[eEXP_MONTH].elementValue);
				strcat(pstCPReqDetails[CP_B_EXP_MMYY].elementValue,pstDHIDtls[eEXP_YEAR].elementValue);
				sprintf(pstCPReqDetails[CP_B_EXP_MMYY].fieldSeparator, "%c", FS);
			}

		break;

	case 3:
	case 4:
	case 5:
	case 6:
	case 7:

		if(pstDHIDtls[eTRACK_DATA].elementValue == NULL || pstDHIDtls[eTRACK_INDICATOR].elementValue == NULL)
		{
			rv = ERR_FLD_REQD;
		}
		else if((atoi(pstDHIDtls[eTRACK_INDICATOR].elementValue) == 1) && (stPymtncmd.iPaymentType == CREDIT))
		{
			strcpy(pstCPReqDetails[CP_B_FULL_MAG_STRIPE_INFO].elementValue, pstDHIDtls[eTRACK_DATA].elementValue);
			sprintf(pstCPReqDetails[CP_B_FULL_MAG_STRIPE_INFO].fieldSeparator, "%c", FS);
		}
		else if(atoi(pstDHIDtls[eTRACK_INDICATOR].elementValue) == 2)
		{
			strcpy(pstCPReqDetails[CP_B_FULL_MAG_STRIPE_INFO].elementValue, pstDHIDtls[eTRACK_DATA].elementValue);
			sprintf(pstCPReqDetails[CP_B_FULL_MAG_STRIPE_INFO].fieldSeparator, "%c", FS);
		}
		else
		{
			strcpy(szRespMsg, "TRACK2 Data Required For This Transaction");
			rv = ERR_INV_FLD;								// DEBIT, GIFT, EBT Won't Support Track1 Data
		}
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: should not come here ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = ERR_INV_FLD;
	}

	if((strcmp(pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue, "45") == 0) || (strcmp(pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue, "53") == 0)
		|| (strcmp(pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue, "44") == 0))
	{
		changeDESBySrvcCodeCheck(pstDHIDtls, pstCPReqDetails);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d ", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillTxnInfoDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int fillTxnInfoDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{
	int   	  rv				 = SUCCESS;
	char	  szTemp[2+1]			 = "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eTRANS_AMOUNT].elementValue == NULL)
	{
		if((stPymtncmd.iCommand == CP_BALANCE) || (stPymtncmd.iCommand == CP_DEACTIVATE))
		{
			strcpy(pstCPReqDetails[CP_C_TXN_AMT].elementValue, "0.00");           //  for balance enquiry making 0.00 by default
			sprintf(pstCPReqDetails[CP_C_TXN_AMT].fieldSeparator, "%c",FS);

			getTransInfoFiller(pstCPReqDetails[CP_C_TXN_FILLER].elementValue,pstCPReqDetails[CP_C_TXN_FILLER].fieldSeparator);

			if((stPymtncmd.iCommand == CP_BALANCE) && (stPymtncmd.iPaymentType == STOREDVALUE))
			{
				sprintf(szTemp, "%c%c" ,FS,FS);
				strcat(pstCPReqDetails[CP_C_TXN_FILLER].fieldSeparator,szTemp);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Transaction amount not present", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_FLD_REQD;
			return rv;
		}
	}
	else
	{
		strcpy(pstCPReqDetails[CP_C_TXN_AMT].elementValue, pstDHIDtls[eTRANS_AMOUNT].elementValue);
		sprintf(pstCPReqDetails[CP_C_TXN_AMT].fieldSeparator, "%c", FS);

		getTransInfoFiller(pstCPReqDetails[CP_C_TXN_FILLER].elementValue,pstCPReqDetails[CP_C_TXN_FILLER].fieldSeparator);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillMarketDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillPinBlock(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int   	  rv	        = SUCCESS;

#ifdef DEBUG
	char	  szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/*According to mail on Monday, April 04, 2016 8:19 PM with subject Chase Direct (enterprise) Debit Cert Project for MX from Sfeir, Edward [mailto:edward.sfeir@chasepaymentech.com]
	 * it is mentioned that if PIN Block and KSN are not present for Debit transactions, it should go as Empty feilds <FS><FS>*/
	if(pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue != NULL)
	{
		strcpy(pstCPReqDetails[CP_D_DUKPT_KSN].elementValue, pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue);
	}
//	else
//	{
//		debug_sprintf(szDbgMsg, "%s: Key Serial Number is Not present Cannot process Further ", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//		rv = ERR_FLD_REQD;
//		return rv;
//	}

	if(pstDHIDtls[ePIN_BLOCK].elementValue != NULL)
	{
		strcpy(pstCPReqDetails[CP_D_DUKPT_PIN_BLOCK].elementValue, pstDHIDtls[ePIN_BLOCK].elementValue);
		sprintf(pstCPReqDetails[CP_D_DUKPT_PIN_BLOCK].fieldSeparator, "%c", FS);
	}
	else
	{
		sprintf(pstCPReqDetails[CP_D_DUKPT_PIN_BLOCK].fieldSeparator, "%c", FS);
//		debug_sprintf(szDbgMsg, "%s: PIN Block is Not present Cannot process Further ", __FUNCTION__);
//		CPHI_LIB_TRACE(szDbgMsg);
//		rv = ERR_FLD_REQD;
//		return rv;
	}

	if(pstDHIDtls[eCASHBACK_AMNT].elementValue != NULL)
	{
		rv = getCashBackAmt(pstDHIDtls[eCASHBACK_AMNT].elementValue, pstCPReqDetails[CP_D_CASHBACK_AMT].elementValue);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Get Cash Amount", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			return rv;
		}
	}
	if(stPymtncmd.iPaymentType == EBT)
	{
		sprintf(pstCPReqDetails[CP_D_CASHBACK_AMT].fieldSeparator, "%c%c", FS, FS);
	}
	else
	{
		sprintf(pstCPReqDetails[CP_D_CASHBACK_AMT].fieldSeparator, "%c", FS);

	}
	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillMarketDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillMarketDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int   	  rv	        = SUCCESS;

#ifdef DEBUG
	char	  szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eAMT_HLTCARE].elementValue != NULL)
	{
		strcpy(pstCPReqDetails[CP_D_MARKET_DATA_IND].elementValue, "M");			//indicating FSA Transaction
	}

	sprintf(pstCPReqDetails[CP_D_MARKET_DATA_IND].fieldSeparator, "%c%c", FS, FS);

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillIndustryDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillIndustryDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int   	  rv				 = SUCCESS;

#ifdef DEBUG
	char	   szDbgMsg[256]	 = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(stPymtncmd.iPaymentType == STOREDVALUE)
	{
		/* getting the industry code*/
		getIndusCode(pstCPReqDetails[CP_F_INDUSTRY_CODE].elementValue, pstDHIDtls[ePAYMENT_TYPE].elementValue);

		/*getting External Transaction identifier*/
		getExtTransId(pstCPReqDetails[CP_F_EXT_TRANS_ID].elementValue ,pstDHIDtls[eLANE].elementValue);

		/*getting Employee Number*/
		rv = getEmployeeNum(pstCPReqDetails[CP_F_EMP_NUM].elementValue, pstDHIDtls[eCASHIER_NUM].elementValue);

		/*getting cash out indicator*/
		getCashOutInd(pstCPReqDetails[CP_F_CASHOUT_IND].elementValue);

		/*getting Sequence Number of cards being issued or activated*/
		getSeqNumCards(pstCPReqDetails[CP_F_SEQ_NUM_CARD].elementValue, pstDHIDtls[eCOMMAND].elementValue);

		/*getting Total Number of Cards being issued or activated */
		getTotalNumCards(pstCPReqDetails[CP_F_TOT_NUM_CARD].elementValue, pstDHIDtls[eCOMMAND].elementValue);

	}

	else
	{
		/* getting the industry code*/
		getIndusCode(pstCPReqDetails[CP_F_INDUSTRY_CODE].elementValue, pstDHIDtls[ePAYMENT_TYPE].elementValue);

		/*getting the invoice number*/
		rv = getInvoiceNum(pstDHIDtls[eINVOICE].elementValue, pstCPReqDetails[CP_F_INVOICE_NUMBER].elementValue);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Get Invoice", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			return rv;
		}

		/*getting item code*/
		getItemCode(pstCPReqDetails[CP_F_ITEM_CODE].elementValue);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);
	return rv;

}


/*
 * ============================================================================
 * Function Name: fillMisceInfoDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillMisceInfoDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int   	  rv	        = SUCCESS;

#ifdef DEBUG
	char	  szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(stPymtncmd.iPaymentType == CREDIT)
	{
		if(stPymtncmd.iCommand == CP_POST_AUTH)
		{
			sprintf(pstCPReqDetails[CP_G_FIELD_SEP].fieldSeparator, "%c", FS);

			if(pstDHIDtls[eAUTH_CODE].elementValue != NULL)
			{
				strcpy(pstCPReqDetails[CP_G_AUTH_CODE].elementValue, pstDHIDtls[eAUTH_CODE].elementValue);
			}
			else if(((pstDHIDtls[eSAF_TRAN].elementValue != NULL) && (atoi(pstDHIDtls[eSAF_TRAN].elementValue) == 1)) && (pstDHIDtls[eSAF_APPROVALCODE].elementValue != NULL))
			{
				strcpy(pstCPReqDetails[CP_G_AUTH_CODE].elementValue, pstDHIDtls[eSAF_APPROVALCODE].elementValue);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Auth Code is missing, Its Mandatory for POST AUTH (prior sale) Transactions.", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				rv = ERR_FLD_REQD;
				return rv;
			}
			sprintf(pstCPReqDetails[CP_G_AUTH_CODE].fieldSeparator, "%c%c", FS, FS);
		}
		else
		{
			sprintf(pstCPReqDetails[CP_G_AUTH_CODE].fieldSeparator, "%c%c%c", FS, FS, FS);   // used CP_G_AUTH_CODE in this case to put FS only
		}
	}

	if(stPymtncmd.iPaymentType == STOREDVALUE)
	{
		if((stPymtncmd.iCommand == CP_POST_AUTH))
		{
			sprintf(pstCPReqDetails[CP_G_FIELD_SEP].fieldSeparator, "%c%c", FS,FS);

			if(pstDHIDtls[eAUTH_CODE].elementValue != NULL)
			{
				strcpy(pstCPReqDetails[CP_G_AUTH_CODE].elementValue, pstDHIDtls[eAUTH_CODE].elementValue);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Auth Code is missing, Its Mandatory for POST AUTH (prior sale) Transactions.", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				rv = ERR_FLD_REQD;
				return rv;
			}
		}
		if((stPymtncmd.iCommand == CP_ACTIVATE) || (stPymtncmd.iCommand == CP_ADDVALUE) || stPymtncmd.iCommand == CP_REDEMPTION)
		{
			sprintf(pstCPReqDetails[CP_G_AUTH_CODE].fieldSeparator, "%c%c%c", FS, FS, FS);
		}
	}


	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillAvsInfoDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillAvsInfoDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{
	int   	   rv				 = SUCCESS;

#ifdef DEBUG
	char	   szDbgMsg[256]	 = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/*T_POLISETTYG1: filling of Field separator for AVS Info in case of Post auth is removed from fillMisceInfoDtls function, so feild separator will be filled here */
//	if(stPymtncmd.iCommand == CP_POST_AUTH)
//	{
//
//		debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__, rv);
//		CPHI_LIB_TRACE(szDbgMsg);
//
//		return rv;
//	}

	if(pstDHIDtls[eCUSTOMER_STREET].elementValue != NULL)
	{
		strcpy(pstCPReqDetails[CP_H_CRDHLDR_ST_ADD].elementValue, pstDHIDtls[eCUSTOMER_STREET].elementValue);
	}

	/* TOD::need to get clarity on extended card holder street address*/

	// used CP_H_EXTD_CRDHLDR_ST_ADD in this case to put FS only
	sprintf(pstCPReqDetails[CP_H_EXTD_CRDHLDR_ST_ADD].fieldSeparator, "%c", FS);

	if(pstDHIDtls[eCUSTOMER_ZIP].elementValue != NULL)
	{
		strcpy(pstCPReqDetails[CP_H_CARDHLDR_ZIP_CODE].elementValue, pstDHIDtls[eCUSTOMER_ZIP].elementValue);
	}

	sprintf(pstCPReqDetails[CP_H_CARDHLDR_ZIP_CODE].fieldSeparator, "%c", FS);   // used CP_H_CARDHLDR_ZIP_CODE in this case to put FS only

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillPurchseCardDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillPurchseCardDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{
	int   	   rv					 = SUCCESS;
	int        iDatareqd         	 = CPHI_FALSE;


#ifdef DEBUG
	char	   szDbgMsg[256]	 = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);


	if(pstDHIDtls[ePURCHASE_ID].elementValue != NULL || pstDHIDtls[eTAX_IND].elementValue != NULL || pstDHIDtls[eTAX_AMOUNT].elementValue != NULL || pstDHIDtls[eDEST_POSTAL_CODE].elementValue != NULL)
	{
		iDatareqd = CPHI_TRUE;
	}
	if(iDatareqd == CPHI_TRUE)
	{
		if(pstDHIDtls[ePURCHASE_ID].elementValue != NULL)
		{
			memset(pstCPReqDetails[CP_I_CUST_REFERENCE_NUM].elementValue, 0X00, sizeof(pstCPReqDetails[CP_I_CUST_REFERENCE_NUM].elementValue));
			rv = custRefNum(pstDHIDtls[ePURCHASE_ID].elementValue, pstCPReqDetails[CP_I_CUST_REFERENCE_NUM].elementValue);
		}
		else if(strlen(pstCPReqDetails[CP_I_CUST_REFERENCE_NUM].elementValue) > 0)
		{
			//do nothing
		}
		else
		{
			strcpy(pstCPReqDetails[CP_I_CUST_REFERENCE_NUM].elementValue, "00000000000000000");		// if purchase order number was not there filling zeros
		}

		if(pstDHIDtls[eTAX_IND].elementValue != NULL)
		{
			strcpy(pstCPReqDetails[CP_I_LOCAL_TAX_FLAG].elementValue , pstDHIDtls[eTAX_IND].elementValue);
		}

		if(pstDHIDtls[eDEST_POSTAL_CODE].elementValue != NULL)
		{
			memset(pstCPReqDetails[CP_I_DEST_ZIP].elementValue, 0X00, sizeof(pstCPReqDetails[CP_I_DEST_ZIP].elementValue));
			rv = destZipCode(pstDHIDtls[eDEST_POSTAL_CODE].elementValue, pstCPReqDetails[CP_I_DEST_ZIP].elementValue);
		}
		else if(strlen(pstCPReqDetails[CP_I_DEST_ZIP].elementValue) > 0)
		{
			//do nothing
		}
		else
		{
			rv = ERR_FLD_REQD;
			return rv;
		}

		if(pstDHIDtls[eTAX_AMOUNT].elementValue == NULL && pstDHIDtls[eTAX_IND].elementValue != NULL)
		{
			if(atoi(pstDHIDtls[eTAX_IND].elementValue) == 1)
			{
				rv = ERR_FLD_REQD;
				return rv;
			}
			else if(atoi(pstDHIDtls[eTAX_IND].elementValue) == 2)
			{
				// for tax Non-taxable transaction
				memset(pstCPReqDetails[CP_I_SALES_TAX_AMT].elementValue, 0X00, sizeof(pstCPReqDetails[CP_I_SALES_TAX_AMT].elementValue));
				sprintf(pstCPReqDetails[CP_I_SALES_TAX_AMT].elementValue, "%s", "000000.00");
			}
		}

		if(pstDHIDtls[eTAX_AMOUNT].elementValue != NULL)
		{
			memset(pstCPReqDetails[CP_I_SALES_TAX_AMT].elementValue, 0X00, sizeof(pstCPReqDetails[CP_I_SALES_TAX_AMT].elementValue));
			getTaxAmt(pstDHIDtls[eTAX_AMOUNT].elementValue, pstCPReqDetails[CP_I_SALES_TAX_AMT].elementValue);
		}
	}

	if(strlen(pstCPReqDetails[CP_I_CUST_REFERENCE_NUM].elementValue) > 0)
	{
		strcpy(pstCPReqDetails[CP_I_PURCHASE_FILLER].elementValue, "        "); 					// purchase card filler 8 Spaces
	}


	sprintf(pstCPReqDetails[CP_I_SALES_TAX_AMT].fieldSeparator, "%c", FS);
	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillTokenDtls
 *
 * Description	: Fills up the required  chase details in structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillTokenDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{
	int   	    rv			   = SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/* below are the multi use token details*/

	/*filling customer defined data  token*/
	getCurrentCustDefData(pstDHIDtls, pstCPReqDetails[CP_J_CUST_DATA].elementValue);
	sprintf(pstCPReqDetails[CP_J_CUST_DATA].fieldSeparator, "%c", FS);

	/* Filling details, if it is a batch management command */
	if(!strcmp(pstCPReqDetails[CP_A_TXN_CODE].elementValue, "50") || !strcmp(pstCPReqDetails[CP_A_TXN_CODE].elementValue, "51"))
	{
		/* TokenAllowed is the default token for every payment type and transaction type  don't change any thing related to this token */
		getTokenAllowed(pstCPReqDetails[CP_J_TOKEN_ALLOWED].elementValue);
		return rv;
	}

	/*filling POS capabilities 1 token*/
	getPosCapblty1Dtls(pstDHIDtls[eSERIAL_NUM].elementValue, pstCPReqDetails[CP_J_POS_CAPBLTY1].elementValue);
	sprintf(pstCPReqDetails[CP_J_POS_CAPBLTY1].fieldSeparator, "%c", FS);

	/*filling POS capabilities 2 token*/
	getPosCapblty2Dtls(pstCPReqDetails[CP_J_POS_CAPBLTY2].elementValue, pstDHIDtls);
	sprintf(pstCPReqDetails[CP_J_POS_CAPBLTY2].fieldSeparator, "%c", FS);

	/* Get Reference number Token */
	if(!(stPymtncmd.iCommand == CP_REVERSAL || stPymtncmd.iCommand == CP_BALANCE))
	{
		getCurrentTknRefNum(pstCPReqDetails[CP_J_REF_NUM].elementValue);
	}
	if(strlen(pstCPReqDetails[CP_J_REF_NUM].elementValue) > 0)
	{
		sprintf(pstCPReqDetails[CP_J_REF_NUM].fieldSeparator, "%c", FS);
	}

	/*filling cvv token*/
	if(pstDHIDtls[eCVV2].elementValue != NULL)
	{
		/* check the transaction type before using this token*/
		getCVV(pstDHIDtls[eCVV2].elementValue, pstCPReqDetails[CP_J_CVV].elementValue);
		sprintf(pstCPReqDetails[CP_J_CVV].fieldSeparator, "%c", FS);
	}

	/*filling health care token*/
	if(pstDHIDtls[eAMT_HLTCARE].elementValue != NULL)
	{
		rv = getHealthCareDtls(pstDHIDtls,pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill the HealthCare Details", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			return rv;
		}
	}
	if(strlen(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue) > 0)
	{
		sprintf(pstCPReqDetails[CP_J_HEALTH_CARE].fieldSeparator, "%c", FS);
	}

	/*filling duplicate check token*/
	if(!(stPymtncmd.iCommand == CP_VOID || stPymtncmd.iCommand == CP_REVERSAL || stPymtncmd.iCommand == CP_BALANCE))
	{
		getDupTxnCheck(pstDHIDtls[eFORCE_FLAG].elementValue, pstCPReqDetails[CP_J_DUP_CHECK].elementValue);
		sprintf(pstCPReqDetails[CP_J_DUP_CHECK].fieldSeparator, "%c", FS);
	}

	/*filling enhanced authorization token*/
	if((stPymtncmd.iCommand == CP_AUTH && stPymtncmd.iPaymentType != STOREDVALUE) || stPymtncmd.iCommand == CP_SALE || stPymtncmd.iCommand == CP_RETURN || stPymtncmd.iCommand == CP_REDEMPTION)
	{
		getEnhacedAuthReq(pstDHIDtls[ePARTIAL_AUTH].elementValue, pstCPReqDetails[CP_J_ENHANCED_AUTH].elementValue);
		sprintf(pstCPReqDetails[CP_J_ENHANCED_AUTH].fieldSeparator, "%c", FS);
	}

	/* below are the tokens required for the EMV Transaction*/
	if((pstDHIDtls[eEMV_TAGS].elementValue != NULL) && (!((pstDHIDtls[eSAF_TRAN].elementValue != NULL) && (atoi(pstDHIDtls[eSAF_TRAN].elementValue) == 1))))
	{
		rv = parseEMVTags(pstCPReqDetails, pstDHIDtls[eEMV_TAGS].elementValue, REQUEST_EMVTAGS);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: successfully parsed EMV tags", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to parse EMV Tags", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		if(stPymtncmd.iCommand == CP_COMPLETION)
		{
			rv = getTagsForCompletion(pstDHIDtls, pstCPReqDetails);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: got EMV tags for follow-on", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get EMV tags for follow-on", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
			}

		}

		/*getting all EMV tags Required for Transaction*/
		rv = getEMVData(pstCPReqDetails,pstDHIDtls);
		if(rv == SUCCESS)
		{
			sprintf(pstCPReqDetails[CP_J_CHIP_CARD_DATA].fieldSeparator, "%c", FS);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill EMV Tags", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			return rv;
		}
	}

	/* below are the credit card token details*/

	if(stPymtncmd.iPaymentType == CREDIT)
	{
		/*filling instore flag token  :: Required for only return*/
		if(stPymtncmd.iCommand == CP_RETURN)
		{
			// supported for private label only add it when required
			//getInstoreFlag(pstCPReqDetails[CP_J_INSTORE_FLAG].elementValue);
			//sprintf(pstCPReqDetails[CP_J_INSTORE_FLAG].fieldSeparator, "%c", FS);
		}

		/*filling payment media token*/
		if(pstDHIDtls[ePAYMENT_MEDIA].elementValue != NULL)
		{
			if(!(stPymtncmd.iCommand == CP_VOID || stPymtncmd.iCommand == CP_REVERSAL))
			{
				getPaymentMethod(pstDHIDtls[ePAYMENT_MEDIA].elementValue, pstCPReqDetails[CP_J_PYMNT_METHOD].elementValue);
				if(strlen(pstCPReqDetails[CP_J_PYMNT_METHOD].elementValue) != 0)
				{
					sprintf(pstCPReqDetails[CP_J_PYMNT_METHOD].fieldSeparator, "%c", FS);
				}
			}
		}

		/*filling cash back amount for Discover Transactions*/
		if(pstDHIDtls[eCASHBACK_AMNT].elementValue != NULL)
		{
			if((strcasecmp(pstDHIDtls[ePAYMENT_MEDIA].elementValue, "DISCOVER") == 0) || (strcasecmp(pstDHIDtls[ePAYMENT_MEDIA].elementValue, "DISC") == 0))
			{
				rv = getCreditCashBackAmt(pstDHIDtls[eCASHBACK_AMNT].elementValue, pstCPReqDetails[CP_J_CREDIT_CASHBACK].elementValue);
				if(rv == SUCCESS)
				{
					sprintf(pstCPReqDetails[CP_J_CREDIT_CASHBACK].fieldSeparator, "%c", FS);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Failed to fill the credit cash back details", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
					return rv;
				}
			}

		}
		/*filling pin block token*/
		if(pstDHIDtls[ePIN_BLOCK].elementValue != NULL)
		{
			getPinblock(pstDHIDtls[ePIN_BLOCK].elementValue, pstCPReqDetails[CP_J_PIN_BLOCK].elementValue);
			sprintf(pstCPReqDetails[CP_J_PIN_BLOCK].fieldSeparator, "%c", FS);
		}

		/* filling key serial number token*/
		if(pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue != NULL)
		{
			getKeySerialNum(pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue,pstCPReqDetails[CP_J_KEY_SERIAL_NUM].elementValue);
			sprintf(pstCPReqDetails[CP_J_KEY_SERIAL_NUM].fieldSeparator, "%c", FS);
		}
	}
	else if(stPymtncmd.iPaymentType == STOREDVALUE)
	{
		/*below are the gift card token details*/
		if((isThirdPartyGiftEnabled() == CPHI_TRUE) && ((stPymtncmd.iCommand == CP_ACTIVATE) || (stPymtncmd.iCommand == CP_ADDVALUE)))
		{
			getThirdPartySV(pstDHIDtls[eCOMMAND].elementValue, pstCPReqDetails[CP_J_SV_TRANS_INFO].elementValue);
			sprintf(pstCPReqDetails[CP_J_SV_TRANS_INFO].fieldSeparator, "%c", FS);
		}
		if(stPymtncmd.iCommand != CP_REDEMPTION)
		{
			memset(pstCPReqDetails[CP_J_ENHANCED_AUTH].elementValue, 0x00, sizeof(pstCPReqDetails[CP_J_ENHANCED_AUTH]));
			memset(pstCPReqDetails[CP_J_ENHANCED_AUTH].fieldSeparator, 0x00, sizeof(pstCPReqDetails[CP_J_ENHANCED_AUTH]));
		}

	}

	/*filling encryption payload token*/
	if((pstDHIDtls[eENCRYPTION_PAYLOAD].elementValue != NULL) && (stPymtncmd.iPaymentType != STOREDVALUE))
	{
		getPanEncryptDtls(pstDHIDtls[eENCRYPTION_PAYLOAD].elementValue, pstCPReqDetails[CP_J_PAN_ENCRYPT].elementValue);
		sprintf(pstCPReqDetails[CP_J_PAN_ENCRYPT].fieldSeparator, "%c", FS);
	}

	/*filling card token realated token*/
	if(!(stPymtncmd.iCommand == CP_VOID || stPymtncmd.iCommand == CP_COMPLETION || stPymtncmd.iCommand == CP_REVERSAL))
	{
		if(stPymtncmd.iPaymentType == CREDIT || stPymtncmd.iPaymentType == DEBIT || stPymtncmd.iPaymentType == DEBIT_CASHBACK || stPymtncmd.iPaymentType == EBT)
		{
			if(pstDHIDtls[eCARD_TOKEN].elementValue != NULL)
			{
				strcpy(pstCPReqDetails[CP_J_CARD_TOKEN].elementValue ,"T3");					// Token indicator
				strcat(pstCPReqDetails[CP_J_CARD_TOKEN].elementValue ,"2");						// Informing that we are sending card token in the account number field
			}
			else if(isTokenModeEnabled() == CPHI_TRUE)
			{
				strcpy(pstCPReqDetails[CP_J_CARD_TOKEN].elementValue ,"T3");					// Token indicator
				strcat(pstCPReqDetails[CP_J_CARD_TOKEN].elementValue ,"1");						// Informing that we are sending account number in the account number field, and asking for card token.
			}
		}
	}
	if(strlen(pstCPReqDetails[CP_J_CARD_TOKEN].elementValue) > 0)
	{
		sprintf(pstCPReqDetails[CP_J_CARD_TOKEN].fieldSeparator, "%c", FS);
	}

	/* TokenAllowed is the default token for every payment type and transaction type  don't change any thing related to this token this token  */
	getTokenAllowed(pstCPReqDetails[CP_J_TOKEN_ALLOWED].elementValue);

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name:  getDataEntrysrc
 *
 * Description	:  This function will get the Data Entry source code and puts in the CPTRANDTLS_STYPE structure
 *
 * Input Params	:  structure containing the DHI details
 * 				   buffer to be filled with the Data Entry source
 *
 * Output Params:  SUCCESS/FAILURE
 * ============================================================================
 */
int getDataEntrysrc(TRANDTLS_PTYPE pstDHIDtls, char* szDataEntrySrc)
{
	int   			rv  						 = SUCCESS;
	int       		iPresentFlag				 = 0;
	int 	  		iTrackIndicator			 	 = 0;
	signed	char    cDESValue					 = 0;

#ifdef DEBUG
	char	  szDbgMsg[256]      = "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
	{
		iPresentFlag = atoi(pstDHIDtls[ePRESENT_FLAG].elementValue);
		if(iPresentFlag < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Present Flag !! Cannot proceed further!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = ERR_INV_FLD_VAL;
			return rv;
		}
	}
	else if(pstDHIDtls[eCARD_TOKEN].elementValue != NULL)
	{
		iPresentFlag    = 1;
		iTrackIndicator = 0; 		// setting as 0 for manual entry
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Present Flag Missing !! Cannot proceed further!!", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = ERR_FLD_REQD;
		return rv;

	}

	if(pstDHIDtls[eTRACK_INDICATOR].elementValue != NULL)
	{
		iTrackIndicator= atoi(pstDHIDtls[eTRACK_INDICATOR].elementValue);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: TRACK_INDICATOR is missing ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Checking As Manual Entry ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		iTrackIndicator = 0; 		// setting as 0 for manual entry
	}

	cDESValue = getDESValue(iTrackIndicator, iPresentFlag);

	if(cDESValue < 0)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Data Entry Source Value ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = ERR_INV_FLD;
		return rv;
	}
	else
	{
		sprintf(szDataEntrySrc, "%02d", cDESValue);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name:  getHeaderDtlsforFollowon
 *
 * Description	:  This function will get the header details for followon and puts in the CPTRANDTLS_STYPE structure
 *
 * Input Params	:  structure containing the DHI details
 * 				   structure to be filled the CPHI details
 *
 *
 * Output Params:  SUCCESS/FAILURE
 * ============================================================================
 */
int getHeaderDtlsforFollowOn(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{
	int       rv  				 =  SUCCESS;

#ifdef DEBUG
	char	  szDbgMsg[256]      = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(stPymtncmd.iCommand == CP_VOID)
	{
		getLastRefNum(pstCPReqDetails[CP_A_LAST_REF_NUM].elementValue);
		sprintf(pstCPReqDetails[CP_A_LAST_REF_NUM].fieldSeparator, "%c", FS);
	}
	else		//	Considering it as a completion or reversal
	{
		/*getting Pin capability code */
		getPinCapabCode(pstCPReqDetails[CP_A_PIN_CAPBLTY_CODE].elementValue);

		/*getting DataEntry Source */
		getDataEntrysrcforFollowOn(pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue);

	}

	debug_sprintf(szDbgMsg, "%s: --- returning %d---", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}
/*
 * ============================================================================
 * Function Name:  getDataEntrysrcforFollowOn
 *
 * Description	:  This function will get the Data Entry source code for follow on and puts in the CPTRANDTLS_STYPE structure
 *
 * Input Params	: buffer contains and to be filled with the Data Entry source
 *
 * Output Params: None
 * ============================================================================
 */
void getDataEntrysrcforFollowOn(char* DataEntrySrc)
{
	int iOldDes   = 0;
	int iNewDes   = 0;
#ifdef DEBUG
	char	  szDbgMsg[256]      = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iOldDes = atoi(DataEntrySrc);

	switch(iOldDes)
	{
	case 01:
	case 02:
	case 03:
	case 04:
		iNewDes = 02;
		break;

	case 12:
	case 13:
	case 14:
		iNewDes = 12;
		break;

	case 31:
	case 32:
	case 33:
	case 34:
	case 35:
		iNewDes = 35;
		break;

	case 36:
	case 37:
	case 38:
	case 40:
	case 41:
	case 42:
	case 43:
	case 44:
	case 45:
	case 46:
		iNewDes = 46;
		break;

	case 47:
	case 48:
	case 49:
	case 50:
	case 52:
	case 53:
	case 54:
		iNewDes = 54;
		break;

	case 07:
	case 55:
	case 56:
	case 57:
		iNewDes = iOldDes;
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: shuold not come here", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		break;

	}

	sprintf(DataEntrySrc, "%02d", iNewDes);

	debug_sprintf(szDbgMsg, "%s: OldDES:%02d, New DES :%02d", __FUNCTION__, iOldDes, iNewDes);
	CPHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
	return ;
}

/*
 * ============================================================================
 * Function Name:  changeDESBySrvcCodeCheck
 *
 * Description	:  This function will change DES Value to 03 if card is an EMV and Present flag is 3.
 *
 * Input Params	:  DHI structure, CP structure.
 *
 * Output Params: None
 * ============================================================================
 */
void changeDESBySrvcCodeCheck(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{

	int 	iTrackIndicator 		= 0;
	int		iAppLogEnabled			= 0;
	char    cServicecode        	= 0;
	char	szTrackData[79+1]		= "";
	char    szExpnSrvcCode[5+1]		= "";
	char	szAppLogData[256]		= "";
	char*	pszName					= NULL;
	char*	pszExp					= NULL;

#ifdef DEBUG
	char	  szDbgMsg[256]      = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	if(pstDHIDtls[eTRACK_DATA].elementValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Track Data, Returning ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return;
	}

	if(pstDHIDtls[eTRACK_INDICATOR].elementValue != NULL)
	{
		iTrackIndicator= atoi(pstDHIDtls[eTRACK_INDICATOR].elementValue);
	}

	memset(szTrackData, 0x00, sizeof(szTrackData));
	memset(szExpnSrvcCode, 0x00, sizeof(szExpnSrvcCode));
	strcpy(szTrackData, pstDHIDtls[eTRACK_DATA].elementValue);

	if (iTrackIndicator == 1)
	{
		if((pszName = strchr(szTrackData, '^')) != NULL)
		{
			if((pszExp = strchr(pszName + 1, '^')) != NULL)
			{
				strncpy(szExpnSrvcCode, pszExp + 1, 5);

				if(szExpnSrvcCode[0] != '^')
				{
					if(szExpnSrvcCode[4] != '^')
					{
						cServicecode = szExpnSrvcCode[4];
					}
				}
				else if (szExpnSrvcCode[0] == '^')
				{
					if(szExpnSrvcCode[1] != '^')
					{
						cServicecode = szExpnSrvcCode[1];
					}
				}
			}
		}

	}
	else if (iTrackIndicator == 2)
	{
		if((pszExp = strchr(szTrackData, '=')) != NULL)
		{
			strncpy(szExpnSrvcCode, pszExp + 1, 5);

			if(szExpnSrvcCode[0] != '=')
			{
				if(szExpnSrvcCode[4] != '=')
				{
					cServicecode = szExpnSrvcCode[4];
				}
			}
			else if (szExpnSrvcCode[0] == '=')
			{
				if(szExpnSrvcCode[1] != '=')
				{
					cServicecode = szExpnSrvcCode[1];
				}
			}

		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Track indicator, Returning", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return;
	}


	if((cServicecode == '2') || (cServicecode == '6'))
	{
		if(iTrackIndicator == 2)
		{
			strcpy(pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue, "03");
		}
		else if(iTrackIndicator == 1)
		{
			strcpy(pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue, "04");
		}


		debug_sprintf(szDbgMsg, "%s: Service code [%c] ,changed DES to [%s]", __FUNCTION__, cServicecode, pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue);
		CPHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Service Code [%c] DES Changed to [%s]", cServicecode, pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Service code [%c] , No change in DES", __FUNCTION__, cServicecode);
		CPHI_LIB_TRACE(szDbgMsg);
	}


	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
	return ;
}

/*
 * ============================================================================
 * Function Name  : getInvoiceNum
 *
 * Description	 :  This function will fills the Invoice Number in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the Invoice Number from the DHI
 * 				    buffer to be filled with the Invoice Number
 *
 * Output Params  :  None
 * ============================================================================
 */

int getInvoiceNum(char* pszDHIInvoiceNum, char* pszCPInvoiceNum)
{
	int 		rv 					= SUCCESS;
	int         iTemp          		= 0;
	char 		szTemp[6+1]			= "";

	if(pszDHIInvoiceNum == NULL)
	{
		strcpy(pszCPInvoiceNum, "000000");					// done for dummy sale will be removed
	}
	else if(strlen(pszDHIInvoiceNum) > 6)
	{
		rv = ERR_INV_FLD_VAL;
	}
	else
	{
		iTemp = strlen(pszDHIInvoiceNum);

		memset(szTemp, '0', 6-iTemp);
		strcat(szTemp, pszDHIInvoiceNum);

		strcpy(pszCPInvoiceNum, szTemp);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name  : CustRefNum
 *
 * Description	 :  This function will fills the customer reference number in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the customer reference number from the DHI
 * 				    buffer to be filled with the customer reference number
 *
 * Output Params  :  SUCCESS/FAILURE
 * ============================================================================
 */

int custRefNum(char* pszDHICustRefNum, char* pszCustRefNum)
{

	int 	rv     	    	 = SUCCESS;
	int     iLen  		     = 0;
	char    szTemp[17+1]     = "";

	iLen = strlen(pszDHICustRefNum);
	strcpy(szTemp , pszDHICustRefNum);

	memset(szTemp + iLen , ' ', sizeof(szTemp)-iLen-1);
	strcpy(pszCustRefNum, szTemp);

	return rv;
}

/*
 * ============================================================================
 * Function Name  : destZipCode
 *
 * Description	 :  This function will fills the destination zip code in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the destination zip code from the DHI
 * 				    buffer to be filled with the destination zip code
 *
 * Output Params  :  SUCCESS/FAILURE
 * ============================================================================
 */

int destZipCode(char* pszDHIdestZipCode, char* pszCPdestZipCode)
{

	int 	rv     	    	 = SUCCESS;
	int     iLen  		     = 0;
	char    szTemp[9+1]      = "";

	iLen = strlen(pszDHIdestZipCode);
	strcpy(szTemp , pszDHIdestZipCode);

	memset(szTemp + iLen , ' ', sizeof(szTemp)-iLen-1);
	strcpy(pszCPdestZipCode, szTemp);

	return rv;
}

/*
 * ============================================================================
 * Function Name  : CustRefNum
 *
 * Description	 :  This function will fills the customer reference number in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the customer reference number from the DHI
 * 				    buffer to be filled with the customer reference number
 *
 * Output Params  :  SUCCESS/FAILURE
 * ============================================================================
 */

int getCashBackAmt(char* pszDHICashbackAmt, char* pszCashbackAmt)
{

	int 	rv     	    	 = SUCCESS;
	float   famount 		 = 0.00;

	famount = atof(pszDHICashbackAmt);
	if(famount < 0.01)
	{
		rv = ERR_INV_FLD_VAL;
	}
	else
	{
		strcpy(pszCashbackAmt, pszDHICashbackAmt);
	}
	return rv;
}

/*
 * ============================================================================
 * Function Name  : getCreditCashBackAmt
 *
 * Description	 :  This function will fills the cash back amount for credit payment in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the cash back amount from the DHI
 * 				    buffer to be filled with the cash back amount
 *
 * Output Params  :  SUCCESS/FAILURE
 * ============================================================================
 */

int getCreditCashBackAmt(char* pszDHICashbackAmt, char* pszCashbackAmt)
{
	int 	rv     	    	 = SUCCESS;
	int     iamount          = 0;
	char    szTemp[12+1]	 = "";
	float   famount 		 = 0.00;

	famount = atof(pszDHICashbackAmt);
	if(famount < 0.01)
	{
		rv = ERR_INV_FLD_VAL;
	}
	else
	{
		iamount = famount * 100;
		sprintf(szTemp, "%012d", iamount);
		strcpy(pszCashbackAmt, "CB");
		strcat(pszCashbackAmt, szTemp);
	}
	return rv;
}

///*
// * ============================================================================
// * Function Name  : getLaneId
// *
// *
// * Description	 :  This function will fills the LaneId in the CPTRANDTLS_STYPE structure
// *
// * Input Params	 :  buffer containing the LaneId from the DHI
// * 				    buffer to be filled with the LaneId
// *
// * Output Params  :  SUCCESS/FAILURE
// * ============================================================================
// */
//
//int getLaneId(char* pszDHILaneId, char* pszCPLaneId)
//{
//
//	int			rv 						= SUCCESS;
//	char 		pszTemp[2+1]			= "";
//	int   		iTemp					= 0;
//
//	if(strlen(pszDHILaneId) <= 2)
//	{
//		// Lane Id token indicator
//		strcpy(pszCPLaneId, "DP");
//		iTemp = atoi(pszDHILaneId);
//		sprintf(pszTemp, "%02d", iTemp);
//		strcat(pszCPLaneId, pszTemp);
//		// its for petro so zero fill
//		strcat(pszCPLaneId, "00");
//	}
//	else
//	{
//		rv  =  ERR_INV_FLD_VAL;
//	}
//
//	return rv;
//}

/*
 * ============================================================================
 * Function Name  : getEnhacedAuthReq
 *
 * Description	 :  This function will fills the Enhanced authorization request details in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the Enhanced authorization request details from the DHI
 * 				    buffer to be filled with the Enhanced authorization request details
 *
 * Output Params   : None
 * ============================================================================
 */
void getEnhacedAuthReq(char* pszDHIPartialAuth, char* pszCPEnhancedAuth)
{

	int   		iTemp				= 0;
	char 		szTemp[1+1]		    = "";

	strcpy(pszCPEnhancedAuth, "P8");	// Enhanced Authorization request indicator

	if(pszDHIPartialAuth != NULL)
	{
		iTemp = atoi(pszDHIPartialAuth);
		sprintf(szTemp, "%d", iTemp);
		strcat(pszCPEnhancedAuth, szTemp);
	}
	else
	{
		strcat(pszCPEnhancedAuth, "0");		// if  partial auth field is not present padding with zero means partial auth not requested
	}

	strcat(pszCPEnhancedAuth, "1");			// balance info requested for all transactions
}

/*
 * ============================================================================
 * Function Name: getInstoreFlag
 *
 * Description	 :  This function will fills the instore payment flag in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer to be filled with the instore payment flag
 *
 * Output Params   : None
 * ============================================================================
 */
//void getInstoreFlag(char* pszInstoreFlag)
//{
//	strcpy(pszInstoreFlag, "PM");	    // INSTORE FLAG indicator
//	strcat(pszInstoreFlag, "Y");		// TELLING IT IS INSTORE BY DEFAULT
//}

/*
 * ============================================================================
 * Function Name  : getPaymentMethod
 *
 * Description	 :  This function will fills the PaymentMethod in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the PaymentMethod from the DHI
 * 				    buffer to be filled with the PaymentMethod
 *
 * Output Params   : None
 * ============================================================================
 */
void getPaymentMethod(char* pszDHIPymntMedia, char* pszCPPymntMedia)
{

	if(strcasecmp(pszDHIPymntMedia, "VISA") == 0)
	{
		strcpy(pszCPPymntMedia, "G1");	// method of payment indicator
		strcat(pszCPPymntMedia, "VI");
	}

	else if ((strcasecmp(pszDHIPymntMedia, "MASTERCARD") == 0) || ((strcasecmp(pszDHIPymntMedia, "MC") == 0)))
	{
		strcpy(pszCPPymntMedia, "G1");	// method of payment indicator
		strcat(pszCPPymntMedia, "MC");
	}


	else if (strcasecmp(pszDHIPymntMedia, "AMEX") == 0)
	{
		strcpy(pszCPPymntMedia, "G1");	// method of payment indicator
		strcat(pszCPPymntMedia, "AX");
	}

	else if (strcasecmp(pszDHIPymntMedia,"JCB") == 0)
	{
		strcpy(pszCPPymntMedia, "G1");	// method of payment indicator
		strcat(pszCPPymntMedia, "JC");
	}

	else if ((strcasecmp(pszDHIPymntMedia, "DISCOVER") == 0) || ((strcasecmp(pszDHIPymntMedia, "DISC") == 0)))
	{
		strcpy(pszCPPymntMedia, "G1");	// method of payment indicator
		strcat(pszCPPymntMedia, "DI");
	}

	else
	{
		return ;     /*need to put default condition*/
	}

}

/*
 * ============================================================================
 * Function Name  : getPinblock
 *
 *
 * Description	 :  This function will fills the Pinblock in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the Pinblock from the DHI
 * 				    buffer to be filled with Pinblock
 *
 * Output Params   : None
 * ============================================================================
 */
void getPinblock(char* pszDHIPinblock, char* pszCPPinblock)
{
	strcpy(pszCPPinblock, "TP");				// pin block indicator
	strcat(pszCPPinblock, pszDHIPinblock);
}

/*
 * ============================================================================
 * Function Name   : getKeySerialNum
 *
 *
 * Description	 :  This function will fills the KeySerialNum in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the KeySerialNum from the DHI
 * 				    buffer to be filled with KeySerialNum
 *
 * Output Params   : None
 * ============================================================================
 */
void getKeySerialNum(char* pszDHIKSN, char* pszCPKSN)
{
	strcpy(pszCPKSN,"TQ");			//key serial number indicator
	strcat(pszCPKSN,pszDHIKSN);
}

/*
 * ============================================================================
 * Function Name   : getThirdPartySV
 *
 *
 * Description	 :  This function will fills the ThirdPartySV in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the ThirdPartySV from the DHI
 * 				    buffer to be filled with ThirdPartySV
 *
 * Output Params   : None
 * ============================================================================
 */

void getThirdPartySV(char* pstDHIDtls , char* pszCPSV)
{

#ifdef DEBUG
	char	  szDbgMsg[256]      = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	strcpy(pszCPSV, "SV");
	strcat(pszCPSV, "D");


	if(strcmp(pstDHIDtls, "ACTIVATE") == 0)
	{
		strcat(pszCPSV, "01");
	}
	else if(strcmp(pstDHIDtls, "ADD_VALUE") == 0)
	{
		strcat(pszCPSV, "02");
	}
	else if(strcmp(pstDHIDtls, "CASH_OUT") == 0)
	{
		strcat(pszCPSV, "05");
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		strcat(pszCPSV, "00");
	}
}
/*
 * ============================================================================
 * Function Name  : getCVV
 *
 *
 * Description	 :  This function will fills the CVV in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the CVV from the DHI
 * 				    buffer to be filled CVV
 *
 * Output Params   : None
 * ============================================================================
 */
void getCVV(char* pszDHICVV, char* pszCPCVV)
{
	int  iTemp	     	= 0;
	char szTemp[2+1]    = "";

	iTemp = strlen(pszDHICVV);
	sprintf(szTemp, "%d", iTemp);

	strcpy(pszCPCVV, "CV");					// CVV  indicator
	strcat(pszCPCVV, "PI");					//presence indicator
	strcat(pszCPCVV, "1");					//value of presence indicator defaulted to 1 after reading all possible cases
	strcat(pszCPCVV, "VF");					// verification indicator
	strcat(pszCPCVV, szTemp);     		    //Length of data elements to follow
	strcat(pszCPCVV, pszDHICVV);		    // cvv value present the card

}

/*
 * ============================================================================
 * Function Name  : getHealthCareDtls
 *
 *
 * Description	: This function will fill the HealthCareDtls  in the structure
 *
 *
 * Input Params	:	structure containing the the HealthCareDtls
 * 					structure to be filled with the HealthCareDtls
 *
 *
 * Output Params: None
 * ============================================================================
 */
int getHealthCareDtls(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPReqDetails)
{
	int    rv 			 = SUCCESS;
	float  fAmount       = 0.00;
	char   szTemp[9+1]	 = "";

	strcpy(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue,"HC");	                 	//	HC = Healthcare Auto-Substantiation Amount Indicator
	strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue,"4S");						//4S = Total of all Qualified Medical Items Indicator (Required)
	fAmount = atof(pstDHIDtls[eAMT_HLTCARE].elementValue);
	sprintf(szTemp, "%09.2f", fAmount);
	strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue, szTemp);

	if(pstDHIDtls[eAMT_PRESCRIPION].elementValue != NULL)
	{
		strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue,"4U");					//4U = Total of Prescription/Rx Items Indicator (Optional)
		fAmount = atof(pstDHIDtls[eAMT_PRESCRIPION].elementValue);
		sprintf(szTemp, "%09.2f", fAmount);
		strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue, szTemp);
	}

	if(pstDHIDtls[eAMT_VISION].elementValue != NULL)
	{
		strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue,"4V");					//4V = Total of Vision/Optical Items Indicator (Optional)
		fAmount = atof(pstDHIDtls[eAMT_VISION].elementValue);
		sprintf(szTemp, "%09.2f", fAmount);
		strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue, szTemp);
	}

	if(pstDHIDtls[eAMT_CLINIC].elementValue != NULL)
	{
		strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue,"4W");					//4W = Total of Clinic/Other Qualified Medical Items Indicator	(Optional)
		fAmount = atof(pstDHIDtls[eAMT_CLINIC].elementValue);
		sprintf(szTemp, "%09.2f", fAmount);
		strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue, szTemp);
	}

	if(pstDHIDtls[eAMT_DENTAL].elementValue != NULL)
	{
		strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue,"4X");					//4X = Total of Dental Items Indicator (Optional)
		fAmount = atof(pstDHIDtls[eAMT_DENTAL].elementValue);
		sprintf(szTemp, "%09.2f", fAmount);
		strcat(pstCPReqDetails[CP_J_HEALTH_CARE].elementValue, szTemp);
	}

	return rv;
}


/*
 * ============================================================================
 * Function Name  : getTokenAllowed
 *
 * Description	 :  This function will fills the TokenAllowed indicator in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer to be filled with the TokenAllowed indicator
 *
 * Output Params   : None
 * ============================================================================
 */
void getTokenAllowed(char* pszCPToken)
{
	strcpy(pszCPToken, "TA");		// token allowed indicator
	strcat(pszCPToken, "Y");
}



/*
 * ============================================================================
 * Function Name  : getDupTxnCheck
 *
 * Description	 :  This function will fills the Duplication Transaction check indicator in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer to be filled with the Duplication Transaction check indicator
 *
 * Output Params   : None
 * ============================================================================
 */

void getDupTxnCheck(char* pszDHIDupCheck, char* pszCPDupCheck)
{
	strcpy(pszCPDupCheck, "DU");					    	// Duplication Transaction check indicator
	if((strcmp(pszDHIDupCheck, "TRUE")) == 0)
	{
		strcat(pszCPDupCheck, "00");						// Indicating blindly perform transaction don't check for duplicate
	}
	else
	{
		strcat(pszCPDupCheck, "02");						// Telling to perform duplicate checking based on sequence number,acc number,txn type,txn amount.
	}

}

/*
 * ============================================================================
 * Function Name  : getPanEncryptDtls
 *
 * Description	 :  This function will fills the encryption details in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the the encryption details
 * 					buffer to be filled with the encryption details
 *
 * Output Params   : None
 * ============================================================================
 */
void getPanEncryptDtls(char* pszDHIEncryptDtls, char* pszCPEncryptDtls)
{
	strcpy(pszCPEncryptDtls, "PE");		               // encryption details indicator
	strcat(pszCPEncryptDtls, "1");
	strcat(pszCPEncryptDtls, pszDHIEncryptDtls);
}

/*
 * ============================================================================
 * Function Name  : getTaxAmt
 *
 * Description	 :  This function will fills the Tax Amount  in the CPTRANDTLS_STYPE structure
 *
 * Input Params	 :  buffer containing the Tax Amount
 * 					buffer to be filled with the Tax Amount
 *
 * Output Params   : None
 * ============================================================================
 */

void getTaxAmt(char* pszDHITax, char* pszCPTax)
{
	float fTaxamt  = 0.00;

	fTaxamt = atof(pszDHITax);
	sprintf(pszCPTax, "%09.2f", fTaxamt);
}

/*
 * ============================================================================
 * Function Name  : saveTranDetailsForFollowOnTxn
 *
 * Description	 :  This function will fills the Transaction Details required for follow on in a file
 *
 * Input Params	 :  structure containing the DHI details
 * 					structure containing the request details
 * 					structure containing the response details
 *
 * Output Params   : None
 * ============================================================================
 */
void saveTranDetailsForFollowOnTxn(TRANDTLS_PTYPE pstDHIDetails, CPTRANDTLS_PTYPE pstCPReqDetails, CPTRANDTLS_PTYPE pstCPRespDetails)
{
	int     rv                  = SUCCESS;
	char	szTranRecord[2048] 	= "";
	char	szBuffer[1024]		= "";
	FILE* 	fptr 				= NULL;


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --------enter--------", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	fptr = fopen(CP_TRAN_RECORDS, "a+");
	if(fptr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Could not open the file could not save the record", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return;
	}

	/*
	 * Order in which the data is going to get stored in the file:
	 * sequenceNum|RefNum|Transaction_Code|DataEntrySrc|AccNum|ExpDtls|Transaction_Amount|purchaseOrderNum|TaxFlag|DestZip|TaxAmt|AuthCode|RN_Token|CRToken|T3 Token|EMV_Tags|EMV_RESP_Tags|Encryption Payload|
	 */

	/*Storing all the fields which are required for the follow on transactions*/

	/*storing sequence Number given by the host*/
	if(strlen(pstCPRespDetails[CP_SEQ_NUM].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPRespDetails[CP_SEQ_NUM].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing reference number given by the host*/
	if(strlen(pstCPRespDetails[CP_RET_REF_NUM].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPRespDetails[CP_RET_REF_NUM].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing transaction code */
	if(strlen(pstCPReqDetails[CP_A_TXN_CODE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPReqDetails[CP_A_TXN_CODE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing data entry source value */
	if(strlen(pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing Account Information*/
	if((strlen(pstCPReqDetails[CP_B_ACC_NUM].elementValue) > 0))
	{
		if((pstDHIDetails[eCARD_TOKEN].elementValue != NULL) && strlen(pstDHIDetails[eCARD_TOKEN].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstDHIDetails[eCARD_TOKEN].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			sprintf(szBuffer, "%s|", pstCPReqDetails[CP_B_ACC_NUM].elementValue);
			strcat(szTranRecord, szBuffer);
		}
	}
	else if(strlen(pstCPReqDetails[CP_B_FULL_MAG_STRIPE_INFO].elementValue) > 0)
	{

		rv = convertTrackData2Manual(pstDHIDetails, pstCPReqDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to convert from track data to manual entry", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		if((pstDHIDetails[eCARD_TOKEN].elementValue != NULL) && strlen(pstDHIDetails[eCARD_TOKEN].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstDHIDetails[eCARD_TOKEN].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			sprintf(szBuffer, "%s|", pstCPReqDetails[CP_B_ACC_NUM].elementValue);
			strcat(szTranRecord, szBuffer);
		}
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing Expiry Details*/

	if(strlen(pstCPReqDetails[CP_B_EXP_MMYY].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPReqDetails[CP_B_EXP_MMYY].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing transaction amount */
	if(strlen(pstCPReqDetails[CP_C_TXN_AMT].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPReqDetails[CP_C_TXN_AMT].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}


	/*storing purchase order Number details */
	if(strlen(pstCPReqDetails[CP_I_CUST_REFERENCE_NUM].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPReqDetails[CP_I_CUST_REFERENCE_NUM].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing Tax Flag details */
	if(strlen(pstCPReqDetails[CP_I_LOCAL_TAX_FLAG].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPReqDetails[CP_I_LOCAL_TAX_FLAG].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing Destination zip details */
	if(strlen(pstCPReqDetails[CP_I_DEST_ZIP].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPReqDetails[CP_I_DEST_ZIP].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing Tax Amount details */
	if(strlen(pstCPReqDetails[CP_I_SALES_TAX_AMT].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPReqDetails[CP_I_SALES_TAX_AMT].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing auth code given by the host*/
	if(strlen(pstCPRespDetails[CP_AUTH_ERROR_CODE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPRespDetails[CP_AUTH_ERROR_CODE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing RN token which is sent in the request*/
	if(strlen(pstCPRespDetails[CP_R_POS_RET_REF_NUM].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPRespDetails[CP_R_POS_RET_REF_NUM].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing CR_Token sent by the host*/
	if(strlen(pstCPRespDetails[CP_R_CARD_AUTH].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstCPRespDetails[CP_R_CARD_AUTH].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing T3_Token*/
	if((pstDHIDetails[eCARD_TOKEN].elementValue != NULL) && strlen(pstDHIDetails[eCARD_TOKEN].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", "T32");				// T3 is card Token indicator : values:: 1 - indicates pan is used 2- indicates card token is used
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}


	/*storing EMV_TAGS Sent in The Request to Host*/
	if((pstDHIDetails[eEMV_TAGS].elementValue != NULL && strlen(pstDHIDetails[eEMV_TAGS].elementValue) > 0) && (stPymtncmd.iPaymentType == CREDIT))
	{
		sprintf(szBuffer, "%s|", pstDHIDetails[eEMV_TAGS].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing EMV RESPONSE TAGS sent by the host*/
	if((strlen(pstCPRespDetails[CP_R_CHIP_CARD_DATA].elementValue) > 0) && (stPymtncmd.iPaymentType == CREDIT))
	{
		sprintf(szBuffer, "%s|", pstCPRespDetails[CP_R_CHIP_CARD_DATA].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing ENCRYPTIN PAYLOAD Sent in The Request to Host if card token is not receieved in the response*/
	if(!((pstDHIDetails[eCARD_TOKEN].elementValue != NULL) && strlen(pstDHIDetails[eCARD_TOKEN].elementValue) > 0) &&
			(pstDHIDetails[eENCRYPTION_PAYLOAD].elementValue != NULL && strlen(pstDHIDetails[eENCRYPTION_PAYLOAD].elementValue) > 0))
	{
		sprintf(szBuffer, "%s|", pstDHIDetails[eENCRYPTION_PAYLOAD].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	strcat(szTranRecord, "\n");

	fprintf(fptr, "%s", szTranRecord);

	fclose(fptr);

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);


	return;

}

/*
 * ============================================================================
 * Function Name: getAccnExpFromTrackData
 *
 * Description	: it will copy the account number and exp date from the track data to respective feilds and changes the data entry source value
 *
 * Input Params	: structre containing dhi details,
 * 				  CPHI structre to be filled
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int convertTrackData2Manual(TRANDTLS_PTYPE pstDHIDetails, CPTRANDTLS_PTYPE pstCPReqDetails)
{
		int				rv 									= SUCCESS;
		int 			iLen								= 0;
		int 			ipresentFlag						= 0;
		int             iTrackIndicator						= 0;
		char  			szBuffer[25+1]						= "";
		char*			pszTemp								= NULL;
		signed	char    cDESValue					     	= 0;


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	iTrackIndicator = atoi(pstDHIDetails[eTRACK_INDICATOR].elementValue);
	getAccnExpFromTrackData(pstDHIDetails[eTRACK_DATA].elementValue, iTrackIndicator, szBuffer);
	pszTemp = strchr(szBuffer, '|');
	strncpy(pstCPReqDetails[CP_B_ACC_NUM].elementValue, szBuffer, pszTemp-szBuffer);
	sprintf(pstCPReqDetails[CP_B_ACC_NUM].fieldSeparator, "%c", FS);
	pszTemp = pszTemp + 1;
	iLen = pszTemp-szBuffer;
	pszTemp = strchr(pszTemp, '|');
	strncpy(pstCPReqDetails[CP_B_EXP_MMYY].elementValue, szBuffer + iLen, 4);			// 4 because MMYY
	sprintf(pstCPReqDetails[CP_B_EXP_MMYY].fieldSeparator, "%c", FS);

	ipresentFlag = 2;       	// changing present flag as manual entry
	iTrackIndicator = 0; 		// setting as 0 for manual entry
	cDESValue = getDESValue(iTrackIndicator, ipresentFlag);
	if(cDESValue < 0)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Data Entry Source Value ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		rv = ERR_INV_FLD;
		return rv;
	}
	else
	{
		sprintf(pstCPReqDetails[CP_A_DATA_ENTRY_SRC].elementValue, "%02d", cDESValue);
	}
	debug_sprintf(szDbgMsg, "%s:returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: getAccnExpFromTrackData
 *
 * Description	: it will copy the account number and exp date from the track data to buffer
 *
 * Input Params	: buffer containing track data,
 * 				  track indicator,
 *				  buffer to be filled with the account num and exp date
 * Output Params:
 * ============================================================================
 */
void getAccnExpFromTrackData(char *pszTrackData, int iTrackIndicator, char *pszAccnExp)
{
	char *	cCurPtr				    = NULL;
	char *	cNxtPtr				    = NULL;
	char    szAccn[19+1]	        = "";
	char    szExp[4+1]	            = "";
	char    szTempExp[4+1]	        = "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(pszTrackData == NULL || pszAccnExp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param is NULL!!!", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return;
	}

	cCurPtr = pszTrackData;

	if(iTrackIndicator == 1) //Track1 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving Account Number and exp from Track1", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		if(!(cCurPtr[0] >= '0' && cCurPtr[0] <= '9')) //If it is not digit then skip that character
		{
			cCurPtr = cCurPtr + 1;
		}

		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '^' sentinel in their track 1 information */
			strcpy(szAccn, cCurPtr);
		}
		else
		{
			memcpy(szAccn, cCurPtr, cNxtPtr - cCurPtr);
		}

		/*skipping card holder name*/
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid track; no Name", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}

		/*copying the exp date*/
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, '\0');
		if((cNxtPtr - cCurPtr) < 4)
		{
			debug_sprintf(szDbgMsg,"%s: Invalid track; no exp dt",__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			memcpy(szTempExp, cCurPtr, 4);
		}
	}
	else if(iTrackIndicator == 2) //Track 2 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving Account Number from Track2", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		cNxtPtr = strchr(cCurPtr, '=');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '=' sentinel in their track 2 */
			strcpy(szAccn, cCurPtr);
		}
		else
		{
			memcpy(szAccn, cCurPtr, cNxtPtr - cCurPtr);
		}

		/* copying exp Date*/
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cNxtPtr, '\0');
		if(cNxtPtr - cCurPtr < 4)
		{
			debug_sprintf(szDbgMsg, "%s: Expiry Date is Missing!!!", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			memcpy(szTempExp, cCurPtr, 4);
		}

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid track to retrieve Account Number And Exp Date!!!", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return;
	}

	/*below operations are to store in MMYY format*/
	strncpy(szExp, szTempExp+2, 2);
	strncat(szExp, szTempExp, 2);

	sprintf(pszAccnExp, "%s|%s|", szAccn, szExp);

	debug_sprintf(szDbgMsg, "%s:returning ", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	return;
}


/*
 * ============================================================================
 * Function Name   : getEMVData
 *
 *
 * Description	 :  This function will fills the All The EMV Tags in the buffer
 *
 * Input Params	 :  buffer needs to be filled with the All The EMV Tags
 * 				    buffer containing all EMV tags
 *
 * Output Params   : SUCCESS/FAILURE
 * ============================================================================
 */
int getEMVData(CPTRANDTLS_PTYPE pstCPReqDetails, TRANDTLS_PTYPE pstDHIDtls)
{
	int			rv								= SUCCESS;
	int			iLen							= 0;
	char    	szTotallength[3+1]				= "";
	char		szEmvTags[1024]					= "";			//size needs to be checked

#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iLen = strlen(pstCPReqDetails[CP_J_CHIP_CARD_DATA].elementValue);
	strcpy(szEmvTags, pstCPReqDetails[CP_J_CHIP_CARD_DATA].elementValue);
	memset(pstCPReqDetails[CP_J_CHIP_CARD_DATA].elementValue ,0X00 ,sizeof(pstCPReqDetails[CP_J_CHIP_CARD_DATA].elementValue));

	if(iLen > 510)
	{
		rv = ERR_INV_FLD;
		return rv;
	}

	strcpy(pstCPReqDetails[CP_J_CHIP_CARD_DATA].elementValue, "EM");
	sprintf(szTotallength, "%03d", iLen);
	strcat(pstCPReqDetails[CP_J_CHIP_CARD_DATA].elementValue, szTotallength);
	strcat(pstCPReqDetails[CP_J_CHIP_CARD_DATA].elementValue, szEmvTags);

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: fillCPDtlsforEMVParamDwnld
 *
 * Description	: Fills up the required chase details structure for EMV Parameter
 * 				  Download
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required chase details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int fillCPDtlsforEMVParamDwnld(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPHIDetails)
{

	int		rv		       = SUCCESS;
//	static int iBlkSeqVal = 0;


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		/*getting system indicator */
		getSystemInd(pstCPHIDetails[CP_A_SYSTEM_IND].elementValue);

		/*getting Routing Indicator */
		getRoutingInd(pstCPHIDetails[CP_A_ROUTING_IND].elementValue);

		/*getting clientNumber  */
		getClientNum(pstCPHIDetails[CP_A_CLIENT_NUM].elementValue);

		/*getting Merchant Number */
		getMerchantNum(pstCPHIDetails[CP_A_MERCHANT_NUM].elementValue);

		/*getting Terminal Number */
		getTerminalNum(pstCPHIDetails[CP_A_TERMINAL_NUM].elementValue);

		if(!strcmp(gstEMVParamDwnld.szDwnldStat,"F"))
			strcpy(pstCPHIDetails[CP_A_TXN_SQN_FLAG].elementValue,"1");
		else
			strcpy(pstCPHIDetails[CP_A_TXN_SQN_FLAG].elementValue,"2");

		/*getting sequence number */
		getCurrentSequenceNum(pstCPHIDetails[CP_A_SEQ_NUM].elementValue);

		/*getting Transaction class */
		getTxnClass(pstCPHIDetails[CP_A_TXN_CLASS].elementValue);

		strcpy(pstCPHIDetails[CP_A_LAST_REF_NUM].elementValue,gstEMVParamDwnld.szLastRetRef);

		strcpy(pstCPHIDetails[CP_A_DWNLD_STATUS].elementValue,pstDHIDtls[eEMV_DOWNLOAD_STAT].elementValue);

		sprintf(pstCPHIDetails[CP_A_TXN_CODE].fieldSeparator,"%c",FS);
		sprintf(pstCPHIDetails[CP_A_LAST_REF_NUM].fieldSeparator,"%c",FS);
		sprintf(pstCPHIDetails[CP_A_DWNLD_STATUS].fieldSeparator,"%c",FS);

		break;
	}

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}


/*
 * ============================================================================
 * Function Name: getTagsForCompletion
 *
 * Description	: It will parse EMV Tags based on TLV FORMAT and fills appropriate fields
 *
 * Input Params	: structure need to fill with the EMV tags
 * 				  structure containing the EMV tags
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int getTagsForCompletion(TRANDTLS_PTYPE pstDHIDtls, CPTRANDTLS_PTYPE pstCPHIDetails)
{
	int  			 rv 					= SUCCESS;
	char			 szFollowonTags[1024]  	= "";			//size needs to be changed
	char             szResponseTags[256+1]	= "";
	char			 *pszStart				= NULL;
	char			 *pszEnd				= NULL;
	char             *pszTemp				= NULL;

#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szFollowonTags, 0X00, sizeof(szFollowonTags));
	memset(szResponseTags, 0X00, sizeof(szResponseTags));

	strcpy(szResponseTags, pstDHIDtls[eEMV_TAGS_RESP].elementValue);

	rv = parseEMVTags(pstCPHIDetails, szResponseTags, RESPONSE_EMVTAGS);
	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully parsed Response EMV TAGS", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to parse  Response EMV TAGS", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	/* Copy Tags upto Tag 82 , Since after Tag 82 we need to add 8A and 91 */
	pszStart = pstCPHIDetails[CP_J_CHIP_CARD_DATA].elementValue;
	pszEnd = strstr(pstCPHIDetails[CP_J_CHIP_CARD_DATA].elementValue,"8202");

	if(!pszStart || !pszEnd)
	{
		debug_sprintf(szDbgMsg, "%s: Tag 82 not found ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	pszTemp = pszEnd + 8;														// Since Tag 82 is totally 8 bytes long
	strncpy(szFollowonTags,pszStart,(pszEnd-pszStart)+8); 						// Since Tag 82 is totally 8 bytes long
	strcat(szFollowonTags, szResponseTags);

	strcat(szFollowonTags, pszTemp);

	/* FIXME: Currently done under assumption that we wont get issuer script results are not coming from POS
			 * here Hard coding to 9F5B00 which tells No data Present for the 9F5B tag*/
	strcat(szFollowonTags, "9F5B00");

	memset(pstCPHIDetails[CP_J_CHIP_CARD_DATA].elementValue, 0X00 ,sizeof(pstCPHIDetails[CP_J_CHIP_CARD_DATA].elementValue));
	strcpy(pstCPHIDetails[CP_J_CHIP_CARD_DATA].elementValue, szFollowonTags);

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: parseEMVTags
 *
 * Description	: It will parse EMV Tags based on TLV FORMAT and fills appropriate fields
 *
 * Input Params	: structure need to fill with the EMV tags
 * 				  buffer containing the EMV tags
 * 				  integer tells request or response to parse
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int parseEMVTags(CPTRANDTLS_PTYPE pstCPHIDetails, char *szEMVtags, int itype)
{
	int						rv		       			= SUCCESS;
	int 					iLengthParsed 			= 0;
	int 					iTotallength			= 0;
	int						iAscIIBuffSize			= 0;
	int 					iCnt					= 0;
	int                     i8AIndicator			= 0;
	int                     i91Indicator			= 0;
	char 					szTag[7]     			= "";
	char    				szTempchar[64+1]		= "";
	char					szTempHex[64+1]			= "";
	char					szTemp[64]				= "";
	char					szFollowonTags[64+1]  	= "";			//size needs to be changed
	char					*ascIIBuffer			= NULL;
	char					*pszTemp				= NULL;
	char					*pszStartTemp			= NULL;
	char					*pszEndTemp				= NULL;
	char					*pszStartEmvResp		= NULL;
	char 					*ptrTagValue			= NULL;
	unsigned int 			tag;
	unsigned short  		Temp;
	unsigned short 			len;


#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);


	ascIIBuffer = (char *) malloc (sizeof(char)* 1024);
	if( ascIIBuffer == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(ascIIBuffer, 0x00, 1024);
	iAscIIBuffSize = Hex2Bin((unsigned char *)szEMVtags, strlen(szEMVtags)/2, (unsigned char *)ascIIBuffer);

	if(iAscIIBuffSize <= 0)
	{
		debug_sprintf(szDbgMsg, "%s:No tags are present adding default tags ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		//return ERR_INV_FLD;
	}

	iTotallength = iAscIIBuffSize;
	memset(szFollowonTags, 0X00, sizeof(szFollowonTags));
	//storing start for freeing later
	pszStartEmvResp = ascIIBuffer;

	while(iLengthParsed < iAscIIBuffSize)
	{
		pszStartTemp = ascIIBuffer;
		tag = 0;
		tag = *ascIIBuffer++;
		iLengthParsed++;

		if( (tag & 0x1F) == 0x1F )// tag value is atleast 2 bytes
		{
			do {
				tag = tag << 8;
				tag = tag | *ascIIBuffer++;
				iLengthParsed++;
			} while((tag & 0x80) == 0x80);
		}

		//Getting Length
		len= *ascIIBuffer++;
		iLengthParsed += 1;
		if (len & 0x80)
		{
			Temp= len & 0x7f;
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *ascIIBuffer++;
				iLengthParsed += 1;
				Temp--; // Temp should decremented for exiting from the closed loop.
			}
		}

		ptrTagValue = NULL;
		ptrTagValue = ascIIBuffer;

		iLengthParsed += len;
		ascIIBuffer += len;
		pszEndTemp = ascIIBuffer;
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);

		if(itype == REQUEST_EMVTAGS)
		{

			if(!strcmp(szTag,"9F06"))
			{
				iCnt++;
				memset(szTempchar, 0X00, sizeof(szTempchar));
				memset(szTempHex, 0X00, sizeof(szTempHex));
				memcpy(szTempchar, ptrTagValue,len);
				Char2Hex(szTempchar, szTempHex, len);

				len = len*2;      // because we are calculating application identifier in ASCI HEX
				if(len >= 14 && len <= 32)
				{
					strcpy(pstCPHIDetails[CP_J_APP_IDENTIFIER].elementValue, "AI");
					strcat(pstCPHIDetails[CP_J_APP_IDENTIFIER].elementValue, szTempHex);
					sprintf(pstCPHIDetails[CP_J_APP_IDENTIFIER].fieldSeparator, "%c", FS);

				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Failed to get application identifier", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
				}

			}
			else if(!strcmp(szTag,"9F41"))
			{
				iCnt++;
				memset(szTempchar, 0X00, sizeof(szTempchar));
				memset(szTempHex, 0X00, sizeof(szTempHex));
				memset(szTemp, 0X00, sizeof(szTemp));
				memcpy(szTempchar, ptrTagValue,len);
				Char2Hex(szTempchar, szTempHex, len);
				len = len*2;      // because we are calculating application identifier in ASCI HEX
				if(len > 0)
				{
					pszTemp = szTempHex;
					if(len > 6)
					{
						pszTemp = pszTemp +2;
					}
					len = strlen(pszTemp);
					memset(szTemp ,'0', 6 - len);
					strcat(szTemp,pszTemp);
					strcpy(pstCPHIDetails[CP_J_TRAN_SEQ_COUNTER].elementValue, "TS");
					strcat(pstCPHIDetails[CP_J_TRAN_SEQ_COUNTER].elementValue, szTemp);
					sprintf(pstCPHIDetails[CP_J_TRAN_SEQ_COUNTER].fieldSeparator, "%c", FS);

				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Failed to get transaction sequence counter", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
				}

			}
			else if(!strcmp(szTag,"5F34"))
			{
				iCnt++;
				memset(szTempchar, 0X00, sizeof(szTempchar));
				memset(szTempHex, 0X00, sizeof(szTempHex));
				memcpy(szTempchar, ptrTagValue,len);
				Char2Hex(szTempchar, szTempHex, len);

				if(len > 0)
				{
						strcpy(pstCPHIDetails[CP_J_PAN_SEQ_NUMBER].elementValue, "PS");
						strcat(pstCPHIDetails[CP_J_PAN_SEQ_NUMBER].elementValue, szTempHex);
						sprintf(pstCPHIDetails[CP_J_PAN_SEQ_NUMBER].fieldSeparator, "%c", FS);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Failed to get transaction sequence counter", __FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
				}

//				pszTemp = NULL;
//				pszTemp = ptrTagValue + len;
			}
			else
			{
				//do nothing
			}
		}
		else
		{

			if(!strcmp(szTag,"8A"))
			{
				i8AIndicator++;
				memset(szTempchar, 0X00, sizeof(szTempchar));
				memset(szTempHex, 0X00, sizeof(szTempHex));

				strncpy(szTempchar, pszStartTemp, pszEndTemp - pszStartTemp);
				Char2Hex(szTempchar, szTempHex, pszEndTemp - pszStartTemp);

				strcat(szFollowonTags, szTempHex);
			}

			else if(!strcmp(szTag,"91"))
			{
				i91Indicator++;
				memset(szTempchar, 0X00, sizeof(szTempchar));
				memset(szTempHex, 0X00, sizeof(szTempHex));

				strncpy(szTempchar, pszStartTemp, pszEndTemp - pszStartTemp);
				Char2Hex(szTempchar, szTempHex, pszEndTemp - pszStartTemp);

				strcat(szFollowonTags, szTempHex);
			}
		}

		if(iCnt == 3)
		{
			break;
		}
	}



	if(itype == REQUEST_EMVTAGS)
	{
//		memset(szTempchar, 0X00, sizeof(szTempchar));
//		memset(szTempHex, 0X00, sizeof(szTempHex));
//
//		memcpy(szTempchar, pszTemp , iTotallength - iLengthParsed);
//		Char2Hex(szTempchar, szTempHex, iTotallength- iLengthParsed);

		strcpy(pstCPHIDetails[CP_J_CHIP_CARD_DATA].elementValue, szEMVtags);
	}

	if(itype == RESPONSE_EMVTAGS)
	{
		/*if 8A tag is not present in the response tags we are hardcoding it to 8A023030 which means transaction got approved.
		 * because we are storing the tags only if transaction got approved
		 */
		if(i8AIndicator == 0)
		{
			strcat(szFollowonTags,"8A023030");
		}
		/*if 91 Tag not present we hardcoing it to 9100 because it tells no data is there*/
		if(i91Indicator == 0)
		{
			strcat(szFollowonTags, "9100");
		}

		len = strlen(szEMVtags);
		memset(szEMVtags, 0X00, len);
		strcpy(szEMVtags, szFollowonTags);
	}

	free(pszStartEmvResp);

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__,rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;

}



/*============================================================================
Hex2Bin - This is utility function to perform Hex to Binary conversion
on input data
@param - data. Pointer to input hex data
@param - length. number of bytes to convert
@param - dest. Buffer to hold converted data buffer
@return - length.
=============================================================================*/
int Hex2Bin (unsigned char *data, int length, unsigned char *dest)
{
    int c;
    unsigned char ch;
    unsigned char byte;
    for (c = 0; c < length; c++)
    {
        byte = 0;
        ch = *data++;

        if (ch >= '0' && ch <= '9')
        {
            ch -= '0';
        }
        else
        {
            if (ch >= 'A' && ch <= 'F')
            {
                ch -= 'A';
                ch += 10;
            }
            else if (ch >= 'a' && ch <= 'f')
            {
            	ch -= 'a';
            	ch += 10;
            }
            else
                return (-1);
        }

        byte = ch << 4;
        ch = *data++;

        if (ch >= '0' && ch <= '9')
        {
            ch -= '0';
        }
        else
        {
            if (ch >= 'A' && ch <= 'F')
            {
                ch -= 'A';
                ch += 10;
            }
            else if (ch >= 'a' && ch <= 'f')
            {
            	ch -= 'a';
            	ch += 10;
            }
            else
                return (-1);
        }

        byte |= ch;
        *dest = byte;
        dest++;
    }
    return (length);
}


/*This function will convert a character buffer into two byte Hex buff*/
static void  Char2Hex(char *inBuff, char *outBuff, int iLen)
{
	char temp[2 + 1];
	char chHex[] = "0123456789ABCDEF\0";
	int i = 0, iDec;
	while(i < iLen)
	{
		iDec = inBuff[i++];
		memset(temp, 0x00, sizeof(temp));

		temp[1] = chHex[iDec%16];
		iDec = iDec/16;
		temp[0] = chHex[iDec%16];

		strcat(outBuff, temp);
	}
}
