/*
 * cphiComm.c
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <time.h>
#include <sys/timeb.h>	// For struct timeb
#include <pthread.h>
#include <svc.h>
#include <setjmp.h>
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <resolv.h>

#include "CPHI/cphicommon.h"
#include "CPHI/cphiHostCfg.h"
#include "CPHI/cphiCfg.h"
#include "CPHI/utils.h"
#include "DHI_APP/appLog.h"
#include "CPHI/cphiMsgQueue.h"


static int 				msgQID;

#define CA_CERT_FILE	"ca-bundle.crt"
#define POST			"POST /NetConnect/controller"

static struct curl_slist *	headers			= NULL;
static pthread_t			ptKeepAliveIntId;
static CURL *				gptCPCurlHandle = NULL;
static pthread_mutex_t  	gptCPCurlHandleMutex;
static sigjmp_buf			keepAliveJmpBuf;

int 			cleanCURLHandle(CPHI_BOOL);

static int 		chkConn(CURL * );
static void 	frameHeadersForCPHost();
static int      initializeOpenSSLLocks();
static void 	keepAliveTimeSigHandler(int);
static int 		getCURLHandle(HOST_URL_ENUM);
static void * 	keepAliveIntervalTimer(void *);
static void		commonInit(CURL *, CPHI_BOOL, int);
static int 		initializeRespData(RESP_DATA_PTYPE);
static int		initCPCURLHandle(CURL **, HOST_URL_ENUM);
static size_t	saveResponse(void *, size_t, size_t, void *);
static int 		sendDataToCPHost(CURL *, char *, int, char **, int *);


#ifdef DEBUG
/* Static functions' declarations */
static int 		curlDbgFunc(CURL *, curl_infotype, char *, size_t, void *);
#endif
/* we have this global to let the callback get easy access to it */
static pthread_mutex_t *lockarray;

static int gWaitTime = 0;

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		cphiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         cphistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  cphistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	cphiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			cphiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)




/*
 * ============================================================================
 * Function Name: initCPHIFace
 *
 * Description	:
 *
 * Input Params	: Nothing
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initCPHIFace()
{
	int				rv				= SUCCESS;
	int				iKeepAlive		= -1;
	char *			pszCURLVer		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iKeepAlive = getKeepAliveParameter();
	/* Initialize the CURL library */
	curl_global_init(CURL_GLOBAL_ALL);

	/*
	 * Praveen_P1: We have started getting problem with the SAF and Online
	 * transactions together. To make it thread safe, we have followed the
	 * following link
	 * http://curl.haxx.se/libcurl/c/threaded-ssl.html
	 *
	 */
	rv = initializeOpenSSLLocks();

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while initializing OpenSSL Lock Callbacks", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return rv;
	}

	/* Get the CURL library version and print it in the logs */
	pszCURLVer = curl_version();

	/* VDR: TODO: Add a syslog statement */
	debug_sprintf(szDbgMsg, "%s: CURL Ver: %s", __FUNCTION__, pszCURLVer);
	CPHI_LIB_TRACE(szDbgMsg);


	frameHeadersForCPHost();
	//headers = curl_slist_append(headers, "User-Agent:SCA v2.19.x");
	//headers = curl_slist_append(headers, "Content-Type:text/xml");
//	headers = curl_slist_append(headers, "Cache-Control:no-cache");

	if(pthread_mutex_init(&gptCPCurlHandleMutex, NULL))//Initialize the CP curl Handle Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create CP Curl Handle Mutex!", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(iKeepAlive == KEEP_ALIVE_DEFINED_TIME)
	{
		//Open the keep Alive Interval Thread
		debug_sprintf(szDbgMsg, "%s - Starting the keepAliveIntervalThread ...", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		pthread_create(&ptKeepAliveIntId, NULL, keepAliveIntervalTimer, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: postDataToCPHost
 *
 * Description	: This function initializes the curl handler and sends the data
 * 				  to the CP Host
 *
 * Input Params	:	char *
 * 					int
 * 					char **
 * 					int *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int postDataToCPHost(char * req, int reqSize, char ** resp, int * respSize)
{
	int			rv					= SUCCESS;
	int			iCnt				= 0;
	int			iAppLogEnabled		= 0;
	static int	host				= PRIMARY_URL;
	char		szAppLogData[256]	= "";
	char		szAppLogDiag[256]	= "";

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: CP HOST REQ = [%s]", DEVDEBUGMSG,__FUNCTION__, req);
		CPHI_LIB_TRACE(szDbgMsg);
	}
#endif

	while(iCnt < getNumUniqueHosts())
	{
		acquireCPMutexLock(&gptCPCurlHandleMutex, "CP Curl Handle Mutex");
		rv = getCURLHandle(host);

		if(rv == SUCCESS)
		{
			 /* CURL Handler was initialized successfully*/
			rv = sendDataToCPHost(gptCPCurlHandle, req, reqSize, resp, respSize);
			releaseCPMutexLock(&gptCPCurlHandleMutex, "CP Curl Handle Mutex");

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Post Data to the CP Host");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				if(rv == ERR_RESP_TO) //Incase of response timeout we should not try with the next url
				{
					debug_sprintf(szDbgMsg, "%s: Response Time out from the CP host",
											__FUNCTION__);
					CPHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}

			if((rv == SUCCESS) && (*resp))
			{
				cleanCURLHandle(CPHI_FALSE);
				break;
			}

			cleanCURLHandle(CPHI_TRUE); //Closing the current curl handler before trying the next host url
		}
		else
		{
			//If getCURLHandle Fails , then we would not have sent the packet and hence not release the lock. Hence release it here.
			releaseCPMutexLock(&gptCPCurlHandleMutex, "CP Curl Handle Mutex");
		}

		/* If the first host is not working then try the next host*/
		host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trial %d..", __FUNCTION__,iCnt);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);

	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: cleanCURLHandle
 *
 * Description	: This function cleans/closes the opened CURL Handle
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int cleanCURLHandle(CPHI_BOOL iForceClose)
{
	int				rv				= SUCCESS;
	static 	int		iKeepAlive		= -1;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(iKeepAlive == -1)
	{
		iKeepAlive = getKeepAliveParameter();
	}

	acquireCPMutexLock(&gptCPCurlHandleMutex, "CP Curl Handle Mutex");
	debug_sprintf(szDbgMsg, "%s: Enter, gptCPCurlHandle [%p]", __FUNCTION__, gptCPCurlHandle);
	CPHI_LIB_TRACE(szDbgMsg);

	if((iForceClose == CPHI_TRUE) || (iKeepAlive == KEEP_ALIVE_DISABLED))
	{
		if(gptCPCurlHandle != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Cleaning up the CURL handle from if cond", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			curl_easy_cleanup(gptCPCurlHandle);
			gptCPCurlHandle = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No need to clean CURL Handle", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

	}

	debug_sprintf(szDbgMsg, "%s: Cleaning up the CURL handle is Done", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	releaseCPMutexLock(&gptCPCurlHandleMutex, "CP Curl Handle Mutex");

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkHostConn
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int checkHostConn()
{
	int			rv						= SUCCESS;
	int			iCnt					= 0;
	int			iAppLogEnabled			= 0;
	static int	host					= PRIMARY_URL;
	char		szAppLogData[256]		= "";
	char		szAppLogDiag[256]		= "";
	CURL *		curlHandle				= NULL;


#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	while(iCnt < getNumUniqueHosts())
	{
		rv = initCPCURLHandle(&curlHandle, host);

		debug_sprintf(szDbgMsg, "%s: curlHandle [%p]", __FUNCTION__, curlHandle);
		CPHI_LIB_TRACE(szDbgMsg);

		if(rv == SUCCESS)
		{
			/* Curl handler initialized successfully*/

			if(chkConn(curlHandle) != SUCCESS)
			{
				rv = ERR_CONN_FAIL;

				sprintf(szAppLogData, "Failed to Connect to CP Host, Error[CURLE_COULDNT_CONNECT]");
				strcpy(szAppLogDiag, "Please Check The CP Host URLs, Otherwise Contact Chase Paymentech");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);

				//break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host connection available", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
			}
		}

		if(curlHandle != NULL)
		{
			curl_easy_cleanup(curlHandle);
		}

		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Breaking from the loop", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* If the first host is not working then try the next host */
		host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trying again..", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: chkConn
 *
 * Description	:
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int chkConn(CURL * curlHandle)
{
	int				rv				= SUCCESS;
//	long			lCode			= 0L;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
//		curl_easy_setopt(curlHandle, CURLOPT_POST, 0L);
//		curl_easy_setopt(curlHandle, CURLOPT_NOBODY, 1);
//		curl_easy_setopt(curlHandle, CURLOPT_VERBOSE, 1L);
//		curl_easy_setopt(curlHandle, CURLOPT_FOLLOWLOCATION, 1);

		curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 1L);

		rv = curl_easy_perform(curlHandle);
		if(rv == CURLE_OK)
		{
			debug_sprintf(szDbgMsg, "%s: Perform SUCCESS", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Return Value from PERFORM = [%d]",
															__FUNCTION__, rv);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}

		/* Unsetting the connect only option */
		curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 0L);

#if 0
		//rv = curl_easy_getinfo(curlHandle, CURLINFO_HTTP_CONNECTCODE, &lCode);

		rv = curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &lCode);

		debug_sprintf(szDbgMsg, "%s: Return Value RESPONSE CODE = [%d], [%ld]",
													__FUNCTION__, rv, lCode);
		CPHI_LIB_TRACE(szDbgMsg);

		if(rv == 0 && lCode == 200)
		{
			debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host",
																__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else if(rv == 0 && lCode == 404) //if connection is available we are getting this one considering good response for now
		{
			debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host",
																__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else if(rv == 0 && lCode == 412)
		{
			debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host",
					__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Didnt get the good resp code from host"
															, __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}

#endif
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: static void * keepAliveIntervalTimer(void * arg)
 *
 * Description	: Thread to run the keep Alive Interval timer
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void * keepAliveIntervalTimer(void * arg)
{
	int			iKeepAliveInterval				= 0;
	int			iVal							= 0;
	ullong		curTime							= 0;
	ullong		endTime							= 0;

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	/* Add signal handler for SIG_KA_TIME SETTIME */
	if(SIG_ERR == signal(SIG_KA_TIME, keepAliveTimeSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIG_KA_TIME",
																__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Keep Alive Interval Thread Started", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iKeepAliveInterval = getKeepAliveInterval();
	iVal = sigsetjmp(keepAliveJmpBuf, 1);
	if(iVal == 1)
	{
		debug_sprintf(szDbgMsg, "%s: Resetting the Keep Alive Interval Timer",
								__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	curTime = svcGetSysMillisec();
	endTime = curTime + (iKeepAliveInterval * 60 * 1000L);

	while(1)
	{
		while(endTime >= curTime)
		{
			svcWait(100); // Sleep little by little in the loop for KeepAliveInterval Duration
			curTime = svcGetSysMillisec();
		}

		if(gptCPCurlHandle != NULL)
		{
			cleanCURLHandle(CPHI_TRUE);
		}
		svcWait(360000); // keep the thread to run permanently by waiting
	}

	return NULL;
}

/*
 * ============================================================================
 * Function Name: keepAliveTimeSigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void keepAliveTimeSigHandler(int iSigNo)
{
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(iSigNo == SIG_KA_TIME)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIG_KA_TIME delivered", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		siglongjmp(keepAliveJmpBuf, 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown signal", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}
	return;
}

/*
 * ===================================================================================================================
 * Function Name: resetKAIntervalUpdationTimer
 *
 * Description	: Sends the signal to Keep Alive Interval thread to reset the timer once set time command is received
 *
 * Input Params	:
 *
 * Output Params: none
 * =====================================================================================================================
 */
int resetKAIntervalUpdationTimer()
{
	int		iRetVal = FAILURE;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Keep Alive Interval Timer thread id: %d", __FUNCTION__, (int)ptKeepAliveIntId);
	CPHI_LIB_TRACE(szDbgMsg);

	//Check for the thread before killing.
	if(ptKeepAliveIntId != 0)
	{
		iRetVal = pthread_kill(ptKeepAliveIntId, SIG_KA_TIME);
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, iRetVal);
	CPHI_LIB_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getCURLHandle
 *
 * Description	: This function returns the curl Handle according to keep Alive configuration parameter
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getCURLHandle(HOST_URL_ENUM host)
{
	int				rv 				= SUCCESS;
	static 	int		iKeepAlive		= -1;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(iKeepAlive == -1)
	{
		iKeepAlive = getKeepAliveParameter();
	}

	/* Daivik:5/5/2016 - Taking this lock in postCPDataToHost. Because
	 * once we have recieved the curlHandler, we need to send the packet before anyone else can modify it.
	 * If we take the lock only here , there is chance that another thread calls clean CURL Handler and hence set the
	 * global curl handler to NULL.
	 */

	//acquireCPMutexLock(&gptCPCurlHandleMutex, "CP Curl Handle Mutex");

	debug_sprintf(szDbgMsg, "%s: iKeepAlive [%d], gptCPCurlHandle ---- [%p]", __FUNCTION__, iKeepAlive, gptCPCurlHandle);
	CPHI_LIB_TRACE(szDbgMsg);

	while(1)
	{


		if(gptCPCurlHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: CURL Handle Not Available, Create CURL Handle", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = initCPCURLHandle(&gptCPCurlHandle, host);

		}

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: CURL Handle Init Failed", __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			break;
		}

		if(iKeepAlive == KEEP_ALIVE_DEFINED_TIME && (ptKeepAliveIntId != 0))
		{
			rv = pthread_kill(ptKeepAliveIntId, SIG_KA_TIME);
		}

		break;
	}

	//releaseCPMutexLock(&gptCPCurlHandleMutex, "CP Curl Handle Mutex");

	return rv;
}

/*
 * ============================================================================
 * Function Name: initCPCURLHandle
 *
 * Description	: Initializes the CP Curl handler with all the required curl library options.
 *
 * Input Params	:	CURL *
 * 					HOST_URL_ENUM
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */


static int initCPCURLHandle(CURL ** handle, HOST_URL_ENUM hostType)
{
	int					rv					= SUCCESS;
	int					iAppLogEnabled		= 0;
	char				szTmpURL[100]		= "";
	char				szAppLogData[256]	= "";
	char				szAppLogDiag[256]	= "";
	CURL *				locHandle			= NULL;
	CPHI_BOOL			certValdReq 		= CPHI_FALSE;
	HOSTDEF_STRUCT_TYPE	hostDef;
#ifdef DEBUG
	char				szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		memset(&hostDef, 0x00, sizeof(hostDef));

		rv = getCPHostDetails(hostType, &hostDef);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the host details",
																__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			break;
		}

		if (strncmp(hostDef.url, "https", 5) == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Url: %s",__FUNCTION__, hostDef.url);
			CPHI_LIB_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Cert Validation required",
																__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
			certValdReq = CPHI_TRUE;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Posting the request to the URL %s", hostDef.url);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		/* Initialize the handle using the CURL library */
		locHandle = curl_easy_init();
		if(locHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Curl initialization for handle failed",
																__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the URL */
		if(hostDef.portNum != 0)
		{
			sprintf(szTmpURL, "%s:%d", hostDef.url, hostDef.portNum);
			curl_easy_setopt(locHandle, CURLOPT_URL, szTmpURL);

			debug_sprintf(szDbgMsg, "%s: Setting CP URL = [%s]", __FUNCTION__,
																	szTmpURL);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			curl_easy_setopt(locHandle, CURLOPT_URL, hostDef.url);

			debug_sprintf(szDbgMsg, "%s: Setting CP URL = [%s]", __FUNCTION__,
																hostDef.url);
			CPHI_LIB_TRACE(szDbgMsg);

		}

		/* Set the connection timeout */
		curl_easy_setopt(locHandle, CURLOPT_CONNECTTIMEOUT, hostDef.conTimeOut);

		/* Set the total timeout */
		if(hostDef.respTimeOut > 0)
		{
			curl_easy_setopt(locHandle, CURLOPT_TIMEOUT, hostDef.respTimeOut);
		}

		/* Set the other common features */

		commonInit(locHandle, certValdReq, hostType);
		*handle = locHandle;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: commonInit
 *
 * Description	: This function initializes the common curl library options
 *
 *
 * Input Params	:	CURL *
 * 					CPHI_BOOL
 *
 *
 * Output Params: None
 * ============================================================================
 */

static void commonInit(CURL * handle, CPHI_BOOL certValdReq, int hostType)
{

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	CPHI_LIB_TRACE(szDbgMsg);

	 /*Set the NO SIGNAL option, This option is very important to set in case
	 * of multi-threaded applications like ours, because otherwise the libcurl
	 * uses signal handling for the communication and that causes the threads
	 * to go crazy and even crash. Fix done for issue 1659 (Church of LDS AmDocs
	 * Case# 130826-3983)*/
	curl_easy_setopt(handle, CURLOPT_NOSIGNAL, 1L);



//	if(hostType == SERVICEDISCOVERY_URL)
//	{
//		/*Set the HTTP post option*/
//		curl_easy_setopt(handle, CURLOPT_HTTPGET, 1L);
//	}
//	else
//	{
		/*Set the HTTP post option*/
		//curl_easy_setopt(handle, CURLOPT_POST, 1L);
		/* Custom POST Request */
		curl_easy_setopt(handle, CURLOPT_CUSTOMREQUEST, POST);
//	}

//	if(getKeepAliveParameter())
	 /*Add the headers*/
	curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headers);

#ifdef DEBUG
	 /*Add the debug function*/
	curl_easy_setopt(handle, CURLOPT_DEBUGFUNCTION, curlDbgFunc);
#endif

	if (certValdReq == CPHI_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Url starts with https. Doing certificate validation",
				__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		curl_easy_setopt(handle, CURLOPT_SSL_VERIFYPEER, 1L);
		curl_easy_setopt(handle, CURLOPT_SSL_VERIFYHOST, 2L);
	}

/*	 Set the CA certificate*/
	curl_easy_setopt(handle, CURLOPT_CAINFO, CA_CERT_FILE);

	 /*Set the write function*/
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, saveResponse);

	 /*Set the detection as TRUE for HTTP errors*/
	curl_easy_setopt(handle, CURLOPT_FAILONERROR, CPHI_TRUE);

	/* To request using TLS for the transfer */
	curl_easy_setopt(handle, CURLOPT_USE_SSL, CURLUSESSL_NONE);

	/* To set preferred TLS/SSL version */
	//curl_easy_setopt(handle, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

	debug_sprintf(szDbgMsg, "%s: --- returning ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
	return;
}
/*
 * ============================================================================
 * Function Name: saveResponse
 *
 * Description	: This function is used to store the response
 *
 *
 * Input Params	:	void *
 * 					size_t
 * 					size_t
 * 					void *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static size_t saveResponse(void * ptr, size_t size, size_t cnt, void * des)
{
	int				rv				= SUCCESS;
	int				iSizeLeft		= 0;
	int				totLen			= 0;
	int				iReqdLen		= 0;
	char *			cTmpPtr			= NULL;
	RESP_DATA_PTYPE	respPtr			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	while(1)
	{

		respPtr = (RESP_DATA_PTYPE) des;
		totLen = size * cnt;
		iSizeLeft = respPtr->iTotSize - respPtr->iWritten;

		if(totLen > iSizeLeft)
		{
			iReqdLen = (respPtr->iTotSize + totLen - iSizeLeft) + 1 /*one extra buffer for NULL at the end for safety purpose*/;

			cTmpPtr = (char *) realloc(respPtr->szMsg, iReqdLen);
			if(cTmpPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Reallocation of memory FAILED",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(cTmpPtr + respPtr->iWritten, 0x00, iReqdLen - respPtr->iWritten);

			respPtr->iTotSize = iReqdLen;
			respPtr->szMsg = cTmpPtr;
		}

		memcpy(respPtr->szMsg + respPtr->iWritten, ptr, totLen);
		respPtr->iWritten += totLen;

		rv = totLen;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: sendDataToCPHost
 *
 * Description	: This function sends the XML data to the CPHost
 *
 *
 * Input Params	:	CURL *
 * 					char *
 * 					int
 * 					char **
 * 					int *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int sendDataToCPHost(CURL * curlHandle, char * req, int reqSize,
												char ** resp, int * respSize)
{
	int				rv				   	= SUCCESS;
	int				iRetVal				= -1;
	int				iAppLogEnabled		= 0;
	char			szCurlErrBuf[4096] 	= "";
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";
	char			szErrBuf[128]	    = "";
	RESP_DATA_STYPE	stRespData;

#ifdef DEVDEBUG
	char			szDbgMsg[4096+256]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(chkConn(curlHandle) != SUCCESS)
		{
			rv = ERR_CONN_FAIL;

			sprintf(szAppLogData, "Failed to Connect to CP Host, Error[CURLE_COULDNT_CONNECT]");
			strcpy(szAppLogDiag, "Please Check The CP Host URLs, Otherwise Contact Chase Paymentech");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);

			break;
		}

/*		 Initialize the response data*/
		rv = initializeRespData(&stRespData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Initialization of response data FAILED"
																, __FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			break;
		}
		 /*Set some curl library options before sending the data*/
		curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &stRespData);
		
		/* Setting the req and its length only if there are existing*/
		if(req && strlen(req) > 0)
		{
			curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, (void *) req);
			curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, reqSize);
		}
		
		curl_easy_setopt(curlHandle, CURLOPT_ERRORBUFFER, szCurlErrBuf);

		debug_sprintf(szDbgMsg, "%s: Posting the data to the server", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		/*post the data to the server*/
		rv = curl_easy_perform(curlHandle);

		/* Free the headers */
	//	curl_slist_free_all(headers);

		debug_sprintf(szDbgMsg, "%s: curl_easy_perform done, rv = %d", __FUNCTION__, rv);
		CPHI_LIB_TRACE(szDbgMsg);
		if(rv == CURLE_OK)
		{
			*resp = stRespData.szMsg;
			*respSize = stRespData.iWritten;
#ifdef DEVDEBUG
			if(strlen(stRespData.szMsg) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: Response Msg = %s\n", __FUNCTION__,
																		stRespData.szMsg);
				CPHI_LIB_TRACE(szDbgMsg);
			}
#endif
			debug_sprintf(szDbgMsg, "%s: Response Len = [%d]", __FUNCTION__,
														stRespData.iWritten);

			CPHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Request Successfully Posted and Received Response Successfully from CP Host");
				addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSING, szAppLogData, NULL);
			}
		}
		else
		{
			iRetVal = res_init();

			debug_sprintf(szDbgMsg, "%s: res_init returned [%d]", __FUNCTION__, iRetVal);
			CPHI_LIB_TRACE(szDbgMsg);

			switch(rv)
			{
			case CURLE_UNSUPPORTED_PROTOCOL: /* 1 */
				/*
				 * The URL you passed to libcurl used a protocol that this libcurl does not support.
				 * The support might be a compile-time option that you didn't use,
				 * it can be a misspelled protocol string or just a protocol libcurl has no code for.
				 */
				debug_sprintf(szDbgMsg, "%s: protocol not supported by library",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_UNSUPPORTED_PROTOCOL]");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Unsupported Protocol");
				rv = FAILURE;
				break;

			case CURLE_URL_MALFORMAT: /* 3 */
				/*
				 * The URL was not properly formatted.
				 */
				debug_sprintf(szDbgMsg, "%s: URL not properly formatted",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_URL_MALFORMAT]");
					strcpy(szAppLogDiag, "Please Check the CP Host URLs Format, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Invalid URL");
				rv = ERR_CFG_PARAMS;
				break;

			case CURLE_COULDNT_RESOLVE_HOST: /* 6 */
				/*
				 * Couldn't resolve host. The given remote host was not resolved.
				 */
				debug_sprintf(szDbgMsg, "%s: Couldnt resolve server's address",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Resolve Host Address, Error[CURLE_COULDNT_RESOLVE_HOST]");
					strcpy(szAppLogDiag, "Please Check the CP Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Host not resolved");
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_COULDNT_CONNECT: /* 7 */
				/*
				 * Failed to connect() to host or proxy.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to connect server",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Connect to CP Host, Error[CURLE_COULDNT_CONNECT]");
					strcpy(szAppLogDiag, "Please Check The CP Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Connection Failed");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_HTTP_RETURNED_ERROR: /* 22 */
				/*
				 * This is returned if CURLOPT_FAILONERROR is set TRUE
				 * and the HTTP server returns an error code that is >= 400.
				 */
				debug_sprintf(szDbgMsg, "%s: Got HTTP error while posting",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_HTTP_RETURNED_ERROR]");
					strcpy(szAppLogDiag, "Please Check the CP Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				
				sprintf(szErrBuf, ":HTTP Error");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_WRITE_ERROR: /* 23 */
				/*
				 * An error occurred when writing received data to a local file,
				 * or an error was returned to libcurl from a write callback.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to save the response data", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_WRITE_ERROR]");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Curl Writing Failed");

				rv = FAILURE;
				break;

			case CURLE_OUT_OF_MEMORY: /* 27 */
				/*
				 * A memory allocation request failed.
				 * This is serious badness and things are severely screwed up if this ever occurs.
				 */
				debug_sprintf(szDbgMsg, "%s: Facing memory shortage ", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_OUT_OF_MEMORY]");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Out of Memory");
				rv = FAILURE;
				break;

			case CURLE_OPERATION_TIMEDOUT: /* 28 */
				/*
				 * Operation timeout.
				 * The specified time-out period was reached according to the conditions.
				 */
				debug_sprintf(szDbgMsg, "%s: Timeout happened while receiving", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Timed Out While Waiting For Response From CP Host");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				sprintf(szErrBuf, ":Curl Timed out");
				
				rv = ERR_RESP_TO;
				break;

			case CURLE_SSL_CONNECT_ERROR: /* 35 */
				/*
				 * A problem occurred somewhere in the SSL/TLS handshake.
				 *
				 */
				debug_sprintf(szDbgMsg, "%s: SSL/TLS Handshake FAILED", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_SSL_CONNECT_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Network, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}


				sprintf(szErrBuf, ":Connection Error");
				
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_PEER_FAILED_VERIFICATION: /* 51 */
				/*
				 * The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK.
				 */
				debug_sprintf(szDbgMsg,
							"%s: Server's SSL certificate verification FAILED",
							__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_PEER_FAILED_VERIFICATION]");
					strcpy(szAppLogDiag, "Please Check the SSL CA Certificate on the Terminal, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":SSL certificate verification FAILED");
				
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SEND_ERROR: /* 55 */
				/*
				 * Failed sending network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to post the request data",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_SEND_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Network, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Posting FAILED");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_RECV_ERROR: /* 56 */
				/*
				 * Failure with receiving network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to receive the response",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_RECV_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Network, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Receiving FAILED");
				rv = ERR_RESP_TO;
				break;

			case CURLE_SSL_CACERT: /* 60 */
				/*
				 * Peer certificate cannot be authenticated with known CA certificates.
				 */
				debug_sprintf(szDbgMsg,
								"%s: Couldnt authenticate peer certificate",
																__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_SSL_CACERT]");
					strcpy(szAppLogDiag, "Please Check the CA Ceritificate File, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Couldnt authenticate peer certificate");
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SSL_CACERT_BADFILE: /* 77 */
				/*
				 * Problem with reading the SSL CA cert (path? access rights?)
				 */
				debug_sprintf(szDbgMsg, "%s: Cant find SSL CA certificate [%s]",
												__FUNCTION__, CA_CERT_FILE);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_SSL_CACERT_BADFILE]");
					strcpy(szAppLogDiag, "Please Check SSL CA Certificate Path, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				
				sprintf(szErrBuf, ":Certificate file not found");

				rv = ERR_CFG_PARAMS;
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Error [%d] occured while posting",
															__FUNCTION__, rv);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to CP Host, Error[CURLE_UNSUPPORTED_PROTOCOL]");
					strcpy(szAppLogDiag, "Please Check The CP Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;
			}

			 /*Deallocate the allocated memory*/
			if(stRespData.szMsg != NULL)
			{
				free(stRespData.szMsg);
				stRespData.szMsg = NULL;
			}
#ifdef DEVDEBUG
			if(strlen(szDbgMsg) < 5000)
			{
				debug_sprintf(szDbgMsg, "%s: Lib Curl Error [%s]",__FUNCTION__, szCurlErrBuf);
				CPHI_LIB_TRACE(szDbgMsg);
			}
#endif
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: initializeRespData
 *
 * Description	: This function initializes the structure response structure
 *
 *
 * Input Params	: RESP_DATA_PTYPE
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int initializeRespData(RESP_DATA_PTYPE stRespPtr)
{
	int		rv				= SUCCESS;
	char *	cTmpPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	cTmpPtr = (char *) malloc(4096 * sizeof(char));
	if(cTmpPtr != NULL)
	{
		/* Initialize the memory */
		memset(cTmpPtr, 0x00, 4096 * sizeof(char));
		memset(stRespPtr, 0x00, sizeof(RESP_DATA_STYPE));

		/* Assign the data */
		stRespPtr->iTotSize = 4096;
		stRespPtr->iWritten = 0;
		stRespPtr->szMsg	= cTmpPtr;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: timeoutReversalThread
 *
 * Description	: This thread will receive the rc host request packet and tries
 * 					to post the packet to the host twice. It ignores after trying for
 * 					two times.
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
#if 0
static void * emvKeyStatusThread(void * arg)
{
	int				rv 					= SUCCESS;
	int				iAppLogEnabled		= 0;
	int		  		iNextDuration 		= 0;

#ifdef DEBUG
	char            szDbgMsg[1024]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	if(getEmvKeyStatdur())
	{
		iNextDuration = getEmvKeyStatdur();
		debug_sprintf(szDbgMsg, "%s: iNextDur=%d", __FUNCTION__,iNextDuration);
		CPHI_LIB_TRACE(szDbgMsg);
		iNextDuration = iNextDuration * 60;

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: iNextDur Not Set, Setting to 24 Hours", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		iNextDuration = (24 * 60 * 60) + 1000; //Added 1000 secs more just to make that date changes
	}
	while(1)
	{
		rv = checkEMVKeysStat();
		svcWait(iNextDuration*1000);
		debug_sprintf(szDbgMsg, "%s: rv:%d", __FUNCTION__,rv);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	return NULL;
}
#endif

/*
 * ============================================================================
 * Function Name: timeoutReversalThread
 *
 * Description	: This thread will receive the rc host request packet and tries
 * 					to post the packet to the host twice. It ignores after trying for
 * 					two times.
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
static void * timeoutReversalThread(void * arg)
{
	int				rv 					= SUCCESS;
	int				iRetryCount			= 0;
	int				iCPReqLen			= 0;
	int				iCPRespLen			= 0;
	int				iWaitTime			= 0;
	int				iAppLogEnabled		= 0;
	char			szTemp[10]			= "";
	char*			szStartTemp  		= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";
	char*			pszCPReq			= NULL;
	char*			pszCPResp			= NULL;
	time_t			tStartEpochTime 	= 0;
	time_t 			tEndEpochTime  		= 0;
	time_t 			tDiffDeconds    	= 0;

#ifdef DEBUG
	char            szDbgMsg[1024]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- CP TOR-----", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: MSG Queue Id is [%d]", __FUNCTION__, msgQID);
	CPHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		debug_sprintf(szDbgMsg, "%s: Waiting for the request", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		//Read data from the message queue
		if(msgrcv(msgQID, &pszCPReq, sizeof(char**), 0, 0) == -1) // IPC system call
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Receive Data From MSG Queue [%d] [%s]", __FUNCTION__, errno, strerror(errno));
			CPHI_LIB_TRACE(szDbgMsg);

			continue; //TODO: Check whether is it valid to do continue.
		}

		iCPReqLen = strlen(pszCPReq);

		/* Code for waiting for total of 40 seconds after the first timeout reversal*/
		if(gWaitTime > 0)
		{
			svcWait(gWaitTime*1000);
			gWaitTime = 0;
		}

		/* Posting the data to the server*/
		while(iRetryCount++ < 2)
		{
			debug_sprintf(szDbgMsg, "%s: Posting the time out reversal request for %d time using CP TOR", __FUNCTION__, iRetryCount+1);
			CPHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Updating Reversal(TOR) Request");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_REQUEST, pszCPReq, NULL);
			}

			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Posting Reversal(TOR) Request for %d time", iRetryCount+1);
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			rv = postDataToCPHost(pszCPReq, iCPReqLen, &pszCPResp, &iCPRespLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				CPHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed to post data to CP Host");
					strcpy(szAppLogDiag, "Please check the network connection");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_CP_RESPONSE, pszCPResp, NULL);
				}

				pszCPResp = pszCPResp + 1;
				//Action Code
				szStartTemp = pszCPResp;

				memset(szTemp, 0x00, sizeof(szTemp));

				strncpy(szTemp, szStartTemp, 1);
				if(strcmp(szTemp, "A") == 0)
				{
					if(iAppLogEnabled)
					{
						strcpy(szAppLogData, "Approved.Successful Timeout Reversal Transaction");
						addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSING, szAppLogData, NULL);

						szStartTemp += 2;
						memset(szTemp, 0x00, sizeof(szTemp));
						strncpy(szTemp, szStartTemp, 6);

						sprintf(szAppLogData, "Authorization Code :[%s]", szTemp);
						addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSING, szAppLogData, NULL);
					}
				}
				else
				{
					//szResult = strdup("ERROR");
					if(iAppLogEnabled)
					{
						strcpy(szAppLogData, "Error");
						strcpy(szAppLogDiag, "Failure Timeout Reversal Transaction");
						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);

						szStartTemp += 2;

						memset(szTemp, 0x00, sizeof(szTemp));
						strncpy(szTemp, szStartTemp, 6);
						sprintf(szAppLogData, "Error Code/ HostResponse Code:[%s]", szTemp);
						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
					}
				}
			}

			if(rv != SUCCESS)
			{
				if(rv == ERR_RESP_TO)
				{
					if(iRetryCount < 2)
					{
						/* Setting the end time*/
						tEndEpochTime = time(NULL);

						debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
						CPHI_LIB_TRACE(szDbgMsg);

						tDiffDeconds  = tEndEpochTime - tStartEpochTime;

						debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
						CPHI_LIB_TRACE(szDbgMsg);

						iWaitTime = 40 - tDiffDeconds;

						debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
						CPHI_LIB_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Received Response Time Out ");
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Waitig For %d Seconds To Send Time Out Reversal Request", iWaitTime);
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iWaitTime > 0)
						{
							svcWait(iWaitTime*1000);
						}
					}

					if(pszCPResp != NULL)
					{
						free(pszCPResp);
						pszCPResp = NULL;
					}
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Successfully posted the time out reversal request", __FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}

		if(pszCPReq != NULL)
		{
			free(pszCPReq);
			pszCPReq = NULL;
		}
		iRetryCount	= 0;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	return NULL;
}

#if 0
/*
 * ============================================================================
 * Function Name: createEMVKeyStatThread
 *
 * Description	: This function handles creation of the thread which periodically checks
 * 				  for any available EMV Keys for update.
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int createEMVKeyStatThread()
{
	int			rv 		= SUCCESS;
	pthread_t	thId		= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	rv = pthread_create(&thId, NULL, emvKeyStatusThread, NULL );
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create time out reversal thread", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Created thread for EMV Key Status", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);


	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);
	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: createTimeoutReversalThread
 *
 * Description	: This function is responsible for creating the timeout reversal
 * 					thread and create the message queues which will be used for
 *					sending the receiving the request packet.
 * Input Params	:
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int createTimeoutReversalThread()
{
	int			rv 		= SUCCESS;
	key_t 		msgQKey = 1234;
	pthread_t	thId		= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/* Create a msg queue to send/receive data to print the application logs */
	debug_sprintf(szDbgMsg, "%s: Creating Queue for receving the data for timeout reversal", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	msgQID = createMsgQueue(msgQKey);
	if(msgQID < 0)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create message queue for time out reversal", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		return FAILURE;
	}

	//Its very important to flush the existing data in the message queue.
	flushMsgQueue(msgQID);

	//Display Message Queue Information
	rv = displayMsgQueueInfo(msgQID);
	if( rv != FAILURE)
	{
		debug_sprintf(szDbgMsg, "%s: Current Number of messages on specified queue: %d", __FUNCTION__, rv);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	rv = pthread_create(&thId, NULL, timeoutReversalThread, NULL );
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create time out reversal thread", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Created thread for Time out reversal", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);


	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: retryTimeoutReversal
 *
 * Description	: This function is responsible for copying the request data
 * 					and passing it to the thread for the timout reversal
 *
 * Input Params	:
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void retryTimeoutReversal(char* pszMsg, int iWaitTime)
{
	int			rv 			= SUCCESS;
	char*		pszCPReq	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	if(pszMsg != NULL)
	{
		/* Taking a duplicate of the data so that there will not be any double free error*/
		pszCPReq = strdup(pszMsg);
	}
	else
	{
		return;
	}

	if(iWaitTime > 0)
	{
		gWaitTime = iWaitTime;
	}

	/*Add Application Log Data to the Message Queue*/
	rv = msgsnd(msgQID, &pszCPReq, sizeof(char**), IPC_NOWAIT);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to send Data to Message Queue, [%d] [%s] ",__FUNCTION__, errno, strerror(errno));
		CPHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: lock_callback
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static void lock_callback(int mode, int type, char *file, int line)
{
#ifdef DEBUG
	//char	szDbgMsg[512]		= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//CPHI_LIB_TRACE(szDbgMsg);

  (void)file;
  (void)line;
  if (mode & CRYPTO_LOCK)
  {
	  //debug_sprintf(szDbgMsg, "%s: RC Mutex Locking", __FUNCTION__);
	  //CPHI_LIB_TRACE(szDbgMsg);
	  pthread_mutex_lock(&(lockarray[type]));
  }
  else
  {
	  //debug_sprintf(szDbgMsg, "%s:  RC Mutex Un-Locking", __FUNCTION__);
	  //CPHI_LIB_TRACE(szDbgMsg);

	  pthread_mutex_unlock(&(lockarray[type]));
  }

  //debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
  //CPHI_LIB_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: thread_id
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static unsigned long thread_id(void)
{
  unsigned long ret;

#ifdef DEBUG
	//char	szDbgMsg[512]		= "";
#endif

  ret=(unsigned long)pthread_self();

//  debug_sprintf(szDbgMsg, "%s: Returning [%ld]", __FUNCTION__, ret);
//  CPHI_LIB_TRACE(szDbgMsg);

  return(ret);
}

/*
 * ============================================================================
 * Function Name: initializeOpenSSLLocks
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static int initializeOpenSSLLocks()
{
	int		rv					= SUCCESS;
	int		iIndex				= 0;

#ifdef DEBUG
	char	szDbgMsg[512]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	lockarray=(pthread_mutex_t *)OPENSSL_malloc(CRYPTO_num_locks() *
	                                            sizeof(pthread_mutex_t));

	 for (iIndex =0; iIndex < CRYPTO_num_locks(); iIndex++)
	 {
	    pthread_mutex_init(&(lockarray[iIndex]),NULL);
	 }

	 CRYPTO_set_id_callback((unsigned long (*)())thread_id);
	 CRYPTO_set_locking_callback((void (*)())lock_callback);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

#ifdef DEBUG
/*
 * ============================================================================
 * Function Name: curlDbgFunc
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static int curlDbgFunc(CURL * handle, curl_infotype infoType, char *msg,
														size_t size, void * arg)
{
	char	szDbgMsg[4096]	= "";
	switch(infoType)
	{
	case CURLINFO_TEXT:
		sprintf(szDbgMsg, "%s: info-text: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_HEADER_IN:
		sprintf(szDbgMsg, "%s: info-header in: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_HEADER_OUT:
		sprintf(szDbgMsg, "%s: info-header out: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_DATA_IN:
		sprintf(szDbgMsg, "%s: info-data in: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_DATA_OUT:
		sprintf(szDbgMsg, "%s: info-data out: %s", __FUNCTION__, msg);
		break;

	default:
		sprintf(szDbgMsg, "%s: info-default: %s", __FUNCTION__, msg);
		break;
	}

	CPHI_LIB_TRACE(szDbgMsg);

	return 0;
}
#endif

void frameHeadersForCPHost()
{
	char szGetString[512]	= "";
	char szTemp[512]		= "";
#ifdef DEBUG
	char	szDbgMsg[512]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/* Getting Merchant ID */
	strcpy(szGetString, "Auth-MID: ");
	getMerchantNum(szTemp);
	strcat(szGetString, szTemp);
	debug_sprintf(szDbgMsg, "%s: MID: %s", __FUNCTION__, szGetString);
	CPHI_LIB_TRACE(szDbgMsg);

	headers = curl_slist_append(headers, szGetString);
	/* Getting Terminal ID */
	memset(szTemp, 0x00, sizeof(szTemp));
	memset(szGetString, 0x00, sizeof(szGetString));
	strcpy(szGetString, "Auth-TID: ");
	getTerminalNum(szTemp);
	strcat(szGetString, szTemp);
	debug_sprintf(szDbgMsg, "%s: TID: %s", __FUNCTION__, szGetString);
	CPHI_LIB_TRACE(szDbgMsg);
	headers = curl_slist_append(headers, szGetString);
	/* Getting User name */
	memset(szTemp, 0x00, sizeof(szTemp));
	getUserName(szTemp);
	debug_sprintf(szDbgMsg, "%s: User name: %s", __FUNCTION__, szTemp);
	CPHI_LIB_TRACE(szDbgMsg);
	headers = curl_slist_append(headers, szTemp);
	/* Getting Password */
	memset(szTemp, 0x00, sizeof(szTemp));
	getPassword(szTemp);
	debug_sprintf(szDbgMsg, "%s: Password: %s", __FUNCTION__, szTemp);
	CPHI_LIB_TRACE(szDbgMsg);
	headers = curl_slist_append(headers, szTemp);

	headers = curl_slist_append(headers, "Stateless-Transaction: true");
	headers = curl_slist_append(headers, "Header-Record: false");
	headers = curl_slist_append(headers, "Content-type: UTF197/HCS");
}

