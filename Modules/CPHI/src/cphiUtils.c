/*
 * cphiUtils.c
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <svcsec.h>
#include <sys/timeb.h>	// For struct timeb
#include <sys/stat.h>
#include <pthread.h>
#include <svc.h>


#include "CPHI/cphicommon.h"
#include "CPHI/utils.h"

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		cphiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         cphistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  cphistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	cphiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			cphiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)



#ifdef DEBUG
char chCPHIDebug 	= NO_DEBUG;
char chCPHIMemDebug  = NO_DEBUG;
/*
 * ============================================================================
 * Function Name: CPHI_LIB_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void CPHI_LIB_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char *			pszMsgPrefix	= "CPHI";
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chCPHIDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chCPHIDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chCPHIDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: CPHI_LIB_MEM_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void CPHI_LIB_MEM_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char *			pszMsgPrefix	= "CPHI";
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chCPHIMemDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chCPHIMemDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chCPHIMemDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: setupCPHIDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupCPHIDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chCPHIDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(CPHI_DEBUG)) == NULL)
	{
		chCPHIDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chCPHIDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chCPHIDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chCPHIDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chCPHIDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}

/*
 * ============================================================================
 * Function Name: setupCPHIMemDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupCPHIMemDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chCPHIMemDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(CPHI_MEM_DEBUG)) == NULL)
	{
		chCPHIMemDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chCPHIMemDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chCPHIMemDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chCPHIMemDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chCPHIMemDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}

/*
 * ============================================================================
 * Function Name: closeupDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupDebug(void)
{
	if(chCPHIDebug == ETHERNET_DEBUG)
	{
		iCloseDebugSocket();
	}

	return;
}

/*
 * ============================================================================
 * Function Name: closeupMemDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupMemDebug(void)
{
	if(chCPHIMemDebug == ETHERNET_DEBUG)
	{
		iCloseDebugSocket();
	}

	return;
}

/*
 * ============================================================================
 * Function Name: setCPHIDebugParamsToEnv
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int setCPHIDebugParamsToEnv(int * piDbgParamSet)
{
	int 	rv 					= SUCCESS;
	char	szParamValue[20]	= "";
	int		iLen;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	iLen = sizeof(szParamValue);

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "CPHI_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting CPHI_DEBUG variable[%s=%s]", __FUNCTION__, "CPHI_DEBUG", szParamValue);
		CPHI_LIB_TRACE(szDbgMsg);

		setenv("CPHI_DEBUG", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "CPHI_MEM_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting CPHI_MEM_DEBUG variable[%s=%s]", __FUNCTION__, "CPHI_MEM_DEBUG", szParamValue);
		CPHI_LIB_TRACE(szDbgMsg);

		setenv("CPHI_MEM_DEBUG", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGIP", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGIP variable[%s=%s]", __FUNCTION__, "DBGIP", szParamValue);
		CPHI_LIB_TRACE(szDbgMsg);

		setenv("DBGIP", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGPRT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGPRT variable[%s=%s]", __FUNCTION__, "DBGPRT", szParamValue);
		CPHI_LIB_TRACE(szDbgMsg);

		setenv("DBGPRT", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DEBUGOPT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DEBUGOPT variable[%s=%s]", __FUNCTION__, "DEBUGOPT", szParamValue);
		CPHI_LIB_TRACE(szDbgMsg);

		setenv("DEBUGOPT", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);
	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: getHILibVersion
 *
 *
 * Description	: This function will give the Version Number and Build Number
 *
 *
 * Input Params	: Version Number and Build Number
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getHILibVersion(char *szLibVersion, char *szHIName)
{
	sprintf(szLibVersion, "%s|%s", CPHI_LIB_VERSION, CPHI_BUILD_NUMBER);
	sprintf(szHIName, "%s", "Chase Paymentech");
}

/*
 * ============================================================================
 * Function Name: doesFileExist
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int doesFileExist(const char *szFullFileName)
{
	int			rv			= SUCCESS;
	struct stat	fileStatus;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	memset(&fileStatus, 0x00, sizeof(fileStatus));

	rv = stat(szFullFileName, &fileStatus);
	if(rv < 0)
	{
		if(errno == EACCES)
		{
			debug_sprintf(szDbgMsg, "%s: Not enough search permissions for %s",
												__FUNCTION__, szFullFileName);
		}
		else if (errno == ENAMETOOLONG)
		{
			debug_sprintf(szDbgMsg, "%s: Name %s is too long", __FUNCTION__,
																szFullFileName);
		}
		else
		{
			/* For ENOENT/ENOTDIR/ELOOP */
			debug_sprintf(szDbgMsg,
						"%s: File - [%s] (Name not correct)/(file doesnt exist)"
												, __FUNCTION__, szFullFileName);
		}
		CPHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		/* Check if the file is a regular file */
		if(S_ISREG(fileStatus.st_mode))
		{
			/* Check if the file is not empty */
			if(fileStatus.st_size <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: File [%s] is empty", __FUNCTION__,
																szFullFileName);
				CPHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error: File [%s] is not a regular file"
												, __FUNCTION__, szFullFileName);
			CPHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: acquireMutexLock
 *
 * Description	: This function is used to acquire a mutex for sychronization
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void acquireCPMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_lock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to acquire %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: releaseMutexLock
 *
 * Description	: This function is used to release an already acquired mutex
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void releaseCPMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_unlock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to release %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: findLRC
 *
 * Description	: The purpose of this function is to calculate the LRC of the
 * 					character string data passed as argument.
 *
 * Input Params	: data -> character data for which the LRC is to be calculated
 * 				  dataLen -> length of the data
 *
 * Output Params: LRC value
 * ============================================================================
 */
uchar findLrc(char *data, int dataLen)
{
	unsigned char	lrcChar	= 0;

	while(dataLen--)
	{
		lrcChar	^= *data++;
	}

	return lrcChar;
}


