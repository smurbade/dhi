/*
 * cphiInit.c
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <syslog.h>
#include <svc.h>

#include "cphi/cphicommon.h"
#include "cphi/cphi.h"
#include "dhi_app/appLog.h"
#include "cphi/cphiGroups.h"

extern int loadCPHIConfigParams();
extern int initCPHIFace();
extern int SaveDebugLogs();
extern int createHashTableForCPFields();
extern int	cleanCURLHandle(CPHI_BOOL);
/* Static Functions */
#ifdef DEBUG
static int giSaveDebugLogs = 0;
static int giSaveMemDebugLogs = 0;
static void sigHandler(int);
static void setupSigHandlers();
#endif



void initEMVDwnloadParams()
{
	gstEMVFIlst.iNumFbNodes = 0;
	gstEMVFIlst.pstFILst = NULL;
	gstEMVFLlst.iNumFlrLmtNodes = 0;
	gstEMVFLlst.pstFLLst = NULL;
	gstEMVParamDwnld.iKeyUpdAvl = 0;
	strcpy(gstEMVParamDwnld.szLastRetRef,"00000000");
	strcpy(gstEMVParamDwnld.szDwnldStat,"I");
	gstEMVPkLst.iMaxPublicKeys = 0;
	gstEMVPkLst.pstKeysLst = NULL;

	return;
}

/*
 * ============================================================================
 * Function Name: initHILib
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int initHILib()
{
	int		rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szErrMsg[256]   	= "";
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	int		iDbgParmsSet						= 0;
	char	szDbgMsg[2560]	= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Initializing Chase Paymentech Host Interface Library");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 1: Initialize the debugger.
	 * ---------------------------------
	 */

#ifdef DEBUG
	setupCPHIDebug(&giSaveDebugLogs);
	setupCPHIMemDebug(&giSaveMemDebugLogs);
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Lib Ver:[%s]", __FUNCTION__, CPHI_LIB_VERSION);
	CPHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Lib Build Number:[%s]", __FUNCTION__, CPHI_BUILD_NUMBER);
	CPHI_LIB_TRACE(szDbgMsg);

#ifdef DEBUG
	 //Setting Signal handlers for debugging purpose
	setupSigHandlers();
#endif

#ifdef DEBUG

	/*
	 * ------------------------------------
	 * STEP 2: Setting the Debug config params to environment.
	 * ------------------------------------
	 */

	rv = setCPHIDebugParamsToEnv(&iDbgParmsSet);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to set debug params to environment",
				__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to set debug params to environment");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		while(1)
		{
			svcWait(10);
		}
	}
	else
	{
		if(iDbgParmsSet == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Debug parameters are reset to environment",
							__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);

			giSaveDebugLogs = 0; //testing purpose
			giSaveMemDebugLogs = 0;

			/* Close the debug socket before updating with new debug env params */
			closeupDebug();
			closeupMemDebug();

			svcWait(3000);

			//Setting debug with new environment variables
			setupCPHIDebug(&giSaveDebugLogs);
			setupCPHIMemDebug(&giSaveMemDebugLogs);


			svcWait(3000);

			debug_sprintf(szDbgMsg, "%s: giSaveDebugLogs[%d], giSavememDebugLogs[%d] ",__FUNCTION__, giSaveDebugLogs, giSaveMemDebugLogs);
			CPHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Reseting DEBUG params to envinorment is not required",
										__FUNCTION__);
			CPHI_LIB_TRACE(szDbgMsg);
		}
	}
#endif

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "CPHI Library Version:[%s], CPHI Libray Build Number:[%s]", CPHI_LIB_VERSION, CPHI_BUILD_NUMBER);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
	}



	/*
	 * ------------------------------------
	 * STEP 3: Loading CPHI config params
	 * ------------------------------------
	 */


	rv = loadCPHIConfigParams();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to initialize CPHI config parameters", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Failed To Load CPHI Configuration Parameters");
			strcpy(szAppLogDiag, "Please Check The CPHI Configuration");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}

		return rv;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Successfully initialized CPHI config parameters", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	rv = initCPHIFace();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to initialize CPHI Interface", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		return rv;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Successfully initialized CPHI Interface", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	/*
	 * ----------------------------------------------------
	 * STEP 4: Creating hash table for parsing Response
	 * ----------------------------------------------------
	 */

	rv = createHashTableForCPFields();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create Hash Table for DHI Fields",
				__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to create Hash Table for DHI Fields");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Create Hash Table");
			strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}
		return rv;
	}
	/*
	 * ----------------------------------------------------
	 * STEP 5: Creating Timeout reversal thread
	 * ----------------------------------------------------
	 */
	rv = createTimeoutReversalThread();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create thread for timeout reversal",
				__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to create thread for timeout reversal");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "FAILED to create thread for tiemout reversal");
			strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}
		return rv;
	}

	/*
	 * --------------------------------------------------------------------------------
	 * STEP 6 : Initialize the Global EMV Structure required during parameter Download
	 * ------------------------------------------------------------------------------
	 */
	initEMVDwnloadParams();

	return rv;

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Initialized Chase Paymentech Host Interface Library Successfully");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, NULL);
	}
}

/*
 * ============================================================================
 * Function Name: cphiMalloc
 *
 * Description	: Wrapper for malloc, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *cphiMalloc(unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	void *		tmpPtr				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = malloc(iSize);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaMalloc", tmpPtr, iSize, pszCallerFuncName, iLineNum);
		CPHI_LIB_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaMalloc");
		CPHI_LIB_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: cphistrdup
 *
 * Description	: Wrapper for strdup, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern char *cphistrdup(char *ptr , int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	char*		   		tmpPtr				= NULL;
	unsigned int        iSize 				= 0;

	iSize= strlen(ptr);

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = strdup(ptr);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaMalloc", tmpPtr, iSize, pszCallerFuncName, iLineNum);
		CPHI_LIB_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaMalloc");
		CPHI_LIB_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: cphistrndup
 *
 * Description	: Wrapper for strndup, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern char *cphistrndup(char *ptr , unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	char *		tmpPtr				= NULL;
//	unsigned int         iSize 				= 0;

//	iSize= strlen(ptr);

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = strndup(ptr, iSize);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaMalloc", tmpPtr, iSize, pszCallerFuncName, iLineNum);
		CPHI_LIB_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaMalloc");
		CPHI_LIB_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}
/*
 * ============================================================================
 * Function Name: cphiReAlloc
 *
 * Description	: Wrapper for realloc, will print num of bytes and starting
 * 					and original address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *cphiReAlloc(void *ptr, unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalReallocMem= 0;
	void *		tmpPtr				= NULL;
	char		szOrigAddr[16]		= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//Storing Original Address
	memset(szOrigAddr, 0x00, sizeof(szOrigAddr));
	sprintf(szOrigAddr, "%p", ptr);

	tmpPtr = realloc(ptr, iSize);

	if(tmpPtr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] OrigStartAddr[%s] NewStartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaReAlloc", szOrigAddr, tmpPtr, iSize, pszCallerFuncName, iLineNum);
		CPHI_LIB_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaReAlloc");
		CPHI_LIB_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: cphiFree
 *
 * Description	: Wrapper for free, will print going to be free
 * 					address also and make the freed pointer points to NULL
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void cphiFree(void **pptr, int iLineNum, char * pszCallerFuncName)
{
	//size_t *sizePtr					= ((size_t *)ptr) - 1;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(*pptr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] FreedAddr[%p] called by [%s] from Line[%d]",
				"scaFree", *pptr, pszCallerFuncName, iLineNum);
		CPHI_LIB_MEM_TRACE(szDbgMsg);
		free(*pptr);
		*pptr = NULL;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: [MEMDEBUG] NULL params Passed ", "scaFree");
		CPHI_LIB_MEM_TRACE(szDbgMsg);
	}
}

#ifdef DEBUG

static void catch_stop()
{
}


/*
 * ============================================================================
 * Function Name: setupSigHandlers
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */

static void setupSigHandlers()
{
	char	szDbgMsg[256]	= "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	/* Set up signal handlers */
	if(SIG_ERR == signal(SIGABRT, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGABRT",
						__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGSEGV, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSEGV",
						__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGILL, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGILL",
						__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGKILL, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGKILL",
						__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGSTOP, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSTOP",
						__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGTERM, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGTERM",
						__FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	return;
}


/*
 * ============================================================================
 * Function Name: sigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */

static void sigHandler(int iSigNo)
{
	char	szDbgMsg[256]	= "";
	struct 	sigaction 		setup_action;
	sigset_t block_mask;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	pthread_t self = pthread_self();

	debug_sprintf(szDbgMsg, "%s: threadId=%lu", __FUNCTION__, (unsigned long)self);
	CPHI_LIB_TRACE(szDbgMsg);

	sigemptyset(&block_mask);

	sigaddset(&block_mask, SIGABRT);
	sigaddset(&block_mask, SIGSEGV);
	sigaddset(&block_mask, SIGILL);
	sigaddset(&block_mask, SIGPIPE);

	setup_action.sa_handler = catch_stop;
	setup_action.sa_mask = block_mask;
	setup_action.sa_flags = 0;
	sigaction(iSigNo, &setup_action, NULL);

	switch(iSigNo)
	{
	case SIGABRT:
		sprintf(szDbgMsg, "%s: SIGABRT signal received", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(CPHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGSEGV:
		sprintf(szDbgMsg, "%s: SIGSEGV signal received", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(CPHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGILL:
		sprintf(szDbgMsg, "%s: SIGILL signal received", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(CPHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGKILL:
		sprintf(szDbgMsg, "%s: SIGKILL signal received", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(CPHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGSTOP:
		sprintf(szDbgMsg, "%s: SIGSTOP signal received", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(CPHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGTERM:
		sprintf(szDbgMsg, "%s: SIGTERM signal received", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(CPHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGPIPE:
		sprintf(szDbgMsg, "%s: SIGPIPE signal received", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);
		cleanCURLHandle(CPHI_TRUE);
		break;

	default:
		sprintf(szDbgMsg, "%s: Signal no = [%d]", __FUNCTION__, iSigNo);
		CPHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);
	}

	return;
}
#endif

