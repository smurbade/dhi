/*
 * cphash.c
 *
 *  Created on: 14-July-2015
 *      Author: BLR_SCA_TEAM
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#include "CPHI/cphiCommon.h"
#include "CPHI/utils.h"
#include "CPHI/cphash.h"
#include "CPHI/cphiGroups.h"
#include "CPHI/cphiTokenResp.inc"

static HASHLST_PTYPE hashtab[HASHSIZE];  // pointer table

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		cphiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         cphistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  cphistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	cphiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			cphiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)



/*
 * ============================================================================
 * Function Name: createHashTableForCPFields
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.ur1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	createHashTableForCPFields()
{
	int				rv				= SUCCESS;
	int				iTotalCnt		= 0;
	int				iCount			= 0;
	HASHLST_PTYPE 	pstHashLst		= NULL;


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		iTotalCnt = sizeof(genCPRespLst) / CPHI_RESP_KEYVAL_SIZE;

		debug_sprintf(szDbgMsg, "%s: Total Number of Fields to be added to HashTable is %d", __FUNCTION__, iTotalCnt);
		CPHI_LIB_TRACE(szDbgMsg);

		/*
		 * Adding FD fields to the hash table
		 * Each tag name in the FD request will be stored as KEY
		 * and its Index in the data strutcure will be stored as VALUE
		 */
		for(iCount = 0; iCount < iTotalCnt; iCount++)
		{
			debug_sprintf(szDbgMsg,"%s: Adding [%s] key with [%d] Value to the Hash Table",__FUNCTION__, genCPRespLst[iCount].key, iCount);
			CPHI_LIB_TRACE(szDbgMsg);

			pstHashLst = addEntryToCPHashTable(genCPRespLst[iCount].key, iCount);
			if(pstHashLst != NULL)
			{
				debug_sprintf(szDbgMsg,"%s: Added [%s] key with [%d] Value to the Hash Table",__FUNCTION__, pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
				CPHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
				CPHI_LIB_TRACE(szDbgMsg);
				//TO_DO: What to do here!!!
			}
			pstHashLst = NULL; //Derefence it before aFDgning
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	CPHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: generateHashVal
 *
 * Description	: This function is used to calculate the hash value for the given
 * 				  input string
 * 				  This adds each character value in the string to a scrambled
 * 				  combination of the previous ones and returns the remainder
 * 				  modulo the array size. This is not the best possible hash function,
 * 				  but it is short and effective
 *
 * Input Params	: Input String to Calculate Hash
 *
 * Output Params: Hash Value
 * ============================================================================
 */
unsigned int generateCPHashVal(char *pszInput)
{
    unsigned int iHashVal = -1;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//CPHI_LIB_TRACE(szDbgMsg);

	if(pszInput == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Input to calculate hash!!! ", __FUNCTION__);
		CPHI_LIB_TRACE(szDbgMsg);
		return iHashVal;
	}

    for (iHashVal = 0; *pszInput != '\0'; pszInput++)
    {
    	iHashVal = *pszInput + (31 * iHashVal);
    }

    debug_sprintf(szDbgMsg, "%s: iHashVal [%u] ", __FUNCTION__, iHashVal);
    CPHI_LIB_TRACE(szDbgMsg);

    return iHashVal % HASHSIZE;
}

/*
 * ============================================================================
 * Function Name: lookupcpHashTable
 *
 * Description	: This function is used to lookup hash table for the given
 * 				  input string
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The seaCPh is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
HASHLST_PTYPE lookupCPHashTable(char *pszInPut)
{
	HASHLST_PTYPE pstHashLst;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	CPHI_LIB_TRACE(szDbgMsg);

    for (pstHashLst = hashtab[generateCPHashVal(pszInPut)]; pstHashLst != NULL; pstHashLst = pstHashLst->next)
    {
        if (strcmp(pszInPut, pstHashLst->pszTagName) == 0)
        {
        	debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table", __FUNCTION__, pszInPut);
        	CPHI_LIB_TRACE(szDbgMsg);

            return pstHashLst;     // Found
        }
    }

    debug_sprintf(szDbgMsg, "%s: Did not find Mapping for %s in hash table", __FUNCTION__, pszInPut);
    CPHI_LIB_TRACE(szDbgMsg);

    return NULL;           // NOT found
}

/*
 * ============================================================================
 * Function Name: addEntryToHashTable
 *
 * Description	: This function is used to add entry to the hash table
 *
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The seaCPh is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
HASHLST_PTYPE addEntryToCPHashTable(char *pszTagName, int iTagIndexVal)
{
	HASHLST_PTYPE pstHashLst;
    unsigned int hashval;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
//	CPHI_LIB_TRACE(szDbgMsg);

    if ((pstHashLst = lookupCPHashTable(pszTagName)) == NULL)        // NOT found
    {
        pstHashLst = (HASHLST_PTYPE)malloc(sizeof(HASHLST_STYPE));

        if ((pstHashLst == NULL) || (pstHashLst->pszTagName = strdup(pszTagName)) == NULL)
        {
        	debug_sprintf(szDbgMsg, "%s: Error while creating memory for Hash list ", __FUNCTION__);
        	CPHI_LIB_TRACE(szDbgMsg);
            return NULL;
        }

        hashval = generateCPHashVal(pszTagName); //Generating the Hash Value

        debug_sprintf(szDbgMsg, "%s: Generated Hash Value for  key [%s] is [%u] ", __FUNCTION__, pszTagName, hashval);
        CPHI_LIB_TRACE(szDbgMsg);

        pstHashLst->next = hashtab[hashval];

        hashtab[hashval] = pstHashLst;
    }
    else // Already there
    {
    	debug_sprintf(szDbgMsg, "%s: Already present, need not add it again ", __FUNCTION__);
    	CPHI_LIB_TRACE(szDbgMsg);
    }

    pstHashLst->iTagIndexVal = iTagIndexVal;

    return pstHashLst;
}


