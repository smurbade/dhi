/*
 * rcReqDataCtrl.c
 *
 *  Created on: Nov 28, 2014
 *      Author: BLR_SCA_TEAM
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "RCHI/rchiCommon.h"
#include "RCHI/rchiHostCfg.h"
#include "RCHI/rchiCfg.h"
#include "RCHI/metaData.h"
#include "RCHI/rchiGroups.h"
#include "RCHI/utils.h"
#include "RCHI/csv.h"

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)




/*typedef METADATA_STYPE	META_STYPE;
typedef METADATA_PTYPE	META_PTYPE;*/
extern void getPANFromTrackData(char *, int, char *);

static int 	fillGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE, int);
static int 	fillGroupDtlsforVarlist(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE, int, RCKEYVAL_PTYPE*, int*);
static int 	getDHIValueLstptr(DHI_VAL_LST_PTYPE* , int*, char* , int*);
static int 	fillDHIValLstPtr(DHI_VAL_LST_PTYPE, int, DHI_VAL_LST_PTYPE, char*, int*);

//static int 	fillCommonGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillCheckGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillCardGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillPinGroupDtls(RCKEYVAL_PTYPE curPtr, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillAdditionalAmountGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE, int);
//static int 	fillTransArmorGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillVisaGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillOrigAuthGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillPurchasecardLvl2GroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillHostTotalsGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillHostTotalsDetailsGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillEBTGroupDtls(RCKEYVAL_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static int 	fillDCCDtls(RCKEYVAL_PTYPE , TRANDTLS_PTYPE , RCTRANDTLS_PTYPE);
//static int  	fillPassthroughGroup(RCKEYVAL_PTYPE , TRANDTLS_PTYPE , RCTRANDTLS_PTYPE);
static int 	addKeyValDataNode(VAL_LST_PTYPE, void*, int);
static void getTrackData(char*, char*);
static int	getCorrectPaymentType(char*, char*, char*);
static int  getCorrectAmount(char *, char*);
static void getVisaACI(char *);
static void getCCVIndicator(char *, char *);
static void getEncryptionTarget(char *, int);
static void getCardExpirationData(char *, char *, char *);
extern void getPOSConditionCode(char *, int );
extern void getPOSEntryMode(char *, int );
extern void getTermEntryCapablt(char *, int );
static int 	getPresentFlag(char *);
static void getEncryptionBlock(char*, TRANDTLS_PTYPE);
static void getAddtlAmountGrp(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
static int  getCorrectCardType(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
static char getFirstByteFromPAN(TRANDTLS_PTYPE );
static void getNetSettlementAmout(char *);
static int 	assignCardType(char*, char*);
static void getRCGenReqDtls(RCTRANDTLS_PTYPE);
static int 	getCorrectPurchaseCardLvl2GroupValues(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
static void getFSAGroupValues(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
extern void getPOSID(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
static void getDevTypeInd(RCTRANDTLS_PTYPE,TRANDTLS_PTYPE);
static int 	getEBTType(char *, char *);
static int 	getCheckTransactionDtls(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
static int	getEMVGrp(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
static int 	getRCPinGrpDtls(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
static int	getCompletionEMVGrp(TRANDTLS_PTYPE);

static int getCardId(RCTRANDTLS_PTYPE, TRANDTLS_PTYPE);
static int getAmtDtlsForFEXCO(RCTRANDTLS_PTYPE, TRANDTLS_PTYPE);
static void getDCCFields(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
int Hex2Bin (unsigned char *, int , unsigned char *);
static void  Char2Hex(char *, char *, int );


/*
 * ============================================================================
 * Function Name: fillCommonGroupDtls
 *
 * Description	: Fills up the required XML Common Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails, int iPymtFlag)
{
	int 			rv 					= SUCCESS;
	int				iCnt				= 0;
	int				iElementIndex		= 0;
	int				iVarLstCnt			= 0;
	int				iTotalElementsAdded	= 0;
	int				iTotCnt				= 0;
	int				iLen				= 0;
	int				iIndex				= -1;
	RCHI_BOOL		bDataAvailable		= RCHI_FALSE;
	RCHI_BOOL		bReturn				= RCHI_FALSE;
	char*			pszBuffer			= NULL;
	char *			tmpPtr				= NULL;
	VAL_LST_PTYPE  	pstGrpLstPtr		= NULL;
	RCKEYVAL_PTYPE	pstGrpPtr			= NULL;
	RCKEYVAL_PTYPE	pstVarLstptr		= NULL;
	RCKEYVAL_PTYPE	pstRCTmpPtr			= NULL;
	METADATA_STYPE	stLocMetaData;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	/* First Check whether the group has to be filled or not*/
	if(strcmp(rcReqPtr->key, "PINGrp") == SUCCESS)
	{
		if(!(iPymtFlag == DEBIT || iPymtFlag == EBT || (iPymtFlag == CREDIT && pstDHIDtls[eEMV_TAGS].elementValue)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "TAGrp") == SUCCESS)
	{
		if(RCHI_TRUE != isVSPEnabledInDevice())
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "VisaGrp") == SUCCESS)
	{
		if(!((pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL) && (strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Visa") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "MCGrp") == SUCCESS)
	{
		if(!((pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL) && (strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "MasterCard") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "DSGrp") == SUCCESS)
	{
		if(!((pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL) && (strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Discover") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "AmexGrp") == SUCCESS)
	{
		if(!((pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL) && (strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Amex") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "EMVGrp") == SUCCESS)
	{
		/*This Group has to be filled for All the transaction types except TATokenRequest*/
		if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "TATokenRequest") == SUCCESS)
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "FileDLGrp") == SUCCESS)
	{
		/* Only for File download this group should be filled*/
		if(!(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "FileDownload") == SUCCESS))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "PurchCardlvl3Grp") == SUCCESS)
	{
		if(!((strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "Sale") == SUCCESS) || (strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "Completion") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}

	if(bReturn == RCHI_TRUE)
	{
		rv = SUCCESS;
		return rv;
	}


	pstGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stCommonGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt 			= ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;
	iElementIndex		= -1;
	iTotalElementsAdded	= iTotCnt;


	pstGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt*RCKEYVAL_SIZE);
	if (pstGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		++iElementIndex;
		//memcpy(&pstCommonGrpPtr[iCnt], &stCommonGrpLst[iCnt], RCKEYVAL_SIZE);
		memcpy(&pstGrpPtr[iElementIndex], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		if(pstGrpPtr[iElementIndex].varlistSource != NULL)
		{
			rv = fillGroupDtlsforVarlist(&pstGrpPtr[iElementIndex], pstDHIDtls, pstRCHIDetails, iPymtFlag, &pstVarLstptr, &iVarLstCnt);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill Pass through group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}
			else
			{
				if((iVarLstCnt > 0) && (pstVarLstptr!= NULL))
				{
					bDataAvailable = RCHI_TRUE;
					iTotalElementsAdded = iTotalElementsAdded + iVarLstCnt;
					pstRCTmpPtr = realloc(pstGrpPtr, RCKEYVAL_SIZE*(iTotalElementsAdded));
					if(pstRCTmpPtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					}

					memset(pstRCTmpPtr+iElementIndex, 0x00, RCKEYVAL_SIZE*((iTotalElementsAdded)-iElementIndex));
					memcpy(pstRCTmpPtr+iElementIndex, pstVarLstptr, RCKEYVAL_SIZE*(iVarLstCnt));

					iElementIndex = iElementIndex+iVarLstCnt;
					pstGrpPtr = pstRCTmpPtr;
					free(pstVarLstptr);
					pstVarLstptr = NULL;
					iVarLstCnt = 0;
				}
			}

		}
		else
		{
			iIndex = pstGrpPtr[iElementIndex].iIndex;
			if(pstGrpPtr[iElementIndex].iSource == DHI)
			{
				pszBuffer = pstDHIDtls[iIndex].elementValue;
			}
			else if(pstGrpPtr[iElementIndex].iSource == RC)
			{
				pszBuffer = pstRCHIDetails[iIndex].elementValue;
			}

			if(pszBuffer != NULL)
			{
				iLen   = strlen(pszBuffer);
				if(iLen > 0)
				{
					tmpPtr = (char *)malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pszBuffer);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}
					pstGrpPtr[iElementIndex].value = tmpPtr;
					bDataAvailable = RCHI_TRUE;
				}

			}
		}
		if(pstGrpPtr[iElementIndex].isMand == RCHI_TRUE && pstGrpPtr[iElementIndex].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	iTotCnt = iTotalElementsAdded;
	if(rv != FAILURE && bDataAvailable == RCHI_TRUE)
	{
		/* In Case of purchase card level2 group details, we will refill the details after filling the level 3 details
		 * in this case we are freeing the previously allocated memory*/
		if(rcReqPtr->value != NULL)
		{
			stLocMetaData.keyValList = rcReqPtr;
			stLocMetaData.iTotCnt = 1;
			freeMetaData(&stLocMetaData);
			rcReqPtr->value = NULL;
		}

		addKeyValDataNode(pstGrpLstPtr, pstGrpPtr, iTotCnt);
		rcReqPtr->value = pstGrpLstPtr;
	}
	else
	{
		free(pstGrpLstPtr);
		free(pstGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillGroupDtlsforVarlist
 *
 * Description	: This Function will create the array RC KEYVALUE structures based on the presence of corresponding VARLIST Presence.
 *
 * Input Params	:  RCKEYVAL_PTYPE 	- which is used for Reference
 * 				   RCKEYVAL_PTYPE * - pointer to be filled with array of RC KEYVALUE
 * 				   int*				- pointer to be filled with the number contains how many RCKEYVALUE structures are present.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillGroupDtlsforVarlist(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails, int iPymtFlag, RCKEYVAL_PTYPE *pstRCVarLstKeyVal, int* piVarLstCnt)
{
	int					rv 							= SUCCESS;
	int					iCnt						= 0;
	int					iCount						= 0;
	int					iIndex 						= 0;
	int					iElementIndex				= 0;
	int					iVarLstCnt					= 0;
	int					iTotalElementsAdded			= 0;
	int					iLen 						= 0;
	int					imemoryflag 				= 0;
	int					iRCKeyValueCnt				= 0;
	int					iDHIElementCnt				= 0;
	RCHI_BOOL			bDataAvailable				= RCHI_FALSE;
	RCHI_BOOL			bReturn						= RCHI_FALSE;
	int					iNodeType					= -1;
	char*				pszValue					= NULL;
	char*				pszBuffer					= NULL;
	char *				tmpPtr						= NULL;
	RCKEYVAL_PTYPE		pstGrpPtr					= NULL;
	VAL_LST_PTYPE 	 	pstGrpLstPtr				= NULL;
	RCKEYVAL_PTYPE		pstVarLstptr				= NULL;
	RCKEYVAL_PTYPE		pstRCTmpPtr					= NULL;
	KEYVAL_PTYPE		pstDHIKeyValue				= NULL;
	DHI_VAL_LST_PTYPE 	pstDHIValuelist				= NULL;
	DHI_VAL_NODE_PTYPE  pstLocValNode				= NULL;
	DHI_VAL_NODE_PTYPE  pstTempLocValNode			= NULL;
	RCKEYVAL_PTYPE		pstLocRCVarLstKeyVal		= NULL;
	RCKEYVAL_PTYPE		pstTempLocRCVarLstKeyVal	= NULL;


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	if(rcReqPtr == NULL || rcReqPtr->varlistSource == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;

	}

	/* First Check whether the group has to be filled or not*/
	if(strcmp(rcReqPtr->key, "PINGrp") == SUCCESS)
	{
		if(!(iPymtFlag == DEBIT || iPymtFlag == EBT || (iPymtFlag == CREDIT && pstDHIDtls[eEMV_TAGS].elementValue)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "TAGrp") == SUCCESS)
	{
		if(RCHI_TRUE != isVSPEnabledInDevice())
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "VisaGrp") == SUCCESS)
	{
		if(!((pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL) && (strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Visa") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "MCGrp") == SUCCESS)
	{
		if(!((pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL) && (strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "MasterCard") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "DSGrp") == SUCCESS)
	{
		if(!((pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL) && (strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Discover") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "AmexGrp") == SUCCESS)
	{
		if(!((pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL) && (strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Amex") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "EMVGrp") == SUCCESS)
	{
		/*This Group has to be filled for All the transaction types except TATokenRequest*/
		if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "TATokenRequest") == SUCCESS)
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "FileDLGrp") == SUCCESS)
	{
		if(!(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "FileDownload") == SUCCESS))
		{
			bReturn = RCHI_TRUE;
		}
	}
	else if(strcmp(rcReqPtr->key, "PurchCardlvl3Grp") == SUCCESS)
	{
		if(!((strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "Sale") == SUCCESS) || (strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "Completion") == SUCCESS)))
		{
			bReturn = RCHI_TRUE;
		}
	}

	if(bReturn == RCHI_TRUE)
	{
		rv = SUCCESS;
		return rv;
	}


	while(1)
	{
		pstLocRCVarLstKeyVal = (RCKEYVAL_PTYPE)malloc(RCKEYVAL_SIZE *1);
		if(pstLocRCVarLstKeyVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pstLocRCVarLstKeyVal, 0x00, RCKEYVAL_SIZE);

		pszValue = strdup(rcReqPtr->varlistSource);

		rv = getDHIValueLstptr(&pstDHIValuelist, &iNodeType, pszValue, &imemoryflag);
		if(rv != SUCCESS)
		{
			rv = FAILURE;
			break;
		}
		if(pstDHIValuelist == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Value Present for [%s]", __FUNCTION__, pszValue);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
			break;
		}

		pstLocValNode = pstDHIValuelist->start;

		debug_sprintf(szDbgMsg, "%s: Total Nodes[%d]",__FUNCTION__, pstDHIValuelist->valCnt);
		RCHI_LIB_TRACE(szDbgMsg);


		while(pstLocValNode)
		{
			if(pstLocValNode->type == iNodeType)
			{
				pstGrpLstPtr = (VAL_LST_PTYPE)malloc(VAL_LST_SIZE);
				if (pstGrpLstPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(pstGrpLstPtr, 0X00, VAL_LST_SIZE);

				pstDHIKeyValue  = pstLocValNode->elemList;
				iDHIElementCnt 	= pstLocValNode->elemCnt;

				iRCKeyValueCnt 		= ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;
				pstGrpPtr			= (RCKEYVAL_PTYPE)malloc(RCKEYVAL_SIZE *iRCKeyValueCnt);

				// filling the data now

				iElementIndex			= -1;
				iTotalElementsAdded		= iRCKeyValueCnt;


				for(iCnt = 0; iCnt < iRCKeyValueCnt; iCnt++)
				{
					++iElementIndex;
					memcpy(&pstGrpPtr[iElementIndex], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

					if(pstGrpPtr[iElementIndex].varlistSource != NULL)
					{
						if(pstGrpPtr[iElementIndex].varlistSource != NULL)
						{
							rv = fillGroupDtlsforVarlist(&pstGrpPtr[iElementIndex], pstDHIDtls, pstRCHIDetails, iPymtFlag, &pstVarLstptr, &iVarLstCnt);
							if(rv != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: FAILED to fill Pass through group req details", __FUNCTION__);
								RCHI_LIB_TRACE(szDbgMsg);
								break;
							}
							else
							{
								if((iVarLstCnt > 0) && (pstVarLstptr!= NULL))
								{
									bDataAvailable = RCHI_TRUE;
									iTotalElementsAdded = iTotalElementsAdded + iVarLstCnt;
									pstRCTmpPtr = realloc(pstGrpPtr, RCKEYVAL_SIZE*(iTotalElementsAdded));
									if(pstRCTmpPtr == NULL)
									{
										debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
										RCHI_LIB_TRACE(szDbgMsg);
										break;
									}

									memset(pstRCTmpPtr+iElementIndex, 0x00, RCKEYVAL_SIZE*((iTotalElementsAdded)-iElementIndex));
									memcpy(pstRCTmpPtr+iElementIndex, pstVarLstptr, RCKEYVAL_SIZE*(iVarLstCnt));

									iElementIndex = iElementIndex+iVarLstCnt;
									pstGrpPtr = pstRCTmpPtr;
									free(pstVarLstptr);
									pstVarLstptr = NULL;
									iVarLstCnt = 0;
								}
							}

						}

					}
					else if(pstGrpPtr[iElementIndex].iSource == DHI)
					{
						iIndex = pstGrpPtr[iElementIndex].iIndex;

						pszBuffer = pstDHIKeyValue[iIndex].value;

						if(pszBuffer != NULL)
						{
							iLen   = strlen(pszBuffer);
							if(iLen > 0)
							{
								tmpPtr = (char *)malloc(iLen + 1);
								if(tmpPtr != NULL)
								{
									memset(tmpPtr, 0x00, iLen + 1);
									strcpy(tmpPtr, pszBuffer);
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
									RCHI_LIB_TRACE(szDbgMsg);

									rv = FAILURE;
									break;
								}
								pstGrpPtr[iElementIndex].value = tmpPtr;
								bDataAvailable = RCHI_TRUE;
							}

						}
					}
					if(pstGrpPtr[iElementIndex].isMand == RCHI_TRUE && pstGrpPtr[iElementIndex].value == NULL )
					{
						debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}
					tmpPtr = NULL;

				}

				iRCKeyValueCnt = iTotalElementsAdded;

				if(rv != SUCCESS)
				{
					free(pstGrpLstPtr);
					free(pstGrpPtr);
					break;
				}

				if(rv != FAILURE && bDataAvailable == RCHI_TRUE)
				{
					++iCount;

					pstTempLocRCVarLstKeyVal = (RCKEYVAL_PTYPE)realloc(pstLocRCVarLstKeyVal, iCount*RCKEYVAL_SIZE);
					if (pstTempLocRCVarLstKeyVal == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}

					memset(pstTempLocRCVarLstKeyVal + iCount -1 , 0x00, RCKEYVAL_SIZE);
					memcpy(pstTempLocRCVarLstKeyVal + iCount -1, rcReqPtr, RCKEYVAL_SIZE);
					pstLocRCVarLstKeyVal = pstTempLocRCVarLstKeyVal;


					addKeyValDataNode(pstGrpLstPtr, pstGrpPtr, iRCKeyValueCnt);
					pstLocRCVarLstKeyVal[iCount-1].value = pstGrpLstPtr;
				}
				else
				{
					free(pstGrpLstPtr);
					free(pstGrpPtr);
				}
			}
			pstLocValNode = pstLocValNode->next;
		}


		if(rv == SUCCESS && iCount > 0)
		{
			*piVarLstCnt 		= iCount;
			*pstRCVarLstKeyVal  = pstLocRCVarLstKeyVal;

		}
		else
		{
			if(pstLocRCVarLstKeyVal != NULL)
			{
				free(pstLocRCVarLstKeyVal);
				pstLocRCVarLstKeyVal = NULL;
			}
		}

		break;
	}


	if(imemoryflag == 1)
	{
		/*If the source of details is present at the depth(child node inside another node then depth =1, it will increase as it goes to in side child nodes) of more than 2
		 * then we will creating dhi value list pointer in side getDHIValueLstptr function, in that case we should be freeing it once that has been added to RC Meta Data*/
		if(pstDHIValuelist != NULL)
		{
			pstLocValNode = pstDHIValuelist->start;

			while(pstLocValNode)
			{
				pstTempLocValNode = pstLocValNode;
				pstLocValNode = pstLocValNode->next;
				free(pstTempLocValNode);
			}

			free(pstDHIValuelist);
			pstDHIValuelist = NULL;
		}
	}

	if(pszValue != NULL)
	{
		free(pszValue);
		pszValue = NULL;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: fillCommonGroupDtls
 *
 * Description	: Fills up the required XML Common Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillCommonGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			 rv 				= SUCCESS;
	int				 iCnt				= 0;
	int				 iTotCnt			= 0;
	int				 iLen				= 0;
	int				 iIndex				= -1;
	int				 iDataAvailable		= RCHI_FALSE;
	char*			 pszBuffer			= NULL;
	char *			 tmpPtr				= NULL;
	VAL_LST_PTYPE 	 pstCommonGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	 pstCommonGrpPtr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstCommonGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstCommonGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstCommonGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stCheckGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstCommonGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt*RCKEYVAL_SIZE);
	if (pstCommonGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
	//	memcpy(&pstCommonGrpPtr[iCnt], &stCommonGrpLst[iCnt], RCKEYVAL_SIZE);
		memcpy(&pstCommonGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstCommonGrpPtr[iCnt].iIndex;
		if(pstCommonGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstCommonGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstCommonGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}

		}
		if(pstCommonGrpPtr[iCnt].isMand == RCHI_TRUE && pstCommonGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstCommonGrpLstPtr, pstCommonGrpPtr, iTotCnt);
		rcReqPtr->value = pstCommonGrpLstPtr;
	}
	else
	{
		free(pstCommonGrpLstPtr);
		free(pstCommonGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillCheckGroupDtls
 *
 * Description	: Fills up the required XML Check Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillCheckGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			 rv 				= SUCCESS;
	int				 iCnt				= 0;
	int				 iTotCnt			= 0;
	int				 iLen				= 0;
	int				 iIndex				= -1;
	int				 iDataAvailable		= RCHI_FALSE;
	char*			 pszBuffer			= NULL;
	char *			 tmpPtr				= NULL;
	VAL_LST_PTYPE 	 pstCheckGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	 pstCheckGrpPtr		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstCheckGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstCheckGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstCheckGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stCheckGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstCheckGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt*RCKEYVAL_SIZE);
	if (pstCheckGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstCheckGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstCheckGrpPtr[iCnt].iIndex;
		if(pstCheckGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstCheckGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Buffer: %s", __FUNCTION__, pszBuffer);
			RCHI_LIB_TRACE(szDbgMsg);

			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstCheckGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}

		}
		if(pstCheckGrpPtr[iCnt].isMand == RCHI_TRUE && pstCheckGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstCheckGrpLstPtr, pstCheckGrpPtr, iTotCnt);
		rcReqPtr->value = pstCheckGrpLstPtr;
	}
	else
	{
		free(pstCheckGrpLstPtr);
		free(pstCheckGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillCardGroupDtls
 *
 * Description	: Fills up the required XML Card Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillCardGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			rv	 				= SUCCESS;
	int 			iCnt 				= 0;
	int				iTotCnt				= 0;
	int				iLen				= 0;
	int				iIndex				= -1;
	int				iDataAvailable		= RCHI_FALSE;
	char*			tmpPtr				= NULL;
	char*			pszBuffer			= NULL;
	VAL_LST_PTYPE 	pstCardGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstCardGrpPtr		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstCardGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstCardGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstCardGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stCardGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstCardGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstCardGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstCardGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstCardGrpPtr[iCnt].iIndex;
		if(pstCardGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstCardGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstCardGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstCardGrpPtr[iCnt].isMand == RCHI_TRUE && pstCardGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstCardGrpLstPtr, pstCardGrpPtr, iTotCnt);
		rcReqPtr->value = pstCardGrpLstPtr;
	}
	else
	{
		free(pstCardGrpLstPtr);
		free(pstCardGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillPinGroupDtls
 *
 * Description	: Fills up the required XML Pin Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillPinGroupDtls(RCKEYVAL_PTYPE rcReqPtr,  TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			rv				= SUCCESS;
	int 			iCnt 			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iIndex			= -1;
	int				iDataAvailable	= RCHI_FALSE;
	char *			pszBuffer		= NULL;
	char *			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstPinGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstPinGrpPtr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstPinGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstPinGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstPinGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

//	iTotCnt = sizeof(stPinGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstPinGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstPinGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstPinGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstPinGrpPtr[iCnt].iIndex;
		if(pstPinGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstPinGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}


		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstPinGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstPinGrpPtr[iCnt].isMand == RCHI_TRUE && pstPinGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstPinGrpLstPtr, pstPinGrpPtr, iTotCnt);
		rcReqPtr->value = pstPinGrpLstPtr;
	}
	else
	{
		free(pstPinGrpLstPtr);
		free(pstPinGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillAdditionalAmountGroupDtls
 *
 * Description	: Fills up the required XML Additional Amount Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillAdditionalAmountGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails, int iIndicator)
{
	int 			rv					= SUCCESS;
	int 			iCnt 				= 0;
	int				iTotCnt				= 0;
	int				iLen				= 0;
	int				iIndex				= -1;
	int				iDataAvailable		= RCHI_FALSE;
	char*			pszBuffer			= NULL;
	char*			tmpPtr				= NULL;
	VAL_LST_PTYPE 	pstAddlAmtGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstAddlAmtGrpPtr	= NULL;
//	RCKEYVAL_PTYPE	pstAmtPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter --- indicator:%d", __FUNCTION__,iIndicator);
//	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstAddlAmtGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstAddlAmtGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstAddlAmtGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}


	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

//	switch(iIndicator)
//	{
//	case 0:
//		iTotCnt = sizeof(stAddtlTaxAmtGrpLst) / RCKEYVAL_SIZE;
//		pstAmtPtr = stAddtlTaxAmtGrpLst;
//		break;
//	case 1:
//		iTotCnt = sizeof(stAddtlCashBakAmtGrpLst) / RCKEYVAL_SIZE;
//		pstAmtPtr = stAddtlCashBakAmtGrpLst;
//		break;
//	case 2:
//		iTotCnt = sizeof(stAddtlFAAmtGrpLst) / RCKEYVAL_SIZE;
//		pstAmtPtr = stAddtlFAAmtGrpLst;
//		break;
//	case 3:
//		iTotCnt = sizeof(stAddtlTAAmtGrpLst) / RCKEYVAL_SIZE;
//		pstAmtPtr = stAddtlTAAmtGrpLst;
//		break;
//	/* Indicator 4 through 8 correspond to the FSA related Additional Amount Groups */
//	case 4:
//		iTotCnt = sizeof(stAddtlHCAmtGrpLst) / RCKEYVAL_SIZE;
//		pstAmtPtr = stAddtlHCAmtGrpLst;
//		break;
//	case 5:
//		iTotCnt = sizeof(stAddtlPRAmtGrpLst) / RCKEYVAL_SIZE;
//		pstAmtPtr = stAddtlPRAmtGrpLst;
//		break;
//	case 6:
//		iTotCnt = sizeof(stAddtlVIAmtGrpLst) / RCKEYVAL_SIZE;
//		pstAmtPtr = stAddtlVIAmtGrpLst;
//		break;
//	case 7:
//		iTotCnt = sizeof(stAddtlDEAmtGrpLst) / RCKEYVAL_SIZE;
//		pstAmtPtr = stAddtlDEAmtGrpLst;
//		break;
//	case 8:
//		iTotCnt = sizeof(stAddtlCLAmtGrpLst) / RCKEYVAL_SIZE;
//		pstAmtPtr = stAddtlCLAmtGrpLst;
//		break;
//	}

	pstAddlAmtGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstAddlAmtGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&pstAddlAmtGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstAddlAmtGrpPtr[iCnt].iIndex;

		if(pstAddlAmtGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstAddlAmtGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstAddlAmtGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstAddlAmtGrpPtr[iCnt].isMand == RCHI_TRUE && pstAddlAmtGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstAddlAmtGrpLstPtr, pstAddlAmtGrpPtr, iTotCnt);
		rcReqPtr->value = pstAddlAmtGrpLstPtr;
	}
	else
	{
		free(pstAddlAmtGrpLstPtr);
		free(pstAddlAmtGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillTransArmorGroupDtls
 *
 * Description	: Fills up the required XML TransArmor Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillTransArmorGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv				= SUCCESS;
	int 			iCnt 			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iIndex			= -1;
	int				iDataAvailable	= RCHI_FALSE;
	char*			pszBuffer		= NULL;
	char*			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstTAGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstTAGrpPtr		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstTAGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstTAGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstTAGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stTransArmorGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstTAGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstTAGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstTAGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstTAGrpPtr[iCnt].iIndex;
		if(pstTAGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstTAGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstTAGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}

		if(pstTAGrpPtr[iCnt].isMand == RCHI_TRUE && pstTAGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstTAGrpLstPtr, pstTAGrpPtr, iTotCnt);
		rcReqPtr->value = pstTAGrpLstPtr;
	}
	else
	{
		free(pstTAGrpLstPtr);
		free(pstTAGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillVisaGroupDtls
 *
 * Description	: Fills up the required XML Visa Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */


static int fillVisaGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv				= SUCCESS;
	int 			iCnt 			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iIndex			= -1;
	int				iDataAvailable	= RCHI_FALSE;
	char * 			pszBuffer		= NULL;
	char *			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstVisaGrpLstPtr= NULL;
	RCKEYVAL_PTYPE	pstVisaGrpPtr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstVisaGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstVisaGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstVisaGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stVisaGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstVisaGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstVisaGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstVisaGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstVisaGrpPtr[iCnt].iIndex;
		if(pstVisaGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstVisaGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstVisaGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstVisaGrpPtr[iCnt].isMand == RCHI_TRUE && pstVisaGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstVisaGrpLstPtr, pstVisaGrpPtr, iTotCnt);
		rcReqPtr->value = pstVisaGrpLstPtr;
	}
	else
	{
		free(pstVisaGrpLstPtr);
		free(pstVisaGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: fillMasterGroupDtls
 *
 * Description	: Fills up the required XML Master Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  DHI and RCHI details structures
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillMasterGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv				= SUCCESS;
	int 			iCnt 			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iIndex			= -1;
	int				iDataAvailable	= RCHI_FALSE;
	char * 			pszBuffer		= NULL;
	char *			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstMasterGrpLstPtr= NULL;
	RCKEYVAL_PTYPE	pstMasterGrpPtr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstMasterGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstMasterGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstMasterGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stMasterGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstMasterGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstMasterGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstMasterGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstMasterGrpPtr[iCnt].iIndex;
		if(pstMasterGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstMasterGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstMasterGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstMasterGrpPtr[iCnt].isMand == RCHI_TRUE && pstMasterGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstMasterGrpLstPtr, pstMasterGrpPtr, iTotCnt);
		rcReqPtr->value = pstMasterGrpLstPtr;
	}
	else
	{
		free(pstMasterGrpLstPtr);
		free(pstMasterGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillDiscoverGroupDtls
 *
 * Description	: Fills up the required XML Discover Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  DHI and RCHI details structures
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillDiscoverGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv				= SUCCESS;
	int 			iCnt 			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iIndex			= -1;
	int				iDataAvailable	= RCHI_FALSE;
	char * 			pszBuffer		= NULL;
	char *			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstDiscoverGrpLstPtr= NULL;
	RCKEYVAL_PTYPE	pstDiscoverGrpPtr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstDiscoverGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstDiscoverGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstDiscoverGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

//	iTotCnt = sizeof(stDiscoverGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstDiscoverGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstDiscoverGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstDiscoverGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstDiscoverGrpPtr[iCnt].iIndex;
		if(pstDiscoverGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstDiscoverGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstDiscoverGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstDiscoverGrpPtr[iCnt].isMand == RCHI_TRUE && pstDiscoverGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstDiscoverGrpLstPtr, pstDiscoverGrpPtr, iTotCnt);
		rcReqPtr->value = pstDiscoverGrpLstPtr;
	}
	else
	{
		free(pstDiscoverGrpLstPtr);
		free(pstDiscoverGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillAmexGroupDtls
 *
 * Description	: Fills up the required XML Amex Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  DHI and RCHI details structures
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillAmexGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv				= SUCCESS;
	int 			iCnt 			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iIndex			= -1;
	int				iDataAvailable	= RCHI_FALSE;
	char * 			pszBuffer		= NULL;
	char *			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstAmexGrpLstPtr= NULL;
	RCKEYVAL_PTYPE	pstAmexGrpPtr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstAmexGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstAmexGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstAmexGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stAmexGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstAmexGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstAmexGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstAmexGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstAmexGrpPtr[iCnt].iIndex;
		if(pstAmexGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstAmexGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstAmexGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstAmexGrpPtr[iCnt].isMand == RCHI_TRUE && pstAmexGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstAmexGrpLstPtr, pstAmexGrpPtr, iTotCnt);
		rcReqPtr->value = pstAmexGrpLstPtr;
	}
	else
	{
		free(pstAmexGrpLstPtr);
		free(pstAmexGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: fillEBTGrpDtls
 *
 * Description	: Fills up the required XML EBT Group Lvl2 Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillEBTGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv						= SUCCESS;
	int 			iCnt 					= 0;
	int				iTotCnt					= 0;
	int				iLen					= 0;
	int				iIndex					= -1;
	int				iDataAvailable			= RCHI_FALSE;
	char*			pszBuffer				= NULL;
	char*			tmpPtr					= NULL;
	VAL_LST_PTYPE 	pstEBTGrpLstPtr			= NULL;
	RCKEYVAL_PTYPE	pstEBTPtr				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstEBTGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstEBTGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstEBTGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stEBTGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstEBTPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstEBTPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;

	}

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&pstEBTPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstEBTPtr[iCnt].iIndex;
		if(pstEBTPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstEBTPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstEBTPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstEBTPtr[iCnt].isMand == RCHI_TRUE && pstEBTPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstEBTGrpLstPtr, pstEBTPtr, iTotCnt);
		rcReqPtr->value = pstEBTGrpLstPtr;
	}
	else
	{
		free(pstEBTGrpLstPtr);
		free(pstEBTPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillPurchasecardLvl2GrpDtls
 *
 * Description	: Fills up the required XML Purchasecard Lvl2 Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillPurchasecardLvl2GroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv						= SUCCESS;
	int 			iCnt 					= 0;
	int				iTotCnt					= 0;
	int				iLen					= 0;
	int				iIndex					= -1;
	int				iDataAvailable			= RCHI_FALSE;
	char*			pszBuffer				= NULL;
	char*			tmpPtr					= NULL;
	VAL_LST_PTYPE 	pstPurchCardGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstPurchCardPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstPurchCardGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstPurchCardGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstPurchCardGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

//	iTotCnt = sizeof(stPurchCardLvl2GrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstPurchCardPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstPurchCardPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;

	}

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&pstPurchCardPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstPurchCardPtr[iCnt].iIndex;
		if(pstPurchCardPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstPurchCardPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstPurchCardPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}

		if(pstPurchCardPtr[iCnt].isMand == RCHI_TRUE && pstPurchCardPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstPurchCardGrpLstPtr, pstPurchCardPtr, iTotCnt);
		rcReqPtr->value = pstPurchCardGrpLstPtr;
	}
	else
	{
		free(pstPurchCardGrpLstPtr);
		free(pstPurchCardPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillOrigAuthGroupDtls
 *
 * Description	: Fills up the required XML Original Authorization Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillOrigAuthGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv						= SUCCESS;
	int 			iCnt 					= 0;
	int				iTotCnt					= 0;
	int				iLen					= 0;
	int				iIndex					= -1;
	int				iDataAvailable			= RCHI_FALSE;
	char * 			pszBuffer				= NULL;
	char *			tmpPtr					= NULL;
	VAL_LST_PTYPE 	pstOrigAuthGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstOrigAuthPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstOrigAuthGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstOrigAuthGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstOrigAuthGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stOrigAuthGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstOrigAuthPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstOrigAuthPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstOrigAuthPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstOrigAuthPtr[iCnt].iIndex;
		if(pstOrigAuthPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstOrigAuthPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstOrigAuthPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}


/*		if(pstOrigAuthPtr[iCnt].isMand == RCHI_TRUE && pstOrigAuthPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}*/
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstOrigAuthGrpLstPtr, pstOrigAuthPtr, iTotCnt);
		rcReqPtr->value = pstOrigAuthGrpLstPtr;
	}
	else
	{
		free(pstOrigAuthGrpLstPtr);
		free(pstOrigAuthPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillHostTotalsGroupDtls
 *
 * Description	: Fills up the required XML Host Totals Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillHostTotalsGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			rv 						= SUCCESS;
	int 			iCnt 					= 0;
	int				iTotCnt					= 0;
	int				iLen					= 0;
	int				iIndex					= -1;
	int				iDataAvailable			= RCHI_FALSE;
	char *			pszBuffer				= NULL;
	char *			tmpPtr					= NULL;
	VAL_LST_PTYPE 	pstHostTotalsGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstHostTotalsPtr		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstHostTotalsGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstHostTotalsGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstHostTotalsGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stHostTotalsGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstHostTotalsPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstHostTotalsPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstHostTotalsPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstHostTotalsPtr[iCnt].iIndex;
		if(pstHostTotalsPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstHostTotalsPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstHostTotalsPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstHostTotalsPtr[iCnt].isMand == RCHI_TRUE && pstHostTotalsPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstHostTotalsGrpLstPtr, pstHostTotalsPtr, iTotCnt);
		rcReqPtr->value = pstHostTotalsGrpLstPtr;
	}
	else
	{
		free(pstHostTotalsGrpLstPtr);
		free(pstHostTotalsPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: fillFileDLGroupDtls
 *
 * Description	: Fills up the required XML File Download Group Details, which is
 * 				  used to initiate the EMV Keys download.
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of fields for the file Download Group
 * ============================================================================
 */
static int fillFileDLGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			rv				= SUCCESS;
	int 			iCnt 			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iIndex			= -1;
	int				iDataAvailable	= RCHI_FALSE;
	char *			pszBuffer		= NULL;
	char *			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstFileDLGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstFileDLGrpPtr	= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstFileDLGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstFileDLGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstFileDLGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));


	//iTotCnt = sizeof(stFileDLGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstFileDLGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstFileDLGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstFileDLGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstFileDLGrpPtr[iCnt].iIndex;
		if(pstFileDLGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstFileDLGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}


		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstFileDLGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstFileDLGrpPtr[iCnt].isMand == RCHI_TRUE && pstFileDLGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstFileDLGrpLstPtr, pstFileDLGrpPtr, iTotCnt);
		rcReqPtr->value = pstFileDLGrpLstPtr;
	}
	else
	{
		free(pstFileDLGrpLstPtr);
		free(pstFileDLGrpPtr);
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;

}
/*
 * ============================================================================
 * Function Name: fillEmvGroupDtls
 *
 * Description	: Fills up the required XML EMV Group Details, which is
 * 				  sent for the EMV Transactions.
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of fields for the EMV Group
 * ============================================================================
 */
static int fillEmvGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			rv				= SUCCESS;
	int 			iCnt 			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iIndex			= -1;
	int				iDataAvailable	= RCHI_FALSE;
	char *			pszBuffer		= NULL;
	char *			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstEMVGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstEMVGrpPtr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstEMVGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstEMVGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstEMVGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));


//	iTotCnt = sizeof(stEmvGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstEMVGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstEMVGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstEMVGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstEMVGrpPtr[iCnt].iIndex;
		if(pstEMVGrpPtr[iCnt].iSource == DHI)
		{
			debug_sprintf(szDbgMsg, "%s: iCnt:%d", __FUNCTION__,iCnt);
			RCHI_LIB_TRACE(szDbgMsg);
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstEMVGrpPtr[iCnt].iSource == RC)
		{
			debug_sprintf(szDbgMsg, "%s: iCnt:%d", __FUNCTION__,iCnt);
			RCHI_LIB_TRACE(szDbgMsg);
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}


		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstEMVGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstEMVGrpPtr[iCnt].isMand == RCHI_TRUE && pstEMVGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstEMVGrpLstPtr, pstEMVGrpPtr, iTotCnt);
		rcReqPtr->value = pstEMVGrpLstPtr;
	}
	else
	{
		free(pstEMVGrpLstPtr);
		free(pstEMVGrpPtr);
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: fillHostTotalsDetailsGroupDtls
 *
 * Description	: Fills up the required XML Host Totals Details Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillHostTotalsDetailsGroupDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv							= SUCCESS;
	int 			iCnt 						= 0;
	int				iTotCnt						= 0;
	int				iLen						= 0;
	int				iIndex						= -1;
	int				iDataAvailable				= RCHI_FALSE;
	char *			pszBuffer					= NULL;
	char *			tmpPtr						= NULL;
	VAL_LST_PTYPE 	pstHostTotalsDtlsGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstHostTotalsDtlsPtr		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstHostTotalsDtlsGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstHostTotalsDtlsGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstHostTotalsDtlsGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stHostTotalsDtlsGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstHostTotalsDtlsPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);
	if (pstHostTotalsDtlsPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		memcpy(&pstHostTotalsDtlsPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstHostTotalsDtlsPtr[iCnt].iIndex;
		if(pstHostTotalsDtlsPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstHostTotalsDtlsPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstHostTotalsDtlsPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		if(pstHostTotalsDtlsPtr[iCnt].isMand == RCHI_TRUE && pstHostTotalsDtlsPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstHostTotalsDtlsGrpLstPtr, pstHostTotalsDtlsPtr, iTotCnt);
		rcReqPtr->value = pstHostTotalsDtlsGrpLstPtr;
	}
	else
	{
		free(pstHostTotalsDtlsGrpLstPtr);
		free(pstHostTotalsDtlsPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillDCCDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillDCCDtls(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			rv				= SUCCESS;
	int 			iCnt 			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iIndex			= -1;
	int				iDataAvailable	= RCHI_FALSE;
	char *			pszBuffer		= NULL;
	char *			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstDCCGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	pstDCCGrpPtr	= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstDCCGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));


	if (pstDCCGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstDCCGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stDCCFields) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstDCCGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt * RCKEYVAL_SIZE);

	if (pstDCCGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{

		memcpy(&pstDCCGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstDCCGrpPtr[iCnt].iIndex;

		pszBuffer = pstRCHIDetails[iIndex].elementValue;
		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstDCCGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}
		}
		/*If any of the fields are mandatory, then need to add this check*/
//		if(pstDCCGrpPtr[iCnt].isMand == RCHI_TRUE && pstDCCGrpPtr[iCnt].value == NULL )
//		{
//			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
//			RCHI_LIB_TRACE(szDbgMsg);
//
//			rv = FAILURE;
//			break;
//		}
		tmpPtr = NULL;
	}

	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstDCCGrpLstPtr, pstDCCGrpPtr, iTotCnt);
		rcReqPtr->value = pstDCCGrpLstPtr;
	}
	else
	{
		free(pstDCCGrpLstPtr);
		free(pstDCCGrpPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;

}
/*
 * ============================================================================
 * Function Name: fillPassthroughGroup
 *
 * Description	: Fills up the required XML Common Group Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  2D character array containing filled details from SCA
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static int fillPassthroughGroup(RCKEYVAL_PTYPE rcReqPtr, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			 rv 						= SUCCESS;
	int				 iCnt						= 0;
	int				 iTotCnt					= 0;
	int				 iLen						= 0;
	int				 iIndex						= -1;
	int				 iDataAvailable				= RCHI_FALSE;
	char*			 pszBuffer					= NULL;
	char *			 tmpPtr						= NULL;
	VAL_LST_PTYPE 	 pstPassThroughGrpLstPtr	= NULL;
	RCKEYVAL_PTYPE	 pstPassThroughGrpPtr	    = NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstPassThroughGrpLstPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstPassThroughGrpLstPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	memset(pstPassThroughGrpLstPtr, 0X00, sizeof(VAL_LST_STYPE));

	//iTotCnt = sizeof(stCommonGrpLst) / RCKEYVAL_SIZE;
	if(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo == NULL) || ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	iTotCnt = ((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->iElemCnt;

	pstPassThroughGrpPtr = (RCKEYVAL_PTYPE)malloc(iTotCnt*RCKEYVAL_SIZE);
	if (pstPassThroughGrpPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	for(iCnt = 0; iCnt < iTotCnt;iCnt++)
	{
		//memcpy(&pstPassThroughGrpPtr[iCnt], &stCommonGrpLst[iCnt], RCKEYVAL_SIZE);
		memcpy(&pstPassThroughGrpPtr[iCnt], &(((VARLST_INFO_PTYPE)rcReqPtr->valMetaInfo)->keyList[iCnt]), RCKEYVAL_SIZE);

		iIndex = pstPassThroughGrpPtr[iCnt].iIndex;
		if(pstPassThroughGrpPtr[iCnt].iSource == DHI)
		{
			pszBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(pstPassThroughGrpPtr[iCnt].iSource == RC)
		{
			pszBuffer = pstRCHIDetails[iIndex].elementValue;
		}

		if(pszBuffer != NULL)
		{
			iLen   = strlen(pszBuffer);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszBuffer);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				pstPassThroughGrpPtr[iCnt].value = tmpPtr;
				iDataAvailable = RCHI_TRUE;
			}

		}
		if(pstPassThroughGrpPtr[iCnt].isMand == RCHI_TRUE && pstPassThroughGrpPtr[iCnt].value == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory field not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		tmpPtr = NULL;
	}
	if(rv != FAILURE && iDataAvailable == RCHI_TRUE)
	{
		addKeyValDataNode(pstPassThroughGrpLstPtr, pstPassThroughGrpPtr, iTotCnt);
		rcReqPtr->value = pstPassThroughGrpLstPtr;
	}
	else
	{
		free(pstPassThroughGrpLstPtr);
		free(pstPassThroughGrpPtr);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: getMetaDataForRCReq
 *
 * Description	: Fills up the required XML RC Request Details
 *
 * Input Params	: Pointer of type RCKEYVAL_PTYPE
 * 				  Address of structure of type RC_REQ_DTLS_STYPE
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
int getMetaDataForRCReq(META_PTYPE pstMeta, TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails, int iPymtFlag)
{
	int				rv							= SUCCESS;
	int 			iCnt 						= 0;
	int				iIndex						= 0;
	int				iVarLstCnt					= 0;
	int				iTotalGroupsAdded			= 0;
	int				iTotalGrpCnt				= 0;
	int				iPurchasel2MetaPosition		= 0;
	char 			cPaymentType				= 0;
	RCHI_BOOL		bPurchaseL3Presence			= RCHI_FALSE;
	RCKEYVAL_PTYPE 	listPtr						= NULL;
	RCKEYVAL_PTYPE	curPtr						= NULL;
	RCKEYVAL_PTYPE	pstRCTmpPtr					= NULL;
	RCKEYVAL_PTYPE	pstVarLstptr				= NULL;
	RCKEYVAL_PTYPE	pstRCFullHostRequestLst		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

///	iTotCnt = sizeof(stRCHostRequestLst) / RCKEYVAL_SIZE;
	iTotalGrpCnt 			= getTotalGrpCount();
	iIndex					= -1;
	iTotalGroupsAdded		= iTotalGrpCnt;


	pstRCFullHostRequestLst = getTotalHostReqLst();
	if(pstRCFullHostRequestLst == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: Failed to get the Request List Details ",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	listPtr = (RCKEYVAL_PTYPE)malloc(iTotalGrpCnt * RCKEYVAL_SIZE);
	if(listPtr == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	memset(listPtr, 0x00, iTotalGrpCnt * RCKEYVAL_SIZE);
	curPtr = listPtr;

	/* Daivik:17/5/16
	 * In case of Auto Voids it is possible that the payment type comes in as a Credit , but the transaction may have been processed as Debit.
	 * i.e Pin Less Debit Flag may have been returned as 'D' , so we should change the ipymtflag based on the payment type in RC structure instead of the passed flag.
	 */
	if((strcmp(pstRCHIDetails[RC_REVERSL_RSN_CODE].elementValue,"Void") == SUCCESS) && (iPymtFlag == CREDIT || iPymtFlag == DEBIT))
	{
		if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,"Credit") == SUCCESS)
		{
			iPymtFlag = CREDIT;
		}
		else if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,"Debit") == SUCCESS)
		{
			iPymtFlag = DEBIT;
		}
	}

	cPaymentType = ( 1<< iPymtFlag);

	/* Note: PRADEEP:17-11-2016
	 * 1.In stead of loading the all group and element details form the rchiReqLists.inc, All the Details will be loaded from  pstRCFullHostRequestLst which is created at the start up of application.
	 * 2.If Pass through File is configured, then priority will be given to groups and elements given in the pass through File, if Pass through file is not configured then all the fields will be loaded from rchiReqLists.inc.
	 * 3.In Case of PurchCardlvl2Grp we have to fill PC3Add based on presence of the PurchCardlvl3Grp, but as per order we would have filled PurchCardlvl2Grp already, so to add PC3Add we will check whether
	 * 	 PurchCardlvl3Grp is present or not, if PurchCardlvl3Grp is present we will refill PurchCardlvl2Grp details.
	 * */
	for(iCnt = 0; iCnt < iTotalGrpCnt; iCnt++)
	{
		++iIndex;
		memcpy(&curPtr[iIndex], &pstRCFullHostRequestLst[iCnt], RCKEYVAL_SIZE);

		if(cPaymentType & curPtr[iIndex].cPaymentType)
		{
			if(strcmp(curPtr[iIndex].key, "PurchCardlvl2Grp") == SUCCESS)
			{
				iPurchasel2MetaPosition = iIndex;
			}

			if(curPtr[iIndex].varlistSource == NULL)
			{
				rv = fillGroupDtls(&curPtr[iIndex], pstDHIDtls, pstRCHIDetails, iPymtFlag);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill [%s] group req details", __FUNCTION__, curPtr[iIndex].key);
					RCHI_LIB_TRACE(szDbgMsg);

					break;
				}
			}
			else
			{
				rv = fillGroupDtlsforVarlist(&curPtr[iIndex], pstDHIDtls, pstRCHIDetails, iPymtFlag, &pstVarLstptr, &iVarLstCnt);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill [%s] group req details", __FUNCTION__, curPtr[iIndex].key);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
				else
				{
					if((iVarLstCnt > 0) && (pstVarLstptr!= NULL))
					{
						if(strcmp(curPtr[iIndex].key, "PurchCardlvl3Grp") == SUCCESS)
						{
							bPurchaseL3Presence = RCHI_TRUE;
							sprintf(pstRCHIDetails[RC_PC3_ADD].elementValue, "%d",  iVarLstCnt);
						}

						iTotalGroupsAdded = iTotalGroupsAdded + iVarLstCnt;
						pstRCTmpPtr = realloc(listPtr, RCKEYVAL_SIZE*(iTotalGroupsAdded));
						if(pstRCTmpPtr == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
							RCHI_LIB_TRACE(szDbgMsg);
							rv = FAILURE;
							break;
						}

						memset(pstRCTmpPtr+iIndex, 0x00, RCKEYVAL_SIZE*((iTotalGroupsAdded)-iIndex));
						memcpy(pstRCTmpPtr+iIndex, pstVarLstptr, RCKEYVAL_SIZE*(iVarLstCnt));

						iIndex = iIndex + iVarLstCnt;
						listPtr = pstRCTmpPtr;
						curPtr	= pstRCTmpPtr;
						free(pstVarLstptr);
						pstVarLstptr = NULL;
						iVarLstCnt = 0;
					}
				}
			}
		}
	}

	/* Refilling the PurchCardlvl2Grp  Details in order to fill the new PCI3 addenda field based on the presence of the PurchCardlvl3Grp*/
	if(bPurchaseL3Presence == RCHI_TRUE)
	{
		if(cPaymentType & curPtr[iPurchasel2MetaPosition].cPaymentType)
		{
			rv = fillGroupDtls(&curPtr[iPurchasel2MetaPosition], pstDHIDtls, pstRCHIDetails, iPymtFlag);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill [%s] group req details", __FUNCTION__, curPtr[iPurchasel2MetaPosition].key);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
	}



#if 0


		if((strcmp(curPtr[iMetaCnt].key, "CommonGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			rv = fillCommonGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill common group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "CheckGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			rv = fillCheckGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill check group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "CardGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			rv = fillCardGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill card group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		/* Pin Group is allowed for EMV Transactions of Credit Payment Type. */
		else if((strcmp(curPtr[iMetaCnt].key, "PINGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			if(iPymtFlag == DEBIT || iPymtFlag == EBT || (iPymtFlag == CREDIT && pstDHIDtls[eEMV_TAGS].elementValue))
			{
				rv = fillPinGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
				if(rv == FAILURE)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill common group req details", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					break;
				}
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "AddtlAmtGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			rv = fillAdditionalAmountGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails, i++);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill additional amount group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "TAGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			if(RCHI_TRUE == isVSPEnabledInDevice())
			{
				rv = fillTransArmorGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
				if(rv == FAILURE)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill transarmor group req details", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					break;
				}
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "VisaGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			if(pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL)
			{
				if(strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Visa") == SUCCESS)
				{
					rv = fillVisaGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
					if(rv == FAILURE)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to fill visa group req details", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						break;
					}
				}
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "MCGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			if(pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL)
			{
				if(strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "MasterCard") == SUCCESS)
				{
					rv = fillMasterGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
					if(rv == FAILURE)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to fill master group req details", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						break;
					}
				}
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "DSGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			if(pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL)
			{
				if(strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Discover") == SUCCESS)
				{
					rv = fillDiscoverGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
					if(rv == FAILURE)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to fill master group req details", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						break;
					}
				}
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "AmexGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			if(pstRCHIDetails[RC_CARD_TYPE].elementValue != NULL)
			{
				if(strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Amex") == SUCCESS)
				{
					rv = fillAmexGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
					if(rv == FAILURE)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to fill master group req details", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						break;
					}
				}
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "EbtGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			rv = fillEBTGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill master EBT group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}

		else if((strcmp(curPtr[iMetaCnt].key, "PurchCardlvl2Grp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			rv = fillPurchasecardLvl2GroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill purchase card lvl2 group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "OrigAuthGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType) )
		{
			rv = fillOrigAuthGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill original authorisation group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "HostTotGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			rv = fillHostTotalsGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill host totals group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "HostTotDetGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			rv = fillHostTotalsDetailsGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill host totals details group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "EMVGrp") == SUCCESS) &&  (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "TATokenRequest"))
			{
				rv = fillEmvGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
				if(rv == FAILURE)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill host totals group req details", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					break;
				}
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "FileDLGrp") == SUCCESS) &&  (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "FileDownload") == SUCCESS)
			{
				rv = fillFileDLGroupDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
				if(rv == FAILURE)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill host totals group req details", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}
		}
		else if((strcmp(curPtr[iMetaCnt].key, "DCCGrp") == SUCCESS) && (cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			rv = fillDCCDtls(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
			if(rv == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill DCC group req details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}
		else if((cPaymentType & curPtr[iMetaCnt].cPaymentType))
		{
			debug_sprintf(szDbgMsg, "%s: Pass through Group [%s]", __FUNCTION__,curPtr[iMetaCnt].key);
			RCHI_LIB_TRACE(szDbgMsg);


			if(curPtr[iMetaCnt].varlistSource != NULL)
			{
				rv = fillGroupDtlsforVarlist(&curPtr[iMetaCnt], &pstVarLstptr, &iVarCnt);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill Pass through group req details", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
				else
				{
					if((iVarCnt > 0) && (pstVarLstptr!= NULL))
					{
						iMemoryCnt = iMemoryCnt + iVarCnt;
						pstRCTmpPtr = realloc(listPtr, RCKEYVAL_SIZE*(iMemoryCnt));
						if(pstRCTmpPtr == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
							RCHI_LIB_TRACE(szDbgMsg);
							break;
						}

						memset(pstRCTmpPtr+iMetaCnt, 0x00, RCKEYVAL_SIZE*((iMemoryCnt)-iMetaCnt));
						memcpy(pstRCTmpPtr+iMetaCnt, pstVarLstptr, RCKEYVAL_SIZE*(iVarCnt));

						iMetaCnt = iMetaCnt+iVarCnt;
						listPtr = pstRCTmpPtr;
						curPtr	= pstRCTmpPtr;
						free(pstVarLstptr);
						pstVarLstptr = NULL;
						iVarCnt = 0;
					}
				}
			}
			else
			{
				rv = fillPassthroughGroup(&curPtr[iMetaCnt], pstDHIDtls, pstRCHIDetails);
				if(rv == FAILURE)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill Pass through group req details", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}
		}
	}

#endif

	if (rv == FAILURE)
	{
		rv = ERR_FLD_REQD;
	}

	pstMeta->keyValList = listPtr;
	pstMeta->iTotCnt	= iTotalGroupsAdded;

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getBankUserData
 *
 * Description	: This function extracts the bank userdata and fill them in the
 * 				  rchi details
 *
 * Input Params	:
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
void getBankUserData(char* pszBankUserData, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int		iSize			= 0;
	char *  cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	cCurPtr = pszBankUserData;

	cNxtPtr = strchr(cCurPtr, '/');
	if(cNxtPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Separator in Bank User data", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}
	iSize = (cNxtPtr - cCurPtr);

	if(iSize > 0)
	{
		strncpy(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, cCurPtr, iSize);
	}

	cCurPtr = cNxtPtr + 1;

	cNxtPtr = strchr(cCurPtr, '/');
	if(cNxtPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Separator in Bank User data", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}
	iSize = (cNxtPtr - cCurPtr);

	if(iSize > 0)
	{
		strncpy(pstRCHIDetails[RC_POS_COND_CODE].elementValue, cCurPtr, iSize);
	}

	cCurPtr = cNxtPtr + 1;

	cNxtPtr = strchr(cCurPtr, '/');
	if(cNxtPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Separator in Bank User data", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}
	iSize = (cNxtPtr - cCurPtr);

	if(iSize > 0)
	{
		strncpy(pstRCHIDetails[RC_TERM_ENTRY_CAPABLT].elementValue, cCurPtr, iSize);
	}

	cCurPtr = cNxtPtr + 1;

	cNxtPtr = strchr(cCurPtr, '/');
	if(cNxtPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Separator in Bank User data", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}
	iSize = (cNxtPtr - cCurPtr);

	if(iSize > 0)
	{
		strncpy(pstRCHIDetails[RC_CARD_TYPE].elementValue, cCurPtr, iSize);
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}


/*
 * ============================================================================
 * Function Name: fillRCDtlsForSAFCompletionTran
 *
 * Description	: Fills up the required rc details datastructure
 *
 * Input Params	:
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
int fillRCDtlsForSAFCompletionTran(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int		rv 				= SUCCESS;
	int		iPresentFlag	= 0;
	int		iIndicator		= -1;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		/* Fill the Payment Type with the correct value */
		getCorrectPaymentType(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, pstDHIDtls[ePAYMENT_TYPE].elementValue, pstDHIDtls[ePINLESSDEBIT].elementValue);

		getRCGenReqDtls(pstRCHIDetails);

		/* Filling the Ref Num */
		getRefNum(pstRCHIDetails[RC_REFERENCE_NUMBER].elementValue);

		iPresentFlag = getPresentFlag(pstDHIDtls[ePRESENT_FLAG].elementValue);
		if(iPresentFlag < 0 && pstDHIDtls[eBANK_USERDATA].elementValue == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: Present Flag & Bank user data missing !! Cannot proceed further!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_FLD_REQD;
			break;
		}

		/*Parsing and filling the bank userdata passed*/
		if(pstDHIDtls[eBANK_USERDATA].elementValue != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Bank User Data received is %s ", __FUNCTION__, pstDHIDtls[eBANK_USERDATA].elementValue);
			RCHI_LIB_TRACE(szDbgMsg);

			getBankUserData(pstDHIDtls[eBANK_USERDATA].elementValue, pstRCHIDetails);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No Bank User data to parse ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			/* Need to change the function definition*/
			getPOSEntryMode(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, iPresentFlag);

			/* Need to change the function definition */
			getPOSConditionCode(pstRCHIDetails[RC_POS_COND_CODE].elementValue, iPresentFlag);

			/* Get Terminal entry capability */
			getTermEntryCapablt(pstRCHIDetails[RC_TERM_ENTRY_CAPABLT].elementValue, iPresentFlag);
		}

		/* Get Merchant Category code */
		getMerchCatCode(pstRCHIDetails[RC_MERCH_CAT_CODE].elementValue);

		/* Get Term Cat Code */
		getTermCatCode(pstRCHIDetails[RC_TERM_CAT_CODE].elementValue);

		/* Get Transaction currency */
		getTranCurrency(pstRCHIDetails[RC_TRANS_CRNCY].elementValue);

		if(pstDHIDtls[eTRANS_AMOUNT].elementValue != NULL)
		{
			/* To get Transaction Amount */
			rv = getCorrectAmount(pstDHIDtls[eTRANS_AMOUNT].elementValue, pstRCHIDetails[RC_TRAN_AMOUNT].elementValue);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Correct transaction amount failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Transaction Amount missing !! Cannot proceed further!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_FLD_REQD;
			break;
		}

		strcpy(pstRCHIDetails[RC_ADDTL_FA_AMT].elementValue, pstRCHIDetails[RC_TRAN_AMOUNT].elementValue);

		getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_FA_AMT_CRNCY].elementValue);

		/* Hard coding the values for the completion function*/
		strcpy(pstRCHIDetails[RC_ADDTL_FA_AMT_TYPE].elementValue, "FirstAuthAmt");

		strcpy(pstRCHIDetails[RC_ADDTL_TA_AMT].elementValue, pstRCHIDetails[RC_TRAN_AMOUNT].elementValue);

		getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_TA_AMT_CRNCY].elementValue);

		/* Hard coding the values for the completion function*/
		strcpy(pstRCHIDetails[RC_ADDTL_TA_AMT_TYPE].elementValue, "TotalAuthAmt");

		if(pstDHIDtls[ePAYMENT_MEDIA].elementValue != NULL)
		{
			rv = getCorrectCardType(pstDHIDtls, pstRCHIDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Correct card type failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}

		/* Get Term loc indicator */
		getTermLocationIndicator(pstRCHIDetails[RC_TERM_LOC_IND].elementValue);

		/* Get Card Capture capability */
		getCardCaptureCapablt(pstRCHIDetails[RC_CARD_CAPT_CAPABLT].elementValue);

		/* Get POS ID */
		getPOSID(pstDHIDtls, pstRCHIDetails); //Not sure if we have to send this value

		/* Get Security Level */
		getSecurityLvl(pstRCHIDetails[RC_SECURITY_LVL].elementValue);

		/* Get token type */
		getTokenType(pstRCHIDetails[RC_TOKEN_TYPE].elementValue);

#if 0
		if(pstDHIDtls[eCARD_TOKEN].elementValue != NULL)
		{
			/* Fill the card token */
			strcpy(pstRCHIDetails[RC_CARD_TOKEN].elementValue, pstDHIDtls[eCARD_TOKEN].elementValue);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card Token missing !! Cannot proceed further!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_FLD_REQD;
			break;
		}
#endif

		if(pstDHIDtls[eCARD_TOKEN].elementValue != NULL)
		{
			/* Fill the card token */
			strcpy(pstRCHIDetails[RC_CARD_TOKEN].elementValue, pstDHIDtls[eCARD_TOKEN].elementValue);

			if(pstDHIDtls[eEXP_MONTH].elementValue != NULL && pstDHIDtls[eEXP_YEAR].elementValue != NULL)
			{
				getCardExpirationData(pstRCHIDetails[RC_CARD_EXP_DATE].elementValue, pstDHIDtls[eEXP_MONTH].elementValue, pstDHIDtls[eEXP_YEAR].elementValue);
			}
		}
		else
		{
			if(iPresentFlag < 3)
			{
				iIndicator = 0;
			}
			else if (pstDHIDtls[eTRACK_INDICATOR].elementValue != NULL)
			{
				iIndicator = atoi(pstDHIDtls[eTRACK_INDICATOR].elementValue);
			}

			/* Get Encryption Block */
			getEncryptionBlock(pstRCHIDetails[RC_ECRYPT_BLK].elementValue, pstDHIDtls);

			/* Get Security Level */
			getSecurityLvl(pstRCHIDetails[RC_SECURITY_LVL].elementValue);

			/* Get token type */
			getTokenType(pstRCHIDetails[RC_TOKEN_TYPE].elementValue);

			if(strlen(pstRCHIDetails[RC_ECRYPT_BLK].elementValue) > 0)
			{
				/* Get Encryption type */
				getEncrptType(pstRCHIDetails[RC_ENCRPT_TYPE].elementValue);

				/* Get key id */
				getKeyID(pstRCHIDetails[RC_KEY_ID].elementValue);

				/* Get Encryption Target */
				getEncryptionTarget(pstRCHIDetails[RC_ENCRPT_TARGET].elementValue, iIndicator);
			}
		}

		/* fill the original auth group */

		/* Fill the original local date and time as current one*/
		strcpy(pstRCHIDetails[RC_ORIG_LOCAL_DATE_TIME].elementValue, pstRCHIDetails[RC_LOCAL_DATE_TIME].elementValue);

		/* Fill the original Trans date and time as current one*/
		strcpy(pstRCHIDetails[RC_ORIG_TRANSM_DATE_TIME].elementValue, pstRCHIDetails[RC_TRANSM_DATE_TIME].elementValue);

		/* Fill the original stan as current stan*/
		strcpy(pstRCHIDetails[RC_ORIG_STAN].elementValue, pstRCHIDetails[RC_STAN].elementValue);

		/* Fill the original response code */
		strcpy(pstRCHIDetails[RC_ORIG_RESP_CODE].elementValue, "000"); //Hard coding the original result code as 000

		/* MasterCard DevTypeInd value for contactless Cards */
		getDevTypeInd(pstRCHIDetails,pstDHIDtls);

		/* Filling EMV Group details */
		rv = getEMVGrp(pstDHIDtls, pstRCHIDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Filling details EMV Group failed", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Credit") == SUCCESS)
		{
			/* Fill Purchase Card Lvl2 group details */
			if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Refund"))
			{
				rv = getCorrectPurchaseCardLvl2GroupValues(pstDHIDtls, pstRCHIDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Filling details for L2 group failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					break;
				}
			}
			/* According to the UMF Document, the FSA fields are valid only for MC and VISA.
			 *  Prescription/HealthCare are the only valid fields for MC , whereas all FSA
			 *  fields i.e Healthcare,Clinical,Prescription,Vision and Dental are allowed for Visa.
			 */
			if(!strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Visa") || !strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "MasterCard"))
			{
				if(pstDHIDtls[eAMT_HLTCARE].elementValue != NULL)
				{
					getFSAGroupValues(pstDHIDtls, pstRCHIDetails);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Healthcare amount is not found  ", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
			}
		}

		if(pstDHIDtls[eSAF_APPROVALCODE].elementValue != NULL && !strcmp(pstDHIDtls[eCOMMAND].elementValue, "SALE"))
		{
			/* Fill the original auth id */
			strcpy(pstRCHIDetails[RC_ORIG_AUTH_ID].elementValue, pstDHIDtls[eSAF_APPROVALCODE].elementValue);
		}
		else if(pstDHIDtls[eCOMMAND].elementValue != NULL && !strcmp(pstDHIDtls[eCOMMAND].elementValue, "POST_AUTH"))
		{
			/* Fill the original auth id */
			if(pstDHIDtls[eAUTH_CODE].elementValue != NULL)
			{
				strcpy(pstRCHIDetails[RC_ORIG_AUTH_ID].elementValue, pstDHIDtls[eAUTH_CODE].elementValue);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SAF Local Approval code is missing !! Cannot proceed further!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_FLD_REQD;
			break;
		}

		/* Copying the order number*/
		if(pstDHIDtls[eINVOICE].elementValue != NULL)
		{
			strcpy(pstRCHIDetails[RC_ORDER_NUMBER].elementValue, pstDHIDtls[eINVOICE].elementValue);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;

}
/*This function will convert a character buffer into two byte Hex buff*/
static void  Char2Hex(char *inBuff, char *outBuff, int iLen)
{
	char temp[2 + 1];
	char chHex[] = "0123456789ABCDEF\0";
	int i = 0, iDec;
	while(i < iLen)
	{
		iDec = inBuff[i++];
		memset(temp, 0x00, sizeof(temp));

		temp[1] = chHex[iDec%16];
		iDec = iDec/16;
		temp[0] = chHex[iDec%16];

		strcat(outBuff, temp);
	}
}
/*============================================================================
Hex2Bin - This is utility function to perform Hex to Binary conversion
on input data
@param - data. Pointer to input hex data
@param - length. number of bytes to convert
@param - dest. Buffer to hold converted data buffer
@return - length.
=============================================================================*/
int Hex2Bin (unsigned char *data, int length, unsigned char *dest)
{
    int c;
    unsigned char ch;
    unsigned char byte;
    for (c = 0; c < length; c++)
    {
        byte = 0;
        ch = *data++;

        if (ch >= '0' && ch <= '9')
        {
            ch -= '0';
        }
        else
        {
            if (ch >= 'A' && ch <= 'F')
            {
                ch -= 'A';
                ch += 10;
            }
            else if (ch >= 'a' && ch <= 'f')
            {
            	ch -= 'a';
            	ch += 10;
            }
            else
                return (-1);
        }

        byte = ch << 4;
        ch = *data++;

        if (ch >= '0' && ch <= '9')
        {
            ch -= '0';
        }
        else
        {
            if (ch >= 'A' && ch <= 'F')
            {
                ch -= 'A';
                ch += 10;
            }
            else if (ch >= 'a' && ch <= 'f')
            {
            	ch -= 'a';
            	ch += 10;
            }
            else
                return (-1);
        }

        byte |= ch;
        *dest = byte;
        dest++;
    }
    return (length);
}
/*
 * ============================================================================
 * Function Name: getCompletionEMVGrp
 *
 * Description	: Fills up the required emv details
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

static int getCompletionEMVGrp(TRANDTLS_PTYPE pstDHIDtls)
{
	int						rv					= SUCCESS;
	char					szTmpEmvTags[1024]	= "";
	char 					tmpTag[2]			= "";
	char					szAuthRespCode[10] = "";
	char					szIssuerAuthData[40] = "";
	char					*pzStart			= NULL;
	char					*pzEnd				= NULL;
	unsigned short 			iFldlen				= 0;
	int						iAscIIBuffSize		= 0;
	int 					iLengthParsed 		= 0;
	char 					szTag[7]     		= "";
	char					*ascIIBuffer		= NULL;
	char					*szascIIstart		= NULL;
	unsigned int 			tag;
	unsigned short  		Temp;
	unsigned short 			len;
	int iValLen= 0;

#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eEMV_TAGS].elementValue)
	{

		ascIIBuffer = (char *) malloc (sizeof(char)* 2048);
		if( ascIIBuffer == NULL)
		{
			debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			return FAILURE;
		}

		memset(ascIIBuffer, 0x00, 2048);
		iAscIIBuffSize = Hex2Bin((unsigned char *)pstDHIDtls[eEMV_TAGS_RESP].elementValue, strlen(pstDHIDtls[eEMV_TAGS_RESP].elementValue)/2, (unsigned char *)ascIIBuffer);
		if(iAscIIBuffSize <= 0)
		{
			debug_sprintf(szDbgMsg, "%s:Unable to Parse EMV_Tags Reseting all tags card will reject", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			free(ascIIBuffer);
			return FAILURE;
		}
		//storing start for freeing later
		szascIIstart = ascIIBuffer ;

		while(iLengthParsed < iAscIIBuffSize)
		{
			tag = 0;
			pzStart = ascIIBuffer;
			tag = *ascIIBuffer++;
			iLengthParsed++;

			if( (tag & 0x1F) == 0x1F )// tag value is atleast 2 bytes
			{
				do {
					tag = tag << 8;
					tag = tag | *ascIIBuffer++;
					iLengthParsed++;
				} while((tag & 0x80) == 0x80);
			}
			debug_sprintf(szDbgMsg, "%s: Got TAG %d[%x] to Read", __FUNCTION__, tag, tag);
			RCHI_LIB_TRACE(szDbgMsg);

			//Getting Length
			len= *ascIIBuffer++;
			iLengthParsed += 1;
			if (len & 0x80)
			{
				Temp= len & 0x7f;
				len= 0;
				while (Temp)
				{
					len *= 256;
					len += *ascIIBuffer++;
					iLengthParsed += 1;
					Temp--; // Temp should decremented for exiting from the closed loop.
				}
			}
			iLengthParsed += len;
			ascIIBuffer += len;
			pzEnd = ascIIBuffer;
			memset(szTag, 0x00, sizeof(szTag));
			sprintf(szTag, "%X", tag);
			debug_sprintf(szDbgMsg, "%s: Got tag [%s]", __FUNCTION__, szTag);
			RCHI_LIB_TRACE(szDbgMsg);
			if(!strcmp(szTag,"8A"))
			{
				Char2Hex((char *)pzStart, szAuthRespCode, pzEnd - pzStart);
				debug_sprintf(szDbgMsg, "%s: Auth Resp Code = %s ", __FUNCTION__,szAuthRespCode);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else if(!strcmp(szTag,"91"))
			{
				Char2Hex((char *)pzStart, szIssuerAuthData, pzEnd - pzStart);
				debug_sprintf(szDbgMsg, "%s: Issuer Auth Data = %s ", __FUNCTION__,szIssuerAuthData);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
		free(szascIIstart);


		/* Copy Tags upto Tag 82 , Since after Tag 82 we need to add 8A and 91 */
		pzStart = pstDHIDtls[eEMV_TAGS].elementValue;
		pzEnd = strstr(pstDHIDtls[eEMV_TAGS].elementValue,"8202");

		if(!pzStart || !pzEnd)
		{
			debug_sprintf(szDbgMsg, "%s: Tag 82 not found ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			return FAILURE;
		}
		strncpy(szTmpEmvTags,pzStart,(pzEnd-pzStart)+8); // Since Tag 82 is totally 8 bytes long

		if(strlen(szAuthRespCode))
		{
			strcat(szTmpEmvTags,szAuthRespCode);
		}
		else
		{
			strcat(szTmpEmvTags,"8A00");
		}
		if(strlen(szIssuerAuthData))
		{
			strcat(szTmpEmvTags,szIssuerAuthData);
		}
		else
		{
			strcat(szTmpEmvTags,"9100");
		}

		pzStart = strstr(pstDHIDtls[eEMV_TAGS].elementValue,"95");
		pzEnd = strstr(pstDHIDtls[eEMV_TAGS].elementValue,"9F10");
		if(!pzStart || !pzEnd)
		{
			debug_sprintf(szDbgMsg, "%s: Tag 95 / Tag 9F10 not found ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			return FAILURE;
		}
		tmpTag[0] = pzEnd[4];
		tmpTag[1] = pzEnd[5];
		iFldlen = strtol(tmpTag,NULL,16); // Calculate the number of bytes in 9F10 tag , since we need to copy all tags including 9F10
		iFldlen = iFldlen * 2 ;           // Each ASCII Value is considered as a separate charachter , so multiplying by 2 . Ex:0x9F will result in 9 and F as  separate characters
		iFldlen = iFldlen + 6;			  // Including 9F10 Tag and its length
		pzEnd += iFldlen;

		strncat(szTmpEmvTags,pzStart,(pzEnd - pzStart));
//		if(strlen(szScriptRes))
//		{
//			strcat(szTmpEmvTags,szScriptRes);
//		}
//		else
//		{
//			//We should not include a tag if it has no value.
//		//strcat(szTmpEmvTags,"9F5B00");
//		}
		strcat(szTmpEmvTags,pzEnd);

		debug_sprintf(szDbgMsg, "%s: Completion EMV Tags %s ", __FUNCTION__,szTmpEmvTags);
		RCHI_LIB_TRACE(szDbgMsg);

		iValLen= strlen(szTmpEmvTags)+1;
		free(pstDHIDtls[eEMV_TAGS].elementValue);
		pstDHIDtls[eEMV_TAGS].elementValue = (char *)malloc(iValLen * sizeof(char));
		memset(pstDHIDtls[eEMV_TAGS].elementValue, 0x00, iValLen * sizeof(char));
		strcpy(pstDHIDtls[eEMV_TAGS].elementValue,szTmpEmvTags);

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: EMV Data Absent", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: getEMVGrp
 *
 * Description	: Fills up the required emv details
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

static int getEMVGrp(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDtls)
{
	int						rv					= SUCCESS;
	char					szTmpEmvTags[1024]	= "";
	char 					tmpTag[2]			= "";
	char					*pzStart			= NULL;
	char					*pzEnd				= NULL;
	int 					iTagsChng           = 0;
	unsigned short 			iFldlen				= 0;
	int iValLen= 0;
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eEMV_TAGS].elementValue != NULL)
	{
		//strcpy(szTmpEmvTags,pstDHIDtls[eEMV_TAGS].elementValue);
		/* Tag 5F34 corresponds to Application PAN Seq Num , which is used to populate the Card Seq Num field
		 * in the EMV Group. This field is a 3 byte field , but we receive a 2 byte value, hence we append a zero
		 * to the start. We must ommit the field incase the field is not present.
		 */
		pzEnd = strstr(pstDHIDtls[eEMV_TAGS].elementValue,"5F34");
		pzStart = pstDHIDtls[eEMV_TAGS].elementValue;
		if((pzEnd != NULL) && (strncmp(pzEnd,"5F3400",6)) )
		{
			strcpy(pstRCHIDtls[RC_CARD_SEQ_NUM].elementValue,"0");
			strncat(pstRCHIDtls[RC_CARD_SEQ_NUM].elementValue,pzEnd+6,2);
		}
		else
		{
			strcpy(pstRCHIDtls[RC_CARD_SEQ_NUM].elementValue,"");
		}

		pzEnd = NULL;
		/*  If 9F53 Tag ( Transaction Category Code ) is present , we must handle the tag according to value and Card Type
		 *  i.e If Master Card and tag is present, then send the value in EMV Tags
		 *      If anyother card type , then ignore the tag
		 *      If Master Card and tag is absent (has length zero) then ignore the tag.
		 */
		pzEnd = strstr(pstDHIDtls[eEMV_TAGS].elementValue,"9F53");
		if(pzEnd)
		{

			if(!strncmp(pzEnd,"9F5300",6))
			{
				strncpy(szTmpEmvTags,pzStart,pzEnd-pzStart);
				strcat(szTmpEmvTags,pzEnd+6);
				iTagsChng = 1;
			}
			else if((strcasecmp(pstDHIDtls[ePAYMENT_MEDIA].elementValue,"MASTERCARD") == SUCCESS) || (strcasecmp(pstDHIDtls[ePAYMENT_MEDIA].elementValue, "MC") == SUCCESS))
			{
				/* If it is any MC AID, only then we send the 9F53 Tag.
				 * This tag is optional field only for Master Card.
				 */
			}
			else
			{
				/* We are getting the length field into tmpTag and converting to integer
				 * Normally 9F53 Tag (transaction cat code) is a one byte value for Master Card.
				 * What if the EMV Kernel returns another interpretation of 9F53 Tag, hence skipping
				 * the required number of bytes
				 */
				tmpTag[0] = pzEnd[4];
				tmpTag[1] = pzEnd[5];
				iFldlen = strtol(tmpTag,NULL,10);
				strncpy(szTmpEmvTags,pzStart,pzEnd-pzStart);
				strcat(szTmpEmvTags,pzEnd+6+(iFldlen*2));
				iTagsChng = 1;
			}
		}
		else
		{
			 // Do nothing , since 9F53 Tag is not present. DHI EMV_TAGS will remain as it is.
		}
		debug_sprintf(szDbgMsg, "%s: EMV Card Seq :%s", __FUNCTION__,pstRCHIDtls[RC_CARD_SEQ_NUM].elementValue);
		RCHI_LIB_TRACE(szDbgMsg);

		if(iTagsChng == 1)
		{
			iValLen= strlen(szTmpEmvTags)+1;
			free(pstDHIDtls[eEMV_TAGS].elementValue);
			pstDHIDtls[eEMV_TAGS].elementValue = (char *)malloc(iValLen * sizeof(char));
			memset(pstDHIDtls[eEMV_TAGS].elementValue, 0x00, iValLen * sizeof(char));
			strcpy(pstDHIDtls[eEMV_TAGS].elementValue,szTmpEmvTags);
			debug_sprintf(szDbgMsg, "%s: EMV TAGS %s", __FUNCTION__,pstDHIDtls[eEMV_TAGS].elementValue);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: EMV Data Absent", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: getRCGenReqDtls
 *
 * Description	: This function is responsible for getting and filling the gen
 *					request details in the rc data structure.
 *
 * Input Params	:
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
static void getRCGenReqDtls(RCTRANDTLS_PTYPE pstRCHIDetails)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Get Local Time */
	getTime(pstRCHIDetails[RC_LOCAL_DATE_TIME].elementValue, LOCALTIME);

	/* Get GMT Date and Time */
	getTime(pstRCHIDetails[RC_TRANSM_DATE_TIME].elementValue, GMTTIME);

	/* Get Stan */
	getSTAN(pstRCHIDetails[RC_STAN].elementValue);

	/* Get TPPID */
	getTPPID(pstRCHIDetails[RC_TPP_ID].elementValue);

	/* Get Terminal ID */
	getTerminalID(pstRCHIDetails[RC_TERMINAL_ID].elementValue);

	/* Get merchant ID */
	getMerchantID(pstRCHIDetails[RC_MERCHANT_ID].elementValue);

	/* Get Group ID */
	getGroupID(pstRCHIDetails[RC_GROUP_ID].elementValue);

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: fillRCDtls
 *
 * Description	: Fills up the required rc details datastructure
 *
 * Input Params	:
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
int fillRCDtls(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv 									= SUCCESS;
	int 			iIndicator 							= 0;
	int				iPresentFlag						= 0;
	int				iHostTotals							= 0;
	char			cFirstByte							= SUCCESS;
	char*			pszRCCardType						= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		iHostTotals = strlen(pstRCHIDetails[RC_TOT_REQ_DATE].elementValue);
		if(iHostTotals > 0)
		{
			iHostTotals = RCHI_TRUE;
		}

		/* Step 1: Getting the payment of the transaction */
		if(pstDHIDtls[ePAYMENT_TYPE].elementValue != NULL)
		{
			rv = getCorrectPaymentType(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, pstDHIDtls[ePAYMENT_TYPE].elementValue, pstDHIDtls[ePINLESSDEBIT].elementValue);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Correct payment type failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}

		/*Step 2: Check if its an EBT Transaction, check for the EBT_TYPE field, since it is mandatory for an EBT Transaction.
		 * If the field is not present then return from this point itself.
		 */
		if(!strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,"EBT"))
		{
			rv = getEBTType(pstDHIDtls[eEBT_TYPE].elementValue, pstRCHIDetails[RC_EBT_TYPE].elementValue);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Cannot perform EBT Transaction", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}


		/*Step 3: Filling all the gen req details which are required for every RC request*/
		getRCGenReqDtls(pstRCHIDetails);

		/* If the Transaction Type is HostTotals,
		 * fill only the required details and break out of the loop */

		if(iHostTotals == RCHI_TRUE)
		{
			getHostTotalsPassword(pstRCHIDetails[RC_PASSWORD].elementValue);
			getNetSettlementAmout(pstRCHIDetails[RC_NET_STLMNT_AMT].elementValue);
			getTranCurrency(pstRCHIDetails[RC_TRANS_CRNCY].elementValue);
			break;
		}


		/* Get Ref Num */
		getRefNum(pstRCHIDetails[RC_REFERENCE_NUMBER].elementValue);

		if(!strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Check"))
		{
			rv = getCheckTransactionDtls(pstDHIDtls,pstRCHIDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Cannot perform Check Transaction", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
			getAddtlAmountGrp(pstDHIDtls,pstRCHIDetails);
			break;
		}

		/* Obtaining the value of Present Flag of DHI to determine POS Entry Mode, POS Condtion Code and Terminal Entry Capability */

		/* If present flag is present, obtain it */
		//if(pstDHIDtls[eCARD_TOKEN].elementValue == NULL)
		if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
		{
			iPresentFlag = getPresentFlag(pstDHIDtls[ePRESENT_FLAG].elementValue);
			if(iPresentFlag < 0)
			{
				debug_sprintf(szDbgMsg, "%s: Present Flag missing !! Cannot proceed further!!", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_FLD_REQD;
				break;
			}
		}

		/* First setting the indicator of the track if it exists
		 * IF it is a manual then setting the indicator to 0
		 * */
		if(iPresentFlag < 3)
		{
			iIndicator = 0;
		}
		else if (pstDHIDtls[eTRACK_INDICATOR].elementValue != NULL)
		{
			iIndicator = atoi(pstDHIDtls[eTRACK_INDICATOR].elementValue);
		}
		else
		{
			rv = ERR_FLD_REQD;
			break;
		}

		if(RCHI_FALSE == isVSPEnabledInDevice() || strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid") ==  SUCCESS)
		{
			/* Getting Track1 or Track2 Data */
			if(pstDHIDtls[eTRACK_DATA].elementValue != NULL)
			{
				if(iIndicator == 1)
				{
					getTrackData(pstRCHIDetails[RC_TRACK1_DATA].elementValue, pstDHIDtls[eTRACK_DATA].elementValue);
				}
				else if(iIndicator == 2)
				{
					getTrackData(pstRCHIDetails[RC_TRACK2_DATA].elementValue, pstDHIDtls[eTRACK_DATA].elementValue);
				}
			}
			else
			{
				/* Copying the account number */
				if(pstDHIDtls[eACCT_NUM].elementValue != NULL)
				{
					strcpy(pstRCHIDetails[RC_ACCT_NUM].elementValue, pstDHIDtls[eACCT_NUM].elementValue);
				}

				if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid"))
				{
					if(pstDHIDtls[eEXP_MONTH].elementValue != NULL && pstDHIDtls[eEXP_YEAR].elementValue != NULL)

					{
						getCardExpirationData(pstRCHIDetails[RC_CARD_EXP_DATE].elementValue, pstDHIDtls[eEXP_MONTH].elementValue, pstDHIDtls[eEXP_YEAR].elementValue);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Expiry Date missing", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						rv = ERR_FLD_REQD;
						break;
					}

					/* Get CCV Indicator */
					if(pstDHIDtls[eCVV2].elementValue != NULL )
					{
						strcpy(pstRCHIDetails[RC_CCV].elementValue, pstDHIDtls[eCVV2].elementValue);
						getCCVIndicator(pstRCHIDetails[RC_CCV_IND].elementValue, pstDHIDtls[eCVV2].elementValue);
					}
				}
			}
		}
		else
		{
			/* Get Encryption Block */
			getEncryptionBlock(pstRCHIDetails[RC_ECRYPT_BLK].elementValue, pstDHIDtls);

			/* Get Security Level */
			getSecurityLvl(pstRCHIDetails[RC_SECURITY_LVL].elementValue);

			/* Get token type */
			getTokenType(pstRCHIDetails[RC_TOKEN_TYPE].elementValue);
			if(strlen(pstRCHIDetails[RC_ECRYPT_BLK].elementValue) > 0)
			{
				/* Get Encryption type */
				getEncrptType(pstRCHIDetails[RC_ENCRPT_TYPE].elementValue);

				/* Get key id */
				getKeyID(pstRCHIDetails[RC_KEY_ID].elementValue);

				/* Get Encryption Target */
				getEncryptionTarget(pstRCHIDetails[RC_ENCRPT_TARGET].elementValue, iIndicator);
			}
			/* Checking for Token Initiated Transaction */
			else if(pstDHIDtls[eCARD_TOKEN].elementValue != NULL && strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Debit") && strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "EBT") )
			{

				/* Filling Card Token */
				strcpy(pstRCHIDetails[RC_CARD_TOKEN].elementValue, pstDHIDtls[eCARD_TOKEN].elementValue);
				/* Filling Card Expiry Date */
				if(pstDHIDtls[eEXP_MONTH].elementValue != NULL && pstDHIDtls[eEXP_YEAR].elementValue != NULL)
				{
					getCardExpirationData(pstRCHIDetails[RC_CARD_EXP_DATE].elementValue, pstDHIDtls[eEXP_MONTH].elementValue, pstDHIDtls[eEXP_YEAR].elementValue);
				}
				else
				{
					if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "Refund"))
					{
						debug_sprintf(szDbgMsg, "%s: Expiry Date missing", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						rv = ERR_FLD_REQD;
						break;
					}
				}
			}
		}

		/*Parsing and filling the bank userdata passed*/
		if(pstDHIDtls[eBANK_USERDATA].elementValue != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Bank User Data received is %s ", __FUNCTION__, pstDHIDtls[eBANK_USERDATA].elementValue);
			RCHI_LIB_TRACE(szDbgMsg);

			getBankUserData(pstDHIDtls[eBANK_USERDATA].elementValue, pstRCHIDetails);
			/* Changing POS Entry mode for Token based transactions */
			strcpy(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, "011");

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Empty Bank User Data passed ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting correct card type only for Credit and Prepaid Payment type */
		if(!(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Debit") == SUCCESS || strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "EBT") == SUCCESS)  &&
				strlen(pstRCHIDetails[RC_CARD_TYPE].elementValue) == 0)
		{
			if(pstDHIDtls[ePAYMENT_MEDIA].elementValue != NULL)
			{
				rv = getCorrectCardType(pstDHIDtls, pstRCHIDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Correct card type failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					break;
				}
			}
			else if ((pstDHIDtls[eCOMMAND].elementValue != NULL) && (strcasecmp(pstDHIDtls[eCOMMAND].elementValue, "TOKEN_QUERY") == 0))
			{
				/* Daivik:4/3/2016 - This part of the code was added in order to ensure we send the payment media in Bank user data for the Token Query Transactions.
				 * In case of STB disabled , Payment Media will not be there in the SSI Request, so we will hit this condition and fill card type, based on
				 * first byte of PAN. This is then used to prepare Bank user Data.
				 */
				cFirstByte = getFirstByteFromPAN(pstDHIDtls);
				pszRCCardType	= pstRCHIDetails[RC_CARD_TYPE].elementValue;
				switch(cFirstByte)
				{
				case '3':
					strcpy(pszRCCardType, "Amex");
					break;
				case '4':
					strcpy(pszRCCardType, "Visa");
					break;
				case '5':
					strcpy(pszRCCardType, "MasterCard");
					break;
				case '6':
					strcpy(pszRCCardType, "Discover");
					break;
				}
			}
			else
			{
				/* Getting correct card type */
				if(pstDHIDtls[eVSP_REG].elementValue != NULL)
				{
					rv = assignCardType(pstRCHIDetails[RC_CARD_TYPE].elementValue, pstDHIDtls[eVSP_REG].elementValue);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Correct card type failed", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						break;
					}
				}
				else
				{
					rv = ERR_FLD_REQD;

					debug_sprintf(szDbgMsg, "%s: Payment Media missing", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}
		}

		/*
		 * If it is a token query command, we need not fill remaining fields so break from here
		 */
		if(pstDHIDtls[eCOMMAND].elementValue != NULL)
		{
			if(strcasecmp(pstDHIDtls[eCOMMAND].elementValue, "TOKEN_QUERY") == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its Token Query Command, no need to fill rest of the fields breaking from here", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				/* Filling original POS Condition Code and Terminal Entry Capability into temporary buffers for subsequent Token Transactions*/
				/* Get POS Condition Code */
				getPOSConditionCode(pstRCHIDetails[RC_POS_COND_CODE_TKN_QUERY].elementValue, iPresentFlag);
				/* Get Terminal entry capability */
				getTermEntryCapablt(pstRCHIDetails[RC_TERM_ENTRY_CAPABLT_TKN_QUERY].elementValue, iPresentFlag);
				break;
			}
		}

		/* Get Transaction currency */
		getTranCurrency(pstRCHIDetails[RC_TRANS_CRNCY].elementValue);

		if(pstDHIDtls[eTRANS_AMOUNT].elementValue != NULL)
		{
			/* To get Transaction Amount */
			rv = getCorrectAmount(pstDHIDtls[eTRANS_AMOUNT].elementValue, pstRCHIDetails[RC_TRAN_AMOUNT].elementValue);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Correct transaction amount failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}
		/* HardCoding Transaction amount of 000 for BalanceInquiry in Prepaid Payment Type, as per the document */
		else if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "BalanceInquiry") == SUCCESS ||
				strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "CashoutActiveStatus") == SUCCESS)
		{
			strcpy(pstRCHIDetails[RC_TRAN_AMOUNT].elementValue, "000");
		}

		if(pstDHIDtls[eDCCInd].elementValue != NULL && strcmp(pstDHIDtls[eDCCInd].elementValue, "2") == SUCCESS)
		{
			free(pstDHIDtls[eDCCInd].elementValue);
			pstDHIDtls[eDCCInd].elementValue = NULL;
		}

		if(pstDHIDtls[eDCCInd].elementValue != NULL)
		{
			free(pstDHIDtls[ePARTIAL_AUTH].elementValue);
			pstDHIDtls[ePARTIAL_AUTH].elementValue = NULL;
		}
		/* Getting Additional Amount */
		getAddtlAmountGrp(pstDHIDtls, pstRCHIDetails);
		/* Getting EMV Group details */
		rv = getEMVGrp(pstDHIDtls, pstRCHIDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Filling details for EMV Group failed", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* Copying the order number*/
		if(pstDHIDtls[eINVOICE].elementValue != NULL)
		{
			strcpy(pstRCHIDetails[RC_ORDER_NUMBER].elementValue, pstDHIDtls[eINVOICE].elementValue);
		}

		rv = getRCPinGrpDtls(pstDHIDtls,pstRCHIDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Filling details for Pin group failed", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* Get Alternate merchant id */
		if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid") ==  SUCCESS)
		{
			/* Get Alternate merchant id */
			getAlternateMerchID(pstRCHIDetails[RC_ALT_MERCH_ID].elementValue);
		}

		/* Get Merchant Category code */
		getMerchCatCode(pstRCHIDetails[RC_MERCH_CAT_CODE].elementValue);

		/* Get Term Cat Code */
		getTermCatCode(pstRCHIDetails[RC_TERM_CAT_CODE].elementValue);

		/* Get POS ID */
		getPOSID(pstDHIDtls, pstRCHIDetails);

		if(pstDHIDtls[eBANK_USERDATA].elementValue == NULL)
		{
			/* Need to change the function definition*/
			getPOSEntryMode(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, iPresentFlag);

			/* Need to change the function definition */
			getPOSConditionCode(pstRCHIDetails[RC_POS_COND_CODE].elementValue, iPresentFlag);

			/* Get Terminal entry capability */
			getTermEntryCapablt(pstRCHIDetails[RC_TERM_ENTRY_CAPABLT].elementValue, iPresentFlag);
		}

		/* Get Term loc indicator */
		getTermLocationIndicator(pstRCHIDetails[RC_TERM_LOC_IND].elementValue);

		/* Get Card Capture capability */
		getCardCaptureCapablt(pstRCHIDetails[RC_CARD_CAPT_CAPABLT].elementValue);

		if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "Refund") != SUCCESS)
		{
			/* Get Visa ACI value */
			getVisaACI(pstRCHIDetails[RC_AUTH_CHARACTERISTICS_IND].elementValue);
		}
		/* MasterCard DevTypeInd value for contactless Cards */
		/* Get Device Type Indicator Value for MasterCard */
		getDevTypeInd(pstRCHIDetails,pstDHIDtls);
		/* Filling purchase card L2 fields if required */
		if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Credit") == SUCCESS)
		{
			/* For a credit refund L2 group is not required */
			if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "Refund"))
			{
				rv = getCorrectPurchaseCardLvl2GroupValues(pstDHIDtls, pstRCHIDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Filling details for L2 group failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					break;
				}
			}
		}
		/*To check DCC Indicator*/
		if((pstDHIDtls[eDCCInd].elementValue != NULL) &&
				((strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue,"Sale") == SUCCESS) || (strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue,"Refund") == SUCCESS)))
		{
			if(strcmp(pstDHIDtls[ePAYMENT_TYPE].elementValue, "CREDIT") == SUCCESS )
			{
				if(strcmp(pstDHIDtls[eDCCInd].elementValue,"1")  == 0 || strcmp(pstDHIDtls[eDCCInd].elementValue,"3") == 0)			//1 �Sale or Refund transaction in which the currency is eligible for DCC and cardholder has accepted the option.
				{
					getDCCFields(pstDHIDtls, pstRCHIDetails);
					debug_sprintf(szDbgMsg, "%s: DCC fields are added", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
//				else if(strcmp(pstDHIDtls[eDCCInd].elementValue,"2") == 0)			//2 - Sale or Refund transaction in which the currency is not eligible for DCC.
//				{
//					strcpy(pstRCHIDetails[RC_DCC_IND].elementValue, pstDHIDtls[eDCCInd].elementValue);
//					if((pstDHIDtls[eDCCCrncy].elementValue != NULL) && strlen(pstDHIDtls[eDCCCrncy].elementValue) > 0)
//					{
//						strcpy(pstRCHIDetails[RC_DCC_CARDHOLDER_CURR_CODE].elementValue, pstDHIDtls[eDCCCrncy].elementValue);
//					}
//				}
#if 0
				else if(strcmp(pstDHIDtls[eDCCInd].elementValue,"3") == 0)			// 3- Sale or Refund transaction in which the currency is eligible for DCC, but cardholder has not accepted the option.
				{
					strcpy(pstRCHIDetails[RC_DCC_IND].elementValue, pstDHIDtls[eDCCInd].elementValue);
					if((pstDHIDtls[eDCCCrncy].elementValue != NULL) && strlen(pstDHIDtls[eDCCCrncy].elementValue) > 0)
					{
						strcpy(pstRCHIDetails[RC_DCC_CARDHOLDER_CURR_CODE].elementValue, pstDHIDtls[eDCCCrncy].elementValue);
					}
				}
#endif
				else
				{
					debug_sprintf(szDbgMsg, "%s: DCC Indicator is Greater than 3", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: fillRCDtlsforEMVAdmin
 *
 * Description	: Fills up the required rc details for the EMV Keys Status Request Packet
 *
 * Input Params	:
 *
 * Output Params: FAILURE/SUCCESS
 * ============================================================================
 */
int fillRCDtlsforEMVAdmin(RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv 		= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	char					szfileDtls[50];
	char 					*pszFileCrc;
	char 					*pszFileSize;
	char 					*pszFileCrtDt;
	FILE *fptr;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	fptr = fopen(EMV_KEY_FL_DTLS, "r");
	if(fptr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Could not open the file could not save the record", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return rv;
	}
	fscanf(fptr, "%s", szfileDtls);
	fclose(fptr);

	pszFileSize = strtok(szfileDtls,"|");
	pszFileCrtDt = strtok(NULL,"|");
	pszFileCrc = strtok(NULL,"|");

	getRCGenReqDtls(pstRCHIDetails);
	memset(pstRCHIDetails[RC_TRAN_TYPE].elementValue,0x00, sizeof(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue));
	strcpy(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "FileDownload");
	memset(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,0x00, sizeof(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue));
	strcpy(pstRCHIDetails[RC_FL_TYPE].elementValue,"EMV2KEY");
	strcpy(pstRCHIDetails[RC_FUNC_CODE].elementValue,"R");

	if(pszFileCrc && pszFileCrtDt && pszFileSize)
	{
		debug_sprintf(szDbgMsg, "%s:%d File Details : CRC:%s Size:%s FlCreat:%s", __FUNCTION__, __LINE__,pszFileCrc,pszFileSize,pszFileCrtDt);
		RCHI_LIB_TRACE(szDbgMsg);
		strcpy(pstRCHIDetails[RC_FL_CRC].elementValue,pszFileCrc);
		strcpy(pstRCHIDetails[RC_FL_CREAT_DATE].elementValue,pszFileCrtDt);
		strcpy(pstRCHIDetails[RC_FL_SZ].elementValue,pszFileSize);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s:%d File Detail is NULL", __FUNCTION__, __LINE__);
		RCHI_LIB_TRACE(szDbgMsg);
		return rv;
	}

	return rv;
}
/*
 * ============================================================================
 * Function Name: fillRCDtlsforCheckTransaction
 *
 * Description	: Fills up the required rc details datastructure
 *
 * Input Params	:
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
int fillRCDtlsforCheckTransaction(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int				rv 									= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{

		/* Step 1: Getting the payment of the transaction */
		if(pstDHIDtls[ePAYMENT_TYPE].elementValue != NULL)
		{
			rv = getCorrectPaymentType(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, pstDHIDtls[ePAYMENT_TYPE].elementValue, pstDHIDtls[ePINLESSDEBIT].elementValue);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Correct payment type failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}

		/* Step 2: Filling Check group */
		rv = getCheckTransactionDtls(pstDHIDtls,pstRCHIDetails);

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Cannot perform Check Transaction", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		/*Step 3: Filling all the gen req details which are required for every RC request*/
		getRCGenReqDtls(pstRCHIDetails);

		/* Get Ref Num */
		getRefNum(pstRCHIDetails[RC_REFERENCE_NUMBER].elementValue);

		/* Hardcoding POSEntryMode and Terminal Entry Capability based on Manual Entry or OCR(Optical Character Reader) */
		if(pstDHIDtls[eMICR].elementValue != NULL)
		{
			strcpy(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, "041");
			strcpy(pstRCHIDetails[RC_TERM_ENTRY_CAPABLT].elementValue, "07");
		}
		else
		{
			strcpy(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, "011");
			strcpy(pstRCHIDetails[RC_TERM_ENTRY_CAPABLT].elementValue, "10");
		}


		/* Hard coding POS Cond Code as per UMF doc */
		strcpy(pstRCHIDetails[RC_POS_COND_CODE].elementValue, "06");

		/* Get Merchant Category code */
		getMerchCatCode(pstRCHIDetails[RC_MERCH_CAT_CODE].elementValue);

		/* Getting additional amount group details */
		getAddtlAmountGrp(pstDHIDtls,pstRCHIDetails);

		/* Get Transaction currency */
		getTranCurrency(pstRCHIDetails[RC_TRANS_CRNCY].elementValue);

		if(pstDHIDtls[eTRANS_AMOUNT].elementValue != NULL)
		{
			/* To get Transaction Amount */
			rv = getCorrectAmount(pstDHIDtls[eTRANS_AMOUNT].elementValue, pstRCHIDetails[RC_TRAN_AMOUNT].elementValue);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Correct transaction amount failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}

		/* Get Term Cat Code */
		getTermCatCode(pstRCHIDetails[RC_TERM_CAT_CODE].elementValue);

		/* Get Term loc indicator */
		getTermLocationIndicator(pstRCHIDetails[RC_TERM_LOC_IND].elementValue);

		/* Get Card Capture capability */
		getCardCaptureCapablt(pstRCHIDetails[RC_CARD_CAPT_CAPABLT].elementValue);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillCardRateDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int fillCardRateDtls(TRANDTLS_PTYPE pstDHIDetails, RCTRANDTLS_PTYPE pstRCDetails)
{
	int 				rv 										= SUCCESS;
	char				szTemp[32+1]							= "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	rv = getCardId(pstRCDetails, pstDHIDetails);

	if(rv == SUCCESS && strcmp(pstDHIDetails[eDCC_OFFD].elementValue, "FALSE") == 0)
	{
		debug_sprintf(szDbgMsg, "%s:Card is not Eliglible for DCC, so setting DCC Offered to FLASE", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		return rv;
	}
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s:Failed to get Card Id", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		return rv;
	}

	rv = getAmtDtlsForFEXCO(pstRCDetails, pstDHIDetails);
	if (rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s:Failed to get Amount Dtls", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		return rv;
	}

	getRefNumForCardRate(szTemp);
	strcpy(pstRCDetails[RC_DCC_REF_NUM].elementValue, szTemp);

	debug_sprintf(szDbgMsg, "%s:Returning[%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDHIValueLstptr
 *
 * Description	: This Function will fill the pstDHIValuelist pointer of corresponding SSI XML Node
 * 				  if The SSI XML Node is present at the depth more than 2(depth = it will increase as it goes inside child of a node)
 * 				  in above case this function will create new DHI value list pointer and pass to fillDHIValLstPtr which will fill the values.
 *
 * Input Params	: DHI_VAL_LST_PTYPE*, int*, char*,  int *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getDHIValueLstptr(DHI_VAL_LST_PTYPE *pstDHIValuelist, int* piNodeType, char* pszVarLstSource, int *piMemoryFlag)
{
	int							rv					= SUCCESS;
	int							iCnt				= 0;
	int							kCnt				= 0;
	int							iKeyValueCnt		= 0;
	int							iDepth				= 0;
	char						szRootName[256]		= "";
	char* 						pszVal				= NULL;
	char*						pszTemp				= NULL;
	char*						cPtr				= NULL;
	RCHI_BOOL					bFound				= RCHI_FALSE;
	PTVARLST_PTYPE				pstDHIVarLst		= NULL;
	DHI_VARLST_INFO_PTYPE	    pstVarLstInfoLoc	= NULL;
	KEYVAL_PTYPE				pstTempKeyValue		= NULL;
	DHI_VAL_LST_PTYPE			valNodesLstPtr		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	if(pszVarLstSource == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;

	}

	valNodesLstPtr = (DHI_VAL_LST_PTYPE) malloc(DHI_VAL_LST_SIZE);
	if(valNodesLstPtr == NULL)
	{
		/* If the memory is not allocated, return ERROR */
		debug_sprintf(szDbgMsg, "%s: Malloc failed for value nodes list ptr",
																__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	else
	{
		/* Initialize the memory if allocated */
		memset(valNodesLstPtr, 0x00, DHI_VAL_LST_SIZE);
	}

	pszVal = strdup(pszVarLstSource);
	pszTemp = pszVal;

	iDepth = 1;
	while((pszTemp = strchr(pszTemp,':')) != NULL)
	{
		++iDepth;
		++pszTemp;
	}

	pstDHIVarLst = getDHIVarLstDtls();

	while(1)
	{
		if((cPtr = strtok(pszVal, ":")) == NULL)
		{
			rv = FAILURE;
			break;
		}
		else
		{
			--iDepth;
			memset(szRootName, 0x00, sizeof(szRootName));
			strcpy(szRootName, cPtr);
		}

		iKeyValueCnt 	= pstDHIVarLst->iReqVARLSTCnt;
		pstTempKeyValue = pstDHIVarLst->pstVarlistReqDtls;


		while(iDepth >= 0)
		{
			for(iCnt = 0; iCnt < iKeyValueCnt; iCnt++)
			{
				if(strcmp(szRootName,pstTempKeyValue[iCnt].key) == SUCCESS)
				{
					pstVarLstInfoLoc = pstTempKeyValue[iCnt].valMetaInfo;

					if(iDepth > 0)
					{
						if((cPtr = strtok(NULL, ":")) == NULL)
						{
							rv = FAILURE;
							break;
						}
						else
						{
							--iDepth;
							memset(szRootName, 0x00, sizeof(szRootName));
							strcpy(szRootName, cPtr);
						}

						kCnt = 0;
						while(pstVarLstInfoLoc[kCnt].nodeType != -1)
						{
							if(pstVarLstInfoLoc[kCnt].szNodeStr != NULL)
							{
								if(strcmp(pstVarLstInfoLoc[kCnt].szNodeStr, szRootName) == SUCCESS)
								{
									if(iDepth > 0)
									{
										if((cPtr = strtok(NULL, ":")) == NULL)
										{
											rv = FAILURE;
											break;
										}
										else
										{
											--iDepth;
											memset(szRootName, 0x00, sizeof(szRootName));
											strcpy(szRootName, cPtr);
										}

										pszTemp = strstr(pszVarLstSource, szRootName);

										rv = fillDHIValLstPtr(pstTempKeyValue[iCnt].value, pstVarLstInfoLoc[kCnt].nodeType, valNodesLstPtr, pszTemp, piNodeType);
										if(rv != SUCCESS)
										{
											if(valNodesLstPtr != NULL)
											{
												free(valNodesLstPtr);
												valNodesLstPtr = NULL;
											}

											debug_sprintf(szDbgMsg, "%s: Failed to create and Fill DHI Value List Ptr", __FUNCTION__);
											RCHI_LIB_TRACE(szDbgMsg);
										}
										else
										{
											*piMemoryFlag = 1; 	// if this parameter is set means DHI value list pointer is created, so once we add the data to RC Meta DHI Value List Pointer should be freed.
											*pstDHIValuelist = valNodesLstPtr;
											bFound 			 = RCHI_TRUE;
										}
										break;

									}
									else
									{
										*piNodeType		 = pstVarLstInfoLoc[kCnt].nodeType;
										*pstDHIValuelist = pstTempKeyValue[iCnt].value;
										bFound = RCHI_TRUE;
										break;
									}
									break;
								}
							}
							++kCnt;
						}
					}
					else
					{
						*piNodeType		 = pstVarLstInfoLoc[0].nodeType;
						*pstDHIValuelist = pstTempKeyValue[iCnt].value;
						bFound = RCHI_TRUE;
						break;
					}
					break;
				}
			}
			break;
		}
		break;
	}


	if(pszVal != NULL)
	{
		free(pszVal);
		pszVal = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: addDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int addDataNode(DHI_VAL_LST_PTYPE valLstPtr, int iNodeType,
										KEYVAL_PTYPE listPtr, int iElemCnt)
{
	int					rv				= SUCCESS;
	DHI_VAL_NODE_PTYPE	tmpNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	tmpNodePtr = (DHI_VAL_NODE_PTYPE) malloc(sizeof(DHI_VAL_NODE_STYPE));
	if(tmpNodePtr != NULL)
	{
		/* Initialize the allocated memory */
		memset(tmpNodePtr, 0x00, sizeof(DHI_VAL_NODE_STYPE));
		tmpNodePtr->elemCnt = iElemCnt;
		tmpNodePtr->elemList = listPtr;
		tmpNodePtr->type = iNodeType;

		/* Add the node to the list */
		if(valLstPtr->end == NULL)
		{
			/* Adding the first element */
			valLstPtr->start = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

//			debug_sprintf(szDbgMsg, "%s: Adding first node", __FUNCTION__);
//			APP_TRACE(szDbgMsg);
		}
		else
		{
			/* Adding the nth element */
			valLstPtr->end->next = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

//			debug_sprintf(szDbgMsg, "%s: Adding some node", __FUNCTION__);
//			APP_TRACE(szDbgMsg);
		}
		/* Increment the node count */
		valLstPtr->valCnt += 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}




/*
 * ============================================================================
 * Function Name: fillDHIValLstPtr
 *
 * Description	: This function will take the DHI Value list pointer as input to get the the data which has to be added to DHI value List Pointer which is passed to this function,
 * 				 It will check Node type passed in the input if the DHI Value list Pointer contains the data on the node type it will be added to Destination Value list pointer
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillDHIValLstPtr(DHI_VAL_LST_PTYPE pstSrcDHIValuelist, int iNodeType, DHI_VAL_LST_PTYPE pstDestDHIValuelist, char *pszVarLstSource, int *piNodeType)
{
	int							rv					= SUCCESS;
	int							iCnt				= 0;
	int							kCnt				= 0;
	int							iDepth				= 0;
	int							iInsideDepth		= 0;
	int							iLocnodeType		= 0;
	char						szRootName[256]		= "";
	char						szMainRootName[256]	= "";
	char* 						pszVal				= NULL;
	char*						pszTemp				= NULL;
	char*						cPtr				= NULL;
	char*						cTmpPtr				= NULL;
	RCHI_BOOL					bFound				= RCHI_FALSE;
	DHI_VAL_NODE_PTYPE			pstLocValNode		= NULL;
	DHI_VAL_NODE_PTYPE			psttmpNode			= NULL;
	DHI_VARLST_INFO_PTYPE	    pstVarLstInfoLoc	= NULL;
	DHI_VAL_LST_PTYPE           pstTempValLstPtr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	if(pszVarLstSource == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;

	}

	if(pstSrcDHIValuelist == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Value is not present in the value List Ptr", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = SUCCESS;
		return rv;

	}

	debug_sprintf(szDbgMsg, "%s: Value Source [%s]", __FUNCTION__, pszVarLstSource);
	RCHI_LIB_TRACE(szDbgMsg);

	pszVal = strdup(pszVarLstSource);
	pszTemp = pszVal;

	iDepth = 1;
	while((pszTemp = strchr(pszTemp,':')) != NULL)
	{
		++iDepth;
		++pszTemp;
	}

	pstLocValNode = pstSrcDHIValuelist->start;

	if(iDepth > 0)
	{
		if((cPtr = strchr(pszVarLstSource, ':')) == NULL)
		{
			--iDepth;
			memset(szMainRootName, 0x00, sizeof(szMainRootName));
			strcpy(szMainRootName, pszVarLstSource);
		}
		else
		{
			--iDepth;
			memset(szMainRootName, 0x00, sizeof(szMainRootName));
			strncpy(szMainRootName, pszVarLstSource, cPtr - pszVarLstSource);

			cPtr = cPtr+1;
		}

		while(pstLocValNode)
		{
			iInsideDepth = iDepth;
			bFound = RCHI_FALSE;

			memset(szRootName, 0x00, sizeof(szRootName));
			strcpy(szRootName, szMainRootName);

			if(pstLocValNode->type == iNodeType)
			{
				for(iCnt = 0 ; iCnt < pstLocValNode->elemCnt; iCnt++)
				{
					if(strcmp(szRootName, pstLocValNode->elemList[iCnt].key) == SUCCESS)
					{
						pstVarLstInfoLoc = pstLocValNode->elemList[iCnt].valMetaInfo;

						if(iInsideDepth > 0)
						{
							if((cTmpPtr = strchr(cPtr, ':')) == NULL)
							{
								--iInsideDepth;
								memset(szRootName, 0x00, sizeof(szRootName));
								strcpy(szRootName, cPtr);
							}
							else
							{
								--iInsideDepth;
								memset(szRootName, 0x00, sizeof(szRootName));
								strncpy(szRootName, cPtr, cTmpPtr - cPtr);
								cTmpPtr = cTmpPtr+1;
							}

							kCnt = 0;
							while(pstVarLstInfoLoc[kCnt].nodeType != -1)
							{
								if(pstVarLstInfoLoc[kCnt].szNodeStr != NULL)
								{
									if(strcmp(pstVarLstInfoLoc[kCnt].szNodeStr, szRootName) == SUCCESS)
									{

										if(iInsideDepth > 0)
										{

											fillDHIValLstPtr(pstLocValNode->elemList[iCnt].value, pstVarLstInfoLoc[kCnt].nodeType, pstDestDHIValuelist, cTmpPtr, piNodeType);
											if(rv != SUCCESS)
											{
												debug_sprintf(szDbgMsg, "%s: Failed fill nodes for [%s]", __FUNCTION__, szRootName);
												RCHI_LIB_TRACE(szDbgMsg);
											}
											break;

										}
										else
										{
											iLocnodeType	= pstVarLstInfoLoc[kCnt].nodeType;
											pstTempValLstPtr = pstLocValNode->elemList[iCnt].value;
											bFound = RCHI_TRUE;
											break;
										}
										break;
									}
								}
								++kCnt;
							}
						}
						else
						{
							bFound = RCHI_TRUE;
							iLocnodeType = pstVarLstInfoLoc[0].nodeType;
							pstTempValLstPtr = pstLocValNode->elemList[iCnt].value;
							break;
						}
						break;
					}
				}

				if(bFound == RCHI_TRUE)
				{
					if(pstTempValLstPtr != NULL)
					{
						psttmpNode = pstTempValLstPtr->start;

						while(psttmpNode)
						{
							if(psttmpNode->type == iLocnodeType)
							{
								*piNodeType = iLocnodeType;
								/* Add the data in the linked list */
								rv = addDataNode(pstDestDHIValuelist, iLocnodeType,
										psttmpNode->elemList, psttmpNode->elemCnt);
								if(rv != SUCCESS)
								{
									debug_sprintf(szDbgMsg, "%s: Addition of data node FAILED",
																				__FUNCTION__);
									RCHI_LIB_TRACE(szDbgMsg);
									break;
								}
							}

							psttmpNode = psttmpNode->next;
						}

					}

				}
			}
			pstLocValNode= pstLocValNode->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: addKeyValDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addKeyValDataNode(VAL_LST_PTYPE valLstPtr, void* elemList, int iCnt)
{
	int				iRetVal				= SUCCESS;
	VAL_NODE_PTYPE	tmpNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//RCHI_LIB_TRACE(szDbgMsg);

	tmpNodePtr = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
	if(tmpNodePtr != NULL)
	{
		/* Initialize the allocated memory */
		memset(tmpNodePtr, 0x00, sizeof(VAL_NODE_STYPE));
		tmpNodePtr->elemList = elemList;
		tmpNodePtr->elemCnt	 = iCnt;
		/* Add the node to the list */
		if(valLstPtr->end == NULL)
		{
			/* Adding the first element */
			valLstPtr->start = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

			debug_sprintf(szDbgMsg, "%s: Adding first node", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Adding the nth element */
			valLstPtr->end->next = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

			//debug_sprintf(szDbgMsg, "%s: Adding nth node", __FUNCTION__);
			//RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Increment the node count */
		valLstPtr->valCnt += 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		iRetVal = FAILURE;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	//RCHI_LIB_TRACE(szDbgMsg);

	return iRetVal;
}
/*
 * ============================================================================
 * Function Name: getCorrectPaymentType
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getCorrectPaymentType(char* szRCPaymentType, char *szDHIPaymentType, char *szPinLessFlag)
{
	int		rv			= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:--------enter--------------", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(strcmp(szDHIPaymentType, "CREDIT") == SUCCESS)
	{
		strcpy(szRCPaymentType,"Credit");
	}
	else if(strcmp(szDHIPaymentType, "DEBIT") == SUCCESS)
	{
		if(szPinLessFlag != NULL)
		{
			int iFlag = atoi(szPinLessFlag);

			if(iFlag == 0)
			{
				strcpy(szRCPaymentType, "Debit");
			}
			else
			{
				strcpy(szRCPaymentType, "PLDebit");
			}
		}
		else
		{
			strcpy(szRCPaymentType, "Debit");
		}
	}
	else if(strcmp(szDHIPaymentType, "GIFT") == SUCCESS)
	{
		strcpy(szRCPaymentType, "Prepaid");
	}
	else if(strcmp(szDHIPaymentType, "EBT") == SUCCESS)
	{
		strcpy(szRCPaymentType, "EBT");
	}
	else if(strcmp(szDHIPaymentType, "CHECK") == SUCCESS)
	{
		strcpy(szRCPaymentType, "Check");
	}
	else if(strcmp(szDHIPaymentType, "ADMIN") == SUCCESS) //For Token Query command, we get ADMIN as the payment type, defaulting that to CREDIT
	{
		strcpy(szRCPaymentType, "Credit");
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: Invalid payment type",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg,"%s: Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTransType
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getTransType(char* szDHITranType, char *szRCTransType, char *szPaymentType)
{
	int		rv			= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: --------enter--------",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(szDHITranType == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: Null params passed",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return ERR_NO_FLD;
	}

	if(strcmp(szDHITranType, "PRE_AUTH") == SUCCESS )
	{
		if(strcmp(szPaymentType, "Prepaid") == SUCCESS)
		{
			strcpy(szRCTransType, "BalanceLock");
			rv = RC_BALANCE_LOCK;
		}
		else
		{
			strcpy(szRCTransType, "Authorization");
			rv = RC_AUTH;
		}
	}
	else if(strcmp(szDHITranType, "COMPLETION") == SUCCESS)
	{
		if(strcmp(szPaymentType, "Prepaid") == SUCCESS)
		{
			strcpy(szRCTransType, "RedemptionUnlock");
			rv = RC_REDEMPTION_UNLOCK;
		}
		else
		{
			strcpy(szRCTransType, "Completion");
			rv = RC_COMPLETION;
		}

	}
	else if( strcmp(szDHITranType, "POST_AUTH") == SUCCESS)
	{
			strcpy(szRCTransType, "Completion");
			rv = RC_VOICE_COMPLETION;
	}
	else if(strcmp(szDHITranType, "SALE") == SUCCESS)
	{
		if(strcmp(szPaymentType, "Prepaid") == SUCCESS)
		{
			strcpy(szRCTransType, "Redemption");
			rv = RC_REDEMPTION;
		}
		/* Changing Transaction type from Sale to Authorization for check based transactions */
		else if(strcmp(szPaymentType, "Check") == SUCCESS)
		{
			strcpy(szRCTransType, "Authorization");
			rv = RC_AUTH;
		}
		else
		{
			strcpy(szRCTransType, "Sale");
			rv = RC_SALE;
		}
	}
	else if(strcmp(szDHITranType, "CREDIT") == SUCCESS)
	{
		strcpy(szRCTransType, "Refund");
		rv = RC_REFUND;
	}
	else if(strcmp(szDHITranType, "VOID") == SUCCESS)
	{
		strcpy(szRCTransType, "Void");
		rv = RC_VOID;
	}
	else if(strcmp(szDHITranType, "ACTIVATE") == SUCCESS)
	{
		strcpy(szRCTransType, "Activation");
		rv = RC_ACTIVATE;
	}
	else if(strcmp(szDHITranType, "ADD_VALUE") == SUCCESS)
	{
		strcpy(szRCTransType, "Reload");
		rv = RC_RELOAD;
	}
	else if(strcmp(szDHITranType, "BALANCE") == SUCCESS)
	{
		strcpy(szRCTransType, "BalanceInquiry");
		rv = RC_BALANCE;
	}
	else if(strcmp(szDHITranType, "GIFT_CLOSE") == SUCCESS)
	{
		//strcpy(szRCTransType, "Cashout");
		strcpy(szRCTransType, "CashoutActiveStatus");
		rv = RC_CASH_OUT;
	}
	else if(strcmp(szDHITranType, "CUTOVER") == SUCCESS || strcmp(szDHITranType, "SITETOTALS") == SUCCESS)
	{
		strcpy(szRCTransType, "HostTotals");
		rv = RC_HOST_TOTALS;
	}
	else if(strcmp(szDHITranType, "TOKEN_QUERY") == SUCCESS)
	{
		strcpy(szRCTransType, "TATokenRequest");
		rv = RC_TOKEN_QUERY;
	}
	else if(strcmp(szDHITranType, "EMVADMIN") == SUCCESS)
	{
		strcpy(szRCTransType, "FileDownload");
		rv = RC_EMV_DOWNLOAD;
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: Invalid transaction type",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_UNSUPP_CMD;
	}

	debug_sprintf(szDbgMsg,"%s: The converted command is %s",__FUNCTION__, szRCTransType);
	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg,"%s: Returning %d",__FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectAmount
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCorrectAmount(char *szDHITranAmount, char* szRCTranAmount)
{
	int		rv			= SUCCESS;
	int 	iAmount		= 0;
	float	fAmount		= 0.0;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:--------enter--------------", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Converting the SSI value to RCHI format */

	fAmount = atof(szDHITranAmount) * 100;
	iAmount = fAmount;

	sprintf(szRCTranAmount,"%.03d",iAmount);

	debug_sprintf(szDbgMsg,"%s: Amount is %s",__FUNCTION__, szRCTranAmount);
	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg,"%s:Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: assignCardType
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int assignCardType(char *szRCHICardType, char *vspRegValue)
{
	int		rv			= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:--------enter--------------", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(strcmp(vspRegValue, "1") == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: It is VSP Registration command, so setting card type as MasterCard", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		strcpy(szRCHICardType, "MasterCard");
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Its not VSP Registration command so need to set the card type", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg,"%s: Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectCardType
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static char getFirstByteFromPAN(TRANDTLS_PTYPE pstDHIDtls)
{
	char		cFirstByte			= SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:--------enter--------------", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eTRACK_DATA].elementValue != NULL)
	{
		cFirstByte = pstDHIDtls[eTRACK_DATA].elementValue[0];
	}
	else if(pstDHIDtls[eACCT_NUM].elementValue != NULL)
	{
		cFirstByte = pstDHIDtls[eACCT_NUM].elementValue[0];
	}

	debug_sprintf(szDbgMsg, "%s:Returning [%c]", __FUNCTION__, cFirstByte);
	RCHI_LIB_TRACE(szDbgMsg);

	return cFirstByte;

}

/*
 * ============================================================================
 * Function Name: getCorrectCardType
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getCorrectCardType(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int		rv				= SUCCESS;
	char	cFirstByte		= 0;
	char*	szDHICardType 	= pstDHIDtls[ePAYMENT_MEDIA].elementValue;
	char*	szRCCardType	= pstRCHIDetails[RC_CARD_TYPE].elementValue;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:--------enter--------------", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(strcasecmp(szDHICardType, "VISA") == SUCCESS)
	{
		strcpy(szRCCardType, "Visa");
	}
	else if((strcasecmp(szDHICardType, "MASTERCARD") == SUCCESS) || ((strcasecmp(szDHICardType, "MC") == SUCCESS)))
	{
		strcpy(szRCCardType, "MasterCard");
	}
	else if(strcasecmp(szDHICardType, "AMEX") == SUCCESS)
	{
		strcpy(szRCCardType, "Amex");
	}
	else if((strcasecmp(szDHICardType, "DISCOVER") == SUCCESS) || ((strcasecmp(szDHICardType, "DISC") == SUCCESS)))
	{
		strcpy(szRCCardType, "Discover");
	}
	else if(strcasecmp(szDHICardType, "JCB") == SUCCESS)
	{
		strcpy(szRCCardType, "JCB");
	}
	else if((strcasecmp(szDHICardType, "DCCB") == SUCCESS) || ((strcasecmp(szDHICardType, "DINERS") == SUCCESS)))
	{
		strcpy(szRCCardType, "Diners");
	}
	else if(strcasecmp(szDHICardType, "GIFT") == SUCCESS)
	{
		strcpy(szRCCardType, "PPayCL");
	}
	else if(strcasecmp(szDHICardType, "DEBIT") == SUCCESS)
	{
		if(strcmp(pstDHIDtls[ePAYMENT_TYPE].elementValue, "CREDIT") == SUCCESS)
		{
			cFirstByte = getFirstByteFromPAN(pstDHIDtls);
			switch(cFirstByte)
			{
			case '3':
				strcpy(szRCCardType, "Amex");
				break;
			case '4':
				strcpy(szRCCardType, "Visa");
				break;
			case '5':
				strcpy(szRCCardType, "MasterCard");
				break;
			case '6':
				strcpy(szRCCardType, "Discover");
				break;
			}
		}
	}
	else if(strcasecmp(szDHICardType, "FSA") == SUCCESS)
	{
		/* We recieve Payment Media as FSA for FSA related transactions and we will have to give the correct card type
		 * depending on the first byte of the PAN.
		 */
		if(strcmp(pstDHIDtls[ePAYMENT_TYPE].elementValue, "CREDIT") == SUCCESS)
		{
			cFirstByte = getFirstByteFromPAN(pstDHIDtls);
			switch(cFirstByte)
			{
			case '3':
				strcpy(szRCCardType, "Amex");
				break;
			case '4':
				strcpy(szRCCardType, "Visa");
				break;
			case '5':
				strcpy(szRCCardType, "MasterCard");
				break;
			case '6':
				strcpy(szRCCardType, "Discover");
				break;
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: Invalid card type",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_INV_FLD_VAL;
	}

	debug_sprintf(szDbgMsg,"%s: Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCCVIndicator
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void getCCVIndicator(char *szCvv2, char *szCvvIndicator)
{

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	if(strlen(szCvv2) > 0)
	{
		strcpy(szCvvIndicator, "Prvded");
	}
	else
	{
		strcpy(szCvvIndicator, "NtPrvded");
	}
	debug_sprintf(szDbgMsg,"%s: Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: getTrackData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void getTrackData(char *szRCTrackData, char *szDHITrackData)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	strcpy(szRCTrackData, szDHITrackData);

	debug_sprintf(szDbgMsg,"%s: Track data found...Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getCardExpirationData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void getCardExpirationData(char *szRCCardExpDate, char *szDHIExpMonth, char *szDHIExpYear)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	strcpy(szRCCardExpDate, "20");
	strcat(szRCCardExpDate,szDHIExpYear);
	strcat(szRCCardExpDate,szDHIExpMonth);

	debug_sprintf(szDbgMsg,"%s: Card Expiration date:%s...Returning",__FUNCTION__, szRCCardExpDate);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getACI
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void getVisaACI(char *szTemp)
{
		strcpy(szTemp, "Y");
}
/*
 * ============================================================================
 * Function Name: getDevTypeInd
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

void getDevTypeInd(RCTRANDTLS_PTYPE pstRCHIDetails,TRANDTLS_PTYPE pstDHIDtls)
{
	if(!strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "MasterCard"))
	{
		if(!strcmp(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, "071") || !strcmp(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, "821")
				||!strcmp(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, "911"))
		{
			if(pstDHIDtls[ePAYPASS_TYPE].elementValue != NULL)
			{
				strcpy(pstRCHIDetails[RC_DEV_TYPE_IND].elementValue, pstDHIDtls[ePAYPASS_TYPE].elementValue);
			}
			else
			{
				strcpy(pstRCHIDetails[RC_DEV_TYPE_IND].elementValue, "0");
			}
		}
	}
}
/*
 * ============================================================================
 * Function Name: getEncryptionTarget
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void getEncryptionTarget(char *szEncrptTgt, int iIndicator)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	if(iIndicator == 0)
	{
		strcpy(szEncrptTgt, "PAN");
	}
	if(iIndicator == 1)
	{
		strcpy(szEncrptTgt, "Track1");
	}
	else if(iIndicator == 2)
	{
		strcpy(szEncrptTgt, "Track2");
	}

	debug_sprintf(szDbgMsg,"%s:Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getEncryptionBlock
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void getEncryptionBlock(char *szEncrptBlk, TRANDTLS_PTYPE pstDHIDtls)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s:----enter-----",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eTRACK_DATA].elementValue != NULL)
	{
		strcpy(szEncrptBlk, pstDHIDtls[eTRACK_DATA].elementValue);
	}
	else
	{
		if(pstDHIDtls[eACCT_NUM].elementValue != NULL &&
		   pstDHIDtls[eEXP_MONTH].elementValue != NULL &&
		   pstDHIDtls[eEXP_YEAR].elementValue != NULL)
		{
			strcpy(szEncrptBlk, pstDHIDtls[eEXP_YEAR].elementValue);
			strcat(szEncrptBlk, pstDHIDtls[eEXP_MONTH].elementValue);
			strcat(szEncrptBlk, pstDHIDtls[eACCT_NUM].elementValue);
		}
	}

	debug_sprintf(szDbgMsg,"%s:Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getPOSConditionCode
 *
 *
 * Description	: This function will fill the szPOSConditionCode buffer
 *
 *
 * Input Params	: POS Condition Code Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getPOSConditionCode(char *szPOSConditionCode, int iPresentFlag)
{

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	switch(iPresentFlag)
	{
		case 1:
		case 2:
			strcpy(szPOSConditionCode, "71");
			break;

		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			strcpy(szPOSConditionCode, "00");
			break;

		default:	debug_sprintf(szDbgMsg,"%s:Should not come here",__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg,"%s:Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: getPOSEntryMode
 *
 * Description	: This function will fill the szPOSEntryMode Buffer
 *
 *
 * Input Params	: POS Entry Mode Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getPOSEntryMode(char *szPOSEntryMode, int iPresentFlag)
{

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	switch(iPresentFlag)
	{
	case 1:
	case 2:
		strcpy(szPOSEntryMode, "011");
		break;

	case 3:
		strcpy(szPOSEntryMode, "901");
		break;

	case 4:
		strcpy(szPOSEntryMode, "911");
		break;
		/*For EMV Transactions we expect present flag to be 5/6/7 for EMV contact/contactless/fallback cases */
	case 5:
		strcpy(szPOSEntryMode,"051");
		break;
	case 6:
		strcpy(szPOSEntryMode,"071");
		break;
	case 7:
		strcpy(szPOSEntryMode,"801");
		break;

	default:
		debug_sprintf(szDbgMsg,"%s:Should not come here",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg,"%s:Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getTermEntryCapablt
 *
 *
 * Description	: This function will fill the szTermEntryCapablt buffer
 *
 *
 * Input Params	: Terminal Entry Capability Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTermEntryCapablt(char *szTermEntryCapablt, int iPresentFlag)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(isEMVEnabledInDevice())
	{
		strcpy(szTermEntryCapablt, "04");

		/* Daivik:11/3/2016 - As per FD we need to set Terminal Cap as 06 when EMV CTLS is enabled in terminal and 04 when only EMV is enabled in terminal */
		if(isEMVCTLSEnabledInDevice())
		{
			strcpy(szTermEntryCapablt, "06");
		}
//		switch(iPresentFlag)
//		{
//			/* 11/1/2016 Daivik : When EMV is enabled , if Present flag indicates MSD Contactless, then Terminal Entry Capability is being set to 11 ( Contactless Magnetic Stripe)
//			 * If present flag indicates EMV Contactless - then we set Term Ent Cap to 6 ( Contactless Chip/RFID )
//			 */
//			case 4:
//			strcpy(szTermEntryCapablt, "11");
//			break;
//
//			case 6:
//			strcpy(szTermEntryCapablt, "06");
//			break;
//		}
	}
	else
	{
		switch(iPresentFlag)
		{
		case 1:
		case 2:
			strcpy(szTermEntryCapablt, "03");
			break;

		case 3:
			strcpy(szTermEntryCapablt, "02");
			break;

		case 4:
			strcpy(szTermEntryCapablt, "11");
			break;
		case 5:
		case 6:
		case 7:
			strcpy(szTermEntryCapablt, "04");
			break;

		default:
			debug_sprintf(szDbgMsg,"%s:Should not come here",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg,"%s:Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getPresentFlag
 *
 *
 * Description	: This function will fill the szTermEntryCapablt buffer
 *
 *
 * Input Params	: Terminal Entry Capability Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
int getPresentFlag(char *szPresentFlag)
{
	int iPresentFlag = 0;

	if(szPresentFlag == NULL)
	{
		return ERR_FLD_REQD;
	}

	iPresentFlag = atoi(szPresentFlag);
	switch(iPresentFlag)
	{
	case 1:
	case 2:
	case 3:
	case 4:
		return iPresentFlag;
		/* Present flags with value 5,6,7 are returned for EMV Contact/Contact Less /MSR Fallback cases */
	case 5:
	case 6:
	case 7:
		return iPresentFlag;
	default:
		return ERR_INV_FLD;
	}
}

/*
* ============================================================================
* Function Name: getEmbossedPANFromTrackData
*
* Description       :
*
* Input Params      :
*
* Output Params:
* ============================================================================
*/
void getEmbossedPANFromTrackData(char *pszTrackData, char *pszEmbossedPAN)
{
	char*	cCurPtr                         = NULL;
	char* 	cNxtPtr                         = NULL;
	char	szPartA[5]						= "";
	char	szPartB[10]						= "";
	char	szPartC[5]						= "";

#ifdef DEBUG
	char   szDbgMsg[256] = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pszTrackData == NULL || pszEmbossedPAN == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param is NULL!!!", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}
	/*
	 * Please refer to UMF document about how to extract Embossed card number
	 * from the track2
	 * To construct the embossed number from Track 2 data
	 * take part (A) i.e. Partial serial number dropping the second digit
	 * Concatenate parts (B) i.e. Partial Account Number and (C) i.e. Complete checksum
	 */

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: TrackData [%s]", DEVDEBUGMSG, __FUNCTION__, pszTrackData);
	RCHI_LIB_TRACE(szDbgMsg);
#endif


	cCurPtr = pszTrackData;
	cNxtPtr = strchr(cCurPtr, '=');
	if(cNxtPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Separator in Track2 data", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}

	/* store the partial account number */
	memset(szPartB, 0x00, sizeof(szPartB));

	cCurPtr = cCurPtr + 6; //It will be pointing to first byte of partial account data

	memcpy(szPartB, cCurPtr, 9); //partial account number would be 9 bytes

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: PartB [%s]", DEVDEBUGMSG, __FUNCTION__, szPartB);
#else
	debug_sprintf(szDbgMsg,"%s: Retrieved PartB from the Track Data",__FUNCTION__);
#endif
	RCHI_LIB_TRACE(szDbgMsg);

	/* store the Partial serial number first four bytes of discretionary data */
	memset(szPartA, 0x00, sizeof(szPartA));

	cCurPtr = cNxtPtr + 13; //It will be pointing to first byte of discretionary data

	memcpy(szPartA, cCurPtr, 4); //partial serial number would be 4 bytes

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: PartA [%s]", DEVDEBUGMSG, __FUNCTION__, szPartA);
#else
	debug_sprintf(szDbgMsg,"%s: Retrieved PartA from the Track Data",__FUNCTION__);
#endif
	RCHI_LIB_TRACE(szDbgMsg);

	cCurPtr = cCurPtr + 4;

	memset(szPartC, 0x00, sizeof(szPartC));

	memcpy(szPartC, cCurPtr, 4); //check sum would be 4 bytes

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: PartC [%s]", DEVDEBUGMSG, __FUNCTION__, szPartC);
#else
	debug_sprintf(szDbgMsg,"%s: Retrieved PartC from the Track Data",__FUNCTION__);
#endif
	RCHI_LIB_TRACE(szDbgMsg);

	sprintf(pszEmbossedPAN, "%c%c%c%s%s", szPartA[0], szPartA[2], szPartA[3], szPartB, szPartC);

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: Embossed Account Number [%s]", DEVDEBUGMSG, __FUNCTION__, pszEmbossedPAN);
#else
	debug_sprintf(szDbgMsg,"%s: Retrieved Embossed from the Track Data", __FUNCTION__);
#endif
	RCHI_LIB_TRACE(szDbgMsg);


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return;
}

/*
* ============================================================================
* Function Name: getPANandEXPFromTrackData
*
* Description       :
*
* Input Params      :
*
* Output Params:
* ============================================================================
*/
void getPANandEXPFromTrackData(char *pszTrackData, int iTrackIndicator, char *pszPAN, char* pszExpDate)
{
	char	szName[20]						 = "";
	char*	cCurPtr                          = NULL;
	char* 	cNxtPtr                          = NULL;

#ifdef DEBUG
	char   szDbgMsg[256] = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pszTrackData == NULL || pszPAN == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param is NULL!!!", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}

	cCurPtr = pszTrackData;

	if(iTrackIndicator == 1) //Track1 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving PAN from Track1", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '^' sentinel in their track 1 information */
			strcpy(pszPAN, cCurPtr);
		}
		else
		{
			memcpy(pszPAN, cCurPtr, cNxtPtr - cCurPtr);
		}

		/*
		 * -----------
		 * Store Name
		 * -----------
		 */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid track; no Name", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		memcpy(szName, cCurPtr, cNxtPtr - cCurPtr);

		/*
		 * ----------------------
		 * Store Expiration Date
		 * ----------------------
		 */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, '\0');
		if((cNxtPtr - cCurPtr) < 4)
		{
			debug_sprintf(szDbgMsg,"%s: Invalid track; no exp dt",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		memcpy(pszExpDate, cCurPtr, 4);
	}
	else if(iTrackIndicator == 2) //Track 2 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving PAN from Track2", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		cNxtPtr = strchr(cCurPtr, '=');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '=' sentinel in their track 2 */
			strcpy(pszPAN, cCurPtr);
		}
		else
		{
			memcpy(pszPAN, cCurPtr, cNxtPtr - cCurPtr);
		}

		/*
		 * ------------------
		 * Store expiry date
		 * ------------------
		 */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cNxtPtr, '\0');
		if(cNxtPtr - cCurPtr < 4)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid track, exp date missing",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

		}

		memcpy(pszExpDate, cCurPtr, 4);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid track to retrieve PAN!!!", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: PAN [%s]", DEVDEBUGMSG, __FUNCTION__, pszPAN);
#else
	debug_sprintf(szDbgMsg,"%s: Retrived PAN from the Track Data",__FUNCTION__);
#endif
	RCHI_LIB_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: fillRCDtlsforTOR
 *
 *
 * Description	: This function will fill the szTermEntryCapablt buffer
 *
 *
 * Input Params	: Terminal Entry Capability Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
int fillRCDtlsforTOR(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails, int iCmd)
{
	int			rv 				= SUCCESS;
	char		szPAN[30]		= "";
	char		szExpDate[5]	= "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	switch(iCmd)
	{
	case RC_VOID:
		if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid") == SUCCESS)
		{
			/* Copying the reversal reason code for void if it is prepaid
			 * TORVoid is supoorted only for the prepaid cards
			 *  */
			strcpy(pstRCHIDetails[RC_REVERSL_RSN_CODE].elementValue, "TORVoid");
			break;
		}

	case RC_BALANCE:
	case RC_HOST_TOTALS:
		/*
		 * Time out reversal not supported for all these transactions
		 * Void(non prepaid), Balance, HostTotals
		 * */

		debug_sprintf(szDbgMsg, "%s: Cannot do time out reversal for this transaction ", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: --- Returning rv = [%d] ---", __FUNCTION__, rv);
		RCHI_LIB_TRACE(szDbgMsg);

		return ERR_RESP_TO;

	default:
		/* For all other commands copying the reversal reason code as Timeout*/
		strcpy(pstRCHIDetails[RC_REVERSL_RSN_CODE].elementValue, "Timeout");
	}

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid") != SUCCESS) //for prepaid this field is not  required
	{
		/* Filling the additional amount details*/
		strcpy(pstRCHIDetails[RC_ADDTL_TA_AMT].elementValue, pstRCHIDetails[RC_TRAN_AMOUNT].elementValue);
		getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_TA_AMT_CRNCY].elementValue);
		strcpy(pstRCHIDetails[RC_ADDTL_TA_AMT_TYPE].elementValue, "TotalAuthAmt");
	}

	/*Filling the orig auth group*/
	strcpy(pstRCHIDetails[RC_ORIG_LOCAL_DATE_TIME].elementValue, pstRCHIDetails[RC_LOCAL_DATE_TIME].elementValue);
	strcpy(pstRCHIDetails[RC_ORIG_TRANSM_DATE_TIME].elementValue, pstRCHIDetails[RC_TRANSM_DATE_TIME].elementValue);
	strcpy(pstRCHIDetails[RC_ORIG_STAN].elementValue, pstRCHIDetails[RC_STAN].elementValue);

	/* Updating the existing details for time out reversal*/
	getTime(pstRCHIDetails[RC_LOCAL_DATE_TIME].elementValue, LOCALTIME);
	getTime(pstRCHIDetails[RC_TRANSM_DATE_TIME].elementValue, GMTTIME);
	getSTAN(pstRCHIDetails[RC_STAN].elementValue);

	strcpy(pstRCHIDetails[RC_ORIG_AUTH_ID].elementValue, "");
	strcpy(pstRCHIDetails[RC_ORIG_RESP_CODE].elementValue, "");

	/* Resetting pin block and key serial number to NULL */
	if(strlen(pstRCHIDetails[RC_PIN_BLOCK].elementValue) > 0)
	{
		memset(pstRCHIDetails[RC_PIN_BLOCK].elementValue, 0x00, sizeof(pstRCHIDetails[RC_PIN_BLOCK].elementValue));
	}

	if(strlen(pstRCHIDetails[RC_KSN].elementValue) > 0)
	{
		memset(pstRCHIDetails[RC_KSN].elementValue, 0x00, sizeof(pstRCHIDetails[RC_KSN].elementValue));
	}

	memset(pstRCHIDetails[RC_CARD_LEVEL_RESULT].elementValue, 0x00, sizeof(pstRCHIDetails[RC_CARD_LEVEL_RESULT].elementValue));
	memset(pstRCHIDetails[RC_AMEX_POS_DATA].elementValue, 0x00, sizeof(pstRCHIDetails[RC_AMEX_POS_DATA].elementValue));
	memset(pstRCHIDetails[RC_AMEX_TRAN_ID].elementValue, 0x00, sizeof(pstRCHIDetails[RC_AMEX_TRAN_ID].elementValue));

	if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid") == SUCCESS)
	{
		memset(pstRCHIDetails[RC_MERCH_CAT_CODE].elementValue, 0x00, strlen(pstRCHIDetails[RC_MERCH_CAT_CODE].elementValue)); //Need not send Merchant Cat code

		if(strlen(pstRCHIDetails[RC_TRACK1_DATA].elementValue) > 0)
		{
			/*
			 * Praveen_P1: In the UMF document, extraction of Embossed card number is given only from the
			 * Track2, thats why we are extracting the PAN form the track here
			 * Ideally we should not enter this condition, if we have entered
			 * then it is not correct
			 */
			memset(szPAN, 0x00, sizeof(szPAN));
			memset(szExpDate, 0x00, sizeof(szExpDate));

			getPANandEXPFromTrackData(pstRCHIDetails[RC_TRACK1_DATA].elementValue, 1, szPAN, szExpDate);

			strcpy(pstRCHIDetails[RC_ACCT_NUM].elementValue, szPAN);

			memset(pstRCHIDetails[RC_TRACK1_DATA].elementValue, 0x00, sizeof(pstRCHIDetails[RC_TRACK1_DATA].elementValue));
		}
		else if(strlen(pstRCHIDetails[RC_TRACK2_DATA].elementValue) > 0)
		{
			memset(szPAN, 0x00, sizeof(szPAN));

			getPANFromTrackData(pstRCHIDetails[RC_TRACK2_DATA].elementValue, 2, szPAN);

			strcpy(pstRCHIDetails[RC_ACCT_NUM].elementValue, szPAN);

			memset(pstRCHIDetails[RC_TRACK2_DATA].elementValue, 0x00, sizeof(pstRCHIDetails[RC_TRACK2_DATA].elementValue));
		}

		memset(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, 0x00, sizeof(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue));

		strcpy(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, "011");
	}
	else if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Debit") == SUCCESS ||
			strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "EBT") == SUCCESS)
	{
		strcpy(pstRCHIDetails[RC_ENCRPT_TARGET].elementValue, "PAN");
	}

	/* DevTypeIndicator for Contactless Mastercard transactions */
	getDevTypeInd(pstRCHIDetails,pstDHIDtls);

	debug_sprintf(szDbgMsg, "%s: --- Returning rv = [%d] ---", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillRCDtlsforFollowOnTran
 *
 *
 * Description	: This function will fill the szTermEntryCapablt buffer
 *
 *
 * Input Params	: Terminal Entry Capability Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
int fillRCDtlsforFollowOnTran(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int			rv					= SUCCESS;
	int			iFound				= RCHI_FALSE;
	int			iCount				= 0;
	int			iIndex				= 0;
	int			iTotal				= 0;
	//char 		szIssuerScripRes[10] = "";
	char*		pszBuffer			= NULL;
	RCHI_BOOL   bVoidTran 			= RCHI_FALSE;
	FILE*		fptr				= NULL;
	int			iRCHIIndices[]		= {
										RC_TRAN_TYPE, RC_ORDER_NUMBER, RC_ORIG_LOCAL_DATE_TIME, RC_ORIG_TRANSM_DATE_TIME, RC_ORIG_STAN,
										RC_POS_ENTRY_MODE, RC_POS_COND_CODE, RC_TERM_ENTRY_CAPABLT, RC_CARD_EXP_DATE, RC_CARD_TYPE, RC_TRAN_AMOUNT,
										RC_ADDTL_AMT_CASHBK, RC_ADDTL_AMT_TAXAMT, RC_CARD_TOKEN, RC_ACCT_NUM, RC_ORIG_AUTH_ID, RC_ORIG_RESP_CODE,
										RC_HOLD_INFO, RC_PARTIAL_AUTH, RC_TRACK1_DATA, RC_TRACK2_DATA, RC_AUTH_CHARACTERISTICS_IND, RC_CARD_LEVEL_RESULT,
										RC_TRAN_ID, RC_BANK_NET_DATA, RC_CCV_ERROR_CODE, RC_POSENTRYMODE_CHG, RC_DISC_PROC_CODE, RC_DISC_POS_ENTRY,
										RC_DISC_RESP_CODE, RC_DISC_POS_DATA, RC_DISC_TRAN_QUALIF, RC_DISC_NRID, RC_AMEX_POS_DATA, RC_AMEX_TRAN_ID,
										RC_TAX_AMOUNT, RC_TAX_IND, RC_PURCHASE_IDFR, RC_PC_ORDER_NUM, RC_DISC_AMOUNT, RC_FREIGHT_AMOUNT, RC_DUTY_AMOUNT, RC_DEST_POSTAL_CODE, RC_SHIP_POSTAL_CODE,
										RC_DEST_COUNTRY_CODE, RC_MERCH_TAX_ID, RC_PROD_DESC, RC_PC3_ADD, RC_ADDTL_AMT_HLTCARE, RC_ADDTL_AMT_DENTAL, RC_ADDTL_AMT_VISION, RC_ADDTL_AMT_CLINIC,
										RC_ADDTL_AMT_PRESCRIPION, RC_EBT_TYPE, RC_X_CODE_RESP, RC_ATH_NTWK_ID, RC_CARD_SEQ_NUM, RC_DEV_TYPE_IND, RC_DCC_IND,RC_DCC_TRAN_TIME_ZONE,
										RC_DCC_CARDHOLDER_CURR_AMT, RC_DCC_CARDHOLDER_CONV_RATE, RC_DCC_CARDHOLDER_CURR_CODE, RC_PLESS_POS_DEB_FLG
									  };
	int			iDHIIndices[]		= {
										eEMV_TAGS, eEMV_TAGS_RESP
									  };
	int 		iPL2GrpIndices[]	= {
										RC_TAX_AMOUNT, RC_TAX_IND, RC_PURCHASE_IDFR, RC_PC_ORDER_NUM, RC_DISC_AMOUNT,
										RC_FREIGHT_AMOUNT, RC_DUTY_AMOUNT, RC_DEST_POSTAL_CODE, RC_SHIP_POSTAL_CODE,
										RC_DEST_COUNTRY_CODE, RC_MERCH_TAX_ID, RC_PROD_DESC, RC_PC3_ADD
									   }; /* In case there are any new fields in PL2 group, please add it here for removing this group from void transactions */

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{

		if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue,"Void") == SUCCESS)
		{
			bVoidTran = RCHI_TRUE;
		}

		getCorrectPaymentType(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, pstDHIDtls[ePAYMENT_TYPE].elementValue, pstDHIDtls[ePINLESSDEBIT].elementValue);

		if(pstDHIDtls[eCTROUTD].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: CTROUTD not found for Void. Returning", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD_VAL;
			break;
		}

		fptr = fopen(TRAN_RECORDS, "r");
		if(fptr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: --- File not available", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_CTROUTD;

			break;
		}

		while((pszBuffer = csvgetline(fptr)) != NULL)
		{
			pszBuffer = csvfield(iCount);
			if(pszBuffer != NULL)
			{
				if(strcmp(pszBuffer, pstDHIDtls[eCTROUTD].elementValue) == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Found the CTROUTD in the records", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					iFound = RCHI_TRUE;
					iCount++;
					break;
				}
			}
		}

		if(iFound == RCHI_FALSE)
		{
			debug_sprintf(szDbgMsg, "%s: CTROUTD not found in the records", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_CTROUTD;

			break;
		}

		/*Copying the original reference number into the RC details*/
		strcpy(pstRCHIDetails[RC_REFERENCE_NUMBER].elementValue, pszBuffer);

		/* NOTE: WE ALWAYS FILL THE RC FIELDS FIRST AND THEN FILL THE DHI FIELDS FROM
		 * TRANSRECORDS.TXT
		 */
		iTotal = sizeof(iRCHIIndices)/sizeof(int);
		for(iIndex = 0; iIndex < iTotal; iIndex++)
		{
			pszBuffer = csvfield(iCount++);
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: RC Field %d - %s", __FUNCTION__, iIndex, pszBuffer);
			RCHI_LIB_TRACE(szDbgMsg);
#endif

			if(pszBuffer && strlen(pszBuffer) > 0)
			{
				strcpy(pstRCHIDetails[iRCHIIndices[iIndex]].elementValue, pszBuffer);
			}
		}
		/* Only for Completion we will receive EMV_TAGS which will contain the issuer script results
		 * So we need to take the copy of this information and free the data, since we are using the
		 * same field to send the actual EMV tags which were part of Authorization Request.
//		 */
//		if(pstDHIDtls[eEMV_TAGS].elementValue && strcmp(pstDHIDtls[eCOMMAND].elementValue, "COMPLETION") == SUCCESS)
//		{
//			strcpy(szIssuerScripRes,pstDHIDtls[eEMV_TAGS].elementValue);
//			free(pstDHIDtls[eEMV_TAGS].elementValue);
//			pstDHIDtls[eEMV_TAGS].elementValue = NULL;
//		}
		/*Copying DHI Fields from the File into DHI Structure*/
		iTotal = sizeof(iDHIIndices)/sizeof(int);
		for(iIndex = 0; iIndex < iTotal; iIndex++)
		{
			pszBuffer = csvfield(iCount++);

			/* We avoid copying from TranRecord.txt to DHI , if DHI fields are being recieved in the SSI Request itself.
			 * For Reversal in presence of the card we get the 2nd Gen AC tags in SSI Request . So we need to send those tags
			 * to the host and not the ones saved in TranRecord.txt
			 */
			if(pszBuffer && strlen(pszBuffer) > 0 && !pstDHIDtls[iDHIIndices[iIndex]].elementValue)
			{
				debug_sprintf(szDbgMsg, "%s: DHI Field %d - %s", __FUNCTION__, iIndex, pszBuffer);
				RCHI_LIB_TRACE(szDbgMsg);
				pstDHIDtls[iDHIIndices[iIndex]].elementValue = (char *)malloc(strlen(pszBuffer)+1);
				memset(pstDHIDtls[iDHIIndices[iIndex]].elementValue, 0x00, strlen(pszBuffer)+1);
				strcpy(pstDHIDtls[iDHIIndices[iIndex]].elementValue, pszBuffer);
			}
		}
		if(pstDHIDtls[eEMV_TAGS_RESP].elementValue)
		{
			if((strcmp(pstDHIDtls[eCOMMAND].elementValue, "COMPLETION") == SUCCESS))
			{
				debug_sprintf(szDbgMsg, "%s: This is a completion command and we have EMV Tags which were returned in Authorization response,so add these tags to current EMV Tags", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				//rv = getCompletionEMVGrp(pstDHIDtls,szIssuerScripRes);
				rv = getCompletionEMVGrp(pstDHIDtls);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Could not obtain Completion Group", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = ERR_SYSTEM;
					break;
				}
			}

			free(pstDHIDtls[eEMV_TAGS_RESP].elementValue);
			pstDHIDtls[eEMV_TAGS_RESP].elementValue = NULL;
		}
		/*
		 * Done with fetching the details and updating the records accordingly
		 * Now we need to fill the other required details.
		 */
		
		getRCGenReqDtls(pstRCHIDetails);

		if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,"Prepaid") ==  SUCCESS)
		{
			/* Get Alternate merchant id */
			getAlternateMerchID(pstRCHIDetails[RC_ALT_MERCH_ID].elementValue);

			/* Making the POS Entry mode as manual for Prepaid void*/
			if(strcmp(pstRCHIDetails[RC_REVERSL_RSN_CODE].elementValue, "Void") == SUCCESS)
			{
				strcpy(pstRCHIDetails[RC_POS_ENTRY_MODE].elementValue, "011");

				strcpy(pstRCHIDetails[RC_AUTH_ID].elementValue, "");
				strcpy(pstRCHIDetails[RC_PARTIAL_AUTH].elementValue, "");
				strcpy(pstRCHIDetails[RC_TRACK1_DATA].elementValue, "");
				strcpy(pstRCHIDetails[RC_TRACK2_DATA].elementValue, "");
				if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "RedemptionUnlock") != SUCCESS)
				{
					strcpy(pstRCHIDetails[RC_HOLD_INFO].elementValue, "");
				}
			}

			/* Filling details for BalnceLock */
			if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue, "BalanceLock") == SUCCESS)
			{

				/* Checking if Account Number field needs to be sent or not in case of RedemptionUnlock */
				if(strcmp(pstRCHIDetails[RC_REVERSL_RSN_CODE].elementValue, "Void"))
				{
					if(strlen(pstRCHIDetails[RC_TRACK1_DATA].elementValue) > 0 || strlen(pstRCHIDetails[RC_TRACK2_DATA].elementValue) > 0 )
					{
						memset(pstRCHIDetails[RC_ACCT_NUM].elementValue, 0x00, sizeof(pstRCHIDetails[RC_ACCT_NUM].elementValue));
					}

					/* Checking if HoldInfo field needs to be filled by default value or not in case of RedemptionUnlock */
					if(strlen(pstRCHIDetails[RC_HOLD_INFO].elementValue) == 0)
					{
						strcpy(pstRCHIDetails[RC_HOLD_INFO].elementValue, "0001");
					}
				}
			}
		}

		/* Get Term Cat Code */
		getTermCatCode(pstRCHIDetails[RC_TERM_CAT_CODE].elementValue);

		/* Get Transaction currency */
		getTranCurrency(pstRCHIDetails[RC_TRANS_CRNCY].elementValue);

		/* Get Term loc indicator */
		getTermLocationIndicator(pstRCHIDetails[RC_TERM_LOC_IND].elementValue);

		/* Get Card Capture capability */
		getCardCaptureCapablt(pstRCHIDetails[RC_CARD_CAPT_CAPABLT].elementValue);

		/* Get POS ID */
		getPOSID(pstDHIDtls, pstRCHIDetails);

		if(isVSPEnabledInDevice() == RCHI_TRUE)
		{
			/* Get Security Level */
			getSecurityLvl(pstRCHIDetails[RC_SECURITY_LVL].elementValue);

			/* Get token type */
			getTokenType(pstRCHIDetails[RC_TOKEN_TYPE].elementValue);
		}

		if(pstDHIDtls[ePIN_BLOCK].elementValue != NULL)
		{
			free(pstDHIDtls[ePIN_BLOCK].elementValue);
			pstDHIDtls[ePIN_BLOCK].elementValue = NULL;
		}

		if(pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue != NULL)
		{
			free(pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue);
			pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue = NULL;
		}

		if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid"))
		{
			/* Filling the additional amount details for the void tran*/

			strcpy(pstRCHIDetails[RC_ADDTL_TA_AMT].elementValue, pstRCHIDetails[RC_TRAN_AMOUNT].elementValue);

			getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_TA_AMT_CRNCY].elementValue);

			/* Hard coding the values for the void function*/
			strcpy(pstRCHIDetails[RC_ADDTL_TA_AMT_TYPE].elementValue, "TotalAuthAmt");

			/* Get Merchant Category code */
			getMerchCatCode(pstRCHIDetails[RC_MERCH_CAT_CODE].elementValue);
		}

		/* Checking if the command is completion*/
		if(strcmp(pstDHIDtls[eCOMMAND].elementValue, "COMPLETION") == SUCCESS)
		{
			if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid") != SUCCESS)
			{
				strcpy(pstRCHIDetails[RC_ADDTL_FA_AMT].elementValue, pstRCHIDetails[RC_TRAN_AMOUNT].elementValue);

				getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_FA_AMT_CRNCY].elementValue);

				/* Hard coding the values for the void function*/
				strcpy(pstRCHIDetails[RC_ADDTL_FA_AMT_TYPE].elementValue, "FirstAuthAmt");
/* KranthiK1:
 * Sending the card expiry date based on the new requirement from First Data
 */
#if 0
				if(strlen(pstRCHIDetails[RC_CARD_EXP_DATE].elementValue) > 0)
				{
					strcpy(pstRCHIDetails[RC_CARD_EXP_DATE].elementValue, "");
				}
#endif
			}
			else
			{
				/* Below fields should not be sent for completion in prepaid*/
			//	strcpy(pstRCHIDetails[RC_ORIG_AUTH_ID].elementValue, "");
				strcpy(pstRCHIDetails[RC_ORIG_LOCAL_DATE_TIME].elementValue, "");
				strcpy(pstRCHIDetails[RC_ORIG_TRANSM_DATE_TIME].elementValue, "");
			//	strcpy(pstRCHIDetails[RC_ORIG_STAN].elementValue, "");
				strcpy(pstRCHIDetails[RC_ORIG_RESP_CODE].elementValue, "");
			}

			getCorrectAmount(pstDHIDtls[eTRANS_AMOUNT].elementValue, pstRCHIDetails[RC_TRAN_AMOUNT].elementValue);

		}

		if(strcmp(pstRCHIDetails[RC_REVERSL_RSN_CODE].elementValue, "Void") == SUCCESS)
		{
			strcpy(pstRCHIDetails[RC_CARD_LEVEL_RESULT].elementValue, "");

			/* Removing Purchase card lvl2 group for void transactions */
			iIndex = 0;
			iTotal = sizeof(iPL2GrpIndices)/sizeof(int);

			while(iIndex < iTotal)
			{
				memset(pstRCHIDetails[iPL2GrpIndices[iIndex]].elementValue, 0x00, sizeof(pstRCHIDetails[iPL2GrpIndices[iIndex]].elementValue));
				iIndex++;
			}
		}

		if(strlen(pstRCHIDetails[RC_ADDTL_AMT_CASHBK].elementValue) > 0)
		{
			getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_AMT_CRNCY_CASHBK].elementValue);

			/* Hard coding the values for the void function*/
			strcpy(pstRCHIDetails[RC_ADDTL_AMT_TYPE_CASHBK].elementValue, "Cashback");
		}

		if(strlen(pstRCHIDetails[RC_ADDTL_AMT_TAXAMT].elementValue) > 0)
		{
			getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_AMT_CRNCY_TAXAMT].elementValue);

			/* Hard coding the values for the void function*/
			strcpy(pstRCHIDetails[RC_ADDTL_AMT_TYPE_TAXAMT].elementValue, "Tax");
		}
		//If it is Void Transaction , then we should send the payment type depending on the pinless debit value that we recieved
		if(bVoidTran)
		{
			if(strlen(pstRCHIDetails[RC_PLESS_POS_DEB_FLG].elementValue))
			{
				debug_sprintf(szDbgMsg, "%s: Stored Pin less debit value is %s", __FUNCTION__,pstRCHIDetails[RC_PAYMENT_TYPE].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
				if(strcmp(pstRCHIDetails[RC_PLESS_POS_DEB_FLG].elementValue,"C") == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Changing the Payment type from %s to Credit", __FUNCTION__,pstRCHIDetails[RC_PAYMENT_TYPE].elementValue);
					RCHI_LIB_TRACE(szDbgMsg);
					memset(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,0x00,sizeof(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue));
					strcpy(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,"Credit");
				}
				else if(strcmp(pstRCHIDetails[RC_PLESS_POS_DEB_FLG].elementValue,"D") == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Changing the Payment type from :%s to Debit", __FUNCTION__,pstRCHIDetails[RC_PAYMENT_TYPE].elementValue);
					RCHI_LIB_TRACE(szDbgMsg);
					memset(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,0x00,sizeof(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue));
					strcpy(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,"Debit");
				}
			}
		}
		/* Making the card type as empty in the case of debit transaction*/
		if(strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Debit") == SUCCESS)
		{
			strcpy(pstRCHIDetails[RC_CARD_TYPE].elementValue, "");
		}

		/* Getting DevTypeIndicator incase of a RFID Mastercard */
		/* Daivik:11/3/2016 - Now we are going to save the dev type indicator during the original transaction.
		 * So we need to use that value if it is present and query again
		 */
		//getDevTypeInd(pstRCHIDetails);

		if(strlen(pstRCHIDetails[RC_ADDTL_AMT_HLTCARE].elementValue) > 0)
		{
			getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_AMT_CRNCY_HLTCARE].elementValue);
			/* Hard coding the values for HealthCare Amount Type */
			strcpy(pstRCHIDetails[RC_ADDTL_AMT_TYPE_HLTCARE].elementValue, "Hltcare");
			/* For refund transactions we are not supposed to add the Market Specific Indicator */
			if(strcmp(pstRCHIDetails[RC_TRAN_TYPE].elementValue,"Refund"))
			{
				if(strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "MasterCard") == SUCCESS)
				{
					strcpy(pstRCHIDetails[RC_MC_MRKT_SPEC_IND].elementValue,"Healthcare");
				}
				else if(strcmp(pstRCHIDetails[RC_CARD_TYPE].elementValue, "Visa") == SUCCESS)
				{
					strcpy(pstRCHIDetails[RC_VISA_MRKT_SPEC_IND].elementValue,"Healthcare");
				}
			}
		}
		if(strlen(pstRCHIDetails[RC_ADDTL_AMT_DENTAL].elementValue) > 0)
		{

			getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_AMT_CRNCY_DENTAL].elementValue);
			/* Hard coding the values for Dental Amount Type */
			strcpy(pstRCHIDetails[RC_ADDTL_AMT_TYPE_DENTAL].elementValue, "Dental");
		}
		if(strlen(pstRCHIDetails[RC_ADDTL_AMT_CLINIC].elementValue) > 0)
		{

			getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_AMT_CRNCY_CLINIC].elementValue);
			/* Hard coding the values for Clinical Amount Type */
			strcpy(pstRCHIDetails[RC_ADDTL_AMT_TYPE_CLINIC].elementValue, "Clinical");
		}
		if(strlen(pstRCHIDetails[RC_ADDTL_AMT_VISION].elementValue) > 0)
		{

			getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_AMT_CRNCY_VISION].elementValue);
			/* Hard coding the values for Vision Amount Type */
			strcpy(pstRCHIDetails[RC_ADDTL_AMT_TYPE_VISION].elementValue, "Vision");
		}
		if(strlen(pstRCHIDetails[RC_ADDTL_AMT_PRESCRIPION].elementValue) > 0)
		{

			getAddtlAmtCurrency(pstRCHIDetails[RC_ADDTL_AMT_CRNCY_PRESCRIPION].elementValue);
			/* Hard coding the values for Prescription Amount Type */
			strcpy(pstRCHIDetails[RC_ADDTL_AMT_TYPE_PRESCRIPION].elementValue, "RX");

		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning rv = [%d] ---", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveTranDetailsForFollowOnTran
 *
 * Description	:
 *
 * Input Params	: Structure
 *
 * Output Params:
 * ============================================================================
 */
void saveTranDetailsForFollowOnTran(TRANDTLS_PTYPE pstDHIDetails, RCTRANDTLS_PTYPE pstRCDetails)
{
	char	szTranRecord[2048] 	= "";
	char	szBuffer[1024]		= "";
	FILE* 	fptr 				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	fptr = fopen(TRAN_RECORDS, "a+");
	if(fptr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Could not open the file could not save the record", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}

	/*
	 * Order in which the data is going to get stored in the file:
	 * RefNum|TxnType|OrderNum|LocalTime|GMTTime|STAN||PosEntryMode|PosCondCode|TermEntryCapability|Card Type|ApprovedAmt|Cashback|TaxAmt|RCToken|
	 * AuthID|RespCode
	 */

	/*Storing all the DHI fields which are required for the follow on transactions*/
	if(strlen(pstRCDetails[RC_REFERENCE_NUMBER].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_REFERENCE_NUMBER].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/* Storing the Transaction type */
	if(strlen(pstRCDetails[RC_TRAN_TYPE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_TRAN_TYPE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_ORDER_NUMBER].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstDHIDetails[eINVOICE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/* Should not store local time in case of balance lock*/
	if(strlen(pstRCDetails[RC_LOCAL_DATE_TIME].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_LOCAL_DATE_TIME].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/* Storing the transmission date and time*/
	/* Should not store transmission time in case of balance lock*/
	if(strlen(pstRCDetails[RC_TRANSM_DATE_TIME].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_TRANSM_DATE_TIME].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/* Should not store stan in case of balance lock*/
	if(strlen(pstRCDetails[RC_STAN].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_STAN].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_POS_ENTRY_MODE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_POS_ENTRY_MODE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_POS_COND_CODE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_POS_COND_CODE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_TERM_ENTRY_CAPABLT].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_TERM_ENTRY_CAPABLT].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strcmp(pstRCDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid") &&
		pstDHIDetails[eEXP_MONTH].elementValue 	!= NULL &&
		pstDHIDetails[eEXP_YEAR].elementValue	!= NULL)
	{
		getCardExpirationData(szBuffer, pstDHIDetails[eEXP_MONTH].elementValue, pstDHIDetails[eEXP_YEAR].elementValue);
		strcat(szBuffer, "|");
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_CARD_TYPE].elementValue) >= 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_CARD_TYPE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_CARD_TRANS_AMOUNT].elementValue) > 0)
	{
		switch(strlen(pstRCDetails[RC_CARD_TRANS_AMOUNT].elementValue))
		{
			case 1:sprintf(szBuffer, "00%s|", pstRCDetails[RC_CARD_TRANS_AMOUNT].elementValue);
				   break;

			case 2:sprintf(szBuffer, "0%s|", pstRCDetails[RC_CARD_TRANS_AMOUNT].elementValue);
			       break;

			default:sprintf(szBuffer, "%s|", pstRCDetails[RC_CARD_TRANS_AMOUNT].elementValue);

		}
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_ADDTL_AMT_CASHBK].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_ADDTL_AMT_CASHBK].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_ADDTL_AMT_TAXAMT].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_ADDTL_AMT_TAXAMT].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_CARD_TOKEN].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_CARD_TOKEN].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strcmp(pstRCDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid") == SUCCESS && strlen(pstRCDetails[RC_ACCT_NUM].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_ACCT_NUM].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/* Storing the response details */
	/* Need to store auth id only for balance lock in case of prepaid transactions*/
	if(strlen(pstRCDetails[RC_AUTH_ID].elementValue) > 0)
	{
		if(strcmp(pstRCDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid"))
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_AUTH_ID].elementValue);
		}
		else if(strcmp(pstRCDetails[RC_TRAN_TYPE].elementValue, "BalanceLock") == SUCCESS)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_AUTH_ID].elementValue);
		}
		else
		{
			sprintf(szBuffer, "|");
		}
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/* Should not store resp code in case of balance lock*/
	if(strlen(pstRCDetails[RC_RESP_CODE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_RESP_CODE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_HOLD_INFO].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_HOLD_INFO].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(!strcmp(pstRCDetails[RC_TRAN_TYPE].elementValue, "BalanceLock") &&
			pstDHIDetails[ePARTIAL_AUTH].elementValue != NULL)
	{
		sprintf(szBuffer, "%s|", pstDHIDetails[ePARTIAL_AUTH].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(!strcmp(pstRCDetails[RC_PAYMENT_TYPE].elementValue, "Prepaid") &&
			strlen(pstRCDetails[RC_TRACK1_DATA].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_TRACK1_DATA].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/* Storing TRACK2DATA only in case of BalanceLock */
	if(!strcmp(pstRCDetails[RC_TRAN_TYPE].elementValue, "BalanceLock") &&
			strlen(pstRCDetails[RC_TRACK2_DATA].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_TRACK2_DATA].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_AUTH_CHARACTERISTICS_IND].elementValue) > 0 &&
			pstRCDetails[RC_AUTH_CHARACTERISTICS_IND].elementValue[0] != 'Y')
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_AUTH_CHARACTERISTICS_IND].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_CARD_LEVEL_RESULT].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_CARD_LEVEL_RESULT].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_TRAN_ID].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_TRAN_ID].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_BANK_NET_DATA].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_BANK_NET_DATA].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_CCV_ERROR_CODE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_CCV_ERROR_CODE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_POSENTRYMODE_CHG].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_POSENTRYMODE_CHG].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_DISC_PROC_CODE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DISC_PROC_CODE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_DISC_POS_ENTRY].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DISC_POS_ENTRY].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_DISC_RESP_CODE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DISC_RESP_CODE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_DISC_POS_DATA].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DISC_POS_DATA].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_DISC_TRAN_QUALIF].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DISC_TRAN_QUALIF].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_DISC_NRID].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DISC_NRID].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_AMEX_POS_DATA].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_AMEX_POS_DATA].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_AMEX_TRAN_ID].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_AMEX_TRAN_ID].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/* Storing Purchase Card Lvl2 Group details for Completion and for Credit Payment Only
	 * Storing all these values only in case of Completion since for void these fields are not needed
	*/
	if(!strcmp(pstRCDetails[RC_TRAN_TYPE].elementValue, "Authorization") && !strcmp(pstRCDetails[RC_PAYMENT_TYPE].elementValue, "Credit"))
	{
		/* Storing Tax Amount only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_TAX_AMOUNT_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_TAX_AMOUNT_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}
		/* Storing Tax Indicator only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_TAX_IND_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_TAX_IND_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}

		/* Storing Purchase Identifier only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_PURCHASE_IDFR_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_PURCHASE_IDFR_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}

		/* Storing PC Order Number only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_PC_ORDER_NUM_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_PC_ORDER_NUM_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}

		/* Storing Discount Amount only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_DISC_AMOUNT_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_DISC_AMOUNT_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}
		/* Storing Freight Amount only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_FREIGHT_AMOUNT_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_FREIGHT_AMOUNT_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}
		/* Storing Duty Amount only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_DUTY_AMOUNT_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_DUTY_AMOUNT_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}
		/* Storing Destination Postal code only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_DEST_POSTAL_CODE_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_DEST_POSTAL_CODE_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}
		/* Storing Ship postal code only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_SHIP_POSTAL_CODE_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_SHIP_POSTAL_CODE_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}
		/* Storing Destination country code only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_DEST_COUNTRY_CODE_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_DEST_COUNTRY_CODE_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}
		/* Storing Alternate Tax ID in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_MERCH_TAX_ID_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_MERCH_TAX_ID_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}
		/* Storing Product Description only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_PROD_DESC_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_PROD_DESC_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}

		/* Storing PC3 Add only in case of Authorization for PurchaseCardlvl2 group */
		if(strlen(pstRCDetails[RC_PC3_ADD_AUTH].elementValue) > 0)
		{
			sprintf(szBuffer, "%s|", pstRCDetails[RC_PC3_ADD_AUTH].elementValue);
			strcat(szTranRecord, szBuffer);
		}
		else
		{
			strcat(szTranRecord, "|");
		}
	}
	/* Fill empty Purchase Card Lvl2 fields with | */
	else
	{
		strcat(szTranRecord, "|||||||||||||");
	}
	if(strlen(pstRCDetails[RC_ADDTL_AMT_HLTCARE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_ADDTL_AMT_HLTCARE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_ADDTL_AMT_DENTAL].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_ADDTL_AMT_DENTAL].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_ADDTL_AMT_VISION].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_ADDTL_AMT_VISION].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_ADDTL_AMT_CLINIC].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_ADDTL_AMT_CLINIC].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_ADDTL_AMT_PRESCRIPION].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_ADDTL_AMT_PRESCRIPION].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_EBT_TYPE].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_EBT_TYPE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_X_CODE_RESP].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_X_CODE_RESP].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_ATH_NTWK_ID].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_ATH_NTWK_ID].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_CARD_SEQ_NUM].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_CARD_SEQ_NUM].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_DEV_TYPE_IND].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DEV_TYPE_IND].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	if(strlen(pstRCDetails[RC_DCC_IND].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DCC_IND].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(pstRCDetails[RC_DCC_TRAN_TIME_ZONE].elementValue)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DCC_TRAN_TIME_ZONE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(pstRCDetails[RC_DCC_CARDHOLDER_CURR_AMT].elementValue)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DCC_CARDHOLDER_CURR_AMT].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(pstRCDetails[RC_DCC_CARDHOLDER_CONV_RATE].elementValue)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DCC_CARDHOLDER_CONV_RATE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(pstRCDetails[RC_DCC_CARDHOLDER_CURR_CODE].elementValue)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_DCC_CARDHOLDER_CURR_CODE].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	if(strlen(pstRCDetails[RC_PLESS_POS_DEB_FLG].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstRCDetails[RC_PLESS_POS_DEB_FLG].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	/* NOTE: ADD ALL RCHI FIELDS BEFORE THIS POINT, ONLY DHI FIELDS MUST BE ADDED BELOW.
	 * ORDER MUST BE MAINTAINED WHILE SAVING FOLLOW ON DETAILS AND WHILE RETRIEVING THE SAME.
	 */
	if(pstDHIDetails[eEMV_TAGS].elementValue)
	{
		sprintf(szBuffer, "%s|", pstDHIDetails[eEMV_TAGS].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}
	/* Only if this is Authorization response we save the EMV Response Tags to send during completion.
	 *
	 */
	if(!strcmp(pstRCDetails[RC_TRAN_TYPE].elementValue, "Authorization") && pstDHIDetails[eEMV_TAGS_RESP].elementValue)
	{
		sprintf(szBuffer, "%s|", pstDHIDetails[eEMV_TAGS_RESP].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	strcat(szTranRecord, "\n");
	
	fprintf(fptr, "%s", szTranRecord);

	fclose(fptr);
	

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: getAddtlAmountGrp
 *
 *
 * Description	: This function will fill the Additional Amount Group buffer
 *
 *
 * Input Params	: Additional Amount Group Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getAddtlAmountGrp(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDtls)
{
	int 	iTaxFlag		= RCHI_FALSE;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: -----enter------ ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	if((strlen(pstRCHIDtls[RC_PAYMENT_TYPE].elementValue) > 0) && strcmp(pstRCHIDtls[RC_PAYMENT_TYPE].elementValue,"Prepaid"))
	{
		if(pstDHIDtls[eCASHBACK_AMNT].elementValue != NULL)

		{
			getCorrectAmount(pstDHIDtls[eCASHBACK_AMNT].elementValue, pstRCHIDtls[RC_ADDTL_AMT_CASHBK].elementValue);

			/* Getting Cashback Addtional amount Currency */
			getAddtlAmtCurrency(pstRCHIDtls[RC_ADDTL_AMT_CRNCY_CASHBK].elementValue);
			/* Filling Additonal Amount Type */
			strcpy(pstRCHIDtls[RC_ADDTL_AMT_TYPE_CASHBK].elementValue, "Cashback");
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No cashback amount found ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		/* Check if Additional tax amount field is required. Since VISA, MC and Amex cards use
		 * Purchase Card L2 group, consider only Debit Cards and in Credit only Discover,
		 * JCB and Diners card. */
		if(!strcmp(pstRCHIDtls[RC_PAYMENT_TYPE].elementValue, "Credit"))
		{
			if(!strcmp(pstRCHIDtls[RC_CARD_TYPE].elementValue, "Discover") || !strcmp(pstRCHIDtls[RC_CARD_TYPE].elementValue, "Diners")
					|| !strcmp(pstRCHIDtls[RC_CARD_TYPE].elementValue, "JCB") )
			iTaxFlag = RCHI_TRUE;

			/* According to the UMF Document, the FSA fields are valid only for MC and VISA.
			 *  Prescription/HealthCare are the only valid fields for MC , whereas all FSA
			 *  fields i.e Healthcare,Clinical,Prescription,Vision and Dental are allowed for Visa.
			 */
			if(!strcmp(pstRCHIDtls[RC_CARD_TYPE].elementValue, "Visa") || !strcmp(pstRCHIDtls[RC_CARD_TYPE].elementValue, "MasterCard"))
			{
				if(pstDHIDtls[eAMT_HLTCARE].elementValue != NULL)
				{
					getFSAGroupValues(pstDHIDtls, pstRCHIDtls);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Healthcare amount is not found  ", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
			}
		}
		else if(!strcmp(pstRCHIDtls[RC_PAYMENT_TYPE].elementValue, "Debit"))
		{
			iTaxFlag = RCHI_TRUE;
		}
		if(pstDHIDtls[eTAX_AMOUNT].elementValue != NULL && iTaxFlag == RCHI_TRUE )
		{
			getCorrectAmount(pstDHIDtls[eTAX_AMOUNT].elementValue, pstRCHIDtls[RC_ADDTL_AMT_TAXAMT].elementValue);

			//Getting Tax Addtional amount Currency
			getAddtlAmtCurrency(pstRCHIDtls[RC_ADDTL_AMT_CRNCY_TAXAMT].elementValue);
			//Filling Additonal Amount Type
			strcpy(pstRCHIDtls[RC_ADDTL_AMT_TYPE_TAXAMT].elementValue, "Tax");
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No tax amount found ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}
	/* Getting Partial Amount Authorization Approval Capability */
	/* Ignoring Partial Amount Authorization Approval Capability field for BalanceLock of Prepaid Transactions */
	if(pstDHIDtls[ePARTIAL_AUTH].elementValue != NULL)
	{
		if(strcmp(pstRCHIDtls[RC_TRAN_TYPE].elementValue, "BalanceLock") && strcmp(pstRCHIDtls[RC_PAYMENT_TYPE].elementValue, "Check"))
		{
			strcpy(pstRCHIDtls[RC_PARTIAL_AUTH].elementValue, pstDHIDtls[ePARTIAL_AUTH].elementValue);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: getNetSettlementAmout
 *
 *
 * Description	: This function will fill the Additional Amount Group buffer
 *
 *
 * Input Params	: Additional Amount Group Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getNetSettlementAmout(char *szNetSettlementAmt)
{
	strcpy(szNetSettlementAmt, "00");
}

/*
 * ============================================================================
 * Function Name: getTotalsRequestDate
 *
 *
 * Description	: This function will fill the Additional Amount Group buffer
 *
 *
 * Input Params	: Additional Amount Group Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
int getTotalsRequestDate(char *pszTotalReqDate, char *pszCommand)
{
	int rv = SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: -----enter------ ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pszCommand != NULL)
	{
		if(strcmp(pszCommand, "CUTOVER") == SUCCESS)
		{
			strcpy(pszTotalReqDate, "CloseBatch");
			rv = RC_CLOSE_BATCH;
		}
		else if(strcmp(pszCommand, "SITETOTALS") == SUCCESS)
		{
			strcpy(pszTotalReqDate, "SiteCurDay");
			rv = RC_SITE_CUR_DAY;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Command ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Command not found ", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}
static int getRCPinGrpDtls(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 			rv						= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: -----enter------ ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue)
	{
		strcat(pstRCHIDetails[RC_KSN].elementValue, pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue);
	}
	if(pstDHIDtls[ePIN_BLOCK].elementValue)
	{
		strcpy(pstRCHIDetails[RC_PIN_BLOCK].elementValue,pstDHIDtls[ePIN_BLOCK].elementValue);
	}
	if(!strcmp(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue, "Debit") && pstDHIDtls[eEMV_TAGS].elementValue)
	{
		/* We support Debit Offline/No Pin Verification Transactions in which the Pin has been verified by the EMV Chip itself.
		 * So we will not recieve the PIN Block/KSN from XPI in such cases, so filling F's.
		 */
		if(!strlen(pstRCHIDetails[RC_PIN_BLOCK].elementValue))
		{
			strcpy(pstRCHIDetails[RC_PIN_BLOCK].elementValue,"FFFFFFFFFFFFFFFF");
		}
		if(!strlen(pstRCHIDetails[RC_KSN].elementValue))
		{
			strcpy(pstRCHIDetails[RC_KSN].elementValue,"FFFFFFFFFFFFFFFFFFFF");
		}

	}
	debug_sprintf(szDbgMsg, "%s: KSN : %s", __FUNCTION__,pstRCHIDetails[RC_KSN].elementValue);
	RCHI_LIB_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

void getEMVFileDownloadGrp(RCTRANDTLS_PTYPE pstRCHIDetails)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	getRCGenReqDtls(pstRCHIDetails);
	memset(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue,0x00, sizeof(pstRCHIDetails[RC_PAYMENT_TYPE].elementValue));
	strcpy(pstRCHIDetails[RC_FL_TYPE].elementValue,"EMV2KEY");
	strcpy(pstRCHIDetails[RC_FUNC_CODE].elementValue,"D");
	strcpy(pstRCHIDetails[RC_FL_BLK_SEQ].elementValue,gstEMVAdmin.szFileBlk);
	strcpy(pstRCHIDetails[RC_FL_BLK_MAX].elementValue,"999");
	strcpy(pstRCHIDetails[RC_REQ_FL_OFF].elementValue,gstEMVAdmin.szDownOffset);

	debug_sprintf(szDbgMsg, "%s: Returning Download offset:%s", __FUNCTION__,gstEMVAdmin.szDownOffset);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: fillRCDtlsforEchoTest
 *
 *
 * Description	: This function prepares the RC Structure for EchoTest Request
 *
 * Input Params	:
 *
 *
 * Output Params: None
 * ============================================================================
 */
void fillRCDtlsforEchoTest(RCTRANDTLS_PTYPE pstRCHIDetails)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	getRCGenReqDtls(pstRCHIDetails);
	strcpy(pstRCHIDetails[RC_TRAN_TYPE].elementValue,"EchoTest");

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: getCorrectPurchaseCardLvl2GroupValues
 *
 *
 * Description	: This function will correct Purchase Card Lvl2 Group details Buffer
 * 				  and also checks if this group is required or not
 *
 * Input Params	: Purchase Card Lvl2 Group details Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
static int getCorrectPurchaseCardLvl2GroupValues(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDtls)
{

	int			iPL2Reqd				= RCHI_FALSE;
	int 		rv						= SUCCESS;
	int 		iIndex					= 0;
	int			iTotal					= 0;
	int			iLvl2Indices[]			= {
											RC_TAX_AMOUNT, RC_TAX_IND, RC_PURCHASE_IDFR, RC_PC_ORDER_NUM,
											RC_DISC_AMOUNT, RC_FREIGHT_AMOUNT, RC_DUTY_AMOUNT, RC_DEST_POSTAL_CODE,
											RC_SHIP_POSTAL_CODE, RC_DEST_COUNTRY_CODE, RC_MERCH_TAX_ID, RC_PROD_DESC,
											RC_PC3_ADD
									  	  };
	int			iLvl2IndicesForAuth[]	= {
											RC_TAX_AMOUNT_AUTH, RC_TAX_IND_AUTH, RC_PURCHASE_IDFR_AUTH, RC_PC_ORDER_NUM_AUTH,
											RC_DISC_AMOUNT_AUTH, RC_FREIGHT_AMOUNT_AUTH, RC_DUTY_AMOUNT_AUTH,
											RC_DEST_POSTAL_CODE_AUTH, RC_SHIP_POSTAL_CODE_AUTH, RC_DEST_COUNTRY_CODE_AUTH,
											RC_MERCH_TAX_ID_AUTH, RC_PROD_DESC_AUTH, RC_PC3_ADD_AUTH,
											};

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: -----enter------ ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Check if Purchase Card Lvl2 Group is needed and also hardcoding PC3 Add value to 0 since we are not using Level3 */
	if(pstDHIDtls[eTAX_AMOUNT].elementValue != NULL || pstDHIDtls[eTAX_IND].elementValue != NULL ||
	   pstDHIDtls[eDISC_AMOUNT].elementValue != NULL || pstDHIDtls[eFREIGHT_AMOUNT].elementValue != NULL ||
	   pstDHIDtls[eDUTY_AMOUNT].elementValue != NULL || pstDHIDtls[eDEST_POSTAL_CODE].elementValue != NULL ||
	   pstDHIDtls[eSHIP_POSTAL_CODE].elementValue != NULL || pstDHIDtls[eDEST_COUNTRY_CODE].elementValue != NULL ||
	   pstDHIDtls[eCUSTOMER_CODE].elementValue != NULL || pstDHIDtls[ePROD_DESC].elementValue != NULL ||
	   pstDHIDtls[eALT_TAX_ID].elementValue != NULL
	  )
	{
		iPL2Reqd = RCHI_TRUE;

	}

	if(iPL2Reqd == RCHI_TRUE)
	{
		if(strlen(pstRCHIDtls[RC_CARD_TYPE].elementValue) > 0)
		{
			/* Checking mandatory field for MasterCard */
			if(strcmp(pstRCHIDtls[RC_CARD_TYPE].elementValue, "MasterCard") == SUCCESS)
			{
				if(pstDHIDtls[eALT_TAX_ID].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Missing mandatory field for L2 group: Alt Tax Ind ", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = ERR_FLD_REQD;
					return rv;
				}
				strcpy(pstRCHIDtls[RC_MERCH_TAX_ID].elementValue, pstDHIDtls[eALT_TAX_ID].elementValue);
			}

			/* Checking mandatory field for Amex */
			else if(strcmp(pstRCHIDtls[RC_CARD_TYPE].elementValue, "Amex") == SUCCESS)
			{
				if(pstDHIDtls[ePROD_DESC].elementValue == NULL || pstDHIDtls[eDEST_POSTAL_CODE].elementValue == NULL)
				{
					rv = ERR_FLD_REQD;
					return rv;
				}
				strcpy(pstRCHIDtls[RC_PROD_DESC].elementValue, pstDHIDtls[ePROD_DESC].elementValue);
			}
			/* As per UMF, if card is not of Visa, MasterCard and Amex type, then L2 group is not needed */
			else if(strcmp(pstRCHIDtls[RC_CARD_TYPE].elementValue, "Visa") != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Purchase Card L2 Group not needed ", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = SUCCESS;
				return rv;
			}

			/* Copying Purchase Identifier */
			if(pstDHIDtls[ePURCHASE_ID].elementValue != NULL)
			{
				strcpy(pstRCHIDtls[RC_PURCHASE_IDFR].elementValue, pstDHIDtls[ePURCHASE_ID].elementValue);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Missing mandatory field for L2 group: Purchase Identifier ", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_FLD_REQD;
				return rv;
			}

			/* Checking for tax amount and setting the tax indicator appropriately */
			if(pstDHIDtls[eTAX_AMOUNT].elementValue != NULL)
			{
				getCorrectAmount(pstDHIDtls[eTAX_AMOUNT].elementValue, pstRCHIDtls[RC_TAX_AMOUNT].elementValue);
			}
			/* Copying PC Order Number */
			if(pstDHIDtls[eCUSTOMER_CODE].elementValue != NULL)
			{
				strcpy(pstRCHIDtls[RC_PC_ORDER_NUM].elementValue, pstDHIDtls[eCUSTOMER_CODE].elementValue);
			}
			/*else
				{
					debug_sprintf(szDbgMsg, "%s: Missing mandatory field for PC Order number when Tax amnt is present ", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = ERR_FLD_REQD;
					return rv;
				}*/


			/* Checking for tax indicator */
			if(pstDHIDtls[eTAX_IND].elementValue != NULL)
			{
				strcpy(pstRCHIDtls[RC_TAX_IND].elementValue, pstDHIDtls[eTAX_IND].elementValue);
			}
			/* Correcting Discount Amount */
			if(pstDHIDtls[eDISC_AMOUNT].elementValue != NULL)
			{
				getCorrectAmount(pstDHIDtls[eDISC_AMOUNT].elementValue, pstRCHIDtls[RC_DISC_AMOUNT].elementValue);
			}
			/* Correcting Freight Amount */
			if(pstDHIDtls[eFREIGHT_AMOUNT].elementValue != NULL)
			{
				getCorrectAmount(pstDHIDtls[eFREIGHT_AMOUNT].elementValue, pstRCHIDtls[RC_FREIGHT_AMOUNT].elementValue);
			}
			/* Correcting Duty Amount */
			if(pstDHIDtls[eDUTY_AMOUNT].elementValue != NULL)
			{
				getCorrectAmount(pstDHIDtls[eDUTY_AMOUNT].elementValue, pstRCHIDtls[RC_DUTY_AMOUNT].elementValue);
			}
			/* Copying Non-Empty Level2 fields: Destination Postal Code, Ship postal code and Destination Country code */
			if(pstDHIDtls[eDEST_POSTAL_CODE].elementValue != NULL)
			{
				strcpy(pstRCHIDtls[RC_DEST_POSTAL_CODE].elementValue, pstDHIDtls[eDEST_POSTAL_CODE].elementValue);
			}
			if(pstDHIDtls[eSHIP_POSTAL_CODE].elementValue != NULL)
			{
				strcpy(pstRCHIDtls[RC_SHIP_POSTAL_CODE].elementValue, pstDHIDtls[eSHIP_POSTAL_CODE].elementValue);
			}
			if(pstDHIDtls[eDEST_COUNTRY_CODE].elementValue != NULL)
			{
				strcpy(pstRCHIDtls[RC_DEST_COUNTRY_CODE].elementValue, pstDHIDtls[eDEST_COUNTRY_CODE].elementValue);
			}

			/* Harcoding PC3 Add value to 0 */
			strcpy(pstRCHIDtls[RC_PC3_ADD].elementValue, "0");

			/* If an authorization is being performed, then this group need not be sent in the request during
			 *auth but instead in completion request these fields are required. Hence storing these fields in
			 *different buffers and clearing the fields for auth request.
			 */
			if(!strcmp(pstRCHIDtls[RC_TRAN_TYPE].elementValue, "Authorization" ))
			{
				iTotal = sizeof(iLvl2Indices)/sizeof(int);
				for(iIndex = 0; iIndex < iTotal; iIndex++)
				{
					if(strlen(pstRCHIDtls[iLvl2Indices[iIndex]].elementValue) > 0)
					{
						strcpy(pstRCHIDtls[iLvl2IndicesForAuth[iIndex]].elementValue, pstRCHIDtls[iLvl2Indices[iIndex]].elementValue);
						memset(pstRCHIDtls[iLvl2Indices[iIndex]].elementValue, 0x00, sizeof(pstRCHIDtls[iLvl2Indices[iIndex]].elementValue));
					}
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card Type not found ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv= ERR_FLD_REQD;

		}
	}
	else
	{

		debug_sprintf(szDbgMsg, "%s: Purchase Card L2 Group not needed ", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getFSAGroupValues
 *
 *
 * Description	: This function will get correct Additional Amount Groups related to
 *  			  FSA Details and checks which group is really required. Finally
 *  			  each of the FSA fields will go as Additional Amount groups.
 *
 * Input Params	: FSA Group details to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
static void getFSAGroupValues(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDtls)
{

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: -----enter------ ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	getCorrectAmount(pstDHIDtls[eAMT_HLTCARE].elementValue, pstRCHIDtls[RC_ADDTL_AMT_HLTCARE].elementValue);
	//Getting Healthcare Additional amount Currency
	getAddtlAmtCurrency(pstRCHIDtls[RC_ADDTL_AMT_CRNCY_HLTCARE].elementValue);
	//Filling Additional Amount Type
	strcpy(pstRCHIDtls[RC_ADDTL_AMT_TYPE_HLTCARE].elementValue, "Hltcare");
	/* Setting the Market Specific data indicator for Both MC and Visa since anyway just one group is going to be sent.
	 * Just Avoiding multiple checks for VISA and MC.
	 */
	if(strcmp(pstRCHIDtls[RC_TRAN_TYPE].elementValue,"Refund"))
	{
		strcpy(pstRCHIDtls[RC_VISA_MRKT_SPEC_IND].elementValue, "Healthcare");
		strcpy(pstRCHIDtls[RC_MC_MRKT_SPEC_IND].elementValue, "Healthcare");
	}

	if(pstDHIDtls[eAMT_PRESCRIPION].elementValue != NULL)
	{
		getCorrectAmount(pstDHIDtls[eAMT_PRESCRIPION].elementValue, pstRCHIDtls[RC_ADDTL_AMT_PRESCRIPION].elementValue);
		//Getting Prescription Additional amount Currency
		getAddtlAmtCurrency(pstRCHIDtls[RC_ADDTL_AMT_CRNCY_PRESCRIPION].elementValue);
		//Filling Additional Amount Type
		strcpy(pstRCHIDtls[RC_ADDTL_AMT_TYPE_PRESCRIPION].elementValue, "RX");
	}
	if(!strcmp(pstRCHIDtls[RC_CARD_TYPE].elementValue, "Visa"))
	{
		if(pstDHIDtls[eAMT_DENTAL].elementValue != NULL)
		{
			getCorrectAmount(pstDHIDtls[eAMT_DENTAL].elementValue, pstRCHIDtls[RC_ADDTL_AMT_DENTAL].elementValue);
			//Getting Dental Additional amount Currency
			getAddtlAmtCurrency(pstRCHIDtls[RC_ADDTL_AMT_CRNCY_DENTAL].elementValue);
			//Filling Additional Amount Type
			strcpy(pstRCHIDtls[RC_ADDTL_AMT_TYPE_DENTAL].elementValue, "Dental");
		}
		if(pstDHIDtls[eAMT_CLINIC].elementValue != NULL)
		{
			getCorrectAmount(pstDHIDtls[eAMT_CLINIC].elementValue, pstRCHIDtls[RC_ADDTL_AMT_CLINIC].elementValue);
			//Getting Clinic Additional amount Currency
			getAddtlAmtCurrency(pstRCHIDtls[RC_ADDTL_AMT_CRNCY_CLINIC].elementValue);
			//Filling Additional Amount Type
			strcpy(pstRCHIDtls[RC_ADDTL_AMT_TYPE_CLINIC].elementValue, "Clinical");
		}
		if(pstDHIDtls[eAMT_VISION].elementValue != NULL)
		{

			getCorrectAmount(pstDHIDtls[eAMT_VISION].elementValue, pstRCHIDtls[RC_ADDTL_AMT_VISION].elementValue);
			//Getting Vision Additional amount Currency
			getAddtlAmtCurrency(pstRCHIDtls[RC_ADDTL_AMT_CRNCY_VISION].elementValue);
			//Filling Additional Amount Type
			strcpy(pstRCHIDtls[RC_ADDTL_AMT_TYPE_VISION].elementValue, "Vision");
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getEBTType
 *
 *
 * Description	: This function will correct EBT Type details Buffer
 *
 *
 * Input Params	: RC EBT Type Buffer to be filled
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getEBTType(char *pszDHIEBTType, char *pszRCEBTType)
{
	int rv = SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: -----enter------ ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pszDHIEBTType == NULL)
	{
		rv = ERR_FLD_REQD;

		debug_sprintf(szDbgMsg, "%s: EBT Type missing", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

	}
	else
	{
		if(!strcmp(pszDHIEBTType, "FOOD_STAMP"))

		{
			strcpy(pszRCEBTType, "SNAP");
		}
		else if(!strcmp(pszDHIEBTType, "CASH_BENEFITS"))
		{
			strcpy(pszRCEBTType, "EBTCash");
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid EBT Type", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}
	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getCheckTransactionDtls
 *
 *
 * Description	: This function will fill check transaction related fields
 *
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getCheckTransactionDtls(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDtls)
{
	int rv = SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif


	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Filling check transaction related fields */

	/* Hard-Coding Check Service Provider */
	strcpy(pstRCHIDtls[RC_CHK_SERVCE_PROVIDER].elementValue, "TeleCheck");

	if(pstDHIDtls[eMICR].elementValue != NULL)
	{
		strcpy(pstRCHIDtls[RC_MICR].elementValue, pstDHIDtls[eMICR].elementValue);
		strcpy(pstRCHIDtls[RC_CHK_ENTRY_METHD].elementValue, "2");
	}
	else if(pstDHIDtls[eABA_NUM].elementValue != NULL && pstDHIDtls[eACCT_NUM].elementValue != NULL && pstDHIDtls[eCHECK_NUM].elementValue != NULL)
	{
		strcpy(pstRCHIDtls[RC_MICR].elementValue, pstDHIDtls[eABA_NUM].elementValue);
		strcat(pstRCHIDtls[RC_MICR].elementValue, "T");
		strcat(pstRCHIDtls[RC_MICR].elementValue, pstDHIDtls[eACCT_NUM].elementValue);
		strcat(pstRCHIDtls[RC_MICR].elementValue,"A");
		strcat(pstRCHIDtls[RC_MICR].elementValue, pstDHIDtls[eCHECK_NUM].elementValue);

		/* Hard coding check entry method because manual details of MICR are provided */
		strcpy(pstRCHIDtls[RC_CHK_ENTRY_METHD].elementValue, "3");
	}
	else if(pstDHIDtls[eDL_NUMBER].elementValue != NULL)
	{

		pstDHIDtls[eDL_DOB].elementValue = "19900101"; //Remove this later
		if(pstDHIDtls[eDL_STATE].elementValue!= NULL && pstDHIDtls[eDL_DOB].elementValue != NULL)
		{
			/* Hard coding check entry method because driving license number is provided */
			strcpy(pstRCHIDtls[RC_CHK_ENTRY_METHD].elementValue, "1");

			pstDHIDtls[eDL_DOB].elementValue = NULL; //Remove this later
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: State Code or Driver License's Date of Birth is Missing!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			return rv;
		}
	}
	else
	{
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 *
 * ============================================================================
 * Function Name: getCardId
 *
 *
 * Description	: This function will fill getCardId Request related fields
 *
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCardId(RCTRANDTLS_PTYPE pstRCHIDetails,TRANDTLS_PTYPE pstDHIDtls)
{
	int rv 			= SUCCESS;
#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eCARDID].elementValue != NULL)
	{
		rv = isFGNCard(pstDHIDtls[eCARDID].elementValue);
		if(rv == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Card is Eligible for DCC. Matches the Bin", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strcpy(pstRCHIDetails[RC_CARDID].elementValue, pstDHIDtls[eCARDID].elementValue);

			pstDHIDtls[eDCC_OFFD].elementValue= strdup("TRUE");

			rv = SUCCESS;
		}
		else if(rv == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Card is not Eligible for DCC. Does not Match the Bin", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			pstDHIDtls[eDCC_OFFD].elementValue= strdup("FALSE");

			rv = SUCCESS;
		}
		else if(rv != SUCCESS)
		{
			//what should we do here ,error can be invalid PAN .
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Card Id Field is Missing", __FUNCTION__);			// What can be filled if the CARDID from the FEXCO is NULL
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_FLD_REQD;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 *
 * ============================================================================
 * Function Name: getAmtDtlsForFEXCO
 *
 *
 * Description	: This function will fill getCardId Request related fields
 *
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getAmtDtlsForFEXCO(RCTRANDTLS_PTYPE pstRCHIDetails,TRANDTLS_PTYPE pstDHIDtls)
{
	int 		rv 		= SUCCESS;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eTRANS_AMOUNT].elementValue != NULL)
	{
		rv = getCorrectAmount(pstDHIDtls[eTRANS_AMOUNT].elementValue, pstRCHIDetails[RC_BASE_AMOUNT].elementValue);
		//strcpy(pstRCHIDetails[RC_BASE_AMOUNT].elementValue, pstDHIDtls[eTRANS_AMOUNT].elementValue);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Amount Field is Missing", __FUNCTION__);	// if the Amount field is Null , Then Throughing back Error as field Required
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_FLD_REQD;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 *
 * ============================================================================
 * Function Name: getDCCFields
 *
 *
 * Description	: This function will fill getCardId Request related fields
 *
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void getDCCFields(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int 	iExchangeRate				= 0;
	char	szExchangeRate[10 + 1]		= "";
	char	szBeforeDecimal[4 + 1]		= "";
	char	szAfterDecimal[4 + 1]		= "";
	char* 	pszDecimalPoint				= NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/*step 1: Add DCC Ind to the Request*/
	strcpy(pstRCHIDetails[RC_DCC_IND].elementValue, pstDHIDtls[eDCCInd].elementValue);

	/*Add Transaction Time zone*/
	strcpy(pstRCHIDetails[RC_DCC_TRAN_TIME_ZONE].elementValue, "+00");

	/*Add Cardholder Currency Amount*/
	if((pstDHIDtls[eDCCAmt].elementValue != NULL) && strlen(pstDHIDtls[eDCCAmt].elementValue) > 0)
	{
		strcpy(pstRCHIDetails[RC_DCC_CARDHOLDER_CURR_AMT].elementValue, pstDHIDtls[eDCCAmt].elementValue);
	}

	/* Add Currency Conversion Rate*/
	if((pstDHIDtls[eDCCRate].elementValue != NULL) && strlen(pstDHIDtls[eDCCRate].elementValue) > 0)
	{
		pszDecimalPoint = strchr(pstDHIDtls[eDCCRate].elementValue, '.');
		strncpy(szBeforeDecimal, pstDHIDtls[eDCCRate].elementValue, pszDecimalPoint - pstDHIDtls[eDCCRate].elementValue);
		strcpy(szAfterDecimal, pszDecimalPoint + 1);

		strcat(szExchangeRate, szBeforeDecimal);
		strcat(szExchangeRate, szAfterDecimal);

		iExchangeRate = atoi(szExchangeRate);

		sprintf(pstRCHIDetails[RC_DCC_CARDHOLDER_CONV_RATE].elementValue,"%08d", iExchangeRate);
		debug_sprintf(szDbgMsg, "%s: --- Exchange Rate is[%s] ---", __FUNCTION__, pstRCHIDetails[RC_DCC_CARDHOLDER_CONV_RATE].elementValue);
		RCHI_LIB_TRACE(szDbgMsg);
		//strcpy(pstRCHIDetails[RC_DCC_CARDHOLDER_CONV_RATE].elementValue, pstDHIDtls[eDCCRate].elementValue);
	}

	/*Add Cardholder Currency Code*/
	if((pstDHIDtls[eDCCCrncy].elementValue != NULL) && strlen(pstDHIDtls[eDCCCrncy].elementValue) > 0)
	{
		strcpy(pstRCHIDetails[RC_DCC_CARDHOLDER_CURR_CODE].elementValue, pstDHIDtls[eDCCCrncy].elementValue);
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

}

