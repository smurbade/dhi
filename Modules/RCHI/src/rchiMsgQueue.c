/*
 * rchiMsgQueue.c
 *
 *  Created on: 05-Jan-2015
 *      Author: BLR SCA TEAM
 */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <time.h>
#include <sys/timeb.h>	// For struct timeb


#include "RCHI/rchiCommon.h"
#include "RCHI/rchiMsgQueue.h"

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)



/*
 * ============================================================================
 * Function Name: createAppLogMsgQueue
 *
 * Description	: This Function would create the msg queue to send/receive app log data.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
unsigned createMsgQueue(key_t msgQKey)
{
	int				msgQId;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	if( (msgQId = msgget(msgQKey, 0666 | IPC_CREAT)) == -1)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get MSG Queue Id", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Something went wrong msgget(): %s, errno=%d", __FUNCTION__,
				strerror(errno), errno);
		RCHI_LIB_TRACE(szDbgMsg);

		return FAILURE;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Successfully Created Application Log Message Queue",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning msgQId %d", __FUNCTION__, msgQId);
	RCHI_LIB_TRACE(szDbgMsg);

	return msgQId;
}

/*
 * ============================================================================
 * Function Name: displayMsgQueueInfo
 *
 * Description	: This Function would display information about the msg queue.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
// Returns number of messages on specified queue
int displayMsgQueueInfo(unsigned msgQId)
{
	int					rv 				= SUCCESS;
	struct msqid_ds buf;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	// Flush the queue in case there are remnant messages in it
	debug_sprintf(szDbgMsg, "%s: Message Queue ID=%d", __FUNCTION__, msgQId);
	RCHI_LIB_TRACE(szDbgMsg);

	// Get a copy of the current message queue control structure and show it
	rv = msgctl(msgQId, IPC_STAT, &buf);
	if (rv == -1)
	{
		debug_sprintf(szDbgMsg, "%s: msgctl() failed!, errno=%d [%s]", __FUNCTION__, errno, strerror(errno));
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* ---
        The msqid_ds data structure is defined in <sys/msg.h> as follows:
        struct msqid_ds
        {
            struct ipc_perm msg_perm;    // Ownership and permissions
            time_t         msg_stime;    // Time of last msgsnd()
            time_t         msg_rtime;    // Time of last msgrcv()
            time_t         msg_ctime;    // Time of last change
            unsigned long  __msg_cbytes; // Current number of bytes in queue (non-standard)
            msgqnum_t      msg_qnum;     // Current number of messages in queue
            msglen_t       msg_qbytes;   // Maximum number of bytes allowed in queue
            pid_t          msg_lspid;    // PID of last msgsnd()
            pid_t          msg_lrpid;    // PID of last msgrcv()
        };
        --- */
		debug_sprintf(szDbgMsg, "%s: msg_cbytes (current # bytes on q) = %lu", __FUNCTION__, buf.msg_cbytes);
		RCHI_LIB_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_qbytes (max # of bytes on q)  = %lu", __FUNCTION__, buf.msg_qbytes);
		RCHI_LIB_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_qnum (# of messages on q)     = %lu", __FUNCTION__, buf.msg_qnum);
		RCHI_LIB_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_lspid (pid of last msgsnd)    = %d",  __FUNCTION__, buf.msg_lspid);
		RCHI_LIB_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_lrpid (pid of last msgrcv)     %d", __FUNCTION__, buf.msg_lrpid);
		RCHI_LIB_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_stime (last msgsnd time)      = %s", __FUNCTION__,
														buf.msg_stime ? ctime(&buf.msg_stime) : "Not Set");
		RCHI_LIB_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_rtime (last msgrcv time)      = %s", __FUNCTION__,
														buf.msg_rtime ? ctime(&buf.msg_rtime) : "Not Set");
		RCHI_LIB_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_ctime (last change time)      = %s", __FUNCTION__, ctime(&buf.msg_ctime));
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returned", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return buf.msg_qnum;
}

/*
 * ============================================================================
 * Function Name: flushMsgQueue
 *
 * Description	: This Function would read the message queue and ignores the data in order to flush the queue first time.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
void flushMsgQueue(unsigned msgQId)
{
	int					rv 				= SUCCESS;
	int 				i				= 0;
	char				szMsg[1024]		= "";
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	// Flush the queue in case there are remnant messages in it
	debug_sprintf(szDbgMsg, "%s: Message Queue ID=%d", __FUNCTION__, msgQId);
	RCHI_LIB_TRACE(szDbgMsg);

	// Read through all messages on queue and discard

	while( (rv = msgrcv(msgQId, szMsg, sizeof(szMsg), 0, IPC_NOWAIT)) > 0 )
	{
		i++;
		debug_sprintf(szDbgMsg, "%s: Flushing Message Queue %d - msg# %d", __FUNCTION__, msgQId, i);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returned", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

