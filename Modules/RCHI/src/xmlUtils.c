/*
 * =============================================================================
 * Filename    : xmlutils.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "DHI_App/tranDef.h"
#include "RCHI/rchiCommon.h"
#include "RCHI/utils.h"
#include "RCHI/xmlUtils.h"
#include "RCHI/rchiHostCfg.h"
#include "RCHI/rchiCfg.h"
#include "RCHI/rchiGroups.h"


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)



/* Static functions declarations */
static int validatenFillRCAmountField(char*);
static int addDataNode(VAL_LST_PTYPE, int, RCKEYVAL_PTYPE, int);
static int getCorrectNumVal(char *, char **, STR_DEF_PTYPE);
static int getCorrectMaskedString(char *, char **, MASK_STR_DEF_PTYPE);
static int getCorrectStrVal(char *, char **, STR_DEF_PTYPE);
static int getCorrectAmtVal(char *, char **, AMT_DEF_PTYPE);
static int getCorrectBoolVal(char *, int **);
static int getCorrectDateVal(char *, char **);
static int getCorrectTimeVal(char *, char **);
static int getCorrectEnumValFromList(char *, LIST_INFO_PTYPE, int **);
static int getCorrectStrFromList(char **, LIST_INFO_PTYPE, int);
static int validateAmountField(char *, AMT_DEF_PTYPE);
static int parseXMLForListElem(xmlDoc *, xmlNode *, VARLST_INFO_PTYPE,
															VAL_LST_PTYPE *);
static int parseXMLForMultiListValues(xmlNode *,LIST_INFO_PTYPE,MLVAL_PTYPE *);
static int parseXMLForData(xmlDoc *, xmlNode *, RCKEYVAL_PTYPE, int);
static void getParamValues(xmlNode *, GENKEYVAL_PTYPE *);
static int parseXMLForAttribLst(xmlDoc *, xmlNode *, VARLST_INFO_PTYPE,
															VAL_NODE_PTYPE *);
static int getAttributes(xmlNode *, RCKEYVAL_PTYPE, int);
//static void freeParamValList(GENKEYVAL_PTYPE);

/* APIs for building the xml message */
static int buildXMLTreeForVarList(xmlDocPtr, xmlNode *, VAL_LST_PTYPE, VARLST_INFO_PTYPE);
static int buildXMLTreeForRecList(xmlNode *, char *, VAL_LST_PTYPE);
static int buildXMLTreeForMultiList(xmlNode *, MLVAL_PTYPE, LIST_INFO_PTYPE);
static int buildXMLTree(xmlDocPtr, xmlNode *, RCKEYVAL_PTYPE, int);

/* ADDED_NEW TO be TESTED: VDR */
static int parseXML(xmlDoc *, char *, char *, METADATA_PTYPE);
static int isXmlNodePropReq(char*);


/*
 * ============================================================================
 * Function Name: parseXMLMsg
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int parseXMLMsg(xmlDoc * docPtr, METADATA_PTYPE metaDataPtr)
{
	int			rv				= SUCCESS;
	int			iElemRead		= 0;
	int			iMandCnt		= 0;
	xmlNode *	root			= NULL;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	iMandCnt = metaDataPtr->iMandCnt;

	debug_sprintf(szDbgMsg, "%s: Tot Cnt = [%d], Mand Cnt = [%d]",
					__FUNCTION__, metaDataPtr->iTotCnt, iMandCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	/*
	 * Praveen_P1: Added the following check for the Report response
	 * We need not parse the SSI response for the REPORT command
	 */
	if(metaDataPtr->iTotCnt == 0)
	{
		debug_sprintf(szDbgMsg, "%s: No fields to parse the XML data",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return rv;
	}

	while(1)
	{
		root = xmlDocGetRootElement(docPtr);
		if(root == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML root is NULL", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the xml for the required data */
		iElemRead = parseXMLForData(docPtr, root->xmlChildrenNode,
								metaDataPtr->keyValList, metaDataPtr->iTotCnt);
		if((iElemRead >= 0) && (iElemRead < iMandCnt))
		{
			debug_sprintf(szDbgMsg, "%s: All mand fields NOT present %d vs %d",
											__FUNCTION__, iElemRead, iMandCnt);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_FLD_REQD;
			break;
		}
		else if(iElemRead < 0)
		{
			/* Set the error value */
			rv = iElemRead;
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXMLMsg_1
 *
 * Description	:
 *
 * Input Params	: 
 *
 * Output Params:
 * ============================================================================
 */
int parseXMLMsg_1(char * szXMLMsg, char * szRoot, char * szNode, METADATA_PTYPE
																	pstMetaData)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	xmlDoc *	docPtr			= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if( (szXMLMsg == NULL) || (szRoot == NULL) || (pstMetaData == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(szXMLMsg);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);

		docPtr = xmlParseMemory(szXMLMsg, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse the xml message",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Parse the xml data retrieved from the message */
		rv = parseXML(docPtr, szRoot, szNode, pstMetaData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse xml", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getXMLRootName
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getXMLRootName(char * szXMLMsg, char * pszRootName)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	xmlDoc *	docPtr			= NULL;
	xmlNode *	rootPtr			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if( (szXMLMsg == NULL) || (pszRootName == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(szXMLMsg);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);

		docPtr = xmlParseMemory(szXMLMsg, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}
		strcpy(pszRootName, (char *)rootPtr->name);

		debug_sprintf(szDbgMsg, "%s: Root Name [%s]",__FUNCTION__, pszRootName);
		RCHI_LIB_TRACE(szDbgMsg);

		break;
	}
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: parseXMLFile
 *
 * Description	:
 *
 * Input Params	: 
 *
 * Output Params:
 * ============================================================================
 */
int parseXMLFile(char * szXMLFile, char * szRoot, char * szNode, METADATA_PTYPE
																	pstMetaData)
{
	int			rv				= SUCCESS;
	xmlDoc *	docPtr			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if( (szXMLFile == NULL) || (szRoot == NULL) || (pstMetaData == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML file using the libxml API */
		docPtr = xmlParseFile(szXMLFile);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Parse the xml data retrieved from the file */
		rv = parseXML(docPtr, szRoot, szNode, pstMetaData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse xml", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXML
 *
 * Description	:
 *
 * Input Params	: 
 *
 * Output Params:
 * ============================================================================
 */
static int parseXML(xmlDoc * docPtr, char * szRoot, char * szNode,
													METADATA_PTYPE pstMetaData)
{
	int			rv				= SUCCESS;
	int			iElemRead		= 0;
	int			iMandCnt		= 0;
	xmlNode *	rootPtr			= NULL;
	xmlNode *	curNode			= NULL;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	iMandCnt = pstMetaData->iMandCnt;

	debug_sprintf(szDbgMsg, "%s: Tot Cnt = [%d], Mand Cnt = [%d]",
					__FUNCTION__, pstMetaData->iTotCnt, iMandCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}
		else if(strcmp((char *)rootPtr->name, szRoot) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Incorrect xml document", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the current node in the xml data from where the search of
		 * required fields should begin */
		if(szNode != NULL)
		{
			curNode = findNodeInXML(szNode, rootPtr);
			if(curNode == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Cant find [%s] node in xml file",
														__FUNCTION__, szNode);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_XML_MSG;
				break;
			}
		}
		else
		{
			curNode = rootPtr->xmlChildrenNode;
		}

		/* Parse the xml data starting from the current node */
		iMandCnt = pstMetaData->iMandCnt;
		iElemRead = parseXMLForData(docPtr, curNode, pstMetaData->keyValList,
														pstMetaData->iTotCnt);
		if((iElemRead >= 0) && (iElemRead < iMandCnt))
		{
			debug_sprintf(szDbgMsg, "%s: All mand fields NOT present %d vs %d",
											__FUNCTION__, iElemRead, iMandCnt);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_FLD_REQD;
			break;
		}
		else if(iElemRead < 0)
		{
			/* Set the error value */
			rv = iElemRead;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLTreeForMultiList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildXMLTreeForMultiList(xmlNode * curNode, MLVAL_PTYPE valListPtr,
													LIST_INFO_PTYPE listInfoPtr)
{
	int				rv				= SUCCESS;
	int				iVal			= 0;
	char *			szVal			= 0;
	MLVAL_PTYPE		curValPtr		= NULL;
	xmlNodePtr		childNodePtr	= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	curValPtr = valListPtr;

	while( (rv == SUCCESS) && (curValPtr != NULL) )
	{
		iVal = curValPtr->iLstVal;
		szVal = NULL;

		getCorrectStrFromList(&szVal, listInfoPtr, iVal);

		childNodePtr = xmlNewChild(curNode, NULL, BAD_CAST szVal, NULL);

		curValPtr = curValPtr->next;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLTreeForVarList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildXMLTreeForVarList(xmlDocPtr docPtr, xmlNode * curNode, VAL_LST_PTYPE valLstPtr,
												VARLST_INFO_PTYPE metaInfoPtr)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iNodeType		= 0;
	int				iTmpCnt			= 0;
	VAL_NODE_PTYPE	valNodePtr		= NULL;
	xmlNodePtr		childNodePtr	= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while( (metaInfoPtr[iTmpCnt].nodeType) != -1)
	{
		iTmpCnt++;
	}
	valNodePtr = valLstPtr->start;
	while((rv == SUCCESS) && (valNodePtr != NULL))
	{
		while((iNodeType = metaInfoPtr[iCnt].nodeType) != -1)
		{	
			if(iNodeType == valNodePtr->type)
			{
				if(metaInfoPtr[iCnt].szNodeStr != NULL)
				{
					childNodePtr = xmlNewChild(curNode, NULL, BAD_CAST
										metaInfoPtr[iCnt].szNodeStr, NULL);
					if(isXmlNodePropReq(metaInfoPtr[iCnt].szNodeStr) == SUCCESS)
					{
						xmlNewProp(childNodePtr, BAD_CAST "Sequence", BAD_CAST "");
						xmlNewProp(childNodePtr, BAD_CAST "LoadID", BAD_CAST "");
						xmlNewProp(childNodePtr, BAD_CAST "LastModified", BAD_CAST "");
					}

				}
				else
				{
					childNodePtr = curNode;
					iTmpCnt--;
				}					
				rv = buildXMLTree(docPtr, childNodePtr, valNodePtr->elemList,
														valNodePtr->elemCnt);
				if(rv != SUCCESS)
				{
					break;
				}				
			}			
			iCnt++;						
		}
		if(iTmpCnt <= 0 )
		{
			debug_sprintf(szDbgMsg, "%s: Element in varlist are over",
												__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}
		iCnt = 0;	
		valNodePtr = valNodePtr->next;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLTreeForRecList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildXMLTreeForRecList(xmlNode * curNode, char * szName,
													VAL_LST_PTYPE valLstPtr)
{
	int				rv				= SUCCESS;
	VAL_NODE_PTYPE	valNodePtr		= NULL;
	xmlNodePtr		childNodePtr	= NULL;
	xmlNodePtr		textNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	valNodePtr = valLstPtr->start;

	while(valNodePtr != NULL)
	{
		childNodePtr = xmlNewChild(curNode, NULL, BAD_CAST szName, NULL);

		if(valNodePtr->szVal != NULL)
		{
			textNodePtr = xmlNewText(BAD_CAST valNodePtr->szVal);
			xmlAddChild(childNodePtr, textNodePtr);
		}

		valNodePtr = valNodePtr->next;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

static int isXmlNodePropReq(char* key)
{
	if (strcmp(key, "basket") == SUCCESS)
		return SUCCESS;
	else if (strcmp(key, "lineItem") == SUCCESS)
		return SUCCESS;
	else if (strcmp(key, "refundItem") == SUCCESS)
		return SUCCESS;
	else if (strcmp(key, "redemptionItem") == SUCCESS)
		return SUCCESS;

	return FAILURE;
}
/*
 * ============================================================================
 * Function Name: buildXMLTree
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildXMLTree(xmlDocPtr docPtr, xmlNode * curNode, RCKEYVAL_PTYPE listPtr, int iTotCnt)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iListVal		= 0;
	char *			szListVal		= NULL;
	xmlNodePtr		elemNodePtr		= NULL;
	xmlNodePtr		textNodePtr		= NULL;
	xmlNodePtr		tempNodePtr		= NULL;
	LIST_INFO_PTYPE	listInfoPtr		= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[5712]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//RCHI_LIB_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (iCnt < iTotCnt))
	{
		if(listPtr[iCnt].value == NULL)
		{
			if(listPtr[iCnt].valType == NULL_ADD)
			{
				elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST listPtr[iCnt].key, NULL);
			}

			iCnt++;
			continue;
		}

		switch(listPtr[iCnt].valType)
		{
		case SINGLETON:
		case STRING:
//		case AMOUNT:
		case DATE:
		case TIME:
		case NUMERICS:
		case MASK_NUM_STR:
		case NULL_ADD:

			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST listPtr[iCnt].key, NULL);

			if (strcmp(listPtr[iCnt].key, "PAYLOAD") == SUCCESS)
			{
				/*Using string length for the size of the buffer
				 * Ideally we should use the size given when building the xml.
				 * Needs a revisit here.
				 */
				/*
				 * Now we have PAYLOAD tag for two packets, one for basket
				 * which is the CDATA block and other one for ERESPONSE packet
				 * Having following condition to check whether we need to include
				 * CDATA block for Payload or add it as the test
				 * If the value contains the BasketSubmission as the root name
				 * then considering it as the basket packet and creating the CDATA block
				 * In case of ERESPONSE the data would be encoded text, so getXMLRootNameS
				 * fails, adding the value as the text to that node
				 */

				/* Looking for the BasketSubmission in the value field so that we will make it CDATA
				 * as required by the bapi host.
				 */

				if(strstr(listPtr[iCnt].value, "BasketSubmission") != NULL)
				{
					textNodePtr = xmlNewCDataBlock(docPtr, listPtr[iCnt].value, strlen(listPtr[iCnt].value));
				}
				else
				{
					textNodePtr = xmlNewText(BAD_CAST listPtr[iCnt].value);
				}
			}
			else
			{
				textNodePtr = xmlNewText(BAD_CAST listPtr[iCnt].value);
			}

			if(textNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Testnode is NULL while adding value for key %s",
														__FUNCTION__, listPtr[iCnt].key);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			tempNodePtr = xmlAddChild(elemNodePtr, textNodePtr);

			if(tempNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: tempNodePtr is NULL while adding value for key %s", 
															__FUNCTION__, listPtr[iCnt].key);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			break;

		case AMOUNT:
			/* PRADEEP : 18-11-2016: If any field is configured as Amount in the rchi pass through file, first we will check whether the amount is filled as per RC format,
			 * if it is as per RC amount format we won't change anything the same will be sent to RC Host, if it is not as per RC amount format we will change it from
			 * SSI amount format to RC Format and it will be sent to RC Host
			 * if Field is not in Format of SSI AMOUNT (decimal amount should be either 2 or 3) it will be ignored*/
			rv = validatenFillRCAmountField(listPtr[iCnt].value);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: RC Amount is not in Proper format[%s]",
														__FUNCTION__, (char*)listPtr[iCnt].value);
				RCHI_LIB_TRACE(szDbgMsg);
				rv = SUCCESS;
				break;
			}
			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST listPtr[iCnt].key, NULL);

			textNodePtr = xmlNewText(BAD_CAST listPtr[iCnt].value);

			if(textNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Testnode is NULL while adding value for key %s",
														__FUNCTION__, listPtr[iCnt].key);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			tempNodePtr = xmlAddChild(elemNodePtr, textNodePtr);

			if(tempNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: tempNodePtr is NULL while adding value for key %s",
															__FUNCTION__, listPtr[iCnt].key);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			break;

		case BOOLEAN:
			iListVal = * ((int *)listPtr[iCnt].value);
			if(iListVal == RCHI_TRUE)
			{
				textNodePtr = xmlNewText(BAD_CAST "TRUE");
			}
			else
			{
				textNodePtr = xmlNewText(BAD_CAST "FALSE");
			}

			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST
													listPtr[iCnt].key, NULL);
			xmlAddChild(elemNodePtr, textNodePtr);

			break;

		case LIST:
			listInfoPtr = (LIST_INFO_PTYPE) listPtr[iCnt].valMetaInfo;
			iListVal = * ((int *)listPtr[iCnt].value);
			szListVal = NULL;

			getCorrectStrFromList(&szListVal, listInfoPtr, iListVal);

			textNodePtr = xmlNewText(BAD_CAST szListVal);
			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST
													listPtr[iCnt].key, NULL);
			xmlAddChild(elemNodePtr, textNodePtr);

			break;

		case VARLIST:
	
			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST listPtr[iCnt].key,
																		NULL);
			if(isXmlNodePropReq(listPtr[iCnt].key) == SUCCESS)
			{
				xmlNewProp(elemNodePtr, BAD_CAST "Sequence", BAD_CAST "");
				xmlNewProp(elemNodePtr, BAD_CAST "LoadID", BAD_CAST "");
				xmlNewProp(elemNodePtr, BAD_CAST "LastModified", BAD_CAST "");
			}

			rv = buildXMLTreeForVarList(docPtr, elemNodePtr, listPtr[iCnt].value,
												listPtr[iCnt].valMetaInfo);
			break;

		case RECLIST:
			rv = buildXMLTreeForRecList(curNode, listPtr[iCnt].key,
														listPtr[iCnt].value);
			break;

		case MULTILIST:
			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST listPtr[iCnt].key,
																		NULL);
			rv = buildXMLTreeForMultiList(elemNodePtr, listPtr[iCnt].value,
												listPtr[iCnt].valMetaInfo);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv == SUCCESS)
		{
			iCnt++;
		}
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	//RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLAdminReqMsgFromMetaData
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildXMLAdminReqMsgFromMetaData(char ** szDestPtr, int * iSize, char * pszRootMsg,
														METADATA_PTYPE metaDataPtr)
{
	int				rv				= SUCCESS;
	int				iTotCnt			= 0;
	int				iCnt			= 0;
	xmlDocPtr		docPtr			= NULL;
	xmlNodePtr		elemNodePtr		= NULL;
	xmlNodePtr		childNode		= NULL;
	xmlNodePtr		rootPtr			= NULL;
	xmlAttrPtr 		newAttr			= NULL;
	RCKEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		/* Create the xml document */
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Create the root node for the xml document */
		rootPtr = xmlNewNode(NULL, BAD_CAST pszRootMsg);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the root element for the doc */
		xmlDocSetRootElement(docPtr, rootPtr);

		childNode = xmlNewChild(rootPtr, NULL, BAD_CAST "HEADER", NULL);

		listPtr = metaDataPtr->keyValList;
		iTotCnt = metaDataPtr->iTotCnt;

		while (iTotCnt > 0 )
		{
			elemNodePtr = xmlNewChild(childNode, NULL, BAD_CAST listPtr[iCnt].key,
													BAD_CAST listPtr[iCnt].value);
			iCnt++;
			iTotCnt--;
		}

		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "ADMIN", NULL);

		newAttr = xmlNewProp (elemNodePtr, BAD_CAST "COMMAND",
										   BAD_CAST "SETUP_REQUEST_V3");
		break;
	}

	if(rv == SUCCESS)
	{
		/* If the XML message tree has formed correctly, dump this message into
		 * the string provided as parameter */
		xmlDocDumpFormatMemory(docPtr, (xmlChar **) szDestPtr, iSize, 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build XML tree", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	/* Free the memory allocated for the xml document tree */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLMsgFromMetaData
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildXMLMsgFromMetaData(char ** szDestPtr, int * iSize, char * pszMsgType,
												METADATA_PTYPE metaDataPtr)
{
	int				rv				= SUCCESS;
	int				iTotCnt			= 0;
	char			szxmlsValue[50]	= "";
	char			szRCVersion[10]	= "";
	xmlDocPtr		docPtr			= NULL;
	xmlNodePtr		rootPtr			= NULL;
	xmlNodePtr		childNode		= NULL;
	xmlAttrPtr 		newAttr			= NULL;
	RCKEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
//	char			szDbgMsg[7120]	= ""; //Remove this please
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		/* Create the xml document */
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldn't create doc",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Create the root node for the xml document */
		rootPtr = xmlNewNode(NULL, BAD_CAST "GMF");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#if 0
		if(version == 1)
		{
			newAttr = xmlNewProp (rootPtr, BAD_CAST "xmlns", BAD_CAST "com/firstdata/Merchant/gmfV4.02");
		}
		else
		{
			newAttr = xmlNewProp (rootPtr, BAD_CAST "xmlns", BAD_CAST "com/firstdata/Merchant/gmfV3.10");
		}
#endif

		/* Making RcVersion as Configurable*/
		strcpy(szxmlsValue, "com/firstdata/Merchant/gmfV");
		getRCVersion(szRCVersion);
		strcat(szxmlsValue, szRCVersion);

		newAttr = xmlNewProp (rootPtr, BAD_CAST "xmlns", BAD_CAST szxmlsValue);

		/* Set the root element for the doc */
		xmlDocSetRootElement(docPtr, rootPtr);

		childNode = xmlNewChild(rootPtr, NULL, BAD_CAST pszMsgType, NULL);

		listPtr = metaDataPtr->keyValList;
		iTotCnt = metaDataPtr->iTotCnt;

		/* Build the xml tree from the data provided */
		rv = buildXMLTree(docPtr, childNode, listPtr, iTotCnt);

		break;
	}

	if(rv == SUCCESS)
	{
		/* If the XML message tree has formed correctly, dump this message into
		 * the string provided as parameter */
		xmlDocDumpFormatMemory(docPtr, (xmlChar **) szDestPtr, iSize, 0);

		debug_sprintf(szDbgMsg, "%s: Total Bytes = [%d]", __FUNCTION__, *iSize);
		RCHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build XML tree", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	/* Free the memory allocated for the xml document tree */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	if (rv == FAILURE)
	{
		rv = ERR_INV_XML_MSG;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: findNodeInXML
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
xmlNode * findNodeInXML(char * szNodeId, xmlNode * curNode)
{
	xmlNode *	nxtNode			= NULL;
	xmlNode *	reqdNode		= NULL;

	if((curNode->type == XML_ELEMENT_NODE) &&
		(xmlStrcmp(curNode->name, (const xmlChar *) szNodeId) == SUCCESS) ) {
		/* Element found */
		reqdNode = curNode;
	}
	else {
		/* Search in other places */
		/* Go for the child node */
		nxtNode = curNode->xmlChildrenNode;
		if(nxtNode != NULL) {
			reqdNode = findNodeInXML(szNodeId, nxtNode);
		}

		if(reqdNode == NULL) {
			/* If all the child nodes are exhausted, go for the sibling node */
			nxtNode = curNode->next;
			if(nxtNode != NULL) {
				reqdNode = findNodeInXML(szNodeId, nxtNode);
			}
		}
	}

	return reqdNode;
}

/*
 * ============================================================================
 * Function Name: parseXMLForData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXMLForData(xmlDoc * docPtr, xmlNode * curNode,
										RCKEYVAL_PTYPE keyValList, int keyCnt)
{
	int				rv				= SUCCESS;
	int				iRetVal			= 0;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iLen			= 0;
	char *			cPtr			= NULL;
	xmlChar *		value			= NULL;
	xmlNode *		nxtNode 		= NULL;
	RCHI_BOOL		bMatched		= RCHI_FALSE;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	//RCHI_LIB_TRACE(szDbgMsg);

	while( (iCnt < keyCnt) && (rv == SUCCESS) )
	{
		if(curNode->type == 3) /* TEXT TYPE NODE */
		{
			/* No need for checking this node */
			break;
		}

		if(curNode->type == XML_ENTITY_REF_NODE)
		{
			debug_sprintf(szDbgMsg, "%s: Node is of type XML_ENTITY_REF_NODE", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		if(!xmlStrcmp(curNode->name, (const xmlChar *)keyValList[iCnt].key))
		{
			switch(keyValList[iCnt].valType)
			{
			case NUMERICS:
				/* Get the correct value according to the allowable types of
				 * strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectNumVal((char *) value,
								(char **) &keyValList[iCnt].value,
								(STR_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == RCHI_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case MASK_NUM_STR:
				/* Get the correct value according to the allowable types of
				 * strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectMaskedString((char *) value,
								(char **) &keyValList[iCnt].value,
							(MASK_STR_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == RCHI_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case STRING:
				/* Get the correct value according to the allowable types of
				 * strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectStrVal((char *) value,
								(char **) &keyValList[iCnt].value,
								(STR_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == RCHI_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case AMOUNT:
				/* Get the correct value according to the allowable types of
				 * boolean strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectAmtVal((char *) value,
								(char **) &keyValList[iCnt].value,
								(AMT_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == RCHI_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case DATE:
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectDateVal((char * )value,
											(char **) &keyValList[iCnt].value);
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == RCHI_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case TIME:
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode,1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectTimeVal((char * )value,
											(char **) &keyValList[iCnt].value);
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == RCHI_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case BOOLEAN:
				/* Get the correct value according to the allowable types of
				 * boolean strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectBoolVal((char * )value,
											(int **) &keyValList[iCnt].value);
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == RCHI_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case SINGLETON:
				/* Get the correct value according to the allowable types of
				 * numerics strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					iLen = xmlStrlen(value);
					if(iLen > 0)
					{
						cPtr = (char *) malloc(iLen + 1);
						memset(cPtr, 0x00, iLen + 1);

						memcpy(cPtr, value, iLen);
						keyValList[iCnt].value = cPtr;
						cPtr = NULL;
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = RCHI_TRUE;
						/* If the value is mandatory and is read, update the
						 * mandatory count */
						if(keyValList[iCnt].isMand == RCHI_TRUE)
						{
							jCnt++;
						}
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break; /* End of SINGLETON switch case */

			case LIST:
				/* Get the correct value according to the allowable types of
				 * LIST strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectEnumValFromList((char *)value,
								(LIST_INFO_PTYPE)keyValList[iCnt].valMetaInfo,
								(int **) &keyValList[iCnt].value);
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == RCHI_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break; /* End of LIST switch case */

			case MULTILIST:

				if(curNode->xmlChildrenNode == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: No child node", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					rv = parseXMLForMultiListValues(curNode->xmlChildrenNode,
								(LIST_INFO_PTYPE)keyValList[iCnt].valMetaInfo,
								(MLVAL_PTYPE *) &keyValList[iCnt].value);

					if(keyValList[iCnt].isMand == RCHI_TRUE)
					{
						if(rv == SUCCESS)
						{
							jCnt++;
						}
					}
					else
					{
						rv = SUCCESS;
					}
				}

				break; /* End of MULTILIST switch case */

			case VARLIST:

				if(curNode->xmlChildrenNode == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: No child node", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
			
					/* Parse xml and store the variable linked list of data */
					rv = parseXMLForListElem(docPtr, curNode->xmlChildrenNode,
							(VARLST_INFO_PTYPE) keyValList[iCnt].valMetaInfo,
							(VAL_LST_PTYPE *) &keyValList[iCnt].value);
					if(keyValList[iCnt].isMand == RCHI_TRUE)
					{
						if(rv == SUCCESS)
						{
							jCnt++;
						}
					}
					else
					{
						rv = SUCCESS;
					}
				}

				break; /* End of VARLIST switch case */

			case ATTR_LKDLST:

				if(curNode->xmlChildrenNode == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: No child node", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Parse xml and store the variable linked list of data */
					rv = parseXMLForAttribLst(docPtr, curNode->xmlChildrenNode,
							(VARLST_INFO_PTYPE) keyValList[iCnt].valMetaInfo,
							(VAL_NODE_PTYPE *) &keyValList[iCnt].value);

					if(keyValList[iCnt].isMand == RCHI_TRUE)
					{
						if(rv == SUCCESS)
						{
							jCnt++;
						}
					}
					else
					{
						rv = SUCCESS;
					}
				}

				break; /* End of ATTR_LKDLST switch case */

			default:
				debug_sprintf(szDbgMsg, "%s: Undefined type = [%d]",
										__FUNCTION__, keyValList[iCnt].valType);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break; /* End of default switch case */

			} /* End of SWITCH */
		} /* End of if condition*/
		/* Deallocating any allocated memory not in use now */
		if(value != NULL)
		{
			xmlFree(value);
			value = NULL;
		}
		if(bMatched == RCHI_TRUE)
		{
			/*Given key matched with XML node*/
			bMatched = RCHI_FALSE;
			break;		
		}
		if(rv == SUCCESS)
		{
			/*Checking Next Key*/
			iCnt++;
		}
	} /* End of WHILE loop */
	if(rv == SUCCESS)
	{
		/* Go for the child Node */
		nxtNode = curNode->xmlChildrenNode;
		if(nxtNode)
		{
			iRetVal = parseXMLForData(docPtr, nxtNode, keyValList, keyCnt);
			if(iRetVal >= 0)
			{
				jCnt += iRetVal;
			}
			else
			{
				jCnt = iRetVal;
			}
		}

		if(jCnt >= 0)
		{
			/* go for the sibling node */
			nxtNode = curNode->next;
			if(nxtNode)
			{
				iRetVal = parseXMLForData(docPtr, nxtNode, keyValList, keyCnt);
				if(iRetVal >= 0)
				{
					jCnt += iRetVal;
				}
				else
				{
					jCnt = iRetVal;
				}
			}
		}
	}
	else
	{
		jCnt = rv;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, jCnt);
	//RCHI_LIB_TRACE(szDbgMsg);
	return jCnt;
}

/*
 * ============================================================================
 * Function Name: parseXMLForMultiListValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXMLForMultiListValues(xmlNode * curNode, LIST_INFO_PTYPE refLst,
													MLVAL_PTYPE * valueList)
{
	int			rv				= SUCCESS;
	int			iCnt			= 0;
	int *		iVal			= NULL;
	char *		value			= NULL;
	MLVAL_PTYPE	tmpValPtr		= NULL;
	MLVAL_PTYPE	curValPtr		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (curNode != NULL))
	{
		if(curNode->type == 3)
		{
			curNode = curNode->next;
			continue;
		}

		value = (char *) curNode->name;

		rv = getCorrectEnumValFromList(value, refLst, &iVal);
		if(rv == SUCCESS)
		{
			tmpValPtr = (MLVAL_PTYPE) malloc(MLVAL_SIZE);
			if(tmpValPtr)
			{
				memset(tmpValPtr, 0x00, MLVAL_SIZE);
				tmpValPtr->iLstVal = *iVal;

				if(curValPtr == NULL)
				{
					curValPtr = tmpValPtr;
					*valueList = tmpValPtr;
				}
				else
				{
					curValPtr->next = tmpValPtr;
					curValPtr = tmpValPtr;
				}

				iCnt++;
			}
		}

		if(iVal != NULL)
		{
			free(iVal);
			iVal = NULL;
		}

		curNode = curNode->next;
	}

	/* If not even a single value was found */
	if(iCnt == 0)
	{
		debug_sprintf(szDbgMsg, "%s: No values found", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_NO_FLD;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getAttributes
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getAttributes(xmlNode * curNode, RCKEYVAL_PTYPE listPtr, int totCnt)
{
	int			rv				= SUCCESS;
	int			iCnt			= 0;
	int			jCnt			= 0;
	int			iLen			= 0;
	char *		cTmpPtr			= NULL;
	xmlChar *	value			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//RCHI_LIB_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < totCnt; iCnt++)
	{
		value = xmlGetProp(curNode, (const xmlChar *) listPtr[iCnt].key);
		if(value != NULL)
		{
			iLen = strlen((char *) value);
			if(iLen > 0)
			{
				cTmpPtr = (char *) malloc(iLen + 1);
				if(cTmpPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILURE", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(cTmpPtr, 0x00, iLen + 1);
				memcpy(cTmpPtr, value, iLen);
				listPtr[iCnt].value = cTmpPtr;

				if(listPtr[iCnt].isMand == RCHI_TRUE)
				{
					jCnt++;
				}
			}

			xmlFree(value);
		}
	}

	if(rv == SUCCESS)
	{
		rv = jCnt;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	//RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXMLForAttribLst
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXMLForAttribLst(xmlDoc * docPtr, xmlNode * curNode,
						VARLST_INFO_PTYPE varLst, VAL_NODE_PTYPE * listBegin)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	int				iSize			= 0;
	int				iAttribRead		= 0;
	xmlChar *		value			= NULL;
	char *			cTmpPtr			= NULL;
	RCKEYVAL_PTYPE	keyLstPtr		= NULL;
	VAL_NODE_PTYPE	tmpNodePtr		= NULL;
	VAL_NODE_PTYPE	curNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (curNode != NULL))
	{
		if(curNode->type == 3)
		{
			curNode = curNode->next;
			continue;
		}

		while( (rv == SUCCESS) && (varLst[iCnt].szNodeStr != NULL) )
		{
			keyLstPtr = NULL;
			tmpNodePtr = NULL;
			iMandCnt = 0;
			cTmpPtr = NULL;

			if( !xmlStrcmp(curNode->name,
									(const xmlChar *)varLst[iCnt].szNodeStr) )
			{
				/* Get the value of the current node if present */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value != NULL)
				{
					iSize = strlen((const char *)value);
					cTmpPtr = (char *) malloc(iSize + 1);
					if(cTmpPtr != NULL)
					{
						memset(cTmpPtr, 0x00, iSize + 1);
						memcpy(cTmpPtr, value, iSize);
					}
				}

				/* Get the elements' count */
				iTotCnt = varLst[iCnt].iElemCnt;
				iSize = iTotCnt * RCKEYVAL_SIZE;

				/* Allocate memory for the key list */
				keyLstPtr = (RCKEYVAL_PTYPE) malloc(iSize);
				if(keyLstPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed for key list",
																__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				/* Copy the reference list here */
				memset(keyLstPtr, 0x00, iSize);
				memcpy(keyLstPtr, varLst[iCnt].keyList, iSize);

				/* Get the count of the mandatory fields */
				for(jCnt = 0; jCnt < iTotCnt; jCnt++)
				{
					if(keyLstPtr[jCnt].isMand == RCHI_TRUE)
					{
						/* Increment the mandatory field count */
						iMandCnt++;
					}
				}

				/* Get the attribute values for the list of attributes */
				iAttribRead = getAttributes(curNode, keyLstPtr, iTotCnt);

				if((iAttribRead >= 0) && (iAttribRead < iMandCnt))
				{
					/* VDR: FIXME: Need to free the memory allocated to the
					 * key list */
					debug_sprintf(szDbgMsg,
								"%s: All mand attributes NOT present %d vs %d",
								__FUNCTION__, iAttribRead, iMandCnt);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = ERR_FLD_REQD;
					break;
				}
				else if(iAttribRead < 0)
				{
					rv = iAttribRead;
					break;
				}

				/* Allocate memory for the data node */
				tmpNodePtr = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
				if(tmpNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed for data node",
																__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(tmpNodePtr, 0x00, sizeof(VAL_NODE_STYPE));
				tmpNodePtr->elemCnt = iTotCnt;
				tmpNodePtr->elemList = keyLstPtr;
				tmpNodePtr->type = varLst[iCnt].nodeType;
				tmpNodePtr->szVal = cTmpPtr;

				/* Add the data in the linked list */
				if(curNodePtr == NULL)
				{
					curNodePtr = tmpNodePtr;
					*listBegin = curNodePtr;
				}
				else
				{
					curNodePtr->next = tmpNodePtr;
					curNodePtr = tmpNodePtr;
				}

				break;
			}

			iCnt++;
		}

		if(varLst[iCnt].nodeType == -1)
		{
			/* None of the string matched with the element */
			debug_sprintf(szDbgMsg, "%s: [%s] is invalid node string",
												__FUNCTION__, curNode->name);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
		}

		if(rv == SUCCESS)
		{
			RCHI_LIB_TRACE(szDbgMsg);
			curNode = curNode->next;
		}
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXMLForListElem
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXMLForListElem(xmlDoc * docPtr, xmlNode * curNode,
					VARLST_INFO_PTYPE varLstPtr, VAL_LST_PTYPE * valNodesLst)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	int				iTotCnt			= 0;
	int				iElemRead		= 0;
	int				iMandCnt		= 0;
	int				iTmpCnt			= 0;
	xmlNode	*		tmpNode			= NULL;
	RCKEYVAL_PTYPE	keyLstPtr		= NULL;
	VAL_LST_PTYPE	valNodesLstPtr	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	valNodesLstPtr = (VAL_LST_PTYPE) malloc(VAL_LST_SIZE);
	if(valNodesLstPtr == NULL)
	{
		/* If the memory is not allocated, return ERROR */
		debug_sprintf(szDbgMsg, "%s: Malloc failed for value nodes list ptr",
																__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* Initialize the memory if allocated */
		memset(valNodesLstPtr, 0x00, VAL_LST_SIZE);

		*valNodesLst = valNodesLstPtr;
	}
	tmpNode = curNode;
	while( (tmpNode != NULL) )
	{
		if(!strcmp((char *)tmpNode->name, "text"))
		{
			tmpNode = tmpNode->next;
			continue;
		}
		else
		{	
			iTmpCnt++;
			tmpNode = tmpNode->next;			
		}
	}
	while( (rv == SUCCESS) && (curNode != NULL) )
	{
		if(curNode->type == 3)
		{
			curNode = curNode->next;
			continue;
		}

		while( (rv == SUCCESS) && (varLstPtr[iCnt].nodeType != -1) )
		{
			if( (varLstPtr[iCnt].szNodeStr == NULL) ||
				(!xmlStrcmp(curNode->name,
							(const xmlChar *)varLstPtr[iCnt].szNodeStr) )  )
			{				
				/* Get the elements count */
				iTotCnt = varLstPtr[iCnt].iElemCnt;
				iSize = RCKEYVAL_SIZE * iTotCnt;
				iMandCnt = 0;

				/* Allocate memory for the key list */
				keyLstPtr = (RCKEYVAL_PTYPE) malloc(iSize);
				if(keyLstPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed for key list",
																__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				/* Copy the reference list here */
				memset(keyLstPtr, 0x00, iSize);
				memcpy(keyLstPtr, varLstPtr[iCnt].keyList, iSize);

				/* Get the count of mandatory fields */
				for(jCnt = 0; jCnt < iTotCnt; jCnt++)
				{
					if(keyLstPtr[jCnt].isMand == RCHI_TRUE)
					{
						/* Increment the mandatory field count */
						iMandCnt++;
					}
				}

				/* Parse the xml for the required data */
				if(varLstPtr[iCnt].szNodeStr == NULL)
				{
					iElemRead = parseXMLForData(docPtr, curNode, keyLstPtr,
																	iTotCnt);
				}
				else
				{
					iElemRead = parseXMLForData(docPtr,curNode->xmlChildrenNode,
															keyLstPtr, iTotCnt);
				}

				if((iElemRead >= 0) && (iElemRead < iMandCnt))
				{
					debug_sprintf(szDbgMsg,
									"%s: All mand fields NOT present %d vs %d",
									__FUNCTION__, iElemRead, iMandCnt);
					RCHI_LIB_TRACE(szDbgMsg);

					/* VDR:FIXME : Free the memory which is not required */

					rv = ERR_FLD_REQD;
					break;
				}
				else if(iElemRead < 0)
				{
					rv = iElemRead;
					break;
				}
				/* Add the data in the linked list */
				rv = addDataNode(valNodesLstPtr, varLstPtr[iCnt].nodeType,
															keyLstPtr, iTotCnt);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Addition of data node FAILED",
																__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
				//Decrement the counter after adding the node.
				iTmpCnt--;

				break;
			}
			iCnt++;
		}
		#if 0 //ArjunU1: Don't check this condition*/
		if(varLstPtr[iCnt].nodeType == -1)
		{
			/* None of the string matched with the element */
			debug_sprintf(szDbgMsg, "%s: [%s] is invalid node string",
												__FUNCTION__, curNode->name);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
		}
		#endif
		if(rv == SUCCESS)
		{
			if(iTmpCnt <= 0 )
			{
				debug_sprintf(szDbgMsg, "%s: Element in varlist are over",
													__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}
			iCnt = 0; //ArjunU1: We must reset this for next iteration.
			/*Checking next node*/
			curNode = curNode->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectStrVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectStrVal(char * xmlStr, char ** dpszStrVal, STR_DEF_PTYPE
																		defPtr)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char *	cPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	iLen = strlen(xmlStr);
	if((iLen < defPtr->minLen) || (iLen > defPtr->maxLen))
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid string value",
														__FUNCTION__, xmlStr);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_INV_FLD;
	}
	else
	{
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszStrVal = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectMaskedString
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectMaskedString(char * xmlStr, char ** dpszStrVal,
													MASK_STR_DEF_PTYPE defPtr)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	int		iMaskedLen		= 0;
	int		iCnt			= 0;
	char *	cPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		iLen = strlen(xmlStr);
		if((iLen < defPtr->minLen) || (iLen > defPtr->maxLen))
		{
			debug_sprintf(szDbgMsg, "%s: [%s] is an invalid string value",
														__FUNCTION__, xmlStr);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		if(strspn(xmlStr, "0123456789*") != iLen)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid characters in the string [%s]",
														__FUNCTION__, xmlStr);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		cPtr = xmlStr;

		for(iCnt = 0; iCnt < defPtr->prefixLen; iCnt++)
		{
			if(*cPtr == '*')
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Masked String [%s]",
														__FUNCTION__, xmlStr);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cPtr++;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		iMaskedLen = iLen - (defPtr->prefixLen) - (defPtr->suffixLen);

		for(iCnt = 0; iCnt < iMaskedLen; iCnt++)
		{
			if(*cPtr != '*')
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Masked String [%s]",
														__FUNCTION__, xmlStr);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cPtr++;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		for(iCnt = 0; iCnt < defPtr->suffixLen; iCnt++)
		{
			if(*cPtr == '*')
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Masked String [%s]",
														__FUNCTION__, xmlStr);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cPtr++;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate the memory for storing the value */
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszStrVal = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectNumVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectNumVal(char * xmlStr, char ** dpszNumVal, STR_DEF_PTYPE
																		defPtr)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char *	cPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);

	/* Validate the field for numeric */
	iLen = strlen(xmlStr);
	if((iLen < defPtr->minLen) || (iLen > defPtr->maxLen))
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid string value (length)",
														__FUNCTION__, xmlStr);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_INV_FLD;
	}
	else if((rv = validateNumericField(xmlStr)) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid numeric value",
														__FUNCTION__, xmlStr);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_INV_FLD;
	}
	else
	{
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszNumVal = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectAmtVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectAmtVal(char * xmlStr, char ** dpszAmt,AMT_DEF_PTYPE defPtr)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char *	cPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);

	/* Validate the field for amount */
	rv = validateAmountField(xmlStr, defPtr);
	if(rv == SUCCESS)
	{
		/* Amount is valid, save the value */
		iLen = strlen(xmlStr);
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszAmt = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid amount", __FUNCTION__,
																		xmlStr);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_INV_FLD;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: validatenFillRCAmountField
 *
 * Description	: This will change the amount in RC field from SSI format to RC format
 * 				  1)if it is already in RC Format this function will not change anything
 * 				  2)If it is correct SSI Amount format it will be changed to RC format
 * 				  3)If it is in Wrong SSI Amount Format this function will return FAILURE
 *
 * Input Params	:char* (RC AMOUNT)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int validatenFillRCAmountField(char * pszRCAmount)
{
	int				rv						= SUCCESS;
	int 			iAmount					= 0;
	int				iLen					= 0;
	int				iValLen					= 0;
	int				iDecimalLen				= 0;
	char *			cCurPtr					= NULL;
	char *			cNxtPtr					= NULL;
	double			fAmount					= 0.0;
	RCHI_BOOL		bChangeinFormatRqd 		= RCHI_FALSE;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Validate the field for amount */

	while(1)
	{
		iValLen = strlen(pszRCAmount);
		if(iValLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No value present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
			break;
		}

		/* Check for characters */
		if(strspn(pszRCAmount, "1234567890-") != iValLen)
		{
			debug_sprintf(szDbgMsg, "%s: Characters other than [0-9] & [-] found",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			bChangeinFormatRqd = RCHI_TRUE;

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: RC Amount is in proper format",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = SUCCESS;
			break;

		}

		if(bChangeinFormatRqd == RCHI_TRUE)
		{
			if(strspn(pszRCAmount, "1234567890.-") != iValLen)
			{
				debug_sprintf(szDbgMsg, "%s: Characters other than [0-9], [.] & [-] found",
						__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			else
			{
				cCurPtr = strchr(pszRCAmount, '.');
				if(cCurPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Decimal character not present",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				cNxtPtr = strchr(cCurPtr + 1, '.');
				if(cNxtPtr != NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Multiple instances of dec character",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				iLen = cCurPtr - pszRCAmount;

				iDecimalLen = iValLen-iLen-1; // removing ':' and integer part of amount, we will get decimal length

				if(iDecimalLen == 2 || iDecimalLen == 3)
				{

					if(iDecimalLen == 2)
					{
						fAmount = atof(pszRCAmount) * 100;
						iAmount = fAmount;
						sprintf(pszRCAmount,"%.03d",iAmount);

						debug_sprintf(szDbgMsg,"%s: Amount is %s",__FUNCTION__, pszRCAmount);
						RCHI_LIB_TRACE(szDbgMsg);
					}
					else if(iDecimalLen == 3)
					{
						fAmount = atof(pszRCAmount) * 1000;
						iAmount = fAmount;
						sprintf(pszRCAmount,"%.04d",iAmount);

						debug_sprintf(szDbgMsg,"%s: Amount is %s",__FUNCTION__, pszRCAmount);
						RCHI_LIB_TRACE(szDbgMsg);
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Amount Contains decimal part of neither 2 nor 3",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getCorrectDateVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectDateVal(char * xmlStr, char ** szDatePtr)
{
	int		rv				= SUCCESS;
	int		day				= 0;
	int		maxDayVal		= 0;
	int		month			= 0;
	int		year			= 0;
	int		iLen			= 0;
	char	szTmp[5]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		iLen = strlen(xmlStr);

		/*
		 * Praveen_P1: Lately PWC changed the DATE format for DUP_CHECK report from
		 * YYYY.MM.DD to MM/DD/YYYY
		 * Now supporting both the formats in SCA
		 *
		 */

		/* YYYY.MM.DD -> 10 characters */
		/* MM/DD/YYYY -> 10 characters */

		if( (iLen != 10) || (strspn(xmlStr, "0123456789./") != iLen) )
		{
			rv = ERR_INV_FLD;
			break;
		}

		cCurPtr = xmlStr;

		if(strchr(cCurPtr, '.') != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Contains . in the date format", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);//Praveen_p1: For testing purpose, remove it later

			//Format : YYYY.MM.DD

			/* Validate the year */
			cNxtPtr = strchr(xmlStr, '.');
			if((cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 4))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid year length", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
			year = atoi(szTmp);

			/* Validate the month */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, '.');
			if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid month length", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

			month = atoi(szTmp);
			if( (month <= 0) || (month > 12))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid month value %d", __FUNCTION__,
																			month);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			/* Validate the day */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, '.');
			if(cNxtPtr != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid date format [%s]",
															__FUNCTION__, xmlStr);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cNxtPtr = strchr(cCurPtr, '\0');
			if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid day len", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

			day = atoi(szTmp);
			switch(month)
			{
			case 1: /* JAN - 31 days */
			case 3: /* MAR - 31 days */
			case 5: /* MAY - 31 days */
			case 7:	/* JUL - 31 days */
			case 8: /* AUG - 31 days */
			case 10:/* OCT - 31 days */
			case 12:/* DEC - 31 days */

				maxDayVal = 31;
				break;

			case 4: /* APR - 30 days */
			case 6: /* JUN - 30 days */
			case 9:	/* SEP - 30 days */
			case 11:/* NOV - 30 days */

				maxDayVal = 30;
				break;

			case 2: /* FEB - 28/29 days */
				if( ((year % 4 == 0) && (year % 100 != 0)) ||
								((year % 100 == 0) && (year % 400 == 0)) )
				{
					maxDayVal = 29;
				}
				else
				{
					maxDayVal = 28;
				}

				break;
			}

			if(day < 1 || day > maxDayVal)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid day value %d", __FUNCTION__,
																			day);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
		}
		else if(strchr(cCurPtr, '/') != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Contains / in the date format", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);//Praveen_p1: For testing purpose, remove it later

			//Format : MM/DD/YYYY

			/* Validate the month */
			cNxtPtr = strchr(xmlStr, '/');
			if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid month length", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

			month = atoi(szTmp);
			if( (month <= 0) || (month > 12))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid month value %d", __FUNCTION__,
																			month);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			/* Validate the day */
			cCurPtr = cNxtPtr + 1;

			cNxtPtr = strchr(cCurPtr, '/');
			if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid day len", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

			day = atoi(szTmp);
			switch(month)
			{
			case 1: /* JAN - 31 days */
			case 3: /* MAR - 31 days */
			case 5: /* MAY - 31 days */
			case 7:	/* JUL - 31 days */
			case 8: /* AUG - 31 days */
			case 10:/* OCT - 31 days */
			case 12:/* DEC - 31 days */

				maxDayVal = 31;
				break;

			case 4: /* APR - 30 days */
			case 6: /* JUN - 30 days */
			case 9:	/* SEP - 30 days */
			case 11:/* NOV - 30 days */

				maxDayVal = 30;
				break;

			case 2: /* FEB - 28/29 days */
				if( ((year % 4 == 0) && (year % 100 != 0)) ||
								((year % 100 == 0) && (year % 400 == 0)) )
				{
					maxDayVal = 29;
				}
				else
				{
					maxDayVal = 28;
				}

				break;
			}

			if(day < 1 || day > maxDayVal)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid day value %d", __FUNCTION__,
																			day);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			/* Validate the year */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, '/');
			if(cNxtPtr != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid date format [%s]",
															__FUNCTION__, xmlStr);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cNxtPtr = strchr(cCurPtr, '\0');
			if((cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 4))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid year length", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
			year = atoi(szTmp);
		}

		/* All the fields of the date are valid */
		cCurPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cCurPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cCurPtr, 0x00, sizeof(char) * (iLen + 1));
		memcpy(cCurPtr, xmlStr, iLen);
		*szDatePtr = cCurPtr;

		break;
	}

	if(rv == ERR_INV_FLD)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid date value [%s]", __FUNCTION__,
																		xmlStr);
		RCHI_LIB_TRACE(szDbgMsg);
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectTimeVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectTimeVal(char * xmlStr, char ** szTimePtr)
{
	int		rv				= SUCCESS;
	int		hour			= 0;
	int		min				= 0;
	int		sec				= 0;
	int		iLen			= 0;
	char	szTmp[3]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		iLen = strlen(xmlStr);

		/* hh:mm:ss -> 8 characters */
		if( (iLen != 8) || (strspn(xmlStr, "0123456789:") != iLen) )
		{
			rv = ERR_INV_FLD;
			break;
		}

		/* Validate the hour */
		cCurPtr = xmlStr;
		cNxtPtr = strchr(xmlStr, ':');
		if((cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid hour len", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}
		memset(szTmp, 0x00, sizeof(szTmp));
		memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

		hour = atoi(szTmp);
		if( (hour < 0) || (hour >= 24))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid hour value", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		/* Validate the minute */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, ':');
		if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid minute len", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}
		memset(szTmp, 0x00, sizeof(szTmp));
		memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

		min = atoi(szTmp);
		if( (min < 0) || (min >= 60) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid minute value", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		/* Validate the second */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, ':');
		if(cNxtPtr != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid time format [%s]",
														__FUNCTION__, xmlStr);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		cNxtPtr = strchr(cCurPtr, '\0');
		if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid second len", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}
		memset(szTmp, 0x00, sizeof(szTmp));
		memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

		sec = atoi(szTmp);
		if( (sec < 0) || (sec >= 60) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid second value", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		/* All the fields of the time are valid */
		cCurPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cCurPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cCurPtr, 0x00, sizeof(char) * (iLen + 1));
		memcpy(cCurPtr, xmlStr, iLen);
		*szTimePtr = cCurPtr;

		break;
	}

	if(rv == ERR_INV_FLD)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid time value [%s]", __FUNCTION__,
																		xmlStr);
		RCHI_LIB_TRACE(szDbgMsg);
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectBoolVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectBoolVal(char * xmlStr, int ** iBoolValPtr)
{
	int		rv				= SUCCESS;
	int *	iBoolVal		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Value to be searched = [%s]",
														__FUNCTION__, xmlStr);
	RCHI_LIB_TRACE(szDbgMsg);

	if( (strcmp(xmlStr, "TRUE") == SUCCESS)	||
		(strcmp(xmlStr, "YES") == SUCCESS)	||
		(strcmp(xmlStr, "T") == SUCCESS)	||
		(strcmp(xmlStr, "1") == SUCCESS)	)
	{
		/* The Boolean value passed in the xml is a TRUE value */

		iBoolVal = (int *) malloc(sizeof(int));
		if(iBoolVal != NULL)
		{
			*iBoolVal = RCHI_TRUE;
			*iBoolValPtr = iBoolVal;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}
	else if( (strcmp(xmlStr, "FALSE") == SUCCESS)||
			 (strcmp(xmlStr, "NO") == SUCCESS)	 ||
			 (strcmp(xmlStr, "F") == SUCCESS)	 ||
			 (strcmp(xmlStr, "0") == SUCCESS)	 )
	{
		/* The Boolean value passed in the xml is a FALSE value */

		iBoolVal = (int *) malloc(sizeof(int));
		if(iBoolVal != NULL)
		{
			*iBoolVal = RCHI_FALSE;
			*iBoolValPtr = iBoolVal;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}
	else
	{
		/* The value passed in the string doesnt match with any valid BOOLEAN
		 * string */
		debug_sprintf(szDbgMsg, "%s: [%s] is an INVALID Boolean Value",
														__FUNCTION__, xmlStr);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_INV_FLD_VAL;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectStrFromList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectStrFromList(char ** szVal, LIST_INFO_PTYPE refListPtr,
																	int iVal)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	/* Search for the data in the constant list (meta information) */
	while(refListPtr[iCnt].iListVal != -1)
	{
		if(refListPtr[iCnt].iListVal == iVal)
		{
			*szVal = refListPtr[iCnt].szListVal;

			debug_sprintf(szDbgMsg, "%s: string value = [%s]", __FUNCTION__,
																		*szVal);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		iCnt++;
	}

	/* Value did not match with any of the list entries */
	if(refListPtr[iCnt].iListVal == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Value [%d] not present in the list",
														__FUNCTION__, iVal);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_INV_FLD_VAL;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectEnumValFromList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectEnumValFromList(char * xmlStr, LIST_INFO_PTYPE refListPtr,
																int ** value)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);

//	debug_sprintf(szDbgMsg, "%s: Value to be searched = [%s]", __FUNCTION__,
//																		xmlStr);
//	RCHI_LIB_TRACE(szDbgMsg);

	/* Search for the data in the constant list (meta information) */
	while(refListPtr[iCnt].iListVal != -1)
	{
		/* Match found with one of the list entries */
		if(strcmp(xmlStr, refListPtr[iCnt].szListVal) == SUCCESS)
		{
			int	*	tmpPtr	= NULL;

			tmpPtr = (int *) malloc(sizeof(int));
			if(tmpPtr != NULL)
			{
				*tmpPtr = refListPtr[iCnt].iListVal;
				*value = tmpPtr;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			break;
		}

		iCnt++;
	}

	/* Value did not match with any of the list entries */
	if(refListPtr[iCnt].iListVal == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Value [%s] not present in the list",
														__FUNCTION__, xmlStr);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = ERR_INV_FLD_VAL;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getParamValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void getParamValues(xmlNode * curXMLNode, GENKEYVAL_PTYPE * paramValList)
{
	xmlChar *			szParam		= NULL;
	xmlChar *			szValue		= NULL;
	GENKEYVAL_PTYPE		curNodePtr	= NULL;
	GENKEYVAL_PTYPE		tmpNodePtr	= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(curXMLNode != NULL)
	{
		if(curXMLNode->type == 3)
		{
			curXMLNode = curXMLNode->next;
			continue;
		}
		else if(strcmp((char *)curXMLNode->name, "Parameter") != SUCCESS)
		{
			curXMLNode = curXMLNode->next;
			continue;
		}

		szParam = xmlGetProp(curXMLNode, (const xmlChar *) "ParamName");
		if(szParam != NULL)
		{
			szValue = xmlGetProp(curXMLNode, (const xmlChar *) "Value");
			if(szValue != NULL)
			{
				tmpNodePtr = (GENKEYVAL_PTYPE) malloc(GENKEYVAL_SIZE);
				if(tmpNodePtr != NULL)
				{
					memset(tmpNodePtr, 0x00, GENKEYVAL_SIZE);
					tmpNodePtr->key = (char *) szParam;
					tmpNodePtr->value = (char *) szValue;
					tmpNodePtr->next = NULL;

					/* Add the node in the linked list */
					if(curNodePtr == NULL)
					{
						/* Adding the first node */
						curNodePtr = tmpNodePtr;
						*paramValList = curNodePtr;
					}
					else
					{
						/* Adding the nth node */
						curNodePtr->next = tmpNodePtr;
						curNodePtr = tmpNodePtr;
					}
				}
				else
				{
					xmlFree(szParam);
					xmlFree(szValue);
				}
			}
			else
			{
				xmlFree(szParam);
			}
		}

		curXMLNode = curXMLNode->next;
	}

	debug_sprintf(szDbgMsg, "%s: returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: parseXMLFileForConfigData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
int parseXMLFileForConfigData(char * szXMLFile, GENKEYVAL_PTYPE * paramValList)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	xmlDoc *	docPtr			= NULL;
	xmlNode *	rootPtr			= NULL;
	xmlNode *	curNode			= NULL;
	xmlChar *	szGid			= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if( (szXMLFile == NULL) || ((iLen = strlen(szXMLFile)) <= 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid data passed as paramter",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML file using the libxml API */
		docPtr = xmlParseFile(szXMLFile);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Find the root node */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}
		else if(strcmp((char *)rootPtr->name, "VeriCentreImportData")!= SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Incorrect xml document", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Find the node "Group" with GID=1 as attribute */
		curNode = findNodeInXML("Group", rootPtr);
		while(curNode != NULL)
		{
			szGid = xmlGetProp(curNode, (const xmlChar *)"GID");
			if(szGid != NULL)
			{
				if(strcmp((char *)szGid, "1") == SUCCESS)
				{
					xmlFree(szGid);
					break;
				}

				xmlFree(szGid);
			}

			curNode = curNode->next;
		}

		if(curNode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Group with gid=1 missing in xml",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the parameters and values from the file */
		getParamValues(curNode->xmlChildrenNode, paramValList);

		break;
	}

	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addDataNode(VAL_LST_PTYPE valLstPtr, int iNodeType,
										RCKEYVAL_PTYPE listPtr, int iElemCnt)
{
	int				rv				= SUCCESS;
	VAL_NODE_PTYPE	tmpNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	tmpNodePtr = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
	if(tmpNodePtr != NULL)
	{
		/* Initialize the allocated memory */
		memset(tmpNodePtr, 0x00, sizeof(VAL_NODE_STYPE));
		tmpNodePtr->elemCnt = iElemCnt;
		tmpNodePtr->elemList = listPtr;
		tmpNodePtr->type = iNodeType;

		/* Add the node to the list */
		if(valLstPtr->end == NULL)
		{
			/* Adding the first element */
			valLstPtr->start = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

			debug_sprintf(szDbgMsg, "%s: Adding first node", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Adding the nth element */
			valLstPtr->end->next = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

			debug_sprintf(szDbgMsg, "%s: Adding some node", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		/* Increment the node count */
		valLstPtr->valCnt += 1;	
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validateAmountField
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int validateAmountField(char * szFieldVal, AMT_DEF_PTYPE amtDefPtr)
{
	int		rv				= SUCCESS;
	int		iValLen			= 0;
	int		iTmpLen			= 0;
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		iValLen = strlen(szFieldVal);
		if(iValLen <= 0) {
			debug_sprintf(szDbgMsg, "%s: No value present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		if(amtDefPtr->isNegAmtAllwd == RCHI_TRUE)
		{
			/* Check for characters */
			if(strspn(szFieldVal, "1234567890.-") != iValLen)
			{
				debug_sprintf(szDbgMsg, "%s: Characters other than [0-9], [.] & [-] found",
																	__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			//Praveen_P1: Just checking if there is - in other positions other than first position
			if(strspn(&szFieldVal[1], "1234567890.") != (iValLen - 1))
			{
				debug_sprintf(szDbgMsg, "%s: Characters other than [0-9] & [.]found from second position onwards",
																	__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else
		{
			/* Check for characters */
			if(strspn(szFieldVal, "1234567890.") != iValLen)
			{
				debug_sprintf(szDbgMsg, "%s: Characters other than 0-9 & . found",
																	__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}			
		}
		cCurPtr = strchr(szFieldVal, '.');
		if(cCurPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Decimal character not present",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		cNxtPtr = strchr(cCurPtr + 1, '.');
		if(cNxtPtr != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Multiple instances of dec character",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Validate the characteristic xyz.00 */
		iTmpLen = cCurPtr - szFieldVal;
		if((iTmpLen < amtDefPtr->minManLen) || (iTmpLen > amtDefPtr->maxManLen))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid length [%d] before decimal",
														__FUNCTION__, iTmpLen);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Validate the mantissa 0.xy */
		iTmpLen = iValLen - iTmpLen - 1;
		if((iTmpLen < amtDefPtr->minDecLen) || (iTmpLen > amtDefPtr->maxDecLen))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid length [%d] after decimal",
														__FUNCTION__, iTmpLen);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLTranForDWReq
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildXMLTranForDWReq(xmlDocPtr docPtr, xmlNodePtr rootPtr, char* pszMsg, int iMsgLen)
{
	int				rv 				= SUCCESS;
	char			szServiceId[50]	= "";
	xmlNodePtr		elemNodePtr		= NULL;
	xmlNodePtr		textNodePtr		= NULL;
	xmlNodePtr		tempNodePtr		= NULL;
	xmlAttrPtr 		newAttr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	getServiceID(szServiceId);
	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "ServiceID", BAD_CAST szServiceId);

	/*Embedding the xml packet as a payload to the request packet*/
	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "Payload", NULL);
	newAttr 	= xmlNewProp (elemNodePtr, BAD_CAST "Encoding", BAD_CAST "cdata");

	textNodePtr = xmlNewCDataBlock(docPtr, (xmlChar*)pszMsg, iMsgLen);

	tempNodePtr = xmlAddChild(elemNodePtr, textNodePtr);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLReqForDW
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildXMLReqForDW(char** pszDestPtr, int* iSize, char* pszPayload, int iPayloadLen, int iType)
{
	int				rv				= SUCCESS;
	char			szServiceId[10]	= "";
	char			szClientTO[10]	= "";
	xmlDocPtr		docPtr			= NULL;
	xmlNodePtr		rootPtr			= NULL;
	xmlNodePtr		childNode		= NULL;
	xmlAttrPtr 		newAttr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	getClientTimeOut(szClientTO);

	docPtr = xmlNewDoc(BAD_CAST "1.0");
	if(docPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldn't create doc", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	/* Create the root node for the xml document */
	rootPtr = xmlNewNode(NULL, BAD_CAST "Request");
	if(rootPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	newAttr = xmlNewProp (rootPtr, BAD_CAST "Version", BAD_CAST "3");


	newAttr = xmlNewProp (rootPtr, BAD_CAST "ClientTimeout", BAD_CAST szClientTO);

	newAttr = xmlNewProp (rootPtr, BAD_CAST "xmlns", BAD_CAST "http://securetransport.dw/rcservice/xml");

	/* Set the root element for the doc */
	xmlDocSetRootElement(docPtr, rootPtr);

	childNode = xmlNewChild(rootPtr, NULL, BAD_CAST "ReqClientID", NULL);
	buildReqClientIdForDWReq(childNode, iType);

	getServiceID(szServiceId);

	switch(iType)
	{
	case REGISTRATION:
		childNode = xmlNewChild(rootPtr, NULL, BAD_CAST "Registration", NULL);

		childNode = xmlNewChild(childNode, NULL, BAD_CAST "ServiceID", BAD_CAST szServiceId);
		break;

	case ACTIVATION:
		childNode = xmlNewChild(rootPtr, NULL, BAD_CAST "Activation", NULL);

		childNode = xmlNewChild(childNode, NULL, BAD_CAST "ServiceID", BAD_CAST szServiceId);

		break;

	case TRANSACTION:
		childNode = xmlNewChild(rootPtr, NULL, BAD_CAST "Transaction", NULL);

		buildXMLTranForDWReq(docPtr, childNode, pszPayload, iPayloadLen);
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = SUCCESS;
	}

	/*Dumping the xml into a char buffer after tree is ready*/
	xmlDocDumpFormatMemory(docPtr, (xmlChar **)pszDestPtr, iSize, 1);

	/* Free the memory allocated for the xml document tree */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	if (rv == FAILURE)
	{
		rv = ERR_INV_XML_MSG;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLTranForDWReq
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildReqClientIdForDWReq(xmlNodePtr	rootPtr, int iType)
{
	int				rv 				= SUCCESS;
	char			szBuffer[50]	= "";
	xmlNodePtr		elemNodePtr		= NULL;

#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(iType != REGISTRATION)
	{
		getDatawireID(szBuffer);
		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "DID", BAD_CAST szBuffer);
	}

	getAppID(szBuffer);
	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "App", BAD_CAST szBuffer);

	getAuth(szBuffer);
	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "Auth", BAD_CAST szBuffer);

	getClientRefNum(szBuffer);
	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "ClientRef", BAD_CAST szBuffer);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLReqForFEXCO
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildXMLReqForFEXCO(char** pszDestPtr, int* iSize, RCTRANDTLS_PTYPE pstRCHIDtls)
{
	int				rv				= SUCCESS;
	xmlDocPtr		docPtr			= NULL;
	xmlNodePtr		rootPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	docPtr = xmlNewDoc(BAD_CAST "1.0");
	if(docPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldn't create doc", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	/* Create the root node for the xml document */
	rootPtr = xmlNewNode(NULL, BAD_CAST "CARDRATEREQUEST");
	if(rootPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	/* Set the root element for the doc */
	xmlDocSetRootElement(docPtr, rootPtr);

	buildReqCardRateForFEXCORequest(rootPtr, pstRCHIDtls);

	/*Dumping the xml into a char buffer after tree is ready*/
	xmlDocDumpFormatMemory(docPtr, (xmlChar **)pszDestPtr, iSize, 1);

	/* Free the memory allocated for the xml document tree */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	if (rv == FAILURE)
	{
		rv = ERR_INV_XML_MSG;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildReqCardRateForFEXCORequest
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildReqCardRateForFEXCORequest(xmlNodePtr	rootPtr, RCTRANDTLS_PTYPE pstRCHIDtls)
{
	int					rv 				= SUCCESS;
	char				szBuffer[50]	= "";
	xmlNodePtr		elemNodePtr		= NULL;


#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memcpy(szBuffer,pstRCHIDtls[RC_DCC_REF_NUM].elementValue, sizeof(szBuffer));

	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "REFERENCENUMBER", BAD_CAST szBuffer);

	memset(szBuffer, 0x00, sizeof(szBuffer));

	memcpy(szBuffer,pstRCHIDtls[RC_CARDID].elementValue, 6);
	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "CARDID", BAD_CAST szBuffer);

	getDCCMerchantId(szBuffer);
	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "MERCHANTID", BAD_CAST szBuffer);

	getDCCAcquirer(szBuffer);
	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "ACQUIRER", BAD_CAST szBuffer);

	memcpy(szBuffer,pstRCHIDtls[RC_BASE_AMOUNT].elementValue, sizeof(szBuffer));

	elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "BASEAMOUNT", BAD_CAST szBuffer);


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * End of file xmlUtils.c
 * ============================================================================
 */
