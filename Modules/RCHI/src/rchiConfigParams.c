#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <svc.h>
#include <sys/stat.h>
#include <errno.h>

#include "iniparser.h"
#include "RCHI/rchiHostCfg.h"
#include "RCHI/rchiCommon.h"
#include "RCHI/rchiCfg.h"
#include "RCHI/utils.h"
#include "RCHI/rcHash.h"
#include "RCHI/rchiGroups.h"
#include "DHI_App/appLog.h"
#include "DHI_App/tranDef.h"
#include "RCHI/xmlUtils.h"
#include "RCHI/rchiReqLists.inc"



#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)




#define HOST_TOTALS_PASSWORD	"./flash/hosttotalspasswd.txt"
#define DATAWIRE_ID_FILE		"./flash/datawireid.dat"

#define PASSWORD_LENGTH			6


static int 	iTotalGrpCount;


static RC_CFG_STYPE 			rcSettings;		/* Structure to store the config variables required for First Data */
static RCKEYVAL_PTYPE 			pstRCFullHostRequestLst = NULL;
static RCHI_PASSTHROUGH_STYPE 	stRCHIPTDtls;
static int* 					piDHIIndexforAmountConvertion = NULL;


static int	frameAuth();
static int	getRCValues();
static int	loadRCHIPTFields();
static int	getRCHostDefinition();
static int	loadHostTotalsPassword(char*);
static int	getCardRateRefNumFromFile();
static int	fillTheExistingReqLstForMeta();
static int	getRCParamsFromFile(RC_FILE_PARAMS_PTYPE );
static void	getRCParamsFromConfig(RC_FILE_PARAMS_PTYPE );

static int fillDHIKeyvalueStruct(KEYVAL_PTYPE *, int*, char*);
static int createMetaInfoforRCKeyvalue(dictionary * , RCKEYVAL_PTYPE);


static pthread_mutex_t gpSTANAcessMutex;

/*
 * ============================================================================
 * Function Name: loadRCHIConfigParams
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.usr1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int loadRCHIConfigParams()
{
	int						iLen 					= 0;
	int						rv 						= 0;
	int						iVal					= 0;
	int						iValLen					= 0;
	int						iAppLogEnabled			= isAppLogEnabled();
	char    				szVal[20]				= "";
	char					szTemp[128] 			= "";
	char 					szRCParams[512]			= "";
	char					szAppLogData[256]		= "";
	char					szAppLogDiag[256]		= "";
	FILE*					fptr					= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iLen = sizeof(szTemp);
	iValLen = sizeof(szVal);

	while(1)
	{
		if(pthread_mutex_init(&gpSTANAcessMutex, NULL))//Initialize the Last Tran Details File Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Stan access mutex!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			return FAILURE;
		}
		/* Setting Time zone*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_REG, TIME_ZONE, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s:Time zone found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = setenv("TZ", szTemp, 1);
			if(rv == -1)
			{
				debug_sprintf(szDbgMsg, "%s:Time zone not set",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Time zone not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		
		/* Getting STAN, Reference Number and Client Reference Number */

		memset(szTemp, 0x00, iLen);
		memset(szRCParams, 0x00, iLen);
		rv = getRCValues();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: RC Values not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/*Getting Ref Number */

		rv = getCardRateRefNumFromFile();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Card Rate Ref Num Not Found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting STAN */
#if 0
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, STAN, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting STAN accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting STAN",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rcSettings.iStan = atoi(szTemp);
			/*Check for value of STAN if it is less than or equal to 0 */
			if(rcSettings.iStan <= 0)
			{
				rcSettings.iStan = 1;
				sprintf(szTemp,"%06d", rcSettings.iStan);
				putEnvFile(SECTION_RCHI, STAN, szTemp);
			}
		}
		else
		{
			/* Set the default value to 1 */
			/* Value not found.. setting default */
			debug_sprintf(szDbgMsg, "%s: STAN not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rcSettings.iStan = 1;
			sprintf(szTemp,"%06d", rcSettings.iStan);
			putEnvFile(SECTION_RCHI, STAN, szTemp);
		}
#endif
		/* Getting TPPID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, TPP_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting TPPID accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting TPPID",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szTPPID, szTemp, sizeof(rcSettings.szTPPID) - 1);
		}
		else
		{
			/* Value not found */
			debug_sprintf(szDbgMsg, "%s: TPPID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Terminal ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, TERMINAL_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Terminal ID",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szTerminalID, szTemp, sizeof(rcSettings.szTerminalID) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Terminal ID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Merchant ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, MERCHANT_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Merchant ID",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szMerchantID, szTemp, sizeof(rcSettings.szMerchantID) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Merchant ID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}


		/* Getting Merchant ID  for Dynamic Currency Conversion*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, MERCHANT_ID_DCC, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting DCC Merchant ID",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szDCCMerchantID, szTemp, sizeof(rcSettings.szDCCMerchantID) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: DCC Merchant ID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Merchant ID  for Dynamic Currency Conversion*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, ACQUIRER, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting DCC Acquirer",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szDCCAcquirer, szTemp, sizeof(rcSettings.szDCCAcquirer) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: DCC Acquirer not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Merchant Category Code */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, MERCH_CAT_CODE, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Merchant Category Code",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szMerchCatCode, szTemp, sizeof(rcSettings.szMerchCatCode) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Merchant Category Code not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Reference Number */
#if 0
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, REFERENCE_NUM, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Reference Number accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting Ref. Number",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rcSettings.iRefNum = atoi(szTemp);
			/*Check for value of Client Ref. Number if it is less than or equal to 0 */

		}
		else
		{

			/* Set the default value to 1 */
			/* Value not found.. setting default */
			debug_sprintf(szDbgMsg, "%s: Ref. Number not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rcSettings.iRefNum = 1;
			sprintf(szTemp,"%12lu", rcSettings.iRefNum);
			putEnvFile(SECTION_RCHI, REFERENCE_NUM, szTemp);
		}
#endif

		/* ------------- Check for keep Alive settings ------------------- */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_HDT, KEEP_ALIVE, szTemp, iLen);
		if(rv > 0)
		{
			iVal = atoi(szTemp);
			rcSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
			rcSettings.iKeepAliveInt = 0;

			if((iVal >= KEEP_ALIVE_DISABLED) && (iVal <= KEEP_ALIVE_DEFINED_TIME))
			{
				rcSettings.iKeepAlive = iVal;
			}
			else
			{
				/* keep Alive parameter value not in range*/
				debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive parameter, Setting Default - Disabled", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
				memset(szVal, 0x00, iValLen);
				sprintf(szVal, "%d", KEEP_ALIVE_DISABLED);
				putEnvFile(SECTION_HDT, KEEP_ALIVE, szVal);
				rv = SUCCESS;
			}

			if(rcSettings.iKeepAlive == 2)
			{
				memset(szTemp, 0x00, iLen);
				rv = getEnvFile(SECTION_HDT, KEEP_ALIVE_INTERVAL, szTemp, iLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal >= 1) && (iVal <= 60))
					{
						rcSettings.iKeepAliveInt = iVal;
					}
					else
					{
						/* No value found for keep Alive Interval parameter */
						debug_sprintf(szDbgMsg,"%s: Value found for Keep Alive Interval parameter is not in range ;Setting to Default", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rcSettings.iKeepAliveInt = DFLT_KEEP_ALIVE_INTERVAL;
						memset(szVal, 0x00, iValLen);
						sprintf(szVal, "%d", DFLT_KEEP_ALIVE_INTERVAL);
						putEnvFile(SECTION_HDT, KEEP_ALIVE_INTERVAL, szVal);
					}
				}
				else
				{
					/* No value found for keep Alive Interval parameter */
					debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive Interval parameter, Setting to Default Value", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.iKeepAliveInt = DFLT_KEEP_ALIVE_INTERVAL;
					memset(szVal, 0x00, iValLen);
					sprintf(szVal, "%d", DFLT_KEEP_ALIVE_INTERVAL);
					putEnvFile(SECTION_HDT, KEEP_ALIVE_INTERVAL, szVal);
				}
			}
			rv = SUCCESS;
		}
		else
		{
			/* No value found for keep Alive parameter */
			debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive parameter, Setting to Default - Disabled", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rcSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
			memset(szVal, 0x00, iValLen);
			sprintf(szVal, "%d", KEEP_ALIVE_DISABLED);
			putEnvFile(SECTION_HDT, KEEP_ALIVE, szVal);
			rv = SUCCESS;
		}


		/* Getting Transaction Currency */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, TRANSACTION_CURRENCY, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Transaction Currency",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szTranCurrency, szTemp, sizeof(rcSettings.szTranCurrency) - 1);
		}
		else
		{
			/* Value not found.. Setting default value to: USD(840) */
			debug_sprintf(szDbgMsg, "%s: Transaction Currency not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strcpy(rcSettings.szTranCurrency, "840");
			putEnvFile(SECTION_RCHI, TRANSACTION_CURRENCY, rcSettings.szTranCurrency);
		}

		/* Getting Group ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, GROUP_ID, szTemp, iLen);

		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Group ID",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szGroupID, szTemp, sizeof(rcSettings.szGroupID) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Group ID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting POS ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, POS_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting POS ID",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szPOSID, szTemp, sizeof(rcSettings.szPOSID) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: POS ID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Additional Amount Currency */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, ADDTL_AMT_CURRENCY, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Additional Amount Currency",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szAddtlAmtCurrency, szTemp, sizeof(rcSettings.szAddtlAmtCurrency) - 1);
		}
		else
		{
			/* Value not found... Set default value to USD (840) */
			debug_sprintf(szDbgMsg, "%s: Additional Amount Currency not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strcpy(rcSettings.szAddtlAmtCurrency, "840");
			putEnvFile(SECTION_RCHI, ADDTL_AMT_CURRENCY, rcSettings.szAddtlAmtCurrency);
		}

		/* Getting Alternate Merchant ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, ALTERNATE_MERCH_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Alternate Merchant ID",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szAlternateMerchID, szTemp, sizeof(rcSettings.szAlternateMerchID) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Alternate Merchant ID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Token Type */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, TOKEN_TYPE, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Token Type",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szTokenType, szTemp, sizeof(rcSettings.szTokenType) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Token type not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Service ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, RC_SERVICE_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting RC Service ID",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szServiceID, szTemp, sizeof(rcSettings.szServiceID) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: RC Service ID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Getting Application ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, RC_APP_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting RC Application ID",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szAppID, szTemp, sizeof(rcSettings.szAppID) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: RC Application ID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#if 0
		/* Getting Client Reference Number */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, CLIENT_REF_NUM, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting STAN accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting Client Ref. Number",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rcSettings.iClientRefNum = atoi(szTemp);
			/*Check for value of Client Ref. Number if it is less than or equal to 0 */

		}
		else
		{

			/* Set the default value to 1 */
			/* Value not found.. setting default */
			debug_sprintf(szDbgMsg, "%s: Client Ref. Number not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rcSettings.iClientRefNum = 0;
			sprintf(szTemp,"%7lu", rcSettings.iClientRefNum);
			getClientRefNum(szTemp);
			putEnvFile(SECTION_RCHI, CLIENT_REF_NUM, szTemp);
		}
#endif
		/* Check if EmbossedNumCalRegd is enabled in the configuration file */
		rv = getEnvFile(SECTION_RCHI, EMBOSSED_NUM_CALC_REQD, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				rcSettings.bEmbossedNumCalReqd = RCHI_TRUE;

				debug_sprintf(szDbgMsg,"%s: Setting Embossed Card Num Calculation is REQUIRED",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				rcSettings.bEmbossedNumCalReqd = RCHI_FALSE;

				debug_sprintf(szDbgMsg,"%s: Setting Embossed Card Num Calculation is NOT REQUIRED",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
		else
		{
			rcSettings.bEmbossedNumCalReqd = RCHI_FALSE;

			debug_sprintf(szDbgMsg, "%s: Setting Embossed Card Num Calculation is NOT REQUIRED (default action)",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Check if stripMIDreqd is enabled in the configuration file */
		rv = getEnvFile(SECTION_RCHI, STRIP_MID_REQD, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				rcSettings.bStripMIDReqd = RCHI_TRUE;

				debug_sprintf(szDbgMsg,"%s: Setting Striping of MID is REQUIRED",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				rcSettings.bStripMIDReqd = RCHI_FALSE;

				debug_sprintf(szDbgMsg,"%s: Setting Striping of MID is NOT REQUIRED",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
		else
		{
			rcSettings.bStripMIDReqd = RCHI_FALSE;

			debug_sprintf(szDbgMsg, "%s: Setting Striping of MID is NOT REQUIRED (default action)",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Getting Auth */
		memset(szTemp, 0x00, iLen);
		/* Framing Auth from available Group ID, Merchant ID and Terminal ID config variables*/
		rv = frameAuth();
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Succesfully framed RC Auth",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else if(rv == FAILURE)
		{
			debug_sprintf(szDbgMsg, "%s: RC Auth framing failed",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		/* Getting Data Wire ID if it exists */
		memset(szTemp, 0x00, iLen);
		if(doesFileExist(DATAWIRE_ID_FILE) == SUCCESS)
		{
			fptr = fopen(DATAWIRE_ID_FILE, "r");
			if(fptr != NULL)
			{
				fscanf(fptr,"%s", szTemp);
				fclose(fptr);

				/* Value found*/
				debug_sprintf(szDbgMsg, "%s: Setting Data Wire ID %s",__FUNCTION__, szTemp);
				RCHI_LIB_TRACE(szDbgMsg);

				chmod(DATAWIRE_ID_FILE, 0666);
				strcpy(rcSettings.szDatawireID,szTemp);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: RC DataWire ID not found",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}

		/*Set DCC Enabled or Disabled Param */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, DCC_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				debug_sprintf(szDbgMsg, "%s: Setting dcc enabled as FALSE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.bDCCEnabled = RCHI_FALSE;
			}
			else if((*szTemp == 'Y') || (*szTemp == 'y'))
			{
				debug_sprintf(szDbgMsg, "%s: Setting dcc enabled as as TRUE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.bDCCEnabled = RCHI_TRUE;
			}
		}
		else
		{
			rcSettings.bDCCEnabled = RCHI_FALSE;
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: DCC Enabled Not Found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Get RC host urls and other communication parameters */
		rv = getRCHostDefinition();
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in getting the RC host definition",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Got RC host definition",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

		}
		/* By Default the Number of Hosts available will be 2. But incase
		 * both the URL's are the same, then we set iNumUniqueHosts = 1.
		 * During Posting the data, if both URL's are same then we will
		 * avoid unnecessarily posting to same URL Twice.
		 */
		rcSettings.iNumUniqueHosts = 2;
		if(rcSettings.hostDef[0].url && rcSettings.hostDef[1].url )
		{
			if(!strcmp(rcSettings.hostDef[0].url,rcSettings.hostDef[1].url))
			{
				debug_sprintf(szDbgMsg, "%s: Both URL's match %s , So 1 Unique Host",__FUNCTION__,rcSettings.hostDef[0].url);
				RCHI_LIB_TRACE(szDbgMsg);
				rcSettings.iNumUniqueHosts = 1;
			}
		}
		debug_sprintf(szDbgMsg, "%s: rc Num Hosts:%d",__FUNCTION__,rcSettings.iNumUniqueHosts);
		RCHI_LIB_TRACE(szDbgMsg);
		/* Load Host totals Password from text file */
		memset(szTemp, 0x00, iLen);
		rv = loadHostTotalsPassword(szTemp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in getting the host totals password",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Password found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			strcpy(rcSettings.szPassword, szTemp);
		}
#if 0
		/* Getting Key ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, KEY_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Key ID, szTemp[%s]",__FUNCTION__, szTemp);
			RCHI_LIB_TRACE(szDbgMsg);

			strcpy(rcSettings.szKeyID, "VAR11;TEST1");
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Key ID not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif
		/* Getting Domain */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, DOMAIN, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Domain, szTemp[%s]",__FUNCTION__, szTemp);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szDomin, szTemp, sizeof(rcSettings.szDomin) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Domain not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, BRAND, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Brand, szTemp[%s]",__FUNCTION__, szTemp);
			RCHI_LIB_TRACE(szDbgMsg);

			strncpy(rcSettings.szBrand, szTemp, sizeof(rcSettings.szBrand) - 1);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Brand not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Filling Key ID, The format is DOMAIN;BRAND*/
		memset(rcSettings.szKeyID, 0x00, sizeof(rcSettings.szKeyID));
		if((strlen(rcSettings.szDomin) > 0) && (strlen(rcSettings.szBrand) > 0))
		{
			sprintf(rcSettings.szKeyID, "%s;%s", rcSettings.szDomin, rcSettings.szBrand);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key ID not filled",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Checking for VSP Encryption */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, VSP_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				debug_sprintf(szDbgMsg, "%s: Setting VSP Enabled as FALSE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.vspEnabled = RCHI_FALSE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Setting VSP Enabled as TRUE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.vspEnabled = RCHI_TRUE;
			}

		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Value for VSP enabled not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Checking whether EMV is enabled */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, EMV_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				debug_sprintf(szDbgMsg, "%s: Setting EMV Enabled as FALSE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.emvEnabled = RCHI_FALSE;
			}
			else if((*szTemp == 'Y') || (*szTemp == 'y'))
			{
				debug_sprintf(szDbgMsg, "%s: Setting EMV Enabled as TRUE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.emvEnabled = RCHI_TRUE;
			}

		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Value for EMV enabled not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		/* Checking whether EMV is enabled */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, CTLS_EMV_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				debug_sprintf(szDbgMsg, "%s: Setting EMV Enabled as FALSE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.ctlsEmvEnabled = RCHI_FALSE;
			}
			else if((*szTemp == 'Y') || (*szTemp == 'y'))
			{
				debug_sprintf(szDbgMsg, "%s: Setting EMV Enabled as TRUE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.ctlsEmvEnabled = RCHI_TRUE;
			}

		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Value for EMV enabled not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Checking whether EMV is enabled */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, FORCE_PL_DEBIT_FLAG, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				debug_sprintf(szDbgMsg, "%s: Setting Force PL Debit Flag as FALSE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.forcePLDebit = RCHI_FALSE;
			}
			else if((*szTemp == 'Y') || (*szTemp == 'y'))
			{
				debug_sprintf(szDbgMsg, "%s: Setting Force PL debit flag as TRUE",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rcSettings.forcePLDebit = RCHI_TRUE;
			}

		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Value for Force PL debit not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/*Set the EMV Key Status Check Duration */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, EMV_KEY_DUR, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting EMV Key Check Duration Type",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rcSettings.emvkeydur = atoi(szTemp);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: EMV Key Dur not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		if(isDCCEnabledInDevice())
		{
			/* Checking if foreign bin card data file exists*/
			memset(szTemp, 0x00, iLen);
			rv = getEnvFile(SECTION_RCHI, FGN_CARD_BIN_DATA_FILE, szTemp, iLen);
			if(rv > 0)
			{
				if(doesFileExist(szTemp) != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: %s File Not Found",__FUNCTION__, FGN_CARD_BIN_DATA_FILE);
					RCHI_LIB_TRACE(szDbgMsg);

					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "DCC is enabled and %s File Not Found", szTemp);
						strcpy(szAppLogDiag, "Please Add the File");
						addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
					}

					rv = FAILURE;
					break;
				}
				strcpy(rcSettings.szFgnBinDataFile, szTemp);
			}
			else
			{
				strcpy(rcSettings.szFgnBinDataFile, "./flash/HFEX.DAT");
			}

			/* Checking if currency data file exists*/
			memset(szTemp, 0x00, iLen);
			rv = getEnvFile(SECTION_RCHI, CURRENCY_DATA_FILE, szTemp, iLen);
			if(rv > 0)
			{
				if(doesFileExist(szTemp) != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: %s File Not Found",__FUNCTION__, CURRENCY_DATA_FILE);
					RCHI_LIB_TRACE(szDbgMsg);

					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "DCC is enabled and %s File Not Found", szTemp);
						strcpy(szAppLogDiag, "Please Add the File");
						addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
					}

					rv = FAILURE;
					break;
				}
				strcpy(rcSettings.szCurrencyDataFile, szTemp);
			}
			else
			{
				strcpy(rcSettings.szCurrencyDataFile, "./flash/FCP.DAT.txt");
			}
		}
		/* Checking if RC Version exists*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_RCHI, RC_VERSION, szTemp, iLen);
		if(rv > 0)
		{
			strncpy(rcSettings.szRCVersion, szTemp, sizeof(rcSettings.szRCVersion) - 1);
			debug_sprintf(szDbgMsg, "%s: RC Scheme Version Found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: RC Scheme Version not Found. Halting the Application",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "RC Scheme Version not Found");
				sprintf(szAppLogDiag, "Please Give RC Scheme Version and restart the application");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				rv = FAILURE;
				break;
			}
		}

		/*Loading Pass through Details*/
		rv = loadRCHIPTFields();
		if(rv == FAILURE)
		{
			sprintf(szAppLogData, "Failed To Load RCHI Passthrough File");
			sprintf(szAppLogDiag, "Please Check The Format of RCHI Passthrough File");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			break;
		}
		else
		{
			if(rv == ERR_FILE_NOT_AVAILABLE || rv == ERR_NO_REQUEST_DATA)
			{
				sprintf(szAppLogData, "RCHI Pass Through File Is Not Present Or Group Section Is Not Present In The RCHI Pass Through File, Loading Existing Request Fields");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);

				rv = fillTheExistingReqLstForMeta();
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to Load Existing Request Details from The RequestList.inc",__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
			}
			else
			{
				sprintf(szAppLogData, "Successfully Loaded RCHI Passthrough Fields");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
			}
		}
		rv = SUCCESS;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]",__FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getFGNCardBinFileName
 *
 * Description	: This function will return the fgn card bin file path
 *
 *
 * Input Params	:
 *
 *
 * Output Params: None
 * ============================================================================
 */
char* getFGNCardBinFileName()
{
	return rcSettings.szFgnBinDataFile;
}

/*
 * ============================================================================
 * Function Name: getCurrencyDataFileName
 *
 * Description	: This function will return currency file path.
 *
 *
 * Input Params	:
 *
 *
 * Output Params: None
 * ============================================================================
 */
char* getCurrencyDataFileName()
{
	return rcSettings.szCurrencyDataFile;
}

/*
 * ============================================================================
 * Function Name: getRCVersion
 *
 * Description	: This function will return RCVersion.
 *
 *
 * Input Params	:
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getRCVersion(char* rcVersion)
{

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
//	char			szDbgMsg[7120]	= ""; //Remove this please
#endif

	debug_sprintf(szDbgMsg, "%s: ---  Entering---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	strcpy(rcVersion, rcSettings.szRCVersion);
}

/*
 * ============================================================================
 * Function Name: getEmvKeyStatdur
 *
 * Description	: This function will return the duration set for periodic check
 *
 *
 * Input Params	:
 *
 *
 * Output Params: Duration set for periodic emv key status check
 * ============================================================================
 */
int getEmvKeyStatdur()
{


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rcSettings.emvkeydur;
}

/*
 * ============================================================================
 * Function Name: getSTAN
 *
 * Description	: This function will generate the STAN number for the current
 * 				  transaction and updates a new STAN value into config.usr1
 *
 *
 * Input Params	: Current Stan Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getSTAN(char *szCurrentStan)
{
	char szUpdatedStan[10] = "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	acquireRCMutexLock(&gpSTANAcessMutex, "Get Stan");

	sprintf(szCurrentStan,"%06d", rcSettings.iStan);
	debug_sprintf(szDbgMsg,"%s: STAN value for current transaction is %s",__FUNCTION__, szCurrentStan);
	RCHI_LIB_TRACE(szDbgMsg);

	if(rcSettings.iStan == 999999)
	{
		rcSettings.iStan = 0;
	}

	/* Incrementing the STAN value and writing the Updated STAN value into config.usr1 */
	rcSettings.iStan++;
	sprintf(szUpdatedStan,"%06d", rcSettings.iStan);

	releaseRCMutexLock(&gpSTANAcessMutex, "Get Stan");

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getCurrentSTAN
 *
 * Description	: This function will generate the STAN number for the current
 * 				  transaction and updates a new STAN value into config.usr1
 *
 *
 * Input Params	: Current Stan Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getCurrentSTAN(char *szCurrentStan)
{

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	sprintf(szCurrentStan,"%06d", rcSettings.iStan);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: getRefNum
 *
 * Description	: This function will generate the Reference number for the current
 * 				  transaction and updates a new Reference value into config.usr1
 *
 *
 * Input Params	: Current Stan Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getRefNum(char *szCurrentRefNum)
{

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	sprintf(szCurrentRefNum,"%lu", ++rcSettings.iRefNum);

	debug_sprintf(szDbgMsg,"%s: Ref Num for current transaction is %s",__FUNCTION__, szCurrentRefNum);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getCurrentRefNum
 *
 * Description	: This function will generate the Reference number for the current
 * 				  transaction and updates a new Reference value into config.usr1
 *
 *
 * Input Params	: Current Stan Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getCurrentRefNum(char *szCurrentRefNum)
{

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	sprintf(szCurrentRefNum,"%lu", rcSettings.iRefNum);

	debug_sprintf(szDbgMsg,"%s: Ref Num for current transaction is %s...Returning",__FUNCTION__, szCurrentRefNum);
	RCHI_LIB_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getClientRefNum
 *
 * Description	: This function will generate the Client Reference number for the current
 * 				  transaction and updates a new Reference value into config.usr1
 *
 *
 * Input Params	: Current Stan Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getClientRefNum(char *szClientRefNum)
{

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	sprintf(szClientRefNum, "%07lu", ++rcSettings.iClientRefNum);
	strcat(szClientRefNum, "V");
	strcat(szClientRefNum, rcSettings.szTPPID);

	debug_sprintf(szDbgMsg,"%s: Client Ref Num for current transaction is %s\n",__FUNCTION__, szClientRefNum);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: getCurrentClientRefNum
 *
 * Description	: This function will generate the Client Reference number for the current
 * 				  transaction and updates a new Reference value into config.usr1
 *
 *
 * Input Params	: Current Stan Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getCurrentClientRefNum(char *szClientRefNum)
{

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	sprintf(szClientRefNum, "%07lu", rcSettings.iClientRefNum);

	debug_sprintf(szDbgMsg,"%s: Client Ref Num for current transaction is %s\n",__FUNCTION__, szClientRefNum);
	RCHI_LIB_TRACE(szDbgMsg);

}

/*
 *
 * ============================================================================
 * Function Name: getRefNumForCardRate
 *
 *
 * Description	: This function will fill ref num for card rate
 *
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void getRefNumForCardRate(char* szCurrentRefNum)
{
#ifdef DEBUG
	char			szDbgMsg[250]	= "";
#endif

	sprintf(szCurrentRefNum,"%032lu", ++rcSettings.iCardRateRefNum);

	debug_sprintf(szDbgMsg, "%s: --- Current Ref Number [%s]---", __FUNCTION__, szCurrentRefNum);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: isVSPEnabledInDevice
 *
 *
 * Description	: This function will check if VSP encryption is enabled or not
 *
 *
 * Input Params	:
 *
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */

RCHI_BOOL isVSPEnabledInDevice()
{
	return rcSettings.vspEnabled;
}
/*
 * ============================================================================
 * Function Name: isEMVEnabledInDevice
 *
 *
 * Description	: This function will check if EMV is enabled or not
 *
 *
 * Input Params	:
 *
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */

RCHI_BOOL isEMVEnabledInDevice()
{
	return rcSettings.emvEnabled;
}
/*
 * ============================================================================
 * Function Name: isEMVCTLSEnabledInDevice
 *
 *
 * Description	: This function will check if EMV is enabled or not
 *
 *
 * Input Params	:
 *
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */

RCHI_BOOL isEMVCTLSEnabledInDevice()
{
	return rcSettings.ctlsEmvEnabled;
}
/*
 * ============================================================================
 * Function Name: isForcePLDebitFlagSet
 *
 *
 * Description	: This function will check if we need to force the Pin Less debit flag for all Credit and Refund Transactions
 * in RCHI irrespective of whether this flag is recieved in the SSI Request.
 *
 *
 * Input Params	:
 *
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
RCHI_BOOL isForcePLDebitFlagSet()
{
	return rcSettings.forcePLDebit;
}
/*
 * ============================================================================
 * Function Name: isEmbossedCardNumCalcRequired
 *
 *
 * Description	: This function will check if VSP encryption is enabled or not
 *
 *
 * Input Params	:
 *
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */

RCHI_BOOL isEmbossedCardNumCalcRequired()
{
	return rcSettings.bEmbossedNumCalReqd;
}
/*
 * ============================================================================
 * Function Name: getTPPID
 *
 *
 * Description	: This function will fill the szTPPID buffer
 *
 *
 * Input Params	: TPPID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTPPID(char *szTPPID)
{
	strcpy(szTPPID, rcSettings.szTPPID);
}

/*
 * ============================================================================
 * Function Name: getTerminalID
 *
 *
 * Description	: This function will fill the szTermID buffer
 *
 *
 * Input Params	: TerminalID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTerminalID(char *szTerminalID)
{
	strcpy(szTerminalID, rcSettings.szTerminalID);
}

/*
 * ============================================================================
 * Function Name: getMerchantID
 *
 *
 * Description	: This function will fill the szMerchID buffer
 *
 *
 * Input Params	: MerchantID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getMerchantID(char *szMerchantID)
{
	strcpy(szMerchantID, rcSettings.szMerchantID);
}

/*
 * ============================================================================
 * Function Name: getMerchCatCode
 *
 *
 * Description	: This function will fill the szMerchCatCode buffer
 *
 *
 * Input Params	: MerchCatCode Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getMerchCatCode(char *szMerchCatCode)
{
	strcpy(szMerchCatCode, rcSettings.szMerchCatCode);
}

/*
 * ============================================================================
 * Function Name: getKeepAliveParameter
 *
 * Description	: This function returns the keep Alive parameter value
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getKeepAliveParameter()
{
	return rcSettings.iKeepAlive;
}

/*
 * ============================================================================
 * Function Name: getKeepAliveInterval
 *
 * Description	: This function returns the keep Alive parameter value
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getKeepAliveInterval()
{
	return rcSettings.iKeepAliveInt;
}

/*
 * ============================================================================
 * Function Name: getTranCurrency
 *
 *
 * Description	: This function will fill the szTranCurrency buffer
 *
 *
 * Input Params	: TranCurrency Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTranCurrency(char *szTranCurrency)
{
	strcpy(szTranCurrency, rcSettings.szTranCurrency);
}

/*
 * ============================================================================
 * Function Name: getGroupID
 *
 *
 * Description	: This function will fill the szGroupID buffer
 *
 *
 * Input Params	: GroupID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getGroupID(char *szGroupID)
{
	strcpy(szGroupID, rcSettings.szGroupID);
}

/*
 * ============================================================================
 * Function Name: getPOSID
 *
 *
 * Description	: This function will fill the szPOSID buffer
 *
 *
 * Input Params	: POSID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getPOSID(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDtls)
{
	int iFlag = RCHI_FALSE;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(strcmp(pstRCHIDtls[RC_PAYMENT_TYPE].elementValue, "Credit") == SUCCESS)
	{
		iFlag = RCHI_TRUE;
	}
	else if(strcmp(pstRCHIDtls[RC_PAYMENT_TYPE].elementValue, "Debit") == SUCCESS)
	{
		if(strcmp(pstRCHIDtls[RC_TRAN_TYPE].elementValue, "Authorization") == SUCCESS || strcmp(pstRCHIDtls[RC_TRAN_TYPE].elementValue, "Completion") == SUCCESS)
		{
			iFlag = RCHI_TRUE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: POS ID not required for the current transaction", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		iFlag = RCHI_FALSE;
	}

	/* Fill POS ID only for the transaction that requires POS ID field by checking the flag variable */
	if(iFlag == RCHI_TRUE)
	{
		if(pstDHIDtls[eLANE].elementValue != NULL)
		{

			strcpy(rcSettings.szPOSID, pstDHIDtls[eLANE].elementValue);
		}

		strcpy(pstRCHIDtls[RC_POS_ID].elementValue, rcSettings.szPOSID);

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, pstRCHIDtls[RC_POS_ID].elementValue );
	RCHI_LIB_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getAddtlAmtCurrency
 *
 *
 * Description	: This function will fill the szAddtlAmtCurrency buffer
 *
 *
 * Input Params	: AddtlAmtCurrency Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getAddtlAmtCurrency(char *szAddtlAmtCurrency)
{
	strcpy(szAddtlAmtCurrency, rcSettings.szAddtlAmtCurrency);
}

/*
 * ============================================================================
 * Function Name: getAlternateMerchID
 *
 *
 * Description	: This function will fill the szAlternateMerchID buffer
 *
 *
 * Input Params	: AlternateMerchID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getAlternateMerchID(char *szAlternateMerchID)
{
	strcpy(szAlternateMerchID, rcSettings.szAlternateMerchID);
}

/*
 * ============================================================================
 * Function Name: getServiceID
 *
 *
 * Description	: This function will fill the szServiceID buffer
 *
 *
 * Input Params	: ServiceID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getServiceID(char *szServiceID)
{
	strcpy(szServiceID, rcSettings.szServiceID);
}

/*
 * ============================================================================
 * Function Name: getClientTimeOut
 *
 *
 * Description	: This function will get the resp timeout
 *
 *
 * Input Params	: Client time out to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getClientTimeOut(char *szClientTO)
{
	HOSTDEF_STRUCT_TYPE primHostDtls;

	getRCHostDetails(PRIMARY_URL, &primHostDtls);

	if(primHostDtls.respTimeOut > 0)
	{
		sprintf(szClientTO, "%d", primHostDtls.respTimeOut);
	}
	else
	{
		strcpy(szClientTO, "40");
	}
}

/*
 * ============================================================================
 * Function Name: getAppID
 *
 *
 * Description	: This function will fill the szAppID buffer
 *
 *
 * Input Params	: AppID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getAppID(char *szAppID)
{
	strcpy(szAppID, rcSettings.szAppID);
}

/*
 * ============================================================================
 * Function Name: getDatawireID
 *
 *
 * Description	: This function will fill the szDatawireID buffer
 *
 *
 * Input Params	: DataWireID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getDatawireID(char *szDatawireID)
{
	strcpy(szDatawireID, rcSettings.szDatawireID);
}

/*
 * ============================================================================
 * Function Name: getDatawireID
 *
 *
 * Description	: This function will fill the szDatawireID buffer
 *
 *
 * Input Params	: DataWireID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void setDatawireID(char *szDatawireID)
{
	FILE*	fptr = NULL;

	/*Writing the data wire id into the file*/
	fptr = fopen(DATAWIRE_ID_FILE, "w");
	if(fptr != NULL)
	{
		fprintf(fptr, "%s", szDatawireID);
		chmod(DATAWIRE_ID_FILE, 0666);
		fclose(fptr);
	}

	strcpy(rcSettings.szDatawireID, szDatawireID);
}

/*
 * ============================================================================
 * Function Name: getAuth
 *
 *
 * Description	: This function will fill the szAuth buffer
 *
 *
 * Input Params	: Auth Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getAuth(char *szAuth)
{
	strcpy(szAuth, rcSettings.szAuth);
}

/*
 * ============================================================================
 * Function Name: getHostTotalsPassword
 *
 *
 * Description	: This function will fill the szDatawireID buffer
 *
 *
 * Input Params	: DataWireID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getHostTotalsPassword(char *szPassword)
{
	strcpy(szPassword, rcSettings.szPassword);
}


/*
 * ============================================================================
 * Function Name: getTermCatCode
 *
 * Description	: This function will fill the szTermCatCode Buffer
 *
 *
 * Input Params	: Terminal Category Code Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTermCatCode(char *szTermCatCode)
{
	strcpy(szTermCatCode, TERM_CAT_CODE);
}

/*
 * ============================================================================
 * Function Name: getTermLocationIndicator
 *
 *
 * Description	: This function will fill the szTermLocationInd
 *
 *
 * Input Params	: Terminal Location Indicator Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTermLocationIndicator(char *szTermLocationInd)
{
	strcpy(szTermLocationInd, TERM_LOCATION_IND);
}

/*
 * ============================================================================
 * Function Name: getCardCaptureCapablt
 *
 *
 * Description	: This function will fill up the szCardCaptureCapablt buffer
 *
 *
 * Input Params	: Card Capture Capability Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getCardCaptureCapablt(char *szCardCaptureCapablt)
{
	strcpy(szCardCaptureCapablt, CARD_CAPTURE_CAPABLT);
}

/*
 * ============================================================================
 * Function Name: getSecurityLvl
 *
 *
 * Description	: This function will fill up the szSecurityLvl buffer
 *
 *
 * Input Params	: Security Level Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getSecurityLvl(char *szSecurityLvl)
{
	strcpy(szSecurityLvl, SECURITY_LVL);
}

/*
 * ============================================================================
 * Function Name: getEncrptType
 *
 *
 * Description	: This function will fill up the szEncrptType buffer
 *
 *
 * Input Params	: Encryption Type Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getEncrptType(char *szEncrptType)
{
	strcpy(szEncrptType, ENCRPT_TYPE);
}

/*
 * ============================================================================
 * Function Name: getKeyID
 *
 *
 * Description	: This function will fill up szKeyID buffer
 *
 *
 * Input Params	: Key ID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getKeyID(char *szKeyID)
{
	strcpy(szKeyID, rcSettings.szKeyID);
}

/*
 * ============================================================================
 * Function Name: getTokenType
 *
 *
 * Description	: This function will fill up the szTokenType buffer
 *
 *
 * Input Params	: Token Type Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTokenType(char *szTokenType)
{
	strcpy(szTokenType, rcSettings.szTokenType);
}

/*
 * ============================================================================
 * Function Name: getDCCMerchantId
 *
 *
 * Description	: This function will fill up the getDCCMerchantId buffer
 *
 *
 * Input Params	: DCC Merchant ID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getDCCMerchantId(char *szDCCMerchantID)
{
	strcpy(szDCCMerchantID, rcSettings.szDCCMerchantID);
}

/*
 * ============================================================================
 * Function Name: getDCCAcquirer
 *
 *
 * Description	: This function will fill up the getDCCAcquirer buffer
 *
 *
 * Input Params	: DCC Acquirer Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getDCCAcquirer(char *getDCCAcquirer)
{
	strcpy(getDCCAcquirer, rcSettings.szDCCAcquirer);
}

/*
 * ============================================================================
 * Function Name: getHostTotalsPassword
 *
 * Description	: This function will fill up the szTokenType buffer
 *
 *
 * Input Params	: Password Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

static int loadHostTotalsPassword(char *szPassword)
{
	int		rv 			= SUCCESS;
	FILE	*fPassword 	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	fPassword = fopen(HOST_TOTALS_PASSWORD, "r");
	if(fPassword == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error: File not found",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		fread(szPassword, PASSWORD_LENGTH, 1, fPassword);

		if(strlen(szPassword) > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Password found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Password not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}
		fclose(fPassword);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameAuth
 *
 *
 * Description	: This function will frame the Auth field
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

static int frameAuth()
{
	int 	rv 				= SUCCESS;
	int		iMidIndex		= 0;
	int		iLength			= 0;
	int		iTidZeroCnt		= 0;
	char	szDWTermID[10]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif


	if( (strlen(rcSettings.szGroupID)) == 0 || (strlen(rcSettings.szMerchantID)) == 0 || (strlen(rcSettings.szTerminalID)) == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Auth framing failed: Group ID or Merchant ID or Terminal ID not found",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		/*
		 * Praveen_P1: First Data came back and said that
		 * stripping of MID is not required
		 * doing this based on the config parameter to ensure we support
		 * backward compability if FD changes stand later
		 * Keep a config parameter which by default is OFF
		 * If this config parameter is not present or OFF we have the new behavior [Which applies to 99% of the customers]
		 * The new behavior is do not strip the MID but pad the TID to make it 8 digits
		 * If the config parameter is present and ON stick with the current McD behavior i.e. striping of MID
		 */

		if(rcSettings.bStripMIDReqd)
		{
			/*
			 * KranthiK1: Added this code as Datawire requires the
			 * leading the zeros to be removed if they exist in
			 * the mid value
			 */
			iLength = strlen(rcSettings.szMerchantID);
			for(iMidIndex = 0; iMidIndex < iLength; iMidIndex++)
			{
				if(rcSettings.szMerchantID[iMidIndex] != '0')
				{
					break;
				}
			}
		}

		iTidZeroCnt = DATAWIRE_TID_LENGTH - strlen(rcSettings.szTerminalID);

		/*KranthiK1: Added this part as the Data wire expects the length
		 * of the tid to be 8 digits.
		 */
		memset(szDWTermID, '0', iTidZeroCnt);
		strcat(szDWTermID, rcSettings.szTerminalID);

		strcpy(rcSettings.szAuth, rcSettings.szGroupID);
		strcat(rcSettings.szAuth, rcSettings.szMerchantID + iMidIndex);
		strcat(rcSettings.szAuth,"|");
		strcat(rcSettings.szAuth, szDWTermID);
		rv = SUCCESS;
	}
	return rv;
}

/*
 * ============================================================================
 * Function Name: getRCHostDefinition
 *
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.usr1 file and store them in the static structure.
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getRCHostDefinition()
{
	int		rv				= 0;
	int		len				= 0;
	int		iCnt			= 0;
	int		jCnt			= 0;
	int		iVal			= 0;
	int		maxLen			= 0;
	char *	tempPtr			= NULL;
	char	szTemp[100]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (iCnt < 4))
	{
		switch(iCnt)
		{
		case PRIMARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_RCHI, PRIM_HOST_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Primary RC Host URL = [%s]",
						__FUNCTION__, szTemp);
				RCHI_LIB_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					rcSettings.hostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing primary URL */
					debug_sprintf(szDbgMsg,
							"%s - memory allocation failed for Primary RC URL",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Primary RC URL not found",
						__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				/* It is a failure case only if the datawireid already exists
				 * If the datawireid doesnt exist then the urls will be obtained from the
				 * registration response
				 */
				if(strlen(rcSettings.szDatawireID) > 0)
				{
					rv = FAILURE;
				}
			}

			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the RC host port number ---- */
				rv = getEnvFile(SECTION_RCHI, PRIM_HOST_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						rcSettings.hostDef[iCnt].portNum = iVal;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid prim rchi port [%d]"
								, __FUNCTION__, iVal);
						rcSettings.hostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Primary host port not found",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.hostDef[iCnt].portNum = 0;
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Connection Timeout ---- */
				rv = getEnvFile(SECTION_RCHI, PRIM_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					rcSettings.hostDef[iCnt].conTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Primary Conn TimeOut = [%d]",
							__FUNCTION__, iVal);
					RCHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
							"%s: Primary conn timeout not found, setting default",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.hostDef[iCnt].conTimeOut =
							DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_RCHI, PRIM_CONN_TO, szTemp);

				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_RCHI, PRIM_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					rcSettings.hostDef[iCnt].respTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Primary Resp TimeOut = [%d]",
							__FUNCTION__, iVal);
					RCHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
							"%s: Primary resp timeout not found, setting default",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.hostDef[iCnt].respTimeOut =
							DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_RCHI, PRIM_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		case SECONDARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_RCHI, SCND_HOST_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Secondary RC Host URL = [%s]",
						__FUNCTION__, szTemp);
				RCHI_LIB_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					rcSettings.hostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing secondary URL */
					debug_sprintf(szDbgMsg,
							"%s - memory allocation failed for Secondary PWC URL",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Secondary PWC URL not found",
						__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(strlen(rcSettings.szDatawireID) > 0)
				{
					rv = FAILURE;
				}
			}

			/* ---- Get Connection Timeout ---- */
			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the PWC host port number ---- */
				rv = getEnvFile(SECTION_RCHI, SCND_HOST_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						rcSettings.hostDef[iCnt].portNum = iVal;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid scnd RC port [%d]"
								, __FUNCTION__, iVal);
						rcSettings.hostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Secondary host port not found",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.hostDef[iCnt].portNum = 0;
				}
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				rv = getEnvFile(SECTION_RCHI, SCND_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					rcSettings.hostDef[iCnt].conTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Secondary Conn TimeOut = [%d]",
							__FUNCTION__, iVal);
					RCHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
							"%s - Secndary conn timeout not found, setting default",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.hostDef[iCnt].conTimeOut =
							DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_RCHI, SCND_CONN_TO, szTemp);
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_RCHI, SCND_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					rcSettings.hostDef[iCnt].respTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Secondary Resp TimeOut = [%d]",
							__FUNCTION__, iVal);
					RCHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
							"%s - Secndary resp timeout not found, setting default",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.hostDef[iCnt].respTimeOut =
							DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_RCHI, SCND_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		case SERVICEDISCOVERY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_RCHI, REG_HOST_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Service Discovery URL = [%s]",
						__FUNCTION__, szTemp);
				RCHI_LIB_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					rcSettings.hostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing secondary URL */
					debug_sprintf(szDbgMsg,
							"%s - memory allocation failed for Service Discovery URL",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Service Discovery URL not found",
						__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			/* ---- Get Connection Timeout ---- */
			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the PWC host port number ---- */
				rv = getEnvFile(SECTION_RCHI, REG_HOST_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						rcSettings.hostDef[iCnt].portNum = iVal;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid registration port [%d]"
								, __FUNCTION__, iVal);
						rcSettings.hostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Registration port not found",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.hostDef[iCnt].portNum = 0;
				}
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				rv = getEnvFile(SECTION_RCHI, REG_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					rcSettings.hostDef[iCnt].conTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Registration Conn TimeOut = [%d]",
							__FUNCTION__, iVal);
					RCHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
							"%s - Registration conn timeout not found, setting default",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.hostDef[iCnt].conTimeOut =
							DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_RCHI, SCND_CONN_TO, szTemp);
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_RCHI, SCND_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					rcSettings.hostDef[iCnt].respTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Registration Resp TimeOut = [%d]",
							__FUNCTION__, iVal);
					RCHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
							"%s - Registration resp timeout not found, setting default",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rcSettings.hostDef[iCnt].respTimeOut =
							DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_RCHI, SCND_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}
			break;

		case FEXCO_URL:
		//	if(isDCCEnabledInDevice())
		//	{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* Get Host URL */
				rv = getEnvFile(SECTION_RCHI, FEXCO_HOST_URL, szTemp, maxLen);
				if(rv > 0)
				{
					debug_sprintf(szDbgMsg, "%s: DCC - FEXCO URL = [%s]",
							__FUNCTION__, szTemp);
					RCHI_LIB_TRACE(szDbgMsg);

					/* NOTE: validation of url remaining */
					len = (rv + 1) * sizeof(char);
					tempPtr = (char *) malloc(len);
					if(tempPtr)
					{
						memset(tempPtr, 0x00, len);
						memcpy(tempPtr, szTemp, rv);
						rcSettings.hostDef[iCnt].url = tempPtr;
						tempPtr = NULL;
						len = 0;

						rv = SUCCESS;
					}
					else
					{
						/* Memory allocation failed for storing secondary URL */
						debug_sprintf(szDbgMsg,
								"%s - memory allocation failed for Dynamic Currency Conversion FEXCO URL",
								__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
					}
				}
				else
				{
					/* URL not found */
					debug_sprintf(szDbgMsg, "%s - DCC FEXCO URL not found",
							__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				/* ---- Get Connection Timeout ---- */
				if(rv == SUCCESS)
				{
					maxLen = sizeof(szTemp);
					memset(szTemp, 0x00, maxLen);

					/* ---- Get the FEXCO host port number ---- */
					rv = getEnvFile(SECTION_RCHI, FEXCO_HOST_PORT, szTemp, maxLen);
					if(rv > 0)
					{
						iVal = atoi(szTemp);
						if( (iVal > 0) && (iVal <= 65535) )
						{
							rcSettings.hostDef[iCnt].portNum = iVal;
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid FEXCO port [%d]"
									, __FUNCTION__, iVal);
							rcSettings.hostDef[iCnt].portNum = 0;
						}
					}
					else
					{
						/* Value not found */
						debug_sprintf(szDbgMsg, "%s:FEXCO port not found",
								__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rcSettings.hostDef[iCnt].portNum = 0;
					}
					maxLen = sizeof(szTemp);
					memset(szTemp, 0x00, maxLen);

					rv = getEnvFile(SECTION_RCHI, FEXCO_CONN_TO, szTemp, maxLen);
					if(rv > 0)
					{
						iVal = atoi(szTemp);
						rcSettings.hostDef[iCnt].conTimeOut = iVal;

						debug_sprintf(szDbgMsg, "%s: FEXCO Conn TimeOut = [%d]",
								__FUNCTION__, iVal);
						RCHI_LIB_TRACE(szDbgMsg);
					}
					else
					{
						/* Value not found */
						/* Set Default Value */
						debug_sprintf(szDbgMsg,
								"%s - FEXCO conn timeout not found, setting default",
								__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rcSettings.hostDef[iCnt].conTimeOut =
								DFLT_HOSTCONN_TIMEOUT;
						/* Set the value in config.usr1 */
						sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
						putEnvFile(SECTION_RCHI, FEXCO_CONN_TO, szTemp);
					}

					maxLen = sizeof(szTemp);
					memset(szTemp, 0x00, maxLen);

					/* ---- Get Response Timeout ---- */
					rv = getEnvFile(SECTION_RCHI, FEXCO_RESP_TO, szTemp, maxLen);
					if(rv > 0)
					{
						iVal = atoi(szTemp);
						rcSettings.hostDef[iCnt].respTimeOut = iVal;

						debug_sprintf(szDbgMsg, "%s: FEXCO Resp TimeOut = [%d]",
								__FUNCTION__, iVal);
						RCHI_LIB_TRACE(szDbgMsg);
					}
					else
					{
						/* Value not found */
						/* Set Default Value */
						debug_sprintf(szDbgMsg,
								"%s - FEXCO resp timeout not found, setting default",
								__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rcSettings.hostDef[iCnt].respTimeOut =
								DFLT_HOSTRESP_TIMEOUT;
						/* Set the value in config.usr1 */
						sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
						putEnvFile(SECTION_RCHI, FEXCO_RESP_TO, szTemp);
					}

					rv = SUCCESS;
				}
			//}
			break;

		default:
			break;
		}

		iCnt++;
	}

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: RC Host definitions load failed",
				__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		/* Free any allocated resources */
		for(jCnt = 0; jCnt <= iCnt; jCnt++)
		{
			free(rcSettings.hostDef[jCnt].url);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRCHostDetails
 *
 * Description	: This function fills up the RC Host Details for a connection to
 * 				  be setup
 *
 * Input Params	:	HOST_URL_ENUM
 * 					HOSTDEF_PTR_TYPE
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getRCHostDetails(HOST_URL_ENUM hostType, HOSTDEF_PTR_TYPE hostDefPtr)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(hostDefPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No placeholder provided for host details",
				__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		memset(hostDefPtr, 0x00, sizeof(HOSTDEF_STRUCT_TYPE));

		switch(hostType)
		{
		case PRIMARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting PRIMARY RC Host details",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(rcSettings.hostDef[PRIMARY_URL]), sizeof(HOSTDEF_STRUCT_TYPE));
			break;

		case SECONDARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting SECONDARY RC Host details",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(rcSettings.hostDef[SECONDARY_URL]), sizeof(HOSTDEF_STRUCT_TYPE));

			break;

		case SERVICEDISCOVERY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting Data Wire Registration Host details",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(rcSettings.hostDef[SERVICEDISCOVERY_URL]), sizeof(HOSTDEF_STRUCT_TYPE));

			break;

		case REGISTRATION_URL:
			debug_sprintf(szDbgMsg, "%s: Getting Data Wire Registration Host details",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(rcSettings.hostDef[REGISTRATION_URL]), sizeof(HOSTDEF_STRUCT_TYPE));

			break;

		case FEXCO_URL:
			debug_sprintf(szDbgMsg, "%s: Getting DCC FEXCO Host details",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(rcSettings.hostDef[FEXCO_URL]), sizeof(HOSTDEF_STRUCT_TYPE));


			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Incorrect Host Type given",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: setRCHIHostUrl
 *
 * Description	: This function sets the newly obtained url from the Data Wire Host
 *
 * Input Params	:	HOST_URL_ENUM
 * 					HOSTDEF_PTR_TYPE
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setRCHIHostUrl(HOST_URL_ENUM hostType, char* pszUrl)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pszUrl == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No placeholder provided for host details",
				__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		switch(hostType)
		{
		case PRIMARY_URL:
			debug_sprintf(szDbgMsg, "%s: Setting PRIMARY RC Host details",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			
			if(rcSettings.hostDef[PRIMARY_URL].url != NULL)
			{
				free(rcSettings.hostDef[PRIMARY_URL].url);
				rcSettings.hostDef[PRIMARY_URL].url = NULL;
			}

			rcSettings.hostDef[PRIMARY_URL].url = strdup(pszUrl);

			putEnvFile(SECTION_RCHI, PRIM_HOST_URL, pszUrl);

			break;

		case SECONDARY_URL:
			debug_sprintf(szDbgMsg, "%s: Setting SECONDARY RC Host details",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(rcSettings.hostDef[SECONDARY_URL].url != NULL)
			{
				free(rcSettings.hostDef[SECONDARY_URL].url);
				rcSettings.hostDef[SECONDARY_URL].url = NULL;
			}

			rcSettings.hostDef[SECONDARY_URL].url = strdup(pszUrl);

			putEnvFile(SECTION_RCHI, SCND_HOST_URL, pszUrl);
		
			break;

		case REGISTRATION_URL:
			debug_sprintf(szDbgMsg, "%s: Setting Registration DW Host details",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			/* Not putting into the config file so that the discovery url 
			*  still remains the same without any change
			*/
			if(rcSettings.hostDef[REGISTRATION_URL].url != NULL)
			{
				free(rcSettings.hostDef[REGISTRATION_URL].url);
				rcSettings.hostDef[REGISTRATION_URL].url = NULL;
			}

			rcSettings.hostDef[REGISTRATION_URL].url = strdup(pszUrl);

			rcSettings.hostDef[REGISTRATION_URL].conTimeOut = DFLT_HOSTCONN_TIMEOUT;

			rcSettings.hostDef[REGISTRATION_URL].respTimeOut = DFLT_HOSTRESP_TIMEOUT;

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Incorrect Host Type given",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: resetBatchParams
 *
 * Input Params	:
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void resetBatchParams()
{
	FILE*		fptr	= NULL;
	int 		rv		= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	rcSettings.iRefNum			= 0;
	rcSettings.iClientRefNum	= 0;
	rcSettings.iCardRateRefNum	= 0;

	/* Updating config variables value in config.usr1 */
	rv = updateConfigParams();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	if(doesFileExist(TRAN_RECORDS) == SUCCESS)
	{
		fptr = fopen(TRAN_RECORDS, "w");
		if(fptr != NULL)
		{
			fclose(fptr);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: getNumHosts
 *
 *
 * Description	: This function will return number of unique hosts
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */

int getNumUniqueHosts()
{
	return rcSettings.iNumUniqueHosts;
}
/*
 * ============================================================================
 * Function Name: writeRCParamsToFile
 *
 *
 * Description	: This function will return number of unique hosts
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */

int writeRCParamsToFile(char *pszStnRefNumClntRefNum)
{
	int		rv 		= SUCCESS;
	FILE*	fptr	= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	fptr = fopen(RC_PARAMS_FILE, "w");
	if(fptr != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Writing into the file",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		/* Writing the parameters in the file in format: STAN|Ref.Num|ClientRefNum */
		fwrite(pszStnRefNumClntRefNum, strlen(pszStnRefNumClntRefNum) + 1, 1, fptr);
		fclose(fptr);
		rv = SUCCESS;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - fopen error; file [%s] could not be opened!",
				__FUNCTION__, RC_PARAMS_FILE);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getRCParamsFromFile
 *
 *
 * Description	: This function will return number of unique hosts
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */
int getRCParamsFromFile(RC_FILE_PARAMS_PTYPE pstFileParams)
{
	int						rv						= SUCCESS;
	char*					pszTempStan				= NULL;
	char*					pszTempRefNum			= NULL;
	char*					pszTempClientRefNum 	= NULL;
	char 					szRCParams[50]			= "";
	FILE*					fptr					= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	memset(szRCParams,0x00,sizeof(szRCParams));
	fptr = fopen(RC_PARAMS_FILE, "r");
	if(fptr != NULL)
	{
		fgets(szRCParams, sizeof(szRCParams), fptr);
		debug_sprintf(szDbgMsg, "%s: szRCParams is :%s",__FUNCTION__, szRCParams);
		RCHI_LIB_TRACE(szDbgMsg);
		fclose(fptr);

		pszTempStan = strtok(szRCParams,"|");
		pszTempRefNum = strtok(NULL,"|");
		pszTempClientRefNum = strtok(NULL,"|");

		/* Value found*/

		debug_sprintf(szDbgMsg, "%s: Checking for STAN, Ref. Num and Client Ref. Num",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		/* Check for the value of STAN */
		if(pszTempStan != NULL)
		{
			/* Value found ... Setting STAN accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting STAN",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			pstFileParams->iStan = atoi(pszTempStan);
			if(pstFileParams->iStan <= 0)
			{
				pstFileParams->iStan = 1;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: STAN not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Checking for Reference Number */

		if(pszTempRefNum != NULL)
		{
			/* Value found ... Setting Ref.Num accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting Reference Number",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			pstFileParams->iRefNum = atoi(pszTempRefNum);
			if(pstFileParams->iRefNum <= 0)
			{
				pstFileParams->iRefNum = 1;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Ref. Number not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Checking for Client Ref. Number */

		if(pszTempClientRefNum != NULL)
		{
			/* Value found ... Setting ClientRefNum accordingly */
			debug_sprintf(szDbgMsg, "%s: Setting Client Reference Number",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			pstFileParams->iClntRefNum = atoi(pszTempClientRefNum);
			if(pstFileParams->iClntRefNum <= 0)
			{
				pstFileParams->iClntRefNum = 1;
			}
		}
		else
		{
			/* Set the default value to 1 */
			/* Value not found.. setting default */
			debug_sprintf(szDbgMsg, "%s: Client Ref. Number not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - fopen error; file [%s] could not be opened!",
				__FUNCTION__, RC_PARAMS_FILE);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: isDCCEnabled
 *
 *
 * Description	: This function will check if DCC is enabled or not
 *
 *
 * Input Params	:
 *
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */

RCHI_BOOL isDCCEnabledInDevice()
{
	return rcSettings.bDCCEnabled;
}

#if 0
/*
 * ============================================================================
 * Function Name: getCardRateParamsFromFile
 *
 *
 * Description	: This function will return number of unique hosts
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */
int getCardRateParamsFromFile()
{
	int						rv						= SUCCESS;
	char*					pszTempRefNum			= NULL;
	char 					szFEXCOParams[50]		= "";
	FILE*					fptr					= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	memset(szFEXCOParams,0x00,sizeof(szFEXCOParams));
	fptr = fopen(FEXCO_PARAMS_FILE, "r");

	if(fptr != NULL)
	{
		fgets(szFEXCOParams, sizeof(szFEXCOParams), fptr);
		fclose(fptr);

		pszTempRefNum = strtok(szFEXCOParams,"\0");

		debug_sprintf(szDbgMsg, "%s: Checking for  Ref. Num [%s]",__FUNCTION__, pszTempRefNum);
		RCHI_LIB_TRACE(szDbgMsg);

		if(pszTempRefNum != NULL)
		{
			/* Value found ... Setting Ref.Num accordingly */

			rcSettings.iCardRateRefNum = atoi(pszTempRefNum);
			if(rcSettings.iCardRateRefNum <= 0)
			{
				rcSettings.iCardRateRefNum = 1;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Ref. Number of FEXCO is not found",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - fopen error; file [%s] could not be opened!",
				__FUNCTION__, FEXCO_PARAMS_FILE);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}

		debug_sprintf(szDbgMsg, "%s: returning", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: getRCParamsFromConfig
 *
 *
 * Description	: This function will return number of unique hosts
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */
void getRCParamsFromConfig(RC_FILE_PARAMS_PTYPE pstConfigParams)
{
	int 	rv 			= SUCCESS;
	int		iLen		= 0;
	char	szTemp[50] 	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	iLen = sizeof(szTemp);
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	/* Getting STAN */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_RCHI, STAN, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting STAN accordingly */
		debug_sprintf(szDbgMsg, "%s: Setting STAN",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		pstConfigParams->iStan = atoi(szTemp);
		/*Check for value of STAN if it is less than or equal to 0 */
		if(pstConfigParams->iStan <= 0)
		{
			pstConfigParams->iStan = 1;
		}
	}
	else
	{
		/* Set the default value to 1 */
		/* Value not found.. setting default */
		debug_sprintf(szDbgMsg, "%s: STAN not found",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		pstConfigParams->iStan = 1;
	}
	/* Getting Reference Number */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_RCHI, REFERENCE_NUM, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Reference Number accordingly */
		debug_sprintf(szDbgMsg, "%s: Setting Ref. Number",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		pstConfigParams->iRefNum = atoi(szTemp);
		if(pstConfigParams->iRefNum <= 0)
		{
			pstConfigParams->iRefNum = 1;
		}
	}
	else
	{

		/* Set the default value to 1 */
		/* Value not found.. setting default */
		debug_sprintf(szDbgMsg, "%s:Ref. Number not found",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		pstConfigParams->iRefNum = 1;
	}
	/* Getting Client Reference Number */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_RCHI, CLIENT_REF_NUM, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting STAN accordingly */
		debug_sprintf(szDbgMsg, "%s: Setting Client Ref. Number",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		pstConfigParams->iClntRefNum = atoi(szTemp);
		if(pstConfigParams->iClntRefNum <= 0)
		{
			pstConfigParams->iClntRefNum = 1;
		}
	}
	else
	{
		/* Set the default value to 1 */
		/* Value not found.. setting default */
		debug_sprintf(szDbgMsg, "%s: Client Ref. Number not found",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		pstConfigParams->iClntRefNum = 1;
	}

	debug_sprintf(szDbgMsg, "%s: Returning",__FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getRCValues
 *
 *
 * Description	: This function will return number of unique hosts
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */
int getRCValues()
{
	int 					rv 						= SUCCESS;
	char					szTemp[50] 				= "";
	char					szStnRefNumClntRef[512]	= "";
	RCHI_BOOL 				bConfigFlag				= RCHI_FALSE;
	RC_FILE_PARAMS_STYPE	stFileParams			= {0};
	RC_FILE_PARAMS_STYPE	stConfigParams			= {0};

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	/* Step 1: Getting values from the file rcparams.txt */
	rv = doesFileExist(RC_PARAMS_FILE);
	if(rv == SUCCESS)
	{
		rv = getRCParamsFromFile(&stFileParams);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully read from the file", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Unable to read from the file", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}
	/* Step 2: Getting values from the file config.usr1 */
	getRCParamsFromConfig(&stConfigParams);

	/* Step 3: Checking the source of values */

	/* If values are obtained both from config.usr1 and rcparams.txt, then we need to store the bigger value,
		 * so the following check is done
		 */
	if(stFileParams.iStan > stConfigParams.iStan)
	{
		rcSettings.iStan = stFileParams.iStan;
	}
	else
	{
		rcSettings.iStan = stConfigParams.iStan;
		bConfigFlag = RCHI_TRUE;
	}
	if(stFileParams.iRefNum > stConfigParams.iRefNum)
	{
		rcSettings.iRefNum = stFileParams.iRefNum;
	}
	else
	{
		rcSettings.iRefNum = stConfigParams.iRefNum;
		bConfigFlag = RCHI_TRUE;
	}
	if(stFileParams.iClntRefNum > stConfigParams.iClntRefNum)
	{
		rcSettings.iClientRefNum = stFileParams.iClntRefNum;
	}
	else
	{
		rcSettings.iClientRefNum = stConfigParams.iClntRefNum;
		bConfigFlag = RCHI_TRUE;
	}
	/* If we have obtained values through config.usr1 or the default values needs to be set
	 * then the data is written into the file
	 */
	if(bConfigFlag == RCHI_TRUE)
	{
		/* Setting Values */

		sprintf(szTemp,"%06d", rcSettings.iStan);
		strcpy(szStnRefNumClntRef, szTemp);
		/* Setting Ref. Num */
		sprintf(szTemp,"%12lu", rcSettings.iRefNum);
		strcat(szStnRefNumClntRef,"|");
		strcat(szStnRefNumClntRef, szTemp);
		/* Setting Client Ref. Num */
		sprintf(szTemp,"%07lu", rcSettings.iClientRefNum);
		strcat(szStnRefNumClntRef,"|");
		strcat(szStnRefNumClntRef, szTemp);

		//sprintf(szStnRefNumClntRef,"%06d|%12lu|%07lu",rcSettings.iStan,rcSettings.iRefNum,rcSettings.iClientRefNum);
		rv = writeRCParamsToFile(szStnRefNumClntRef);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully written",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s - Write unsuccessful in file [%s]!",
					__FUNCTION__, RC_PARAMS_FILE);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}
		/* Deleting in config.usr1 */
		memset(szTemp, 0x00, sizeof(szTemp));
		putEnvFile(SECTION_RCHI, STAN, szTemp);
		putEnvFile(SECTION_RCHI, REFERENCE_NUM, szTemp);
		putEnvFile(SECTION_RCHI, CLIENT_REF_NUM, szTemp);
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getCardRateRefNum
 *
 *
 * Description	: This function will return SUCCESS or FALIURE of FEXCO sparams File
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */
int getCardRateRefNumFromFile()
{
	int 					rv 						= SUCCESS;
	char					szCardRateRefNum[50]	= "";
	FILE*					fptr					= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(doesFileExist(CARDRATE_REFNUM_FILE) == SUCCESS)
	{
		fptr = fopen(CARDRATE_REFNUM_FILE, "r");
		if(fptr != NULL)
		{
			fgets(szCardRateRefNum, sizeof(szCardRateRefNum) - 1, fptr);
			fclose(fptr);

			rcSettings.iCardRateRefNum = atol(szCardRateRefNum);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s - fopen error; file [%s] could not be opened!", __FUNCTION__, CARDRATE_REFNUM_FILE);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCardRateRefNumToFile
 *
 *
 * Description	: This function updates the card rate refnum to the file
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */
int updateCardRateRefNumToFile()
{
	int		rv 		= SUCCESS;
	FILE*	fptr	= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	fptr = fopen(CARDRATE_REFNUM_FILE, "w");
	if(fptr != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Writing into the file",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		/* Writing the parameter in the file */
		fprintf(fptr,"%lu", rcSettings.iCardRateRefNum);
		fclose(fptr);
		fptr = NULL;
		rv = SUCCESS;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - fopen error; file [%s] could not be opened!",
				__FUNCTION__, CARDRATE_REFNUM_FILE);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadRCHIPTFields
 *
 * Description	: This function is used to Load RCHI pass Through Request and response Fields in Memory
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int loadRCHIPTFields()
{
	int							rv						= SUCCESS;
	int							iCnt					= 1;
	int							jCnt					= 0;
	int							kCnt					= 0;
	int							lCnt					= 0;
	int 						iGrpcnt					= 0;
	int 						iElementCount			= 0;
	int							iResTagCnt				= 0;
	int							iAddlAmtFlag			= 0;
	int							iLen					= 0;
	int							iTemp					= 0;
	int							iAppLogEnabled			= isAppLogEnabled();
	int							iPTIndex				= 0;
	int							iIndex					= -1;
	int 						iDHIAmtFormatchngindex  = 0;
	char						cPymtType				= 0;
	char						szKey[256]				= "";
	char						szPymntType[6+1]		= "";
	char						szSectionName[256]		= "";
	char						szSourceXmlName[256]	= "";
	char						szAppLogData[300]		= "";
	char						szAppLogDiag[300]		= "";
	char						szPassThroughFile[256]	= "";
	char*						cPtr					= NULL;
	char*						pszVal					= NULL;
	char*						pszValTemp				= NULL;
	char*						pszTemp					= NULL;
	dictionary*					dict					= NULL;
	RCHI_BOOL					bIndexFound				= RCHI_FALSE;
	RCKEYVAL_PTYPE				pstElementLst			= NULL;
	VARLST_INFO_PTYPE			pstListInfo				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(&stRCHIPTDtls, 0x00, sizeof(RCHI_PASSTHROUGH_STYPE));
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
	memset(szPassThroughFile, 0x00, sizeof(szPassThroughFile));

	while(1)
	{
		/* Check if file exist */
		rv = doesFileExist(RCHI_PASSTHROUGH_FILE_PATH1);
		if(rv == SUCCESS)
		{
			strcpy(szPassThroughFile,RCHI_PASSTHROUGH_FILE_PATH1);
			debug_sprintf(szDbgMsg, "%s: Loaded [%s] File", __FUNCTION__, szPassThroughFile);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			rv = doesFileExist(RCHI_PASSTHROUGH_FILE_PATH2);
			if(rv == SUCCESS)
			{
				strcpy(szPassThroughFile,RCHI_PASSTHROUGH_FILE_PATH2);
				debug_sprintf(szDbgMsg, "%s: Loaded [%s] File", __FUNCTION__, szPassThroughFile);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: RCHI Pass Through File does Not Exist", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "RCHI Pass Through INI File Does Not Exist.");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSED, szAppLogData, NULL);
				}
				rv = ERR_FILE_NOT_AVAILABLE;
				break;
			}
		}

		/* Load the ini file in the parser library */
		dict = iniparser_load(szPassThroughFile);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to parse ini file [%s], errno [%d]",__FUNCTION__, szPassThroughFile, errno);
			RCHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Failed To Load [%s] File, Error No [%d].", szPassThroughFile, errno);
				sprintf(szAppLogDiag, "Please Check The Format Of [%s] File.", szPassThroughFile);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Loaded [%s] successfully",__FUNCTION__, szPassThroughFile);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/*Loading Request Related Fields*/
		while(1)
		{
			/* Get the total number of tags present in REQUEST section */
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "RCHIGROUPS:count");
			pszVal = iniparser_getstring(dict, szKey, NULL);
			if(pszVal == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Count value is not present",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				iGrpcnt = 0;
			}
			else
			{
				iGrpcnt = atoi(pszVal);
			}

			iTotalGrpCount = iGrpcnt;
			debug_sprintf(szDbgMsg, "%s: Total [%d] RCHI Groups fields are configured for pass through", __FUNCTION__, iTotalGrpCount);
			RCHI_LIB_TRACE(szDbgMsg);

			iCnt = 1;
			if(iGrpcnt > 0)
			{
				/* Allocate memory for request tag list*/
				pstRCFullHostRequestLst = (RCKEYVAL_PTYPE)malloc(iGrpcnt * RCKEYVAL_SIZE);
				if(pstRCFullHostRequestLst == NULL)
				{
					debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}

				// Initialize the memory
				memset(pstRCFullHostRequestLst, 0x00, iGrpcnt * RCKEYVAL_SIZE);

				/*Loading Details of each Group*/
				while(iCnt <= iGrpcnt)
				{
					memset(szKey, 0x00, sizeof(szKey));
					sprintf(szKey, "RCHIGROUPS:tag%d", iCnt);
					pszVal = iniparser_getstring(dict, szKey, NULL);
					if(pszVal == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: [%s] is not present in the rchiPassThrgFields.INI",__FUNCTION__, szKey);
						RCHI_LIB_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
					debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s]",__FUNCTION__, szKey, pszVal);
					RCHI_LIB_TRACE(szDbgMsg);

					pszValTemp = strdup(pszVal);

					if((cPtr = strtok(pszValTemp, ",")) == NULL)			// GROUP NAME
					{
						rv = FAILURE;
						break;
					}

					iLen = strlen(cPtr);

					pstRCFullHostRequestLst[iCnt - 1].key = (char*) malloc(iLen + 1);
					if(pstRCFullHostRequestLst[iCnt - 1].key == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: malloc failed",__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}

					memset(pstRCFullHostRequestLst[iCnt - 1].key, 0x00, iLen + 1);
					strcpy(pstRCFullHostRequestLst[iCnt - 1].key, cPtr);

					pstRCFullHostRequestLst[iCnt - 1].iIndex = NO_ENUM;
					pstRCFullHostRequestLst[iCnt - 1].iSource= NO_ENUM;
					pstRCFullHostRequestLst[iCnt - 1].value  = NULL;
					pstRCFullHostRequestLst[iCnt - 1].isMand = RCHI_FALSE; // MANDATORY FLAG is not validated in case of Groups, so setting it to FLASE.


					/*T_POLISETTYG1: PAYMENT TYPE CALICULATION:
					* Currently RCHI Supports CREDIT,DEBIT,PREPAID,ADMIN,CHECK,EBT Payment types, Each Payment type corresponds to individual bit of a byte.
					* CREDIT - 0 bit, DEBIT  - 1 bit, PREPAID- 2 bit, ADMIN  - 3 bit, CHECK  - 4 bit, EBT    - 5 bit, 6 & 7th bits are reserved
					* Based on the Payment type the corresponding bit can be set, and calculate the value accordingly.
					*
					* From PassThrough File we will expect in the below order
					* CREDIT,DEBIT,PREPAID,ADMIN,CHECK,EBT, the value should be 6 bytes string, each byte indicate the one payment type.
					* If the byte is set then the group will go in the corresponding Payment type request
					*
					*/
					if((cPtr = strtok(NULL, ",")) == NULL)		//PAYMENT TYPE
					{
						rv = FAILURE;
						break;
					}

					if(strlen(cPtr) == 6)
					{
						strcpy(szPymntType, cPtr);

						for(jCnt = 0; jCnt < 6; jCnt++)
						{
							if(szPymntType[jCnt] == '1')
							{
								cPymtType = cPymtType | (1 << jCnt);
							}
						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid Payment type Value [%s] ",__FUNCTION__, cPtr);
						RCHI_LIB_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}

					if(cPymtType > 0 && cPymtType < 64)
					{
						pstRCFullHostRequestLst[iCnt - 1].cPaymentType = cPymtType;

						cPymtType = 0;//one we assign the payment type value to pstRCFullHostRequestLst Structure we need to reset the cPymtType value, so that it won't effect other groups
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid Payment type Value [%d] ",__FUNCTION__, cPymtType);
						RCHI_LIB_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}

					pstRCFullHostRequestLst[iCnt - 1].varlistSource = NULL;

					if((cPtr = strtok(NULL, ",")) == NULL)
					{
						// skipping if this value is not configured
					}
					else
					{
						/*If a group is configured for VARLIST, then the corresponding SSI VARLIST(from Root Node) should be inside square braces '[]',
						 * if the value is not present in '[]' then value will be ignored*/
						if(strncmp(cPtr, "[", 1) == SUCCESS)
						{
							cPtr = cPtr + 1;
							if((pszTemp = strchr(cPtr, ']')) != NULL)
							{
								iLen = pszTemp - cPtr;
								pstRCFullHostRequestLst[iCnt - 1].varlistSource = (char*) malloc(iLen + 1);
								memset(pstRCFullHostRequestLst[iCnt - 1].varlistSource, 0x00, iLen + 1);
								strncpy(pstRCFullHostRequestLst[iCnt - 1].varlistSource, cPtr, iLen);
							}
						}
					}
					pstRCFullHostRequestLst[iCnt - 1].valType = VARLIST;
					pstRCFullHostRequestLst[iCnt - 1].valMetaInfo = NULL;
					iCnt++;
					free(pszValTemp);
					pszValTemp = NULL;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: RCGROUP Section is Not present",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "RCHI Pass Through File Is Not Containing RCHI Request Related Fields, Loading Default Fields");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSED, szAppLogData, NULL);
				}
				rv = ERR_NO_REQUEST_DATA;
				break;

			}

			if(rv != SUCCESS)
			{
				// INI file is not in correct format
				debug_sprintf(szDbgMsg, "%s: [%s] Section is not in correct format",__FUNCTION__, SECTION_RCHIGROUPS);
				RCHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "[%s] Section Is Not In Correct Format.", SECTION_RCHIGROUPS);
					strcpy(szAppLogDiag, "Please Check The Format Of RCHI Pass Through INI File.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				break;
			}

			/*loading the elements in each group*/
			for(iCnt = 0; iCnt < iGrpcnt; iCnt++)
			{
				/*If the group is mapped with VARLIST, then mapping of RC and SSI elements will be done in createMetaInfoforRCKeyvalue function*/
				if(pstRCFullHostRequestLst[iCnt].valType == VARLIST && pstRCFullHostRequestLst[iCnt].varlistSource != NULL)
				{
					rv = createMetaInfoforRCKeyvalue(dict, &pstRCFullHostRequestLst[iCnt]);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to Map Varlist Elements with rc elements",__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
				}
				else
				{
					memset(szSectionName, 0x00, sizeof(szSectionName));
					strcpy(szSectionName, pstRCFullHostRequestLst[iCnt].key);

					memset(szKey, 0x00, sizeof(szKey));
					sprintf(szKey, "%s:count", szSectionName);
					pszVal = iniparser_getstring(dict, szKey, NULL);
					if(pszVal == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Count value is not present",__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						iElementCount = 0;
					}
					else
					{
						iElementCount = atoi(pszVal);
					}

					debug_sprintf(szDbgMsg, "%s: Total [%d] elements are configured under [%s]Group for pass through", __FUNCTION__, iElementCount, szSectionName);
					RCHI_LIB_TRACE(szDbgMsg);

					if(iElementCount > 0)
					{
						jCnt = 1;

						/* Allocate memory for request tag list*/
						pstElementLst = (RCKEYVAL_PTYPE)malloc(iElementCount * RCKEYVAL_SIZE);
						if(pstElementLst == NULL)
						{
							debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
							RCHI_LIB_TRACE(szDbgMsg);
							rv = FAILURE;
							break;
						}

						// Initialize the memory
						memset(pstElementLst, 0x00, iElementCount * RCKEYVAL_SIZE);

						pstListInfo = (VARLST_INFO_PTYPE)malloc(2* sizeof(VARLST_INFO_STYPE));
						memset(pstListInfo, 0x00, 2* sizeof(VARLST_INFO_STYPE));

						pstListInfo[1].nodeType = -1;
						pstListInfo[1].szNodeStr= NULL;
						pstListInfo[1].iElemCnt = 0;
						pstListInfo[1].keyList = NULL;

						pstListInfo[0].nodeType= 0;
						pstListInfo[0].szNodeStr= NULL;
						pstListInfo[0].iElemCnt = iElementCount;
						pstListInfo[0].keyList = pstElementLst;


						/*T_POLISETTYG1: In case of AddtlAmtGrp Group, in order to differentiate different types of AddtlAmtGrp, we will expecting the the name as
						 * AddtlAmtGrp_type(Type = As per FD Spec, the type of AddtlAmtGrp should be mentioned after _), and all names are case sensitive.
						 * if it is not falling under existing groups in RCHI Code, it will be treated as pass through group.
						 * After checking the type of AddtlAmtGrp, we will replace the group name from AddtlAmtGrp_type to AddtlAmtGrp, in order top send AddtlAmtGrp as a group name in RC Request.
						 * */
						if(!(strncmp(szSectionName, "AddtlAmtGrp", 11)))
						{

							if(strcmp(szSectionName, "AddtlAmtGrp_Tax") == SUCCESS)
							{
								iAddlAmtFlag = 0;
							}
							else if(strcmp(szSectionName, "AddtlAmtGrp_Cashback") == SUCCESS)
							{
								iAddlAmtFlag = 1;
							}
							else if(strcmp(szSectionName, "AddtlAmtGrp_FirstAuthAmt") == SUCCESS)
							{
								iAddlAmtFlag = 2;
							}
							else if(strcmp(szSectionName, "AddtlAmtGrp_TotalAuthAmt") == SUCCESS)
							{
								iAddlAmtFlag = 3;
							}
							else if(strcmp(szSectionName, "AddtlAmtGrp_Hltcare") == SUCCESS)
							{
								iAddlAmtFlag = 4;
							}
							else if(strcmp(szSectionName, "AddtlAmtGrp_RX") == SUCCESS)
							{
								iAddlAmtFlag = 5;
							}
							else if(strcmp(szSectionName, "AddtlAmtGrp_Vision") == SUCCESS)
							{
								iAddlAmtFlag = 6;
							}
							else if(strcmp(szSectionName, "AddtlAmtGrp_Dental") == SUCCESS)
							{
								iAddlAmtFlag = 7;
							}
							else if(strcmp(szSectionName, "AddtlAmtGrp_Clinical") == SUCCESS)
							{
								iAddlAmtFlag = 8;
							}
							else
							{
								iAddlAmtFlag = 9;
							}

							if(pstRCFullHostRequestLst[iCnt].key != NULL)
							{
								free(pstRCFullHostRequestLst[iCnt].key);
							}
							pstRCFullHostRequestLst[iCnt].key = strdup("AddtlAmtGrp");
						}

						while(jCnt <= iElementCount)
						{
							memset(szKey, 0x00, sizeof(szKey));
							sprintf(szKey, "%s:tag%d", szSectionName, jCnt);
							pszVal = iniparser_getstring(dict, szKey, NULL);
							if(pszVal == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Value not Found for [%s] in [%s] section",__FUNCTION__, szKey, szSectionName);
								RCHI_LIB_TRACE(szDbgMsg);
								rv = FAILURE;
								break;
							}
							debug_sprintf(szDbgMsg, "%s: Group [%s], Key[%s] Value[%s]",__FUNCTION__, pstRCFullHostRequestLst[iCnt].key, szKey, pszVal);
							RCHI_LIB_TRACE(szDbgMsg);

							pszValTemp = strdup(pszVal);

							if((cPtr = strtok(pszValTemp, ",")) == NULL)		// RCHI XML TAG NAME
							{
								rv = FAILURE;
								break;
							}

							iLen = strlen(cPtr);

							pstElementLst[jCnt - 1].key = (char*) malloc(iLen + 1);
							if(pstElementLst[jCnt - 1].key == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: malloc failed",__FUNCTION__);
								RCHI_LIB_TRACE(szDbgMsg);
								rv = FAILURE;
								break;
							}

							memset(pstElementLst[jCnt - 1].key, 0x00, iLen + 1);
							strcpy(pstElementLst[jCnt - 1].key, cPtr);


							pstElementLst[jCnt - 1].value  = NULL;

							if((cPtr = strtok(NULL, ",")) == NULL)		//MANDATORY_FLAG
							{
								rv = FAILURE;
								break;
							}

							if(! strcmp(cPtr, "TRUE"))
							{
								pstElementLst[jCnt - 1].isMand = RCHI_TRUE;
							}
							else
							{
								pstElementLst[jCnt - 1].isMand = RCHI_FALSE;
							}


							pstElementLst[jCnt - 1].iIndex 			= NO_ENUM;
							pstElementLst[jCnt - 1].iSource			= NO_ENUM;
							pstElementLst[jCnt - 1].valMetaInfo 	= NULL;
							pstElementLst[jCnt - 1].varlistSource 	= NULL;

							//Format in which the field has to be sent in RC Request 1(SINGLETON: Send the value in SSI Field as it is),3(AMOUNT Convert Amount from SSI(1.00 format to RC Format(100))
							if((cPtr = strtok(NULL, ",")) == NULL)
							{
								rv = FAILURE;
								break;
							}

							iTemp = atoi(cPtr);
							if(iTemp == AMOUNT)
							{
								pstElementLst[jCnt - 1].valType 		= AMOUNT;
							}
							else if(iTemp == VARLIST)
							{
								pstElementLst[jCnt - 1].valType 		= VARLIST;
							}
							else
							{
								pstElementLst[jCnt - 1].valType 		= SINGLETON;
							}

							if((cPtr = strtok(NULL, ",")) == NULL)
							{
								/*If this Field is not configured, then that field will be treated as internal*/
								//pstElementLst[jCnt - 1].valType 		= SINGLETON;
								memset(szSourceXmlName, 0x00, sizeof(szSourceXmlName));
								strcpy(szSourceXmlName, "NULL");
							}
							else
							{
								/*If an element is configured for VARLIST, then the corresponding SSI VARLIST(from Root Node) should be inside square braces '[]',
								 * if the value is not present in '[]' then value will be treated as SSI XML Name*/
								if(pstElementLst[jCnt - 1].valType == VARLIST)
								{
									if(strncmp(cPtr, "[", 1) == SUCCESS)
									{
										cPtr = cPtr + 1;
										if((pszTemp = strchr(cPtr, ']')) != NULL)
										{
											iLen = pszTemp - cPtr;
											pstElementLst[jCnt - 1].varlistSource = (char*) malloc(iLen + 1);
											memset(pstElementLst[jCnt - 1].varlistSource, 0x00, iLen + 1);
											strncpy(pstElementLst[jCnt - 1].varlistSource, cPtr, iLen);

											pstElementLst[jCnt - 1].valType = VARLIST;

											rv = createMetaInfoforRCKeyvalue(dict , &pstElementLst[jCnt - 1]);
											if(rv != SUCCESS)
											{
												debug_sprintf(szDbgMsg, "%s: Failed to Map Varlist Elements with rc elements",__FUNCTION__);
												RCHI_LIB_TRACE(szDbgMsg);
												rv = FAILURE;
												break;
											}
										}
									}
								}
								else if(pstElementLst[jCnt - 1].valType == SINGLETON || pstElementLst[jCnt - 1].valType == AMOUNT)
								{
									memset(szSourceXmlName, 0x00, sizeof(szSourceXmlName));
									strcpy(szSourceXmlName, cPtr);
									if(strcasecmp(szSourceXmlName, "NULL"))
									{
										pstElementLst[jCnt - 1].iSource = DHI;
									}
									//pstElementLst[jCnt - 1].valType 		= SINGLETON;
								}
							}

							if(pstElementLst[jCnt - 1].valType == SINGLETON || pstElementLst[jCnt - 1].valType == AMOUNT)
							{
								bIndexFound			= RCHI_FALSE;

								if((!(strcasecmp(szSourceXmlName, "NULL"))) && (iAddlAmtFlag < 9) )
								{
									//Taking Index value from the rchiReqLists.inc
									for(kCnt = 0; kCnt < sizeof(stRCHostRequestLst)/RCKEYVAL_SIZE; kCnt++)
									{
										if(!(strcmp(pstRCFullHostRequestLst[iCnt].key, stRCHostRequestLst[kCnt].key)))
										{
											for(lCnt = 0; lCnt < ((VARLST_INFO_PTYPE)stRCHostRequestLst[kCnt + iAddlAmtFlag].valMetaInfo)->iElemCnt; lCnt++)
											{
												if(!(strcmp(pstElementLst[jCnt - 1].key, ((VARLST_INFO_PTYPE)stRCHostRequestLst[kCnt + iAddlAmtFlag].valMetaInfo)->keyList[lCnt].key)))
												{
													pstElementLst[jCnt - 1].iSource = ((VARLST_INFO_PTYPE)stRCHostRequestLst[kCnt + iAddlAmtFlag].valMetaInfo)->keyList[lCnt].iSource;
													pstElementLst[jCnt - 1].iIndex = ((VARLST_INFO_PTYPE)stRCHostRequestLst[kCnt + iAddlAmtFlag].valMetaInfo)->keyList[lCnt].iIndex;
													bIndexFound			= RCHI_TRUE;
													break;
												}
											}
										}

										if(bIndexFound == RCHI_TRUE)
										{
											break;
										}
									}
								}
								else if(pstElementLst[jCnt - 1].iSource == DHI)
								{
									iIndex = getDHIIndex(szSourceXmlName);
									if(iIndex > 0)
									{
										pstElementLst[jCnt - 1].iIndex = iIndex;
										bIndexFound			= RCHI_TRUE;
									}
								}
								else
								{
									bIndexFound			= RCHI_FALSE;
								}

								if(bIndexFound == RCHI_FALSE)
								{
									debug_sprintf(szDbgMsg, "%s: No Index found for [%s] ",__FUNCTION__, pstElementLst[jCnt - 1].key);
									RCHI_LIB_TRACE(szDbgMsg);

									/* PRADEEP: In Case of no SSI Mapping found we will log it and proceed further,
									 * But in case of Field is configured as internal field and if we are not able find any mapping for it then application will be halted
									 * Configuring as internal means giving the SSI Value as NULL */
									if((pstElementLst[jCnt - 1].iSource == DHI) && (strcasecmp(szSourceXmlName, "NULL")))
									{
										if(iAppLogEnabled == 1)
										{
											sprintf(szAppLogData, "Unable to Map the SSI Field[%s] with RC Element[%s] ", szSourceXmlName, pstElementLst[jCnt - 1].key);
											addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
										}

										/*As No Mapping Found setting source as NO ENUM, so that while filling the Meta data this field will be ignored*/
										pstElementLst[jCnt - 1].iSource = NO_ENUM;
										pstElementLst[jCnt - 1].iIndex  = NO_ENUM;

									}
									else
									{
										if(iAppLogEnabled == 1)
										{
											sprintf(szAppLogData, "RC Element [%s] is defined as Internal, Unable to Map With Internal Field", pstElementLst[jCnt - 1].key);
											addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, NULL);
										}
										rv = FAILURE;
										break;
									}
								}
							}
							jCnt++;
							free(pszValTemp);
							pszValTemp = NULL;
						}

						iAddlAmtFlag = 0;
						pstRCFullHostRequestLst[iCnt].valMetaInfo = pstListInfo;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s:[%s]Group is defined, But no elements are configured under the same group ", __FUNCTION__, szSectionName);
						RCHI_LIB_TRACE(szDbgMsg);

						if(iAppLogEnabled == 1)
						{
							sprintf(szAppLogData, "[%s] Group Is Defined With No Elements Under the Group", szSectionName);
							strcpy(szAppLogDiag, "Please Check The Format Of RCHI Pass Through INI File.");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
						}

						rv = FAILURE;
					}

					if(rv != SUCCESS)
					{
						break;
					}
				}
			}
			break;
		}


		if((rv != SUCCESS) && (rv != ERR_NO_REQUEST_DATA))
		{
			break;
		}

		/*Loading Response Related Fields*/
		while(1)
		{
			/* Get the total number of tags present in RESPONSE section */
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "RCHIRESPONSE:count");
			pszVal = iniparser_getstring(dict, szKey, NULL);
			if(pszVal == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Count value is not present",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				iResTagCnt = 0;
			}
			else
			{
				iResTagCnt = atoi(pszVal);
			}

			stRCHIPTDtls.iRespCnt = iResTagCnt;
			debug_sprintf(szDbgMsg, "%s: Total [%d] RCHI Response fields are configured for pass through", __FUNCTION__, iResTagCnt);
			RCHI_LIB_TRACE(szDbgMsg);


			iCnt = 1;
			iPTIndex = RC_ELEMENTS_INDEX_MAX;
			if(iResTagCnt > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Starting Response section ",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				/* Allocate memory for request tag list*/
				stRCHIPTDtls.pstPTRespDtls = (RCHI_RESP_KEYVAL_PTYPE) malloc(iResTagCnt * sizeof(RCHI_RESP_KEYVAL_STYPE));
				if(stRCHIPTDtls.pstPTRespDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Failed",__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}

				// Initialize the memory
				memset(stRCHIPTDtls.pstPTRespDtls, 0x00, (iResTagCnt * sizeof(RCHI_RESP_KEYVAL_STYPE)));

				/* In Case of RCHI Amount has to be converted from RC Format(100) to SSI Format(1.00), in order to convert those fields after parsing of the RC Response
				 * we should know all the DHI Structure Index's where the value has to be converted from RC to SSI Format, in below array we will store all the index's
				 * where the value has to be converted. After Filling the DHI Index at the end we will fill -1, so that when looking for Index's we will stop once we find -1*/
				piDHIIndexforAmountConvertion = (int*)malloc((iResTagCnt + 1)*sizeof(int));
				if(piDHIIndexforAmountConvertion == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Failed",__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				memset(piDHIIndexforAmountConvertion, 0x00, (iResTagCnt+1)*sizeof(int));

				/* Get all the XML tag details from RCHI RESPONSE sections which needs to be passed along from Host Response to SSI */
				while(iCnt <= iResTagCnt)
				{
					memset(szKey, 0x00, sizeof(szKey));
					sprintf(szKey, "RCHIRESPONSE:tag%d", iCnt);
					pszVal = iniparser_getstring(dict, szKey, NULL);
					if(pszVal == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: End of RESPONSE Section",__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
					debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s]",__FUNCTION__, szKey, pszVal);
					RCHI_LIB_TRACE(szDbgMsg);

					pszValTemp = strdup(pszVal);

					if((cPtr = strtok(pszValTemp, ",")) == NULL)		//RCHI Xml Name
					{
						rv = FAILURE;
						break;
					}
					iLen = strlen(cPtr);
					stRCHIPTDtls.pstPTRespDtls[iCnt - 1].key = (char*) malloc(iLen + 1);
					if(stRCHIPTDtls.pstPTRespDtls[iCnt - 1].key == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Memory Allocation Failed",__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
					memset(stRCHIPTDtls.pstPTRespDtls[iCnt - 1].key, 0x00, iLen + 1);
					strcpy(stRCHIPTDtls.pstPTRespDtls[iCnt - 1].key, cPtr);


					if((cPtr = strtok(NULL, ",")) == NULL)		//Format of The RC XML Name(whether to convert amount format, or its singleton(same value from RC Response will be sent in SSI XML Name))
					{
						rv = FAILURE;
						break;
					}

					iTemp = atoi(cPtr); 		// used below

					if((cPtr = strtok(NULL, ",")) == NULL)		//SSI XML TAG
					{
						rv = FAILURE;
						break;
					}
					iLen = strlen(cPtr);
					stRCHIPTDtls.pstPTRespDtls[iCnt - 1].iDHIIndex= -1;
					stRCHIPTDtls.pstPTRespDtls[iCnt - 1].iRCIndex = -1;
					++iPTIndex;
					stRCHIPTDtls.pstPTRespDtls[iCnt - 1].iRCIndex  = iPTIndex;

					iIndex = getDHIIndex(cPtr);
					if(iIndex > 0)
					{
						stRCHIPTDtls.pstPTRespDtls[iCnt - 1].iDHIIndex = iIndex;
						debug_sprintf(szDbgMsg, "%s: DHI Index [%d] ",__FUNCTION__, iIndex);
						RCHI_LIB_TRACE(szDbgMsg);
						/*In Case if the Value is AMOUNT, means we have to convert the format, so storing DHI index for changing the format after parsing the RC Response*/
						if(iTemp == AMOUNT)
						{
							piDHIIndexforAmountConvertion[iDHIAmtFormatchngindex] = iIndex;
							++iDHIAmtFormatchngindex;
						}
					}
					else
					{
						if(iAppLogEnabled == 1)
						{
							sprintf(szAppLogData, "Unable to Map the SSI Field[%s] with RC Element[%s] ", cPtr, stRCHIPTDtls.pstPTRespDtls[iCnt - 1].key);
							addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
						}

						debug_sprintf(szDbgMsg, "%s: Couldn't find the DHI Index for[%s] ",__FUNCTION__, stRCHIPTDtls.pstPTRespDtls[iCnt - 1].key);
						RCHI_LIB_TRACE(szDbgMsg);
					}
					iCnt++;
					free(pszValTemp);
					pszValTemp = NULL;
				}

				/* If there are no Fields to convert we will free the memory as it is not required*/
				if(iDHIAmtFormatchngindex == 0)
				{
					free(piDHIIndexforAmountConvertion);
					piDHIIndexforAmountConvertion = NULL;
				}
				else
				{
					piDHIIndexforAmountConvertion[iDHIAmtFormatchngindex] = -1;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: RCHI RESPONSE Section is not present",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			if(rv == FAILURE)
			{
				// INI file is not in correct format
				debug_sprintf(szDbgMsg, "%s: RCHI Response Section is not in correct format",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed To Parse RCHI Pass Through Resposne Fields.");
					strcpy(szAppLogDiag, "Please Check The Format Of RCHI Pass Through INI File.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				break;
			}
			break;
		}
		break;
	}

	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createMetaInfoforRCKeyvalue
 *
 * Description	: This Function will created Meta Info for the RC Key passed as parameter and adds that to RC keyvalue structure.
 *
 * Input Params	: dictionary *, RCKEYVAL_PTYPE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int createMetaInfoforRCKeyvalue(dictionary * dict, RCKEYVAL_PTYPE pstRCKeyValue)
{
	int							rv					= SUCCESS;
	int							jCnt				= 0;
	int							iCnt				= 0;
	int							iLen				= 0;
	int							iTemp				= 0;
	int							iDHIElementCount	= 0;
	int							iElementCount		= 0;
	int							iAppLogEnabled		= isAppLogEnabled();
	char						szSectionName[256]	= "";
	char						szSSSIXMLName[256]	= "";
	char						szAppLogData[300]	= "";
	char						szAppLogDiag[300]	= "";
	char						szKey[256]			= "";
	char*						cPtr				= NULL;
	char*						pszVal	 			= NULL;
	char*						pszValTemp	 		= NULL;
	char*						pszTemp				= NULL;
	RCHI_BOOL					bIndexFound			= RCHI_FALSE;
	RCKEYVAL_PTYPE				pstElementLst		= NULL;
	VARLST_INFO_PTYPE			pstListInfo			= NULL;
	KEYVAL_PTYPE				pstDHIKeyValue		= NULL;

#ifdef DEBUG
	char	szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	if(pstRCKeyValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Null Param passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	/* Below function will fill the DHI Key value structure and the number of Key value structures based on which RC element index will be filled with DHI position
	 * so that while framing the request we can directly access the position of corresponding DHI key value structure*/
	rv = fillDHIKeyvalueStruct(&pstDHIKeyValue, &iDHIElementCount, pstRCKeyValue->varlistSource);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to Get DHI KeyValue", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	if(pstDHIKeyValue == NULL)
	{
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Unable to Map the SSI Varlist[%s] with RC Elements of [%s] ", pstRCKeyValue->varlistSource, pstRCKeyValue->key);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	memset(szSectionName, 0x00, sizeof(szSectionName));
	strcpy(szSectionName, pstRCKeyValue->key);

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s:count", szSectionName);
	pszVal = iniparser_getstring(dict, szKey, NULL);
	if(pszVal == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Count value is not present",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		iElementCount = 0;
	}
	else
	{
		iElementCount = atoi(pszVal);
	}

	debug_sprintf(szDbgMsg, "%s: Total [%d] elements are configured under [%s]Group for pass through", __FUNCTION__, iElementCount, szSectionName);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if(iElementCount > 0)
		{
			jCnt = 1;

			/* Allocate memory for request tag list*/
			pstElementLst = (RCKEYVAL_PTYPE)malloc(iElementCount * RCKEYVAL_SIZE);
			if(pstElementLst == NULL)
			{
				debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			// Initialize the memory
			memset(pstElementLst, 0x00, iElementCount * RCKEYVAL_SIZE);

			pstListInfo = (VARLST_INFO_PTYPE)malloc(2* sizeof(VARLST_INFO_STYPE));
			memset(pstListInfo, 0x00, 2* sizeof(VARLST_INFO_STYPE));

			pstListInfo[1].nodeType = -1;
			pstListInfo[1].szNodeStr= NULL;
			pstListInfo[1].iElemCnt = 0;
			pstListInfo[1].keyList = NULL;

			pstListInfo[0].nodeType= 0;
			pstListInfo[0].szNodeStr= NULL;
			pstListInfo[0].iElemCnt = iElementCount;
			pstListInfo[0].keyList = pstElementLst;


			/*T_POLISETTYG1: In case of AddtlAmtGrp Group, in order to differentiate different types of AddtlAmtGrp, we will expecting the the name as
			 * AddtlAmtGrp_type(Type = As per FD Spec, the type of AddtlAmtGrp should be mentioned after _), and all names are case sensitive.
			 * if it is not falling under existing groups in RCHI Code, it will be treated as pass through group.
			 * After checking the type of AddtlAmtGrp, we will replace the group name from AddtlAmtGrp_type to AddtlAmtGrp, in order top send AddtlAmtGrp as a group name in RC Request.
			 * */

			if(!(strncmp(szSectionName, "AddtlAmtGrp", 11)))
			{
				if(pstRCKeyValue->key != NULL)
				{
					free(pstRCKeyValue->key);
				}
				pstRCKeyValue->key = strdup("AddtlAmtGrp");
			}

			while(jCnt <= iElementCount)
			{
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:tag%d", szSectionName, jCnt);
				pszVal = iniparser_getstring(dict, szKey, NULL);
				if(pszVal == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Value not Found for [%s] in [%s] section",__FUNCTION__, szKey, szSectionName);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				debug_sprintf(szDbgMsg, "%s: Group [%s], Key[%s] Value[%s]",__FUNCTION__, pstRCKeyValue->key, szKey, pszVal);
				RCHI_LIB_TRACE(szDbgMsg);

				pszValTemp= strdup(pszVal);

				if((cPtr = strtok(pszValTemp, ",")) == NULL)		// RCHI XML TAG NAME
				{
					rv = FAILURE;
					break;
				}

				iLen = strlen(cPtr);

				pstElementLst[jCnt - 1].key = (char*) malloc(iLen + 1);
				if(pstElementLst[jCnt - 1].key == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: malloc failed",__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}

				memset(pstElementLst[jCnt - 1].key, 0x00, iLen + 1);
				strcpy(pstElementLst[jCnt - 1].key, cPtr);


				pstElementLst[jCnt - 1].value  = NULL;

				if((cPtr = strtok(NULL, ",")) == NULL)		//MANDATORY_FLAG
				{
					rv = FAILURE;
					break;
				}

				if(! strcmp(cPtr, "TRUE"))
				{
					pstElementLst[jCnt - 1].isMand = RCHI_TRUE;
				}
				else
				{
					pstElementLst[jCnt - 1].isMand = RCHI_FALSE;
				}

				//Format in which the field has to be sent in RC Request 1(SINGLETON: Send the value in SSI Field as it is),3(AMOUNT Convert Amount from SSI(1.00 format to RC Format(100))
				if((cPtr = strtok(NULL, ",")) == NULL)
				{
					rv = FAILURE;
					break;
				}

				iTemp = atoi(cPtr);
				if(iTemp == AMOUNT)
				{
					pstElementLst[jCnt - 1].valType 		= AMOUNT;
				}
				else if(iTemp == VARLIST)
				{
					pstElementLst[jCnt - 1].valType 		= VARLIST;
				}
				else
				{
					pstElementLst[jCnt - 1].valType 		= SINGLETON;
				}

				pstElementLst[jCnt - 1].varlistSource = NULL;

				if((cPtr = strtok(NULL, ",")) == NULL)
				{
					//pstElementLst[jCnt - 1].valType 		= SINGLETON;
					pstElementLst[jCnt - 1].iSource 		= NO_ENUM;
					pstElementLst[jCnt - 1].iIndex 			= NO_ENUM;
					pstElementLst[jCnt - 1].valMetaInfo 	= NULL;
					pstElementLst[jCnt - 1].varlistSource 	= NULL;
					memset(szSSSIXMLName, 0x00, sizeof(szSSSIXMLName));

					debug_sprintf(szDbgMsg, "%s: SSI XML Mapping is not Configured for Key :[%s]",__FUNCTION__, pstElementLst[jCnt - 1].key);
					RCHI_LIB_TRACE(szDbgMsg);

				}
				else
				{
					/*If an element is configured for VARLIST, then the corresponding SSI VARLIST(from Root Node) should be inside square braces '[]',
					 * if the value is not present in '[]' then value will be treated as SSI XML Name*/
					if(pstElementLst[jCnt - 1].valType == VARLIST)
					{
						if(strncmp(cPtr, "[", 1) == SUCCESS)
						{
							cPtr = cPtr + 1;
							if((pszTemp = strchr(cPtr, ']')) != NULL)
							{
								iLen = pszTemp - cPtr;
								pstElementLst[jCnt - 1].varlistSource = (char*) malloc(iLen + 1);
								memset(pstElementLst[jCnt - 1].varlistSource, 0x00, iLen + 1);
								strncpy(pstElementLst[jCnt - 1].varlistSource, cPtr, iLen);
								pstElementLst[jCnt - 1].valType = VARLIST;

								rv = createMetaInfoforRCKeyvalue(dict , &pstElementLst[jCnt - 1]);
								if(rv != SUCCESS)
								{
									debug_sprintf(szDbgMsg, "%s: Failed to Map Varlist Elements with rc elements",__FUNCTION__);
									RCHI_LIB_TRACE(szDbgMsg);
									rv = FAILURE;
									break;
								}

							}
						}
					}
					else if(pstElementLst[jCnt - 1].valType == SINGLETON || pstElementLst[jCnt - 1].valType == AMOUNT)
					{
					//	pstElementLst[jCnt - 1].valType 		= SINGLETON;
						pstElementLst[jCnt - 1].iSource 		= NO_ENUM;
						pstElementLst[jCnt - 1].iIndex 			= NO_ENUM;
						pstElementLst[jCnt - 1].valMetaInfo 	= NULL;
						pstElementLst[jCnt - 1].varlistSource 	= NULL;
						memset(szSSSIXMLName, 0x00, sizeof(szSSSIXMLName));
						strcpy(szSSSIXMLName, cPtr);

					}
				}

				/*Below we will assign DHI Key Position to the index of RC element, so that while filling the data for RC, the value will be fetched from the corresponding position of DHI Key value position
				 * as the DHI Key value positions will be fixed*/
				if(pstElementLst[jCnt - 1].valType == SINGLETON || pstElementLst[jCnt - 1].valType == AMOUNT)
				{
					bIndexFound = RCHI_FALSE;

					if(pstDHIKeyValue != NULL)
					{
						for(iCnt = 0; iCnt < iDHIElementCount; iCnt++)
						{
							if(strcmp(szSSSIXMLName, pstDHIKeyValue[iCnt].key) == SUCCESS)
							{
								pstElementLst[jCnt - 1].iSource = DHI;
								pstElementLst[jCnt - 1].iIndex =  iCnt;
								bIndexFound = RCHI_TRUE;

								debug_sprintf(szDbgMsg, "%s: Index [%d]",__FUNCTION__, pstElementLst[jCnt - 1].iIndex);
								RCHI_LIB_TRACE(szDbgMsg);
							}
						}
					}

					if(bIndexFound == RCHI_TRUE)
					{
						debug_sprintf(szDbgMsg, "%s: Found Mapping for SSI XML Name",__FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: No Mapping Found for Key :[%s]",__FUNCTION__, pstElementLst[jCnt - 1].key);
						RCHI_LIB_TRACE(szDbgMsg);
						if(iAppLogEnabled == 1)
						{
							sprintf(szAppLogData, "Unable to Map the SSI Field[%s] with RC Element[%s] ", szSSSIXMLName, pstElementLst[jCnt - 1].key);
							addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
						}
					}
				}

				jCnt++;

				free(pszValTemp);
				pszValTemp = NULL;
			}
			pstRCKeyValue->valMetaInfo = pstListInfo;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:[%s]Group is defined, But no elements are configured under the same group ", __FUNCTION__, szSectionName);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "[%s] Group Is Defined With No Elements Under the Group", szSectionName);
				strcpy(szAppLogDiag, "Please Check The Format Of RCHI Pass Through INI File.");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}
		break;

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillDHIKeyvaluestruct
 *
 * Description	: This function will fill DHI Key value structure and the number of elements of in the array of DHI structures based on the value sent in pszVarLstSource.
 *
 * Input Params	: KEYVAL_PTYPE - pointer to be filled with DHI structure
 * 				  int*    	   - pointer to be filled with number of elements of DHI Structure
 * 				  char*		   - buffer containing SSI VARLIST from root node
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillDHIKeyvalueStruct(KEYVAL_PTYPE *pstKeyValue, int* piCount, char* pszVarLstSource)
{
	int							rv					= SUCCESS;
	int							iCnt				= 0;
	int							kCnt				= 0;
	int							iKeyValueCnt		= 0;
	int							iDepth				= 0;
	char						szRootName[256]		= "";
	char* 						pszVal				= NULL;
	char*						pszTemp				= NULL;
	char*						cPtr				= NULL;
	RCHI_BOOL					bContinue			= RCHI_FALSE;
	RCHI_BOOL					bFound				= RCHI_FALSE;
	PTVARLST_PTYPE				pstDHIVarLst		= NULL;
	DHI_VARLST_INFO_PTYPE	    pstVarLstInfoLoc	= NULL;
	KEYVAL_PTYPE				pstTempKeyValue		= NULL;

#ifdef DEBUG
	char	szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szRootName, 0x00, sizeof(szRootName));

	if(pszVarLstSource == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Null Param passed", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	pstDHIVarLst = getDHIVarLstDtls();

	pszVal = strdup(pszVarLstSource);
	pszTemp = pszVal;


	debug_sprintf(szDbgMsg, "%s: VAR SOURCE[%s]",__FUNCTION__, pszTemp);
	RCHI_LIB_TRACE(szDbgMsg);

	iDepth = 1;
	while((pszTemp = strchr(pszTemp,':')) != NULL)
	{
		++iDepth;
		++pszTemp;
	}

	debug_sprintf(szDbgMsg, "%s: At Depth[%d]",__FUNCTION__, iDepth);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if((cPtr = strtok(pszVal, ":")) == NULL)
		{
			rv = FAILURE;
			break;
		}
		else
		{
			--iDepth;
			memset(szRootName, 0x00, sizeof(szRootName));
			strcpy(szRootName, cPtr);
		}

		iKeyValueCnt 	= pstDHIVarLst->iReqVARLSTCnt;
		pstTempKeyValue = pstDHIVarLst->pstVarlistReqDtls;

		while(iDepth >= 0)
		{
			bContinue			= RCHI_FALSE;

			for(iCnt = 0; iCnt < iKeyValueCnt; iCnt++)
			{
				if(strcmp(szRootName,pstTempKeyValue[iCnt].key) == SUCCESS)
				{
					pstVarLstInfoLoc = pstTempKeyValue[iCnt].valMetaInfo;

					if(iDepth > 0)
					{
						if((cPtr = strtok(NULL, ":")) == NULL)
						{
							rv = FAILURE;
							break;
						}
						else
						{
							--iDepth;
							memset(szRootName, 0x00, sizeof(szRootName));
							strcpy(szRootName, cPtr);
						}


						kCnt = 0;
						while(pstVarLstInfoLoc[kCnt].nodeType != -1)
						{
							if(pstVarLstInfoLoc[kCnt].szNodeStr != NULL)
							{
								if(strcmp(pstVarLstInfoLoc[kCnt].szNodeStr, szRootName) == SUCCESS)
								{
									if(iDepth > 0)
									{
										if((cPtr = strtok(NULL, ":")) == NULL)
										{
											rv = FAILURE;
											break;
										}
										else
										{
											--iDepth;
											memset(szRootName, 0x00, sizeof(szRootName));
											strcpy(szRootName, cPtr);
										}

										iKeyValueCnt 	= pstVarLstInfoLoc[kCnt].iElemCnt;
										pstTempKeyValue = pstVarLstInfoLoc[kCnt].keyList;
										bContinue = RCHI_TRUE;
										break;

									}
									else
									{
										*piCount	= pstVarLstInfoLoc[kCnt].iElemCnt;
										*pstKeyValue = pstVarLstInfoLoc[kCnt].keyList;
										bFound = RCHI_TRUE;
										break;
									}
									break;
								}
							}
							++kCnt;
						}
					}
					else
					{
						*piCount	= pstVarLstInfoLoc[0].iElemCnt;
						*pstKeyValue = pstVarLstInfoLoc[0].keyList;
						bFound = RCHI_TRUE;
						break;
					}
					break;
				}
			}

			if(bContinue == RCHI_TRUE && rv == SUCCESS)
			{
				continue;
			}
			else
			{
				break;
			}
		}

		if(bFound == RCHI_TRUE)
		{

			debug_sprintf(szDbgMsg, "%s: Mapping for [%s] is found", __FUNCTION__, pszVarLstSource);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Mapping for [%s] is not found", __FUNCTION__, pszVarLstSource);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		break;
	}

	if(pszVal != NULL)
	{
		free(pszVal);
		pszVal = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillTheExistingReqLstForMeta
 *
 * Description	: This Function will Copy All Request Related Fields into pstRCFullHostRequestLst
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillTheExistingReqLstForMeta()
{

	int							rv					= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	iTotalGrpCount = sizeof(stRCHostRequestLst) / RCKEYVAL_SIZE;


	pstRCFullHostRequestLst = (RCKEYVAL_PTYPE)malloc(iTotalGrpCount * RCKEYVAL_SIZE);
	if(pstRCFullHostRequestLst == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	memset(pstRCFullHostRequestLst, 0x00, iTotalGrpCount * RCKEYVAL_SIZE);

	memcpy(pstRCFullHostRequestLst, stRCHostRequestLst, iTotalGrpCount*RCKEYVAL_SIZE);


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: getPassthroughFields
 *
 * Description	: This function will return the address of Response pass through structure
 *
 * Input Params	: NONE
 *
 * Output Params: Address
 * ============================================================================
 */
RCHI_PASSTHROUGH_PTYPE getPassthroughFields()
{
	return &stRCHIPTDtls;
}

/*
 * ============================================================================
 * Function Name: getTotalGrpCount
 *
 * Description	: The Function will return the total Group count.
 *
 * Input Params	: NONE
 *
 * Output Params: Group Count
 * ============================================================================
 */
int getTotalGrpCount()
{
	return iTotalGrpCount;
}

/*
 * ============================================================================
 * Function Name: getTotalHostReqLst
 *
 * Description	: This function will return the pointer to Full request list
 *
 * Input Params	: NONE
 *
 * Output Params: Pointer of type RCKEYVAL_PTYPE
 * ============================================================================
 */
RCKEYVAL_PTYPE getTotalHostReqLst()
{
	return pstRCFullHostRequestLst;
}


/*
 * ============================================================================
 * Function Name: getArrayofDHIIndexforAmtConversion
 *
 * Description	: This function will return array Containing all DHI Index in which Amount Format hsa to be changed
 *
 * Input Params	: NONE
 *
 * Output Params: Pointer of type RCKEYVAL_PTYPE
 * ============================================================================
 */
int* getArrayofDHIIndexforAmtConversion()
{
	return piDHIIndexforAmountConvertion;
}
