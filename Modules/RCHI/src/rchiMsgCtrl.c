/*
 * fdMsgCtrl.c
 *
 *  Created on: Dec 5, 2014
 *      Author: BLR_SCA_TEAM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "RCHI/rchiCommon.h"
#include "RCHI/rchiGroups.h"
#include "RCHI/metaData.h"

/*typedef METADATA_STYPE	META_STYPE;
typedef METADATA_PTYPE	META_PTYPE;*/

int buildRCMsgReq(char **, char **);


/*
int buildRCMsgReq(char **szDHIDtls, char **szRCDtls)
{
	int			rv				= SUCCESS;
	META_STYPE	stMetaData;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		 Got the transaction data. Use this to get the message

		memset(&stMetaData, 0x00, METADATA_SIZE);
		fillRCDtls(szDHIDtls, szRCDtls);
		rv = getMetaDataForRCReq(&stMetaData, szDHIDtls, szRCDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}
		 Call the xml utility api to build the xml message from the metadata
		 * just built from the transaction data
		rv = buildXMLMsgFromMetaData(pszBuf, iLen, SSIREQ_STR, &stMetaData);
	}
}
*/
