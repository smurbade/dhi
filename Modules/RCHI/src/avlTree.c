/*
 * avlTree.c
 *
 *  Created on: 13-May-2016
 *      Author: T_AkshayaM1
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "RCHI/rchiHostCfg.h"
#include "RCHI/rchiCommon.h"
#include "RCHI/rchiCfg.h"
#include "RCHI/utils.h"
#include "RCHI/rcHash.h"
#include "RCHI/rchiGroups.h"
#include "DHI_App/appLog.h"
#include "DHI_App/tranDef.h"
#include "RCHI/xmlUtils.h"
#include "RCHI/avlTree.h"

AVL_PTYPE	gpstFCPDat;


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)



/*
 * ============================================================================
 * Function Name: loadFrgCurFile
 *
 * Description	: This function is used to load the Details of FCP.DAT(Foreign Currecny Properties File)
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
int loadFrgCurFile()
{
	int					rv					= SUCCESS;
	int					iLineCount			= 0;
	int					iCurrCode			= 0;
	int 				iLen				= 0;
	int					iLength				= 0;
	int					iAppLogEnabled		= 0;
	char				szBuffer[100]		= "";
	char*				pszStartTemp		= NULL;
	char*				pszNxtPtr			= NULL;
	char*				pszTemp				= NULL;
	char*				pszCurrCode			= NULL;
	char*				pszCountryName		= NULL;
	char*				pszCurrName			= NULL;
	char*				pszAlphaCurrCode	= NULL;
	char* 				pszCountryAlpha2Code= NULL;
	char*				pszCurrSym			= NULL;
	char*				pszMinorCurrUnit	= NULL;
const char* 			pszFilePath			= NULL;
	char				szAppLogData[256]	= "";
	char				szAppLogDiag[256]	= "";
	FILE*				fptr				= NULL;
	FEXCORESPDTLS_PTYPE pszValue 			= NULL;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Entering ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	pszFilePath = (const char*)getCurrencyDataFileName();
	if(doesFileExist(pszFilePath) == SUCCESS)
	{
		fptr = fopen(pszFilePath, "r");
		if(fptr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Not able to open file %s", __FUNCTION__, pszFilePath);
			RCHI_LIB_TRACE(szDbgMsg);

			return FAILURE;
		}

		/*This FGETS loop is to ignore the Beginning lines of FCP.DAT.txt */
		while (iLineCount < 2)
		{
			fgets(szBuffer, sizeof(szBuffer) - 1, fptr);
			if(szBuffer[strlen(szBuffer) - 1] == '\n')
			{
				iLineCount++;
			}
		}

		debug_sprintf(szDbgMsg, "%s:Skipeed reading the first two lines from the file", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

//		memset(szBuffer, 0x00, sizeof(szBuffer));
//		fgets(szBuffer, sizeof(szBuffer) - 1, fptr);

		while(fgets(szBuffer, sizeof(szBuffer) - 1, fptr))
		{
			iLength = strlen(szBuffer);
			szBuffer[iLength - 1] = '\0';

			pszStartTemp	= szBuffer;
			pszNxtPtr		= szBuffer;
			pszTemp 		= szBuffer;

			pszTemp = strchr(pszStartTemp , ',');
			if(pszTemp != NULL)
			{
				pszValue = (FEXCORESPDTLS_PTYPE)malloc (sizeof(FEXCORESPDTLS_STYPE));
				if(pszValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s:Error in Memory Allocation", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					return FAILURE;
					break;
				}

				memset(pszValue, 0x00, sizeof(pszValue));

				pszNxtPtr 	= strchr(pszStartTemp, ',');
				if(pszNxtPtr != NULL)
				{
					iLen = pszNxtPtr - pszStartTemp;
					pszCurrCode = strndup (pszStartTemp, iLen);
					pszStartTemp += iLen + 1;
				}
				else
				{
					pszStartTemp++;
				}

				if(pszCurrCode != NULL)
				{
					iCurrCode = atoi(pszCurrCode);
					strcpy(pszValue->szCurrCode, pszCurrCode);
				}

				pszNxtPtr 	= strchr(pszStartTemp, ',');
				if(pszNxtPtr != NULL)
				{
					iLen = pszNxtPtr - pszStartTemp;
					pszCountryName = strndup (pszStartTemp, iLen);
					pszStartTemp += iLen + 1;
				}
				else
				{
					pszStartTemp++;
				}

				if(pszCountryName != NULL)
				{
					strcpy(pszValue->szCountryName, pszCountryName);
				}

				pszNxtPtr 	= strchr(pszStartTemp, ',');
				if(pszNxtPtr != NULL)
				{
				iLen = pszNxtPtr - pszStartTemp;
				pszCurrName = strndup (pszStartTemp, iLen);
				pszStartTemp += iLen + 1;
				}
				else
				{
					pszStartTemp++;
				}

				if(pszCurrName != NULL)
				{
					strcpy(pszValue->szCurrName, pszCurrName);
				}

				pszNxtPtr 	= strchr(pszStartTemp, ',');
				if(pszNxtPtr != NULL)
				{
					iLen = pszNxtPtr - pszStartTemp;
					pszAlphaCurrCode = strndup (pszStartTemp, iLen);
					pszStartTemp += iLen + 1;
				}
				else
				{
					pszStartTemp++;
				}

				if(pszAlphaCurrCode != NULL)
				{
					strcpy(pszValue->szAlphaCurrCode, pszAlphaCurrCode);
				}

				pszNxtPtr 	= strchr(pszStartTemp, ',');
				if(pszNxtPtr != NULL)
				{
					iLen = pszNxtPtr - pszStartTemp;
					pszCountryAlpha2Code = strndup (pszStartTemp, iLen);
					pszStartTemp += iLen + 1;
				}
				else
				{
					pszStartTemp++;
				}

				if(pszCountryAlpha2Code != NULL)
				{
					strcpy(pszValue->szCountryAlpha2Code, pszCountryAlpha2Code);
				}
				pszNxtPtr 	= strchr(pszStartTemp, ',');
				if(pszNxtPtr != NULL)
				{
					iLen = pszNxtPtr - pszStartTemp;
					pszCurrSym = strndup (pszStartTemp, iLen);
					pszStartTemp += iLen + 1;
				}
				else
				{
					pszStartTemp++;
				}
				if(pszCurrSym != NULL)
				{
					strcpy(pszValue->szCurrSym, pszCurrSym);
				}

				pszNxtPtr 	= strchr(pszStartTemp, ',');
				if(pszNxtPtr != NULL)
				{
					iLen = pszNxtPtr - pszStartTemp;
					pszMinorCurrUnit = strndup (pszStartTemp, iLen);
					pszStartTemp += iLen + 1;
				}
				else
				{
					pszStartTemp++;
				}

				if(pszMinorCurrUnit != NULL)
				{
					strcpy(pszValue->szMinorCurrUnit, pszMinorCurrUnit);
				}
				gpstFCPDat = (AVL_PTYPE)insertElement(gpstFCPDat , iCurrCode, pszValue);
			}
		}
		fclose(fptr);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: File %s does not exist. Halting the application", __FUNCTION__, pszFilePath);
		RCHI_LIB_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "DCC is Enabled File %s does not exist", pszFilePath);
			sprintf(szAppLogDiag, "Please download %s file and restart the application", pszFilePath);
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			rv = FAILURE;
		}
	}
	if(rv != FAILURE)
	{
		sprintf(szAppLogData, "Loaded the %s successfully", pszFilePath);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);

		RCHI_LIB_TRACE(szDbgMsg);
	}
	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: insertElement
 *
 *
 * Description	: Based on the Currency code from FCP.DAT.txt file
 * 				  This function will insert the Currecny code into the AVL Tree.
 *
 * Input Params	: node
 *
 *
 * Output Params:
 * ============================================================================
 */
AVL_PTYPE insertElement(AVL_PTYPE pszNode, int iKey, FEXCORESPDTLS_PTYPE pszvalue)
{
	int 	iBalance 			= 0;

	/*step 1: Perform the Normal Binary Search Tree rotation*/
	if(pszNode == NULL)
	{
		return(createNewNode(iKey, pszvalue));
	}

	if(iKey < pszNode->iKey)
	{
		pszNode->left=insertElement(pszNode->left, iKey, pszvalue);
	}
	else
	{
		pszNode->right=insertElement(pszNode->right, iKey, pszvalue);
	}

	/*step 2 : Update Height of this node*/
	pszNode->iHeight = maxOfTwoNodes(calculateHeight(pszNode->left) , calculateHeight(pszNode->right)) + 1;

	/*step 3: Check the Balance Factor of This ancestor node to check whether this node became unbalanced*/
	iBalance = getBalance(pszNode);

	/*If this node is unbalanced , then there are 4 cases to do*/

	/*Left Left Case*/
	if(iBalance > 1 && iKey < pszNode->left->iKey)
	{
		return rightRotation(pszNode);
	}

	/*Right Right Case*/
	if(iBalance <-1 && iKey > pszNode->right->iKey)
	{
		return leftRotation(pszNode);
	}

	/*left Right case*/
	if(iBalance > 1 && iKey >pszNode->left->iKey)
	{
		pszNode->left= leftRotation(pszNode->left);
		return rightRotation(pszNode);
	}
	/*Right Left Case*/
	if(iBalance < -1 && iKey < pszNode->right->iKey)
	{
		pszNode->right = rightRotation(pszNode->right);
		return leftRotation(pszNode);
	}

	return pszNode;
}
/*
 * ============================================================================
 * Function Name: createNewNode
 *
 *
 * Description	:This function will Create a new node for each Currecny code,
 * 				  put the value and make the right and left node to NULL.
 *
 *
 * Input Params	: node
 *
 *
 * Output Params:
 * ============================================================================
 */
AVL_PTYPE createNewNode(int iKey, FEXCORESPDTLS_PTYPE pszValue)
{

	AVL_PTYPE pszNewNode 		= NULL;


	pszNewNode = (AVL_PTYPE)malloc (sizeof(AVL_STYPE) + 1) ;

	if(pszNewNode == NULL)
	{
		return 0;
	}
	else
	{
		pszNewNode->iKey 		= iKey;
		pszNewNode->pszValue 	= pszValue;
		pszNewNode->left 		= NULL;
		pszNewNode->right 		= NULL;
		pszNewNode->iHeight 	= 1;
	}

	return pszNewNode;
}
/*
 * ============================================================================
 * Function Name: maxOfTwoNodes
 *
 *
 * Description	: This function will return the maximum of two Nodes
 *
 *
 * Input Params	: node
 *
 *
 * Output Params:
 * ============================================================================
 */
static int maxOfTwoNodes(int iNode1, int iNode2)
{
	if(iNode1 > iNode2)
	{
		return iNode1;
	}
	else
	{
		return iNode2;
	}
}
/*
 * ============================================================================
 * Function Name: calculateHeight
 *
 *
 * Description	: This function will return the Height of the Tree.
 *
 *
 * Input Params	: node
 *
 *
 * Output Params:
 * ============================================================================
 */
static int calculateHeight(AVL_PTYPE pszHeight)
{
	if(pszHeight == NULL)
	{
		return 0;
	}
	else
	{
		return pszHeight->iHeight;
	}
}
/*
 * ============================================================================
 * Function Name: getBalance
 *
 *
 * Description	: This function will return the Balance of the Tree.
 *
 *
 * Input Params	: node
 *
 *
 * Output Params:
 * ============================================================================
 */
static int getBalance(AVL_PTYPE pszbalance)
{
	if(pszbalance == NULL)
	{
		return 0;
	}
	else
	{
		return calculateHeight(pszbalance->left ) - calculateHeight(pszbalance->right);
	}
}
/*
 * ============================================================================
 * Function Name: rightRotation
 *
 *
 * Description	: This function will return the node after right rotation.
 *
 *
 * Input Params	: node
 *
 *
 * Output Params:
 * ============================================================================
 */
AVL_PTYPE rightRotation(AVL_PTYPE pszRightRot)
{
	AVL_PTYPE pszRtNewNode = pszRightRot->left;
	AVL_PTYPE pszTemp = pszRtNewNode->right;

	/*Performing Rotation*/
	pszRtNewNode->right =pszRightRot;
	pszRightRot->left = pszTemp;

	/*update Height */
	pszRightRot->iHeight = maxOfTwoNodes(calculateHeight(pszRightRot->left), calculateHeight(pszRightRot->right))+1;
	pszRtNewNode->iHeight = maxOfTwoNodes(calculateHeight(pszRtNewNode->left), calculateHeight(pszRtNewNode->right))+1;

	return pszRtNewNode;
}
/*
 * ============================================================================
 * Function Name: LeftRotation
 *
 *
 * Description	: This function will return the node after Left rotation.
 *
 *
 * Input Params	: node
 *
 *
 * Output Params:
 * ============================================================================
 */
AVL_PTYPE leftRotation(AVL_PTYPE pszLeftRot)
{
	AVL_PTYPE pszLtNewNode = pszLeftRot ->right;
	AVL_PTYPE pszTemp = pszLtNewNode->left;

	/*Perform Rotation*/
	pszLtNewNode->left = pszLeftRot;
	pszLeftRot->right = pszTemp;

	/*update Height*/
	pszLeftRot->iHeight = maxOfTwoNodes(calculateHeight(pszLeftRot->left), calculateHeight(pszLeftRot->right))+1;
	pszLtNewNode->iHeight = maxOfTwoNodes(calculateHeight(pszLtNewNode->left), calculateHeight(pszLtNewNode->right))+1;

	return pszLtNewNode;
}

/*
 * ===============================================================================
 * Function Name: find
 *
 * Description	: This function is used to call the Function Search using the Gloabal Variable
 *
 * Input Params	:
 *
 * Output Params: none
 * ===============================================================================
 */
AVL_PTYPE findCurrCode(int ival)
{
	AVL_PTYPE pszElement=NULL;

	/*To Search the Element from the Root*/
	pszElement = searchElement(gpstFCPDat,ival);

	if(pszElement != NULL)
	{
		return pszElement;
	}
	else
	{
		return NULL;
	}
}


/*
 * ============================================================================
 * Function Name: searchElement
 *
 * Description	: This function is used to search the particular Element in the Tree.
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
AVL_PTYPE searchElement(AVL_PTYPE pszTree, int iValue)
{
	AVL_PTYPE pszTemp		=	NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pszTree == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: --- Returning NULL, The Root Element is NULL ---", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return NULL;
	}
	if(iValue == pszTree->iKey)
	{
		debug_sprintf(szDbgMsg, "%s: --- The Search Slement and the Root Node are same[%d] ---", __FUNCTION__, pszTree->iKey);
		RCHI_LIB_TRACE(szDbgMsg);

		return pszTree;
	}
	else if(iValue < pszTree->iKey)
	{
		debug_sprintf(szDbgMsg, "%s: --- The Search Element Lesser then Root Node . so Traversing Left side ---", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		pszTemp = searchElement(pszTree->left, iValue);
	}
	else if(iValue > pszTree->iKey)
	{
		debug_sprintf(szDbgMsg, "%s: --- the Search Element Greater then Root Node . so Traversing Right side ---", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		pszTemp = searchElement(pszTree->right, iValue);
	}

	return pszTemp;
}

