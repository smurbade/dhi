/*
 * ============================================================================
 * File Name	: utils.c
 *
 * Description	:
 *
 * Author		:
 * ============================================================================
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <semaphore.h>
#include <svcsec.h>
#include <sys/timeb.h>	// For struct timeb
#include <sys/stat.h>
#include <pthread.h>
#include <svc.h>


#include "RCHI/rchiCommon.h"
#include "RCHI/utils.h"
#include "DHI_APP/appLog.h"
#include "RCHI/metaData.h"
#include "RCHI/rchiHostCfg.h"
#include "RCHI/rchiCfg.h"



#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)



static unsigned char* 	pszFgnBins[10]	= {0};
static int toUTF(ushort , uchar *);

typedef union TwoByteShort
{
	uchar	b1b2[2];
	ushort	shVal;
}
TWOBYTE_SHORT;

#ifdef DEBUG
char chRCHIDebug = NO_DEBUG;
char chRCHIMemDebug = NO_DEBUG;
/*
 * ============================================================================
 * Function Name: RCHI_LIB_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void RCHI_LIB_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char *			pszMsgPrefix	= "RCHI";
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chRCHIDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chRCHIDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chRCHIDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: RCHI_LIB_MEM_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void RCHI_LIB_MEM_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char *			pszMsgPrefix	= "RCHI";
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chRCHIMemDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chRCHIMemDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chRCHIMemDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: RCHI_LIB_TRACE_EX
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void RCHI_LIB_TRACE_EX(char *pszMessage, int iMsgLen)
{
	char 		 szTimeStampPrefix[40+1]; // MM/DD/YYYY HH:MM:SS:sss
	char 		 szDebugMsg[MAX_DEBUG_MSG_SIZE+1+MAX_DEBUG_MSG_SIZE];
	char 		 *pszMsgPrefix = "PS";
	struct timeb the_time;
	struct tm 	 *tm_ptr;
	pid_t 		 processID = getpid();		// Get current process ID
	pthread_t 	 threadID = pthread_self();	// Get current thread's ID
	int          iPrefixLen;
	int          iDebugMsgSize = sizeof(szDebugMsg)-1;

	if (chRCHIDebug == NO_DEBUG)
	{
		return;
	}

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if (the_time.millitm >= 1000)
	{
		the_time.millitm = 999;	// Keep within 3 digits, since sprintf's width.precision specification does not truncate large values
	}
	sprintf(szTimeStampPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d",
						tm_ptr->tm_mon+1, tm_ptr->tm_mday, 1900+tm_ptr->tm_year,
						tm_ptr->tm_hour, tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	if (chRCHIDebug == ETHERNET_DEBUG)
	{
		iPrefixLen = sprintf(szDebugMsg, "%s %04d %04d %s: ", szTimeStampPrefix, processID,
							(int)threadID, pszMsgPrefix);
		// 07-Oct-08: make sure we don't exceed szDebugMsg[] capacity
		if ((iPrefixLen+iMsgLen) >= iDebugMsgSize) // If addition of full datasize will exceed szDebugMsg[]
		{
			iMsgLen = iDebugMsgSize - iPrefixLen;  // Chop iMsgLen down
		}

		memcpy(szDebugMsg+iPrefixLen, pszMessage, iMsgLen);
		DebugMsgEx((unsigned char *)szDebugMsg, iPrefixLen+iMsgLen);
	}
}

/*
 * ============================================================================
 * Function Name: setupRCHIDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupRCHIDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chRCHIDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(RCHI_DEBUG)) == NULL)
	{
		chRCHIDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chRCHIDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chRCHIDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chRCHIDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chRCHIDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}


/*
 * ============================================================================
 * Function Name: setupRCHIMemDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupRCHIMemDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chRCHIMemDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(RCHI_MEM_DEBUG)) == NULL)
	{
		chRCHIMemDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chRCHIMemDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chRCHIMemDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chRCHIMemDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chRCHIMemDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}


/*
 * ============================================================================
 * Function Name: closeupDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupDebug(void)
{
	if(chRCHIDebug == ETHERNET_DEBUG)
	{
		iCloseDebugSocket();
	}

	return;
}

/*
 * ============================================================================
 * Function Name: closeupMemDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupMemDebug(void)
{
	if(chRCHIMemDebug == ETHERNET_DEBUG)
	{
		iCloseDebugSocket();
	}

	return;
}

/*
 * ============================================================================
 * Function Name: setRCHIDebugParamsToEnv
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int setRCHIDebugParamsToEnv(int * piDbgParamSet)
{
	int 	rv 					= SUCCESS;
	char	szParamValue[20]	= "";
	int		iLen;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	iLen = sizeof(szParamValue);

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "RCHI_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting RCHI_DEBUG variable[%s=%s]", __FUNCTION__, "RCHI_DEBUG", szParamValue);
		RCHI_LIB_TRACE(szDbgMsg);

		setenv("RCHI_DEBUG", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "RCHI_MEM_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting RCHI_MEM_DEBUG variable[%s=%s]", __FUNCTION__, "RCHI_MEM_DEBUG", szParamValue);
		RCHI_LIB_TRACE(szDbgMsg);

		setenv("RCHI_MEM_DEBUG", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGIP", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGIP variable[%s=%s]", __FUNCTION__, "DBGIP", szParamValue);
		RCHI_LIB_TRACE(szDbgMsg);

		setenv("DBGIP", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGPRT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGPRT variable[%s=%s]", __FUNCTION__, "DBGPRT", szParamValue);
		RCHI_LIB_TRACE(szDbgMsg);

		setenv("DBGPRT", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DEBUGOPT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DEBUGOPT variable[%s=%s]", __FUNCTION__, "DEBUGOPT", szParamValue);
		RCHI_LIB_TRACE(szDbgMsg);

		setenv("DEBUGOPT", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: getTime
 *
 * Description	: This function fills the Time buffer with the time based
 * 				  on the format(GMT or Local Time).
 *
 * Input Params	: Time Buffer to be filled,
 * 				  Time format(Local Time or GMT)
 *
 * Output Params: None
 * ============================================================================
 */
void getTime(char* szTimebuf, int iFormat)
{
	int 		rv 			= SUCCESS;
	time_t 		currTime 	= 0;
	struct tm 	*tm_ptr 	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	currTime = time(NULL);

	switch(iFormat)
	{
	/* KranthiK1: 05-08-2016
	 * Flipping the times as the values that are generated are one for other as we are seetting the TZ variable.
	 */
		case LOCALTIME:
			tm_ptr = gmtime(&currTime);

			if(tm_ptr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get local date and time",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			break;

		case GMTTIME:
			tm_ptr = localtime(&currTime);

			if(tm_ptr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Transmission date and time",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here...",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;

	}

	if(rv == SUCCESS)
	{
		sprintf(szTimebuf, "%04d%02d%02d%02d%02d%02d",tm_ptr->tm_year + 1900, tm_ptr->tm_mon + 1, tm_ptr->tm_mday, tm_ptr->tm_hour, tm_ptr->tm_min, tm_ptr->tm_sec);
		debug_sprintf(szDbgMsg,"%s:Time:%s",__FUNCTION__, szTimebuf);
		RCHI_LIB_TRACE(szDbgMsg);

	}
}

/*
 * ============================================================================
 * Function Name: validateNumericField
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int validateNumericField(char * szFieldVal)
{
	int		rv				= SUCCESS;
	int		iValLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		iValLen = strlen(szFieldVal);
		if(iValLen <= 0) {
			debug_sprintf(szDbgMsg, "%s: No value present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Check for characters */
		if(strspn(szFieldVal, "1234567890") != iValLen)
		{
			debug_sprintf(szDbgMsg, "%s: Characters other than 0-9 found",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getHILibVersion
 *
 *
 * Description	: This function will fill the szDatawireID buffer
 *
 *
 * Input Params	: DataWireID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getHILibVersion(char *szLibVersion, char *szHIName)
{
	sprintf(szLibVersion, "%s|%s", RCHI_LIB_VERSION, RCHI_BUILD_NUMBER);
	sprintf(szHIName, "%s", "Rapid Connect");
}

/*
 * ============================================================================
 * Function Name: doesFileExist
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int doesFileExist(const char *szFullFileName)
{
	int			rv			= SUCCESS;
	struct stat	fileStatus;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	memset(&fileStatus, 0x00, sizeof(fileStatus));

	rv = stat(szFullFileName, &fileStatus);
	if(rv < 0)
	{
		if(errno == EACCES)
		{
			debug_sprintf(szDbgMsg, "%s: Not enough search permissions for %s",
												__FUNCTION__, szFullFileName);
		}
		else if (errno == ENAMETOOLONG)
		{
			debug_sprintf(szDbgMsg, "%s: Name %s is too long", __FUNCTION__,
																szFullFileName);
		}
		else
		{
			/* For ENOENT/ENOTDIR/ELOOP */
			debug_sprintf(szDbgMsg,
						"%s: File - [%s] (Name not correct)/(file doesnt exist)"
												, __FUNCTION__, szFullFileName);
		}
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		/* Check if the file is a regular file */
		if(S_ISREG(fileStatus.st_mode))
		{
			/* Check if the file is not empty */
			if(fileStatus.st_size <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: File [%s] is empty", __FUNCTION__,
																szFullFileName);
				RCHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error: File [%s] is not a regular file"
												, __FUNCTION__, szFullFileName);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: acquireMutexLock
 *
 * Description	: This function is used to acquire a mutex for sychronization
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void acquireRCMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_lock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to acquire %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: releaseMutexLock
 *
 * Description	: This function is used to release an already acquired mutex
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void releaseRCMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_unlock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to release %s mutex, errno = %d", __FUNCTION__, pszName, errno);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: loadFgnCardData
 *
 * Description	: This function is used to load all the foreign card into the memory
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
/*
 * Logic: Using a Bit map to represent the Foreign Bin Card Data
 * Assigning one particular bit for all possible numbers starting with 3 or 4 or 5 etc.
 * If there is a bin starting with 4 in the HFEX.DAT then we allocate 12500 bytes of memory to
 * represent all possible numbers starting with 4 having length 6. So total will be 100000 and we need
 * 100000 bits i.e 12500 bytes to represent all of them
 * If the bin is existing in the file we set that particular bit or else it will be left as 0
 */
int loadFgnCardBinData()
{
	int				rv					= SUCCESS;
	int				iBin				= -1;
	int				iBitNo				= -1;
	int				iLength				= 0;
	int				iCardStartIndex 	= -1;
	int				iCardRangeByte		= -1;
	char			szBuffer[100 ]		= "";
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";
const char* 		pszFilePath			= NULL;
	FILE*			fptr				= NULL;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pszFilePath = (const char*)getFGNCardBinFileName();
	if(doesFileExist(pszFilePath) == SUCCESS)
	{
		fptr = fopen(pszFilePath, "r");
		if(fptr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Not able to open file %s", __FUNCTION__, pszFilePath);
			RCHI_LIB_TRACE(szDbgMsg);

			return FAILURE;
		}

		while(fgets(szBuffer, sizeof(szBuffer) - 1, fptr))
		{
			iLength = strlen(szBuffer);
			szBuffer[iLength - 1] = '\0';

			if(strlen(szBuffer) == 6)
			{
				if(isdigit(szBuffer[0]))
				{
					iCardStartIndex = szBuffer[0] - '0';
					if (pszFgnBins[iCardStartIndex] == NULL)
					{
						pszFgnBins[iCardStartIndex] = (unsigned char *)calloc(12500, 1);
						if(pszFgnBins[iCardStartIndex] == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Not able to open file %s", __FUNCTION__, pszFilePath);
							RCHI_LIB_TRACE(szDbgMsg);

							rv = FAILURE;
							break;
						}
					}
				}
				iBin			= atoi(szBuffer + 1);
				iCardRangeByte	= iBin / 8; //Calculating the byte number under which this bin falls
				iBitNo			= iBin % 8; // Calculating the bit number which is needed to be set for this bin
				pszFgnBins[iCardStartIndex][iCardRangeByte] |= (1 << iBitNo); //Setting the bit to 1
			}
		}

		fclose(fptr);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: File %s does not exist. Halting the application", __FUNCTION__, pszFilePath);
		RCHI_LIB_TRACE(szDbgMsg);

		sprintf(szAppLogData, "DCC is Enabled File %s does not exist", pszFilePath);
		sprintf(szAppLogDiag, "Please download %s file and restart the application", pszFilePath);
		addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);

		rv = FAILURE;
	}

	if(rv != FAILURE)
	{
		sprintf(szAppLogData, "Loaded the %s DCC Bin Data successfully", pszFilePath);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);

		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning --- %d", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isFGNCard
 *
 * Description	: This function is used to tell if the card swiped is a foreign card
 * 				by checking if the bit for that number is being set.
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
int isFGNCard(char* pszPan)
{
	int		iBin				= -1;
	int		iBitNo				= -1;
	int		iResult				= -1;
	int		iCardRangeByte		= -1;
	int		iCardStartIndex		= -1;
	char	szBin[7]			= "";
	char	szAppLogData[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pszPan != NULL)
	{
		strncpy(szBin, pszPan, 6);
		if(strspn(szBin, "0123456789") != 6)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid PAN", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			iResult = FAILURE;
		}
		else
		{
			iCardStartIndex = szBin[0] - '0';

			iBin			= atoi(szBin + 1);
			iCardRangeByte	= iBin / 8; // Calculating the byte number under which this bin falls
			iBitNo			= iBin % 8; // Calculating the bit number which is needed to be set for this bin

			if(pszFgnBins[iCardStartIndex] != NULL)
			{
				iResult 	= pszFgnBins[iCardStartIndex][iCardRangeByte] & (1 << iBitNo); //Checking if the bit in the location is set
			}
		}
	}

	if(iResult > 0)
	{
		iResult = RCHI_TRUE;

		sprintf(szAppLogData, "Card is Eligible for DCC");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);

		debug_sprintf(szDbgMsg, "%s: Card is Eligible for DCC. Matches the Bin: %s", __FUNCTION__, szBin);
		RCHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		iResult = RCHI_FALSE;

		sprintf(szAppLogData, "Card is Not Eligible for DCC");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);

		debug_sprintf(szDbgMsg, "%s: Card is Not Eligible for DCC", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning --- %d", __FUNCTION__, iResult);
	RCHI_LIB_TRACE(szDbgMsg);

	return iResult;
}

#if 0
void preOrder(AVL_PTYPE root)
{
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

    if(root != NULL)
    {
    	preOrder(root->right);
    	preOrder(root->left);
    }
}
#endif

/*
 * ============================================================================
 * Function Name: toUTF
 *
 * Description	: just a patch code for the multi-language compatibility...
 * 					TODO Find a proper fix for the whole issue asap
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int toUTF(ushort utfShort, uchar * pszOut)
{
	int		iLen			= 0;
	uchar	utfArr[4]		= "";	/* 3 + 1 */
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: utfShort = [%04X]", __FUNCTION__, utfShort);
	RCHI_LIB_TRACE(szDbgMsg);

	if((utfShort == 0xFEFF) || (utfShort == 0xFFFE))
	{
		/* ignore BOM character */
		iLen = 0;
	}
	else if (utfShort < 0x80)					// One byte UTF-8
	{

		utfArr[0] = (uchar) utfShort;			// Use number as is
		iLen = 1;
	}
	else if (utfShort < 0x0800)					// Two byte UTF-8
	{
		utfArr[1] = 0x80 + (utfShort & 0x3F);	// Least Significant 6 bits



		utfShort = utfShort >> 6;				// Shift utfShort right 6 bits
		utfArr[0] = 0xC0 + (utfShort & 0x1F);	// Use 5 remaining bits



		debug_sprintf(szDbgMsg, "%s: -- %02X - %02X", __FUNCTION__, utfArr[0],
																utfArr[1]);
		RCHI_LIB_TRACE(szDbgMsg);

		iLen = 2;
	}
	else										// Three byte UTF-8
	{
		utfArr[2] = 0x80 + (utfShort & 0x3F);	// Least Significant 6 bits
		utfShort = utfShort >> 6;				// Shift utfShort right 6 bits
		utfArr[1] = 0x80 + (utfShort & 0x3F);	// Use next 6 bits
		utfShort = utfShort >> 6;				// Shift utfShort right 6 bits
		utfArr[0] = 0xE0 + (utfShort & 0x0F);	// Use 4 remaining bits
		debug_sprintf(szDbgMsg, "%s: -- %02X - %02X - %02X", __FUNCTION__, utfArr[0],
																		utfArr[1], utfArr[2]);
		iLen = 3;
	}

	if(iLen > 0)
	{
		memcpy(pszOut, utfArr, iLen);	// Copy UTF-8 bytes
	}

	return iLen;
}

/*
 * ============================================================================
 * Function Name: convStrToUTFEscStr
 *
 * Description	: just a patch code for the multi-language compatibility...
 * 					TODO Find a proper fix for the whole issue asap
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void convStrToUTFStr(char * szInp, char * szOut)
{
	int				iCnt			= 0;
	int				jCnt			= 0;
	TWOBYTE_SHORT	byte2Short;

	for(iCnt = 0; szInp[iCnt] != '\0'; iCnt++)
	{
		if(szInp[iCnt] > 0x7F)
		{

			byte2Short.b1b2[1] = 0x00;
			byte2Short.b1b2[0] = szInp[iCnt];
			jCnt = toUTF(byte2Short.shVal, (uchar *)szOut);
		}
		else
		{
			*szOut = szInp[iCnt];
			jCnt = 1;
		}

		szOut += jCnt;
	}

	return;
}

