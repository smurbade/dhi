
/*
 * =============================================================================
 * Filename    : metaDataUtils.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *							SCA Dev Team
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "RCHI/rchiCommon.h"
#include "RCHI/metaData.h"
#include "DHI_App/tranDef.h"


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)



/*
 * ============================================================================
 * Function Name: freeMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int freeMetaData(METADATA_PTYPE metaDataPtr)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	void *			dataPtr			= NULL;
	void *			tmpPtr			= NULL;
	VAL_LST_PTYPE	valLstPtr		= NULL;
	VAL_NODE_PTYPE	valNodePtr		= NULL;
	RCKEYVAL_PTYPE	listPtr			= NULL;
	METADATA_STYPE	stLocMetaData;
#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//RCHI_LIB_TRACE(szDbgMsg);

	memset(&stLocMetaData, 0x00, METADATA_SIZE);

	listPtr = metaDataPtr->keyValList;

	if(listPtr == NULL)
	{
		//debug_sprintf(szDbgMsg, "%s: No list to free!!!", __FUNCTION__);
		//RCHI_LIB_TRACE(szDbgMsg);
		//memset(metaDataPtr, 0x00, METADATA_SIZE);

		return rv;
	}

	/* Clean all the values */
	for(iCnt = 0; iCnt < metaDataPtr->iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}

		switch(listPtr[iCnt].valType)
		{
		case SINGLETON:
		case AMOUNT:
		case NUMERICS:
		case MASK_NUM_STR:
		case BOOLEAN:
		case STRING:
		case LIST:
		case TIME:
		case DATE:
			free(listPtr[iCnt].value);
			listPtr[iCnt].value = NULL;

			break;

		case ATTR_LKDLST: /* VDR: FIX THIS */

			valNodePtr = (VAL_NODE_PTYPE) listPtr[iCnt].value;

			while(valNodePtr)
			{
				stLocMetaData.iTotCnt = valNodePtr->elemCnt;
				stLocMetaData.keyValList = valNodePtr->elemList;

				freeMetaData(&stLocMetaData);

				if(valNodePtr->szVal != NULL)
				{
					free(valNodePtr->szVal);
				}

				dataPtr = valNodePtr;
				valNodePtr = valNodePtr->next;

				free(dataPtr);
			}

			break;

		case MULTILIST:
			dataPtr = listPtr[iCnt].value;
			while(dataPtr)
			{
				tmpPtr = dataPtr;
				dataPtr = ((MLVAL_PTYPE) dataPtr)->next;

				free(tmpPtr);
				tmpPtr = NULL;
			}

			break;

		case VARLIST:
		case RECLIST:
			valLstPtr = (VAL_LST_PTYPE) listPtr[iCnt].value;

			valNodePtr = valLstPtr->start;
			while(valNodePtr)
			{
				stLocMetaData.iTotCnt = valNodePtr->elemCnt;
				stLocMetaData.keyValList = valNodePtr->elemList;

				freeMetaData(&stLocMetaData);

				if(valNodePtr->szVal != NULL)
				{
					free(valNodePtr->szVal);
				}

				/* DAIVIK:16/5/2016- The absence of the below free ,would cause memory leak when freeing the metadata.
				 * Required when we are freeing a list within another list.
				 */
				if(valNodePtr->elemList != NULL)
				{
					free(valNodePtr->elemList);
					valNodePtr->elemList = NULL;
				}

				dataPtr = valNodePtr;
				valNodePtr = valNodePtr->next;
				free(dataPtr);
			}

			free(valLstPtr);

			break;

		case NULL_ADD:
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: [%s]SHOULD NEVER COME HERE [%d]",
												__FUNCTION__, listPtr[iCnt].key,
												listPtr[iCnt].valType);
			RCHI_LIB_TRACE(szDbgMsg);

			free(listPtr[iCnt].value);
			listPtr[iCnt].value = NULL;

			break;
		}
	}

	memset(metaDataPtr, 0x00, METADATA_SIZE);

	return rv;
}

/*
 * ============================================================================
 * Function Name: freeMetaDataEx
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int freeMetaDataEx(METADATA_PTYPE metaDataPtr)
{
	int				rv		= SUCCESS;
	RCKEYVAL_PTYPE	listPtr	= NULL;

#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	listPtr = metaDataPtr->keyValList;

	if(metaDataPtr != NULL)
	{
		freeMetaData(metaDataPtr);
	}

	if(listPtr != NULL)
	{
		free(listPtr);
	}

	memset(metaDataPtr, 0x00, METADATA_SIZE);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * End of file metaDataUtils.c
 * ============================================================================
 */
