/******************************************************************
 *                      RCResponseParse.c                          *
 *******************************************************************
 * Application: DHI                                                *
 * Platform:    Mx9XX                                              *
 * Language:    C                                                  *
 * Lib used:    none                                               *
 * Purpose:     Contains the apis related to parsing of            *
 *              RC Response                                        *
 * 			                                                       *
 *                                                                 *
 * Created on: Dec 8, 2014                                         *
 * History:                                                        *
 * Date     Ver   Developer     Description                        *
 * -------- ----  ------------  -----------------------------      *
 *                 VFI                                             *
 *                                                                 *
 * ================================================================*
 *                   Copyright, 1995 - 2002 VeriFone, Inc.         *
 *                   2455 Augustine Drive                          *
 *                   Santa Clara, CA 95054                         *
 *                                                                 *
 *                   All Rights Reserved.                          *
 * ================================================================*/

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "DHI_App/appLog.h"
#include "RCHI/rchiCommon.h"
//#include "RCHI/httpHandler.h"
#include "DHI_App/tranDef.h"
#include "RCHI/metaData.h"
#include "RCHI/rchiGroups.h"
#include "RCHI/xmlUtils.h"
#include "RCHI/rchi.h"
#include "RCHI/rcHash.h"
#include "RCHI/rchiRespFields.inc"
#include "RCHI/rchiHostCfg.h"
#include "RCHI/rchiCfg.h"


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)



#define MAX_SIZE			256
#define NUMBER_CONVERION 	10
#define RCHI_INCOMPLETE_KEY 2

extern void getEmbossedPANFromTrackData(char *, char *);
extern RCHI_BOOL isEmbossedCardNumCalcRequired();
extern void convStrToUTFStr(char *, char *);

static int fillRCDataStruct(char *, char *, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
static int buildGenRespDtlsForSSI(xmlDocPtr, xmlNode *, TRANDTLS_PTYPE);
static int fillHostTotalNode(xmlNodePtr, VAL_HOSTTOTALNODE_PTYPE);
static int getCardTag(xmlNodePtr, char **);
static int getCardTransAmt(xmlNodePtr, char **);
static int getTransCount(xmlNodePtr, char **);
static void fillAvailableBalance(char *, char **);
static void fillInternAndTranSeqNum(RCTRANDTLS_PTYPE, TRANDTLS_PTYPE);
VAL_HOSTTOTALNODE_PTYPE addNode(VAL_HOSTTOTALNODE_PTYPE,VAL_HOSTTOTALNODE_PTYPE);

FEXCORESPDTLS_PTYPE findCurrCode(int);
static void delEMVAdminNode(VAL_EMVADMINNODE_PTYPE);
static int createAndAddFilledNode(char *);
//REMOVE THIS LATER
void displayEMV();

/* Global Head Node for EMV Public Key List.
 */
VAL_EMVADMINHEADNODE_STYPE gstEMVAdmin;
static int 			iAddAmtIndex	= 0;
static RCHI_BOOL	bFlag			= RCHI_FALSE;
/*
 * ============================================================================
 * Function Name: parseAndFillDataStruct
 *
 * Description	: This function  parses the RC response and then fills the transaction
 * 				  details structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void parseAndFillDataStruct(xmlNodePtr node, TRANDTLS_PTYPE pstDHIReqResFields, RCTRANDTLS_PTYPE pstRCReqResFields)
{
	xmlChar*	value = NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);

	/*Base case of the recursive function checking for null*/
	if(node == NULL)
	{
		return;
		debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

//	debug_sprintf(szDbgMsg, "%s: Node Name[%s] Node type[%d] children[%p] childrencount [%ld]", __FUNCTION__, node->name, node->type, node->children, xmlChildElementCount(node));
//	RCHI_LIB_TRACE(szDbgMsg);

	if (node->type == XML_TEXT_NODE)
	{
		parseAndFillDataStruct(node->next, pstDHIReqResFields, pstRCReqResFields);
	}
	else if(xmlChildElementCount(node) == 0 )
	{
		value = xmlNodeGetContent(node);
		/* If the node is an element node then filling the node and the content in the datastructure*/

		fillRCDataStruct((char*)node->name, (char*)value, pstDHIReqResFields, pstRCReqResFields);

		/* We need to parse it into required format and then send to SSI.
		 * We are also receiving keys which are not complete, hence we save the entire list of keys and return only complete
		 * keys.
		 */
		if(!strcmp((char *)node->name,"FBData"))
		{
			gstEMVAdmin.pszEMVKeyLst = (char *)malloc(strlen((char*)value)+1);
			strcpy(gstEMVAdmin.pszEMVKeyLst,(char*)value);
		}

		if(value != NULL)
		{
			xmlFree(value);
			value = NULL;
		}

		/*After filling the node going to the sibling of the same node*/
		parseAndFillDataStruct(node->next, pstDHIReqResFields, pstRCReqResFields);
	}
	else
	{
		parseAndFillDataStruct(node->xmlChildrenNode, pstDHIReqResFields, pstRCReqResFields);

		parseAndFillDataStruct(node->next, pstDHIReqResFields, pstRCReqResFields);
	}
}

/*
 * ============================================================================
 * Function Name: parseAndFillDataStructForHostTotals
 *
 * Description	: This function  parses the RC response and then fills the transaction
 * 				  details structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void parseAndFillDataStructForHostTotals(xmlNodePtr node, TRANDTLS_PTYPE pstDHIReqResFields, RCTRANDTLS_PTYPE pstRCReqResFields,VAL_HOSTTOTALHEADNODE_PTYPE stHostTotalLst)
{
	int 		rv								= SUCCESS;
	float		fAmount							= 0.0;
	xmlChar*	value 							= NULL;
	xmlNodePtr tempNode 						= NULL;
	VAL_HOSTTOTALNODE_PTYPE HostTotalGrp 		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);


	/*Base case of the recursive function checking for null*/
	if(node == NULL)
	{
		return;
		debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

//	debug_sprintf(szDbgMsg, "%s: Node Name[%s] Node type[%d] children[%p] childrencount [%ld]", __FUNCTION__, node->name, node->type, node->children, xmlChildElementCount(node));
//	RCHI_LIB_TRACE(szDbgMsg);

	if (node->type == XML_TEXT_NODE)
	{
		parseAndFillDataStructForHostTotals(node->next, pstDHIReqResFields, pstRCReqResFields,stHostTotalLst);
	}
	else if(xmlChildElementCount(node) == 0 )
	{
		value = xmlNodeGetContent(node);
		/* Storing the obtained Net Settlement amount for providing it to the POS */
		if(!xmlStrcmp(node->name, (const xmlChar *) "NetSettleAmt"))
		{
			strcpy(stHostTotalLst->szNetSettlemntAmt, (char*)value);
			/* Correcting the amount value */
			fAmount = atof(stHostTotalLst->szNetSettlemntAmt);
			fAmount	= fAmount/100;
			sprintf(stHostTotalLst->szNetSettlemntAmt, "%.02f", fAmount);

			debug_sprintf(szDbgMsg, "%s: Filled Net Settlement Amt: %s", __FUNCTION__,stHostTotalLst->szNetSettlemntAmt);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		/* If the node is an element node then filling the node and the content in the datastructure*/
		fillRCDataStruct((char*)node->name, (char*)value, pstDHIReqResFields, pstRCReqResFields);

		if(value != NULL)
		{
			xmlFree(value);
			value = NULL;
		}

		/*After filling the node going to the sibling of the same node*/
		parseAndFillDataStructForHostTotals(node->next, pstDHIReqResFields, pstRCReqResFields,stHostTotalLst);
	}
	else
	{

		/* Checking for Host Total Details Group Node */
		if(!xmlStrcmp(node->name, (const xmlChar *) "HostTotDetGrp" ))
		{

			tempNode = node->xmlChildrenNode;
			HostTotalGrp = (VAL_HOSTTOTALNODE_PTYPE)malloc(HOSTTOTALNODE_SIZE);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				return;

			}
			rv = fillHostTotalNode(tempNode, HostTotalGrp);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Host Total Details Group node could not be filled", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

			}

			debug_sprintf(szDbgMsg, "%s: Card Tag[%s], TransAmt[%s] and TransCount[%s]", __FUNCTION__,HostTotalGrp->pszCardTag, HostTotalGrp->pszTansAmt, HostTotalGrp->pszTransCnt );
			RCHI_LIB_TRACE(szDbgMsg);

			stHostTotalLst->pstHead = addNode(stHostTotalLst->pstHead, HostTotalGrp);
			stHostTotalLst->iMaxHostTotalGroup++;
			//display(stHostTotalLst->pstHead);
		}
		else
		{
			parseAndFillDataStructForHostTotals(node->xmlChildrenNode, pstDHIReqResFields, pstRCReqResFields,stHostTotalLst);
		}

		parseAndFillDataStructForHostTotals(node->next, pstDHIReqResFields, pstRCReqResFields, stHostTotalLst);
	}
}


/*
 * ============================================================================
 * Function Name: correctDHIResponseFields
 *
 * Description	: This function  parses the RC response and then fills the transaction
 * 				  details structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int correctDHIResponseFields(TRANDTLS_PTYPE pstDHIReqResFields)
{
	int			rv 			 	= SUCCESS;
	int			iHostRsltCode	= -1;
	float		fAmount			= 0;
	char		szBuffer[50]	= "";
	char		szTime[10]		= "";
	char		szDate[10]		= "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	/*Filling the required response details in the ssi response*/
	if(pstDHIReqResFields[eHOST_RESPCODE].elementValue != NULL)
	{
		iHostRsltCode = atoi(pstDHIReqResFields[eHOST_RESPCODE].elementValue);

		if(pstDHIReqResFields[eTERMINATION_STATUS].elementValue == NULL)
		{
			pstDHIReqResFields[eTERMINATION_STATUS].elementValue = strdup("SUCCESS");
		}

		if (iHostRsltCode == 0 || iHostRsltCode == 2 || iHostRsltCode == 12 || iHostRsltCode == 701 || iHostRsltCode == 703 )
		{
			if (pstDHIReqResFields[eCOMMAND].elementValue != NULL      &&
					!strcmp(pstDHIReqResFields[eCOMMAND].elementValue, "VOID")) //For VOID we have send 7
			{
				pstDHIReqResFields[eRESULT_CODE].elementValue = strdup("7");
				pstDHIReqResFields[eRESULT].elementValue = strdup("VOIDED");
			}
			else if(pstDHIReqResFields[eVSP_REG].elementValue != NULL      &&
					!strcmp(pstDHIReqResFields[eVSP_REG].elementValue, "1") && (iHostRsltCode == 0 || iHostRsltCode == 12)) // VSP Registration Case
			{
				pstDHIReqResFields[eRESULT_CODE].elementValue = strdup("905");

				if(pstDHIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
				{
					pstDHIReqResFields[eRESULT].elementValue = strdup(pstDHIReqResFields[eRESPONSE_TEXT].elementValue);
				}
			}
			else
			{
				pstDHIReqResFields[eRESULT_CODE].elementValue = strdup("5");
				if(iHostRsltCode == 701 || iHostRsltCode == 703)
				{
					pstDHIReqResFields[eRESPONSE_TEXT].elementValue = strdup("APPROVED");
				}
			}

			if(pstDHIReqResFields[eRESPONSE_TEXT].elementValue != NULL &&
					pstDHIReqResFields[eRESULT].elementValue == NULL)
			{
				pstDHIReqResFields[eRESULT].elementValue = strdup(pstDHIReqResFields[eRESPONSE_TEXT].elementValue);
			}

			/* Modifying the approved amount matching SSI format*/
			if(pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue != NULL)
			{
				fAmount = atof(pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue);
				fAmount	= fAmount/100;

				sprintf(pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue, "%.02f", fAmount);
			}
		}
		else
		{
			pstDHIReqResFields[eRESULT_CODE].elementValue = strdup("6");

			pstDHIReqResFields[eRESULT].elementValue = strdup("Error");

			/* For Non-Success Cases we need not send the Approved amount*/
			if(pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue != NULL)
			{
				free(pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue);
				pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue = NULL;
			}

		}
	}

	/*Filling the date and time in SSI format*/
	if(pstDHIReqResFields[eTRANS_DATE].elementValue != NULL)
	{
		strcpy(szBuffer, pstDHIReqResFields[eTRANS_DATE].elementValue);

		/* Date returned by RCHI will be of format 20141217145900
		 * Separating the date first by printing in the expected way
		 */
		sprintf(szDate, "%.4s.%.2s.%.2s", szBuffer, szBuffer+4, szBuffer+6);

		sprintf(szTime, "%.2s:%.2s:%.2s", szBuffer+8, szBuffer+10, szBuffer+12);

		if(pstDHIReqResFields[eTRANS_DATE].elementValue != NULL)
		{
			strcpy(pstDHIReqResFields[eTRANS_DATE].elementValue, szDate);
		}
		if(pstDHIReqResFields[eTRANS_TIME].elementValue != NULL)
		{
			strcpy(pstDHIReqResFields[eTRANS_TIME].elementValue, szTime);
		}
	}

	/* Filling the vsp result if there is a time out
	 *
	 * */
	if(pstDHIReqResFields[eHOST_RESPCODE].elementValue && pstDHIReqResFields[eRESPONSE_TEXT].elementValue)
	{
		if(strcmp(pstDHIReqResFields[eHOST_RESPCODE].elementValue, "909") == SUCCESS
			&& strcmp(pstDHIReqResFields[eRESPONSE_TEXT].elementValue, "VSP Timeout") == SUCCESS)
		{
			pstDHIReqResFields[eVSP_CODE].elementValue = strdup("327");
			pstDHIReqResFields[eVSP_RESULTDESC].elementValue = strdup("VSP Registration Required");
		}
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillBankUserData
 *
 * Description	: Filling the bank user data that could be required in the follow on
 * 				  transactions
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void fillBankUserData(RCTRANDTLS_PTYPE pstRCReqResFields, char** pszBankUserData)
{
	char szBuffer[128]	= "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Order in which the bank userdata is filled */
	/* PosEntryMode|PosCondCode|TermEntryCapability|CardType|*/

	if(strcmp(pstRCReqResFields[RC_TRAN_TYPE].elementValue, "TATokenRequest") == SUCCESS)
	{
		/* Hard coding POS Entry Mode for 011 for a refund on token query */
		strcat(szBuffer, "011");
		strcat(szBuffer, "/");

		strcat(szBuffer, pstRCReqResFields[RC_POS_COND_CODE_TKN_QUERY].elementValue);
		strcat(szBuffer, "/");

		strcat(szBuffer, pstRCReqResFields[RC_TERM_ENTRY_CAPABLT_TKN_QUERY].elementValue);
		strcat(szBuffer, "/");
	}

	else
	{
		strcat(szBuffer, pstRCReqResFields[RC_POS_ENTRY_MODE].elementValue);
		strcat(szBuffer, "/");

		strcat(szBuffer, pstRCReqResFields[RC_POS_COND_CODE].elementValue);
		strcat(szBuffer, "/");

		strcat(szBuffer, pstRCReqResFields[RC_TERM_ENTRY_CAPABLT].elementValue);
		strcat(szBuffer, "/");

	}

	strcat(szBuffer, pstRCReqResFields[RC_CARD_TYPE].elementValue);
	strcat(szBuffer, "/");

	*pszBankUserData = strdup(szBuffer);

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: parseRCResponse
 *
 * Description	: This function parses the RC Response and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int parseRCResponse(char* pszRCResp, TRANDTLS_PTYPE pstDHIReqResFields, RCTRANDTLS_PTYPE pstRCReqResFields)
{
	int			rv									= SUCCESS;
	int			iCnt								= 0;
	int			iLen								= 0;
	int			iValLen								= 0;
	int			iSize								= 0;
	int			iAmount								= 0;
	int			iIndex								= 0;
	int			iAppLogEnabled						= 0;
	char		szRootName[50]						= "";
	char		szAppLogData[256]					= "";
	char		szPAN[30]							= "";
	char		szTemp[256]							= "";
	xmlDoc *	docPtr								= NULL;
	xmlNode *	rootPtr								= NULL;
	int*		piDHIIndexforAmountConvertion		= NULL;
	double		fAmount								= 0.0;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(pszRCResp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Response to Parse!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(pszRCResp);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);
#if 0
#ifdef DEVDEBUG
		if(strlen(pszRCResp) < 4500)
		{
			debug_sprintf(szDbgMsg, "%s: %s", __FUNCTION__, pszRCResp);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif
#endif
		docPtr = xmlParseMemory(pszRCResp, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		strcpy(szRootName, (char *)rootPtr->name);

		debug_sprintf(szDbgMsg, "%s: The root name of the RC XML Response is %s",__FUNCTION__, szRootName);
		RCHI_LIB_TRACE(szDbgMsg);

		/* Calling a recursive function which parses through the xml and fills all the response details*/
		parseAndFillDataStruct(rootPtr, pstDHIReqResFields, pstRCReqResFields);

		iAddAmtIndex	= 0;
		bFlag			= RCHI_FALSE;

		if(iAppLogEnabled && strlen(pstRCReqResFields[RC_RESP_CODE].elementValue) > 0)
		{
			sprintf(szAppLogData, "Response Code From RC Host is %s", pstRCReqResFields[RC_RESP_CODE].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		correctDHIResponseFields(pstDHIReqResFields);

		fillBankUserData(pstRCReqResFields, &pstDHIReqResFields[eBANK_USERDATA].elementValue);

		/* For now, its only for prepaid and EBT transactions */
		if((strlen(pstRCReqResFields[RC_ADDTL_BAL_AMT].elementValue) > 0) && ((strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_TYPE].elementValue, "AvailBal") == SUCCESS) || (strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_TYPE].elementValue, "EndingBal") == SUCCESS)) )
		{
			if(strlen(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE].elementValue))
			{
				if(!strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE].elementValue, "CashBenefit"))
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT].elementValue, &pstDHIReqResFields[eCB_AVAIL_BALANCE].elementValue);
				}
				else if(!strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE].elementValue, "SNAP"))
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT].elementValue, &pstDHIReqResFields[eFS_AVAIL_BALANCE].elementValue);
				}
				else if(!strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE].elementValue, "Prepaid"))
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT].elementValue, &pstDHIReqResFields[eAVAIL_BALANCE].elementValue);
				}
				else
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT].elementValue, &pstDHIReqResFields[eAVAIL_BALANCE].elementValue);
				}
			}
		}
		if((strlen(pstRCReqResFields[RC_ADDTL_BAL_AMT_1].elementValue) > 0) && ((strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_TYPE_1].elementValue, "AvailBal") == SUCCESS) || (strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_TYPE_1].elementValue, "EndingBal") == SUCCESS) ) )
		{
			if(strlen(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE_1].elementValue))
			{
				if(!strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE_1].elementValue, "CashBenefit"))
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT_1].elementValue, &pstDHIReqResFields[eCB_AVAIL_BALANCE].elementValue);
				}
				else if(!strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE_1].elementValue, "SNAP"))
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT_1].elementValue, &pstDHIReqResFields[eFS_AVAIL_BALANCE].elementValue);
				}
				else if(!strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE_1].elementValue, "Prepaid"))
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT_1].elementValue, &pstDHIReqResFields[eAVAIL_BALANCE].elementValue);
				}
				else
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT_1].elementValue, &pstDHIReqResFields[eAVAIL_BALANCE].elementValue);
				}
			}
		}
		if((strlen(pstRCReqResFields[RC_ADDTL_BAL_AMT_2].elementValue) > 0) && ((strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_TYPE_2].elementValue, "AvailBal") == SUCCESS) || (strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_TYPE_2].elementValue, "EndingBal") == SUCCESS)) )
		{
			if(strlen(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE_2].elementValue))
			{
				if(!strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE_2].elementValue, "CashBenefit"))
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT_2].elementValue, &pstDHIReqResFields[eCB_AVAIL_BALANCE].elementValue);
				}
				else if(!strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE_2].elementValue, "SNAP"))
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT_2].elementValue, &pstDHIReqResFields[eFS_AVAIL_BALANCE].elementValue);
				}
				else if(!strcmp(pstRCReqResFields[RC_ADDTL_BAL_AMT_ACC_TYPE_2].elementValue, "Prepaid"))
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT_2].elementValue, &pstDHIReqResFields[eAVAIL_BALANCE].elementValue);
				}
				else
				{
					fillAvailableBalance(pstRCReqResFields[RC_ADDTL_BAL_AMT_2].elementValue, &pstDHIReqResFields[eAVAIL_BALANCE].elementValue);
				}
			}
		}

		/* Filling Internal Sequence Number */
		if(strlen(pstRCReqResFields[RC_STAN].elementValue) > 0)
		{
			fillInternAndTranSeqNum(pstRCReqResFields, pstDHIReqResFields);
		}

		/* If we receive the pinless pos debit flag update dhi structure with it*/
		if(strlen(pstRCReqResFields[RC_PLESS_POS_DEB_FLG].elementValue) > 0)
		{
			if(pstDHIReqResFields[ePINLESSDEBIT].elementValue != NULL)
			{
				strcpy(pstDHIReqResFields[ePINLESSDEBIT].elementValue, pstRCReqResFields[RC_PLESS_POS_DEB_FLG].elementValue);
			}
			else
			{
				pstDHIReqResFields[ePINLESSDEBIT].elementValue = strdup(pstRCReqResFields[RC_PLESS_POS_DEB_FLG].elementValue);
			}
		}
		else if(pstDHIReqResFields[ePINLESSDEBIT].elementValue != NULL)
		{
			//If PIN Less debit was not returned by the host in response and if it was present in DHI field. Then we save it for follow on transactions
			strcpy(pstRCReqResFields[RC_PLESS_POS_DEB_FLG].elementValue,pstDHIReqResFields[ePINLESSDEBIT].elementValue);
		}

		if(strcmp(pstRCReqResFields[RC_PAYMENT_TYPE].elementValue, "Prepaid")) //for non-prepaid transactions we need not do fill the embossed card number
		{
			if(pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue != NULL)
			{
				free(pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue);
				pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue = NULL;
			}
		}
		else
		{
			/*
			 * Currently RC is giving embossed card number only for standard RC cards
			 * Merchant specific cards, we need to calculate this number from the
			 * track data.
			 * Introduced a config variable for this, if this is on calculate
			 * the embossed card num from the track data otherwise
			 * return acctnum field in the RC response as the embossed card num
			 */
			if(isEmbossedCardNumCalcRequired())
			{
				debug_sprintf(szDbgMsg, "%s: Need to calculate embossed card number from the track data", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				/*
				 * Praveen_P1: In the UMF document, extraction of Embossed card number is given only from the
				 * Track2, thats why we are extracting the PAN form the track2 here
				 * Ideally we should not get to the situation, where we have track1 not track2
				 *
				 */

				if(strlen(pstRCReqResFields[RC_TRACK2_DATA].elementValue) > 0)
				{
					memset(szPAN, 0x00, sizeof(szPAN));

					getEmbossedPANFromTrackData(pstRCReqResFields[RC_TRACK2_DATA].elementValue, szPAN);

					if(pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue != NULL)
					{
						free(pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue);
						pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue = NULL;
					}

					iSize = (strlen(szPAN) + 1) * sizeof(char);

					pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue = (char *)malloc(iSize);

					if(pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue != NULL)
					{
						memset(pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue, 0x00, iSize);
						memcpy(pstDHIReqResFields[eEMBOSSED_ACCT_NUM].elementValue, szPAN, strlen(szPAN));
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Track2 data is not present, will not able to calculate Embossed card number", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No Need to calculate embossed card number from the track data", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}

		/* KranthiK1:
		 * Copying the expiry date received from rc only when 4 bytes of data is there
		 */
		if(strlen(pstRCReqResFields[RC_TA_EXP_DATE].elementValue) == 4)
		{
			if(pstDHIReqResFields[eEXP_MONTH].elementValue != NULL)
			{
				free(pstDHIReqResFields[eEXP_MONTH].elementValue);
				pstDHIReqResFields[eEXP_MONTH].elementValue = NULL;
			}

			if(pstDHIReqResFields[eEXP_YEAR].elementValue != NULL)
			{
				free(pstDHIReqResFields[eEXP_YEAR].elementValue);
				pstDHIReqResFields[eEXP_YEAR].elementValue = NULL;
			}

			pstDHIReqResFields[eEXP_MONTH].elementValue = strndup(pstRCReqResFields[RC_TA_EXP_DATE].elementValue, 2);
			pstDHIReqResFields[eEXP_YEAR].elementValue = strndup(pstRCReqResFields[RC_TA_EXP_DATE].elementValue + 2, 2);
		}


		/*PRADEEP:18-11-16: After Parsing the RC Response, now we have to change the amount format from RC to SSI, we will get
		 * all the DHI index where we need to change the amount format, once we get -1 in place of DHI Index it mentions that we changed all
		 * SSI Amount Fields format*/
		piDHIIndexforAmountConvertion = getArrayofDHIIndexforAmtConversion();
		if(piDHIIndexforAmountConvertion == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No fields are present to change amount format ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			while(1)
			{
				iIndex = piDHIIndexforAmountConvertion[iCnt];
				debug_sprintf(szDbgMsg, "%s: index[%d] ", __FUNCTION__, iIndex);
				RCHI_LIB_TRACE(szDbgMsg);
				if(iIndex == -1)
				{
					break;
				}
				else
				{
					if(pstDHIReqResFields[iIndex].elementValue != NULL)
					{
						iValLen = strlen(pstDHIReqResFields[iIndex].elementValue);

						if(strspn(pstDHIReqResFields[iIndex].elementValue, "1234567890-") != iValLen)
						{
							debug_sprintf(szDbgMsg, "%s: Characters other than [0-9] & [-] found, so same value will be sent to SSI Field",
									__FUNCTION__);
							RCHI_LIB_TRACE(szDbgMsg);
						}
						else
						{
							iAmount = atoi(pstDHIReqResFields[iIndex].elementValue);
							fAmount = iAmount/100;
							sprintf(szTemp, "%.02f",fAmount);
							/*Freeing the previously allocated Memory*/
							free(pstDHIReqResFields[iIndex].elementValue);
							pstDHIReqResFields[iIndex].elementValue = NULL;

							pstDHIReqResFields[iIndex].elementValue = strdup((char*)szTemp);
						}
					}
				}
				++iCnt;
			}
		}

		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
		}

		if(pszRCResp != NULL)
		{
			free(pszRCResp);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: parseRCResponseForHostTotals
 *
 * Description	: This function parses the RC Response and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int parseRCResponseForHostTotals(char* pszRCResp, TRANDTLS_PTYPE pstDHIReqResFields, RCTRANDTLS_PTYPE pstRCReqResFields, VAL_HOSTTOTALHEADNODE_PTYPE stHostTotalLst)
{
	int			rv									= SUCCESS;
	int			iLen								= 0;
	int			iCnt								= 0;
	int			iAmount								= 0;
	int			iValLen								= 0;
	int			iIndex								= 0;
	int			iAppLogEnabled						= 0;
	char		szRootName[50]						= "";
	char		szAppLogData[256]					= "";
	char		szTemp[256]							= "";
	xmlDoc *	docPtr								= NULL;
	xmlNode *	rootPtr								= NULL;
	int*		piDHIIndexforAmountConvertion		= NULL;
	double		fAmount								= 0.0;

//	char*		pszRCResp	= "<?xml version=\"1.0\" encoding=\"UTF-8\"?><GMF xmlns=\"com/firstdata/Merchant/gmfV3.10\"><DebitResponse><CommonGrp><PymtType>Debit</PymtType><TxnType>Sale</TxnType><LocalDateTime>20141121094643</LocalDateTime><TrnmsnDateTime>20141121094643</TrnmsnDateTime><STAN>000001</STAN><RefNum>000000000001</RefNum><OrderNum>000000000001</OrderNum><TermID>00000001</TermID><MerchID>8961</MerchID><TxnAmt>1072</TxnAmt><TxnCrncy>840</TxnCrncy></CommonGrp><TAGrp><Tkn>7149893917395556</Tkn><TAExpDate>0416</TAExpDate></TAGrp><DebitGrp><BillingInd>V</BillingInd></DebitGrp><RespGrp><RespCode>000</RespCode><AuthID>019913</AuthID><AddtlRespData>APPROVED</AddtlRespData><SttlmDate>1219</SttlmDate><AthNtwkID>28</AthNtwkID></RespGrp></DebitResponse></GMF>";

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(pszRCResp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Response to Parse!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(pszRCResp);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		if(strlen(pszRCResp) < 4500)
		{
			debug_sprintf(szDbgMsg, "%s: %s", __FUNCTION__, pszRCResp);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif

		docPtr = xmlParseMemory(pszRCResp, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		strcpy(szRootName, (char *)rootPtr->name);

		debug_sprintf(szDbgMsg, "%s: The root name of the RC XML Response is %s",__FUNCTION__, szRootName);
		RCHI_LIB_TRACE(szDbgMsg);

		/* Calling a recursive function which parses through the xml and fills all the response details*/
		parseAndFillDataStructForHostTotals(rootPtr, pstDHIReqResFields, pstRCReqResFields, stHostTotalLst);

		if(iAppLogEnabled && strlen(pstRCReqResFields[RC_RESP_CODE].elementValue) > 0)
		{
			sprintf(szAppLogData, "Response Code From RC Host is %s", pstRCReqResFields[RC_RESP_CODE].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		correctDHIResponseFields(pstDHIReqResFields);

		/*PRADEEP:18-11-16: After Parsing the RC Response, now we have to change the amount format from RC to SSI, we will get
		 * all the DHI index where we need to change the amount format, once we get -1 in place of DHI Index it mentions that we changed all
		 * SSI Amount Fields format*/
		piDHIIndexforAmountConvertion = getArrayofDHIIndexforAmtConversion();
		if(piDHIIndexforAmountConvertion == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No fields are present to change amount format ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			while(1)
			{
				iIndex = piDHIIndexforAmountConvertion[iCnt];
				if(iIndex == -1)
				{
					break;
				}
				else
				{
					if(pstDHIReqResFields[iIndex].elementValue != NULL)
					{
						iValLen = strlen(pstDHIReqResFields[iIndex].elementValue);

						if(strspn(pstDHIReqResFields[iIndex].elementValue, "1234567890-") != iValLen)
						{
							debug_sprintf(szDbgMsg, "%s: Characters other than [0-9] & [-] found, so same value will be sent to SSI Field",
									__FUNCTION__);
							RCHI_LIB_TRACE(szDbgMsg);
						}
						else
						{
							iAmount = atoi(pstDHIReqResFields[iIndex].elementValue);
							fAmount = iAmount/100;
							sprintf(szTemp, "%.02f",fAmount);
							/*Freeing the previously allocated Memory*/
							free(pstDHIReqResFields[iIndex].elementValue);
							pstDHIReqResFields[iIndex].elementValue = NULL;

							pstDHIReqResFields[iIndex].elementValue = strdup((char*)szTemp);
						}
					}
				}
				++iCnt;
			}
		}


		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
		}

		if(pszRCResp != NULL)
		{
			free(pszRCResp);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillRCDataStruct
 *
 * Description	: This function fills/stores the value at the corresponding
 * 				  index in the data structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int fillRCDataStruct(char *pszTagName, char *pszTagValue, TRANDTLS_PTYPE pstDHIReqResFields, RCTRANDTLS_PTYPE pstRCReqResFields)
{
	int							rv						= SUCCESS;
	int							iRCIndex				= -1;
	int							iDHIIndex				= -1;
	int							iRespCount				= 0;
	HASHLST_PTYPE 				pstHashLst				= NULL;
	RCHI_PASSTHROUGH_PTYPE		pstPTFields				= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if(pszTagName == NULL || pszTagValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Either TagName or TagValue is NULL!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Do the Hash look up for this Tag Name */
		pstHashLst = lookupRCHashTable(pszTagName);
		if(pstHashLst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Did not find entry for this Tag[%s] in the Hash Table!!! So ignoring this tag", __FUNCTION__, pszTagName);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Index for this Tag in the structure is %d", __FUNCTION__, pstHashLst->iTagIndexVal);
		RCHI_LIB_TRACE(szDbgMsg);

		//		if(pstHashLst->iTagIndexVal > eELEMENT_INDEX_MAX) //Safety Check
		//		{
		//			debug_sprintf(szDbgMsg, "%s: Invalid Index Value!!!", __FUNCTION__);
		//			RCHI_LIB_TRACE(szDbgMsg);
		//			rv = FAILURE;
		//			break;
		//			//Should not be the case, something wrong
		//		}

		//		iDHIIndex = genRespLst[pstHashLst->iTagIndexVal].iDHIIndex;
		//		iRCIndex  = genRespLst[pstHashLst->iTagIndexVal].iRCIndex;


		pstPTFields = getPassthroughFields();
		iRespCount = pstPTFields->iRespCnt;

		if(pstHashLst->iTagIndexVal > (RC_ELEMENTS_INDEX_MAX + iRespCount))	//Safety Check
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Index Value!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
			//Should not be the case, something wrong
		}
		else if(pstHashLst->iTagIndexVal > RC_ELEMENTS_INDEX_MAX) //Safety Check
		{
			iDHIIndex 				= pstPTFields->pstPTRespDtls[pstHashLst->iTagIndexVal - (RC_ELEMENTS_INDEX_MAX + 1)].iDHIIndex;
			iRCIndex  				= pstPTFields->pstPTRespDtls[pstHashLst->iTagIndexVal - (RC_ELEMENTS_INDEX_MAX + 1)].iRCIndex;
		}
		else if(pstHashLst->iTagIndexVal == NO_ENUM)
		{
			/*T_POLISETTYG1: In Case of pass through fields,  iTagIndexVal value will be (-1)NO_ENUM if does not have any matching index, so skipping in that case*/
		}
		else
		{
			iDHIIndex 					= genRespLst[pstHashLst->iTagIndexVal].iDHIIndex;
			iRCIndex  					= genRespLst[pstHashLst->iTagIndexVal].iRCIndex;
		}

		/* For Multiple additional groups, in case of EBT Transactions */
		if(!strcmp(pstRCReqResFields[RC_PAYMENT_TYPE].elementValue, "EBT"))
		{
			if(!strcmp(pstRCReqResFields[RC_TRAN_TYPE].elementValue, "BalanceInquiry"))
			{
				if((bFlag == RCHI_FALSE) && (strcmp(pszTagName, "AddAmtAcctType") == SUCCESS))
				{
					bFlag = RCHI_TRUE;
				}
				else if(bFlag == RCHI_TRUE)
				{
					/* Making sure that it is a Additional Amount group */
					if(strcmp(pszTagName, "AddAmt") == SUCCESS)
					{
						iAddAmtIndex++;
					}
					if(iAddAmtIndex)
					{
						iRCIndex = iRCIndex + 4;
						iAddAmtIndex++;

						if(iAddAmtIndex == 5)
						{
							iAddAmtIndex = 0;
							bFlag = RCHI_FALSE;
						}
					}
				}
			}
		}
		else if(!strcmp(pstRCReqResFields[RC_PAYMENT_TYPE].elementValue, "Prepaid"))
		{
			/*AjayS2: 04 May 2016: We will get upto 3 amount groups(4 nodes in each group), After 1st we will
			 *  increase RC index by 4. Also AddAmtAcctType is last element in a group and AddAmt is first.
			 *  Fixed JIRA PTMX 1304: When configured as First Data Direct with Valuelink Gift, Point is not returning the Available Balance back to the POS on a Balance command.
			 */
			if((bFlag == RCHI_FALSE) && (strcmp(pszTagName, "AddAmtAcctType") == SUCCESS))
			{
				bFlag = RCHI_TRUE;
			}
			else if(bFlag == RCHI_TRUE)
			{
				if((strcmp(pszTagName, "AddAmt") == SUCCESS) || (strcmp(pszTagName, "AddAmtAcctType") == SUCCESS) || (strcmp(pszTagName, "AddAmtCrncy") == SUCCESS) || (strcmp(pszTagName, "AddAmtType") == SUCCESS) )
				{
					/* Making sure that it is a Additional Amount group */
					if(strcmp(pszTagName, "AddAmt") == SUCCESS)
					{
						iAddAmtIndex++;
					}
					if(iAddAmtIndex)
					{

						iRCIndex = iRCIndex + 4*iAddAmtIndex;
						debug_sprintf(szDbgMsg, "%s: Changing RC Index to %d since iAddAmtIndex :%d", __FUNCTION__, iRCIndex,iAddAmtIndex);
						RCHI_LIB_TRACE(szDbgMsg);

						if(iAddAmtIndex >= 2 && (strcmp(pszTagName, "AddAmtAcctType") == SUCCESS))
						{
							iAddAmtIndex = 0;
							bFlag = RCHI_FALSE;
						}
					}
				}
			}
		}

		//debug_sprintf(szDbgMsg, "%s: Length of the tagvalue is %d", __FUNCTION__, iValLen);
		//RCHI_LIB_TRACE(szDbgMsg);

		if(iDHIIndex != NO_ENUM)
		{
			/* Allocate memory at the corresponding index in the data structure */

			/*	pstDHIReqResFields[iDHIIndex].elementValue = (char *)malloc(iValLen + 1 * sizeof(char));
			if(pstDHIReqResFields[iDHIIndex].elementValue == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory Allocation failed!!!", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			memset(pstDHIReqResFields[iDHIIndex].elementValue, 0x00, (iValLen + 1 * sizeof(char)));*/

			if(pstDHIReqResFields[iDHIIndex].elementValue != NULL)
			{
				free(pstDHIReqResFields[iDHIIndex].elementValue);
				pstDHIReqResFields[iDHIIndex].elementValue = NULL;
			}

			/* Copy the tagvalue to the data structure */
			pstDHIReqResFields[iDHIIndex].elementValue = strdup((char*)pszTagValue);
			//	strcpy(pstDHIReqResFields[iDHIIndex].elementValue, pszTagValue);
		}

		if(iRCIndex != NO_ENUM)
		{

			memset(pstRCReqResFields[iRCIndex].elementValue, 0x00, sizeof(pstRCReqResFields[iRCIndex].elementValue));
			strncpy(pstRCReqResFields[iRCIndex].elementValue, pszTagValue, MAX_SIZE - 1);
			//sprintf(pstRCReqResFields[iRCIndex].elementValue,"%.*s", MAX_SIZE-1, pszTagValue);
		}

		break;
	}


//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNodeValFromDWRegResp
 *
 * Description	: This function extracts the values from the DW Reg response
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getNodeValFromDWRegResp(char* pszDWResp, char** pszNodeNames, char** pszNodeValues, int iNodeCount, char* szStatMsg)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iCount				= 0;
	int			iAppLogEnabled		= 0;
	char		szRootName[50]		= "";
	char		szAppLogData[256]	= "";
	xmlDoc  *	docPtr				= NULL;
	xmlNode *	rootPtr				= NULL;
	xmlNode *	nodePtr				= NULL;
	xmlNode *	statusNode			= NULL;
	xmlChar * 	nodeValue			= NULL;
	xmlChar *	statusCode			= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(pszDWResp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Response to Parse!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(pszDWResp);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);

		docPtr = xmlParseMemory(pszDWResp, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}
		strcpy(szRootName, (char *)rootPtr->name);

		debug_sprintf(szDbgMsg, "%s: The root name of the RC XML Response is %s",__FUNCTION__, szRootName);
		RCHI_LIB_TRACE(szDbgMsg);

		/*
		 * First find the status code returned by the datawire server if the status retunred is OK then search for Payload in the xml
		 * Else return error
		 */
		statusNode = findNodeInXML("Status", rootPtr);
		if(statusNode != NULL)
		{
			statusCode = xmlGetProp(statusNode, (const xmlChar *) "StatusCode");

			if(iAppLogEnabled && statusCode != NULL)
			{
				sprintf(szAppLogData, "Status Code From Datawire Host is %s", (char*)statusCode);
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			if(statusCode != NULL && strcmp((char*)statusCode, "OK") == SUCCESS)
			{
				nodePtr = statusNode->next;
				for(iCount = 0; iCount < iNodeCount; iCount++)
				{
					nodePtr = findNodeInXML( pszNodeNames[iCount], nodePtr);
					if(nodePtr != NULL)
					{
						/*Getting the content of the xml node*/
						debug_sprintf(szDbgMsg, "%s:Name of this node is %s", __FUNCTION__, (char *)nodePtr->name);
						RCHI_LIB_TRACE(szDbgMsg);

						nodeValue = xmlNodeGetContent(nodePtr);
						if(nodeValue != NULL)
						{
						#ifdef DEVDEBUG
							if(strlen((char*)nodeValue) < 4500)
							{
								debug_sprintf(szDbgMsg, "%s:%s: Node Name [%s], Node Value[%s]", DEVDEBUGMSG, __FUNCTION__, (char *)nodePtr->name, (char *)nodeValue);
								RCHI_LIB_TRACE(szDbgMsg);
							}
						#endif

							pszNodeValues[iCount] = strdup((char*)nodeValue);

							xmlFree(nodeValue);
							nodeValue = NULL;
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: No Value for this node", __FUNCTION__);
							RCHI_LIB_TRACE(szDbgMsg);
						}
						nodePtr = nodePtr->next;
					}
					else
					{
						rv = ERR_INV_RESP;
						break;
					}
				}

				xmlFree(statusCode);
			}
			else
			{
				rv = ERR_HOST_ERROR;

				strcpy(szStatMsg, (char*)statusCode);
				if(statusCode != NULL)
				{
					xmlFree(statusCode);
				}

				nodeValue = xmlNodeGetContent(statusNode);
				if(nodeValue != NULL)
				{
					if(strlen((char*)nodeValue) > 0)
					{
						sprintf(szStatMsg, "%s: %s", szStatMsg, (char*)nodeValue);
					}
					xmlFree(nodeValue);
					nodeValue = NULL;
				}
			}
		}

		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNodeValFromDWResp
 *
 * Description	: This function extracts the payload from the DW response
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getNodeValFromDWResp(char* pszDWResp, char** pszNodeNames, char** pszNodeValues, int iNodeCount, char* szStatMsg)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iCount				= 0;
	int			iDWReturnCode		= -1;
	int			iAppLogEnabled		= 0;
	char		szRootName[50]		= "";
	char		szAppLogData[256]	= "";
	xmlDoc  *	docPtr				= NULL;
	xmlNode *	rootPtr				= NULL;
	xmlNode *	nodePtr				= NULL;
	xmlNode *	statusNode			= NULL;
	xmlNode *	dwReturnCode		= NULL;
	xmlChar * 	nodeValue			= NULL;
	xmlChar *	statusCode			= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(pszDWResp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Response to Parse!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(pszDWResp);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);

		docPtr = xmlParseMemory(pszDWResp, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}
		strcpy(szRootName, (char *)rootPtr->name);

		debug_sprintf(szDbgMsg, "%s: The root name of the RC XML Response is %s",__FUNCTION__, szRootName);
		RCHI_LIB_TRACE(szDbgMsg);

		/*
		 * First find the status code returned by the datawire server if the status retunred is OK then search for Payload in the xml
		 * Else return error
		 */
		statusNode = findNodeInXML("Status", rootPtr);
		if(statusNode != NULL)
		{
			statusCode = xmlGetProp(statusNode, (const xmlChar *) "StatusCode");

			if(iAppLogEnabled && statusCode != NULL)
			{
				sprintf(szAppLogData, "Status Code From Datawire Host is %s", (char*)statusCode);
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			if(statusCode != NULL && strcmp((char*)statusCode, "OK") == SUCCESS)
			{
				nodePtr = statusNode->next;

				dwReturnCode = findNodeInXML("ReturnCode", nodePtr);
				if(dwReturnCode != NULL)
				{
					nodeValue = xmlNodeGetContent(dwReturnCode);
					if(nodeValue != NULL)
					{
						iDWReturnCode = atoi((char*)nodeValue);
						xmlFree(nodeValue);
						nodeValue =  NULL;
					}
					nodePtr = dwReturnCode->next;
				}

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Return Code From Datawire Host is %d ", iDWReturnCode);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				if(iDWReturnCode == 0)
				{
					for(iCount = 0; iCount < iNodeCount; iCount++)
					{
						nodePtr = findNodeInXML( pszNodeNames[iCount], nodePtr);
						if(nodePtr != NULL)
						{
							/*Getting the content of the xml node*/
							debug_sprintf(szDbgMsg, "%s:Name of this node is %s", __FUNCTION__, (char *)nodePtr->name);
							RCHI_LIB_TRACE(szDbgMsg);

							nodeValue = xmlNodeGetContent(nodePtr);
							if(nodeValue != NULL)
							{
							#ifdef DEVDEBUG
								if(strlen((char*)nodeValue) < 4500)
								{
									debug_sprintf(szDbgMsg, "%s:%s: Node Name [%s], Node Value[%s]", DEVDEBUGMSG, __FUNCTION__, (char *)nodePtr->name, (char *)nodeValue);
									RCHI_LIB_TRACE(szDbgMsg);
								}
							#endif

								pszNodeValues[iCount] = strdup((char*)nodeValue);

								xmlFree(nodeValue);
								nodeValue = NULL;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: No Value for this node", __FUNCTION__);
								RCHI_LIB_TRACE(szDbgMsg);
							}
							nodePtr = nodePtr->next;
						}
						else
						{
							rv = ERR_INV_RESP;
							break;
						}
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s:Received Failure error code from DW [%d]", __FUNCTION__, iDWReturnCode);
					RCHI_LIB_TRACE(szDbgMsg);

					switch(iDWReturnCode)
					{
					case ERR_DW_CONN_ERROR:
					case ERR_DW_HOST_BUSY:
					case ERR_DW_HOST_UNAVAILABLE:
						rv = ERR_HOST_NOT_AVAILABLE;
						break;
					case ERR_DW_HOST_COMM_ERROR:
					case ERR_DW_HOST_DROP:
					case ERR_DW_HOST_SEND_ERROR:
					case ERR_DW_NETWORK_ERROR1:
					case ERR_DW_NETWORK_ERROR2:
					case ERR_DW_NO_RESPONSE:
					case ERR_DW_TIME_OUT:
						rv = ERR_RESP_TO;
						break;
					default:
						rv = FAILURE;
					}
				}
				xmlFree(statusCode);
			}
			else
			{
				rv = ERR_HOST_ERROR;

				strcpy(szStatMsg, (char*)statusCode);
				if(statusCode != NULL)
				{
					xmlFree(statusCode);
				}

				nodeValue = xmlNodeGetContent(statusNode);
				if(nodeValue != NULL)
				{
					if(strlen((char*)nodeValue) > 0)
					{
						sprintf(szStatMsg, "%s: %s", szStatMsg, (char*)nodeValue);
					}
					xmlFree(nodeValue);
					nodeValue = NULL;
				}
			}
		}

		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillErrorDtls
 *
 * Description	: This function is responsible for filling error details to DHI on occurence
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void fillErrorDtls(int iErrCode, TRANDTLS_PTYPE pstDHIReqResFields, char* szStagMsg)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	char 	szTmpVal[50];

	int iRetHostInfo = 0;

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	switch(iErrCode)
	{
	case ERR_UNSUPP_CMD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59006");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Command Not Supported by Processor");
		break;

	case ERR_INV_XML_MSG:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59027");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("XML Error");
		break;

	case ERR_RESP_TO:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("23");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Response Timeout");
		iRetHostInfo = 1;
		break;

	case ERR_CONN_TO:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Connection Timeout");
		iRetHostInfo = 1;
		break;

	case ERR_CONN_FAIL:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Connection Disrupted");
		iRetHostInfo = 1;
		break;

	case ERR_HOST_TRAN_REVERSED:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("22");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Transaction Reversed");
		iRetHostInfo = 1;
		break;

	case ERR_HOST_TRAN_TIMEOUT:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("23");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Transaction Timed out");
		iRetHostInfo = 1;
		break;

	case ERR_HOST_NOT_AVAILABLE:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable");
		iRetHostInfo = 1;
		break;

	case ERR_CFG_PARAMS:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Configuration Error");
		break;

	case ERR_INV_COMBI:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59041");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Invalid Combination");
		break;

	case ERR_FLD_REQD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59041");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Field is Required");
		break;

	case ERR_INV_FLD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59043");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Field is Invalid");
		break;

	case ERR_NO_FLD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59045");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Field Does Not Exist");
		break;

	case ERR_INV_FLD_VAL:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59045");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Value Not Valid For Field");
		break;

	case ERR_INV_RESP:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("HOST_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Invalid Response From Host");
		break;

	case ERR_HOST_ERROR:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup(szStagMsg);
		break;

	case ERR_INV_CTROUTD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("CTROUTD Not Found in the Current Batch");
		break;

	case ERR_TOKEN_NOT_FOUND:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("TOKEN Not Found in the Response");
		break;
	case ERR_SYSTEM:
	case FAILURE:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59020");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("INTERNAL_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("System Error");
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Invalid error code ", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}
	/* It is required to return Merchant ID and terminal ID even in case of error.
	 * Hence populating the same from the configured values.
	 */
	/* Get Terminal ID */
	if(iRetHostInfo)
	{
		memset(szTmpVal,0x00,sizeof(szTmpVal));
		getMerchantID(szTmpVal);
		pstDHIReqResFields[eMERCHID].elementValue = strdup(szTmpVal);

		/* Get merchant ID */
		memset(szTmpVal,0x00,sizeof(szTmpVal));
		getTerminalID(szTmpVal);
		pstDHIReqResFields[eTERMID].elementValue = strdup(szTmpVal);
	}
	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: fillHostTotalNode
 *
 * Description	: Getter function for the result code
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillHostTotalNode(xmlNodePtr node, VAL_HOSTTOTALNODE_PTYPE HostTotalDetails)
{
	int	rv	= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	//Base case of the recursive function checking for null
	if(node == NULL)
	{
		return ERR_NO_FLD;
		debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Node Name[%s] Node type[%d] children[%p] childrencount [%ld]", __FUNCTION__, node->name, node->type, node->children, xmlChildElementCount(node));
	RCHI_LIB_TRACE(szDbgMsg);

	if(xmlChildElementCount(node) == 0 )
	{
		rv = getCardTag(node, &HostTotalDetails->pszCardTag);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Could not obtain card tag", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			return rv;
		}
		node = node->next;
		rv = getTransCount(node, &HostTotalDetails->pszTransCnt );
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Could not obtain card transaction count", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			return rv;
		}
		node = node->next;
		rv = getCardTransAmt(node, &HostTotalDetails->pszTansAmt);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Could not obtain card transaction amount", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			return rv;
		}

	}
	HostTotalDetails->next = NULL;
	return rv;
}

/*
 * ============================================================================
 * Function Name: getCardTag
 *
 * Description	: Getter function for the result code
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCardTag(xmlNodePtr node, char **pszCardTag)
{
	xmlChar*	value	= NULL;
	int 		rv	 	= SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	value = xmlNodeGetContent(node);
	/* If the node is an element node then filling the node and the content in the datastructure*/
	if((char*) node->name == NULL || (char*)value == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Either TagName or TagValue is NULL!!!", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	*pszCardTag = strdup((char*)value);

	if(value != NULL)
	{
		xmlFree(value);
		value = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: --- Returning --- [%s]", __FUNCTION__, *pszCardTag);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;

}
/*
 * ============================================================================
 * Function Name: getTransCount
 *
 * Description	: Getter function for the result code
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getTransCount(xmlNodePtr node, char **pszTransCnt)
{
	xmlChar*	value	= NULL;
	int			rv		= SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	value = xmlNodeGetContent(node);
	/* If the node is an element node then filling the node and the content in the datastructure*/
	if((char*) node->name == NULL || (char*)value == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Either TagName or TagValue is NULL!!!", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	*pszTransCnt = strdup((char*)value);

	if(value != NULL)
	{
		xmlFree(value);
		value = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: ---Returning ---[%s]", __FUNCTION__, *pszTransCnt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getCardTransAmt
 *
 * Description	: Getter function for the result code
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCardTransAmt(xmlNodePtr node, char **pszTransAmt)
{
	xmlChar*	value 	= NULL;
	int 		rv		= SUCCESS;
	float		fAmount	= 0.0;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	value = xmlNodeGetContent(node);
	/* If the node is an element node then filling the node and the content in the datastructure*/
	if((char*) node->name == NULL || (char*)value == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Either TagName or TagValue is NULL!!!", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	*pszTransAmt = strdup((char*)value);
	/* Modifying the approved amount matching SSI format*/
	if(*pszTransAmt != NULL)
	{
		fAmount = atof(*pszTransAmt);
		fAmount	= fAmount/100;

		sprintf(*pszTransAmt, "%.02f", fAmount);
	}
	if(value != NULL)
	{
		xmlFree(value);
		value = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning --- [%s]", __FUNCTION__, *pszTransAmt);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: addNode
 *
 * Description	: Getter function for the result code
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
VAL_HOSTTOTALNODE_PTYPE addNode(VAL_HOSTTOTALNODE_PTYPE pstHead,VAL_HOSTTOTALNODE_PTYPE node)
{
	VAL_HOSTTOTALNODE_PTYPE temp = NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif


	temp = pstHead;
	if(temp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Adding first node", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		temp = node;
		return temp;
	}
	while(temp->next != NULL)
	{
		temp = temp->next;
	}

	temp->next = node;

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return pstHead;
}

/* NEED TO REMOVE THIS */
void display(VAL_HOSTTOTALNODE_PTYPE head)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(head == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Empty LL", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

	}
	else
	{
		while(head != NULL)
		{
			debug_sprintf(szDbgMsg, "%s:Card Tag[%s], TransAmt[%s] and TransCount[%s]", __FUNCTION__,head->pszCardTag, head->pszTansAmt, head->pszTransCnt );
			RCHI_LIB_TRACE(szDbgMsg);
			head = head->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s:Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

}
/*
 * ============================================================================
 * Function Name: frameSSIRespForCutOverOrSiteTotals
 *
 * Description	: Getter function for the result code
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int frameSSIRespForCutOverOrSiteTotals(TRANDTLS_PTYPE pstSSIReqResFields, VAL_HOSTTOTALHEADNODE_PTYPE stHostTotalLst)
{
	int						rv					= SUCCESS;
	int						iAppLogEnabled		= 0;
	int						iCnt				= 0;
	int						iSize				= 0;
	int						iCount				= 0;
	int						iPTRespCnt			= 0;
	int						iDHIIndex			= -1;
	xmlDocPtr				docPtr				= NULL;
	xmlNodePtr				rootPtr				= NULL;
	xmlNodePtr				recordsPtr			= NULL;
	xmlNodePtr				recordPtr			= NULL;
	xmlNodePtr				elemNodePtr			= NULL;
	char					szAppLogData[256]	= "";
	char					szAppLogDiag[256]	= "";
	char*					pszRCHIResp			= NULL;
	PASSTHROUGH_PTYPE		pstDHIPTLst 		= getDHIPassThroughDtls();
	VAL_HOSTTOTALNODE_PTYPE pstTempHead 		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Host Total Command Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtlsForSSI(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstSSIReqResFields[eCOMMAND].elementValue != NULL)
		{
			RCHI_LIB_TRACE("Adding Command"); //Praveen_P1: testing purpose please remove it

			elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "COMMAND", BAD_CAST pstSSIReqResFields[eCOMMAND].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding COMMAND field", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
		else
		{
			RCHI_LIB_TRACE("Not Adding Command"); //Praveen_P1: testing purpose please remove it
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		//Create the root node
		//recordsPtr = xmlNewNode(NULL, BAD_CAST "RECORDS");
		/* HOST TOTAL DETAILS GROUP */
		/* NEED TO REVIEW THIS */

		pszRCHIResp = (char *)malloc(NUMBER_CONVERION);
		if(pszRCHIResp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;

			return rv;
		}

		memset(pszRCHIResp,0x00,NUMBER_CONVERION);
		sprintf(pszRCHIResp,"%d", stHostTotalLst->iMaxHostTotalGroup);

		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "MAX_NUM_RECORDS_RETURNED", BAD_CAST pszRCHIResp);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMINATION_STATUS field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		/* Free the temporary memory used for storing the number as string */
		free(pszRCHIResp);

		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "NET_SETTLE_AMT", BAD_CAST stHostTotalLst->szNetSettlemntAmt);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMINATION_STATUS field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		/* Creating RECORDS tree */
		recordsPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "RECORDS", NULL);
		if(recordsPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create records", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			return rv;
		}
		pstTempHead = stHostTotalLst->pstHead;
		if(pstTempHead != NULL)
		{
			for(iCnt = 0; iCnt < stHostTotalLst->iMaxHostTotalGroup;iCnt++)

			{
				recordPtr = xmlNewChild(recordsPtr, NULL, BAD_CAST "RECORD",NULL);
				if(recordPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create record", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					return rv;
				}

				elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "CARDTAG", BAD_CAST pstTempHead->pszCardTag);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create CARDTAG", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					return rv;
				}
				elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "ITEMCOUNT", BAD_CAST pstTempHead->pszTransCnt);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create ITEMCOUNT", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					return rv;
				}
				elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "ITEMAMOUNT", BAD_CAST pstTempHead->pszTansAmt);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create ITEMAMOUNT", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					return rv;
				}

				pstTempHead = pstTempHead->next;

			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Records not present", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

		}

		/* PRADEEP: Adding Pass through Details to SSI Request*/
		iPTRespCnt = pstDHIPTLst->iRespXMLTagsCnt;

		debug_sprintf(szDbgMsg, "%s: iPTRespCnt [%d]", __FUNCTION__, iPTRespCnt);
		RCHI_LIB_TRACE(szDbgMsg);

		if(iPTRespCnt > 0)
		{
			for(iCount = 0; iCount < iPTRespCnt; iCount++)
			{
				iDHIIndex = pstDHIPTLst->pstPTXMLRespDtls[iCount].index;

				if(iDHIIndex != -1 && (pstDHIPTLst->pstPTXMLRespDtls[iCount].key != NULL) && (strlen(pstDHIPTLst->pstPTXMLRespDtls[iCount].key) > 0)
						&& (pstSSIReqResFields[iDHIIndex].elementValue != NULL) && (strlen(pstSSIReqResFields[iDHIIndex].elementValue) > 0))
				{
					elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST pstDHIPTLst->pstPTXMLRespDtls[iCount].key, BAD_CAST pstSSIReqResFields[iDHIIndex].elementValue);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while adding Pass Through field", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
					}
				}
			}
		}
		xmlDocDumpFormatMemory(docPtr, (xmlChar **)&pstSSIReqResFields[eREPORT_RESPONSE].elementValue, &iSize, 1);

		iCnt = strlen(pstSSIReqResFields[eREPORT_RESPONSE].elementValue);

		debug_sprintf(szDbgMsg, "%s: Length is: %d", __FUNCTION__, iCnt );
		RCHI_LIB_TRACE(szDbgMsg);

		if(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue == NULL)
		{
			pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue = (char *)malloc(NUMBER_CONVERION);
			if(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;

				return rv;
			}

			memset(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue,0x00,NUMBER_CONVERION);
			sprintf(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue,"%d", iSize);

		}

		RCHI_LIB_TRACE(pstSSIReqResFields[eREPORT_RESPONSE].elementValue);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);

		}

		/*if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);

		}*/

		break;
	}

	if(stHostTotalLst->pstHead != NULL)
	{
		pstTempHead = stHostTotalLst->pstHead;

		while(pstTempHead != NULL)
		{
			free(pstTempHead->pszCardTag);
			free(pstTempHead->pszTansAmt);
			free(pstTempHead->pszTransCnt);
			pstTempHead = pstTempHead->next;
		}
		free(stHostTotalLst->pstHead);

	}
	if(stHostTotalLst != NULL)
	{
		free(stHostTotalLst);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: buildGenRespDtls
 *
 * Description	: This function adds the general response details to the
 * 				  xml doc
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildGenRespDtlsForSSI(xmlDocPtr docPtr, xmlNode * rootNode, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv				= SUCCESS;
	xmlNodePtr		elemNodePtr		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(docPtr == NULL || rootNode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param(docPtr/rootNode) is NULL", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Adding TERMINATION_STATUS field */
	if(pstSSIReqResFields[eTERMINATION_STATUS].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST pstSSIReqResFields[eTERMINATION_STATUS].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMINATION_STATUS field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}
	else
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "SUCCESS");
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMINATION_STATUS field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding RESULT field */
	if(pstSSIReqResFields[eRESULT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT", BAD_CAST pstSSIReqResFields[eRESULT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}
	else
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT", BAD_CAST "OK");
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding RESULT_CODE field */
	if(pstSSIReqResFields[eRESULT_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT_CODE", BAD_CAST pstSSIReqResFields[eRESULT_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT_CODE field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}
	else
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "-1");
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT_CODE field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding RESPONSE_TEXT field */
	if(pstSSIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST pstSSIReqResFields[eRESPONSE_TEXT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESPONSE_TEXT field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding eHOST_RESPCODE field */
	if(pstSSIReqResFields[eHOST_RESPCODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST pstSSIReqResFields[eHOST_RESPCODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding HOST_RESPCODE field", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: fillAvailableBalance
 *
 * Description	:
 *
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static void fillAvailableBalance(char *pszRCAvailBal, char **pszDHIAvailBal)
{
	float fAmount	=	0.0;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Converting balance amount to SSI type */
	fAmount = atof(pszRCAvailBal);
	fAmount	= fAmount/100;

	sprintf(pszRCAvailBal, "%.02f", fAmount);

	*pszDHIAvailBal = strdup(pszRCAvailBal);

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, pszRCAvailBal);
	RCHI_LIB_TRACE(szDbgMsg);

}
/*
 * ============================================================================
 * Function Name: fillInternSeqNum
 *
 * Description	:
 *
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static void fillInternAndTranSeqNum(RCTRANDTLS_PTYPE pstRCReqResFields, TRANDTLS_PTYPE pstDHIReqResFields)
{

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pstDHIReqResFields[eINTRN_SEQ_NUM].elementValue = strdup(pstRCReqResFields[RC_STAN].elementValue);
	pstDHIReqResFields[eTRANS_SEQ_NUM].elementValue = strdup(pstRCReqResFields[RC_STAN].elementValue);

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

}
/*
 * ============================================================================
 * Function Name: frameSSIRespForEMVAdmin
 *
 * Description	: This function frames the SSI Response for EMV Admin Request.
 *
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int frameSSIRespForEMVAdmin(TRANDTLS_PTYPE pstSSIReqResFields)
{
		int						rv						= SUCCESS;
		int						iAppLogEnabled			= 0;
		int						iCnt					= 0;
		int						iSize					= 0;
		int						iCount					= 0;
		int						iPTRespCnt				= 0;
		int						iDHIIndex				= -1;
		xmlDocPtr				docPtr					= NULL;
		xmlNodePtr				rootPtr					= NULL;
		xmlNodePtr				pKeysLstPtr				= NULL;
		xmlNodePtr				recordPtr				= NULL;
		xmlNodePtr				elemNodePtr				= NULL;
		char					szAppLogData[256]		= "";
		char					szAppLogDiag[256]		= "";
		PASSTHROUGH_PTYPE		pstDHIPTLst 			= getDHIPassThroughDtls();
		VAL_EMVADMINNODE_PTYPE 	pstTempHead 			= gstEMVAdmin.pstKeysLst;
		VAL_EMVADMINNODE_PTYPE	pstDelNode				= NULL;

		char*					pkFieldNames[6] 		=  {"PUBLIC_KEY_0","PUBLIC_KEY_1","PUBLIC_KEY_2","PUBLIC_KEY_3","PUBLIC_KEY_4","PUBLIC_KEY_5"};


	#ifdef DEBUG
		char			szDbgMsg[2048]	= "";
	#endif

		debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

		iAppLogEnabled = isAppLogEnabled();

		pstSSIReqResFields[eRESULT].elementValue = strdup("APPROVED");
		pstSSIReqResFields[eRESULT_CODE].elementValue = strdup("5");
		pstSSIReqResFields[eRESPONSE_TEXT].elementValue = strdup("APPROVED");
		while(1)
		{
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Framing EMV Admin Command Response");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			//Create the XML doc
			docPtr = xmlNewDoc(BAD_CAST "1.0");
			if(docPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			//Create the root node
			rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
			if(rootPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			xmlDocSetRootElement(docPtr, rootPtr);

			rv = buildGenRespDtlsForSSI(docPtr, rootPtr, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			if(pstSSIReqResFields[eTROUTD].elementValue)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "TROUTD", BAD_CAST pstSSIReqResFields[eTROUTD].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding TROUTD field", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
			}

			if(pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "INTRN_SEQ_NUM", BAD_CAST pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding INTRN_SEQ_NUM field", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
			}
			if(pstSSIReqResFields[eTRANS_SEQ_NUM].elementValue)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "TRANS_SEQ_NUM", BAD_CAST pstSSIReqResFields[eTRANS_SEQ_NUM].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding TRANS_SEQ_NUM field", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
				}
			}

			elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "DOWNLOAD_STATUS", BAD_CAST gstEMVAdmin.szDwnldStat);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create DOWNLOAD_STATUS", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				return rv;
			}
			/* Creating PUBLIC_KEYS Tree */
			pKeysLstPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "PUBLIC_KEY", NULL);
			if(pKeysLstPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				return rv;
			}

			if(pstTempHead != NULL)
			{
				for(iCnt = 0;pstTempHead != NULL;iCnt++)
				{
					recordPtr = xmlNewChild(pKeysLstPtr, NULL, BAD_CAST pkFieldNames[iCnt],NULL);
					if(recordPtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_CHECKSUM", BAD_CAST pstTempHead->pszPkCkSum);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_CHECKSUM", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_EXP_LEN", BAD_CAST pstTempHead->pszPkExpLen);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_EXP_LEN", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "APP_ID", BAD_CAST pstTempHead->pszRID);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create APP_ID", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_MOD", BAD_CAST pstTempHead->pszPkMod);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_MOD", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_CHECKSUM_LEN", BAD_CAST pstTempHead->pszPkCkSumLen);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_CHECKSUM_LEN", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_IND", BAD_CAST "PK");
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_IND", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_MOD_LEN", BAD_CAST pstTempHead->pszPkModLen);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_MOD_LEN", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_EXP", BAD_CAST pstTempHead->pszPkExp);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_EXP", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}


					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_INDEX", BAD_CAST pstTempHead->pszPkIndex);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_INDEX", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}

					elemNodePtr = xmlNewChild(recordPtr, NULL, BAD_CAST "PUBLIC_KEY_EXP_DATE", BAD_CAST pstTempHead->pszPkExpDate);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create PUBLIC_KEY_EXP_DATE", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						rv = FAILURE;
						return rv;
					}
					pstDelNode = pstTempHead;
					gstEMVAdmin.pstKeysLst = pstTempHead = pstTempHead->next;
					delEMVAdminNode(pstDelNode);

					gstEMVAdmin.iMaxPublicKeys--;
				}
				gstEMVAdmin.pstKeysLst = NULL;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Records not present", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			break;
		}

		/* PRADEEP: Adding Pass through Details to SSI Request*/
		iPTRespCnt = pstDHIPTLst->iRespXMLTagsCnt;

		debug_sprintf(szDbgMsg, "%s: iPTRespCnt [%d]", __FUNCTION__, iPTRespCnt);
		RCHI_LIB_TRACE(szDbgMsg);

		if(iPTRespCnt > 0)
		{
			for(iCount = 0; iCount < iPTRespCnt; iCount++)
			{
				iDHIIndex = pstDHIPTLst->pstPTXMLRespDtls[iCount].index;

				if(iDHIIndex != -1 && (pstDHIPTLst->pstPTXMLRespDtls[iCount].key != NULL) && (strlen(pstDHIPTLst->pstPTXMLRespDtls[iCount].key) > 0)
						&& (pstSSIReqResFields[iDHIIndex].elementValue != NULL) && (strlen(pstSSIReqResFields[iDHIIndex].elementValue) > 0))
				{
					elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST pstDHIPTLst->pstPTXMLRespDtls[iCount].key, BAD_CAST pstSSIReqResFields[iDHIIndex].elementValue);
					if(elemNodePtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while adding Pass Through field", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
					}
				}
			}
		}
		xmlDocDumpFormatMemory(docPtr, (xmlChar **)&pstSSIReqResFields[eEMVADMIN_RESPONSE].elementValue, &iSize, 1);

		iCnt = strlen(pstSSIReqResFields[eEMVADMIN_RESPONSE].elementValue);

		debug_sprintf(szDbgMsg, "%s: Length is: %d", __FUNCTION__, iCnt );
		RCHI_LIB_TRACE(szDbgMsg);

		if(pstSSIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue == NULL)
		{
			pstSSIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue = (char *)malloc(10);
			if(pstSSIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;

				return rv;
			}

			memset(pstSSIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue,0x00,10);
			sprintf(pstSSIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue,"%d", iSize);

		}

		RCHI_LIB_TRACE(pstSSIReqResFields[eEMVADMIN_RESPONSE].elementValue);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);

		}

		debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
		RCHI_LIB_TRACE(szDbgMsg);

		return rv;

}
/*
 * ============================================================================
 * Function Name: delEMVAdminNode
 *
 * Description	: This function is used to free a node from the Public keys List.
 *
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static void delEMVAdminNode(VAL_EMVADMINNODE_PTYPE pstNode)
{

#ifdef DEBUG
	char            szDbgMsg[4096]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	if(pstNode)
	{
		if(pstNode->pszPkCkSum)
			free(pstNode->pszPkCkSum);
		if(pstNode->pszPkExpDate)
			free(pstNode->pszPkExpDate);
		if(pstNode->pszPkCkSumLen)
			free(pstNode->pszPkCkSumLen);
		if(pstNode->pszPkExp)
			free(pstNode->pszPkExp);
		if(pstNode->pszPkIndex)
			free(pstNode->pszPkIndex);
		if(pstNode->pszPkMod)
			free(pstNode->pszPkMod);
		if(pstNode->pszPkModLen)
			free(pstNode->pszPkModLen);
		if(pstNode->pszPkExpLen)
			free(pstNode->pszPkExpLen);
		if(pstNode->pszRID)
			free(pstNode->pszRID);

		free(pstNode);
	}

	debug_sprintf(szDbgMsg, "%s: --- returning ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
* ============================================================================
* Function Name: parseAndCreateLinkedList
*
* Description  : Parsing the RC Response and creating Linked list
*
*
* Input Params : None
*
* Output Params: None
* ============================================================================
*/
int parseAndCreateLinkedList()
{
	int			rv              = SUCCESS;
	char 		*pszfirst		= NULL;
	char 		temp[999]  		= "";
	int 		iFlOffset 		= 0;

#ifdef DEBUG
	char            szDbgMsg[4096]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	iFlOffset  = atoi(gstEMVAdmin.szDownOffset);
	pszfirst = gstEMVAdmin.pszEMVKeyLst;

	if(!pszfirst)
	{
		debug_sprintf(szDbgMsg, "%s: Empty Key List", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}
	while(*gstEMVAdmin.pszEMVKeyLst != '\0')
	{
		sscanf(gstEMVAdmin.pszEMVKeyLst, "%s", temp);
		if(strstr(temp,"CA_KEYS") != NULL)
		{
			gstEMVAdmin.pszEMVKeyLst = gstEMVAdmin.pszEMVKeyLst + strlen(temp)+1;
			continue;
		}
		gstEMVAdmin.pszEMVKeyLst = gstEMVAdmin.pszEMVKeyLst + strlen(temp);

		if(*(gstEMVAdmin.pszEMVKeyLst) == '\n')
		{
			debug_sprintf(szDbgMsg, "%s: Complete Key :%s", __FUNCTION__, temp);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = createAndAddFilledNode(temp);
			/* If the previous key could not be filled because of insufficient data
			 * then we return RCHI_INCOMPLETE_KEY. In this case , we just move ahead with
			 * forming the next available key.
			 */
			if(rv == RCHI_INCOMPLETE_KEY)
			{
				debug_sprintf(szDbgMsg, "%s: Incomplete key , Continue with next key", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				rv = SUCCESS;
				continue;
			}
			else if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to create a node", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				return rv;
			}
			gstEMVAdmin.iMaxPublicKeys++;
			gstEMVAdmin.pszEMVKeyLst++;
		}
		else
		{
			break;
		}
	}
	if(temp[strlen(temp)] != '\n')
	{
		iFlOffset = iFlOffset - strlen(temp);
		sprintf(gstEMVAdmin.szDownOffset,"%d",iFlOffset);
	}
	gstEMVAdmin.pszEMVKeyLst = pszfirst;
	free(gstEMVAdmin.pszEMVKeyLst);

	displayEMV();

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__,gstEMVAdmin.iMaxPublicKeys );
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;

}
/*
* ============================================================================
* Function Name: convertDateFmt
*
* Description  : Host sends in MMDDYYYY format , we need to send SSI resp in DDMMYY format
*
*
* Input Params : None
*
* Output Params: None
* ============================================================================
*/
static int convertDateFmt(char *pzExpDate,char *pszPkExpDate)
{
	char 	szReqFmt[7];
	int 	rv = SUCCESS;
#ifdef DEBUG
     char            szDbgMsg[4096]  = "";
#endif

     debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
     RCHI_LIB_TRACE(szDbgMsg);

     if(strlen(pzExpDate) != 8)
	{
        debug_sprintf(szDbgMsg, "%s: Date Not in Correct Format From host", __FUNCTION__);
        RCHI_LIB_TRACE(szDbgMsg);
        rv = FAILURE;
	}
	szReqFmt[0]= pzExpDate[2];
	szReqFmt[1]= pzExpDate[3];
	szReqFmt[2]= pzExpDate[0];
	szReqFmt[3]= pzExpDate[1];
	szReqFmt[4]= pzExpDate[6];
	szReqFmt[5]= pzExpDate[7];
	szReqFmt[6] = '\0';
	strcpy(pszPkExpDate,szReqFmt);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
* ============================================================================
* Function Name: createAndAddFilledNode
*
* Description  : Parsing of a Public key string and filling the node in the linked list
*
*
* Input Params : None
*
* Output Params: None
* ============================================================================
*/

static int createAndAddFilledNode(char *pszTemp)
{
	int                        rv				= SUCCESS;
	int                        iStart			= 0;
	int                        iEnd				= 0;
	int                        iCount			= 0;
	int                        iField			= 0;
	int                        iLen				= 0;
	char                       szField[999]		= "";
	VAL_EMVADMINNODE_PTYPE     pstNode			= NULL;
	VAL_EMVADMINNODE_PTYPE     pstTemp			= NULL;
#ifdef DEBUG
     char            szDbgMsg[4096]  = "";
#endif

     debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
     RCHI_LIB_TRACE(szDbgMsg);

     pstNode = (VAL_EMVADMINNODE_PTYPE)malloc(sizeof(VAL_EMVADMINNODE_STYPE));
     if(pstNode == NULL)
     {
           debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
           RCHI_LIB_TRACE(szDbgMsg);
           rv = FAILURE;

           return rv;
     }

     for(iStart = 0, iCount = 0; pszTemp[iStart] != '\0' && pszTemp[iStart] != '\n'; iStart++, iCount++)
     {
    	 memset(szField, 0x00, sizeof(szField));
    	 iField = 0;
    	 for(iEnd = iStart; pszTemp[iEnd] != '\0' && pszTemp[iEnd] != '\n' && pszTemp[iEnd] != ','; iEnd++);
    	 if(iCount == 1 || iCount == 2)
    	 {
    		 iStart = iEnd;
    		 continue;
    	 }
    	 while(iStart < iEnd)
    	 {

    		 szField[iField] = pszTemp[iStart];
    		 iStart++;
    		 iField++;
    	 }
    	 szField[iField] = '\0';
    	 iStart = iEnd;

    	 if(iField != 0)
    	 {
    		 switch(iCount)
    		 {
    		 case 0:
    			 /* Convert the obtained Date Field into the required Format and then copy into
    			  * pstNode->pszPkExpDate.
    			  */
    			 pstNode->pszPkExpDate = (char *)malloc(6+1);
    			 rv = convertDateFmt(szField,pstNode->pszPkExpDate);
    			 if((pstNode->pszPkExpDate == NULL) || rv == FAILURE)
    			 {
    				 debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
    				 RCHI_LIB_TRACE(szDbgMsg);

    				 rv = FAILURE;
    				 return rv;
    			 }
    			 debug_sprintf(szDbgMsg, "%s: [%s]", __FUNCTION__,pstNode->pszPkExpDate );
    			 RCHI_LIB_TRACE(szDbgMsg);
    			 break;

    		 case 3:
    			 pstNode->pszRID = (char *)malloc(iField);
    			 if(pstNode->pszRID == NULL)
    			 {
    				 debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
    				 RCHI_LIB_TRACE(szDbgMsg);

    				 rv = FAILURE;
    				 return rv;
    			 }
    			 strcpy(pstNode->pszRID, szField);
    			 debug_sprintf(szDbgMsg, "%s: [%s]", __FUNCTION__,pstNode->pszRID );
    			 RCHI_LIB_TRACE(szDbgMsg);
    			 break;
    		 case 4:
    			 pstNode->pszPkIndex = (char *)malloc(iField);
    			 if(pstNode->pszPkIndex == NULL)
    			 {
    				 debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
    				 RCHI_LIB_TRACE(szDbgMsg);

    				 rv = FAILURE;
    				 return rv;
    			 }
    			 strcpy(pstNode->pszPkIndex, szField);
    			 debug_sprintf(szDbgMsg, "%s: Index:[%s]", __FUNCTION__,pstNode->pszPkIndex);
    			 RCHI_LIB_TRACE(szDbgMsg);
    			 break;
    		 case 5:
    			 pstNode->pszPkMod = (char *)malloc(iField);
    			 if(pstNode->pszPkMod == NULL)
    			 {
    				 debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
    				 RCHI_LIB_TRACE(szDbgMsg);

    				 rv = FAILURE;
    				 return rv;
    			 }
    			 strcpy(pstNode->pszPkMod, szField);
    			 iLen = strlen(pstNode->pszPkMod);
    			 pstNode->pszPkModLen = (char *)malloc(NUMBER_CONVERION);
    			 if(pstNode->pszPkMod == NULL)
    			 {
    				 debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
    				 RCHI_LIB_TRACE(szDbgMsg);

    				 rv = FAILURE;
    				 return rv;
    			 }
    			 sprintf(pstNode->pszPkModLen,"%d",iLen);
    			 break;
    		 case 6:
    			 pstNode->pszPkExp = (char *)malloc(iField);
    			 if(pstNode->pszPkExp == NULL)
    			 {
    				 debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
    				 RCHI_LIB_TRACE(szDbgMsg);

    				 rv = FAILURE;
    				 return rv;
    			 }
    			 strcpy(pstNode->pszPkExp, szField);
    			 iLen = strlen(pstNode->pszPkExp);
    			 pstNode->pszPkExpLen = (char *)malloc(NUMBER_CONVERION);
    			 if(pstNode->pszPkExpLen == NULL)
    			 {
    				 debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
    				 RCHI_LIB_TRACE(szDbgMsg);

    				 rv = FAILURE;
    				 return rv;
    			 }
    			 sprintf(pstNode->pszPkExpLen,"%d",iLen);
    			 break;

    		 case 7:
    			 pstNode->pszPkCkSum = (char *)malloc(iField);
    			 if(pstNode->pszPkCkSum == NULL)
    			 {
    				 debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
    				 RCHI_LIB_TRACE(szDbgMsg);

    				 rv = FAILURE;
    				 return rv;
    			 }
    			 strcpy(pstNode->pszPkCkSum, szField);
    			 iLen = strlen(pstNode->pszPkCkSum);
    			 pstNode->pszPkCkSumLen = (char *)malloc(NUMBER_CONVERION);
    			 if(pstNode->pszPkCkSumLen == NULL)
    			 {
    				 debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
    				 RCHI_LIB_TRACE(szDbgMsg);

    				 rv = FAILURE;
    				 return rv;
    			 }
    			 sprintf(pstNode->pszPkCkSumLen,"%d",iLen);
    			 break;
    		 }
    	 }
    	 else
    	 {
    		 delEMVAdminNode(pstNode);
    		 rv = RCHI_INCOMPLETE_KEY;
    	     debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__,rv);
    	     RCHI_LIB_TRACE(szDbgMsg);
    		 return rv;
    	 }
     }

     pstNode->next = NULL;

     if(gstEMVAdmin.pstKeysLst == NULL)
     {
    	 gstEMVAdmin.pstKeysLst = pstNode;
     }
     else
     {
    	 pstTemp = gstEMVAdmin.pstKeysLst;
    	 while(pstTemp->next != NULL)
    	 {
    		 pstTemp = pstTemp->next;
    	 }
    	 pstTemp->next = pstNode;
     }
     debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
     RCHI_LIB_TRACE(szDbgMsg);


     return rv;
}
/*
* ============================================================================
* Function Name: displayEMV
*
* Description  : Displays the public keys list
*
*
* Input Params : None
*
* Output Params: None
* ============================================================================
*/


void displayEMV()
{
	VAL_EMVADMINNODE_PTYPE     pstTemp              = gstEMVAdmin.pstKeysLst;
#ifdef DEBUG
	char            szDbgMsg[4096]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: ----enter-----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pstTemp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Empty Linked List", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		while(pstTemp != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Node value is: pszPkCkSum:%s,pszPkCkSumLen:%s,pszPkExp:%s,pszPkExpLen:%s,pszPkIndex:%s,"
					"pszPkMod:%s, pszPkModLen:%s,pszRID:%s", __FUNCTION__, pstTemp->pszPkCkSum,pstTemp->pszPkCkSumLen,
					pstTemp->pszPkExp,pstTemp->pszPkExpLen,pstTemp->pszPkIndex,pstTemp->pszPkMod,pstTemp->pszPkModLen,pstTemp->pszRID);
			RCHI_LIB_TRACE(szDbgMsg);

			pstTemp = pstTemp->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
* ============================================================================
* Function Name: parseFEXCOResponse
*
* Description  : Displays the public keys list
*
*
* Input Params : None
*
* Output Params: None
* ============================================================================
*/
/*Change the Place of the this function to Parse Function side*/
int parseFEXCOResponse(char* pszDCCResp, TRANDTLS_PTYPE pstDHIReqResFields, RCTRANDTLS_PTYPE pstRCReqResFields)
{
	int			rv									= SUCCESS;
	int			iLen								= 0;
	int			iCnt								= 0;
	int			iAmount								= 0;
	int			iValLen								= 0;
	int			iIndex								= 0;
	int			iAppLogEnabled						= 0;
	char		szTemp[256]							= "";
	char		szRootName[50]						= "";
	char		szAppLogData[256]					= "";
	xmlDoc *	docPtr								= NULL;
	xmlNode *	rootPtr								= NULL;
	int*		piDHIIndexforAmountConvertion		= NULL;
	double		fAmount								= 0.0;


#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(pszDCCResp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Response to Parse!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(pszDCCResp);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);
#if 0
#ifdef DEVDEBUG
		if(strlen(pszDCCResp) < 4500)
		{
			debug_sprintf(szDbgMsg, "%s: %s", __FUNCTION__, pszDCCResp);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif
#endif

		if(strlen(pszDCCResp) < 4500)
		{
			debug_sprintf(szDbgMsg, "%s: %s", __FUNCTION__, pszDCCResp);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		docPtr = xmlParseMemory(pszDCCResp, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		strcpy(szRootName, (char *)rootPtr->name);

		debug_sprintf(szDbgMsg, "%s: The root name of the FEXCO XML Response is %s",__FUNCTION__, szRootName);
		RCHI_LIB_TRACE(szDbgMsg);

		/* Calling a recursive function which parses through the xml and fills all the response details*/
		parseAndFillDataStruct(rootPtr, pstDHIReqResFields, pstRCReqResFields);


		/*PRADEEP:18-11-16: After Parsing the RC Response, now we have to change the amount format from RC to SSI, we will get
		 * all the DHI index where we need to change the amount format, once we get -1 in place of DHI Index it mentions that we changed all
		 * SSI Amount Fields format*/
		piDHIIndexforAmountConvertion = getArrayofDHIIndexforAmtConversion();
		if(piDHIIndexforAmountConvertion == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No fields are present to change amount format ", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			while(1)
			{
				iIndex = piDHIIndexforAmountConvertion[iCnt];
				if(iIndex == -1)
				{
					break;
				}
				else
				{
					if(pstDHIReqResFields[iIndex].elementValue != NULL)
					{
						iValLen = strlen(pstDHIReqResFields[iIndex].elementValue);

						if(strspn(pstDHIReqResFields[iIndex].elementValue, "1234567890-") != iValLen)
						{
							debug_sprintf(szDbgMsg, "%s: Characters other than [0-9] & [-] found, so same value will be sent to SSI Field",
									__FUNCTION__);
							RCHI_LIB_TRACE(szDbgMsg);
						}
						else
						{
							iAmount = atoi(pstDHIReqResFields[iIndex].elementValue);
							fAmount = iAmount/100;
							sprintf(szTemp, "%.02f",fAmount);
							/*Freeing the previously allocated Memory*/
							free(pstDHIReqResFields[iIndex].elementValue);
							pstDHIReqResFields[iIndex].elementValue = NULL;

							pstDHIReqResFields[iIndex].elementValue = strdup((char*)szTemp);
						}
					}
				}
				++iCnt;
			}
		}


		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
		}

		if(pszDCCResp != NULL)
		{
			free(pszDCCResp);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillCardCurrDtls
 *
 * Description	: Fills up the required rc details for the EMV Keys Status Request Packet
 *
 * Input Params	:
 *
 * Output Params: FAILURE/SUCCESS
 * ============================================================================
 */
int fillCardCurrDtls(TRANDTLS_PTYPE pstDHIReqResFields, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int						rv						= SUCCESS;
	int						iTmp 					= 0;
	int						iAppLogEnabled			= 0;
	char 					szTemp[53+1]			= "";
	char					szAppLogData[256]		= "";
	char					szAppLogDiag[256]		= "";
	AVL_PTYPE				pszReturnValue			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData , 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	debug_sprintf(szDbgMsg, "%s: --- Fgn Currency Code[%s] ---", __FUNCTION__, pstRCHIDetails[RC_FGNCURCODE].elementValue);
	RCHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

		if(pstRCHIDetails[RC_FGNCURCODE].elementValue != NULL)
		{

			strcpy(szTemp, pstRCHIDetails[RC_FGNCURCODE].elementValue);
			iTmp = atoi(szTemp);

			pszReturnValue =(AVL_PTYPE) findCurrCode(iTmp);

			if(pszReturnValue != NULL)
			{
				/*Parsing Country Name*/
				if(pszReturnValue->pszValue->szCountryName != NULL)
				{
					strcpy(pstRCHIDetails[RC_FEXCO_COUNTRY_NAME].elementValue, pszReturnValue->pszValue->szCountryName);
					pstDHIReqResFields[eCOUN_NAME].elementValue = strdup(pstRCHIDetails[RC_FEXCO_COUNTRY_NAME].elementValue);
				}

				/*Parsing Currency Name*/
				if(pszReturnValue->pszValue->szCurrName != NULL)
				{
					strcpy(pstRCHIDetails[RC_FEXCO_CURRENCY_NAME].elementValue, pszReturnValue->pszValue->szCurrName);
					pstDHIReqResFields[eCURR_NAME].elementValue = strdup(pstRCHIDetails[RC_FEXCO_CURRENCY_NAME].elementValue);
				}

				/*Parsing Alpha Currency Code */
				if(pszReturnValue->pszValue->szAlphaCurrCode != NULL)
				{
					strcpy(pstRCHIDetails[RC_FEXCO_ALPHABETIC_CURRENCY_CODE].elementValue, pszReturnValue->pszValue->szAlphaCurrCode);
					pstDHIReqResFields[eALPHA_CURR_CODE].elementValue = strdup(pstRCHIDetails[RC_FEXCO_ALPHABETIC_CURRENCY_CODE].elementValue);
				}

				/*Parsing CountryAlpha2Code */
				if(pszReturnValue->pszValue->szCountryAlpha2Code != NULL)
				{
					strcpy(pstRCHIDetails[RC_FEXCO_COUNTRY_ALPHA_2_CODE].elementValue, pszReturnValue->pszValue->szCountryAlpha2Code);
					pstDHIReqResFields[eCOUN_ALPHA_2_CODE].elementValue = strdup(pstRCHIDetails[RC_FEXCO_COUNTRY_ALPHA_2_CODE].elementValue);
				}

				/*Parsing Currrency Symbol */
				if(pszReturnValue->pszValue->szCurrSym != NULL)
				{
					/*Convertion of String to UTF format for Currency Symbol*/
					convStrToUTFStr(pszReturnValue->pszValue->szCurrSym, pstRCHIDetails[RC_FEXCO_CURRENCY_SYMBOL].elementValue);
					//strcpy(pstRCHIDetails[RC_FEXCO_CURRENCY_SYMBOL].elementValue, pszReturnValue->pszValue->szCurrSym);
					pstDHIReqResFields[eCURR_SYMBOL].elementValue = strdup(pstRCHIDetails[RC_FEXCO_CURRENCY_SYMBOL].elementValue);
				}

				/*Parsing minor Currency unit*/
				if(pszReturnValue->pszValue->szMinorCurrUnit != NULL)
				{
					strcpy(pstRCHIDetails[RC_FEXCO_MINOR_CURRENCY_UNIT].elementValue, pszReturnValue->pszValue->szMinorCurrUnit);
				}

				if(pstDHIReqResFields[eMINR_UNT].elementValue == NULL)
				{
					pstDHIReqResFields[eMINR_UNT].elementValue = strdup(pstRCHIDetails[RC_FEXCO_MINOR_CURRENCY_UNIT].elementValue);
				}
			}
			else
			{
				pstDHIReqResFields[eDCC_OFFD].elementValue = strdup("FALSE");

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Country Code [%s]is not Present in FCP.DAT.txt file.", pstRCHIDetails[RC_FGNCURCODE].elementValue);
					sprintf(szAppLogDiag, "Please Contact FEXCO");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				/*Need to Clear every other details*/

				if(pstDHIReqResFields[eFGN_CUR_CODE].elementValue != NULL)
				{
					free(pstDHIReqResFields[eFGN_CUR_CODE].elementValue);
					pstDHIReqResFields[eFGN_CUR_CODE].elementValue = NULL;
				}
				if(pstDHIReqResFields[eEXGN_RATE].elementValue != NULL)
				{
					free(pstDHIReqResFields[eEXGN_RATE].elementValue);
					pstDHIReqResFields[eEXGN_RATE].elementValue = NULL;
				}
				if(pstDHIReqResFields[eFGN_AMT].elementValue != NULL)
				{
					free(pstDHIReqResFields[eFGN_AMT].elementValue);
					pstDHIReqResFields[eFGN_AMT].elementValue = NULL;
				}
				if(pstDHIReqResFields[eVALID_HRS].elementValue != NULL)
				{
					free(pstDHIReqResFields[eVALID_HRS].elementValue);
					pstDHIReqResFields[eVALID_HRS].elementValue = NULL;
				}
				if(pstDHIReqResFields[eMRGN_RATE_PERCENT].elementValue != NULL)
				{
					free(pstDHIReqResFields[eMRGN_RATE_PERCENT].elementValue);
					pstDHIReqResFields[eMRGN_RATE_PERCENT].elementValue = NULL;
				}
				if(pstDHIReqResFields[eEXGN_RATE_SRC_NAME].elementValue != NULL)
				{
					free(pstDHIReqResFields[eEXGN_RATE_SRC_NAME].elementValue);
					pstDHIReqResFields[eEXGN_RATE_SRC_NAME].elementValue = NULL;
				}
				if(pstDHIReqResFields[eCOMM_PERCENT].elementValue != NULL)
				{
					free(pstDHIReqResFields[eCOMM_PERCENT].elementValue);
					pstDHIReqResFields[eCOMM_PERCENT].elementValue = NULL;
				}
				if(pstDHIReqResFields[eEXGN_RATE_SRCTIMESTAMP].elementValue != NULL)
				{
					free(pstDHIReqResFields[eEXGN_RATE_SRCTIMESTAMP].elementValue);
					pstDHIReqResFields[eEXGN_RATE_SRCTIMESTAMP].elementValue = NULL;
				}

				debug_sprintf(szDbgMsg, "%s: --- The Country Code is not present in the FCP.DAT.txt. Please Contact FEXCO---", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		debug_sprintf(szDbgMsg, "%s: returning[%d]", __FUNCTION__, rv);
		RCHI_LIB_TRACE(szDbgMsg);

		return rv;

}
