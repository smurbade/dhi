/*
 * rchiMain.c
 *
 *  Created on: Dec 17, 2014
 *      Author: BLR_SCA_TEAM
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>


//#include "tcpipdbg.h""
#include <svc.h>
#include "RCHI/metaData.h"
#include "RCHI/rchiCommon.h"
#include "RCHI/rchiGroups.h"
#include "RCHI/xmlUtils.h"
#include "RCHI/rchi.h"
#include "DHI_APP/appLog.h"

extern int 	initRCIFace();
extern int 	loadRCHIConfigParams();
extern int	loadFrgCurFile();
extern int  loadFgnCardBinData();
extern int	processCreditTransaction(TRANDTLS_PTYPE);
extern int	processDebitTransaction(TRANDTLS_PTYPE);
extern int  processGiftTransaction(TRANDTLS_PTYPE);
extern int	processReportCommand(TRANDTLS_PTYPE);
extern int	processEBTTransaction(TRANDTLS_PTYPE);
extern int	processCheckTransaction(TRANDTLS_PTYPE);
extern int	createHashTableForRCFields();
extern int	cleanCURLHandle(RCHI_BOOL);
extern int  isDCCEnabledInDevice();
/* Static Functions */
static int 	initEMVAdminCfg();
#ifdef DEBUG
static int giSaveDebugLogs 	  = 0;
static int giSaveMemDebugLogs = 0;
static void sigHandler(int);
static void setupSigHandlers();
#endif
extern int SaveDebugLogs();

/*
 * ============================================================================
 * Function Name: initHILib
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int initHILib()
{
	int		rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szErrMsg[256]   	= "";
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	int				iDbgParmsSet						= 0;
	char	szDbgMsg[2560]	= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Initializing Rapid Connect Host Interface Library");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 1: Initialize the debugger.
	 * ---------------------------------
	 */
#ifdef DEBUG
	setupRCHIDebug(&giSaveDebugLogs);
	setupRCHIMemDebug(&giSaveMemDebugLogs);
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Lib Ver:[%s]", __FUNCTION__, RCHI_LIB_VERSION);
	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Lib Build Number:[%s]", __FUNCTION__, RCHI_BUILD_NUMBER);
	RCHI_LIB_TRACE(szDbgMsg);

#ifdef DEBUG
	 //Setting Signal handlers for debugging purpose
	setupSigHandlers();
#endif

#ifdef DEBUG
	/*
	 * ------------------------------------
	 * STEP 2: Setting the Debug config params to environment.
	 * ------------------------------------
	 */
	rv = setRCHIDebugParamsToEnv(&iDbgParmsSet);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to set debug params to environment",
				__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to set debug params to environment");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		while(1)
		{
			svcWait(10);
		}
	}
	else
	{
		if(iDbgParmsSet == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Debug parameters are reset to environment",
							__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			giSaveDebugLogs = 0; //testing purpose
			giSaveMemDebugLogs = 0; //testing purpose

			/* Close the debug socket before updating with new debug env params */
			closeupDebug();
			closeupMemDebug();

			svcWait(3000);

			//Setting debug with new environment variables
			setupRCHIDebug(&giSaveDebugLogs);
			setupRCHIMemDebug(&giSaveMemDebugLogs);

			svcWait(3000);

			debug_sprintf(szDbgMsg, "%s: giSaveDebugLogs[%d], giSaveMemDebugLogs [%d]",__FUNCTION__, giSaveDebugLogs, giSaveMemDebugLogs);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Reseting DEBUG params to envinorment is not required",
										__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}
#endif

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "RCHI Library Version:[%s], RCHI Libray Build Number:[%s]", RCHI_LIB_VERSION, RCHI_BUILD_NUMBER);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * ------------------------------------
	 * STEP 3: Loading RCHI config params
	 * ------------------------------------
	 */
	rv = loadRCHIConfigParams();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to initialize RCHI config parameters", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Failed To Load RCHI Configuration Parameters");
			strcpy(szAppLogDiag, "Please Check The RCHI Configuration");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}

		return rv;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Successfully initialized RCHI config parameters", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	if(isDCCEnabledInDevice())
	{
		rv = loadFgnCardBinData();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to initialize Foreign Card BIN data", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			return rv;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Successfully initialized Foreign Card BIN data", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		rv = loadFrgCurFile();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to initialize Foreign Currency File", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			return rv;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Successfully initialized Foreign Currency File", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
	}

	rv = initEMVAdminCfg();
	rv = initRCIFace();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to initialize RCHI Interface", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		return rv;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Successfully initialized RCHI Interface", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	/*
	 * ----------------------------------------------------
	 * STEP 4: Creating hash table for parsing XML Response
	 * ----------------------------------------------------
	 */
	rv = createHashTableForRCFields();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create Hash Table for SSI Fields",
				__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to create Hash Table for SSI Fields");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Create Hash Table");
			strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}
		return rv;
	}

	/*
	 * ----------------------------------------------------
	 * STEP 5: Creating Timeout reversal thread
	 * ----------------------------------------------------
	 */
	rv = createTimeoutReversalThread();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create thread for tiemout reversal",
				__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to create thread for tiemout reversal");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "FAILED to create thread for tiemout reversal");
			strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}
		return rv;
	}

	/*
	 * ----------------------------------------------------
	 * STEP 5: Creating EMV Status Request Thread
	 * ----------------------------------------------------
	 */
	rv = createEMVKeyStatThread();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create thread for tiemout reversal",
				__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to create thread for tiemout reversal");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "FAILED to create thread for tiemout reversal");
			strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}
		return rv;
	}
	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Initialized Rapid Connect Host Interface Library Successfully");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);


	return rv;
}

/*
 * ============================================================================
 * Function Name: rchiMalloc
 *
 * Description	: Wrapper for malloc, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *rchiMalloc(unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	void *		tmpPtr				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = malloc(iSize);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaMalloc", tmpPtr, iSize, pszCallerFuncName, iLineNum);
		RCHI_LIB_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaMalloc");
		RCHI_LIB_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: cphistrdup
 *
 * Description	: Wrapper for strdup, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern char *rchistrdup(char *ptr , int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	char*		   		tmpPtr				= NULL;
	unsigned int        iSize 				= 0;

	iSize= strlen(ptr);

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = strdup(ptr);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaMalloc", tmpPtr, iSize, pszCallerFuncName, iLineNum);
		RCHI_LIB_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaMalloc");
		RCHI_LIB_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: rchistrndup
 *
 * Description	: Wrapper for strndup, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern char *rchistrndup(char *ptr , unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	char *		tmpPtr				= NULL;
//	unsigned int         iSize 				= 0;

//	iSize= strlen(ptr);

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = strndup(ptr, iSize);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaMalloc", tmpPtr, iSize, pszCallerFuncName, iLineNum);
		RCHI_LIB_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaMalloc");
		RCHI_LIB_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}
/*
 * ============================================================================
 * Function Name: rchiReAlloc
 *
 * Description	: Wrapper for realloc, will print num of bytes and starting
 * 					and original address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *rchiReAlloc(void *ptr, unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalReallocMem= 0;
	void *		tmpPtr				= NULL;
	char		szOrigAddr[16]		= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//Storing Original Address
	memset(szOrigAddr, 0x00, sizeof(szOrigAddr));
	sprintf(szOrigAddr, "%p", ptr);

	tmpPtr = realloc(ptr, iSize);

	if(tmpPtr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] OrigStartAddr[%s] NewStartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaReAlloc", szOrigAddr, tmpPtr, iSize, pszCallerFuncName, iLineNum);
		RCHI_LIB_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaReAlloc");
		RCHI_LIB_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: rchiFree
 *
 * Description	: Wrapper for free, will print going to be free
 * 					address also and make the freed pointer points to NULL
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void rchiFree(void **pptr, int iLineNum, char * pszCallerFuncName)
{
	//size_t *sizePtr					= ((size_t *)ptr) - 1;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(*pptr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] FreedAddr[%p] called by [%s] from Line[%d]",
				"scaFree", *pptr, pszCallerFuncName, iLineNum);
		RCHI_LIB_MEM_TRACE(szDbgMsg);
		free(*pptr);
		*pptr = NULL;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: [MEMDEBUG] NULL params Passed ", "scaFree");
		RCHI_LIB_MEM_TRACE(szDbgMsg);
	}
}

#ifdef DEBUG

static void catch_stop()
{
}

/*
 * ============================================================================
 * Function Name: setupSigHandlers
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void setupSigHandlers()
{
	char	szDbgMsg[256]	= "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Set up signal handlers */
	if(SIG_ERR == signal(SIGABRT, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGABRT",
						__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGSEGV, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSEGV",
						__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGILL, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGILL",
						__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGKILL, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGKILL",
						__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGSTOP, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSTOP",
						__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGTERM, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSTOP",
						__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return;
}


/*
 * ============================================================================
 * Function Name: sigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void sigHandler(int iSigNo)
{
	char	szDbgMsg[256]	= "";
	struct 	sigaction 		setup_action;
	sigset_t block_mask;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	pthread_t self = pthread_self();

	debug_sprintf(szDbgMsg, "%s: threadId=%lu", __FUNCTION__, (unsigned long)self);
	RCHI_LIB_TRACE(szDbgMsg);

	sigemptyset(&block_mask);

	sigaddset(&block_mask, SIGABRT);
	sigaddset(&block_mask, SIGSEGV);
	sigaddset(&block_mask, SIGILL);
	sigaddset(&block_mask, SIGPIPE);

	setup_action.sa_handler = catch_stop;
	setup_action.sa_mask = block_mask;
	setup_action.sa_flags = 0;
	sigaction(iSigNo, &setup_action, NULL);

	switch(iSigNo)
	{
	case SIGABRT:
		sprintf(szDbgMsg, "%s: SIGABRT signal received", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1)
		{
//			SaveDebugLogs();
		}
		cleanCURLHandle(RCHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGSEGV:
		sprintf(szDbgMsg, "%s: SIGSEGV signal received", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(RCHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGILL:
		sprintf(szDbgMsg, "%s: SIGILL signal received", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(RCHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGKILL:
		sprintf(szDbgMsg, "%s: SIGKILL signal received", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(RCHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGSTOP:
		sprintf(szDbgMsg, "%s: SIGSTOP signal received", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(RCHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGTERM:
		sprintf(szDbgMsg, "%s: SIGTERM signal received", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1)
		{
			SaveDebugLogs();
		}
		cleanCURLHandle(RCHI_TRUE);

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGPIPE:
		sprintf(szDbgMsg, "%s: SIGPIPE signal received", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);
		cleanCURLHandle(RCHI_TRUE);
		break;

	default:
		sprintf(szDbgMsg, "%s: Signal no = [%d]", __FUNCTION__, iSigNo);
		RCHI_LIB_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);
	}

	return;
}
#endif

/* Initialize the global structure which is used to initiate EMV Key Downloads and also
 * parse the response from the host.
 */
static int initEMVAdminCfg()
{
	strcpy(gstEMVAdmin.szFileBlk,"0001");
	strcpy(gstEMVAdmin.szDownOffset,"0");
	gstEMVAdmin.pszEMVKeyLst = NULL;
	gstEMVAdmin.iMaxPublicKeys = 0;
	gstEMVAdmin.pstKeysLst = NULL;
	gstEMVAdmin.iKeyUpdAvl = 0;
	memset(gstEMVAdmin.szDwnldStat,0x00,sizeof(gstEMVAdmin.szDwnldStat));

	return SUCCESS;
}

