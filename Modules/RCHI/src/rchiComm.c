/*
 * rcComm.c
 *
 *  Created on: Dec 8, 2014
 *      Author: BLR_SCA_TEAM
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <time.h>
#include <sys/timeb.h>	// For struct timeb
#include <pthread.h>
#include <svc.h>
#include <setjmp.h>
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <sys/types.h>        /*  socket types              */
#include <sys/socket.h>       /*  socket definitions        */
#include <netinet/in.h>
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>
#include <netdb.h>
#include <arpa/nameser.h>
#include <resolv.h>

#include "RCHI/rchiCommon.h"
#include "RCHI/rchiHostCfg.h"
#include "RCHI/rchiCfg.h"
#include "RCHI/rchiMsgQueue.h"
#include "RCHI/utils.h"
#include "DHI_APP/appLog.h"



#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)




static int 				msgQID;

#define CA_CERT_FILE	"ca-bundle.crt"

static struct curl_slist *	headers			= NULL;
static pthread_t			ptKeepAliveIntId;
static CURL *				gptRCCurlHandle = NULL;
static pthread_mutex_t  	gptRCCurlHandleMutex;
static sigjmp_buf			keepAliveJmpBuf;

static int 		getCURLHandle(HOST_URL_ENUM);

static int		initRCCURLHandle(CURL **, HOST_URL_ENUM);
static void		commonInit(CURL *, RCHI_BOOL, int);
static size_t	saveResponse(void *, size_t, size_t, void *);
static int 		sendDataToRCHost(CURL *, char *, int, char **, int *);
static int 		initializeRespData(RESP_DATA_PTYPE);
static void * 	keepAliveIntervalTimer(void *);
static void 	keepAliveTimeSigHandler(int);
static int      initializeOpenSSLLocks();
static int 		chkConn(CURL * );
extern int 		getNodeValFromDWResp(char*, char**, char**, int, char*);

#ifdef DEBUG
/* Static functions' declarations */
static int 		curlDbgFunc(CURL *, curl_infotype, char *, size_t, void *);
#endif
/* we have this global to let the callback get easy access to it */
static pthread_mutex_t *lockarray;

static int gWaitTime = 0;

/*
 * ============================================================================
 * Function Name: initRCIFace
 *
 * Description	:
 *
 * Input Params	: Nothing
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initRCIFace()
{
	int				rv				= SUCCESS;
	int				iKeepAlive		= -1;
	char *			pszCURLVer		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	iKeepAlive = getKeepAliveParameter();
	/* Initialize the CURL library */
	curl_global_init(CURL_GLOBAL_ALL);

	/*
	 * Praveen_P1: We have started getting problem with the SAF and Online
	 * transactions together. To make it thread safe, we have followed the
	 * following link
	 * http://curl.haxx.se/libcurl/c/threaded-ssl.html
	 *
	 */
	rv = initializeOpenSSLLocks();

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while initializing OpenSSL Lock Callbacks", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return rv;
	}

	/* Get the CURL library version and print it in the logs */
	pszCURLVer = curl_version();

	/* VDR: TODO: Add a syslog statement */
	debug_sprintf(szDbgMsg, "%s: CURL Ver: %s", __FUNCTION__, pszCURLVer);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Add data to the headers */
	headers = curl_slist_append(headers, "User-Agent:SCA v2.19.x");
	headers = curl_slist_append(headers, "Content-Type:text/xml");
//	headers = curl_slist_append(headers, "Cache-Control:no-cache");

	if(pthread_mutex_init(&gptRCCurlHandleMutex, NULL))//Initialize the RC curl Handle Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create RC Curl Handle Mutex!", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(iKeepAlive == KEEP_ALIVE_DEFINED_TIME)
	{
		//Open the keep Alive Interval Thread
		debug_sprintf(szDbgMsg, "%s - Starting the keepAliveIntervalThread ...", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		pthread_create(&ptKeepAliveIntId, NULL, keepAliveIntervalTimer, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: postDataToRCHost
 *
 * Description	: This function initializes the curl handler and sends the data
 * 				  to the RC Host
 *
 * Input Params	:	char *
 * 					int
 * 					char **
 * 					int *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int postDataToRCHost(char * req, int reqSize, char ** resp, int * respSize)
{
	int			rv					= SUCCESS;
	int			iCnt				= 0;
	int			iAppLogEnabled		= 0;
	static int	host				= PRIMARY_URL;
	char		szAppLogData[256]	= "";
	char		szAppLogDiag[256]	= "";

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: RC HOST REQ = [%s]", DEVDEBUGMSG,__FUNCTION__, req);
		RCHI_LIB_TRACE(szDbgMsg);
	}
#endif

	while(iCnt < getNumUniqueHosts())
	{
		acquireRCMutexLock(&gptRCCurlHandleMutex, "RC Curl Handle Mutex");
		rv = getCURLHandle(host);
		//rv = initRCCURLHandle(&curlHandle, host);

		if(rv == SUCCESS)
		{
			 /* CURL Handler was initialized successfully*/
			rv = sendDataToRCHost(gptRCCurlHandle, req, reqSize, resp, respSize);
			releaseRCMutexLock(&gptRCCurlHandleMutex, "RC Curl Handle Mutex");

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Post Data to the RC Host");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				if(rv == ERR_RESP_TO) //Incase of response timeout we should not try with the next url
				{
					debug_sprintf(szDbgMsg, "%s: Response Time out from the RC host",
											__FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}

			if((rv == SUCCESS) && (*resp))
			{
				cleanCURLHandle(RCHI_FALSE);
				break;
			}

			cleanCURLHandle(RCHI_TRUE); //Closing the current curl handler before trying the next host url
		}
		else
		{
			//If getCURLHandle Fails , then we would not have sent the packet and hence not release the lock. Hence release it here.
			releaseRCMutexLock(&gptRCCurlHandleMutex, "RC Curl Handle Mutex");
		}

		/* If the first host is not working then try the next host*/
		host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trial %d..", __FUNCTION__,iCnt);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);

	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: cleanCURLHandle
 *
 * Description	: This function cleans/closes the opened CURL Handle
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int cleanCURLHandle(RCHI_BOOL iForceClose)
{
	int				rv				= SUCCESS;
	static 	int		iKeepAlive		= -1;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(iKeepAlive == -1)
	{
		iKeepAlive = getKeepAliveParameter();
	}

	acquireRCMutexLock(&gptRCCurlHandleMutex, "RC Curl Handle Mutex");
	debug_sprintf(szDbgMsg, "%s: Enter, gptRCCurlHandle [%p]", __FUNCTION__, gptRCCurlHandle);
	RCHI_LIB_TRACE(szDbgMsg);

	if((iForceClose == RCHI_TRUE) || (iKeepAlive == KEEP_ALIVE_DISABLED))
	{
		if(gptRCCurlHandle != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Cleaning up the CURL handle from if cond", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			curl_easy_cleanup(gptRCCurlHandle);
			gptRCCurlHandle = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No need to clean CURL Handle", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

	}

	releaseRCMutexLock(&gptRCCurlHandleMutex, "RC Curl Handle Mutex");

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkHostConn
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int checkHostConn(char * req, int reqSize, char ** resp, int * respSize)
{
	int			rv				= SUCCESS;
	int			iCnt			= 0;
	static int	host			= PRIMARY_URL;
	CURL *		curlHandle		= NULL;
#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(iCnt < getNumUniqueHosts())
	{
		rv = initRCCURLHandle(&curlHandle, host);

		debug_sprintf(szDbgMsg, "%s: curlHandle [%p]", __FUNCTION__, curlHandle);
		RCHI_LIB_TRACE(szDbgMsg);

		if(rv == SUCCESS)
		{
			/* Curl handler initialized successfully
			 * Checking connection */
			/* Daivik:4/5/2016 - We now need to send EchoTest Request to the host to check the host connectivity instead
			 * of an empty packet. We will consider the host to be available based on the response code.
			 */
			//rv = chkConn(curlHandle);
			rv = sendDataToRCHost(curlHandle,req, reqSize, resp, respSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to check Host connection",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host connection may be available Check Resp Code", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}

		if(curlHandle != NULL)
		{
			curl_easy_cleanup(curlHandle);
		}

		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Breaking from the loop", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* If the first host is not working then try the next host */
		host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trial :%d", __FUNCTION__,iCnt);
		RCHI_LIB_TRACE(szDbgMsg);

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: chkConn
 *
 * Description	:
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

static int chkConn(CURL * curlHandle)
{
	int				rv				= SUCCESS;
//	long			lCode			= 0L;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
//		curl_easy_setopt(curlHandle, CURLOPT_POST, 0L);
//		curl_easy_setopt(curlHandle, CURLOPT_NOBODY, 1);
//		curl_easy_setopt(curlHandle, CURLOPT_VERBOSE, 1L);
//		curl_easy_setopt(curlHandle, CURLOPT_FOLLOWLOCATION, 1);

//		curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, NULL);
//		curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, 0);

//		curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, NULL);

		curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 1L);

		rv = curl_easy_perform(curlHandle);
		if(rv == CURLE_OK)
		{
			debug_sprintf(szDbgMsg, "%s: Perform SUCCESS", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Return Value from PERFORM = [%d]",
															__FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}

		/* Unsetting the connect only option */
		curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 0L);

#if 0
		//rv = curl_easy_getinfo(curlHandle, CURLINFO_HTTP_CONNECTCODE, &lCode);

		rv = curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &lCode);

		debug_sprintf(szDbgMsg, "%s: Return Value RESPONSE CODE = [%d], [%ld]",
													__FUNCTION__, rv, lCode);
		RCHI_LIB_TRACE(szDbgMsg);

		if(rv == 0 && lCode == 200)
		{
			debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else if(rv == 0 && lCode == 404) //if connection is available we are getting this one considering good response for now
		{
			debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Didnt get the good resp code from host"
															, __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}
#endif
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: static void * keepAliveIntervalTimer(void * arg)
 *
 * Description	: Thread to run the keep Alive Interval timer
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void * keepAliveIntervalTimer(void * arg)
{
	int			iKeepAliveInterval				= 0;
	int			iVal							= 0;
	ullong		curTime							= 0;
	ullong		endTime							= 0;

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	/* Add signal handler for SIG_KA_TIME SETTIME */
	if(SIG_ERR == signal(SIG_KA_TIME, keepAliveTimeSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIG_KA_TIME",
																__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Keep Alive Interval Thread Started", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	iKeepAliveInterval = getKeepAliveInterval();
	iVal = sigsetjmp(keepAliveJmpBuf, 1);
	if(iVal == 1)
	{
		debug_sprintf(szDbgMsg, "%s: Resetting the Keep Alive Interval Timer",
								__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	curTime = svcGetSysMillisec();
	endTime = curTime + (iKeepAliveInterval * 60 * 1000L);

	while(1)
	{
		while(endTime >= curTime)
		{
			svcWait(100); // Sleep little by little in the loop for KeepAliveInterval Duration
			curTime = svcGetSysMillisec();
		}

		if(gptRCCurlHandle != NULL)
		{
			cleanCURLHandle(RCHI_TRUE);
		}
		svcWait(360000); // keep the thread to run permanently by waiting
	}

	return NULL;
}

/*
 * ============================================================================
 * Function Name: keepAliveTimeSigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void keepAliveTimeSigHandler(int iSigNo)
{
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(iSigNo == SIG_KA_TIME)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIG_KA_TIME delivered", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		siglongjmp(keepAliveJmpBuf, 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown signal", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}
	return;
}

/*
 * ===================================================================================================================
 * Function Name: resetKAIntervalUpdationTimer
 *
 * Description	: Sends the signal to Keep Alive Interval thread to reset the timer once set time command is received
 *
 * Input Params	:
 *
 * Output Params: none
 * =====================================================================================================================
 */
int resetKAIntervalUpdationTimer()
{
	int		iRetVal = FAILURE;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Keep Alive Interval Timer thread id: %d", __FUNCTION__, (int)ptKeepAliveIntId);
	RCHI_LIB_TRACE(szDbgMsg);

	//Check for the thread before killing.
	if(ptKeepAliveIntId != 0)
	{
		iRetVal = pthread_kill(ptKeepAliveIntId, SIG_KA_TIME);
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, iRetVal);
	RCHI_LIB_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getCURLHandle
 *
 * Description	: This function returns the curl Handle according to keep Alive configuration parameter
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getCURLHandle(HOST_URL_ENUM host)
{
	int				rv 				= SUCCESS;
	static 	int		iKeepAlive		= -1;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(iKeepAlive == -1)
	{
		iKeepAlive = getKeepAliveParameter();
	}

	/* Daivik:5/5/2016 - Taking this lock in postRCDataToHost. Because
	 * once we have recieved the curlHandler, we need to send the packet before anyone else can modify it.
	 * If we take the lock only here , there is chance that another thread calls clean CURL Handler and hence set the
	 * global curl handler to NULL.
	 */
	//acquireRCMutexLock(&gptRCCurlHandleMutex, "RC Curl Handle Mutex");

	debug_sprintf(szDbgMsg, "%s: iKeepAlive [%d], gptRCCurlHandle ---- [%p]", __FUNCTION__, iKeepAlive, gptRCCurlHandle);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if(gptRCCurlHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: CURL Handle Not Available, Create CURL Handle", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = initRCCURLHandle(&gptRCCurlHandle, host);

		}

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: CURL Handle Init Failed", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}

		if(iKeepAlive == KEEP_ALIVE_DEFINED_TIME && (ptKeepAliveIntId != 0))
		{
			rv = pthread_kill(ptKeepAliveIntId, SIG_KA_TIME);
		}

		break;
	}

	//releaseRCMutexLock(&gptRCCurlHandleMutex, "RC Curl Handle Mutex");

	return rv;
}

/*
 * ============================================================================
 * Function Name: initRCCURLHandle
 *
 * Description	: Initializes the RC Curl handler with all the required curl library options.
 *
 * Input Params	:	CURL *
 * 					HOST_URL_ENUM
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */


static int initRCCURLHandle(CURL ** handle, HOST_URL_ENUM hostType)
{
	int					rv					= SUCCESS;
	int					iAppLogEnabled		= 0;
	char				szTmpURL[100]		= "";
	char				szAppLogData[256]	= "";
	char				szAppLogDiag[256]	= "";
	CURL *				locHandle			= NULL;
	RCHI_BOOL			certValdReq 		= RCHI_FALSE;
	HOSTDEF_STRUCT_TYPE	hostDef;
#ifdef DEBUG
	char				szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		memset(&hostDef, 0x00, sizeof(hostDef));

		rv = getRCHostDetails(hostType, &hostDef);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the host details",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		if (strncmp(hostDef.url, "https", 5) == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Url: %s",__FUNCTION__, hostDef.url);
			RCHI_LIB_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Cert Validation required",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			certValdReq = RCHI_TRUE;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Posting the request to the URL %s", hostDef.url);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		/* Initialize the handle using the CURL library */
		locHandle = curl_easy_init();
		if(locHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Curl initialization for handle failed",
																__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the URL */
		if(hostDef.portNum != 0)
		{
			sprintf(szTmpURL, "%s:%d", hostDef.url, hostDef.portNum);
			curl_easy_setopt(locHandle, CURLOPT_URL, szTmpURL);

			debug_sprintf(szDbgMsg, "%s: Setting RC URL = [%s]", __FUNCTION__,
																	szTmpURL);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			curl_easy_setopt(locHandle, CURLOPT_URL, hostDef.url);

			debug_sprintf(szDbgMsg, "%s: Setting RC URL = [%s]", __FUNCTION__,
																hostDef.url);
			RCHI_LIB_TRACE(szDbgMsg);

		}

		/* Set the connection timeout */
		curl_easy_setopt(locHandle, CURLOPT_CONNECTTIMEOUT, hostDef.conTimeOut);

		/* Set the total timeout */
		if(hostDef.respTimeOut > 0)
		{
			curl_easy_setopt(locHandle, CURLOPT_TIMEOUT, hostDef.respTimeOut);
		}

		/* Set the other common features */

		commonInit(locHandle, certValdReq, hostType);
		*handle = locHandle;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: commonInit
 *
 * Description	: This function initializes the common curl library options
 *
 *
 * Input Params	:	CURL *
 * 					RCHI_BOOL
 *
 *
 * Output Params: None
 * ============================================================================
 */

static void commonInit(CURL * handle, RCHI_BOOL certValdReq, int hostType)
{

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	RCHI_LIB_TRACE(szDbgMsg);

	 /*Set the NO SIGNAL option, This option is very important to set in case
	 * of multi-threaded applications like ours, because otherwise the libcurl
	 * uses signal handling for the communication and that causes the threads
	 * to go crazy and even crash. Fix done for issue 1659 (Church of LDS AmDocs
	 * Case# 130826-3983)*/
	curl_easy_setopt(handle, CURLOPT_NOSIGNAL, 1L);



	if(hostType == SERVICEDISCOVERY_URL)
	{
		/*Set the HTTP post option*/
		curl_easy_setopt(handle, CURLOPT_HTTPGET, 1L);
	}
	else
	{
		/*Set the HTTP post option*/
		curl_easy_setopt(handle, CURLOPT_POST, 1L);
	}

//	if(getKeepAliveParameter())
	 /*Add the headers*/
	curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headers);

#ifdef DEBUG
	 /*Add the debug function*/
	curl_easy_setopt(handle, CURLOPT_DEBUGFUNCTION, curlDbgFunc);
#endif

	if (certValdReq == RCHI_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Url starts with https. Doing certificate validation",
				__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		curl_easy_setopt(handle, CURLOPT_SSL_VERIFYPEER, 1L);
		curl_easy_setopt(handle, CURLOPT_SSL_VERIFYHOST, 2L);
	}

/*	 Set the CA certificate*/
	curl_easy_setopt(handle, CURLOPT_CAINFO, CA_CERT_FILE);

	 /*Set the write function*/
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, saveResponse);

	 /*Set the detection as TRUE for HTTP errors*/
	curl_easy_setopt(handle, CURLOPT_FAILONERROR, RCHI_TRUE);

	/* To request using TLS for the transfer */
	curl_easy_setopt(handle, CURLOPT_USE_SSL, CURLUSESSL_NONE);

	/* To set preferred TLS/SSL version */
	//curl_easy_setopt(handle, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

	debug_sprintf(szDbgMsg, "%s: --- returning ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	return;
}

/*
 * ============================================================================
 * Function Name: saveResponse
 *
 * Description	: This function is used to store the response
 *
 *
 * Input Params	:	void *
 * 					size_t
 * 					size_t
 * 					void *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static size_t saveResponse(void * ptr, size_t size, size_t cnt, void * des)
{
	int				rv				= SUCCESS;
	int				iSizeLeft		= 0;
	int				totLen			= 0;
	int				iReqdLen		= 0;
	char *			cTmpPtr			= NULL;
	RESP_DATA_PTYPE	respPtr			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	while(1)
	{

		respPtr = (RESP_DATA_PTYPE) des;
		totLen = size * cnt;
		iSizeLeft = respPtr->iTotSize - respPtr->iWritten;

		if(totLen > iSizeLeft)
		{
			iReqdLen = (respPtr->iTotSize + totLen - iSizeLeft) + 1 /*one extra buffer for NULL at the end for safety purpose*/;

			cTmpPtr = (char *) realloc(respPtr->szMsg, iReqdLen);
			if(cTmpPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Reallocation of memory FAILED",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(cTmpPtr + respPtr->iWritten, 0x00, iReqdLen - respPtr->iWritten);

			respPtr->iTotSize = iReqdLen;
			respPtr->szMsg = cTmpPtr;
		}

		memcpy(respPtr->szMsg + respPtr->iWritten, ptr, totLen);
		respPtr->iWritten += totLen;

		rv = totLen;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: sendDataToRCHost
 *
 * Description	: This function sends the XML data to the RCHost
 *
 *
 * Input Params	:	CURL *
 * 					char *
 * 					int
 * 					char **
 * 					int *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int sendDataToRCHost(CURL * curlHandle, char * req, int reqSize,
												char ** resp, int * respSize)
{
	int				rv				   	= SUCCESS;
	int				iRetVal				= -1;
	int				iAppLogEnabled		= 0;
	char			szCurlErrBuf[4096] 	= "";
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";
	char			szErrBuf[128]	    = "";
	RESP_DATA_STYPE	stRespData;

#ifdef DEVDEBUG
	char			szDbgMsg[4096+256]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(chkConn(curlHandle) != SUCCESS)
		{
			rv = ERR_CONN_FAIL;

			sprintf(szAppLogData, "Failed to Connect to RC Host, Error[CURLE_COULDNT_CONNECT]");
			strcpy(szAppLogDiag, "Please Check The RC Host URLs, Otherwise Contact First Data");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);

			break;
		}

/*		 Initialize the response data*/
		rv = initializeRespData(&stRespData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Initialization of response data FAILED"
																, __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}
		 /*Set some curl library options before sending the data*/
		curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &stRespData);
		
		/* Setting the req and its length only if there are existing*/
		if(req && strlen(req) > 0)
		{
			curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, (void *) req);
			curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, reqSize);
		}
		
		curl_easy_setopt(curlHandle, CURLOPT_ERRORBUFFER, szCurlErrBuf);

		debug_sprintf(szDbgMsg, "%s: Posting the data to the server", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		/*post the data to the server*/
		rv = curl_easy_perform(curlHandle);

		debug_sprintf(szDbgMsg, "%s: curl_easy_perform done, rv = %d", __FUNCTION__, rv);
		RCHI_LIB_TRACE(szDbgMsg);
		if(rv == CURLE_OK)
		{
			*resp = stRespData.szMsg;
			*respSize = stRespData.iWritten;
#ifdef DEVDEBUG
			if(strlen(stRespData.szMsg) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: Response Msg = %s\n", __FUNCTION__,
																		stRespData.szMsg);
				RCHI_LIB_TRACE(szDbgMsg);
			}
#endif
			debug_sprintf(szDbgMsg, "%s: Response Len = [%d]", __FUNCTION__,
														stRespData.iWritten);

			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Request Successfully Posted and Received Response Successfully from RC Host");
				addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSING, szAppLogData, NULL);
			}
		}
		else
		{
			iRetVal = res_init();

			debug_sprintf(szDbgMsg, "%s: res_init returned [%d]", __FUNCTION__, iRetVal);
			RCHI_LIB_TRACE(szDbgMsg);

			switch(rv)
			{
			case CURLE_UNSUPPORTED_PROTOCOL: /* 1 */
				/*
				 * The URL you passed to libcurl used a protocol that this libcurl does not support.
				 * The support might be a compile-time option that you didn't use,
				 * it can be a misspelled protocol string or just a protocol libcurl has no code for.
				 */
				debug_sprintf(szDbgMsg, "%s: protocol not supported by library",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_UNSUPPORTED_PROTOCOL]");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Unsupported Protocol");
				rv = FAILURE;
				break;

			case CURLE_URL_MALFORMAT: /* 3 */
				/*
				 * The URL was not properly formatted.
				 */
				debug_sprintf(szDbgMsg, "%s: URL not properly formatted",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_URL_MALFORMAT]");
					strcpy(szAppLogDiag, "Please Check the RC Host URLs Format, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Invalid URL");
				rv = ERR_CFG_PARAMS;
				break;

			case CURLE_COULDNT_RESOLVE_HOST: /* 6 */
				/*
				 * Couldn't resolve host. The given remote host was not resolved.
				 */
				debug_sprintf(szDbgMsg, "%s: Couldnt resolve server's address",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Resolve Host Address, Error[CURLE_COULDNT_RESOLVE_HOST]");
					strcpy(szAppLogDiag, "Please Check the RC Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Host not resolved");
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_COULDNT_CONNECT: /* 7 */
				/*
				 * Failed to connect() to host or proxy.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to connect server",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Connect to RC Host, Error[CURLE_COULDNT_CONNECT]");
					strcpy(szAppLogDiag, "Please Check The RC Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Connection Failed");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_HTTP_RETURNED_ERROR: /* 22 */
				/*
				 * This is returned if CURLOPT_FAILONERROR is set TRUE
				 * and the HTTP server returns an error code that is >= 400.
				 */
				debug_sprintf(szDbgMsg, "%s: Got HTTP error while posting",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_HTTP_RETURNED_ERROR]");
					strcpy(szAppLogDiag, "Please Check the RC Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				
				sprintf(szErrBuf, ":HTTP Error");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_WRITE_ERROR: /* 23 */
				/*
				 * An error occurred when writing received data to a local file,
				 * or an error was returned to libcurl from a write callback.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to save the response data", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_WRITE_ERROR]");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Curl Writing Failed");

				rv = FAILURE;
				break;

			case CURLE_OUT_OF_MEMORY: /* 27 */
				/*
				 * A memory allocation request failed.
				 * This is serious badness and things are severely screwed up if this ever occurs.
				 */
				debug_sprintf(szDbgMsg, "%s: Facing memory shortage ", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_OUT_OF_MEMORY]");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Out of Memory");
				rv = FAILURE;
				break;

			case CURLE_OPERATION_TIMEDOUT: /* 28 */
				/*
				 * Operation timeout.
				 * The specified time-out period was reached according to the conditions.
				 */
				debug_sprintf(szDbgMsg, "%s: Timeout happened while receiving", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Timed Out While Waiting For Response From RC Host");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				sprintf(szErrBuf, ":Curl Timed out");
				
				rv = ERR_RESP_TO;
				break;

			case CURLE_SSL_CONNECT_ERROR: /* 35 */
				/*
				 * A problem occurred somewhere in the SSL/TLS handshake.
				 *
				 */
				debug_sprintf(szDbgMsg, "%s: SSL/TLS Handshake FAILED", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_SSL_CONNECT_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Network, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}


				sprintf(szErrBuf, ":Connection Error");
				
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_PEER_FAILED_VERIFICATION: /* 51 */
				/*
				 * The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK.
				 */
				debug_sprintf(szDbgMsg,
							"%s: Server's SSL certificate verification FAILED",
							__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_PEER_FAILED_VERIFICATION]");
					strcpy(szAppLogDiag, "Please Check the SSL CA Certificate on the Terminal, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":SSL certificate verification FAILED");
				
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SEND_ERROR: /* 55 */
				/*
				 * Failed sending network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to post the request data",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_SEND_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Network, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Posting FAILED");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_RECV_ERROR: /* 56 */
				/*
				 * Failure with receiving network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to receive the response",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_RECV_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Network, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Receiving FAILED");
				rv = ERR_RESP_TO;
				break;

			case CURLE_SSL_CACERT: /* 60 */
				/*
				 * Peer certificate cannot be authenticated with known CA certificates.
				 */
				debug_sprintf(szDbgMsg,
								"%s: Couldnt authenticate peer certificate",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_SSL_CACERT]");
					strcpy(szAppLogDiag, "Please Check the CA Ceritificate File, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Couldnt authenticate peer certificate");
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SSL_CACERT_BADFILE: /* 77 */
				/*
				 * Problem with reading the SSL CA cert (path? access rights?)
				 */
				debug_sprintf(szDbgMsg, "%s: Cant find SSL CA certificate [%s]",
												__FUNCTION__, CA_CERT_FILE);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_SSL_CACERT_BADFILE]");
					strcpy(szAppLogDiag, "Please Check SSL CA Certificate Path, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				
				sprintf(szErrBuf, ":Certificate file not found");

				rv = ERR_CFG_PARAMS;
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Error [%d] occured while posting",
															__FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to RC Host, Error[CURLE_UNSUPPORTED_PROTOCOL]");
					strcpy(szAppLogDiag, "Please Check The RC Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;
			}

			 /*Deallocate the allocated memory*/
			if(stRespData.szMsg != NULL)
			{
				free(stRespData.szMsg);
				stRespData.szMsg = NULL;
			}
#ifdef DEVDEBUG
			if(strlen(szDbgMsg) < 5000)
			{
				debug_sprintf(szDbgMsg, "%s: Lib Curl Error [%s]",__FUNCTION__, szCurlErrBuf);
				RCHI_LIB_TRACE(szDbgMsg);
			}
#endif
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: initializeRespData
 *
 * Description	: This function initializes the structure response structure
 *
 *
 * Input Params	: RESP_DATA_PTYPE
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int initializeRespData(RESP_DATA_PTYPE stRespPtr)
{
	int		rv				= SUCCESS;
	char *	cTmpPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	cTmpPtr = (char *) malloc(4096 * sizeof(char));
	if(cTmpPtr != NULL)
	{
		/* Initialize the memory */
		memset(cTmpPtr, 0x00, 4096 * sizeof(char));
		memset(stRespPtr, 0x00, sizeof(RESP_DATA_STYPE));

		/* Assign the data */
		stRespPtr->iTotSize = 4096;
		stRespPtr->iWritten = 0;
		stRespPtr->szMsg	= cTmpPtr;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: postRegReqToDWHost
 *
 * Description	: This function initializes the curl handle and then sends
 *				  the
 * 				  
 *
 * Input Params	:	char *
 * 					int
 * 					char **
 * 					int *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int postRegReqToDWHost(char * req, int reqSize, char ** resp, int * respSize, int host)
{
	int			rv					= SUCCESS;
	int			iCnt				= 0;
	int			iAppLogEnabled		= 0;
	CURL *		curlHandle			= NULL;
	char		szAppLogData[256]	= "";
	char		szAppLogDiag[256]	= "";

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: RC HOST REQ = [%s]", DEVDEBUGMSG,__FUNCTION__, req);
		RCHI_LIB_TRACE(szDbgMsg);
	}
#endif

	while(iCnt < 2)
	{
		rv = initRCCURLHandle(&curlHandle, host);

		if(rv == SUCCESS)
		{
			 /* CURL Handler was initialized successfully*/

			rv = sendDataToRCHost(curlHandle, req, reqSize, resp, respSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
																__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Post Data to the RC Host");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
		}

		curl_easy_cleanup(curlHandle);

		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Response received is %s",
															__FUNCTION__, *resp);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		iCnt++;
	}
		debug_sprintf(szDbgMsg, "%s: Trying again..", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);

	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateTimeandStanInDWReq(pszDWReq);
 *
 * Description	:
 *
 * Input Params	:  char*		->
 *
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int	updateTimeandStanInDWReq(char* pszDWReq)
{
	int			rv					= SUCCESS;
	char		szTime[20]			= "";
	char		szSTAN[20]			= "";
	char*		tagName				= NULL;
	char*		szStart				= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pszDWReq == NULL)
	{
		return FAILURE;
	}

	tagName = "<LocalDateTime>";
	szStart = strstr(pszDWReq, tagName);
	if(szStart != NULL)
	{
		szStart	= szStart + strlen(tagName);
		getTime(szTime, LOCALTIME);
		strncpy(szStart, szTime, strlen(szTime));
	}
	else
	{
		return FAILURE;
	}

	tagName = "<TrnmsnDateTime>";
	szStart = strstr(pszDWReq, tagName);
	if(szStart != NULL)
	{
		szStart	= szStart + strlen(tagName);
		getTime(szTime, GMTTIME);
		strncpy(szStart, szTime, strlen(szTime));
	}
	else
	{
		return FAILURE;
	}

	tagName = "<STAN>";
	szStart = strstr(pszDWReq, tagName);
	if(szStart != NULL)
	{
		szStart	= szStart + strlen(tagName);
		getSTAN(szSTAN);
		strncpy(szStart, szSTAN, strlen(szSTAN));
	}
	else
	{
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: timeoutReversalThread
 *
 * Description	: This thread will receive the rc host request packet and tries
 * 					to post the packet to the host twice. It ignores after trying for
 * 					two times.
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
static void * emvKeyStatusThread(void * arg)
{
	int				rv 					= SUCCESS;
	int				iAppLogEnabled		= 0;
	int		  		iNextDuration 		= 0;

#ifdef DEBUG
	char            szDbgMsg[1024]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	if(getEmvKeyStatdur())
	{
		iNextDuration = getEmvKeyStatdur();
		debug_sprintf(szDbgMsg, "%s: iNextDur=%d", __FUNCTION__,iNextDuration);
		RCHI_LIB_TRACE(szDbgMsg);
		iNextDuration = iNextDuration * 60;

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: iNextDur Not Set, Setting to 24 Hours", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		iNextDuration = (24 * 60 * 60) + 1000; //Added 1000 secs more just to make that date changes
	}
	while(1)
	{
		rv = checkEMVKeysStat();
		svcWait(iNextDuration*1000);
		debug_sprintf(szDbgMsg, "%s: rv:%d", __FUNCTION__,rv);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return NULL;
}
/*
 * ============================================================================
 * Function Name: timeoutReversalThread
 *
 * Description	: This thread will receive the rc host request packet and tries
 * 					to post the packet to the host twice. It ignores after trying for
 * 					two times.
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
static void * timeoutReversalThread(void * arg)
{
	int				rv 					= SUCCESS;
	int				iRetryCount			= 0;
	int				iDWReqLen			= 0;
	int				iDWRespLen			= 0;
	int				iWaitTime			= 0;
	int				iAppLogEnabled		= 0;
	char			szStatMsg[256]		= "";
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";
	char*			pszDWReq			= NULL;
	char*			pszDWResp			= NULL;
	char*			pszRCHIResp			= NULL;
	char*			pszNodeName			= "Payload";
	time_t			tStartEpochTime 	= 0;
	time_t 			tEndEpochTime  		= 0;
	time_t 			tDiffDeconds    	= 0;

#ifdef DEBUG
	char            szDbgMsg[1024]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: MSG Queue Id is [%d]", __FUNCTION__, msgQID);
	RCHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		debug_sprintf(szDbgMsg, "%s: Waiting for the request", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		//Read data from the message queue
		if(msgrcv(msgQID, &pszDWReq, sizeof(char**), 0, 0) == -1) // IPC system call
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Receive Data From MSG Queue [%d] [%s]", __FUNCTION__, errno, strerror(errno));
			RCHI_LIB_TRACE(szDbgMsg);

			continue; //TODO: Check whether is it valid to do continue.
		}

		iDWReqLen = strlen(pszDWReq);

		/* Code for waiting for total of 40 seconds after the first timeout reversal*/
		if(gWaitTime > 0)
		{
			svcWait(gWaitTime*1000);
			gWaitTime = 0;
		}

		/* Posting the data to the server*/
		while(iRetryCount++ < 2)
		{
			debug_sprintf(szDbgMsg, "%s: Posting the time out reversal request for %d time using", __FUNCTION__, iRetryCount+1);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Updating Reversal(TOR) Request");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			rv = updateTimeandStanInDWReq(pszDWReq);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to update time and stan in request", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed to Update Time and Stan in TOR Request");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone.");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
			}

			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Posting Reversal(TOR) Request for %d time", iRetryCount+1);
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed to post data to RC Host");
					strcpy(szAppLogDiag, "Please check the network connection");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
				}

				rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_NOT_AVAILABLE:
						debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_RESP_TO:
						debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_ERROR:
						debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					}

				}
			}

			/* This is the code for the time out reversal
			 * We need to consider two cases for the timeout reversal
			 * Either the Datawire Server return some error code or
			 * We get response timeout with datawire server itself
			 */
			if(rv != SUCCESS)
			{
				if(rv == ERR_RESP_TO)
				{
					if(iRetryCount < 2)
					{
						/* Setting the end time*/
						tEndEpochTime = time(NULL);

						debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
						RCHI_LIB_TRACE(szDbgMsg);

						tDiffDeconds  = tEndEpochTime - tStartEpochTime;

						debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
						RCHI_LIB_TRACE(szDbgMsg);

						iWaitTime = 40 - tDiffDeconds;

						debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
						RCHI_LIB_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Received Response Time Out ");
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Waitig For %d Seconds To Send Time Out Reversal Request", iWaitTime);
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iWaitTime > 0)
						{
							svcWait(iWaitTime*1000);
						}
					}

					if(pszDWResp != NULL)
					{
						free(pszDWResp);
						pszDWResp = NULL;
					}
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Successfully posted the time out reversal request", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}

		if(pszDWReq != NULL)
		{
			free(pszDWReq);
			pszDWReq = NULL;
		}
		iRetryCount	= 0;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return NULL;
}
/*
 * ============================================================================
 * Function Name: createEMVKeyStatThread
 *
 * Description	: This function handles creation of the thread which periodically checks
 * 				  for any available EMV Keys for update.
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int createEMVKeyStatThread()
{
	int			rv 		= SUCCESS;
	pthread_t	thId		= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	rv = pthread_create(&thId, NULL, emvKeyStatusThread, NULL );
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create time out reversal thread", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Created thread for EMV Key Status", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: createTimeoutReversalThread
 *
 * Description	: This function is responsible for creating the timeout reversal
 * 					thread and create the message queues which will be used for
 *					sending the receiving the request packet.
 * Input Params	:
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int createTimeoutReversalThread()
{
	int			rv 		= SUCCESS;
	key_t 		msgQKey = 1234;
	pthread_t	thId		= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Create a msg queue to send/receive data to print the application logs */
	debug_sprintf(szDbgMsg, "%s: Creating Queue for receving the data for timeout reversal", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	msgQID = createMsgQueue(msgQKey);
	if(msgQID < 0)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create message queue for time out reversal", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		return FAILURE;
	}

	//Its very important to flush the existing data in the message queue.
	flushMsgQueue(msgQID);

	//Display Message Queue Information
	rv = displayMsgQueueInfo(msgQID);
	if( rv != FAILURE)
	{
		debug_sprintf(szDbgMsg, "%s: Current Number of messages on specified queue: %d", __FUNCTION__, rv);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	rv = pthread_create(&thId, NULL, timeoutReversalThread, NULL );
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create time out reversal thread", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Created thread for Time out reversal", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: retryTimeoutReversal
 *
 * Description	: This function is responsible for copying the request data
 * 					and passing it to the thread for the timout reversal
 *
 * Input Params	:
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void retryTimeoutReversal(char* pszMsg, int iWaitTime)
{
	int			rv 			= SUCCESS;
	char*		pszDWReq	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pszMsg != NULL)
	{
		/* Taking a duplicate of the data so that there will not be any double free error*/
		pszDWReq = strdup(pszMsg);
	}
	else
	{
		return;
	}

	if(iWaitTime > 0)
	{
		gWaitTime = iWaitTime;
	}

	/*Add Application Log Data to the Message Queue*/
	rv = msgsnd(msgQID, &pszDWReq, sizeof(char**), IPC_NOWAIT);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to send Data to Message Queue, [%d] [%s] ",__FUNCTION__, errno, strerror(errno));
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: lock_callback
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static void lock_callback(int mode, int type, char *file, int line)
{
#ifdef DEBUG
	//char	szDbgMsg[512]		= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//RCHI_LIB_TRACE(szDbgMsg);

  (void)file;
  (void)line;
  if (mode & CRYPTO_LOCK)
  {
	  //debug_sprintf(szDbgMsg, "%s: RC Mutex Locking", __FUNCTION__);
	  //RCHI_LIB_TRACE(szDbgMsg);
	  pthread_mutex_lock(&(lockarray[type]));
  }
  else
  {
	  //debug_sprintf(szDbgMsg, "%s:  RC Mutex Un-Locking", __FUNCTION__);
	  //RCHI_LIB_TRACE(szDbgMsg);

	  pthread_mutex_unlock(&(lockarray[type]));
  }

  //debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
  //RCHI_LIB_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: thread_id
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static unsigned long thread_id(void)
{
  unsigned long ret;

#ifdef DEBUG
	//char	szDbgMsg[512]		= "";
#endif

  ret=(unsigned long)pthread_self();

//  debug_sprintf(szDbgMsg, "%s: Returning [%ld]", __FUNCTION__, ret);
//  RCHI_LIB_TRACE(szDbgMsg);

  return(ret);
}

/*
 * ============================================================================
 * Function Name: initializeOpenSSLLocks
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static int initializeOpenSSLLocks()
{
	int		rv					= SUCCESS;
	int		iIndex				= 0;

#ifdef DEBUG
	char	szDbgMsg[512]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	lockarray=(pthread_mutex_t *)OPENSSL_malloc(CRYPTO_num_locks() *
	                                            sizeof(pthread_mutex_t));

	 for (iIndex =0; iIndex < CRYPTO_num_locks(); iIndex++)
	 {
	    pthread_mutex_init(&(lockarray[iIndex]),NULL);
	 }

	 CRYPTO_set_id_callback((unsigned long (*)())thread_id);
	 CRYPTO_set_locking_callback((void (*)())lock_callback);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

#ifdef DEBUG
/*
 * ============================================================================
 * Function Name: curlDbgFunc
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static int curlDbgFunc(CURL * handle, curl_infotype infoType, char *msg,
														size_t size, void * arg)
{
	char	szDbgMsg[4096]	= "";
	switch(infoType)
	{
	case CURLINFO_TEXT:
		sprintf(szDbgMsg, "%s: info-text: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_HEADER_IN:
		sprintf(szDbgMsg, "%s: info-header in: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_HEADER_OUT:
		sprintf(szDbgMsg, "%s: info-header out: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_DATA_IN:
		sprintf(szDbgMsg, "%s: info-data in: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_DATA_OUT:
		sprintf(szDbgMsg, "%s: info-data out: %s", __FUNCTION__, msg);
		break;

	default:
		sprintf(szDbgMsg, "%s: info-default: %s", __FUNCTION__, msg);
		break;
	}

	RCHI_LIB_TRACE(szDbgMsg);

	return 0;
}
#endif

/*
 * ============================================================================
 * Function Name: convertHostNameToIP
 *
 * Description	: This function sends the XML data to the FEXCOHost
 *
 *
 * Input Params	:	HostName and the IP
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int convertHostNameToIP(char * hostname , char* ip)
{
    struct hostent 		*he;
    struct in_addr 		**addr_list;
    int 				i;

    if ( (he = gethostbyname( hostname ) ) == NULL)
    {
        return 1;
    }

    addr_list = (struct in_addr **) he->h_addr_list;

    for(i = 0; addr_list[i] != NULL; i++)
    {
        //Return the first one;
        strcpy(ip , inet_ntoa(*addr_list[i]) );
        return 0;
    }

    return 1;
}

/*
 * ============================================================================
 * Function Name: writen
 *
 * Description	: Function which writes the data into the socket
 *
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

ssize_t						/* Write "n" bytes to a descriptor. */
writen(int fd, const void *vptr, size_t n)
{
	size_t		nleft;
	ssize_t		nwritten;
	const char	*ptr;

	ptr = vptr;
	nleft = n;
	while (nleft > 0) {
		if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
			if (nwritten < 0 && errno == EINTR)
				nwritten = 0;		/* and call write() again */
			else
				return(-1);			/* error */
		}

		nleft -= nwritten;
		ptr   += nwritten;
	}
	return(n);
}

/*
 * ============================================================================
 * Function Name: postDataToFEXCOHost
 *
 * Description	: This function sends the XML data to the FEXCOHost
 *
 *
 * Input Params	:	char *
 * 					int
 * 					char **
 * 					int *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int postDataToFEXCOHost(char * req, int reqSize, char ** resp, int * respSize)
{
		int					rv				   	= SUCCESS;
		int					iBytesRead			= 0;
		int					iTotBytes			= 0;
		int					iRetryCount			= 100;
		int					iSockFd				= -1;
		int 				numDesc;
		int					iAppLogEnabled		= 0;
		char				szAppLogData[256]	= "";
		char				szAppLogDiag[256]	= "";
static	char				szFexcoIP[20]		= "";
static  RCHI_BOOL			bFirstTime			= RCHI_TRUE;
		char* 				pszBuffer			= NULL;
		struct 				sockaddr_in 		fexcoServaddr;
		fd_set	 wset,rset;
		struct timeval timeOut;
		RESP_DATA_STYPE		stRespData;
		HOSTDEF_STRUCT_TYPE	stFexcoHost;

#ifdef DEVDEBUG
	char			szDbgMsg[4096+256]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		/*		 Initialize the response data*/
		rv = initializeRespData(&stRespData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Initialisation of response data FAILED"
					, __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		getRCHostDetails(FEXCO_URL, &stFexcoHost);

		if(bFirstTime)
		{
			convertHostNameToIP(stFexcoHost.url, szFexcoIP);
			bFirstTime = RCHI_FALSE;
		}

		if ( (iSockFd = socket ( AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0 )) < 0 )
		{												/*Creating the socket*/
			perror ("Cannot create a socket");
			exit(1);
		}

		bzero (&fexcoServaddr, sizeof(fexcoServaddr) );
		fexcoServaddr.sin_family =  AF_INET;
		fexcoServaddr.sin_port = htons(stFexcoHost.portNum);

		if ( inet_pton ( AF_INET, szFexcoIP, &fexcoServaddr.sin_addr ) < 0 )
		{									/*Assigning the ipaddress*/
			debug_sprintf (szDbgMsg, "Inet pton error for: %s", szFexcoIP);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}


		rv = connect (iSockFd, ( const struct sockaddr* ) &fexcoServaddr, sizeof(fexcoServaddr) );

		debug_sprintf(szDbgMsg, "%s: Connect to FEXCO server Result: [%d] s:[%s]", __FUNCTION__, errno, strerror(errno));
		RCHI_LIB_TRACE(szDbgMsg);

		if((rv != SUCCESS) && (errno == 115)) // Only if Error indicates EINPROGRESS
		{
			FD_ZERO(&wset);
			FD_SET(iSockFd, &wset);
			memset(&timeOut, 0x00, sizeof(timeOut));
			timeOut.tv_sec =  stFexcoHost.conTimeOut;
			timeOut.tv_usec = 0;

			// Check if socket is ready to write
			if ( (numDesc = select(iSockFd+1, NULL, &wset, NULL, &timeOut)) == 0)
			{
				errno = ETIMEDOUT; 	/* timeout */
				rv = FAILURE;
				debug_sprintf(szDbgMsg, "%s: Select Timedout", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			if (FD_ISSET(iSockFd, &wset))
			{
				debug_sprintf(szDbgMsg, "%s: Able to connect to socket", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				rv = SUCCESS;

			}
			else
			{
				rv = FAILURE;
				debug_sprintf(szDbgMsg, "%s: Unable to connect to socket", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);;
			}
		}

		if ( rv < 0)
		{
			sprintf(szAppLogData, "Connect Error: %s", strerror(errno));
			RCHI_LIB_TRACE(szAppLogData);

			sprintf(szAppLogDiag, "Please Check Your Connection. Contact FEXCO");

			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);

			rv = ERR_HOST_NOT_AVAILABLE;

			break;
		}

		if ( writen ( iSockFd, req, reqSize+1 ) < 0 )
		{
			sprintf(szAppLogData, "Write Error: %s", strerror(errno));
			RCHI_LIB_TRACE(szAppLogData);

			sprintf(szAppLogDiag, "Please Contact Fexco");

			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);

			rv = ERR_HOST_ERROR;

			break;
		}

		pszBuffer 	= stRespData.szMsg;
		//iRetryCount = stFexcoHost.respTimeOut * 20;
		FD_ZERO(&rset);
		FD_SET(iSockFd, &rset);
		memset(&timeOut, 0x00, sizeof(timeOut));
		timeOut.tv_sec =  stFexcoHost.respTimeOut;
		timeOut.tv_usec = 0;

		//Check if socket is ready for read
		if ( (numDesc = select(iSockFd+1, &rset, NULL, NULL, &timeOut)) == 0)
		{
			errno = ETIMEDOUT; 	/* timeout */
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Select Timedout", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		if (FD_ISSET(iSockFd, &rset))
		{
			debug_sprintf(szDbgMsg, "%s: Socket ready to be read", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = SUCCESS;

		}
		else
		{
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Nothing to read from socket", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);;
		}
		while((rv == SUCCESS))
		{
			iBytesRead = recv(iSockFd, pszBuffer, stRespData.iTotSize, MSG_DONTWAIT );
			if(iBytesRead > 0)
			{
				iRetryCount = 0;
				while(iRetryCount < 5)
				{
					if(iBytesRead > 0)
					{
						if(iTotBytes + iBytesRead <= stRespData.iTotSize)
						{
							pszBuffer += iBytesRead;
							iTotBytes += iBytesRead;
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Exceeding Maximum bytes read!!!", __FUNCTION__);
							RCHI_LIB_TRACE(szDbgMsg);

							break;
						}
					}

					iBytesRead = recv(iSockFd, pszBuffer, stRespData.iTotSize - iTotBytes, MSG_DONTWAIT);
					iRetryCount++;
					svcWait(10);
				}

				break;
			}
			else
			{
				/* Recv got interrupted by a signal perhaps, since it is not a
				 * fatal error, reading on the socket can be tried again a couple
				 * of times */
				/* Recv failed because of some fatal error */
				debug_sprintf(szDbgMsg, "%s: recv FAILED - error = [%d]", __FUNCTION__, errno);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		if(rv < 0)
		{
			rv = ERR_RESP_TO;
		}
		*resp = stRespData.szMsg;
		*respSize = iTotBytes;

		break;
	}

	if(iSockFd)
	{
		close(iSockFd);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

