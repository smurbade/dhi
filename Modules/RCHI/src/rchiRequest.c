/*
* rchiRequest.c
*
*  Created on: 17-Dec-2014
*      Author: BLR_SCA_TEAM
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <svc.h>
#include <iniparser.h>
#include <errno.h>
//#include "tcpipdbg.h""

#include "RCHI/rchiCommon.h"
#include "RCHI/metaData.h"
#include "RCHI/rchiGroups.h"
#include "RCHI/xmlUtils.h"
#include "RCHI/rchi.h"
#include "RCHI/rchiResp.h"
#include "RCHI/rchiHostCfg.h"
#include "RCHI/rchiCfg.h"
#include "DHI_APP/appLog.h"
#include "RCHI/utils.h"


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		rchiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         rchistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  rchistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	rchiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			rchiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)



#define MAX_SIZE			256
#define CONFIG_USR_FILE 	"/mnt/flash/config/config.usr1"
#define MAX_CONFIG_PARAMS	3

static RCHI_BOOL isSAFTranProcessRegd(TRANDTLS_PTYPE );
static int preProcessSAFTransaction(TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
//static RCHI_BOOL isHostConnected();

extern int checkHostConn(char * req, int reqSize, char ** resp, int * respSize);
static pthread_mutex_t gpEMVAdminStructAcessMutex;
static pthread_mutex_t gpConfigParamsAcessMutex;
static int initiateEMVDownload();
static int handleEMVDownload(TRANDTLS_PTYPE , RCTRANDTLS_PTYPE );
/*
* ============================================================================
* Function Name: processCreditTransaction
*
* Description	:
*
* Input Params	: Structure
*
* Output Params:
* ============================================================================
*/
int	processCreditTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int						rv 										= SUCCESS;
	int						iCmd									= -1;
	int						iLen									= 0;
	int						iDWReqLen								= 0;
	int						iDWRespLen								= 0;
	int						iWaitTime								= 0;
	int						iAppLogEnabled							= 0;
	char					szStatMsg[256]							= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszMsg	 								= NULL;
	char*					pszDWReq								= NULL;
	char*					pszDWResp								= NULL;
	char*					pszRCHIResp								= NULL;
	char*					pszRequestType							= "CreditRequest";
	char*					pszNodeName								= "Payload";
	time_t					tStartEpochTime  						= 0;
	time_t 					tEndEpochTime  							= 0;
	time_t 					tDiffDeconds       						= 0;
	RCHI_BOOL				bTORRequest								= RCHI_FALSE;
	RCHI_BOOL				bTORFirstTime							= RCHI_TRUE;
	RCHI_BOOL				bSAFTran								= RCHI_FALSE;
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls							= getPassthroughFields();
	RCTRANDTLS_STYPE		pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];
	METADATA_STYPE			stMeta;

#ifdef DEVDEBUG
	int				iIndex			= 0;
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Resetting all the fields in the structure for the new transaction*/
	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));
	while(1)
	{

		bSAFTran = isSAFTranProcessRegd(pstDHIDetails);
		if(bSAFTran)
		{
			debug_sprintf(szDbgMsg, "%s: It is a SAF Transaction", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			if(pstDHIDetails[eCOMMAND].elementValue != NULL)
			{
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Processing SAF Credit %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
			rv = preProcessSAFTransaction(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to process SAF Transaction!!!", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}
			debug_sprintf(szDbgMsg, "%s: Successfully processed SAF Transaction", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: It is a Non-SAF Transaction", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);//For Testing purpose, remove it later
		}

		iCmd = getTransType(pstDHIDetails[eCOMMAND].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue, "Credit");
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure, cant process the transaction!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found");
				strcpy(szAppLogDiag, "Internal DHI-RCHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = iCmd;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Credit %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case RC_SALE:
		case RC_AUTH:
		case RC_REFUND:
		case RC_TOKEN_QUERY:
		case RC_EMV_DOWNLOAD:

			if(iCmd == RC_TOKEN_QUERY)
			{
				pszRequestType = "TransArmorRequest";
			}

			if(iCmd == RC_EMV_DOWNLOAD )
			{
				rv = handleEMVDownload(pstDHIDetails,pstRCDetails);
				break;
			}
			/*fill rc details*/
			rv = fillRCDtls(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to correct RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			break;

		case RC_VOID:
			pszRequestType = "ReversalRequest";

			/* Copying the Void into the reversal reason code*/
			strcpy(pstRCDetails[RC_REVERSL_RSN_CODE].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue);
			rv = fillRCDtlsforFollowOnTran(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill DHI or RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			break;

		case RC_COMPLETION:

			rv = fillRCDtlsforFollowOnTran(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill DHI or RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
			/*
			 * Copying the transaction type again as the actual tran is copied in the
			 * the field.
			 */
			strcpy(pstRCDetails[RC_TRAN_TYPE].elementValue, "Completion");
			break;

		/* If voice approval code is present */
		case RC_VOICE_COMPLETION:
			rv = preProcessSAFTransaction(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to process Voice Completion Transaction!!!", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}
			debug_sprintf(szDbgMsg, "%s: Successfully processed Voice Completion Transaction", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			break;

		case RC_ACTIVATE:
		case RC_REDEMPTION:
		case RC_BALANCE:
		case RC_CASH_OUT:
		case RC_HOST_TOTALS:
			rv = ERR_INV_COMBI;
			break;

		default:
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		else if((pstDHIDetails[eCOMMAND].elementValue != NULL && !strcmp(pstDHIDetails[eCOMMAND].elementValue, "POST_AUTH")) || (iCmd == RC_EMV_DOWNLOAD))
		{
			break;
		}
		while(1)
		{

			memset(&stMeta, 0x00, METADATA_SIZE);

			/* First Data is expecting all credit transactions ( Sale and Refund ) to have the Pinless Debit flag.
			 * But when Pin Block is present in the transaction request we are not supposed to add this flag.
			 * Pinless debit flag with manual/token transactions returns an invalid response from Host. So avoid sending
			 * this flag for such transactions.
			 */
			if(isForcePLDebitFlagSet())
			{
				if((iCmd == RC_SALE/* || iCmd == RC_REFUND*/) && !pstDHIDetails[ePIN_BLOCK].elementValue && !pstDHIDetails[ePINLESSDEBIT].elementValue && strcmp(pstRCDetails[RC_POS_ENTRY_MODE].elementValue, "011") && (strlen(pstRCDetails[RC_ADDTL_AMT_CASHBK].elementValue) == 0))
				{
					if(pstDHIDetails[eTRACK_INDICATOR].elementValue != NULL && !strcmp(pstDHIDetails[eTRACK_INDICATOR].elementValue,"1"))
					{
						//Dont add the Pin less debit flag where Track 1 Data is being sent.
					}
					else
					{
						pstDHIDetails[ePINLESSDEBIT].elementValue = strdup("1");
					}
				}
			}
			if(iCmd == RC_REFUND && pstDHIDetails[ePINLESSDEBIT].elementValue != NULL)
			{
				free(pstDHIDetails[ePINLESSDEBIT].elementValue);
				pstDHIDetails[ePINLESSDEBIT].elementValue = NULL;
			}
			rv = getMetaDataForRCReq(&stMeta, pstDHIDetails, pstRCDetails, CREDIT);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}


			rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
						__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
					__FUNCTION__, iLen);
			RCHI_LIB_TRACE(szDbgMsg);


#ifdef DEVDEBUG
			if(strlen(pszMsg) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
				RCHI_LIB_TRACE(szDbgMsg);
			}
#endif

			/*	Free the meta data*/
			freeMetaDataEx(&stMeta);

			rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
			}

			/* Updating config variables value in config.usr1 */
			rv = updateConfigParams();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}

			/* Setting the parameter that RC lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			debug_sprintf(szDbgMsg, "%s: Start Epoch time in Seconds is %ld", __FUNCTION__, tStartEpochTime);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
				}

				rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_NOT_AVAILABLE:
						debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_RESP_TO:
						debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_ERROR:
						debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
			}

			/* This is the code for the time out reversal
			 * We need to consider two cases for the timeout reversal
			 * Either the Datawire Server return some error code or
			 * We get response timeout with datawire server itself
			 */
			if(rv != SUCCESS)
			{
				if(rv == ERR_RESP_TO)
				{
					/* Setting the end time*/
					tEndEpochTime = time(NULL);
					debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
					RCHI_LIB_TRACE(szDbgMsg);
					tDiffDeconds  = tEndEpochTime - tStartEpochTime;
					debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
					RCHI_LIB_TRACE(szDbgMsg);
					iWaitTime = 40 - tDiffDeconds;
					debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
					RCHI_LIB_TRACE(szDbgMsg);

					if(bTORFirstTime == RCHI_TRUE)
					{
						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Received Response Time Out, Waiting For %d Seconds To Send Time Out Reversal Request", iWaitTime);
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Sending Time Out Reversal Request For 1st time");
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iWaitTime > 0)
						{
							svcWait(iWaitTime*1000);
						}

						if(pszMsg != NULL)
						{
							free(pszMsg);
							pszMsg = NULL;
						}

						if(pszDWReq != NULL)
						{
							free(pszDWReq);
							pszDWReq = NULL;
						}

						/* Need to time out reversal now*/
						pszRequestType = "ReversalRequest";

						rv = fillRCDtlsforTOR(pstDHIDetails, pstRCDetails, iCmd);
						if(rv == SUCCESS)
						{
							bTORRequest = RCHI_TRUE;
							bTORFirstTime = RCHI_FALSE;
							continue;
						}
					}
					else
					{
						/* Retrying the timeout reversal for another two times*/
						retryTimeoutReversal(pszDWReq, iWaitTime);

						rv = ERR_HOST_TRAN_TIMEOUT;
						break;
					}
				}	/*KranthiK1: Added this change so that we do not saf the actual transaction when we get conn failure for TOR*/
				else if (bTORRequest && rv == ERR_CONN_FAIL)
				{
					/* Retrying the timeout reversal for another two times*/
					retryTimeoutReversal(pszDWReq, iWaitTime);

					rv = ERR_HOST_TRAN_TIMEOUT;
					break;
				}
			}
			else
			{
				rv = parseRCResponse(pszRCHIResp, pstDHIDetails, pstRCDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}

			}
			break;
		}

		break;
	}

	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
	{
		if(bTORRequest == RCHI_TRUE)
		{
			free(pstDHIDetails[eRESULT_CODE].elementValue);
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("22");

			if(pstDHIDetails[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstDHIDetails[eRESPONSE_TEXT].elementValue);
				pstDHIDetails[eRESPONSE_TEXT].elementValue =  strdup("Transaction Reversed");
			}
		}
		else
		{
			if(iCmd != RC_EMV_DOWNLOAD)
			{
				saveTranDetailsForFollowOnTran(pstDHIDetails, pstRCDetails);
			}
		}
	}

	if(rv != SUCCESS)
	{
		fillErrorDtls(rv, pstDHIDetails, szStatMsg);
	}
	else
	{

		/* If key Update is available and if this is Response for SALE/Refund Transaction then we should add EMV_UPDATE_FIELD as Y
		 * If the EMV_UPDATE flag can be returned in the responses of other transactions, then we can add them to the switch statement below.
		 */

		if(gstEMVAdmin.iKeyUpdAvl)
		{
			switch(iCmd)
			{
				case RC_SALE:
				case RC_REFUND:
					pstDHIDetails[eEMV_UPDATE].elementValue = strdup("Y");
					break;

				default:
					debug_sprintf(szDbgMsg, "%s: Not a valid command to return the Key Update Flag", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
			}
		}

	}


#ifdef DEVDEBUG
		for(iIndex = 0; iIndex < eELEMENT_INDEX_MAX; iIndex++)
		{
			if(pstDHIDetails[iIndex].elementValue != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: DHI[%d] Value = [%s]", __FUNCTION__, iIndex, pstDHIDetails[iIndex].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
#endif

	if(pszMsg != NULL)
	{
		free(pszMsg);
		pszMsg = NULL;
	}

	if(pszDWReq != NULL)
	{
		free(pszDWReq);
		pszDWReq = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
* ============================================================================
* Function Name: processDebitTransaction
*
* Description	:
*
* Input Params	: Structure
*
* Output Params:
* ============================================================================
*/
int	processDebitTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int						rv 										= SUCCESS;
	int						iCmd									= -1;
	int						iLen									= 0;
	int						iDWReqLen								= 0;
	int						iWaitTime								= 0;
	int						iDWRespLen								= 0;
	int						iAppLogEnabled							= 0;
	char					szStatMsg[256]							= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszMsg	 								= NULL;
	char*					pszRequestType							= "DebitRequest";
	char*					pszDWReq								= NULL;
	char*					pszDWResp								= NULL;
	char*					pszRCHIResp								= NULL;
	char*					pszNodeName								= "Payload";
	time_t					tStartEpochTime  						= 0;
	time_t 					tEndEpochTime  							= 0;
	time_t 					tDiffDeconds       						= 0;
	RCHI_BOOL				bTORRequest								= RCHI_FALSE;
	RCHI_BOOL				bTORFirstTime							= RCHI_TRUE;
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls							= getPassthroughFields();
	RCTRANDTLS_STYPE		pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];
	METADATA_STYPE			stMeta;

#ifdef DEVDEBUG
	int				iIndex			= 0;
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/*
	 * Allocating the memory for filling the RC Details
	 */
	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));

	while(1)
	{
		iCmd = getTransType(pstDHIDetails[eCOMMAND].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue, "Debit");
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure, cant process the transaction!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found");
				strcpy(szAppLogDiag, "Internal DHI-RCHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = iCmd;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Debit %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case RC_SALE:
		case RC_REFUND:
		case RC_TOKEN_QUERY:

			if(iCmd == RC_TOKEN_QUERY)
			{
				pszRequestType = "TransArmorRequest";
			}
			/*fill rc details*/
			rv = fillRCDtls(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to correct RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			break;

		case RC_VOID:
			pszRequestType = "ReversalRequest";

			/* Copying the Void into the reversal reason code*/
			strcpy(pstRCDetails[RC_REVERSL_RSN_CODE].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue);

			rv = fillRCDtlsforFollowOnTran(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill DHI or RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
			break;

		case RC_AUTH:
		case RC_COMPLETION:
		case RC_ACTIVATE:
		case RC_REDEMPTION:
		case RC_BALANCE:
		case RC_CASH_OUT:
		case RC_HOST_TOTALS:
		case RC_VOICE_COMPLETION:
		case RC_EMV_DOWNLOAD:

			rv = ERR_INV_COMBI;
			break;

		default:
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		while(1)
		{
			memset(&stMeta, 0x00, METADATA_SIZE);

			rv = getMetaDataForRCReq(&stMeta, pstDHIDetails, pstRCDetails, DEBIT);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta data for the basket",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}

			rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
						__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
					__FUNCTION__, iLen);
			RCHI_LIB_TRACE(szDbgMsg);


#ifdef DEVDEBUG
			if(strlen(pszMsg) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
				RCHI_LIB_TRACE(szDbgMsg);
			}
#endif

			/*	Free the meta data*/
			freeMetaDataEx(&stMeta);

			rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
			}
			/* Updating config variables value in config.usr1 */
			rv = updateConfigParams();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}


			/* Setting the parameter that RC lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");


			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			debug_sprintf(szDbgMsg, "%s: Start Epoch time in Seconds is %ld", __FUNCTION__, tStartEpochTime);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
				}

				rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_NOT_AVAILABLE:
						debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_RESP_TO:
						debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_ERROR:
						debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
			}

			/* This is the code for the time out reversal
			 * We need to consider two cases for the timeout reversal
			 * Either the Datawire Server return some error code or
			 * We get response timeout with datawire server itself
			 */
			if(rv != SUCCESS)
			{
				if(rv == ERR_RESP_TO)
				{
					/* Setting the end time*/
					tEndEpochTime = time(NULL);

					debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
					RCHI_LIB_TRACE(szDbgMsg);

					tDiffDeconds  = tEndEpochTime - tStartEpochTime;

					debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
					RCHI_LIB_TRACE(szDbgMsg);

					iWaitTime = 40 - tDiffDeconds;

					debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
					RCHI_LIB_TRACE(szDbgMsg);

					if(bTORFirstTime == RCHI_TRUE)
					{
						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Received Response Time Out, Waitig For %d Seconds To Send Time Out Reversal Request", iWaitTime);
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Sending Time Out Reversal Request For 1st Time");
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iWaitTime > 0)
						{
							svcWait(iWaitTime*1000);
						}

						if(pszMsg != NULL)
						{
							free(pszMsg);
							pszMsg = NULL;
						}

						if(pszDWReq != NULL)
						{
							free(pszDWReq);
							pszDWReq = NULL;
						}

						/* Need to time out reversal now*/
						pszRequestType = "ReversalRequest";

						rv = fillRCDtlsforTOR(pstDHIDetails, pstRCDetails, iCmd);
						if(rv == SUCCESS)
						{
							bTORRequest = RCHI_TRUE;
							bTORFirstTime = RCHI_FALSE;
							continue;
						}
					}
					else
					{
						/* Need to create a thread which sends time out reversal for another two times*/
						retryTimeoutReversal(pszDWReq, iWaitTime);
						rv = ERR_HOST_TRAN_TIMEOUT;
						break;
					}
				}	/*KranthiK1: Added this change so that we do not saf the actual transaction when we get conn failure for TOR*/
				else if (bTORRequest && rv == ERR_CONN_FAIL)
				{
					/* Retrying the timeout reversal for another two times*/
					retryTimeoutReversal(pszDWReq, iWaitTime);

					rv = ERR_HOST_TRAN_TIMEOUT;
					break;
				}
			}
			else
			{
				rv = parseRCResponse(pszRCHIResp, pstDHIDetails, pstRCDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}
			break;
		}

		break;
	}

	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
	{
		if(bTORRequest == RCHI_TRUE)
		{
			free(pstDHIDetails[eRESULT_CODE].elementValue);
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("22");

			if(pstDHIDetails[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstDHIDetails[eRESPONSE_TEXT].elementValue);
				pstDHIDetails[eRESPONSE_TEXT].elementValue =  strdup("Transaction Reversed");
			}
		}
		else
		{
			saveTranDetailsForFollowOnTran(pstDHIDetails, pstRCDetails);
		}
	}

	if(rv != SUCCESS)
	{
		fillErrorDtls(rv, pstDHIDetails, szStatMsg);
	}
	else
	{
		/* If key Update is available and if this is Response for SALE/Refund Transaction then we should add EMV_UPDATE_FIELD as Y
		 * If the EMV_UPDATE flag can be returned in the responses of other transactions, then we can add them to the switch statement below.
		 */

		if(gstEMVAdmin.iKeyUpdAvl)
		{
			switch(iCmd)
			{
				case RC_SALE:
				case RC_REFUND:
						pstDHIDetails[eEMV_UPDATE].elementValue = strdup("Y");
					break;

				default:
					debug_sprintf(szDbgMsg, "%s: Not a valid command to return the Key Update Flag", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
			}
		}

	}
#ifdef DEVDEBUG
		for(iIndex = 0; iIndex < eELEMENT_INDEX_MAX; iIndex++)
		{
			if(pstDHIDetails[iIndex].elementValue != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: DHI[%d] Value = [%s]", __FUNCTION__, iIndex, pstDHIDetails[iIndex].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
#endif

	if(pszMsg != NULL)
	{
		free(pszMsg);
		pszMsg = NULL;
	}

	if(pszDWReq != NULL)
	{
		free(pszDWReq);
		pszDWReq = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}
/*
* ============================================================================
* Function Name: isRchiDevRegAllowed
*
* Description	: Checks whether we must go ahead with device registration or return
* Result code 31 in SSI response , so that SCA requests for checking MID/TID and other
* required configuration for Device Registration
*
* Input Params	:
*
* Output Params:
* ============================================================================
*/
RCHI_BOOL isRchiDevRegAllowed()
{
	int rv = RCHI_TRUE;
	char termId[50];
	char merchId[50];

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	/* Get Configured Merchant ID and Terminal ID.
	 * And verify whether we can go ahead with Device Registration.
	 */
	getTerminalID(termId);
	getMerchantID(merchId);

	while(1)
	{
		if((strlen(termId) == 0) || (strlen(merchId) == 0))
		{
			debug_sprintf(szDbgMsg, "%s: --- Dev Reg Not allowed ---", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv =  RCHI_FALSE;

			break;
		}

		if(strspn(merchId, "0123456789") == strlen(merchId) && atoi(merchId) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: --- Dev Reg Not allowed ---", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv =  RCHI_FALSE;
			break;
		}

		if(strspn(termId, "0123456789") == strlen(termId) && atoi(termId) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: --- Dev Reg Not allowed ---", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			rv =  RCHI_FALSE;
			break;
		}
		break;
	}


	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
* ============================================================================
* Function Name: processDeviceRegistration
*
* Description	:
*
* Input Params	: Structure
*
* Output Params:
* ============================================================================
*/
int	processDeviceRegistration(TRANDTLS_PTYPE pstDHIDetails)
{
	int				rv 					= SUCCESS;
	int				iDWReqLen			= 0;
	int				iDWRespLen			= 0;
	int				iAppLogEnabled		= 0;
	char			szStatMsg[256]		= "";
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";
	char			szDID[20]			= "";
	char*			pszDWReq			= "";
	char*			pszDWResp			= NULL;
	char* 			pszNodeNames[3]		= {NULL};
	char*			pszNodeVal[3]		= {NULL};

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Processing Device Registration Command");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	while(1)
	{
		/*Step 0:Check whether we have MID/TID and any other parameter required to do a device registration
		 */

		if(isRchiDevRegAllowed() != RCHI_TRUE)
		{
			// Return ERR_DEV_REG_DATA_INSUFF since Dev Registration Should not go through without valid Merchant ID or Terminal ID.
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("31");
			rv = SUCCESS;
			break;
		}

		/*
		* Step 1: Send the service discovery request
		*/
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing the first step in Device Registration Command: Service Discovery");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s:Processing the first step in Device Registration Command: Service Discovery", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = postRegReqToDWHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen, SERVICEDISCOVERY_URL);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Posting request to the DW host failed", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Posting request to the DW host failed.");
				strcpy(szAppLogDiag, "Connection Error, Please check connection settings.");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Service Discovery is a success", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Response received is %s", __FUNCTION__, pszDWResp);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
			}

			pszNodeNames[0] = "URL";

			rv = getNodeValFromDWRegResp(pszDWResp, pszNodeNames, pszNodeVal, 1, szStatMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Received Failure for Service Discovery", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Received Failure for Service Discovery");
					strcpy(szAppLogDiag, "Please Contact Admin.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}
			else
			{
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Registration Url received during service discovery is %s", pszNodeVal[0]);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RECEIVE, szAppLogData, NULL);
				}

				setRCHIHostUrl(REGISTRATION_URL, pszNodeVal[0]);
				free(pszNodeVal[0]);
				pszNodeVal[0] = NULL;
			}
		}

		if(pszDWResp != NULL)
		{
			xmlFree(pszDWResp);
			pszDWResp = NULL;
		}

		/*
		* Step 2: Send the registration request if it is a success
		* then save the did and url and then send for activation
		*/
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing the Second step in Device Registration Command: Registration");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s: Processing the Second step in Device Registration Command: Registration", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, NULL, 0, REGISTRATION);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed.");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone.");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
		}

		/* Updating config variables value in config.usr1 */
		rv = updateConfigParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		rv = postRegReqToDWHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen, REGISTRATION_URL);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Posting request to the DW host failed.");
				strcpy(szAppLogDiag, "Connection Error, Please check connection settings.");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}
		else
		{

			debug_sprintf(szDbgMsg, "%s: Response received for registration is %s", __FUNCTION__, pszDWResp);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
			}

			pszNodeNames[0] = "DID";
			pszNodeNames[1]	= "URL";
			pszNodeNames[2]	= "URL";

			rv = getNodeValFromDWRegResp(pszDWResp, pszNodeNames, pszNodeVal, 3, szStatMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:  Received Error Response From Host for Registration", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, " Received Error Response From Host for Registration.");
					strcpy(szAppLogDiag, "Please Contact Admin.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
				/*Do something here*/
			}
			else
			{
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Device Registration Successful");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
				if(pszNodeVal[0] != NULL)
				{
					setDatawireID(pszNodeVal[0]);
					
					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "DID received is %s", pszNodeVal[0]);
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
					}

					debug_sprintf(szDbgMsg, "%s: DID received is %s", __FUNCTION__, pszNodeVal[0]);
					RCHI_LIB_TRACE(szDbgMsg);
					
					free(pszNodeVal[0]);
					pszNodeVal[0] = NULL;
				}

				if(pszNodeVal[1] != NULL)
				{
					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Primary Url received is %s", pszNodeVal[1]);
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
					}					
					
					debug_sprintf(szDbgMsg, "%s: Primary Url received is %s ", __FUNCTION__, pszNodeVal[1]);
					RCHI_LIB_TRACE(szDbgMsg);

					setRCHIHostUrl(PRIMARY_URL, pszNodeVal[1]);

					free(pszNodeVal[1]);
					pszNodeVal[1] = NULL;
				}

				if(pszNodeVal[2] != NULL)
				{
					
					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Secondary Url received is %s", pszNodeVal[2]);
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
					}					
					
					debug_sprintf(szDbgMsg, "%s: Secondary Url received is %s ", __FUNCTION__, pszNodeVal[2]);
					RCHI_LIB_TRACE(szDbgMsg);


					setRCHIHostUrl(SECONDARY_URL, pszNodeVal[2]);

					free(pszNodeVal[2]);
					pszNodeVal[2] = NULL;
				}
			}

		}

		if(pszDWReq != NULL)
		{
			xmlFree(pszDWReq);
			pszDWReq = NULL;
		}

		if(pszDWResp != NULL)
		{
			xmlFree(pszDWResp);
			pszDWResp = NULL;
		}

		/*
		* Step 3: Activate the account if the previous step is a success
		*/
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing the third step in Device Registration Command: Activation");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s: Processing the Third step in Device Registration Command: Activation", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

		rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, NULL, 0, ACTIVATION);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}
		
		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
		}

		/* Updating config variables value in config.usr1 */
		rv = updateConfigParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		rv = postRegReqToDWHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen, REGISTRATION_URL);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Posting request to the DW host failed");
				strcpy(szAppLogDiag, "Connection Error, Please check connection settings");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}
		else
		{

			debug_sprintf(szDbgMsg, "%s: Response received is %s", __FUNCTION__, pszDWResp);
			RCHI_LIB_TRACE(szDbgMsg);
			
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
			}

			rv = getNodeValFromDWRegResp(pszDWResp, NULL, NULL, 0, szStatMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Received Error Response From Host for Activation", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Received Error Response From Host for Activation.");
					strcpy(szAppLogDiag, "Please Contact Admin.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Activation of the account Success", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Successfully Activated the Account");
					addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
		}

		if(pszDWReq != NULL)
		{
			xmlFree(pszDWReq);
			pszDWReq = NULL;
		}

		if(pszDWResp != NULL)
		{
			xmlFree(pszDWResp);
			pszDWResp = NULL;
		}
		break;
	}

	/* Filling the response details for the DID request*/
	/*Filling the  if did exists*/
	getDatawireID(szDID);

	if(rv != SUCCESS && strlen(szDID) > 0)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Device Registration Failed, Sending SSI The Existing DID");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
	}
	if(pstDHIDetails[eRESULT_CODE].elementValue && !strcmp(pstDHIDetails[eRESULT_CODE].elementValue,"31"))
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Device Registration Not Performed due to missing fields");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
	}

	if(strlen(szDID) > 0)
	{
		pstDHIDetails[eDEVICEKEY].elementValue 		= strdup(szDID);
		pstDHIDetails[eCLIENT_ID].elementValue 		= strdup("100010001");
		pstDHIDetails[eCREDIT_PROC_ID].elementValue = strdup("SVDOT");
		pstDHIDetails[eDEBIT_PROC_ID].elementValue 	= strdup("SVDOT");
		pstDHIDetails[eGIFT_PROC_ID].elementValue 	= strdup("SVDOT");
		rv = SUCCESS;
	}

	return rv;
}
/*
 * ============================================================================
 * Function Name: processReportCommand
 *
 * Description	:
 *
 * Input Params	: Structure
 *
 * Output Params:
 * ============================================================================
 */
int	processReportCommand(TRANDTLS_PTYPE pstDHIDetails)
{
	int							rv 										= SUCCESS;
	int							iCmd									= 0;
	int							iLen									= 0;
	int							iDWReqLen								= 0;
	int							iDWRespLen								= 0;
	int							iAppLogEnabled							= 0;
	char						szStatMsg[256]							= "";
	char						szAppLogData[256]						= "";
	char						szAppLogDiag[256]						= "";
	char*						pszMsg	 								= NULL;
	char*						pszDWReq								= NULL;
	char*						pszDWResp								= NULL;
	char*						pszRCHIResp								= NULL;
	char*						pszRequestType							= "AdminRequest";
	char*						pszNodeName								= "Payload";
	RCHI_PASSTHROUGH_PTYPE		pstRCHIPTDtls							= getPassthroughFields();
	RCTRANDTLS_STYPE			pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];
	METADATA_STYPE				stMeta;
	VAL_HOSTTOTALHEADNODE_PTYPE stHostTotalLst							= NULL;

#ifdef DEVDEBUG
	int				iIndex			= 0;
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();


	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));

	while(1)
	{
		iCmd = getTransType(pstDHIDetails[eCOMMAND].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue, "Admin");
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure, cant process the transaction!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found");
				strcpy(szAppLogDiag, "Internal DHI-RCHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = iCmd;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Report %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
		/* Get Total Request Date */
		/* Getting appropriate command */
		rv = getTotalsRequestDate(pstRCDetails[RC_TOT_REQ_DATE].elementValue, pstDHIDetails[eCOMMAND].elementValue);
		switch(rv)
		{
		case RC_CLOSE_BATCH:
		case RC_SITE_CUR_DAY:
			/*fill rc details*/
			rv = fillRCDtls(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to correct RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			break;
		default:
			rv = ERR_UNSUPP_CMD;

			break;
		}
		if(rv != SUCCESS)
		{
			break;
		}

		/* Got the session details. Use this data to create the metadata for
		 * basket building */
		memset(&stMeta, 0x00, METADATA_SIZE);

		rv = getMetaDataForRCReq(&stMeta, pstDHIDetails, pstRCDetails, ADMIN);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
				__FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);


#ifdef DEVDEBUG
		if(strlen(pszMsg) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif

		/*	Free the meta data*/
		freeMetaDataEx(&stMeta);

		rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
		}
		/* Updating config variables value in config.usr1 */
		rv = updateConfigParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}
		rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}
		else
		{

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s", pszDWResp);
			RCHI_LIB_TRACE(szDbgMsg);
#endif
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
			}

			rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
			if(rv != SUCCESS)
			{
				switch(rv)
				{
				case ERR_INV_RESP:
					debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_NOT_AVAILABLE:
					debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_RESP_TO:
					debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_ERROR:
					debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case FAILURE:
				default:
					debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
				break;
			}

			/* Creating a node to store the total number of nodes and the head value of the linked list */
			stHostTotalLst = (VAL_HOSTTOTALHEADNODE_PTYPE)malloc(sizeof(VAL_HOSTTOTALHEADNODE_STYPE));
			if(stHostTotalLst == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
			stHostTotalLst->pstHead = NULL;
			stHostTotalLst->iMaxHostTotalGroup = 0;
			memset(stHostTotalLst->szNetSettlemntAmt, 0x00, sizeof(stHostTotalLst->szNetSettlemntAmt));
			/* Parsing the response */
			parseRCResponseForHostTotals(pszRCHIResp, pstDHIDetails, pstRCDetails,stHostTotalLst);

			if(strcmp(pstRCDetails[RC_TOT_REQ_DATE].elementValue, "CloseBatch") == SUCCESS || strcmp(pstRCDetails[RC_TOT_REQ_DATE].elementValue, "SiteCurDay") == SUCCESS)
			{
				frameSSIRespForCutOverOrSiteTotals(pstDHIDetails, stHostTotalLst);
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pstDHIDetails[eREPORT_RESPONSE].elementValue, NULL);
			}

			if(strcmp(pstRCDetails[RC_RESP_CODE].elementValue, "000") == SUCCESS && strcmp(pstRCDetails[RC_TOT_REQ_DATE].elementValue, "CloseBatch") == SUCCESS)
			{
				resetBatchParams();
			}
		}
		break;
	}


	if(rv != SUCCESS)
	{
		fillErrorDtls(rv, pstDHIDetails, szStatMsg);
	}

#ifdef DEVDEBUG
		for(iIndex = 0; iIndex < eELEMENT_INDEX_MAX; iIndex++)
		{
			if(pstDHIDetails[iIndex].elementValue != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: DHI[%d] Value = [%s]", __FUNCTION__, iIndex, pstDHIDetails[iIndex].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
#endif

	if(pszMsg != NULL)
	{
		free(pszMsg);
		pszMsg = NULL;
	}

	if(pszDWReq != NULL)
	{
		free(pszDWReq);
		pszDWReq = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
* ============================================================================
* Function Name: processGiftTransaction
*
* Description	:
*
* Input Params	: Structure
*
* Output Params:
* ============================================================================
*/
int	processGiftTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int						rv 										= SUCCESS;
	int						iCmd									= 0;
	int						iLen									= 0;
	int						iDWReqLen								= 0;
	int						iWaitTime								= 0;
	int						iDWRespLen								= 0;
	int						iAppLogEnabled							= 0;
	char					szStatMsg[256]							= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszMsg	 								= NULL;
	char*					pszDWReq								= NULL;
	char*					pszDWResp								= NULL;
	char*					pszRCHIResp								= NULL;
	char*					pszRequestType							= "PrepaidRequest";
	char*					pszNodeName								= "Payload";
	time_t					tStartEpochTime  						= 0;
	time_t 					tEndEpochTime  							= 0;
	time_t 					tDiffDeconds       						= 0;
	RCHI_BOOL				bTORRequest								= RCHI_FALSE;
	RCHI_BOOL				bTORFirstTime							= RCHI_TRUE;
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls							= getPassthroughFields();
	RCTRANDTLS_STYPE		pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];
	METADATA_STYPE			stMeta;

#ifdef DEVDEBUG
	int				iIndex			= 0;
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/*
	 * Allocating the memory for filling the RC Details
	 */

	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));

	while(1)
	{
		iCmd = getTransType(pstDHIDetails[eCOMMAND].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue, "Prepaid");
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure, cant process the transaction!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found");
				strcpy(szAppLogDiag, "Internal DHI-RCHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = iCmd;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Prepaid %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case RC_ACTIVATE:
		case RC_BALANCE_LOCK:
		case RC_REDEMPTION:
		case RC_BALANCE:
		case RC_CASH_OUT:
		case RC_RELOAD:
		case RC_TOKEN_QUERY:

			if(iCmd == RC_TOKEN_QUERY)
			{
				pszRequestType = "TransArmorRequest";
			}

			/*fill rc details*/
			rv = fillRCDtls(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			break;

		case RC_REFUND:
			/*fill rc details*/
			if(pstDHIDetails[eCTROUTD].elementValue != NULL)
			{
				rv = ERR_INV_COMBI;
				break;
			}

			rv = fillRCDtls(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			break;

		case RC_VOID:
			pszRequestType = "ReversalRequest";

			/* Copying the Void into the reversal reason code*/
			strcpy(pstRCDetails[RC_REVERSL_RSN_CODE].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue);

			rv = fillRCDtlsforFollowOnTran(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill follow on transaction values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			break;

		case RC_REDEMPTION_UNLOCK:

			rv = fillRCDtlsforFollowOnTran(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill follow on Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
			strcpy(pstRCDetails[RC_TRAN_TYPE].elementValue, "RedemptionUnlock");
			break;


		case RC_SALE:
		case RC_HOST_TOTALS:
		case RC_VOICE_COMPLETION:
		case RC_EMV_DOWNLOAD:
			rv = ERR_INV_COMBI;
			break;

		default:
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		while(1)
		{
			memset(&stMeta, 0x00, METADATA_SIZE);

			rv = getMetaDataForRCReq(&stMeta, pstDHIDetails, pstRCDetails, PREPAID);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}

			rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
						__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]", __FUNCTION__, iLen);
			RCHI_LIB_TRACE(szDbgMsg);

#ifdef DEVDEBUG
			if(strlen(pszMsg) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
				RCHI_LIB_TRACE(szDbgMsg);
			}
#endif

			/*	Free the meta data*/
			freeMetaDataEx(&stMeta);

			rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}
			/* Updating config variables value in config.usr1 */
			rv = updateConfigParams();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
			}

			/* Setting the parameter that RC lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			/* Setting the start time*/
			tStartEpochTime= time(NULL);

			debug_sprintf(szDbgMsg, "%s: Start Epoch time in Seconds is %ld", __FUNCTION__, tStartEpochTime);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
				}

				rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_NOT_AVAILABLE:
						debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_RESP_TO:
						debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_ERROR:
						debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
			}

			/* This is the code for the time out reversal
			 * We need to consider two cases for the timeout reversal
			 * Either the Datawire Server return some error code or
			 * We get response timeout with datawire server itself
			 */
			if(rv != SUCCESS)
			{
				if(rv == ERR_RESP_TO)
				{
					/* Setting the end time*/
					tEndEpochTime = time(NULL);

					debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
					RCHI_LIB_TRACE(szDbgMsg);

					tDiffDeconds  = tEndEpochTime - tStartEpochTime;

					debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
					RCHI_LIB_TRACE(szDbgMsg);

					iWaitTime = 40 - tDiffDeconds;

					debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
					RCHI_LIB_TRACE(szDbgMsg);

					if(bTORFirstTime == RCHI_TRUE)
					{
						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Received Response Time Out, Waiting For %d Seconds To Send Time Out Reversal Request", iWaitTime);
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Sending Time Out Reversal Request For 1st Time");
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						debug_sprintf(szDbgMsg, "%s: Waiting to send the TOR request", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);

						if(iWaitTime > 0)
						{
							svcWait(iWaitTime*1000);
						}

						if(pszMsg != NULL)
						{
							free(pszMsg);
							pszMsg = NULL;
						}

						if(pszDWReq != NULL)
						{
							free(pszDWReq);
							pszDWReq = NULL;
						}

						/* Need to time out reversal now*/
						pszRequestType = "ReversalRequest";

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Framing Reversal(TOR) Request");
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						rv = fillRCDtlsforTOR(pstDHIDetails, pstRCDetails, iCmd);
						if(rv == SUCCESS)
						{
							bTORRequest = RCHI_TRUE;
							bTORFirstTime = RCHI_FALSE;
							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Posting Reversal(TOR) Request for 1 time");
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							continue;
						}
					}
					else
					{
						/* Retrying the timeout reversal for another two times*/
						retryTimeoutReversal(pszDWReq, iWaitTime);

						rv = ERR_HOST_TRAN_TIMEOUT;
						break;
					}
				}	/*KranthiK1: Added this change so that we do not saf the actual transaction when we get conn failure for TOR*/
				else if (bTORRequest && rv == ERR_CONN_FAIL)
				{
					/* Retrying the timeout reversal for another two times*/
					retryTimeoutReversal(pszDWReq, iWaitTime);

					rv = ERR_HOST_TRAN_TIMEOUT;
					break;
				}
			}
			else
			{
				rv = parseRCResponse(pszRCHIResp, pstDHIDetails, pstRCDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}
			break;
		}

		break;
	}

	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
	{
		if(bTORRequest == RCHI_TRUE)
		{
			free(pstDHIDetails[eRESULT_CODE].elementValue);
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("22");

			if(pstDHIDetails[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstDHIDetails[eRESPONSE_TEXT].elementValue);
				pstDHIDetails[eRESPONSE_TEXT].elementValue =  strdup("Transaction Reversed");
			}
		}
		else
		{
			saveTranDetailsForFollowOnTran(pstDHIDetails, pstRCDetails);
		}
	}

	if(rv != SUCCESS)
	{
		fillErrorDtls(rv, pstDHIDetails, szStatMsg);
	}


#ifdef DEVDEBUG
		for(iIndex = 0; iIndex < eELEMENT_INDEX_MAX; iIndex++)
		{
			if(pstDHIDetails[iIndex].elementValue != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: DHI[%d] Value = [%s]", __FUNCTION__, iIndex, pstDHIDetails[iIndex].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
#endif

	if(pszMsg != NULL)
	{
		free(pszMsg);
		pszMsg = NULL;
	}

	if(pszDWReq != NULL)
	{
		free(pszDWReq);
		pszDWReq = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processTokenQueryCommand
 *
 * Description	:
 *
 * Input Params	: Structure
 *
 * Output Params:
 * ============================================================================
 */
int	processTokenQueryCommand(TRANDTLS_PTYPE pstDHIDetails)
{
	int						rv 										= SUCCESS;
	int						iLen									= 0;
	int						iCmd									= 0;
	int						iDWReqLen								= 0;
	int						iDWRespLen								= 0;
	int						iAppLogEnabled							= 0;
	int						iPymtFlag								= 0;
	char					szStatMsg[256]							= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszMsg	 								= NULL;
	char*					pszDWReq								= NULL;
	char*					pszDWResp								= NULL;
	char*					pszRCHIResp								= NULL;
	char*					pszRequestType							= "TransArmorRequest";
	char*					pszNodeName								= "Payload";
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls							= getPassthroughFields();
	RCTRANDTLS_STYPE		pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];
	METADATA_STYPE			stMeta;

#ifdef DEVDEBUG
	int				iIndex			= 0;
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();


	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Token Query Command");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		iCmd = getTransType(pstDHIDetails[eCOMMAND].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue, "Credit");
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure, cant process the transaction!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found");
				strcpy(szAppLogDiag, "Internal DHI-RCHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = iCmd;
			break;
		}

		/*fill rc details*/
		rv = fillRCDtls(pstDHIDetails, pstRCDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill RC Values", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		/* Got the session details. Use this data to create the metadata for
		 * basket building */
		memset(&stMeta, 0x00, METADATA_SIZE);

		if(pstDHIDetails[ePAYMENT_TYPE].elementValue == NULL)
		{
			iPymtFlag = CREDIT; //If payment type is not present then we are defaulting to CREDIT
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "CREDIT") == SUCCESS)
		{
			iPymtFlag = CREDIT;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "ADMIN") == SUCCESS)
		{
			iPymtFlag = CREDIT;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "DEBIT") == SUCCESS)
		{
			iPymtFlag = DEBIT;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "GIFT") == SUCCESS)
		{
			iPymtFlag = PREPAID;
		}

		rv = getMetaDataForRCReq(&stMeta, pstDHIDetails, pstRCDetails, iPymtFlag);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
				__FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);


#ifdef DEVDEBUG
		if(strlen(pszMsg) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif

		/*	Free the meta data*/
		freeMetaDataEx(&stMeta);

		rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
		}
		/* Updating config variables value in config.usr1 */
		rv = updateConfigParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}
		rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}
		else
		{

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s", pszDWResp);
			RCHI_LIB_TRACE(szDbgMsg);
#endif
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
			}

			rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
			if(rv != SUCCESS)
			{
				switch(rv)
				{
				case ERR_INV_RESP:
					debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_NOT_AVAILABLE:
					debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_RESP_TO:
					debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_ERROR:
					debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case FAILURE:
				default:
					debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
				break;
			}

			rv = parseRCResponse(pszRCHIResp, pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}

		}
		break;
	}


	if(rv != SUCCESS)
	{
		fillErrorDtls(rv, pstDHIDetails, szStatMsg);
	}

#ifdef DEVDEBUG
		for(iIndex = 0; iIndex < eELEMENT_INDEX_MAX; iIndex++)
		{
			if(pstDHIDetails[iIndex].elementValue != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: DHI[%d] Value = [%s]", __FUNCTION__, iIndex, pstDHIDetails[iIndex].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
#endif

	if(pszMsg != NULL)
	{
		free(pszMsg);
		pszMsg = NULL;
	}

	if(pszDWReq != NULL)
	{
		free(pszDWReq);
		pszDWReq = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
* ============================================================================
* Function Name: checkHostConnection
*
* Description	: For First Data , this function generates the EchoTest Request.
* If the host is available a 000 Response code is expected. Else Host is considered
* not reachable.
*
* Input Params	: NONE
*
* Output Params:  1/0
* ============================================================================
*/
int checkHostConnection(void *pData) //Actually nothing will be passed to this function
{
	int						rv 								= 0;
	int						iLen							= 0;
	int						iIndex							= 0;
	int						iDWReqLen						= 0;
	int						iDWRespLen						= 0;
	int						iAppLogEnabled					= 0;
	char					szAppLogData[256]				= "";
	char					szStatMsg[256]					= "";
	char					szAppLogDiag[256]				= "";
	char*					pszMsg	 						= NULL;
	char*					pszDWReq						= NULL;
	char*					pszDWResp						= NULL;
	char*					pszRCHIResp						= NULL;
	char*					pszRequestType					= "AdminRequest";
	char*					pszNodeName						= "Payload";
	PASSTHROUGH_PTYPE		pstDHIPTLst 					= getDHIPassThroughDtls();
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls					= getPassthroughFields();
	int						iDHIIndexCnt					= eELEMENT_INDEX_MAX + pstDHIPTLst->iReqXMLTagsCnt + pstDHIPTLst->iRespXMLTagsCnt + 1; // The DHi Enum Index starts from 0, hence we allocate +1
	RCTRANDTLS_STYPE		pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];
	TRANDTLS_STYPE   		ssiReqResFields[iDHIIndexCnt]; 														//Declared because we are using the parseRCResponse function which requires this structure.
	METADATA_STYPE			stMeta;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
	iAppLogEnabled = isAppLogEnabled();

	memset(&ssiReqResFields, 0x00, (sizeof(TRANDTLS_STYPE) *  iDHIIndexCnt ) );

	for(iIndex = 0; iIndex < iDHIIndexCnt; iIndex++)
	{
		ssiReqResFields[iIndex].elementValue = NULL;
	}

	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));
	memset(&stMeta, 0x00, METADATA_SIZE);

	while(1)
	{

		fillRCDtlsforEchoTest(pstRCDetails);

		rv = getMetaDataForRCReq(&stMeta, ssiReqResFields, pstRCDetails, CREDIT);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}

		//Last Parameter indicates that we need to use the latest version
		rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
				__FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);


#ifdef DEVDEBUG
		if(strlen(pszMsg) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif

		/*	Free the meta data*/
		freeMetaDataEx(&stMeta);

		rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
		}

		/* Updating config variables value in config.usr1 */
		rv = updateConfigParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}

		iDWRespLen		= 0;
		pszDWResp		= NULL;

		rv = checkHostConn(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
		}

		if(rv == SUCCESS)
		{

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s", pszDWResp);
			RCHI_LIB_TRACE(szDbgMsg);
#endif

			rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
			if(rv != SUCCESS)
			{
				switch(rv)
				{
				case ERR_INV_RESP:
					debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_NOT_AVAILABLE:
					debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_RESP_TO:
					debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_ERROR:
					debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case FAILURE:
				default:
					debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
				break;
			}


			rv = parseRCResponse(pszRCHIResp, ssiReqResFields, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			debug_sprintf(szDbgMsg, "%s: RC Host Response Code:%s",__FUNCTION__,pstRCDetails[RC_RESP_CODE].elementValue);
			RCHI_LIB_TRACE(szDbgMsg);

			if(strcmp(pstRCDetails[RC_RESP_CODE].elementValue,"000") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: RC Host connection is available",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Rapid Connect Host Connection is Available");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				rv = 1;
			}
			else
			{
				rv = 0;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: RC Host connection is NOT available",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Rapid Connect Host Connection is Not Available");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			rv = 0;
		}

		for(iIndex = 0; iIndex < iDHIIndexCnt; iIndex++)
		{

			if(ssiReqResFields[iIndex].elementValue != NULL)
			{
				free(ssiReqResFields[iIndex].elementValue);
				ssiReqResFields[iIndex].elementValue = NULL;
			}
		}
		break;
	}

	if(pszDWReq != NULL)
	{
		free(pszDWReq);
		pszDWReq = NULL;
	}
	if(pszMsg != NULL)
	{
		free(pszMsg);
		pszMsg = NULL;
	}

	// CLeanup CURL Handle can be handled here , if it is required in all cases.
	if(rv != 1)
	{
		/* Daivik:5/5/16-We need to reestablish the connectivity since the EchoTest Request did not recieve a good response.
		 * This is as per the UMF Doc RVE031.
		 */
		debug_sprintf(szDbgMsg, "%s: Clean Curl Handle since Host is not available.", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		cleanCURLHandle(RCHI_TRUE);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: isHostConnected
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
static RCHI_BOOL isHostConnected()
{
	RCHI_BOOL	bRv				= RCHI_TRUE;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(SUCCESS == checkHostConn())
	{
		bRv = RCHI_TRUE;
	}
	else
	{
		bRv = RCHI_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__,
					(bRv == RCHI_TRUE)? "TRUE" : "FALSE");
	RCHI_LIB_TRACE(szDbgMsg);

	return  bRv;
}
#endif
/*
* ============================================================================
* Function Name: preProcessSAFTransaction
*
* Description	: This function does the Step1 for SAF transaction i.e. Token Query
*                 gets the token query and fill the DHI structure
*
* Input Params	: Structure
*
* Output Params:
* ============================================================================
*/
static int preProcessSAFTransaction(TRANDTLS_PTYPE pstDHIDetails, RCTRANDTLS_PTYPE pstRCDetails)
{
	int					rv 									= SUCCESS;
	int					iSAFProcessStep						= 0;
	int					iAppLogEnabled						= 0;
	int					iLen								= 0;
	int					iDWReqLen							= 0;
	int					iDWRespLen							= 0;
	int					iIndex								= 0;
	int					iWaitTime							= 0;
	char				szAppLogData[256]					= "";
	char				szAppLogDiag[256]					= "";
	char				szStatMsg[256]						= "";
	char*				pszNodeName							= "Payload";
	char*				pszMsg	 							= NULL;
	char*				pszDWReq							= NULL;
	char*				pszDWResp							= NULL;
	char*				pszRCHIResp							= NULL;
	char*				pszRequestType						= NULL;
	char*				pszOrigCommand						= NULL;
	RCHI_BOOL			bTORRequest							= RCHI_FALSE;
	RCHI_BOOL			bTORFirstTime						= RCHI_TRUE;
	time_t				tStartEpochTime  					= 0;
	time_t 				tEndEpochTime  						= 0;
	time_t 				tDiffDeconds       					= 0;
	//TRANDTLS_PTYPE 		pstLocalDHIDetails				= NULL;
	PASSTHROUGH_PTYPE		pstDHIPTLst 					= getDHIPassThroughDtls();
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls					= getPassthroughFields();
	int						iRCHIIndexCnt					= RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1;
	int						iDHIIndexCnt					= eELEMENT_INDEX_MAX + pstDHIPTLst->iReqXMLTagsCnt + pstDHIPTLst->iRespXMLTagsCnt + 1; // The DHi Enum Index starts from 0, hence we allocate +1
	TRANDTLS_STYPE   		stLocalDHIDetails[iDHIIndexCnt];
//	RCTRANDTLS_STYPE	pstRCDetails[RC_ELEMENTS_INDEX_MAX];
	METADATA_STYPE		stMeta;

#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	memset(&stLocalDHIDetails, 0x00, (sizeof(TRANDTLS_STYPE) *  iDHIIndexCnt ) );

	for(iIndex = 0; iIndex < iDHIIndexCnt; iIndex++)
	{
		stLocalDHIDetails[iIndex].elementValue = NULL;
	}

	iAppLogEnabled = isAppLogEnabled();

	/* KranthiK1:
	 * Making these changes based on the changes made on host side where SAF sale needs to be
	 * sent as completion with track data
	 * Post auth will continue to go as two step process.
	 * Transactions with card token will directly hit step 2.
	 */

	if(pstDHIDetails[eCOMMAND].elementValue != NULL)
	{
		if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "POST_AUTH") == SUCCESS &&
				pstDHIDetails[eCARD_TOKEN].elementValue == NULL)
		{
			iSAFProcessStep = 1;
		}
		else
		{
			iSAFProcessStep = 2;
		}

		pszOrigCommand = (char *)malloc(strlen((pstDHIDetails[eCOMMAND].elementValue) + 1)  * sizeof(char));
		if(pszOrigCommand == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocating memory to store the command name", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			return FAILURE;
		}

		memset(pszOrigCommand, 0x00, strlen(pstDHIDetails[eCOMMAND].elementValue) + 1);

		strncpy(pszOrigCommand, pstDHIDetails[eCOMMAND].elementValue, strlen(pstDHIDetails[eCOMMAND].elementValue));
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Command is not present!!!", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Checking for card token. If present then directly performing Step-2 */
#if 0
	if(pstDHIDetails[eCARD_TOKEN].elementValue != NULL)
	{
		iSAFProcessStep = 2;
	}

	if(pstDHIDetails[eCOMMAND].elementValue != NULL)
	{
		pszOrigCommand = (char *)malloc(strlen((pstDHIDetails[eCOMMAND].elementValue) + 1)  * sizeof(char));
		if(pszOrigCommand == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocating memory to store the command name", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			return FAILURE;
		}

		memset(pszOrigCommand, 0x00, strlen(pstDHIDetails[eCOMMAND].elementValue) + 1);

		strncpy(pszOrigCommand, pstDHIDetails[eCOMMAND].elementValue, strlen(pstDHIDetails[eCOMMAND].elementValue));
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Command is not present!!!", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}
#endif
	while(1)
	{
		/*
		 * SAF Transaction to RC host is two step process
		 * Step1: Do the Token Query
		 * Stpe2: Completion
		 */

		/* Set the Transaction type as TAToken Request */

		if(iSAFProcessStep == 1) //STEP-1 for Token Query
		{
			/* Resetting all the fields in the structure for the new transaction*/
			memset(pstRCDetails, 0x00, sizeof(RCTRANDTLS_STYPE) * iRCHIIndexCnt);


			debug_sprintf(szDbgMsg, "%s: 201 value:[%s]", __FUNCTION__,pstRCDetails[201].elementValue);
			RCHI_LIB_TRACE(szDbgMsg);

			strcpy(pstRCDetails[RC_TRAN_TYPE].elementValue, "TATokenRequest");

			pszRequestType = "TransArmorRequest";

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Processing Token query");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			if(pstDHIDetails[eCOMMAND].elementValue != NULL)
			{
				free(pstDHIDetails[eCOMMAND].elementValue); //Freeing the command to copy new value
			}

			pstDHIDetails[eCOMMAND].elementValue = (char *)malloc((strlen("TOKEN_QUERY") + 1) * sizeof(char));
			if(pstDHIDetails[eCOMMAND].elementValue == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while allocation memory", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			memset(pstDHIDetails[eCOMMAND].elementValue, 0x00, strlen("TOKEN_QUERY") + 1);
			strncpy(pstDHIDetails[eCOMMAND].elementValue, "TOKEN_QUERY", strlen("TOKEN_QUERY"));

			debug_sprintf(szDbgMsg, "%s: Command for Step1 for SAf processing is %s", __FUNCTION__, pstDHIDetails[eCOMMAND].elementValue);
			RCHI_LIB_TRACE(szDbgMsg);

			if(pstDHIDetails[ePARTIAL_AUTH].elementValue != NULL) //for Token Query step this is not required
			{
				free(pstDHIDetails[ePARTIAL_AUTH].elementValue);
				pstDHIDetails[ePARTIAL_AUTH].elementValue = NULL;
			}

			if(pstDHIDetails[ePINLESSDEBIT].elementValue != NULL) //for Token Query step this is not required
			{
				free(pstDHIDetails[ePINLESSDEBIT].elementValue);
				pstDHIDetails[ePINLESSDEBIT].elementValue = NULL;
			}

			/*fill rc details*/
			rv = fillRCDtls(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to correct RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}
		else if(bTORRequest == RCHI_TRUE && iSAFProcessStep == 2)
		{
			debug_sprintf(szDbgMsg, "%s: Sending TOR request for Completion", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else //STEP-2 for Completion
		{
			/* Resetting all the fields in the structure for the new transaction*/
			memset(pstRCDetails, 0x00, sizeof(RCTRANDTLS_STYPE) * iRCHIIndexCnt);

			if(pstDHIDetails[eCOMMAND].elementValue != NULL)
			{
				free(pstDHIDetails[eCOMMAND].elementValue); //Freeing the command to copy new value
			}
			if(!strcmp(pszOrigCommand, "SALE"))
			{
				pstDHIDetails[eCOMMAND].elementValue = (char *)malloc((strlen("SALE") + 1) * sizeof(char));

				if(pstDHIDetails[eCOMMAND].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while allocation memory", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}

				memset(pstDHIDetails[eCOMMAND].elementValue, 0x00, strlen("SALE") + 1);
				strncpy(pstDHIDetails[eCOMMAND].elementValue, "SALE", strlen("SALE"));
			}
			else if(!strcmp(pszOrigCommand, "POST_AUTH"))
			{
				pstDHIDetails[eCOMMAND].elementValue = (char *)malloc((strlen("POST_AUTH") + 1) * sizeof(char));

				if(pstDHIDetails[eCOMMAND].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while allocation memory", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}

				memset(pstDHIDetails[eCOMMAND].elementValue, 0x00, strlen("POST_AUTH") + 1);
				strncpy(pstDHIDetails[eCOMMAND].elementValue, "POST_AUTH", strlen("POST_AUTH"));
			}

			debug_sprintf(szDbgMsg, "%s: Command for Step2 for SAf processing is %s", __FUNCTION__, pstDHIDetails[eCOMMAND].elementValue);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Processing Completion");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			strcpy(pstRCDetails[RC_TRAN_TYPE].elementValue, "Completion");

			if(pstDHIDetails[ePAYMENT_TYPE].elementValue != NULL) //Need to set the Request type
			{
				if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "CREDIT") == SUCCESS)
				{
					pszRequestType = "CreditRequest";
				}
				else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "DEBIT") == SUCCESS)
				{
					pszRequestType = "DebitRequest";
				}
				else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "GIFT") == SUCCESS)
				{
					pszRequestType = "PrepaidRequest";
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Invalid Payment Type", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					rv = ERR_HOST_NOT_AVAILABLE;
					break;

				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Payment not found", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_HOST_NOT_AVAILABLE;
				break;
			}

			rv = fillRCDtlsForSAFCompletionTran(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to correct RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}

		memset(&stMeta, 0x00, METADATA_SIZE);

		rv = getMetaDataForRCReq(&stMeta, pstDHIDetails, pstRCDetails, CREDIT); //TODO currently sending as CREDIT may have to change this
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}


		rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
				__FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		if(strlen(pszMsg) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif

		/*	Free the meta data*/
		freeMetaDataEx(&stMeta);

		rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
		}
		/* Updating config variables value in config.usr1 */
		rv = updateConfigParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		/* Setting the parameter that RC lib hit the host*/
		pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

		if(iSAFProcessStep == 2)
		{
			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			debug_sprintf(szDbgMsg, "%s: Start Epoch time in Seconds is %ld", __FUNCTION__, tStartEpochTime);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
			}

			rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
			if(rv != SUCCESS)
			{
				switch(rv)
				{
				case ERR_INV_RESP:
					debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_NOT_AVAILABLE:
					debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_RESP_TO:
					debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_ERROR:
					debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case FAILURE:
				default:
					debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}
		}

		/* This is the code for the time out reversal
		 * We need to consider two cases for the timeout reversal
		 * Either the Datawire Server return some error code or
		 * We get response timeout with datawire server itself
		 */
		if(rv != SUCCESS)
		{
			if(rv == ERR_RESP_TO && iSAFProcessStep == 2)
			{
				/* Setting the end time*/
				tEndEpochTime = time(NULL);

				debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
				RCHI_LIB_TRACE(szDbgMsg);

				tDiffDeconds  = tEndEpochTime - tStartEpochTime;

				debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
				RCHI_LIB_TRACE(szDbgMsg);

				iWaitTime = 40 - tDiffDeconds;

				debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
				RCHI_LIB_TRACE(szDbgMsg);

				if(bTORFirstTime == RCHI_TRUE)
				{
					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Received Response Time Out, Waiting For %d Seconds To Send Time Out Reversal Request", iWaitTime);
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
					}

					if(iWaitTime > 0)
					{
						svcWait(iWaitTime*1000);
					}

					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Sending Time Out Reversal Request For 1st time");
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
					}

					if(pszMsg != NULL)
					{
						free(pszMsg);
						pszMsg = NULL;
					}

					if(pszDWReq != NULL)
					{
						free(pszDWReq);
						pszDWReq = NULL;
					}

					/* Need to time out reversal now*/
					pszRequestType = "ReversalRequest";

					rv = fillRCDtlsforTOR(pstDHIDetails, pstRCDetails, RC_COMPLETION);
					if(rv == SUCCESS)
					{
						bTORRequest = RCHI_TRUE;
						bTORFirstTime = RCHI_FALSE;
						continue;
					}
				}
				else
				{
					/* Retrying the timeout reversal for another two times*/
					retryTimeoutReversal(pszDWReq, iWaitTime);

					rv = ERR_HOST_TRAN_TIMEOUT;
					break;
				}

			}
			else
			{
				/* Retrying the timeout reversal for another two times*/
				retryTimeoutReversal(pszDWReq, iWaitTime);

				rv = ERR_HOST_NOT_AVAILABLE;
				break;
			}
		}
		else
		{
			if(iSAFProcessStep == 1)
			{
				rv = parseRCResponse(pszRCHIResp, stLocalDHIDetails, pstRCDetails);
			}
			else
			{
				rv = parseRCResponse(pszRCHIResp, pstDHIDetails, pstRCDetails);
			}
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				rv = ERR_HOST_NOT_AVAILABLE; //TODO Is it ok to break from here??
				break;
			}
		}

		if(iSAFProcessStep == 1) //updating the card token value in the dhi structure
		{
			if((stLocalDHIDetails[eCARD_TOKEN].elementValue != NULL) && (strlen(stLocalDHIDetails[eCARD_TOKEN].elementValue) > 0))
			{
	#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s: Card Token [%s]", __FUNCTION__, stLocalDHIDetails[eCARD_TOKEN].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
	#endif
				pstDHIDetails[eCARD_TOKEN].elementValue = strdup(stLocalDHIDetails[eCARD_TOKEN].elementValue); //updating DHI structure

				if(stLocalDHIDetails[eEXP_MONTH].elementValue && stLocalDHIDetails[eEXP_YEAR].elementValue)
				{
					if(pstDHIDetails[eEXP_MONTH].elementValue != NULL)
					{
						free(pstDHIDetails[eEXP_MONTH].elementValue);
						pstDHIDetails[eEXP_MONTH].elementValue = NULL;
					}

					if(pstDHIDetails[eEXP_YEAR].elementValue != NULL)
					{
						free(pstDHIDetails[eEXP_YEAR].elementValue);
						pstDHIDetails[eEXP_YEAR].elementValue = NULL;
					}

					pstDHIDetails[eEXP_MONTH].elementValue = strdup(stLocalDHIDetails[eEXP_MONTH].elementValue);
					pstDHIDetails[eEXP_YEAR].elementValue  = strdup(stLocalDHIDetails[eEXP_YEAR].elementValue);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Did not get the Token, so cant perform Completion!!!", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed to Get Token For This Card. ");
					strcpy(szAppLogDiag, "Please Contact Admin");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				// MukeshS3: 21-June-16: We will consider this as transaction declined
				//rv = ERR_HOST_NOT_AVAILABLE;
				rv = ERR_TOKEN_NOT_FOUND;
				break;
			}
		}

		if(iSAFProcessStep == 2)
		{
			break;
		}
		iSAFProcessStep++; //Going to next step

		debug_sprintf(szDbgMsg, "%s: Card token received, going to perform next step i.e. Completion", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);

	}

	if(pstDHIDetails[eCOMMAND].elementValue != NULL)
	{
		free(pstDHIDetails[eCOMMAND].elementValue); //Freeing the command to copy original value
		pstDHIDetails[eCOMMAND].elementValue = NULL;
	}

	pstDHIDetails[eCOMMAND].elementValue = pszOrigCommand; //This will be freed by DHI

	for(iIndex = 0; iIndex < iDHIIndexCnt; iIndex++)
	{
		if(stLocalDHIDetails[iIndex].elementValue)
		{
			free(stLocalDHIDetails[iIndex].elementValue);
			stLocalDHIDetails[iIndex].elementValue = NULL;
		}
	}

	/* KranthiK1: 04-Oct-2016
	 * Making this change so that SCA will repost the transaction as it is a SAFFED transaction
	 * If the transaction is actually posted to host then timeout reversal should have reversed the transaction
	 * If the transaction is not posted then TOR will get rejected
	 * Either way we need to repost the transaction. So giving connection timeout to SCA so that it will repost
	 * the transaction
	 */

	if(bTORRequest)
	{
		rv = ERR_HOST_NOT_AVAILABLE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
* ============================================================================
* Function Name: isSAFTranProcessRegd
*
* Description	:
*
* Input Params	: Structure
*
* Output Params:
* ============================================================================
*/
static RCHI_BOOL isSAFTranProcessRegd(TRANDTLS_PTYPE pstDHIDetails)
{
	RCHI_BOOL rv = RCHI_FALSE;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDetails[eSAF_TRAN].elementValue != NULL)//contains SAF_TRAN field in the request
	{
		if(strcmp(pstDHIDetails[eSAF_TRAN].elementValue, "1") == 0) //SAF_TRAN set to 1 so it is a SAF transaction
		{
			rv = RCHI_TRUE;
		}
	}
	/* Support is added to ensure Void Transactions get SAF'd . But we should be treating SAF'd Void Transaction as normal Void and send the Reversal
	 * Request.
	 * For commands other than Void/Refund , we send SAF'd transactions as a completion request after a token query.
	 */
	if(pstDHIDetails[eCOMMAND].elementValue != NULL && (!strcmp(pstDHIDetails[eCOMMAND].elementValue, "CREDIT") || !strcmp(pstDHIDetails[eCOMMAND].elementValue, "VOID")) )
	{
		rv = RCHI_FALSE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning %s", __FUNCTION__, ( rv == RCHI_TRUE)?"TRUE":"FALSE");
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
* ============================================================================
* Function Name: processCheckTransaction
*
* Description	:
*
* Input Params	: Structure
*
* Output Params:
* ============================================================================
*/

int	processCheckTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int						rv 										= SUCCESS;
	int						iCmd									= -1;
	int						iLen									= 0;
	int						iDWReqLen								= 0;
	int						iDWRespLen								= 0;
	int						iAppLogEnabled							= 0;
	char					szStatMsg[256]							= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszMsg	 								= NULL;
	char*					pszDWReq								= NULL;
	char*					pszDWResp								= NULL;
	char*					pszRCHIResp								= NULL;
	char*					pszRequestType							= "CheckRequest";
	char*					pszNodeName								= "Payload";
	RCHI_BOOL				bTORRequest								= RCHI_FALSE;
	time_t					tStartEpochTime  						= 0;
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls							= getPassthroughFields();
	RCTRANDTLS_STYPE		pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];
	METADATA_STYPE			stMeta;

#ifdef DEVDEBUG
	int				iIndex			= 0;
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Resetting all the fields in the structure for the new transaction*/
	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));
	while(1)
	{
		iCmd = getTransType(pstDHIDetails[eCOMMAND].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue, "Check");
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure, cant process the transaction!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found");
				strcpy(szAppLogDiag, "Internal DHI-RCHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = iCmd;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Credit %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case RC_AUTH:
			/*fill rc details*/
			rv = fillRCDtlsforCheckTransaction(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to correct RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			break;

		case RC_VOID:
		case RC_VOICE_COMPLETION:
		case RC_COMPLETION:
		case RC_SALE:
		case RC_REFUND:
		case RC_TOKEN_QUERY:
		case RC_ACTIVATE:
		case RC_REDEMPTION:
		case RC_BALANCE:
		case RC_CASH_OUT:
		case RC_HOST_TOTALS:
		case RC_EMV_DOWNLOAD:
			rv = ERR_INV_COMBI;
			break;

		default:
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		while(1)
		{

			memset(&stMeta, 0x00, METADATA_SIZE);

			rv = getMetaDataForRCReq(&stMeta, pstDHIDetails, pstRCDetails, CHECK);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}


			rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
						__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
					__FUNCTION__, iLen);
			RCHI_LIB_TRACE(szDbgMsg);

#ifdef DEVDEBUG
			if(strlen(pszMsg) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
				RCHI_LIB_TRACE(szDbgMsg);
			}
#endif

			/*	Free the meta data*/
			freeMetaDataEx(&stMeta);

			rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
			}

			/* Setting the parameter that RC lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			debug_sprintf(szDbgMsg, "%s: Start Epoch time in Seconds is %ld", __FUNCTION__, tStartEpochTime);
			RCHI_LIB_TRACE(szDbgMsg);
			rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
				}

				rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_NOT_AVAILABLE:
						debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_RESP_TO:
						debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_ERROR:
						debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
			}

			/* This is the code for the time out reversal
			 * We need to consider two cases for the timeout reversal
			 * Either the Datawire Server return some error code or
			 * We get response timeout with datawire server itself
			 */
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get response from the host.. TOR not supported for Check Authorization", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;

			}
			else
			{
				rv = parseRCResponse(pszRCHIResp, pstDHIDetails, pstRCDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}

			}
			break;
		}

		break;
	}

	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
	{
		if(bTORRequest == RCHI_TRUE)
		{
			free(pstDHIDetails[eRESULT_CODE].elementValue);
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("22");

			if(pstDHIDetails[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstDHIDetails[eRESPONSE_TEXT].elementValue);
				pstDHIDetails[eRESPONSE_TEXT].elementValue =  strdup("Transaction Reversed");
			}
		}
		else
		{
			saveTranDetailsForFollowOnTran(pstDHIDetails, pstRCDetails);
		}
	}

	if(rv != SUCCESS)
	{
		fillErrorDtls(rv, pstDHIDetails, szStatMsg);
	}


#ifdef DEVDEBUG
		for(iIndex = 0; iIndex < eELEMENT_INDEX_MAX; iIndex++)
		{
			if(pstDHIDetails[iIndex].elementValue != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: DHI[%d] Value = [%s]", __FUNCTION__, iIndex, pstDHIDetails[iIndex].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
#endif

	if(pszMsg != NULL)
	{
		free(pszMsg);
		pszMsg = NULL;
	}

	if(pszDWReq != NULL)
	{
		free(pszDWReq);
		pszDWReq = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
* ============================================================================
* Function Name: updateConfigParams
*
* Description	: Updating RC Config Parameters in config.usr1
*
* Input Params	: Null
*
* Output Params: SUCCESS/FAILURE
* ============================================================================
*/

int updateConfigParams()
{
	extern	int		errno;
	int				rv							= SUCCESS;
	int				iCount						= 0;
	char			szStnRefNumClntRefNum[256]	= "";
	char 			*szValue[MAX_CONFIG_PARAMS]= { NULL };
	//dictionary 		*pDictObj 					= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	acquireRCMutexLock(&gpConfigParamsAcessMutex, "Updating config params");

	for(iCount = 0; iCount < MAX_CONFIG_PARAMS; iCount++)
	{
		szValue[iCount] = (char *)malloc(MAX_SIZE);
		if(szValue[iCount] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc failed!!!",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			releaseRCMutexLock(&gpConfigParamsAcessMutex, "Updating Config params");
			return rv;
		}
		switch(iCount)
		{
			case 0: getCurrentSTAN(szValue[iCount]);
					strcpy(szStnRefNumClntRefNum,szValue[iCount]);
					break;
			case 1: getCurrentRefNum(szValue[iCount]);
					strcat(szStnRefNumClntRefNum, "|");
					strcat(szStnRefNumClntRefNum,szValue[iCount]);
					break;
			case 2:	getCurrentClientRefNum(szValue[iCount]);
					strcat(szStnRefNumClntRefNum, "|");
					strcat(szStnRefNumClntRefNum,szValue[iCount]);
					break;
		}

		debug_sprintf(szDbgMsg, "%s - writing key=[%s]", __FUNCTION__, szStnRefNumClntRefNum);
		RCHI_LIB_TRACE(szDbgMsg);

		/* Freeing the pointer */
		free(szValue[iCount]);
	}

	/* Writing into the file */
	rv = writeRCParamsToFile(szStnRefNumClntRefNum);
	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully written",__FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - Write unsuccessful in file [%s]!",
				__FUNCTION__, RC_PARAMS_FILE);
		RCHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	releaseRCMutexLock(&gpConfigParamsAcessMutex, "Updating Config params");

	debug_sprintf(szDbgMsg, "%s Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
* ============================================================================
* Function Name: initiateEMVDownload
*
* Description	: Creates and Posts the EMV Keys Download Request
*
* Input Params	: RC and DHI Fields, which are formed from the SSI request
*
* Output Params: SUCCESS/FAILURE
* ============================================================================
*/

static int initiateEMVDownload(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int						rv										= SUCCESS;
	METADATA_STYPE			stMeta;
	int						iAppLogEnabled							= 0;
	char					szStatMsg[256]							= "";
	char*					pszRequestType							= "AdminRequest";
	char*					pszDWReq								= NULL;
	char*					pszDWResp								= NULL;
	char*					pszNodeName								= "Payload";
	int						iLen									= 0;
	char*					pszMsg	 								= NULL;
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	int						iDWReqLen								= 0;
	int						iDWRespLen								= 0;
	time_t					tStartEpochTime  						= 0;
	int 					iBlkSeqVal								= 1;
	char*					pszRCHIResp								= NULL;
	FILE*					fptr;
#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	memset(&stMeta, 0x00, METADATA_SIZE);
	debug_sprintf(szDbgMsg, "%s:----enter----", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		/* Step 1: Prepare EMV Key Download Request  */
		rv = getMetaDataForRCReq(&stMeta, pstDHIDtls, pstRCHIDetails, CREDIT);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

		}
		debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
				__FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);


#ifdef DEVDEBUG
		if(strlen(pszMsg) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif

		/*	Free the meta data*/
		freeMetaDataEx(&stMeta);

		rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}
		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
		}

		/* Updating config variables value in config.usr1 */
		rv = updateConfigParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		/* Setting the parameter that RC lib hit the host*/
		pstDHIDtls[eHIT_HOST].elementValue = strdup("1");

		/* Setting the start time*/
		tStartEpochTime = time(NULL);

		debug_sprintf(szDbgMsg, "%s: Start Epoch time in Seconds is %ld", __FUNCTION__, tStartEpochTime);
		RCHI_LIB_TRACE(szDbgMsg);

		iDWRespLen								= 0;
		pszDWResp								= NULL;
		/* Step 2: Post the EMV Key Download Request to the Host. This Request is an Admin Request which contains
		 * the File Dowload Group
		 */
		rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}
		else
		{
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
			}

			rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
			if(rv != SUCCESS)
			{
				switch(rv)
				{
				case ERR_INV_RESP:
					debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_NOT_AVAILABLE:
					debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_RESP_TO:
					debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_ERROR:
					debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case FAILURE:
				default:
					debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}

			/* Step 3: Parse the Response for the EMV key Download Request */
			rv = parseRCResponse(pszRCHIResp, pstDHIDtls, pstRCHIDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}

			/* Step 4: Checking if we have received the last packet and resetting global emvAdmin values */
			if(!strcmp(pstRCHIDetails[RC_FUNC_CODE].elementValue,"C"))
			{
				iBlkSeqVal = atoi(pstRCHIDetails[RC_FL_BLK_SEQ].elementValue);
				iBlkSeqVal++;
				if(iBlkSeqVal < 10)
				{
					sprintf(gstEMVAdmin.szFileBlk,"000%d",iBlkSeqVal);
				}
				else
				{
					sprintf(gstEMVAdmin.szFileBlk,"00%d",iBlkSeqVal);
				}
				debug_sprintf(szDbgMsg, "%s:%d file off:%s seq_nu:%s", __FUNCTION__, __LINE__,pstRCHIDetails[RC_REQ_FL_OFF].elementValue,pstRCHIDetails[RC_FL_BLK_SEQ].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
				strcpy(gstEMVAdmin.szDwnldStat,"M");
				strcpy(gstEMVAdmin.szDownOffset,pstRCHIDetails[RC_REQ_FL_OFF].elementValue);

			}
			else if(!strcmp(pstRCHIDetails[RC_FUNC_CODE].elementValue,"L"))
			{
				strcpy(gstEMVAdmin.szFileBlk,"0001");
				strcpy(gstEMVAdmin.szDownOffset,"0");
				strcpy(gstEMVAdmin.szDwnldStat,"F");
				//Save all the File Properties - File Creation Date, File CRC , File Size
				fptr = fopen(EMV_KEY_FL_DTLS, "w");
				if(fptr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Could not open the file could not save the record", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					return rv;
				}
				fprintf(fptr, "%s|%s|%s", (char*)pstRCHIDetails[RC_FL_SZ].elementValue,(char*)pstRCHIDetails[RC_FL_CREAT_DATE].elementValue,(char*)pstRCHIDetails[RC_FL_CRC].elementValue);
				fclose(fptr);

				acquireRCMutexLock(&gpEMVAdminStructAcessMutex, "Updating Key Update Available Flag");
				gstEMVAdmin.iKeyUpdAvl = 0;
				releaseRCMutexLock(&gpEMVAdminStructAcessMutex, "Updating Key Update Available Flag");
			}
			break;

		}
	}
	debug_sprintf(szDbgMsg, "%s:Returning", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}
/*
* ============================================================================
* Function Name: initiateEMVStatusReq
*
* Description	: Function for preparing and posting the EMV Key Status Req
* 				  Depending on the response from Host, the global variable
* 				  is being set. And based on this variable we will send the
* 				  EMV_UPDATE field in SSI response.
*
* Input Params	:
*
* Output Params: SUCCESS/FAILURE
* ============================================================================
*/
static void initiateEMVStatusReq(TRANDTLS_PTYPE pstDHIDtls,RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int						rv					= SUCCESS;
	int						iLen				= 0;
	int						iDWReqLen			= 0;
	int						iDWRespLen			= 0;
	int						iAppLogEnabled		= 0;
	char					szStatMsg[256]		= "";
	char					szAppLogData[256]	= "";
	char					szAppLogDiag[256]	= "";
	char*					pszRequestType		= "AdminRequest";
	char*					pszDWReq			= NULL;
	char*					pszDWResp			= NULL;
	char*					pszNodeName			= "Payload";
	char*					pszMsg	 			= NULL;
	char*					pszRCHIResp			= NULL;
	METADATA_STYPE			stMeta;


#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	memset(&stMeta, 0x00, METADATA_SIZE);
	iAppLogEnabled = isAppLogEnabled();
	rv = fillRCDtlsforEMVAdmin(pstRCHIDetails);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to fill RC Details for EMV admin", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
		return;
	}

	while(1)
	{
		rv = getMetaDataForRCReq(&stMeta, pstDHIDtls, pstRCHIDetails, CREDIT);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
		}

		rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
					__FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

		}

		debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
				__FUNCTION__, iLen);
		RCHI_LIB_TRACE(szDbgMsg);


#ifdef DEVDEBUG
		if(strlen(pszMsg) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
			RCHI_LIB_TRACE(szDbgMsg);
		}
#endif

		/*	Free the meta data*/
		freeMetaDataEx(&stMeta);

		rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
		}

		/* Updating config variables value in config.usr1 */
		rv = updateConfigParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			break;
		}

		iDWRespLen								= 0;
		pszDWResp								= NULL;

		rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
			}

			rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
			if(rv != SUCCESS)
			{
				switch(rv)
				{
				case ERR_INV_RESP:
					debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_NOT_AVAILABLE:
					debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_RESP_TO:
					debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case ERR_HOST_ERROR:
					debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				case FAILURE:
				default:
					debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}

			rv = parseRCResponse(pszRCHIResp, pstDHIDtls, pstRCHIDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}

			debug_sprintf(szDbgMsg, "%s:%d File Details : updateflag:%s", __FUNCTION__, __LINE__,pstRCHIDetails[RC_FUNC_CODE].elementValue);
			RCHI_LIB_TRACE(szDbgMsg);
			if(!strcmp("U",pstRCHIDetails[RC_FUNC_CODE].elementValue))
			{
				gstEMVAdmin.iKeyUpdAvl = 1;
				debug_sprintf(szDbgMsg, "%s: Must redo the EMV Initialization since update is availbale", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				putEnvFile(SECTION_DCT, EMV_SETUP_REQ,  "2");
			}
		}
		break;
	}
	return ;
}
/*
* ============================================================================
* Function Name: checkEMVKeysStat
*
* Description	: Main function for handling status Check of EMV Keys
* 				  This function is called from a thread, to periodically
* 				  check whether the keys we currently have are the updated
* 				  keys. If update is available , then we set a global
* 				  variable that sets EMV_UPDATE in SALE  and REFUND
* 				  Responses.
*
* Input Params	:
*
* Output Params: SUCCESS/FAILURE
* ============================================================================
*/
int checkEMVKeysStat()
{
	int						rv							= SUCCESS;
	int 					iCnt						= 0;
	PASSTHROUGH_PTYPE		pstDHIPTLst 				= getDHIPassThroughDtls();
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls				= getPassthroughFields();
	int						iDHIIndexCnt				= eELEMENT_INDEX_MAX + pstDHIPTLst->iReqXMLTagsCnt + pstDHIPTLst->iRespXMLTagsCnt + 1; // The DHi Enum Index starts from 0, hence we allocate +1
	RCTRANDTLS_STYPE		pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];
	TRANDTLS_STYPE   		pstDHIDtls[iDHIIndexCnt]; 														//Declared because we are using the parseRCResponse function which requires this structure.

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));

	debug_sprintf(szDbgMsg, "%s: -----enter----- ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);


	for(iCnt = 0; iCnt < iDHIIndexCnt; iCnt++)
	{
		pstDHIDtls[iCnt].elementValue = NULL;
	}

	acquireRCMutexLock(&gpEMVAdminStructAcessMutex, "Reading Key Update Available Flag");
	if((SUCCESS != doesFileExist(EMV_KEY_FL_DTLS)) || gstEMVAdmin.iKeyUpdAvl )
	{
		releaseRCMutexLock(&gpEMVAdminStructAcessMutex, "Updating Key Update Available Flag");
		debug_sprintf(szDbgMsg, "%s: EMV Keys Status File Does not exist , KeyUpd:%d ", __FUNCTION__,gstEMVAdmin.iKeyUpdAvl);
		RCHI_LIB_TRACE(szDbgMsg);
		return rv;
	}
	else
	{	releaseRCMutexLock(&gpEMVAdminStructAcessMutex, "Reading Key Update Available Flag");
		initiateEMVStatusReq(pstDHIDtls,pstRCDetails);
	}

	for(iCnt = 0; iCnt < iDHIIndexCnt; iCnt++)
	{
		if(pstDHIDtls[iCnt].elementValue)
		{
			free(pstDHIDtls[iCnt].elementValue);
			pstDHIDtls[iCnt].elementValue = NULL;
		}

	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}
/*
* ============================================================================
* Function Name: handleEMVDownload
*
* Description	: Main function for handling the EMV Public Keys Download
* 				  Calls other function for Posting the EMV Keys Download Request,
* 				  Parsing the Keys and framing  the SSI response.
*
* Input Params	: RC and DHI Fields, which are formed from the SSI request
*
* Output Params: SUCCESS/FAILURE
* ============================================================================
*/
static int handleEMVDownload(TRANDTLS_PTYPE pstDHIDtls, RCTRANDTLS_PTYPE pstRCHIDetails)
{
	int		rv			= SUCCESS;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: -----enter----- ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	if(!strcmp(pstDHIDtls[eEMV_DOWNLOAD_STAT].elementValue,"I") || !strcmp(pstDHIDtls[eEMV_DOWNLOAD_STAT].elementValue,"S"))
	{
		debug_sprintf(szDbgMsg, "%s: Must initiate new Download Request as Update Available :%d ", __FUNCTION__,__LINE__);
		RCHI_LIB_TRACE(szDbgMsg);

		/* Step 1: Getting details of EMVDowloadGrp */
		getEMVFileDownloadGrp(pstRCHIDetails);
		/* Step 2: Initiating a EMVFileDownload request and obtaining parsing the response from RC Host */
		rv = initiateEMVDownload(pstDHIDtls,pstRCHIDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to initiate an EMVFileDownload Request/Response", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			return rv;
		}

		if(strcmp(pstDHIDtls[eRESULT_CODE].elementValue,"5"))
		{
			return rv;
		}

		/* Step 3: Creation of linked list for the obtained Public Keys */
		rv = parseAndCreateLinkedList();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to create linked list", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			return rv;
		}
		if(!strcmp(gstEMVAdmin.szDwnldStat,"F"))
		{
			strcpy(gstEMVAdmin.szDownOffset,"0");
		}
		/* Step 4: Framing SSI response */
		rv = frameSSIRespForEMVAdmin(pstDHIDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to frame SSI response", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			return rv;
		}
	}
	else
	{
		rv = FAILURE;
		debug_sprintf(szDbgMsg, "%s: Should not come here ", __FUNCTION__);
		RCHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}
/*
* ============================================================================
* Function Name: processEBTTransaction
*
* Description	:
*
* Input Params	: Structure
*
* Output Params:
* ============================================================================
*/
int	processEBTTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int						rv 										= SUCCESS;
	int						iCmd									= -1;
	int						iLen									= 0;
	int						iDWReqLen								= 0;
	int						iDWRespLen								= 0;
	int						iWaitTime								= 0;
	int						iAppLogEnabled							= 0;
	char					szStatMsg[256]							= "";
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszMsg	 								= NULL;
	char*					pszDWReq								= NULL;
	char*					pszDWResp								= NULL;
	char*					pszRCHIResp								= NULL;
	char*					pszRequestType							= "EBTRequest";
	char*					pszNodeName								= "Payload";
	RCHI_BOOL				bTORRequest								= RCHI_FALSE;
	RCHI_BOOL				bTORFirstTime							= RCHI_TRUE;
	time_t					tStartEpochTime  						= 0;
	time_t 					tEndEpochTime  							= 0;
	time_t 					tDiffDeconds       						= 0;
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls							= getPassthroughFields();
	RCTRANDTLS_STYPE		pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];
	METADATA_STYPE			stMeta;

#ifdef DEVDEBUG
	int				iIndex			= 0;
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Resetting all the fields in the structure for the new transaction*/
	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));
	while(1)
	{
		iCmd = getTransType(pstDHIDetails[eCOMMAND].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue, "EBT");
		if(iCmd < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure, cant process the transaction!!!", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found");
				strcpy(szAppLogDiag, "Internal DHI-RCHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = iCmd;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing EBT %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case RC_SALE:
		case RC_REFUND:
		case RC_BALANCE:

			/*fill rc details*/
			rv = fillRCDtls(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to correct RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}

			break;

		case RC_VOID:
			pszRequestType = "ReversalRequest";

			/* Copying the Void into the reversal reason code*/
			strcpy(pstRCDetails[RC_REVERSL_RSN_CODE].elementValue, pstRCDetails[RC_TRAN_TYPE].elementValue);
			rv = fillRCDtlsforFollowOnTran(pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill DHI or RC Values", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			break;

		case RC_COMPLETION:
		case RC_AUTH:
		case RC_ACTIVATE:
		case RC_REDEMPTION:
		case RC_CASH_OUT:
		case RC_HOST_TOTALS:
			rv = ERR_INV_COMBI;
			break;

		default:
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		while(1)
		{

			memset(&stMeta, 0x00, METADATA_SIZE);

			rv = getMetaDataForRCReq(&stMeta, pstDHIDetails, pstRCDetails, EBT);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}


			rv = buildXMLMsgFromMetaData(&pszMsg, &iLen, pszRequestType, &stMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
						__FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
					__FUNCTION__, iLen);
			RCHI_LIB_TRACE(szDbgMsg);


#ifdef DEVDEBUG
			if(strlen(pszMsg) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,pszMsg);
				RCHI_LIB_TRACE(szDbgMsg);
			}
#endif

			/*	Free the meta data*/
			freeMetaDataEx(&stMeta);

			rv = buildXMLReqForDW(&pszDWReq, &iDWReqLen, pszMsg, iLen, TRANSACTION);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Building of RC Request Failed");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_REQUEST, pszDWReq, NULL);
			}

			/* Updating config variables value in config.usr1 */
			rv = updateConfigParams();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Config params update failed:%d!!!", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);

				break;
			}

			/* Setting the parameter that RC lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			/* Setting the start time*/
			tStartEpochTime = time(NULL);

			debug_sprintf(szDbgMsg, "%s: Start Epoch time in Seconds is %ld", __FUNCTION__, tStartEpochTime);
			RCHI_LIB_TRACE(szDbgMsg);

			rv = postDataToRCHost(pszDWReq, iDWReqLen, &pszDWResp, &iDWRespLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				RCHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RC_RESPONSE, pszDWResp, NULL);
				}

				rv = getNodeValFromDWResp(pszDWResp, &pszNodeName, &pszRCHIResp, 1, szStatMsg);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_NOT_AVAILABLE:
						debug_sprintf(szDbgMsg, "%s: Host Not Available", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_RESP_TO:
						debug_sprintf(szDbgMsg, "%s: Host Response Timed Out", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case ERR_HOST_ERROR:
						debug_sprintf(szDbgMsg, "%s: Host Error", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the DW Response", __FUNCTION__);
						RCHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
			}

			/* This is the code for the time out reversal
			 * We need to consider two cases for the timeout reversal
			 * Either the Datawire Server return some error code or
			 * We get response timeout with datawire server itself
			 */
			if(rv != SUCCESS)
			{
				if(rv == ERR_RESP_TO)
				{
					/* Setting the end time*/
					tEndEpochTime = time(NULL);
					debug_sprintf(szDbgMsg, "%s: End Epoch time in Seconds is %ld", __FUNCTION__, tEndEpochTime);
					RCHI_LIB_TRACE(szDbgMsg);
					tDiffDeconds  = tEndEpochTime - tStartEpochTime;
					debug_sprintf(szDbgMsg, "%s: Different between start and end epcoh time in seconds is  %ld", __FUNCTION__, tDiffDeconds);
					RCHI_LIB_TRACE(szDbgMsg);
					iWaitTime = 40 - tDiffDeconds;
					debug_sprintf(szDbgMsg, "%s: Waiting time to send reversal request  %d", __FUNCTION__, iWaitTime);
					RCHI_LIB_TRACE(szDbgMsg);

					if(bTORFirstTime == RCHI_TRUE)
					{
						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Received Response Time Out, Waiting For %d Seconds To Send Time Out Reversal Request", iWaitTime);
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Sending Time Out Reversal Request For 1st time");
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						if(iWaitTime > 0)
						{
							svcWait(iWaitTime*1000);
						}

						if(pszMsg != NULL)
						{
							free(pszMsg);
							pszMsg = NULL;
						}

						if(pszDWReq != NULL)
						{
							free(pszDWReq);
							pszDWReq = NULL;
						}

						/* Need to time out reversal now*/
						pszRequestType = "ReversalRequest";

						rv = fillRCDtlsforTOR(pstDHIDetails, pstRCDetails, iCmd);
						if(rv == SUCCESS)
						{
							bTORRequest = RCHI_TRUE;
							bTORFirstTime = RCHI_FALSE;
							continue;
						}
					}
					else
					{
						/* Retrying the timeout reversal for another two times*/
						retryTimeoutReversal(pszDWReq, iWaitTime);

						rv = ERR_HOST_TRAN_TIMEOUT;
						break;
					}
				}	/*KranthiK1: Added this change so that we do not saf the actual transaction when we get conn failure for TOR*/
				else if (bTORRequest && rv == ERR_CONN_FAIL)
				{
					/* Retrying the timeout reversal for another two times*/
					retryTimeoutReversal(pszDWReq, iWaitTime);

					rv = ERR_HOST_TRAN_TIMEOUT;
					break;
				}
			}
			else
			{
				rv = parseRCResponse(pszRCHIResp, pstDHIDetails, pstRCDetails);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Parsing of RC Response failed", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}

			}
			break;
		}

		break;
	}

	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
	{
		if(bTORRequest == RCHI_TRUE)
		{
			free(pstDHIDetails[eRESULT_CODE].elementValue);
			pstDHIDetails[eRESULT_CODE].elementValue = strdup("22");

			if(pstDHIDetails[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstDHIDetails[eRESPONSE_TEXT].elementValue);
				pstDHIDetails[eRESPONSE_TEXT].elementValue =  strdup("Transaction Reversed");
			}
		}
		else
		{
			saveTranDetailsForFollowOnTran(pstDHIDetails, pstRCDetails);
		}
	}

	if(rv != SUCCESS)
	{
		fillErrorDtls(rv, pstDHIDetails, szStatMsg);
	}


#ifdef DEVDEBUG
		for(iIndex = 0; iIndex < eELEMENT_INDEX_MAX; iIndex++)
		{
			if(pstDHIDetails[iIndex].elementValue != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: DHI[%d] Value = [%s]", __FUNCTION__, iIndex, pstDHIDetails[iIndex].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);
			}
		}
		for(iIndex = RC_ADDTL_BAL_AMT_1; iIndex <= RC_ADDTL_BAL_AMT_ACC_TYPE; iIndex++)
		{

				debug_sprintf(szDbgMsg, "%s: RCHI[%d] Value = [%s]", __FUNCTION__, iIndex, pstRCDetails[iIndex].elementValue);
				RCHI_LIB_TRACE(szDbgMsg);

		}
#endif

	if(pszMsg != NULL)
	{
		free(pszMsg);
		pszMsg = NULL;
	}

	if(pszDWReq != NULL)
	{
		free(pszDWReq);
		pszDWReq = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);
	return rv;
}


/*
 * ============================================================================
 * Function Name: processCardRateCommand
 *
 * Description	:
 *
 * Input Params	: Structure
 *
 * Output Params:
 * ============================================================================
 */
int	processCardRateCommand(TRANDTLS_PTYPE pstDHIDetails)
{
	int						rv 										= SUCCESS;
	int 					iDCCReqLen								= 0;
	int						iDCCRespLen								= 0;
	int						iAppLogEnabled							= 0;
	char					szAppLogData[256]						= "";
	char					szAppLogDiag[256]						= "";
	char*					pszDCCReq								= NULL;
	char*					pszDCCResp								= NULL;
	RCHI_PASSTHROUGH_PTYPE	pstRCHIPTDtls							= getPassthroughFields();
	RCTRANDTLS_STYPE		pstRCDetails[RC_ELEMENTS_INDEX_MAX + pstRCHIPTDtls->iRespCnt + 1];

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	memset(pstRCDetails, 0x00, sizeof(pstRCDetails));

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	RCHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Card Rate Command");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		rv = fillCardRateDtls(pstDHIDetails, pstRCDetails);
		if(rv == SUCCESS && strcmp(pstDHIDetails[eDCC_OFFD].elementValue, "FALSE") == 0)
		{
			debug_sprintf(szDbgMsg, "%s:Card is not Eliglible for DCC, so setting DCC Offered to FLASE", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);

			return rv;
		}

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill FEXCO Values", __FUNCTION__);
			RCHI_LIB_TRACE(szDbgMsg);
			//pstDHIDetails[eDCC_OFFD].elementValue = strdup("FALSE");			// If Some fields are missing while Filling the Request ,Setting DCCOFFERED to FALSE
			break;
		}

		rv = buildXMLReqForFEXCO(&pszDCCReq, &iDCCReqLen, pstRCDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Building the xml Msg Failed [%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Building of RC FEXCO Request Failed");
				strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_FEXCO_REQUEST, pszDCCReq, NULL);
		}

		rv = updateCardRateRefNumToFile();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Reference Number update for Card Rate Command failed :%d!!!", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);
			break;
		}

		rv = postDataToFEXCOHost(pszDCCReq, iDCCReqLen, &pszDCCResp, &iDCCRespLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post the data to the FEXCO host[%d]", __FUNCTION__, rv);
			RCHI_LIB_TRACE(szDbgMsg);

			//pstDHIDetails[eDCC_OFFD].elementValue = strdup("FALSE");				// If any Error From the HOST we Return back DCC OFFERED as FALSE
			switch(rv)
			{
			case ERR_INV_RESP:
				debug_sprintf(szDbgMsg, "%s: Invalid Response code in the response", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				strcpy(szAppLogData, "Invaild Response code ");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);

				break;
			case ERR_HOST_NOT_AVAILABLE:
				debug_sprintf(szDbgMsg, "%s: FEXCO Host Not Available", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				strcpy(szAppLogData, "FEXCO Host Not available");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
				break;
			case ERR_RESP_TO:
				debug_sprintf(szDbgMsg, "%s: FEXCO Host Response Timed Out", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				strcpy(szAppLogData, "FEXCO Host Timed Out");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);

				break;
			case ERR_HOST_ERROR:
				debug_sprintf(szDbgMsg, "%s: FEXCO Host Error", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);

				strcpy(szAppLogData, "FEXCO Host Unavailable");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);

				break;
			case FAILURE:
			default:
				debug_sprintf(szDbgMsg, "%s: Error while parsing the DCC Response", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				break;
			}
			break;
		}
		else
		{
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s", pszDCCResp);
			RCHI_LIB_TRACE(szDbgMsg);
#endif
			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_FEXCO_RESPONSE, pszDCCResp, NULL);
			}

			rv = parseFEXCOResponse(pszDCCResp, pstDHIDetails, pstRCDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Parsing of DCC Response failed", __FUNCTION__);
				RCHI_LIB_TRACE(szDbgMsg);
				//pstDHIDetails[eDCC_OFFD].elementValue = strdup("FALSE");
				break;
			}
			else
			{
				if(strcmp(pstDHIDetails[eRESPONSE_CODE].elementValue, "0") > 0)
				{
					if(pstDHIDetails[eDCC_OFFD].elementValue != NULL)
					{
						free(pstDHIDetails[eDCC_OFFD].elementValue);
						pstDHIDetails[eDCC_OFFD].elementValue = NULL;
					}
					pstDHIDetails[eDCC_OFFD].elementValue = strdup("FALSE");
					debug_sprintf(szDbgMsg, "%s: DCC Offered is FALSE", __FUNCTION__);
					RCHI_LIB_TRACE(szDbgMsg);
					break;
				}
			}

			/* once the FGNCURCODE is got in the Response, Parsing the FCP.DAT file to Get the Other Fields*/
			if((pstRCDetails[RC_FGNCURCODE].elementValue != NULL))
			{
				rv = fillCardCurrDtls(pstDHIDetails, pstRCDetails);
			}
		}
		break;
	}
	if(rv != SUCCESS)
	{
		if(pstDHIDetails[eDCC_OFFD].elementValue != NULL)
		{
			free(pstDHIDetails[eDCC_OFFD].elementValue);
			pstDHIDetails[eDCC_OFFD].elementValue = NULL;
		}
		pstDHIDetails[eDCC_OFFD].elementValue = strdup("FALSE");

		fillErrorDtls(rv, pstDHIDetails, "Host Error");
	}

	debug_sprintf(szDbgMsg, "%s: returning[%d]", __FUNCTION__, rv);
	RCHI_LIB_TRACE(szDbgMsg);

	return rv;
}

