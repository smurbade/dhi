/******************************************************************
*                      appLog.c                                   *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis corresponds to application       *
* 			   logging (this logs to file)                        *
*                                                                 *
* Created on: Dec 15, 2014                                        *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <errno.h>

#include "DHI_App/common.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/externfunc.h"

#define INIT_APPLOG_FUNC_NAME  		"initAppLog"
#define CLOSE_APPLOG_FUNC_NAME  	"closeAppLog"
#define ENABLED_APPLOG_FUNC_NAME  	"isAppLogEnabled"
#define ADDEVENT_APPLOG_FUNC_NAME	"addAppEventLog"


int (*fnpInitAppLog)(void *);
int (*fnpCloseAppLog)(void *);
int (*fnpIsAppLogEnabled)(void *);
void (*fnpAddAppEventLog)(char *, char *, char *, char *, char *);

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: initializeAppLog
 *
 * Description	: This function is to initialize the application logging
 *
 * 				  We are using the applog library calls for this feature
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initializeAppLog()
{
	int 	rv 				= SUCCESS;
	char 	szLibName[100]	= "";

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: ----- Enter ----- ",__FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(szLibName, 0x00, sizeof(szLibName));

#ifdef DEBUG
		sprintf(szLibName, "libpsAppLogD.so");
#else
		sprintf(szLibName, "libpsAppLog.so");
#endif

		debug_sprintf(szDbgMsg, "%s: App Log library path [%s]", __FUNCTION__, szLibName);
		APP_TRACE(szDbgMsg);

		fnpInitAppLog  = NULL;
		fnpCloseAppLog = NULL;

		rv = setDynamicFunctionPtr(szLibName, INIT_APPLOG_FUNC_NAME, &fnpInitAppLog);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, INIT_APPLOG_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			//What to do here!!!
		}

		rv = setDynamicFunctionPtr(szLibName, CLOSE_APPLOG_FUNC_NAME, &fnpCloseAppLog);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, CLOSE_APPLOG_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			//What to do here!!!
		}

		rv = setDynamicFunctionPtr(szLibName, ENABLED_APPLOG_FUNC_NAME, &fnpIsAppLogEnabled);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, CLOSE_APPLOG_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			//What to do here!!!
		}

		rv = setDynamicFunctionPtr_1(szLibName, ADDEVENT_APPLOG_FUNC_NAME, &fnpAddAppEventLog);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, CLOSE_APPLOG_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			//What to do here!!!
		}

		if(fnpInitAppLog == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: InitApplog is not set, could not initialize!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS; //If it is not set, we should not throw the hard error thats why reassigning to SUCCESS here
			break;
		}

		rv = fnpInitAppLog(NULL);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while initializing the applog library!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: closeAppLog
 *
 * Description	: This function closes the applog library
 *
 * 				  We are using the applog library calls for this feature
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int closeAppLog()
{
	int 	rv 				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: ----- Enter ----- ",__FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(fnpCloseAppLog != NULL)
	{
		rv = fnpCloseAppLog(NULL);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while closing the applog library!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Successfully closed the applog library!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: closeApplog is not set, could not call library function!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isAppLogEnabled
 *
 * Description	: This function tells whether applog is enabled
 *
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int isAppLogEnabled()
{
	int 	rv 				= 0;

#ifdef DEBUG
	//char		szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg,"%s: ----- Enter ----- ",__FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(fnpIsAppLogEnabled != NULL)
	{
		rv = fnpIsAppLogEnabled(NULL);
#if 0
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while calling the applog lib fucntion!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
#endif
	}
	else
	{
		//debug_sprintf(szDbgMsg, "%s: isAppLogEnabled is not set, could not call library function!!!", __FUNCTION__);
		//APP_TRACE(szDbgMsg);
	}

	//debug_sprintf(szDbgMsg, "%s: Returning %s", __FUNCTION__, ( rv == PAAS_TRUE)?"TRUE":"FALSE");
	//APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isAppLogEnabled
 *
 * Description	: This function tells whether applog is enabled
 *
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void addAppEventLog(char*pszAppName, char* pszEntryType, char* pszEntryId, char* pszData, char* pszErrDiagSteps)
{

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg,"%s: ----- Enter ----- ",__FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(fnpAddAppEventLog != NULL)
	{
		fnpAddAppEventLog(pszAppName, pszEntryType, pszEntryId, pszData, pszErrDiagSteps);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: addAppEventLog is not set, could not call library function!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return ;
}
