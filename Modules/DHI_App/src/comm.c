/******************************************************************
*                      comm.c                                     *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis which deals with communication   *
* 			   part of the DHI Application                        *
*                                                                 *
* Created on: 09-Jul-2014                                         *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <svc.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "DHI_App/common.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/externfunc.h"
#include "DHI_App/xmlUtils.h"

#define SERVER_PORT          (9999)
#define LOCAL_HOST			"127.0.0.1"

typedef struct sockaddr_in	ADDR_IN;
typedef struct sockaddr		ADDR;
typedef struct timeval		TIME_VAL;

/* Static variables */
static DHI_BOOL	bRunListener = DHI_FALSE;


void * onServiceRequest(int arg);

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: createDHIServer
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int createDHIServer(int * fd)
{
	int		rv					= SUCCESS;
	int		iOn					= 1;
	int		listenFd			= 0;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	ADDR_IN	stSelfAddr;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		/* Create the socket for listening to POS connections */
		listenFd = socket(AF_INET, SOCK_STREAM, 0);
		if(listenFd == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Socket creation FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Socket Creation Failed");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		/* ---------- Set the socket options ----------- */

		/* 1. Socket Option: Set socket as reusable */
		rv = setsockopt(listenFd, SOL_SOCKET, SO_REUSEADDR, (char *)&iOn,
																sizeof(iOn));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to make socket reusable",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed To Set Socket Options(REUSABLE), Return Value[%d]", rv);
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		/* 2. Socket Option: Set socket as non blocking */
		rv = ioctl(listenFd, FIONBIO, &iOn);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to make socket non-blocking",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed To Set Socket Options(NON-BLOCKING), Return Value[%d]", rv);
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		/* 3. Set the address and port details for the server */
		memset(&stSelfAddr, 0x00, sizeof(ADDR_IN));
		stSelfAddr.sin_family = AF_INET;
		stSelfAddr.sin_port = htons(SERVER_PORT);
		stSelfAddr.sin_addr.s_addr = inet_addr(LOCAL_HOST);

		/* 4. Bind the socket to the details just set in the step above */
		rv = bind(listenFd, (ADDR *) &stSelfAddr, sizeof(stSelfAddr));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Server bind FAILED, rv [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed To Bind the Server, Return Value[%d]", rv);
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		/* Start listening on the socket */
		rv = listen(listenFd, 10); //TODO currently queue length we are putting 10
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to listen", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed To Listen To Connections, Return Value[%d]", rv);
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Listening on [%d]", __FUNCTION__,listenFd);
		APP_TRACE(szDbgMsg);

		*fd = listenFd;

		bRunListener = DHI_TRUE;

		break;
	}

	if((rv != SUCCESS) && (listenFd > 0))
	{
		/* Close the socket in case the socket was created but some other step
		 * failed. */

		debug_sprintf(szDbgMsg, "%s: Error while creating the server, closing it!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		close(listenFd);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: startListener
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int startListener(int listenFd)
{
	int		rv					= SUCCESS;
	int		iClientFd			= 0;
	//int		iOn					= 1;
	int		iSize				= 0;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	char	szIPPrt[50]			= "";
	ADDR_IN	stAddr;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	if(bRunListener != DHI_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Server NOT created/started, NOT starting listener ", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "DHI Server NOT Created/Started, Not Starting Listener");
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}

		rv = FAILURE;
		return rv;
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, END_INDICATOR);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Server created/started, starting listener ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(bRunListener == DHI_TRUE)
	{
		iSize = sizeof(stAddr);
		memset(&stAddr, 0x00, iSize);

		/* Accept the new connection */
		iClientFd = accept(listenFd, (ADDR *)&stAddr, (socklen_t *) &iSize);
		if(iClientFd < 0)
		{
			if((errno != EAGAIN) && (errno !=  EWOULDBLOCK))
			{
				debug_sprintf(szDbgMsg, "%s: accept FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				svcWait(500);
				continue;
			}
		}
		else if(iClientFd > 0)
		{
			/* ------- Got the new connection -------- */
			/* Get the connection information string for the new connection. The
			 * connection information string consists of IP and the port number
			 * of the POS client in the format specified below
			 * xxx.xxx.xxx.xxx:yyyy */
			sprintf(szIPPrt, "%s:%d", inet_ntoa(stAddr.sin_addr),
														htons(stAddr.sin_port));

			debug_sprintf(szDbgMsg, "%s: new conn from [%s] on %d",__FUNCTION__,
															szIPPrt, iClientFd);
			APP_TRACE(szDbgMsg);

			/* Set the new socket as non-blocking */
			//ioctl(iClientFd, FIONBIO, &iOn); //Not sure whether we have make this socket non-blocking

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, START_INDICATOR);
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);

				sprintf(szAppLogData, "Accepted New Connection From Client, Adding Job to the Queue");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START, szAppLogData, NULL);
			}

			/* Need to assign service request to thread */
			rv = addJobToThreadPool((void *)onServiceRequest, iClientFd);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding the job to the threadpool", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Adding Job to the Queue");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				//TODO What to do here?? Should we retry to add the job again??
			}
			/* Now go back to accept new connections */
		}

		svcWait(10); //Waiting for sometime before checking for the new connection
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: onServiceRequest
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void * onServiceRequest(int arg)
{
	int					rv							= SUCCESS;
	int					iIndex						= 0;
	int					iClientFd					= 0;
	int					iAppLogEnabled				= 0;
	pthread_t			threadID					= 0L;
	char				szAppLogData[256]			= "";
	char				szAppLogDiag[256]			= "";
	HTTPINFO_STYPE  	stHTTPInfo;
	PASSTHROUGH_PTYPE	pstDHIPTLst 				= getDHIPassThroughDtls();
	int					iIndexCnt					= eELEMENT_INDEX_MAX + pstDHIPTLst->iReqXMLTagsCnt + pstDHIPTLst->iRespXMLTagsCnt + 1; // The DHi Enum Index starts from 0, hence we allocate +1
	TRANDTLS_STYPE   	ssiReqResFields[iIndexCnt];
	PTVARLST_PTYPE		pstDHIVarLst				= NULL;
	DHI_METADATA_STYPE	stLocMetaData;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	threadID = pthread_self();

	debug_sprintf(szDbgMsg, "%s: Thread ID [%d] is servicing this request", __FUNCTION__, (int)threadID);
	APP_TRACE(szDbgMsg);

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Thread ID [%d] is Servicing This Request", (int)threadID);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START, szAppLogData, NULL);
	}

	memset(&stHTTPInfo, 0x00, sizeof(HTTPINFO_STYPE));

	memset(&ssiReqResFields, 0x00, (sizeof(TRANDTLS_STYPE) *  (iIndexCnt)) );

	for(iIndex = 0; iIndex <= iIndexCnt; iIndex++)
	{
		ssiReqResFields[iIndex].elementValue = NULL;
	}

	while(1)
	{
		iClientFd = (int )arg;

		debug_sprintf(szDbgMsg, "%s: Client FD[%d] ", __FUNCTION__, iClientFd);
		APP_TRACE(szDbgMsg);

		/*
		 * ---------------------------------
		 * STEP 1: Read the request
		 * ---------------------------------
		 */

		debug_sprintf(szDbgMsg, "%s: STEP #1 - Read the Request", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = readHTTPRequest(iClientFd, &stHTTPInfo);
		if(rv != SUCCESS)
		{
			if(stHTTPInfo.iMethod == HEAD_METHOD)
			{
				debug_sprintf(szDbgMsg, "%s: Need to check the Host Connection", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				checkHostConnection(&stHTTPInfo);

				debug_sprintf(szDbgMsg, "%s: Status is %d", __FUNCTION__, stHTTPInfo.iStatusCode);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to read HTTP request!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					//sprintf(szAppLogData, "Failed to Read HTTP Request From Client");
					//strcpy(szAppLogDiag, "Please Check Connection Parameters of SCA and DHI");
					//addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, szAppLogDiag);
				}
			}

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Status Code[%d] ", __FUNCTION__, stHTTPInfo.iStatusCode);
		APP_TRACE(szDbgMsg);

		if(stHTTPInfo.iStatusCode != HTTP_STATUS_OK)
		{
			debug_sprintf(szDbgMsg, "%s: HTTP Status code is not OK, need to send error response!!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Successfully Received the Request from the Client");
			addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_RECEIVE, szAppLogData, NULL);
		}

		/*
		 * ---------------------------------
		 * STEP 2: Parse the SSI request
		 * ---------------------------------
		 */
		debug_sprintf(szDbgMsg, "%s: STEP #2 - Parse the Request", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = parseSSIRequest(&stHTTPInfo, ssiReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse SSI request!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Parse SSI Request");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}

		/*
		 * ---------------------------------
		 * STEP 3: Process the SSI request
		 * ---------------------------------
		 */
		debug_sprintf(szDbgMsg, "%s: STEP #3 - Process the Request", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = processSSIRequest(&stHTTPInfo, ssiReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to process SSI request!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Process SSI Request");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}

		/*
		 * ---------------------------------
		 * STEP 4: Frame the SSI response
		 * ---------------------------------
		 */
		debug_sprintf(szDbgMsg, "%s: STEP #4 - Frame the Response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = frameSSIResponse(&stHTTPInfo, ssiReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to frame SSI response!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Frame SSI Response");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}

		break;
	}

	if(rv == SUCCESS)
	{
		rv = sendHTTPSuccessResponse(iClientFd, &stHTTPInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while sending response to client!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Send SUCCESS Response to Client");
				strcpy(szAppLogDiag, "Please Check Connection Parameters of SCA and DHI");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_SENT, szAppLogData, szAppLogDiag);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Successfully sent the response to client!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		rv = sendHTTPErrorResponse(iClientFd, &stHTTPInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while sending response to client!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Send ERROR Response to Client");
				strcpy(szAppLogDiag, "Please Check Connection Parameters of SCA and DHI");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_SENT, szAppLogData, szAppLogDiag);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Successfully sent the response to client!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	if(stHTTPInfo.pszRequest != NULL)
	{
		free(stHTTPInfo.pszRequest);
		stHTTPInfo.pszRequest = NULL;
	}

	if(stHTTPInfo.pszResponse != NULL)
	{
		xmlFree(stHTTPInfo.pszResponse);
		stHTTPInfo.pszResponse = NULL;
	}

	/*PRADEEP:17-11-16: on each transaction the values for the varlist's will get stored in PTVARLST_STYPE structure at the end of each transaction
	 * will free the value part of structure*/
	pstDHIVarLst 				= getDHIVarLstDtls();
	stLocMetaData.iTotCnt 		= pstDHIVarLst->iReqVARLSTCnt;
	stLocMetaData.keyValList 	= pstDHIVarLst->pstVarlistReqDtls;
	freeMetaData(&stLocMetaData);

	//freeing the SSI Data field structure
	for(iIndex = 0; iIndex <= iIndexCnt; iIndex++)
	{
		if(ssiReqResFields[iIndex].elementValue != NULL && (iIndex != eREPORT_RESPONSE && iIndex != eEMVADMIN_RESPONSE))
			//The fields eREPORT_RESPONSE and eEMVADMIN_RESPONSE are assigned to stHTTPInfo.pszResponse which is freed above, thats why we need not free it here
		{
			free(ssiReqResFields[iIndex].elementValue);
			ssiReqResFields[iIndex].elementValue = NULL;
		}
	}

	close(iClientFd);

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Thread ID [%d] Serviced This Request", (int)threadID);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START, szAppLogData, NULL);

		strcpy(szAppLogData, END_INDICATOR);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_COMMAND_END, szAppLogData, NULL);

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return NULL;
}
