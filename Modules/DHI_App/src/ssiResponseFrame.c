	/******************************************************************
*                      ssiResponseFrame.c                         *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis related to framing of            *
*              SSI response                                       *
* 			                                                      *
*                                                                 *
* Created on: Dec 10, 2014                                        *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "DHI_App/common.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/hash.h"
#include "DHI_App/externfunc.h"

static int framePaymentFuncTypeResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int buildGenRespDtls(xmlDocPtr , xmlNode * , TRANDTLS_PTYPE );
static int frameVersionCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameSaleCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int buildGenPymtRespDtls(xmlDocPtr , xmlNode * , TRANDTLS_PTYPE );
static int frameRefundCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameVoidCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameDevRegCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameSignatureCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameAuthCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameGiftMgtCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameReportFuncTypeResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameLastTranCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameTokenQueryCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int frameBatchFuncTypeResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
static int buildDHIPTRespDtls(xmlDocPtr , xmlNode * , TRANDTLS_PTYPE );
static int buildRespDtlsForCardRateCmd(xmlDocPtr , xmlNode * , TRANDTLS_PTYPE);
static int frameCardRateCmdResp(HTTPINFO_PTYPE , TRANDTLS_PTYPE);
extern const sFUNCTION_TYPE FUNCTION_TYPE;
extern const sCOMMAND COMMAND;
extern const sPAYMENT_TYPE PAYMENT_TYPE;

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: frameSSIResponse
 *
 * Description	: This function parses the SSI request and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int frameSSIResponse(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int			rv				= SUCCESS;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstHTTPInfo == NULL || pstSSIReqResFields == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input Params NULL!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstSSIReqResFields[eCOMMAND].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not present in the request!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if( (pstSSIReqResFields[eCOMMAND].elementValue != NULL) &&
		    (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.VERSION)) )
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for Version command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameVersionCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Version command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		if( (pstSSIReqResFields[eCOMMAND].elementValue != NULL) &&
			(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.DEVADMIN)) )
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for Device Registration command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameDevRegCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Device Registration response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		if( (pstSSIReqResFields[eCOMMAND].elementValue != NULL) &&
				(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.CARD_RATE)) )
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for CardRate command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameCardRateCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Version command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		if( (pstSSIReqResFields[eFUNCTION_TYPE].elementValue != NULL) &&
		    (!strcasecmp(pstSSIReqResFields[eFUNCTION_TYPE].elementValue, FUNCTION_TYPE.PAYMENT)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Payment Function Type", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = framePaymentFuncTypeResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing response for payment function type command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
#if 0
				rv = fillErrGenRespDtls(pstSSIReqResFields, rv);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while filling General Error response details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
#endif
			}

			break;
		}

		if( (pstSSIReqResFields[eFUNCTION_TYPE].elementValue != NULL) &&
				    (!strcasecmp(pstSSIReqResFields[eFUNCTION_TYPE].elementValue, FUNCTION_TYPE.REPORT)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Report Function Type", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameReportFuncTypeResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing response for Report function type command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;
		}

		if( (pstSSIReqResFields[eFUNCTION_TYPE].elementValue != NULL) &&
						    (!strcasecmp(pstSSIReqResFields[eFUNCTION_TYPE].elementValue, FUNCTION_TYPE.BATCH)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Batch Function Type", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameBatchFuncTypeResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing response for Batch function type command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: framePaymentFuncTypeResp
 *
 * Description	: This function frames the response for the payment function
 * 				  type command
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int framePaymentFuncTypeResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstSSIReqResFields[eCOMMAND].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Command Name not present!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.SALE))
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for SALE command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameSaleCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Sale command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.TOKEN_QUERY))
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for TOKEN_QUERY command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameTokenQueryCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Token Query command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		/*
		 * Praveen_P1: currently putting all of them under AUTH
		 * TO_DO: Please change them later
		 */
		if((!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.PRE_AUTH))  ||
		   (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.POST_AUTH)) ||
		   (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.COMPLETION))||
		   (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.VOICE_AUTH)) )
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for AUTH command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameAuthCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Auth command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.VOID))
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for VOID command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameVoidCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Void command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.CREDIT))
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for REFUND command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameRefundCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Refund command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.SIGNATURE))
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for Signature command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameSignatureCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Signature command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		/* We have populated eEMVADMIN_RESPONSE field in RCHI frameSSIRespForEMVAdmin.
		 * The SSI response consists of a list of public keys and the respective fields as passed
		 * by the host
		 */
		if(pstSSIReqResFields[eEMVADMIN_RESPONSE].elementValue != NULL)
		{
			pstHTTPInfo->pszResponse = pstSSIReqResFields[eEMVADMIN_RESPONSE].elementValue;

			if(pstSSIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue != NULL)
			{
				pstHTTPInfo->iResLength = atoi(pstSSIReqResFields[eEMVADMIN_RESP_LENGTH].elementValue);
			}
			else
			{
				pstHTTPInfo->iResLength = strlen(pstSSIReqResFields[eEMVADMIN_RESPONSE].elementValue);
			}
			break;
		}


		/*
		 * Praveen_P1: currently putting all gift management commands
		 * TO_DO: Please change them later i frequired
		 */
		if((!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.ACTIVATE))  ||
		   (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.BALANCE)) ||
		   (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.REMOVE_VALUE))||
		   (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.ADD_VALUE)) ||
		   (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.GIFT_CLOSE)) ||
		   (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.DEACTIVATE)))
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for Gift Management command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameGiftMgtCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing Gift Management command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Did not frame the response, command[%s] not detected", __FUNCTION__, pstSSIReqResFields[eCOMMAND].elementValue);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameBatchFuncTypeResp
 *
 * Description	: This function frames the response for the report function
 * 				  type command
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameBatchFuncTypeResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameReportFuncTypeResp
 *
 * Description	: This function frames the response for the report function
 * 				  type command
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameReportFuncTypeResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.LAST_TRAN))
		{
			debug_sprintf(szDbgMsg, "%s: Framing response for LAST_TRAN command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = frameLastTranCmdResp(pstHTTPInfo, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while framing LAST_TRAN command response!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		/*
		 * DHI will not frame the response for the other REPORT transactions like other commands
		 * response for the report transactions will be specific to the host, thats why
		 * it will be framed by the host libraries
		 */

		if(pstSSIReqResFields[eREPORT_RESPONSE].elementValue != NULL)
		{
			pstHTTPInfo->pszResponse = pstSSIReqResFields[eREPORT_RESPONSE].elementValue;

			if(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue != NULL)
			{
				pstHTTPInfo->iResLength = atoi(pstSSIReqResFields[eREPORT_RESP_LENGTH].elementValue);
			}
			else
			{
				pstHTTPInfo->iResLength = strlen(pstSSIReqResFields[eREPORT_RESPONSE].elementValue);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Response not received from the host library, need to frame the error response from the fields given by Host Library", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//Create the XML doc
			docPtr = xmlNewDoc(BAD_CAST "1.0");
			if(docPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			//Create the root node
			rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
			if(rootPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			xmlDocSetRootElement(docPtr, rootPtr);

			rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			/* If the XML message tree has formed correctly, dump this message into
			* the string provided as parameter */
			xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

			/* Free the memory allocated for the xml document tree */
			if(docPtr != NULL)
			{
				xmlFreeDoc(docPtr);
				xmlCleanupParser();
				xmlMemoryDump();
			}

			if(iAppLogEnabled)
			{
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameDevRegCmdResp
 *
 * Description	: This function frames the device registration response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameDevRegCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	xmlNodePtr		elemNodePtr			= NULL;
	xmlNodePtr		adminNodePtr		= NULL;
	xmlNodePtr		headerNodePtr		= NULL;
	xmlAttrPtr 		newattr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Device Registration Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "MSH");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		/*
		 * Add Header Node
		 */
		headerNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "HEADER", NULL);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create header", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		if(pstSSIReqResFields[eRESULT_CODE].elementValue != NULL)
		{
			/*
			 * add elem RESULT_CODE inside HEADER
			 */
			elemNodePtr = xmlNewChild(headerNodePtr, NULL, BAD_CAST "RESULT_CODE", BAD_CAST pstSSIReqResFields[eRESULT_CODE].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding Result Code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		if(pstSSIReqResFields[eDEVTYPE].elementValue != NULL)
		{
			/*
			 * add elem DEVTYPE inside HEADER
			 */
			elemNodePtr = xmlNewChild(headerNodePtr, NULL, BAD_CAST "DEVTYPE", BAD_CAST pstSSIReqResFields[eDEVTYPE].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding DEVTYPE field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		if(pstSSIReqResFields[eSERIALNUM].elementValue != NULL)
		{
			/*
			 * add elem SERIALNUM inside HEADER
			 */
			elemNodePtr = xmlNewChild(headerNodePtr, NULL, BAD_CAST "SERIALNUM", BAD_CAST pstSSIReqResFields[eSERIALNUM].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding SERIALNUM field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		if(pstSSIReqResFields[eTERMINAL].elementValue != NULL)
		{
			/*
			 * add elem TERMINAL inside HEADER
			 */
			elemNodePtr = xmlNewChild(headerNodePtr, NULL, BAD_CAST "TERMINAL", BAD_CAST pstSSIReqResFields[eTERMINAL].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding TERMINAL field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/*
		 * add elem (ADMIN) in root RESPONSE
		 * add attrib COMMAND="SETUP_RESPONSEV3"
		 */
		adminNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "ADMIN", NULL);
		newattr = xmlNewProp(adminNodePtr, BAD_CAST "COMMAND", BAD_CAST "SETUP_RESPONSEV3");
		if(newattr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstSSIReqResFields[eCREDIT_PROC_ID].elementValue != NULL) //Need to add CREDIT PROCESSOR ID
		{
			/*
			 * add elem SUPPORTED_FUNCTION inside ADMIN
			 */
			elemNodePtr = xmlNewChild(adminNodePtr, NULL, BAD_CAST "SUPPORTED_FUNCTION", NULL);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding CLIENT_ID field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				/*
				 * Add Attributes PROCESSOR_ID and ID to the SSUPORTED_FUCNTION NODE
				 */
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "PROCESSOR_ID", BAD_CAST pstSSIReqResFields[eCREDIT_PROC_ID].elementValue);
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "ID", BAD_CAST "CREDIT");
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

		}

		if(pstSSIReqResFields[eDEBIT_PROC_ID].elementValue != NULL) //Need to add DEBIT PROCESSOR ID
		{
			/*
			 * add elem SUPPORTED_FUNCTION inside ADMIN
			 */
			elemNodePtr = xmlNewChild(adminNodePtr, NULL, BAD_CAST "SUPPORTED_FUNCTION", NULL);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding CLIENT_ID field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				/*
				 * Add Attributes PROCESSOR_ID and ID to the SSUPORTED_FUCNTION NODE
				 */
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "PROCESSOR_ID", BAD_CAST pstSSIReqResFields[eDEBIT_PROC_ID].elementValue);
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "ID", BAD_CAST "DEBIT");
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

		}

		if(pstSSIReqResFields[eGIFT_PROC_ID].elementValue != NULL) //Need to add GIFT PROCESSOR ID
		{
			/*
			 * add elem SUPPORTED_FUNCTION inside ADMIN
			 */
			elemNodePtr = xmlNewChild(adminNodePtr, NULL, BAD_CAST "SUPPORTED_FUNCTION", NULL);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding CLIENT_ID field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				/*
				 * Add Attributes PROCESSOR_ID and ID to the SSUPORTED_FUCNTION NODE
				 */
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "PROCESSOR_ID", BAD_CAST pstSSIReqResFields[eGIFT_PROC_ID].elementValue);
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "ID", BAD_CAST "GIFT");
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

		}

		if(pstSSIReqResFields[ePRIVLBL_PROC_ID].elementValue != NULL) //Need to add PRIV_LBL PROCESSOR ID
		{
			/*
			 * add elem SUPPORTED_FUNCTION inside ADMIN
			 */
			elemNodePtr = xmlNewChild(adminNodePtr, NULL, BAD_CAST "SUPPORTED_FUNCTION", NULL);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding CLIENT_ID field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				/*
				 * Add Attributes PROCESSOR_ID and ID to the SSUPORTED_FUCNTION NODE
				 */
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "PROCESSOR_ID", BAD_CAST pstSSIReqResFields[ePRIVLBL_PROC_ID].elementValue);
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "ID", BAD_CAST "PRIV_LBL");
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

		}

		if(pstSSIReqResFields[eCHECK_PROC_ID].elementValue != NULL) //Need to add GIFT PROCESSOR ID
		{
			/*
			 * add elem SUPPORTED_FUNCTION inside ADMIN
			 */
			elemNodePtr = xmlNewChild(adminNodePtr, NULL, BAD_CAST "SUPPORTED_FUNCTION", NULL);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding CLIENT_ID field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				/*
				 * Add Attributes PROCESSOR_ID and ID to the SSUPORTED_FUCNTION NODE
				 */
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "PROCESSOR_ID", BAD_CAST pstSSIReqResFields[eCHECK_PROC_ID].elementValue);
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				newattr = xmlNewProp(elemNodePtr, BAD_CAST "ID", BAD_CAST "CHECK");
				if(newattr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt add attribute", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

		}

		if(pstSSIReqResFields[eCLIENT_ID].elementValue != NULL)
		{
			/*
			 * add elem CLIENT_ID inside ADMIN
			 */
			elemNodePtr = xmlNewChild(adminNodePtr, NULL, BAD_CAST "CLIENT_ID", BAD_CAST pstSSIReqResFields[eCLIENT_ID].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding CLIENT_ID field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		if(pstSSIReqResFields[eDEVICEKEY].elementValue != NULL)
		{
			/*
			 * add elem CLIENT_ID inside DEVICEKEY
			 */
			elemNodePtr = xmlNewChild(adminNodePtr, NULL, BAD_CAST "DEVICEKEY", BAD_CAST pstSSIReqResFields[eDEVICEKEY].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding DEVICEKEY field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/*
		 * add elem STATUSS inside ADMIN
		 */
		elemNodePtr = xmlNewChild(adminNodePtr, NULL, BAD_CAST "STATUS", BAD_CAST "1");
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding STATUS field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: frameVersionCmdResp
 *
 * Description	: This function frames the version command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameVersionCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	xmlNodePtr		elemNodePtr			= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Response for Version Command");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Adding VERSION_INFO field */
		if(pstSSIReqResFields[eVERSION_INFO].elementValue != NULL)
		{
			elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "VERSION_INFO", BAD_CAST pstSSIReqResFields[eVERSION_INFO].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding VERSION_INFO field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameSignatureCmdResp
 *
 * Description	: This function frames the Signature command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameSignatureCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Response for Signature Command");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameAuthCmdResp
 *
 * Description	: This function frames the sale command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameAuthCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Auth Command Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildGenPymtRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general payment details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameGiftMgtCmdResp
 *
 * Description	: This function frames the Gift management command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameGiftMgtCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Gift %s Command Response", pstSSIReqResFields[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildGenPymtRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general payment details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameLastTranCmdResp
 *
 * Description	: This function frames the last tran command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameLastTranCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	xmlNodePtr		elementPtr			= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing LAST_TRAN Command Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildGenPymtRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general payment details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Adding TRANS_AMOUNT field */
		if(pstSSIReqResFields[eTRANS_AMOUNT].elementValue != NULL) //TRANS_AMOUNT field is not being added in the buildGenPymtRespDtsl function thats why adding them here
		{
			elementPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "TRANS_AMOUNT", BAD_CAST pstSSIReqResFields[eTRANS_AMOUNT].elementValue);
			if(elementPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding TRANS_AMOUNT field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/* Adding ACCT_NUM field */
		if(pstSSIReqResFields[eACCT_NUM].elementValue != NULL)
		{
			elementPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "ACCT_NUM", BAD_CAST pstSSIReqResFields[eACCT_NUM].elementValue);
			if(elementPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding ACCT_NUM field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/* Adding CARDHOLDER field */
		if(pstSSIReqResFields[eCARDHOLDER].elementValue != NULL)
		{
			elementPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "CARDHOLDER", BAD_CAST pstSSIReqResFields[eCARDHOLDER].elementValue);
			if(elementPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding CARDHOLDER field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/* Adding EXPIRY MONTH field */
		if(pstSSIReqResFields[eEXP_MONTH].elementValue != NULL)
		{
			elementPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "CARD_EXP_MONTH", BAD_CAST pstSSIReqResFields[eEXP_MONTH].elementValue);
			if(elementPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding CARDHOLDER field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/* Adding EXPIRY YEAR field */
		if(pstSSIReqResFields[eEXP_YEAR].elementValue != NULL)
		{
			elementPtr = xmlNewChild(rootPtr, NULL, BAD_CAST "CARD_EXP_YEAR", BAD_CAST pstSSIReqResFields[eEXP_YEAR].elementValue);
			if(elementPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding CARDHOLDER field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: frameTokenQueryCmdResp
 *
 * Description	: This function frames the Token Query command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameTokenQueryCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Token Query Command Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildGenPymtRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general payment details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: frameSaleCmdResp
 *
 * Description	: This function frames the sale command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameSaleCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Sale Command Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildGenPymtRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general payment details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameVoidCmdResp
 *
 * Description	: This function frames the void command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameVoidCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Void Command Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildGenPymtRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general payment details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameRefundCmdResp
 *
 * Description	: This function frames the refund command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameRefundCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Refund Command Response");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildGenPymtRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general payment details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* If the XML message tree has formed correctly, dump this message into
		* the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}
		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameCardRateCmdResp
 *
 * Description	: This function frames the version command response from the
 * 				  the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int frameCardRateCmdResp(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= 0;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Response for Card Rate Command");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		//Create the XML doc
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Create the root node
		rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		xmlDocSetRootElement(docPtr, rootPtr);

		rv = buildGenRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildRespDtlsForCardRateCmd(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildDHIPTRespDtls(docPtr, rootPtr, pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldnt build PT Response details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* If the XML message tree has formed correctly, dump this message into
		 * the string provided as parameter */
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) &pstHTTPInfo->pszResponse, &pstHTTPInfo->iResLength, "UTF-8", 0);

		/* Free the memory allocated for the xml document tree */
		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
			xmlCleanupParser();
			xmlMemoryDump();
		}

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, pstHTTPInfo->pszResponse, NULL);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildGenRespDtls
 *
 * Description	: This function adds the general response details to the
 * 				  xml doc
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildGenRespDtls(xmlDocPtr docPtr, xmlNode * rootNode, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv				= SUCCESS;
	xmlNodePtr		elemNodePtr		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(docPtr == NULL || rootNode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param(docPtr/rootNode) is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Adding TERMINATION_STATUS field */
	if(pstSSIReqResFields[eTERMINATION_STATUS].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST pstSSIReqResFields[eTERMINATION_STATUS].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMINATION_STATUS field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding RESULT field */
	if(pstSSIReqResFields[eRESULT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT", BAD_CAST pstSSIReqResFields[eRESULT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding RESULT_CODE field */
	if(pstSSIReqResFields[eRESULT_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT_CODE", BAD_CAST pstSSIReqResFields[eRESULT_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding RESPONSE_TEXT field */
	if(pstSSIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST pstSSIReqResFields[eRESPONSE_TEXT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESPONSE_TEXT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding eHOST_RESPCODE field */
	if(pstSSIReqResFields[eHOST_RESPCODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST pstSSIReqResFields[eHOST_RESPCODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding HOST_RESPCODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}


	/* Adding eGATEWAY_RESULT_CODE field
	Added this specifically for Elavon Host. We usually modify the RESULT_CODE to 4, 5, 6, or 7 to SCA to understand Approved or Declined, but Elavon POS may expect the original RESULT code from Host.
	So sending original RESULT code in this field..*/
	if(pstSSIReqResFields[eGATEWAY_RESULT_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "GATEWAY_RESULT_CODE", BAD_CAST pstSSIReqResFields[eGATEWAY_RESULT_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding eGATEWAY_RESULT_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildRespDtlsForCardRateCmd
 *
 * Description	: This function adds the response details of FEXCO to the
 * 				  xml doc
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildRespDtlsForCardRateCmd(xmlDocPtr docPtr, xmlNode * rootNode, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv				= SUCCESS;
	xmlNodePtr		elemNodePtr		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(docPtr == NULL || rootNode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param(docPtr/rootNode) is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Adding EXCHANGERATE field */
	if((pstSSIReqResFields[eEXGN_RATE].elementValue != NULL) && strlen(pstSSIReqResFields[eEXGN_RATE].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "EXCHANGE_RATE", BAD_CAST pstSSIReqResFields[eEXGN_RATE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EXCHANGE_RATE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding FGNCURCODE field */
	if((pstSSIReqResFields[eFGN_CUR_CODE].elementValue != NULL) && strlen(pstSSIReqResFields[eFGN_CUR_CODE].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "FGN_CURR_CODE", BAD_CAST pstSSIReqResFields[eFGN_CUR_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding FGN_CURR_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding FGNAMOUNT field */
	if((pstSSIReqResFields[eFGN_AMT].elementValue != NULL) && strlen(pstSSIReqResFields[eFGN_AMT].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "FGN_AMOUNT", BAD_CAST pstSSIReqResFields[eFGN_AMT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding FGN_AMOUNT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding DCCOFFERED field */
	if((pstSSIReqResFields[eDCC_OFFD].elementValue != NULL) && strlen(pstSSIReqResFields[eDCC_OFFD].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "DCC_OFFERED", BAD_CAST pstSSIReqResFields[eDCC_OFFD].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding DCC_OFFERED field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding VALID_HOURS field */
	if((pstSSIReqResFields[eVALID_HRS].elementValue != NULL) && strlen(pstSSIReqResFields[eVALID_HRS].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "VALID_HOURS", BAD_CAST pstSSIReqResFields[eVALID_HRS].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding VALID_HOURS field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding MARGINRATEPERCENTAGE field */
	if((pstSSIReqResFields[eMRGN_RATE_PERCENT].elementValue != NULL) && strlen(pstSSIReqResFields[eMRGN_RATE_PERCENT].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "MARGINRATE_PERCENTAGE", BAD_CAST pstSSIReqResFields[eMRGN_RATE_PERCENT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding MARGINRATE_PERCENTAGE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding EXCHANGERATESOURCENAME field */
	if((pstSSIReqResFields[eEXGN_RATE_SRC_NAME].elementValue != NULL) && strlen(pstSSIReqResFields[eEXGN_RATE_SRC_NAME].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "EXCHANGE_RATE_SOURCENAME", BAD_CAST pstSSIReqResFields[eEXGN_RATE_SRC_NAME].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EXCHANGE_RATE_SOURCENAME field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding COMMISSIONPERCENTAGE field */
	if((pstSSIReqResFields[eCOMM_PERCENT].elementValue != NULL) && strlen(pstSSIReqResFields[eCOMM_PERCENT].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "COMMISSION_PERCENTAGE", BAD_CAST pstSSIReqResFields[eCOMM_PERCENT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding COMMISSION_PERCENTAGE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding EXCHANGERATESOURCETIMESTAMP field */
	if((pstSSIReqResFields[eEXGN_RATE_SRCTIMESTAMP].elementValue != NULL) && strlen(pstSSIReqResFields[eEXGN_RATE_SRCTIMESTAMP].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "EXCHANGERATE_SOURCETIMESTAMP", BAD_CAST pstSSIReqResFields[eEXGN_RATE_SRCTIMESTAMP].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EXCHANGERATESOURCETIMESTAMP field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding MINORUNITS field */
	if((pstSSIReqResFields[eMINR_UNT].elementValue != NULL) && strlen(pstSSIReqResFields[eMINR_UNT].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "MINOR_UNITS", BAD_CAST pstSSIReqResFields[eMINR_UNT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding MINOR_UNITS field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
#if 0
	/* Adding CURRENCY_CODE field */
	if(pstSSIReqResFields[eCURR_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "CURRENCY_CODE", BAD_CAST pstSSIReqResFields[eCURR_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding CURRENCY_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
#endif

	/* Adding COUNTRY_NAME field */
	if((pstSSIReqResFields[eCOUN_NAME].elementValue != NULL) && strlen(pstSSIReqResFields[eCOUN_NAME].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "COUNTRY_NAME", BAD_CAST pstSSIReqResFields[eCOUN_NAME].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding COUNTRY_NAME field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding CURRENCY_NAME field */
	if((pstSSIReqResFields[eCURR_NAME].elementValue != NULL) && strlen(pstSSIReqResFields[eCURR_NAME].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "CURRENCY_NAME", BAD_CAST pstSSIReqResFields[eCURR_NAME].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding CURRENCY_NAME field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding ALPHA_CURRENCY_CODE field */
	if((pstSSIReqResFields[eALPHA_CURR_CODE].elementValue != NULL) && strlen(pstSSIReqResFields[eALPHA_CURR_CODE].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "ALPHA_CURRENCY_CODE", BAD_CAST pstSSIReqResFields[eALPHA_CURR_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding ALPHA_CURRENCY_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding COUNTRY_ALPHA_2_CODE field */
	if((pstSSIReqResFields[eCOUN_ALPHA_2_CODE].elementValue != NULL) && strlen(pstSSIReqResFields[eCOUN_ALPHA_2_CODE].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "COUNTRY_ALPHA_2_CODE", BAD_CAST pstSSIReqResFields[eCOUN_ALPHA_2_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding COUNTRY_ALPHA_2_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding CURRENCY_SYMBOL field */
	if((pstSSIReqResFields[eCURR_SYMBOL].elementValue != NULL) && strlen(pstSSIReqResFields[eCURR_SYMBOL].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "CURRENCY_SYMBOL", BAD_CAST pstSSIReqResFields[eCURR_SYMBOL].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding CURRENCY_SYMBOL field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding MINOR_CURRENCY_UNIT field */
	if((pstSSIReqResFields[eMINOR_CURR_UNIT].elementValue != NULL) && strlen(pstSSIReqResFields[eMINOR_CURR_UNIT].elementValue) > 0)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "MINOR_CURRENCY_UNIT", BAD_CAST pstSSIReqResFields[eMINOR_CURR_UNIT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding MINOR_CURRENCY_UNIT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: buildGenPymtRespDtls
 *
 * Description	: This function adds the general response details to the
 * 				  xml doc
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildGenPymtRespDtls(xmlDocPtr docPtr, xmlNode * rootNode, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv				= SUCCESS;
	xmlNodePtr		elemNodePtr		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(docPtr == NULL || rootNode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param(docPtr/rootNode) is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Adding COMMAND field */
	if(pstSSIReqResFields[eCOMMAND].elementValue != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Command is present %s", __FUNCTION__, pstSSIReqResFields[eCOMMAND].elementValue);
		APP_TRACE(szDbgMsg); //For testing purpose, remove it later

		if(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.LAST_TRAN)) //If it is last tran command we have send the last tran command
		{
			debug_sprintf(szDbgMsg, "%s: Its last tran command", __FUNCTION__);
			APP_TRACE(szDbgMsg); //For testing purpose, remove it later

			if(pstSSIReqResFields[eLASTTRAN_COMMAND].elementValue != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Last tran command present %s", __FUNCTION__, pstSSIReqResFields[eLASTTRAN_COMMAND].elementValue);
				APP_TRACE(szDbgMsg); //For testing purpose, remove it later

				elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "COMMAND", BAD_CAST pstSSIReqResFields[eLASTTRAN_COMMAND].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding COMMAND field", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else
		{
			elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "COMMAND", BAD_CAST pstSSIReqResFields[eCOMMAND].elementValue);
			if(elemNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding COMMAND field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
	}


	/* Adding INTRN_SEQ_NUM field */
	if(pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "INTRN_SEQ_NUM", BAD_CAST pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding INTRN_SEQ_NUM field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding TRANS_SEQ_NUM field */
	if(pstSSIReqResFields[eTRANS_SEQ_NUM].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TRANS_SEQ_NUM", BAD_CAST pstSSIReqResFields[eTRANS_SEQ_NUM].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TRANS_SEQ_NUM field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding TROUTD field */
	if(pstSSIReqResFields[eTROUTD].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TROUTD", BAD_CAST pstSSIReqResFields[eTROUTD].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TROUTD field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding CTROUTD field */
	if(pstSSIReqResFields[eCTROUTD].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "CTROUTD", BAD_CAST pstSSIReqResFields[eCTROUTD].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding CTROUTD field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding AUTH_CODE field */
	if(pstSSIReqResFields[eAUTH_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "AUTH_CODE", BAD_CAST pstSSIReqResFields[eAUTH_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding AUTH_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding CLIENT_ID field */
	if(pstSSIReqResFields[eCLIENT_ID].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "CLIENT_ID", BAD_CAST pstSSIReqResFields[eCLIENT_ID].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding CLIENT_ID field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding INVOICE field */
	if(pstSSIReqResFields[eINVOICE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "INVOICE", BAD_CAST pstSSIReqResFields[eINVOICE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding INVOICE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding PAYMENT_MEDIA field */
	if(pstSSIReqResFields[ePAYMENT_MEDIA].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "PAYMENT_MEDIA", BAD_CAST pstSSIReqResFields[ePAYMENT_MEDIA].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding PAYMENT_MEDIA field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding PAYMENT_TYPE field */
	if(pstSSIReqResFields[ePAYMENT_TYPE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "PAYMENT_TYPE", BAD_CAST pstSSIReqResFields[ePAYMENT_TYPE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding PAYMENT_TYPE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding VSP_CODE field */
	if(pstSSIReqResFields[eVSP_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "VSP_CODE", BAD_CAST pstSSIReqResFields[eVSP_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding VSP_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding VSP_TRXID field */
	if(pstSSIReqResFields[eVSP_TRXID].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "VSP_TRXID", BAD_CAST pstSSIReqResFields[eVSP_TRXID].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding VSP_TRXID field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding VSP_RESULTDESC field */
	if(pstSSIReqResFields[eVSP_RESULTDESC].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "VSP_RESULTDESC", BAD_CAST pstSSIReqResFields[eVSP_RESULTDESC].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding VSP_RESULTDESC field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding CVV2_CODE field */
	if(pstSSIReqResFields[eCVV2_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "CVV2_CODE", BAD_CAST pstSSIReqResFields[eCVV2_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding CVV2_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding AVS_CODE field */
	if(pstSSIReqResFields[eAVS_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "AVS_CODE", BAD_CAST pstSSIReqResFields[eAVS_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding AVS_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding AUTH_RESP_CODE field */
	if(pstSSIReqResFields[eAUTH_RESP_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "AUTH_RESP_CODE", BAD_CAST pstSSIReqResFields[eAUTH_RESP_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding AUTH_RESP_CODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding TRACE_NUM field */
	if(pstSSIReqResFields[eTRACE_NUM].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TRACE_NUM", BAD_CAST pstSSIReqResFields[eTRACE_NUM].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TRACE_NUM field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding TXN_POSENTRYMODE field */
	if(pstSSIReqResFields[eTXN_POSENTRYMODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TXN_POSENTRYMODE", BAD_CAST pstSSIReqResFields[eTXN_POSENTRYMODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TXN_POSENTRYMODE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding BATCH_NUMBER field */
	if(pstSSIReqResFields[eBATCH_NUM].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "BATCH_NUMBER", BAD_CAST pstSSIReqResFields[eBATCH_NUM].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding BATCH_NUM field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

//	/* Adding CVV2_CODE field */
//	if(pstSSIReqResFields[eCVV2_CODE].elementValue != NULL)
//	{
//		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "CVV2_CODE", BAD_CAST pstSSIReqResFields[eCVV2_CODE].elementValue);
//		if(elemNodePtr == NULL)
//		{
//			debug_sprintf(szDbgMsg, "%s: Error while adding CVV2_CODE field", __FUNCTION__);
//			APP_TRACE(szDbgMsg);
//		}
//	}

	/* Adding MERCHID field */
	if(pstSSIReqResFields[eMERCHID].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "MERCHID", BAD_CAST pstSSIReqResFields[eMERCHID].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding MERCHID field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding TERMID field */
	if(pstSSIReqResFields[eTERMID].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TERMID", BAD_CAST pstSSIReqResFields[eTERMID].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMID field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding LANE field */
	if(pstSSIReqResFields[eLANE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "LANE", BAD_CAST pstSSIReqResFields[eLANE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding LANE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding APPROVED_AMOUNT field */
	if(pstSSIReqResFields[eAPPROVED_AMOUNT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "APPROVED_AMOUNT", BAD_CAST pstSSIReqResFields[eAPPROVED_AMOUNT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding APPROVED_AMOUNT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding AUTH_AMOUNT field */
	if(pstSSIReqResFields[eAUTH_AMOUNT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "AUTH_AMOUNT", BAD_CAST pstSSIReqResFields[eAUTH_AMOUNT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding AUTH_AMOUNT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding AVAIL_BALANCE field */
	if(pstSSIReqResFields[eAVAIL_BALANCE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "AVAIL_BALANCE", BAD_CAST pstSSIReqResFields[eAVAIL_BALANCE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding AVAIL_BALANCE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding CASH_BENEFIT Field */
	if(pstSSIReqResFields[eCB_AVAIL_BALANCE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "CB_AVAIL_BALANCE", BAD_CAST pstSSIReqResFields[eCB_AVAIL_BALANCE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding CB_AVAIL_BALANCE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding FOOD_STAMP Field */
	if(pstSSIReqResFields[eFS_AVAIL_BALANCE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "FS_AVAIL_BALANCE", BAD_CAST pstSSIReqResFields[eFS_AVAIL_BALANCE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding FS_AVAIL_BALANCE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding AMOUNT_BALANCE field */
	if(pstSSIReqResFields[eAMOUNT_BALANCE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "AMOUNT_BALANCE", BAD_CAST pstSSIReqResFields[eAMOUNT_BALANCE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding AMOUNT_BALANCE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding ORIG_TRANS_AMOUNT field */
	if(pstSSIReqResFields[eORIG_TRANS_AMOUNT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "ORIG_TRANS_AMOUNT", BAD_CAST pstSSIReqResFields[eORIG_TRANS_AMOUNT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding ORIG_TRANS_AMOUNT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding eDIFF_AMOUNT_DUE field */
	if(pstSSIReqResFields[eDIFF_AMOUNT_DUE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "DIFF_AMOUNT_DUE", BAD_CAST pstSSIReqResFields[eDIFF_AMOUNT_DUE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding DIFF_AMOUNT_DUE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding DIFF_DUE_AMOUNT field */
	if(pstSSIReqResFields[eDIFF_DUE_AMOUNT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "DIFF_DUE_AMOUNT", BAD_CAST pstSSIReqResFields[eDIFF_DUE_AMOUNT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding DIFF_DUE_AMOUNT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding DIFF_AUTH_AMOUNT field */
	if(pstSSIReqResFields[eDIFF_AUTH_AMOUNT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "DIFF_AUTH_AMOUNT", BAD_CAST pstSSIReqResFields[eDIFF_AUTH_AMOUNT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding DIFF_AUTH_AMOUNT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding PREVIOUS_BALANCE field */
	if(pstSSIReqResFields[ePREVIOUS_BALANCE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "PREVIOUS_BALANCE", BAD_CAST pstSSIReqResFields[ePREVIOUS_BALANCE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding PREVIOUS_BALANCE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding EMBOSSED_ACCT_NUM field */
	if(pstSSIReqResFields[eEMBOSSED_ACCT_NUM].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "EMBOSSED_ACCT_NUM", BAD_CAST pstSSIReqResFields[eEMBOSSED_ACCT_NUM].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EMBOSSED_ACCT_NUM field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding BANK_USERDATA field */
	if(pstSSIReqResFields[eBANK_USERDATA].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "BANK_USERDATA", BAD_CAST pstSSIReqResFields[eBANK_USERDATA].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding BANK_USERDATA field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding eTRANS_DATE field */
	if(pstSSIReqResFields[eTRANS_DATE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TRANS_DATE", BAD_CAST pstSSIReqResFields[eTRANS_DATE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TRANS_DATE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding eTRANS_TIME field */
	if(pstSSIReqResFields[eTRANS_TIME].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TRANS_TIME", BAD_CAST pstSSIReqResFields[eTRANS_TIME].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TRANS_TIME field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding LPTOKEN field */
	if(pstSSIReqResFields[eLPTOKEN].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "LPTOKEN", BAD_CAST pstSSIReqResFields[eLPTOKEN].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding LPTOKEN field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding ACK_REQUIRED field */
	if(pstSSIReqResFields[eACK_REQUIRED].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "ACK_REQUIRED", BAD_CAST pstSSIReqResFields[eACK_REQUIRED].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding ACK_REQUIRED field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding CARD_TOKEN field */
	if(pstSSIReqResFields[eCARD_TOKEN].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "CARD_TOKEN", BAD_CAST pstSSIReqResFields[eCARD_TOKEN].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding CARD_TOKEN field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding PINLESSDEBIT field */
	if(pstSSIReqResFields[ePINLESSDEBIT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "PINLESSDEBIT", BAD_CAST pstSSIReqResFields[ePINLESSDEBIT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding PINLESSDEBIT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding EMV Tags which are returned from Host */
	if(pstSSIReqResFields[eEMV_TAGS_RESP].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "EMV_TAGS_RESPONSE", BAD_CAST pstSSIReqResFields[eEMV_TAGS_RESP].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding PINLESSDEBIT field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	/* Adding EMV Tags which are returned from Host */
	if(pstSSIReqResFields[eEMV_UPDATE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "EMV_UPDATE", BAD_CAST pstSSIReqResFields[eEMV_UPDATE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EMV UPDATE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding AUTHNWID Tags which are returned from Host */
	if(pstSSIReqResFields[eAUTH_NWID].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "AUTHNWID", BAD_CAST pstSSIReqResFields[eAUTH_NWID].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EMV UPDATE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding Passthrough Tags - Currently this is specific to Elavon Host */
	if(pstSSIReqResFields[ePASSTHROUGH_FIELDS].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "PASSTHROUGH_FIELDS", BAD_CAST pstSSIReqResFields[ePASSTHROUGH_FIELDS].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding Passthrough XML Tag in DHI/SSI Response field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding EMV tag 91 - Currently this is specific to Elavon Host */
	if(pstSSIReqResFields[eISSUER_AUTH_DATA_91TAG].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "API_FIELD_1301", BAD_CAST pstSSIReqResFields[eISSUER_AUTH_DATA_91TAG].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EMV tag 91 XML Tag in DHI/SSI Response field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding  EMV TAG 71 - Currently this is specific to Elavon Host */
	if(pstSSIReqResFields[ePRE_AC_GEN_ISSUE_SCRIPTS_71].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "API_FIELD_1308", BAD_CAST pstSSIReqResFields[ePRE_AC_GEN_ISSUE_SCRIPTS_71].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EMV TAG 71 XML Tag in DHI/SSI Response field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding EMV TAG 72 - Currently this is specific to Elavon Host */
	if(pstSSIReqResFields[ePRE_AC_GEN_ISSUE_SCRIPTS_72].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "API_FIELD_1343", BAD_CAST pstSSIReqResFields[ePRE_AC_GEN_ISSUE_SCRIPTS_72].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EMV TAG72 XML Tag in DHI/SSI Response field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding EMV TAG 9F18- Currently this is specific to Elavon Host */
	if(pstSSIReqResFields[eISSUER_SCRIPT_DENTIFIER_9F18].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "API_FIELD_1310", BAD_CAST pstSSIReqResFields[eISSUER_SCRIPT_DENTIFIER_9F18].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding EMV TAG 9F18 XML Tag in DHI/SSI Response field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding Transaction Amount- Currently this is specific to Elavon Host */
	if(pstSSIReqResFields[eELAVON_TRANS_AMOUNT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "API_FIELD_0002", BAD_CAST pstSSIReqResFields[eELAVON_TRANS_AMOUNT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding Elavon Transaction Amount XML Tag in DHI/SSI Response field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Adding Approved Amount- Currently this is specific to Elavon Host */
	if(pstSSIReqResFields[eELAVON_APPROVED_AMOUNT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "API_FIELD_0130", BAD_CAST pstSSIReqResFields[eELAVON_APPROVED_AMOUNT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding Elavon Approved Amount XML Tag in DHI/SSI Response field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildDHIPTRespDtls
 *
 * Description	: This function adds the DHI Pass Through Response details to the
 * 				  xml doc
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildDHIPTRespDtls(xmlDocPtr docPtr, xmlNode * rootNode, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int					rv				= SUCCESS;
	int					iCount			= 0;
	int					iDHIIndex		= 0;
	int					iPTRespCnt		= 0;
	PASSTHROUGH_PTYPE	pstDHIPTLst		= NULL;
	xmlNodePtr			elemNodePtr		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(docPtr == NULL || rootNode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param(docPtr/rootNode) is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	pstDHIPTLst = getDHIPassThroughDtls();
	iPTRespCnt = pstDHIPTLst->iRespXMLTagsCnt;

	debug_sprintf(szDbgMsg, "%s: iPTRespCnt [%d]", __FUNCTION__, iPTRespCnt);
	APP_TRACE(szDbgMsg);

	if(iPTRespCnt > 0)
	{
		for(iCount = 0; iCount < iPTRespCnt; iCount++)
		{
			iDHIIndex = pstDHIPTLst->pstPTXMLRespDtls[iCount].index;

			if(iDHIIndex != -1 && (pstDHIPTLst->pstPTXMLRespDtls[iCount].key != NULL) && (strlen(pstDHIPTLst->pstPTXMLRespDtls[iCount].key) > 0)
					&& (pstSSIReqResFields[iDHIIndex].elementValue != NULL) && (strlen(pstSSIReqResFields[iDHIIndex].elementValue) > 0))
			{
				elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST pstDHIPTLst->pstPTXMLRespDtls[iCount].key, BAD_CAST pstSSIReqResFields[iDHIIndex].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding Pass Through field", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
