/******************************************************************
*                      configParams.c                             *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis which loads and reads the 		  *
* 			   config parameters required for the DHI Application *
*                                                                 *
* Created on: 09-Jul-2014                                         *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <svc.h>
#include <errno.h>

#include "iniparser.h"
#include "DHI_App/common.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/externfunc.h"
#include "DHI_App/dhiCfgDef.h"

DHI_CFG_STYPE dhiSettings;

static PASSTHROUGH_STYPE	gstDHIPTLst;
static PTVARLST_STYPE		gstPTVarLst;

static void initializeDHISettings();
static int loadPassThroughVarlist(dictionary * , KEYVAL_PTYPE, char * );
static int loadKeyListDetails(dictionary * , DHI_VARLST_INFO_PTYPE, char * );
static int getValueTypeofVarListElement(char * , int *);
static int addVarListSingleElements(char *, DHI_VARLST_INFO_PTYPE) ;


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: loadConfigParams
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.ur1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	loadConfigParams()
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int		iAppLogEnabled		= 0;
	char	szTemp[50]			= "";
	char	szTemp2[20]			= "";
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	initializeDHISettings();

	iLen = sizeof(szTemp);

	/* ------------ Check for Credit Processor ID ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DHI, CREDIT_PROCESSOR, szTemp, iLen);
	if(rv > 0)
	{
		sprintf(dhiSettings.szCreditProcID, "%s", szTemp);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Credit Processor [%s]", dhiSettings.szCreditProcID);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Credit Processor not found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Config Parameter [%s] Not Found", CREDIT_PROCESSOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Credit Processor [%s]", __FUNCTION__, dhiSettings.szCreditProcID);
	APP_TRACE(szDbgMsg);

	/* ------------ Check for Debit Processor ID ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DHI, DEBIT_PROCESSOR, szTemp, iLen);
	if(rv > 0)
	{
		sprintf(dhiSettings.szDebitProcID, "%s", szTemp);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Debit Processor [%s]", dhiSettings.szDebitProcID);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Debit Processor not found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Config Parameter [%s] Not Found", DEBIT_PROCESSOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Debit Processor [%s]", __FUNCTION__, dhiSettings.szDebitProcID);
	APP_TRACE(szDbgMsg);


	/* ------------ Check for Gift Processor ID ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DHI, GIFT_PROCESSOR, szTemp, iLen);
	if(rv > 0)
	{
		sprintf(dhiSettings.szGiftProcID, "%s", szTemp);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Gift Processor [%s]", dhiSettings.szGiftProcID);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Gift Processor not found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Config Parameter [%s] Not Found", GIFT_PROCESSOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Gift Processor [%s]", __FUNCTION__, dhiSettings.szGiftProcID);
	APP_TRACE(szDbgMsg);

	/* ------------ Check for Private Label Processor ID ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DHI, PRIVLBL_PROCESSOR, szTemp, iLen);
	if(rv > 0)
	{
		sprintf(dhiSettings.szPrivLblProcID, "%s", szTemp);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Private Label Processor [%s]", dhiSettings.szPrivLblProcID);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Private Label Processor not found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Config Parameter [%s] Not Found", PRIVLBL_PROCESSOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Private Label Processor [%s]", __FUNCTION__, dhiSettings.szPrivLblProcID);
	APP_TRACE(szDbgMsg);

	/* ------------ Check for Check Processor ID ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DHI, CHECK_PROCESSOR, szTemp, iLen);
	if(rv > 0)
	{
		sprintf(dhiSettings.szCheckProcID, "%s", szTemp);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Check Processor [%s]", dhiSettings.szCheckProcID);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Check Processor not found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Config Parameter [%s] Not Found", CHECK_PROCESSOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Check Processor [%s]", __FUNCTION__, dhiSettings.szCheckProcID);
	APP_TRACE(szDbgMsg);

	/* ------------ Check for EBT Processor ID ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DHI, EBT_PROCESSOR, szTemp, iLen);
	if(rv > 0)
	{
		sprintf(dhiSettings.szEBTProcID, "%s", szTemp);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "EBT Processor [%s]", dhiSettings.szEBTProcID);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: EBT Processor not found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Config Parameter [%s] Not Found", EBT_PROCESSOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: EBT Processor [%s]", __FUNCTION__, dhiSettings.szEBTProcID);
	APP_TRACE(szDbgMsg);

	/* ------------ Check for MID ----------- */
	memset(szTemp, 0x00, iLen);
	memset(szTemp2, 0x00, sizeof(szTemp2));
	if(strcmp(dhiSettings.szCreditProcID, "rc") == 0)
	{
		rv = getEnvFile(SECTION_RCHI, MERCHANT_ID, szTemp, iLen);
	}
	else if(strcmp(dhiSettings.szCreditProcID, "cp") == 0)
	{
		rv = getEnvFile(SECTION_CPHI, MERCHANT_ID, szTemp, iLen);
	}
	else if(strcmp(dhiSettings.szCreditProcID, "elvn") == 0)
	{
		rv = getEnvFile(SECTION_EHI, CHAIN_CODE, szTemp, iLen);
		strcat(szTemp, ":");
		rv = getEnvFile(SECTION_EHI, LOCATION, szTemp2, iLen);
		strcat(szTemp, szTemp2);
	}
	else
	{
		rv = getEnvFile(SECTION_DHI, MERCHANT_ID, szTemp, iLen);
	}
	if(rv > 0)
	{
		sprintf(dhiSettings.szMID, "%s", szTemp);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "MID [%s]", dhiSettings.szMID);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: MID not found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Config Parameter [%s] Not Found", MERCHANT_ID);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: MID [%s]", __FUNCTION__, dhiSettings.szMID);
	APP_TRACE(szDbgMsg);

	/* ------------ Check for TID ----------- */
	memset(szTemp, 0x00, iLen);
	if(strcmp(dhiSettings.szCreditProcID, "rc") == 0)
	{
		rv = getEnvFile(SECTION_RCHI, TERMINAL_ID, szTemp, iLen);
	}
	else if(strcmp(dhiSettings.szCreditProcID, "cp") == 0)
	{
		rv = getEnvFile(SECTION_CPHI, TERMINAL_ID, szTemp, iLen);
	}
	else if(strcmp(dhiSettings.szCreditProcID, "elvn") == 0)
	{
		rv = getEnvFile(SECTION_EHI, TERMINAL_ID, szTemp, iLen);
	}
	else
	{
		rv = getEnvFile(SECTION_DHI, TERMINAL_ID, szTemp, iLen);
	}
	if(rv > 0)
	{
		sprintf(dhiSettings.szTID, "%s", szTemp);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "TID [%s]", dhiSettings.szTID);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: TID not found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Config Parameter [%s] Not Found", TERMINAL_ID);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: TID [%s]", __FUNCTION__, dhiSettings.szTID);
	APP_TRACE(szDbgMsg);

	/* ------------ Check for DHI Processor Field ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DHI, DHI_PROCESSOR, szTemp, iLen);
	if(rv > 0)
	{
		sprintf(dhiSettings.szDHIProcessor, "%s", szTemp);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "DHI Processor Field Value [%s]", dhiSettings.szDHIProcessor);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: DHI Processor Config Parameter not found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Config Parameter [%s] Not Found", DHI_PROCESSOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}
	}

	/*If Pass through File is Configured wrongly, DHI Application won't come up because transactions may go wrong in Case of EHI*/
	rv = loadDHIPassThroughXMLDtls();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to load the DHI Pass through Fields [%d]", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);
	}

	//rv = SUCCESS; //Assigning them to SUCCESS since there is no hard error if any of the config param is missing

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: initializeDHISettings
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static void initializeDHISettings()
{
	memset(&dhiSettings, 0x00, sizeof(DHI_CFG_STYPE));
}

/*
 * ============================================================================
 * Function Name: getCreditProcessorName
 *
 * Description	: This function gives the credit processor id
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
void getCreditProcessorName(char *szProcessor)
{
	sprintf(szProcessor, "%s", dhiSettings.szCreditProcID);
}

/*
 * ============================================================================
 * Function Name: getDebitProcessorName
 *
 * Description	: This function gives the Debit processor id
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
void getDebitProcessorName(char *szProcessor)
{
	sprintf(szProcessor, "%s", dhiSettings.szDebitProcID);
}

/*
 * ============================================================================
 * Function Name: getGiftProcessorName
 *
 * Description	: This function gives the Gift processor id
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
void getGiftProcessorName(char *szProcessor)
{
	sprintf(szProcessor, "%s", dhiSettings.szGiftProcID);
}

/*
 * ============================================================================
 * Function Name: getPrivLblProcessorName
 *
 * Description	: This function gives the Private Label processor id
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
void getPrivLblProcessorName(char *szProcessor)
{
	sprintf(szProcessor, "%s", dhiSettings.szPrivLblProcID);
}

/*
 * ============================================================================
 * Function Name: getCheckProcessorName
 *
 * Description	: This function gives the Check processor id
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
void getCheckProcessorName(char *szProcessor)
{
	sprintf(szProcessor, "%s", dhiSettings.szCheckProcID);
}

/*
 * ============================================================================
 * Function Name: getDHIProcessorField
 *
 * Description	: This function gives the processor field value in DHI section
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
void getDHIProcessorField(char *szProcessor)
{
	sprintf(szProcessor, "%s", dhiSettings.szDHIProcessor);
}

/*
 * ============================================================================
 * Function Name: loadDHIPassThroughXMLDtls
 *
 * Description	: This Function will the Pass Through XML Tags and Responses
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int loadDHIPassThroughXMLDtls()
{
	int				rv						= SUCCESS;
	int				iCnt					= 1;
	int				kCnt					= 0;
	int				iIndex					= 0;
	int				iReqCnt					= 0;
	int				iReqTagCnt				= 0;
	int				iReqVarCnt				= 0;
	int				iResCnt					= 0;
	int				iLen					= 0;
	int				iPTIndex				= 0;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szPassThroughFile[256]	= "";
	char 			szPassThroughFile1[256]	= "";
	char 			szPassThroughFile2[256]	= "";
	char			szAppLogData[300]		= "";
	char			szAppLogDiag[300]		= "";
	char			szKey[256]				= "";
	char			*pszVal					= NULL;
	char			*pszTemp				= NULL;
	char			*cPtr					= NULL;
	char			*cTemp					= NULL;
	dictionary 		*dict					= NULL;
	KEYVAL_PTYPE	pstLocTagKeyVal;
	KEYVAL_PTYPE	pstLocVARKeyVal;
	KEYVAL_PTYPE	pstTempKeyVal;
	KEYVAL_STYPE	stKeyVal;

#ifdef DEBUG
	char	szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&gstDHIPTLst, 0x00, sizeof(PASSTHROUGH_STYPE));
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
	memset(szPassThroughFile, 0x00, sizeof(szPassThroughFile));


	pstLocTagKeyVal = (KEYVAL_PTYPE)malloc(sizeof(KEYVAL_STYPE));
	pstLocVARKeyVal = (KEYVAL_PTYPE)malloc(sizeof(KEYVAL_STYPE));

	while(1)
	{
		/* Check if file exist */
		if(strcmp(dhiSettings.szDHIProcessor, "rc") == 0)
		{
			strcpy(szPassThroughFile1,"./flash/rchiPassThrgFields.INI");
			strcpy(szPassThroughFile2,"./flash/rchiPassThrgFields.ini");
		}
		else if(strcmp(dhiSettings.szDHIProcessor, "elvn") == 0)
		{
			strcpy(szPassThroughFile1,"./flash/ehiPassThrgFields.INI");
			strcpy(szPassThroughFile2,"./flash/ehiPassThrgFields.ini");
		}

		rv = doesFileExist(szPassThroughFile1);
		if(rv == SUCCESS)
		{
			strcpy(szPassThroughFile,szPassThroughFile1);
			debug_sprintf(szDbgMsg, "%s: Loaded [%s] File", __FUNCTION__, szPassThroughFile);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			rv = doesFileExist(szPassThroughFile2);
			if(rv == SUCCESS)
			{
				strcpy(szPassThroughFile,szPassThroughFile2);
				debug_sprintf(szDbgMsg, "%s: Loaded [%s] File", __FUNCTION__, szPassThroughFile);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: [%s] does Not Exist", __FUNCTION__, szPassThroughFile2);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "[%s] File Does Not Exist.", szPassThroughFile2);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
				}
				rv = SUCCESS;
				break;
			}
		}

		/* Load the ini file in the parser library */
		dict = iniparser_load(szPassThroughFile);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to parse ini file [%s], errno [%d]",__FUNCTION__, szPassThroughFile, errno);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Failed To Load [%s] Pass Through INI File, Error No[%d].", szPassThroughFile, errno);
				sprintf(szAppLogDiag, "Please Check The Format Of [%s] Pass Through INI File.", szPassThroughFile);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Loaded [%s] successfully",__FUNCTION__, szPassThroughFile);
			APP_TRACE(szDbgMsg);
		}

		/* Get the total number of tags present in REQUEST section */
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "SSIREQUEST:count");
		pszVal = iniparser_getstring(dict, szKey, NULL);
		if(pszVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Count value is not present for SSIREQUEST Section",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iReqCnt = 0;
			//rv = FAILURE;
			//break;
		}
		else
		{
			iReqCnt = atoi(pszVal);
		}

		/* Get the total number of tags present in RESPONSE section */
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "SSIRESPONSE:count");
		pszVal = iniparser_getstring(dict, szKey, NULL);
		if(pszVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Count value is not present for SSIRESPONSE Section",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iResCnt = 0;
		}
		else
		{
			iResCnt = atoi(pszVal);
		}

		iPTIndex = eELEMENT_INDEX_MAX;


		if(iReqCnt > 0)
		{
			iCnt = 1;
			while(iCnt <= iReqCnt)
			{
				memset(&stKeyVal, 0x00, sizeof(KEYVAL_STYPE));
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "SSIREQUEST:tag%d", iCnt);
				pszVal = iniparser_getstring(dict, szKey, NULL);
				if(pszVal == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: [%s]is not present",__FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s]",__FUNCTION__, szKey, pszVal);
				APP_TRACE(szDbgMsg);

				pszTemp =  strdup(pszVal);

				if((cPtr = strtok(pszTemp, ",")) == NULL)		//TAG_NAME
				{
					rv = FAILURE;
					break;
				}

				stKeyVal.key = strdup(cPtr);

				if((cPtr = strtok(NULL, ",")) == NULL)			//Value type is not present
				{
					/*Treating as SINGLETONE ELEMENT*/
					stKeyVal.valType = SINGLETON;

				}
				else
				{
					//value type is present

					if(atoi(cPtr) == VARLIST)
					{
						stKeyVal.valType = VARLIST;
						if((cPtr = strtok(NULL, ",")) == NULL)			//Value type is varlist
						{
							rv = FAILURE;
							break;
						}

						debug_sprintf(szDbgMsg, "%s: VarList Info Section [%s]",__FUNCTION__, cPtr);
						APP_TRACE(szDbgMsg);

						rv = loadPassThroughVarlist(dict, &stKeyVal, cPtr);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Failed to Load VarList Details for [%s]",__FUNCTION__, stKeyVal.key);
							APP_TRACE(szDbgMsg);

							if(iAppLogEnabled == 1)
							{
								sprintf(szAppLogData, "Failed To Parse VarList Info Details INI File for Key [%s].", cPtr);
								sprintf(szAppLogDiag, "Please Check The Format Of VarList Details in [%s]", szPassThroughFile);
								addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
							}
							break;
						}

						++iReqVarCnt;
						pstTempKeyVal = (KEYVAL_PTYPE)realloc(pstLocVARKeyVal, sizeof(KEYVAL_STYPE)*iReqVarCnt);
						if(pstTempKeyVal == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							return FAILURE;
						}

						pstLocVARKeyVal = pstTempKeyVal;

						memset(pstLocVARKeyVal + iReqVarCnt - 1, 0x00, sizeof(KEYVAL_STYPE)); // Memset the re-allocated Varlist Info, keeping the original already copied data as it is
						memcpy(pstLocVARKeyVal + iReqVarCnt - 1, &stKeyVal, sizeof(KEYVAL_STYPE));
					}
					else
					{
						stKeyVal.valType = SINGLETON;
					}
				}

				if(stKeyVal.valType == SINGLETON)
				{

					++iReqTagCnt;
					stKeyVal.index = ++iPTIndex;

					pstTempKeyVal = (KEYVAL_PTYPE)realloc(pstLocTagKeyVal, sizeof(KEYVAL_STYPE)*iReqTagCnt);
					if(pstTempKeyVal == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}

					memset(pstTempKeyVal + iReqTagCnt - 1, 0x00, sizeof(KEYVAL_STYPE)); // Memset the re-allocated Varlist Info, keeping the original already copied data as it is

					memcpy(pstTempKeyVal + iReqTagCnt - 1, &stKeyVal, sizeof(KEYVAL_STYPE));


					pstLocTagKeyVal = pstTempKeyVal;
				}
				iCnt++;
				free(pszTemp);
				pszTemp = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SSI REQUEST Section is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(rv != SUCCESS)
		{
			// INI file is not in correct format
			debug_sprintf(szDbgMsg, "%s: SSI Request Section is not in correct format",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed To Parse SSI Pass Through Request Fields.");
				sprintf(szAppLogDiag, "Please Check The Format Of SSIREQUEST section in [%s]", szPassThroughFile);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}
			break;
		}


		if(iResCnt > 0)
		{
			iIndex = 0;
			iCnt = 1;

			gstDHIPTLst.iRespXMLTagsCnt = iResCnt;
			/* Allocate memory for request tag list*/
			gstDHIPTLst.pstPTXMLRespDtls = (KEYVAL_PTYPE) malloc(iResCnt * sizeof(KEYVAL_STYPE));
			if(gstDHIPTLst.pstPTXMLRespDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory Allocation Failed",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			// Initialize the memory
			memset(gstDHIPTLst.pstPTXMLRespDtls, 0x00, iResCnt * sizeof(KEYVAL_STYPE));

			while(iCnt <= iResCnt)
			{
				memset(&stKeyVal, 0x00, sizeof(KEYVAL_STYPE));
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "SSIRESPONSE:tag%d", iCnt);
				pszVal = iniparser_getstring(dict, szKey, NULL);
				if(pszVal == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: No Value Found for Key[%s]",__FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s]",__FUNCTION__, szKey, pszVal);
				APP_TRACE(szDbgMsg);

				iLen = strlen(pszVal);
				cTemp = (char*) malloc(iLen + 1);
				if(cTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation failed",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				memset(cTemp, 0x00, iLen + 1);
				strcpy(cTemp, pszVal);

				stKeyVal.key = cTemp;
				stKeyVal.index = ++iPTIndex;

				debug_sprintf(szDbgMsg, "%s: iPTIndex [%d]",__FUNCTION__, iPTIndex);
				APP_TRACE(szDbgMsg);

				memcpy(gstDHIPTLst.pstPTXMLRespDtls + iIndex, &stKeyVal, sizeof(KEYVAL_STYPE));

				iCnt++;
				iIndex++;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SSIRESPONSE Section is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(rv != SUCCESS)
		{
			// INI file is not in correct format
			debug_sprintf(szDbgMsg, "%s: DHI Response Section is not in correct format",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed To Parse DHI Pass Through Response Fields.");
				sprintf(szAppLogDiag, "Please Check The Format Of SSIRESPONSE section in [%s]", szPassThroughFile);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}
			break;
		}


		/*COPY Request tag Key list and var key list to global structures*/

		gstDHIPTLst.iReqXMLTagsCnt = iReqTagCnt;

		if(pstLocTagKeyVal != NULL)
		{
			gstDHIPTLst.pstPTXMLReqDtls = pstLocTagKeyVal;
		}

		gstPTVarLst.iReqVARLSTCnt = iReqVarCnt;

		if(pstLocVARKeyVal != NULL)
		{
			gstPTVarLst.pstVarlistReqDtls = pstLocVARKeyVal;

			if(iReqVarCnt > 0)
			{
				for(kCnt = 0; kCnt < iReqVarCnt; kCnt++)
				{
					debug_sprintf(szDbgMsg, "%s: Key VALUE [%s]",__FUNCTION__, gstPTVarLst.pstVarlistReqDtls[kCnt].key);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Successfully Loaded the [%s].", szPassThroughFile);
			addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSED, szAppLogData, NULL);
		}
		break;
	}

	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadPassThroughVarlist
 *
 * Description	: This function gets the details of Var List type in PassThrough File
 *
 * Input Params	: none
 *
 * Output Params: int rv SUCCESS/FAILURE
 * ============================================================================
 */
static int loadPassThroughVarlist(dictionary * dict, KEYVAL_PTYPE pstLocKeyVal, char * pszVarListName)
{
	int						rv 					= SUCCESS;
	int						iLen				= 0;
	int						iCnt				= 1;
	int						iListInfoCnt		= 0;
	int						iTagCnt				= 0;
	int						iValType			= -1;
	int						iAppLogEnabled		= isAppLogEnabled();
	char					szAppLogData[300]	= "";
	char					szAppLogDiag[300]	= "";
	char					szKey[256]			= "";
	char					*pszValTemp			= NULL;
	char					*pszTemp			= NULL;
	char					*pszVal				= NULL;
	char					*cPtr				= NULL;
	char					*cTemp				= NULL;
	DHI_VARLST_INFO_PTYPE	pstVarLstInfoLoc	= NULL;
	DHI_VARLST_INFO_PTYPE	pstVarLstInfoTemp	= NULL;
	DHI_VARLST_INFO_STYPE	stVarLstInfoLoc;
	DHI_VARLST_INFO_STYPE	stVarLstInfoSingleElem;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	/* Allocate memory for VARLIST INFO for Single Elements in the VARLIST request
	 * Even if Single Elements are not present in the VARLIST, the value will be by default set to NULL*/
	iListInfoCnt = 1;
	pstVarLstInfoLoc = (DHI_VARLST_INFO_PTYPE) malloc(sizeof(DHI_VARLST_INFO_STYPE));
	if(pstVarLstInfoLoc == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pstVarLstInfoLoc, 0x00, sizeof(DHI_VARLST_INFO_STYPE)); // Memset the allocated Varlist Info,
	memset(&stVarLstInfoSingleElem, 0x00, sizeof(DHI_VARLST_INFO_STYPE));


	/* Get the total number of tags present in REQUEST section */
	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s:count", pszVarListName);
	pszVal = iniparser_getstring(dict, szKey, NULL);
	if(pszVal == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Count value is not present",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}
	else
	{
		iTagCnt = atoi(pszVal);
	}

	while(iCnt <= iTagCnt)
	{
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:tag%d", pszVarListName, iCnt);
		pszVal = iniparser_getstring(dict, szKey, NULL);
		if(pszVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Value for Key[%s] ", __FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s]",__FUNCTION__, szKey, pszVal);
		APP_TRACE(szDbgMsg);

		pszValTemp	= strdup(pszVal);
		pszTemp 	=  strdup(pszVal);
		rv = getValueTypeofVarListElement(pszValTemp, &iValType);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the Value Type of Varlist Element. Please Check the VarList Passthrough Format",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(pstVarLstInfoLoc != NULL)
			{
				free(pstVarLstInfoLoc);
			}

			break;
		}
		free(pszValTemp);
		pszValTemp = NULL;

		memset(&stVarLstInfoLoc, 0x00, sizeof(DHI_VARLST_INFO_STYPE));

		if(iValType == VARLIST)
		{
			iListInfoCnt++;

			debug_sprintf(szDbgMsg, "%s: VARLIST Noticed inside VARLIST",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstVarLstInfoTemp = (DHI_VARLST_INFO_PTYPE) realloc(pstVarLstInfoLoc, sizeof(DHI_VARLST_INFO_STYPE) * iListInfoCnt);
			if(pstVarLstInfoTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				return FAILURE;
			}
			memset(pstVarLstInfoTemp + iListInfoCnt - 1, 0x00, sizeof(DHI_VARLST_INFO_STYPE)); // Memset the re-allocated Varlist Info, keeping the original already copied data as it is
			pstVarLstInfoLoc = pstVarLstInfoTemp;

			if((cPtr = strtok(pszTemp, ",")) == NULL)		//VarList TAG_NAME
			{
				rv = FAILURE;
				break;
			}
			iLen = strlen(cPtr);

			debug_sprintf(szDbgMsg, "%s: VARList Node Name [%s]",__FUNCTION__, cPtr);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: iLen [%d]",__FUNCTION__, iLen);
			APP_TRACE(szDbgMsg);

			cTemp = (char*) malloc(iLen + 1);
			if(cTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(cTemp, 0x00, iLen + 1);
			strcpy(cTemp, cPtr);

			stVarLstInfoLoc.nodeType = iListInfoCnt - 1;
			stVarLstInfoLoc.szNodeStr = cTemp;

			debug_sprintf(szDbgMsg, "%s: pszVal [%s]",__FUNCTION__, pszVal);
			APP_TRACE(szDbgMsg);


			if((cPtr = strtok(NULL, ",")) == NULL)		//Value type
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Value type [%s]",__FUNCTION__, cPtr);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			if((cPtr = strtok(NULL, ",")) == NULL)		//KeyList Section Name
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get keylist section name [%s]",__FUNCTION__, cPtr);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			debug_sprintf(szDbgMsg, "%s: VarLst Info keyList Name [%s]",__FUNCTION__, cPtr);
			APP_TRACE(szDbgMsg);

			rv = loadKeyListDetails(dict, &stVarLstInfoLoc, cPtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Load KeyList Details for Node [%s]",__FUNCTION__, stVarLstInfoLoc.szNodeStr);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Failed To Parse KeyValue List Details for VarList [%s] in INI File.", stVarLstInfoLoc.szNodeStr);
					strcpy(szAppLogDiag, "Please Check The Format Of VarList Details.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}

				break;
			}

			// copy the details into memory for this XML tag
			memcpy((pstVarLstInfoLoc + iListInfoCnt - 1), &stVarLstInfoLoc, sizeof(DHI_VARLST_INFO_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Single Elements in VARLIST Noticed. We will gather all the Signgle Elements in this VARLISt and load the KeyList at the End",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			stVarLstInfoSingleElem.nodeType = 0;
			stVarLstInfoSingleElem.szNodeStr = NULL;

			rv = addVarListSingleElements(pszTemp, &stVarLstInfoSingleElem);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to add KeyList Details for Single Elements in VARLIST [%s]",__FUNCTION__, pszVarListName);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Failed To Parse KeyValue List Details for Single Elements in VarList [%s]", pszVarListName);
					strcpy(szAppLogDiag, "Please Check The Format Of VarList Details.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				break;
			}
		}

		iCnt++;
		free(pszTemp);
		pszTemp = NULL;
	}

	debug_sprintf(szDbgMsg, "%s:iCnt [%d]",__FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);


	if(stVarLstInfoSingleElem.keyList != NULL)
	{
		// copy the Single Elements VarList info details into memory. This will be copied into the first index of the VarList Info.
		memcpy(pstVarLstInfoLoc, &stVarLstInfoSingleElem, sizeof(DHI_VARLST_INFO_STYPE));
	}

	//After loading the VarList Info of the passthrough Request, Terminate the Info details with NULL and -1.
	iListInfoCnt++;
	memset(&stVarLstInfoLoc, 0x00, sizeof(DHI_VARLST_INFO_STYPE));
	stVarLstInfoLoc.nodeType = -1;
	stVarLstInfoLoc.iElemCnt = 0;
	stVarLstInfoLoc.szNodeStr = NULL;
	stVarLstInfoLoc.keyList = NULL;
	pstVarLstInfoTemp = (DHI_VARLST_INFO_PTYPE) realloc(pstVarLstInfoLoc, sizeof(DHI_VARLST_INFO_STYPE) * iListInfoCnt);
	if(pstVarLstInfoTemp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pstVarLstInfoTemp + iListInfoCnt - 1, 0x00, sizeof(DHI_VARLST_INFO_STYPE)); // Memset the re-allocated Varlist Info, keeping the original already copied data as it is
	memcpy((pstVarLstInfoTemp + iListInfoCnt - 1), &stVarLstInfoLoc, sizeof(DHI_VARLST_INFO_STYPE));
	pstVarLstInfoLoc = pstVarLstInfoTemp;

	//Finally Assign the value meta Info to the original Passthrough request.
	pstLocKeyVal->valMetaInfo = pstVarLstInfoLoc;

	debug_sprintf(szDbgMsg, "%s: ListInfoCnt [%d]", __FUNCTION__, iListInfoCnt);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getValueTypeofVarListElement
 *
 * Description	: This function will determine what is the value type for the tag inside the VARLIST Passthrough Element.
 * There is a possibility that the Element can be VARLIST or SINGLETON, NUMERIC or STRING
 *
 * Input Params	: Passthrough tag string and Value Type pointer
 *
 * Output Params: SUCCESS/FAILUE. The Value Type of the tag is filled.
 * ============================================================================
 */
static int getValueTypeofVarListElement(char * pszVal, int *piValType)
{
	int		rv 		= SUCCESS;
	char *	cPtr	= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	while(1)
	{
		if(pszVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input string is NULL.",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}


		if((cPtr = strtok(pszVal, ",")) == NULL)		//VarList TAG_NAME
		{
			rv = FAILURE;
			break;
		}

		if((cPtr = strtok(NULL, ",")) == NULL)			//Value type
		{
			//rv = FAILURE;
			*piValType = SINGLETON;
			break;
		}

		*piValType = atoi(cPtr);
		/*PRADEEP : IN CASE If The Value Type is other than 1 and 10 we will stop the application*/
		if(!(*piValType == VARLIST || *piValType ==  SINGLETON))
		{
			rv = FAILURE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Value Type Returned [%d]", __FUNCTION__, *piValType);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: loadKeyListDetails
 *
 * Description	: This function gets the details of Var List type in PassThrough File
 *
 * Input Params	: dictionary*, DHI_VARLST_INFO_PTYPE, char*
 *
 * Output Params:  SUCCESS/FAILURE
 * ============================================================================
 */
static int loadKeyListDetails(dictionary * dict, DHI_VARLST_INFO_PTYPE pstVarListInfo, char * pszKeyListName)
{
	int					rv 					= SUCCESS;
	int					iLen				= 0;
	int					iCnt				= 1;
	int					iKeyListCnt			= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	char				szAppLogDiag[300]	= "";
	char				szKey[256]			= "";
	char				*pszVal				= NULL;
	char				*pszTemp			= NULL;
	char				*cPtr				= NULL;
	char				*cTemp				= NULL;
	KEYVAL_STYPE		stLocKeyVal;


#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	/* Get the total number of tags present in keyList Details section */

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s:count", pszKeyListName);
	pszVal = iniparser_getstring(dict, szKey, NULL);
	if(pszVal == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Count value is not present",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}
	else
	{
		iKeyListCnt = atoi(pszVal);
	}

	pstVarListInfo->iElemCnt = iKeyListCnt;
	pstVarListInfo->keyList = (KEYVAL_PTYPE) malloc(sizeof(KEYVAL_STYPE) * iKeyListCnt);
	if(pstVarListInfo->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;

		return rv;
	}
	memset(pstVarListInfo->keyList, 0x00, sizeof(sizeof(KEYVAL_STYPE) * iKeyListCnt));

	while(iCnt <= iKeyListCnt)
	{
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:tag%d", pszKeyListName, iCnt);
		pszVal = iniparser_getstring(dict, szKey, NULL);
		if(pszVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Value Found for Key[%s]", __FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s] iKeyListCnt [%d]",__FUNCTION__, szKey, pszVal, iKeyListCnt);
		APP_TRACE(szDbgMsg);

		pszTemp = strdup(pszVal);
		memset(&stLocKeyVal, 0x00, sizeof(KEYVAL_STYPE));

		// parse the tag related details like name,value type etc & store them in memory
		// Format for value part of key tag<n> is:
		// <TAG_NAME>,<VALUE_TYPE>,<VARLIST>

		if((cPtr = strtok(pszTemp, ",")) == NULL)		//TAG_NAME
		{
			rv = FAILURE;
			break;
		}
		iLen = strlen(cPtr);
		cTemp = (char*) malloc(iLen + 1);
		if(cTemp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(cTemp, 0x00, iLen + 1);
		strcpy(cTemp, cPtr);
		stLocKeyVal.key = cTemp;

		if((cPtr = strtok(NULL, ",")) == NULL)		//VALUE_TYPE
		{
			rv = FAILURE;
			break;
		}
		stLocKeyVal.valType = atoi(cPtr);


		if(stLocKeyVal.valType == VARLIST)
		{
			if((cPtr = strtok(NULL, ",")) == NULL)		//Var List Info Details
			{
				rv = FAILURE;
				break;
			}

			debug_sprintf(szDbgMsg, "%s: VarList Info Section [%s]",__FUNCTION__, cPtr);
			APP_TRACE(szDbgMsg);

			rv = loadPassThroughVarlist(dict, &stLocKeyVal, cPtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Load VarList Details for [%s]",__FUNCTION__, stLocKeyVal.key);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Failed To Parse VarList Info Details INI File for Key [%s].", cPtr);
					strcpy(szAppLogDiag, "Please Check The Format Of VarList Details.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}

				break;
			}
		}
		else if(stLocKeyVal.valType == SINGLETON)
		{
			stLocKeyVal.valMetaInfo = NULL;
		}

		// copy the details into memory for this XML tag
		memcpy(pstVarListInfo->keyList + iCnt - 1, &stLocKeyVal, sizeof(KEYVAL_STYPE));
		iCnt++;
		free(pszTemp);
		pszTemp = NULL;
	}

	/*for(iCnt = 0; iCnt < iKeyListCnt; iCnt++)
	{
		pstLocStrDef = (STR_DEF_PTYPE)(pstVarListInfo->keyList + iCnt)->valMetaInfo;
		debug_sprintf(szDbgMsg, "%s: key[%s] valType[%d] mandFlag[%d]", __FUNCTION__,
										(pstVarListInfo->keyList+iCnt)->key,
										(pstVarListInfo->keyList+iCnt)->valType,
										(pstVarListInfo->keyList+iCnt)->isMand
										);
		APP_TRACE(szDbgMsg);
		if(pstLocStrDef != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: minLen[%d] maxLen[%d]", __FUNCTION__,
														pstLocStrDef->minLen,
														pstLocStrDef->maxLen);
			APP_TRACE(szDbgMsg);
		}

	}*/

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: addVarListSingleElements
 *
 * Description	: This function adds the single Elements of VarList PassThrough request and updates the keylist of Single Element KeyList structure in VARLIST
 *
 * Input Params	: char*, DHI_VARLST_INFO_PTYPE
 *
 * Output Params: int rv SUCCESS/FAILURE
 * ============================================================================
 */
static int addVarListSingleElements(char * pszVal, DHI_VARLST_INFO_PTYPE pstVarListSingleElem)
{
	int					rv 					= SUCCESS;
	int					iLen				= 0;
	int					iCnt				= 1;
	char				*cPtr				= NULL;
	char				*cTemp				= NULL;
	KEYVAL_PTYPE		pstLocKeyVal;
	KEYVAL_STYPE		stLocKeyVal;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	pstVarListSingleElem->iElemCnt++;
	iCnt = pstVarListSingleElem->iElemCnt;
	pstLocKeyVal = (KEYVAL_PTYPE) realloc(pstVarListSingleElem->keyList, sizeof(KEYVAL_STYPE) * iCnt);
	if(pstLocKeyVal == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;

		return rv;
	}
	pstVarListSingleElem->keyList = pstLocKeyVal;
	memset(pstVarListSingleElem->keyList + iCnt -1, 0x00, sizeof(KEYVAL_STYPE));

	while(1)
	{
		debug_sprintf(szDbgMsg, "%s: Value[%s]",__FUNCTION__, pszVal);
		APP_TRACE(szDbgMsg);

		memset(&stLocKeyVal, 0x00, sizeof(KEYVAL_STYPE));

		// parse the tag related details like name,value type etc & store them in memory
		// Format for value part of key tag<n> is:
		// <TAG_NAME>,<VALUE_TYPE>

		if((cPtr = strtok(pszVal, ",")) == NULL)		//TAG_NAME
		{
			rv = FAILURE;
			break;
		}
		iLen = strlen(cPtr);
		cTemp = (char*) malloc(iLen + 1);
		if(cTemp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(cTemp, 0x00, iLen + 1);
		strcpy(cTemp, cPtr);
		stLocKeyVal.key = cTemp;


		if((cPtr = strtok(NULL, ",")) == NULL)		//VALUE_TYPE
		{
			rv = FAILURE;
			break;
		}

		if(atoi(cPtr) == SINGLETON)
		{
			stLocKeyVal.valType = SINGLETON;
			stLocKeyVal.valMetaInfo = NULL;
			// copy the details into memory for this XML tag
			memcpy(pstVarListSingleElem->keyList + iCnt - 1, &stLocKeyVal, sizeof(KEYVAL_STYPE));
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDHIPassThroughDtls
 *
 * Description	: This Function will return the address of the global DHI Pass through Details
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
PASSTHROUGH_PTYPE getDHIPassThroughDtls()
{
	return &gstDHIPTLst;
}

/*
 * ============================================================================
 * Function Name: getDHIVarLstDtls
 *
 * Description	: This Function will return the address of the global DHI Varlist Details
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
PTVARLST_PTYPE getDHIVarLstDtls()
{
	return &gstPTVarLst;
}


