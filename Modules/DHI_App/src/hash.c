/******************************************************************
*                      utils.c                                    *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis for hash table creation          *
*              and hash look up                                   *
* 			                                                      *
*                                                                 *
* Created on: Dec 5, 2014                                         *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "DHI_App/hash.h"
#include "DHI_App/common.h"
#include "DHI_App/ssiDataFields.inc"
#include "DHI_App/tranDef.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/externfunc.h"
//#include "ssiDataFields.inc"


static HASHLST_PTYPE hashtab[HASHSIZE];  // pointer table

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)


/*
 * ============================================================================
 * Function Name: createHashTableForSSIFields
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.ur1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	createHashTableForSSIFields()
{
	int					rv					= SUCCESS;
	int					iTotalCnt			= 0;
	int					iCount				= 0;
	int					iAppLogEnabled		= 0;
	char				szAppLogData[256]	= "";
	char				szAppLogDiag[256]	= "";
	HASHLST_PTYPE 		pstHashLst			= NULL;
	PASSTHROUGH_PTYPE	pstDHIPTLst			= NULL;

#ifdef DEBUG
	char				szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		iTotalCnt = sizeof(genReqLst) / KEYVAL_SIZE;

		debug_sprintf(szDbgMsg, "%s: Total Number of Fields to be added to HashTable is %d", __FUNCTION__, iTotalCnt);
		APP_TRACE(szDbgMsg);

		/*
		 * Adding SSI fields to the hash table
		 * Each tag name in the SSI request will be stored as KEY
		 * and its Index in the data strutcure will be stored as VALUE
		 */
		for(iCount = 0; iCount < iTotalCnt; iCount++)
		{
			//debug_sprintf(szDbgMsg,"%s: Adding [%s] key with [%d] Value to the Hash Table",__FUNCTION__, genReqLst[iCount].key, (int)genReqLst[iCount].value);
			//APP_TRACE(szDbgMsg);

			pstHashLst = addEntryToHashTable(genReqLst[iCount].key, (int)genReqLst[iCount].index);
			if(pstHashLst != NULL)
			{
//				debug_sprintf(szDbgMsg,"%s: Added [%s] key with [%d] Value to the Hash Table",__FUNCTION__, pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
//				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Adding [%s] key [%d] Value to the Hash Table", pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}

				//TO_DO: What to do here!!!
			}
			pstHashLst = NULL; //Derefence it before assigning
		}

		pstDHIPTLst = getDHIPassThroughDtls();

		debug_sprintf(szDbgMsg,"%s: DHI Pass Through Request Count [%d]",__FUNCTION__, pstDHIPTLst->iReqXMLTagsCnt);
		APP_TRACE(szDbgMsg);

		for(iCount = 0; iCount < pstDHIPTLst->iReqXMLTagsCnt ; iCount++)
		{
			//debug_sprintf(szDbgMsg,"%s: iCount [%d], Adding [%s] key with [%d] Value to the Hash Table",__FUNCTION__, iCount, pstDHIPTLst->pstPTXMLReqDtls[iCount].key, pstDHIPTLst->pstPTXMLReqDtls[iCount].value);
			//APP_TRACE(szDbgMsg);

			pstHashLst = addEntryToHashTable(pstDHIPTLst->pstPTXMLReqDtls[iCount].key,  pstDHIPTLst->pstPTXMLReqDtls[iCount].index);
			if(pstHashLst != NULL)
			{
				//debug_sprintf(szDbgMsg,"%s: Added [%s] key with [%d] Value to the Hash Table",__FUNCTION__, pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
				//APP_TRACE(szDbgMsg);

				 pstDHIPTLst->pstPTXMLReqDtls[iCount].index = pstHashLst->iTagIndexVal;
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Adding [%s] key [%d] Value to the Hash Table", pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}

				//TO_DO: What to do here!!!
			}
			pstHashLst = NULL; //Derefence it before assigning
		}

		debug_sprintf(szDbgMsg,"%s: DHI Pass Through Response Count [%d]",__FUNCTION__, pstDHIPTLst->iRespXMLTagsCnt);
		APP_TRACE(szDbgMsg);

		for(iCount = 0; iCount < pstDHIPTLst->iRespXMLTagsCnt ; iCount++)
		{
			//debug_sprintf(szDbgMsg,"%s: iCount [%d], Adding [%s] key with [%d] Value to the Hash Table",__FUNCTION__, iCount, pstDHIPTLst->pstPTXMLReqDtls[iCount].key, pstDHIPTLst->pstPTXMLReqDtls[iCount].value);
			//APP_TRACE(szDbgMsg);

			pstHashLst = addEntryToHashTable(pstDHIPTLst->pstPTXMLRespDtls[iCount].key,  pstDHIPTLst->pstPTXMLRespDtls[iCount].index);
			if(pstHashLst != NULL)
			{
				//debug_sprintf(szDbgMsg,"%s: Added [%s] key with [%d] Value to the Hash Table",__FUNCTION__, pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
				//APP_TRACE(szDbgMsg);

				 pstDHIPTLst->pstPTXMLRespDtls[iCount].index = pstHashLst->iTagIndexVal;
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Adding [%s] key [%d] Value to the Hash Table", pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}

				//TO_DO: What to do here!!!
			}
			pstHashLst = NULL; //Derefence it before assigning
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: generateHashVal
 *
 * Description	: This function is used to calculate the hash value for the given
 * 				  input string
 * 				  This adds each character value in the string to a scrambled
 * 				  combination of the previous ones and returns the remainder
 * 				  modulo the array size. This is not the best possible hash function,
 * 				  but it is short and effective
 *
 * Input Params	: Input String to Calculate Hash
 *
 * Output Params: Hash Value
 * ============================================================================
 */
unsigned int generateHashVal(char *pszInput)
{
    unsigned int iHashVal = -1;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(pszInput == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Input to calculate hash!!! ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iHashVal;
	}

    for (iHashVal = 0; *pszInput != '\0'; pszInput++)
    {
    	iHashVal = *pszInput + (31 * iHashVal);
    }

    //debug_sprintf(szDbgMsg, "%s: iHashVal [%u] ", __FUNCTION__, iHashVal);
    //APP_TRACE(szDbgMsg);

    return iHashVal % HASHSIZE;
}

/*
 * ============================================================================
 * Function Name: lookupHashTable
 *
 * Description	: This function is used to lookup hash table for the given
 * 				  input string
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
HASHLST_PTYPE lookupHashTable(char *pszInPut)
{
	HASHLST_PTYPE pstHashLst;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

    for (pstHashLst = hashtab[generateHashVal(pszInPut)]; pstHashLst != NULL; pstHashLst = pstHashLst->next)
    {
        if (strcmp(pszInPut, pstHashLst->pszTagName) == 0)
        {
        	debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table", __FUNCTION__, pszInPut);
        	APP_TRACE(szDbgMsg);

            return pstHashLst;     // Found
        }
    }

    debug_sprintf(szDbgMsg, "%s: Did not find Mapping for %s in hash table", __FUNCTION__, pszInPut);
    APP_TRACE(szDbgMsg);

    return NULL;           // NOT found
}

/*
 * ============================================================================
 * Function Name: addEntryToHashTable
 *
 * Description	: This function is used to add entry to the hash table
 *
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
HASHLST_PTYPE addEntryToHashTable(char *pszTagName, int iTagIndexVal)
{
	HASHLST_PTYPE pstHashLst;
    unsigned int hashval;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    if ((pstHashLst = lookupHashTable(pszTagName)) == NULL)        // NOT found
    {
        pstHashLst = (HASHLST_PTYPE)malloc(sizeof(HASHLST_STYPE));

        if ((pstHashLst == NULL) || (pstHashLst->pszTagName = strdup(pszTagName)) == NULL)
        {
        	debug_sprintf(szDbgMsg, "%s: Error while creating memory for Hash list ", __FUNCTION__);
        	APP_TRACE(szDbgMsg);
            return NULL;
        }

//		debug_sprintf((szDbgMsg, "%s - TagName=[%s], allocated %d bytes, pointer=%p",
                        //__FUNCTION__, pszTagName, sizeof(HASHLST_STYPE), pstHashLst));
        //APP_TRACE(szDbgMsg);

        hashval = generateHashVal(pszTagName); //Generating the Hash Value

//		debug_sprintf(szDbgMsg, "%s: Generated Hash Value for  key [%s] is [%d], key Value is [%d] ", __FUNCTION__, pszTagName, hashval, iTagIndexVal);
//		APP_TRACE(szDbgMsg);

        pstHashLst->next = hashtab[hashval];

        hashtab[hashval] = pstHashLst;

    }
    else // Already there
    {
    	debug_sprintf(szDbgMsg, "%s: Already present, need not add it again ", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
    }

    pstHashLst->iTagIndexVal = iTagIndexVal;

    return pstHashLst;
}
