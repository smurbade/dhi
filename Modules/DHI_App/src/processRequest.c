/******************************************************************
*                      processRequest.c                           *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis related to processing of         *
*              request                                            *
* 			                                                      *
*                                                                 *
* Created on: Dec 10, 2014                                        *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/stat.h>
#include <string.h>

#include "DHI_App/common.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/hash.h"
#include "DHI_App/externfunc.h"
#include "DHI_App/dhiCfgDef.h"

const sFUNCTION_TYPE FUNCTION_TYPE = {
	"PAYMENT",
	"BATCH",
	"REPORT",
};

const sPAYMENT_TYPE PAYMENT_TYPE = {
	"CREDIT",
	"DEBIT",
	"GIFT",
	"PRIV_LBL",
	"CHECK",
	"EBT",
};

const sCOMMAND COMMAND = {
	"PRE_AUTH",
	"SALE",
	"CREDIT",
	"VOID",
	"POST_AUTH",
	"ADD_TIP",
	"COMPLETION",
	"ADD_VALUE",
	"REMOVE_VALUE",
	"ACTIVATE",
	"DEACTIVATE",
	"BALANCE",
	"SIGNATURE",
	"TOKEN_QUERY",
	"SETTLE",
	"DAYSUMMARY",
	"VERSION",
	"SETUP_REQUEST_V3",
	"TOR_REQ",
	"VOICE_AUTH",
	"CUTOVER",
	"SITETOTALS",
	"LAST_TRAN",
	"GIFT_CLOSE",
	"CARD_RATE"
};

static pthread_mutex_t gptLastTranFileMutex;

static int processVersionCommand(TRANDTLS_PTYPE );
static int processReportFuncType(TRANDTLS_PTYPE );
static int processBatchFuncType(TRANDTLS_PTYPE );
static int processPaymentFuncType(TRANDTLS_PTYPE );
static int processDevRegistrationCommand(TRANDTLS_PTYPE );
static int processSignatureCommand(TRANDTLS_PTYPE );
static int processLastTranCommand(TRANDTLS_PTYPE );

static int fillErrGenRespDtls(TRANDTLS_PTYPE , int );

static int readAndParseLastTranDataLine(FILE * , TRANDTLS_PTYPE );
static int getRecordIndex(int );

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: processSSIRequest
 *
 * Description	: This function process the SSI request
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processSSIRequest(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= 0;
	char		szAppLogData[256]	= "";
	char		szAppLogDiag[256]	= "";
	static int	iMutexInited		= 0;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	if(!iMutexInited) //TODO currently putting it here need to move to appropriate place
	{
		if(pthread_mutex_init(&gptLastTranFileMutex, NULL))//Initialize the Last Tran Details File Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Last Tran Details File mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		iMutexInited = 1;
	}

	while(1)
	{
		if(pstHTTPInfo == NULL || pstSSIReqResFields == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input Params NULL!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstSSIReqResFields[eCOMMAND].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not present in the request!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing %s Command Request from the Client", pstSSIReqResFields[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s: Command [%s]", __FUNCTION__, pstSSIReqResFields[eCOMMAND].elementValue);
		APP_TRACE(szDbgMsg);

		if( (pstSSIReqResFields[eCOMMAND].elementValue != NULL) &&
		    (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.VERSION)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Version command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processVersionCommand(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing Version command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Processing Version Command");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
			}
			break;
		}

		if( (pstSSIReqResFields[eCOMMAND].elementValue != NULL) &&
				    (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.DEVADMIN)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Device Registration command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processDevRegistrationCommand(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing Device Registration command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Processing Device Registration Command");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
			}
			break;
		}

		if( (pstSSIReqResFields[eFUNCTION_TYPE].elementValue != NULL) &&
		    (!strcasecmp(pstSSIReqResFields[eFUNCTION_TYPE].elementValue, FUNCTION_TYPE.PAYMENT)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Payment Function Type", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processPaymentFuncType(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing payment function type command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = fillErrGenRespDtls(pstSSIReqResFields, rv);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while filling General Error response details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

			break;
		}

		if( (pstSSIReqResFields[eFUNCTION_TYPE].elementValue != NULL) &&
		    (!strcasecmp(pstSSIReqResFields[eFUNCTION_TYPE].elementValue, FUNCTION_TYPE.BATCH)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Batch Function Type", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processBatchFuncType(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Batch Function Type command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = fillErrGenRespDtls(pstSSIReqResFields, rv);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while filling General Error response details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

			break;
		}

		if( (pstSSIReqResFields[eFUNCTION_TYPE].elementValue) &&
			(!strcasecmp(pstSSIReqResFields[eFUNCTION_TYPE].elementValue, FUNCTION_TYPE.REPORT)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Report Function Type", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processReportFuncType(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Processing Report Function Type command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = fillErrGenRespDtls(pstSSIReqResFields, rv);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while filling General Error response details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateLastTranDtls
 *
 * Description	: This function updates the Last Transaction Details
 * 				  data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int updateLastTranDtls(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int		rv					= SUCCESS;
	int		iBufLen				= 0;
	FILE	*Fh     	  		= NULL;
	char	szBuffer[1024]		= "";
	char	szAcctNum[30]		= "";
	char	szMaskAcctNum[30]	= "";

#ifdef DEBUG
	char			szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptLastTranFileMutex, "Last Tran File");

	Fh = fopen(LAST_TRAN_FILE_NAME, "w"); //Create an empty file for output operations. If a file with the same name already exists, its contents are discarded and the file is treated as a new empty file.
	if(Fh == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening %s file ", __FUNCTION__, LAST_TRAN_FILE_NAME);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptLastTranFileMutex, "Last Tran File");
		return FAILURE;
	}

	memset(szBuffer, 0x00, sizeof(szBuffer));

	/* Format of the LAST TRAN File
	 * <RESULT>|<RESULT_CODE>|<TERMINATION_STATUS>|<RESPONSE_TEXT>|<HOST_RESPCODE>|<CLIENTID>|<TROUTD>|<CTROUTD>|<INTRN_SEQ_NUM>|<COMMAND>|<ACCT_NUM>|
	 * <CARDHOLDER>|<TRANS_AMOUNT>|<APPROVED_AMOUNT>|<TRANS_DATE>|<TRANS_TIME>|<INVOICE>|<PAYMENT_TYPE>|<PAYMENT_MEDIA>|<CARD_TOKEN>
	 * ;
	 */

	if(pstSSIReqResFields[eRESULT].elementValue != NULL) //RESULT field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eRESULT].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eRESULT_CODE].elementValue != NULL) //RESULT_CODE field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eRESULT_CODE].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eTERMINATION_STATUS].elementValue != NULL) //TERMINATION_STATUS field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eTERMINATION_STATUS].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eRESPONSE_TEXT].elementValue != NULL) //RESPONSE_TEXT field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eRESPONSE_TEXT].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eHOST_RESPCODE].elementValue != NULL) //HOST_RESPCODE field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eHOST_RESPCODE].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eCLIENT_ID].elementValue != NULL) //CLIENT_ID field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eCLIENT_ID].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eTROUTD].elementValue != NULL) //TROUTD field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eTROUTD].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eCTROUTD].elementValue != NULL) //CTROUTD field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eCTROUTD].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue != NULL) //INTRN_SEQ_NUM field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eCOMMAND].elementValue != NULL) //COMMAND field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eCOMMAND].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eACCT_NUM].elementValue != NULL) //ACCT_NUM field is present
	{
		getMaskedPAN(pstSSIReqResFields[eACCT_NUM].elementValue, szMaskAcctNum); //Need to mask the account number before storing
		strcat(szBuffer, szMaskAcctNum);
	}
	else if(pstSSIReqResFields[eTRACK_DATA].elementValue != NULL) //TRACK_DATA field is present
	{
		getPANFromTrackData(pstSSIReqResFields[eTRACK_DATA].elementValue, atoi(pstSSIReqResFields[eTRACK_INDICATOR].elementValue), szAcctNum);
		getMaskedPAN(szAcctNum, szMaskAcctNum); //Need to mask the account number before storing
		strcat(szBuffer, szMaskAcctNum);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eCARDHOLDER].elementValue != NULL) //CARDHOLDER field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eCARDHOLDER].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eTRANS_AMOUNT].elementValue != NULL) //TRANS_AMOUNT field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eTRANS_AMOUNT].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eAPPROVED_AMOUNT].elementValue != NULL) //APPROVED_AMOUNT field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eAPPROVED_AMOUNT].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eTRANS_DATE].elementValue != NULL) //TRANS_DATE field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eTRANS_DATE].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eTRANS_TIME].elementValue != NULL) //TRANS_TIME field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eTRANS_TIME].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eINVOICE].elementValue != NULL) //INVOICE field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eINVOICE].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[ePAYMENT_TYPE].elementValue != NULL) //PAYMENT_TYPE field is present
	{
		strcat(szBuffer, pstSSIReqResFields[ePAYMENT_TYPE].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[ePAYMENT_MEDIA].elementValue != NULL) //PAYMENT_MEDIA field is present
	{
		strcat(szBuffer, pstSSIReqResFields[ePAYMENT_MEDIA].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eCARD_TOKEN].elementValue != NULL) //CARD_TOKEN field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eCARD_TOKEN].elementValue);
	}
	strcat(szBuffer, "\n");

	if(pstSSIReqResFields[eAUTH_CODE].elementValue != NULL) //AUTH_CODE field is present
	{
		strcat(szBuffer, pstSSIReqResFields[eAUTH_CODE].elementValue);
	}
	strcat(szBuffer, "\n");

	iBufLen = strlen(szBuffer);

	debug_sprintf(szDbgMsg, "%s -Updated Last Tran data [%s] and its length is %d", __FUNCTION__, szBuffer, iBufLen);
	APP_TRACE(szDbgMsg);

	rv = fwrite(szBuffer, sizeof(char), iBufLen, Fh);

	if(rv == iBufLen)
	{
		debug_sprintf(szDbgMsg, "%s- Successfully updated the Last Tran Data file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = SUCCESS;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s- Error while updating Last Tran data file, Number of bytes written(%d), Actual count (%d)", __FUNCTION__, rv, iBufLen);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	fclose(Fh);

	releaseMutexLock(&gptLastTranFileMutex, "Last Tran File");

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processDevRegistrationCommand
 *
 * Description	: This function process the Version Command and fill the corresponding
 * 				  data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int processDevRegistrationCommand(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int		rv					= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = processDeviceRegistration(pstSSIReqResFields);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while processing Device Registration Command!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processVersionCommand
 *
 * Description	: This function process the Version Command and fill the corresponding
 * 				  data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int processVersionCommand(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szTemp[300]			= "";
	char	szHIName[50]		= "";
	char	szHILibVer[50]		= "";
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	char	*pszTempBuf			= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		/* Preparing Termination Status */
		memset(szTemp, 0x00, sizeof(szTemp));
		strcpy(szTemp, "SUCCESS");

		if(pstSSIReqResFields[eTERMINATION_STATUS].elementValue != NULL)
		{
			free(pstSSIReqResFields[eTERMINATION_STATUS].elementValue);
			pstSSIReqResFields[eTERMINATION_STATUS].elementValue = NULL;
		}

		pszTempBuf = (char *)malloc((strlen(szTemp) + 1)* sizeof(char));
		if(pszTempBuf == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszTempBuf, 0x00, (strlen(szTemp) + 1)* sizeof(char));
		strncpy(pszTempBuf, szTemp, strlen(szTemp));
		pstSSIReqResFields[eTERMINATION_STATUS].elementValue = pszTempBuf;
		pszTempBuf = NULL;

		/* Preparing RESULT */
		memset(szTemp, 0x00, sizeof(szTemp));
		strcpy(szTemp, "OK");

		if(pstSSIReqResFields[eRESULT].elementValue != NULL)
		{
			free(pstSSIReqResFields[eRESULT].elementValue);
			pstSSIReqResFields[eRESULT].elementValue = NULL;
		}

		pszTempBuf = (char *)malloc((strlen(szTemp) + 1)* sizeof(char));
		if(pszTempBuf == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Result!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszTempBuf, 0x00, (strlen(szTemp) + 1)* sizeof(char));
		strncpy(pszTempBuf, szTemp, strlen(szTemp));
		pstSSIReqResFields[eRESULT].elementValue = pszTempBuf;
		pszTempBuf = NULL;

		/* Preparing Result Code */
		memset(szTemp, 0x00, sizeof(szTemp));
		strcpy(szTemp, "4");

		if(pstSSIReqResFields[eRESULT_CODE].elementValue != NULL)
		{
			free(pstSSIReqResFields[eRESULT_CODE].elementValue);
			pstSSIReqResFields[eRESULT_CODE].elementValue = NULL;
		}

		pszTempBuf = (char *)malloc((strlen(szTemp) + 1)* sizeof(char));
		if(pszTempBuf == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszTempBuf, 0x00, (strlen(szTemp) + 1)* sizeof(char));
		strncpy(pszTempBuf, szTemp, strlen(szTemp));
		pstSSIReqResFields[eRESULT_CODE].elementValue = pszTempBuf;
		pszTempBuf = NULL;

		/* Preparing Response Text */
		memset(szTemp, 0x00, sizeof(szTemp));
		strcpy(szTemp, "VERSION INFO CAPTURED");

		if(pstSSIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
		{
			free(pstSSIReqResFields[eRESPONSE_TEXT].elementValue);
			pstSSIReqResFields[eRESPONSE_TEXT].elementValue = NULL;
		}

		pszTempBuf = (char *)malloc((strlen(szTemp) + 1)* sizeof(char));
		if(pszTempBuf == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszTempBuf, 0x00, (strlen(szTemp) + 1)* sizeof(char));
		strncpy(pszTempBuf, szTemp, strlen(szTemp));
		pstSSIReqResFields[eRESPONSE_TEXT].elementValue = pszTempBuf;
		pszTempBuf = NULL;

		memset(szHILibVer, 0x00, sizeof(szHILibVer));
		memset(szHIName, 0x00, sizeof(szHIName));

		getHILibDtls(szHILibVer, szHIName);

		/* Preparing Version Info field */
		memset(szTemp, 0x00, sizeof(szTemp));
		sprintf(szTemp, "%s|MID=%s|TID=%s|%s", szHILibVer,dhiSettings.szMID,dhiSettings.szTID, szHIName);

		if(pstSSIReqResFields[eVERSION_INFO].elementValue != NULL)
		{
			free(pstSSIReqResFields[eVERSION_INFO].elementValue);
			pstSSIReqResFields[eVERSION_INFO].elementValue = NULL;
		}

		pszTempBuf = (char *)malloc((strlen(szTemp) + 1)* sizeof(char));
		if(pszTempBuf == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszTempBuf, 0x00, (strlen(szTemp) + 1)* sizeof(char));
		strncpy(pszTempBuf, szTemp, strlen(szTemp));
		pstSSIReqResFields[eVERSION_INFO].elementValue = pszTempBuf;
		pszTempBuf = NULL;

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "DHI Version Info[%s]", pstSSIReqResFields[eVERSION_INFO].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processLastTranCommand
 *
 * Description	: This function process the Last Tran Command and fill the corresponding
 * 				  data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int processLastTranCommand(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	FILE	*Fh                 = NULL;
	struct	stat st;

#ifdef DEBUG
	char			szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(stat(LAST_TRAN_FILE_NAME, &st) != 0)
		{
			debug_sprintf(szDbgMsg, "%s: %s doesn't exist", __FUNCTION__, LAST_TRAN_FILE_NAME);
			APP_TRACE(szDbgMsg);

			rv = fillErrGenRespDtls(pstSSIReqResFields, LAST_TRAN_ERROR);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while filling General Error response details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}


			break;
		}

		acquireMutexLock(&gptLastTranFileMutex, "Last Tran File");

		Fh = fopen(LAST_TRAN_FILE_NAME, "r+");
		if(Fh == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while opening the file, ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = fillErrGenRespDtls(pstSSIReqResFields, LAST_TRAN_ERROR);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while filling General Error response details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			releaseMutexLock(&gptLastTranFileMutex, "Last Tran File");

			break;
		}

		rv = readAndParseLastTranDataLine(Fh, pstSSIReqResFields);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s - Successfully parsed the Last Tran Data File", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s - Failure while parsing the Last Tran Data File", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = fillErrGenRespDtls(pstSSIReqResFields, LAST_TRAN_ERROR);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while filling General Error response details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

		}
#if 0
		if(fgets(szTempBuf, sizeof(szTempBuf), Fh) != NULL)
		{
			debug_sprintf(szDbgMsg, "%s - Read line from the LAST_TRAN file is [%s] ", __FUNCTION__, szTempBuf);
			APP_TRACE(szDbgMsg);

			rv = parseLastTranDataLine(szTempBuf, pstSSIReqResFields);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s - Successfully parsed the Last Tran Data line", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s - Failure while parsing the Last Tran Data line", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = fillErrGenRespDtls(pstSSIReqResFields, LAST_TRAN_ERROR);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while filling General Error response details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

			}
		}
#endif
		fclose(Fh);

		releaseMutexLock(&gptLastTranFileMutex, "Last Tran File");

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processSignatureCommand
 *
 * Description	: This function process the Signature Command and fill the corresponding
 * 				  data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int processSignatureCommand(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szTemp[256]			= "";
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	char	*pszTempBuf			= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		/* Preparing Termination Status */
		memset(szTemp, 0x00, sizeof(szTemp));
		strcpy(szTemp, "SUCCESS");

		if(pstSSIReqResFields[eTERMINATION_STATUS].elementValue != NULL)
		{
			free(pstSSIReqResFields[eTERMINATION_STATUS].elementValue);
			pstSSIReqResFields[eTERMINATION_STATUS].elementValue = NULL;
		}

		pszTempBuf = (char *)malloc((strlen(szTemp) + 1)* sizeof(char));
		if(pszTempBuf == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszTempBuf, 0x00, (strlen(szTemp) + 1)* sizeof(char));
		strncpy(pszTempBuf, szTemp, strlen(szTemp));
		pstSSIReqResFields[eTERMINATION_STATUS].elementValue = pszTempBuf;
		pszTempBuf = NULL;

		/* Preparing RESULT */
		memset(szTemp, 0x00, sizeof(szTemp));
		strcpy(szTemp, "OK");

		if(pstSSIReqResFields[eRESULT].elementValue != NULL)
		{
			free(pstSSIReqResFields[eRESULT].elementValue);
			pstSSIReqResFields[eRESULT].elementValue = NULL;
		}

		pszTempBuf = (char *)malloc((strlen(szTemp) + 1)* sizeof(char));
		if(pszTempBuf == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Result!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszTempBuf, 0x00, (strlen(szTemp) + 1)* sizeof(char));
		strncpy(pszTempBuf, szTemp, strlen(szTemp));
		pstSSIReqResFields[eRESULT].elementValue = pszTempBuf;
		pszTempBuf = NULL;

		/* Preparing Result Code */
		memset(szTemp, 0x00, sizeof(szTemp));
		strcpy(szTemp, "-1");

		if(pstSSIReqResFields[eRESULT_CODE].elementValue != NULL)
		{
			free(pstSSIReqResFields[eRESULT_CODE].elementValue);
			pstSSIReqResFields[eRESULT_CODE].elementValue = NULL;
		}

		pszTempBuf = (char *)malloc((strlen(szTemp) + 1)* sizeof(char));
		if(pszTempBuf == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszTempBuf, 0x00, (strlen(szTemp) + 1)* sizeof(char));
		strncpy(pszTempBuf, szTemp, strlen(szTemp));
		pstSSIReqResFields[eRESULT_CODE].elementValue = pszTempBuf;
		pszTempBuf = NULL;

		/* Preparing Response Text */
		memset(szTemp, 0x00, sizeof(szTemp));
		strcpy(szTemp, "SIGNATURE ADDED TO CURRENT TRAN");

		if(pstSSIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
		{
			free(pstSSIReqResFields[eRESPONSE_TEXT].elementValue);
			pstSSIReqResFields[eRESPONSE_TEXT].elementValue = NULL;
		}

		pszTempBuf = (char *)malloc((strlen(szTemp) + 1)* sizeof(char));
		if(pszTempBuf == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszTempBuf, 0x00, (strlen(szTemp) + 1)* sizeof(char));
		strncpy(pszTempBuf, szTemp, strlen(szTemp));
		pstSSIReqResFields[eRESPONSE_TEXT].elementValue = pszTempBuf;
		pszTempBuf = NULL;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processPaymentFuncType
 *
 * Description	: This function process the Payment Function Type Command
 * 				  and fill the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int processPaymentFuncType(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if( (pstSSIReqResFields[eCOMMAND].elementValue != NULL) &&
						    (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.SIGNATURE)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Signature command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processSignatureCommand(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing Signature command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Processing Signature Command");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
			}
			break;
		}

		if( (pstSSIReqResFields[eCOMMAND].elementValue != NULL) &&
								    (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.TOKEN_QUERY)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Token Query command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processTokenQueryCommand(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing Token Query command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Processing Token Query Command");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
			}
			break;
		}

		if( (pstSSIReqResFields[eCOMMAND].elementValue != NULL) &&
				(!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.CARD_RATE)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Card Rate command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = processCardRateCommand(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing Card Rate command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Processing Card Rate Command");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
			}
			break;
		}

		if(pstSSIReqResFields[ePAYMENT_TYPE].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Payment Type is not present in the request, cant process", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = PAYMENT_TYPE_NOT_PRESENT;
			break;
		}
		debug_sprintf(szDbgMsg, "%s: Payment Type [%s]", __FUNCTION__, pstSSIReqResFields[ePAYMENT_TYPE].elementValue);
		APP_TRACE(szDbgMsg);

		if(!strcasecmp(pstSSIReqResFields[ePAYMENT_TYPE].elementValue, PAYMENT_TYPE.CREDIT))
		{
			debug_sprintf(szDbgMsg, "%s: Its CREDIT transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Processing Credit Transaction");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			rv = processCreditTransaction(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing CREDIT Transaction!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

			}
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[ePAYMENT_TYPE].elementValue, PAYMENT_TYPE.DEBIT))
		{
			debug_sprintf(szDbgMsg, "%s: Its DEBIT transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Processing Debit Transaction");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			rv = processDebitTransaction(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing DEBIT Transaction!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

			}
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[ePAYMENT_TYPE].elementValue, PAYMENT_TYPE.GIFT))
		{
			debug_sprintf(szDbgMsg, "%s: Its Gift transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Processing Gift Transaction");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			rv = processGiftTransaction(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing GIFT Transaction!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

			}
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[ePAYMENT_TYPE].elementValue, PAYMENT_TYPE.PRIVLBL))
		{
			debug_sprintf(szDbgMsg, "%s: Its Private Label transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Processing Private Label Transaction");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			rv = processPrivLblTransaction(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing Private Label Transaction!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

			}
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[ePAYMENT_TYPE].elementValue, PAYMENT_TYPE.CHECK))
		{
			debug_sprintf(szDbgMsg, "%s: Its Check transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Processing Check Transaction");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}


			rv = processCheckTransaction(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing Check Transaction!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

			}
			break;
		}

		if(!strcasecmp(pstSSIReqResFields[ePAYMENT_TYPE].elementValue, PAYMENT_TYPE.EBT))
		{
			debug_sprintf(szDbgMsg, "%s: Its EBT transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Processing EBT Transaction");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}


			rv = processEBTTransaction(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing EBT Transaction!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

			}
			break;
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processBatchFuncType
 *
 * Description	: This function process the Batch Function Type Command
 * 				  and fill the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int processBatchFuncType(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int		rv				= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Currently we are not supporting these commands, please add once we need to support */
		rv = COMMAND_NOT_SUPPORTED;

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processReportFuncType
 *
 * Description	: This function process the Report Function Type Command
 * 				  and fill the corresponding data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int processReportFuncType(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szDHIProcessor[15]	= "";
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

	#ifdef DEBUG
		char			szDbgMsg[512]	= "";
	#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		debug_sprintf(szDbgMsg, "%s: Its %s Report Command", __FUNCTION__, pstSSIReqResFields[eCOMMAND].elementValue);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing %s Report Command", pstSSIReqResFields[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		/* For Elavon Host Interface, LAST_TRAN command needs to be mapped to TRANS_INQUIRY to Host and get the Response from Host. So Not Handling it in DHI*/
		getDHIProcessorField(szDHIProcessor);
		if( (pstSSIReqResFields[eCOMMAND].elementValue != NULL) &&
								    (!strcasecmp(pstSSIReqResFields[eCOMMAND].elementValue, COMMAND.LAST_TRAN)) && !(strcasecmp(szDHIProcessor, "ELVN") == SUCCESS) )
		{
			debug_sprintf(szDbgMsg, "%s: Its Last Tran command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processLastTranCommand(pstSSIReqResFields);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while processing Last Tran command!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Processing Last Tran Command");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
			}
			break;
		}

		rv = processReportCommand(pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Report Command!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		}


		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: readAndParseLastTranDataLine
 *
 * Description	: This function parses the last tran file line buffer and fills
 * 				  the data system
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int readAndParseLastTranDataLine(FILE *Fh , TRANDTLS_PTYPE pstSSIReqResFields)
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int		iBufRead			= 0;
	char	szTempBuffer[256+1] = "";
	int		iIndex				= 0;
	int 	iRecordIndex		= 0;

#ifdef DEBUG
	char			szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Format of the LAST TRAN File
	 * <RESULT><LF><RESULT_CODE><LF><TERMINATION_STATUS><LF><RESPONSE_TEXT><LF><HOST_RESPCODE><LF><CLIENTID><LF><TROUTD><LF><CTROUTD><LF><INTRN_SEQ_NUM><LF><COMMAND><LF><ACCT_NUM><LF>
	 * <CARDHOLDER><LF><TRANS_AMOUNT><LF><APPROVED_AMOUNT><LF><TRANS_DATE><LF><TRANS_TIME><LF><INVOICE><LF><PAYMENT_TYPE><LF><PAYMENT_MEDIA><LF><CARD_TOKEN><LF>
	 *
	 */

	if(Fh == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No File to read/parse!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	iBufRead = sizeof(szTempBuffer) - 1;
	iIndex = 1;

	while(1)
	{
		iRecordIndex = getRecordIndex(iIndex);
		if(iRecordIndex == FAILURE)
		{
			debug_sprintf(szDbgMsg, "%s: No more to read from file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains RESULT field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				debug_sprintf(szDbgMsg, "%s: Read [%s], Len [%d]", __FUNCTION__, szTempBuffer, iLen);
				APP_TRACE(szDbgMsg);

				pstSSIReqResFields[iRecordIndex].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[iRecordIndex].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[iRecordIndex].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[iRecordIndex].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		iIndex++;

#if 0
		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains RESULT_CODE field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				debug_sprintf(szDbgMsg, "%s: Read [%s], Len [%d]", __FUNCTION__, szTempBuffer, iLen);
				APP_TRACE(szDbgMsg);

				pstSSIReqResFields[eRESULT_CODE].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eRESULT_CODE].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eRESULT_CODE].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eRESULT_CODE].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains Termination Status
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) -1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				debug_sprintf(szDbgMsg, "%s: Read [%s], Len [%d]", __FUNCTION__, szTempBuffer, iLen);
				APP_TRACE(szDbgMsg);

				pstSSIReqResFields[eTERMINATION_STATUS].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eTERMINATION_STATUS].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eTERMINATION_STATUS].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eTERMINATION_STATUS].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains Response Text
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) -1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				debug_sprintf(szDbgMsg, "%s: Read [%s], Len [%d]", __FUNCTION__, szTempBuffer, iLen);
				APP_TRACE(szDbgMsg);

				pstSSIReqResFields[eRESPONSE_TEXT].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eRESPONSE_TEXT].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eRESPONSE_TEXT].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eRESPONSE_TEXT].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains Host Response code
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				debug_sprintf(szDbgMsg, "%s: Read [%s], Len [%d]", __FUNCTION__, szTempBuffer, iLen);
				APP_TRACE(szDbgMsg);

				pstSSIReqResFields[eHOST_RESPCODE].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eHOST_RESPCODE].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eHOST_RESPCODE].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eHOST_RESPCODE].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains Client ID field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				debug_sprintf(szDbgMsg, "%s: Read [%s], Len [%d]", __FUNCTION__, szTempBuffer, iLen);
				APP_TRACE(szDbgMsg);

				pstSSIReqResFields[eCLIENT_ID].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eCLIENT_ID].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eCLIENT_ID].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eCLIENT_ID].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains TROUTD field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				debug_sprintf(szDbgMsg, "%s: Read [%s], Len [%d]", __FUNCTION__, szTempBuffer, iLen);
				APP_TRACE(szDbgMsg);

				pstSSIReqResFields[eTROUTD].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eTROUTD].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eTROUTD].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eTROUTD].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains CTROUTD field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				debug_sprintf(szDbgMsg, "%s: Read [%s], Len [%d]", __FUNCTION__, szTempBuffer, iLen);
				APP_TRACE(szDbgMsg);

				pstSSIReqResFields[eCTROUTD].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eCTROUTD].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eCTROUTD].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eCTROUTD].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains INTRN_SEQ_NUM field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eINTRN_SEQ_NUM].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains COMMAND field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eLASTTRAN_COMMAND].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eLASTTRAN_COMMAND].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eLASTTRAN_COMMAND].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eLASTTRAN_COMMAND].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		/* Format of the LAST TRAN File
		 * <RESULT><LF><RESULT_CODE><LF><TERMINATION_STATUS><LF><RESPONSE_TEXT><LF><HOST_RESPCODE><LF><CLIENTID><LF><TROUTD><LF><CTROUTD><LF><INTRN_SEQ_NUM><LF><COMMAND><LF><ACCT_NUM><LF>
		 * <CARDHOLDER><LF><TRANS_AMOUNT><LF><APPROVED_AMOUNT><LF><TRANS_DATE><LF><TRANS_TIME><LF><INVOICE><LF><PAYMENT_TYPE><LF><PAYMENT_MEDIA><LF><CARD_TOKEN><LF>
		 *
		 */

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains ACCT_NUM field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eACCT_NUM].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eACCT_NUM].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eACCT_NUM].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eACCT_NUM].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains CARDHOLDER field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eCARDHOLDER].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eCARDHOLDER].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eCARDHOLDER].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eCARDHOLDER].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains TRANS_AMOUNT field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eTRANS_AMOUNT].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eTRANS_AMOUNT].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eTRANS_AMOUNT].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eTRANS_AMOUNT].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains APPROVED_AMOUNT field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eAPPROVED_AMOUNT].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eAPPROVED_AMOUNT].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eAPPROVED_AMOUNT].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eAPPROVED_AMOUNT].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains TRANS_DATE field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eTRANS_DATE].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eTRANS_DATE].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eTRANS_DATE].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eTRANS_DATE].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains TRANS_TIME field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eTRANS_TIME].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eTRANS_TIME].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eTRANS_TIME].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eTRANS_TIME].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains INVOICE field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eINVOICE].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eINVOICE].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eINVOICE].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eINVOICE].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		/* Format of the LAST TRAN File
		 * <RESULT><LF><RESULT_CODE><LF><TERMINATION_STATUS><LF><RESPONSE_TEXT><LF><HOST_RESPCODE><LF><CLIENTID><LF><TROUTD><LF><CTROUTD><LF><INTRN_SEQ_NUM><LF><COMMAND><LF><ACCT_NUM><LF>
		 * <CARDHOLDER><LF><TRANS_AMOUNT><LF><APPROVED_AMOUNT><LF><TRANS_DATE><LF><TRANS_TIME><LF><INVOICE><LF><PAYMENT_TYPE><LF><PAYMENT_MEDIA><LF><CARD_TOKEN><LF>
		 *
		 */

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains PAYMENT_TYPE field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[ePAYMENT_TYPE].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[ePAYMENT_TYPE].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[ePAYMENT_TYPE].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[ePAYMENT_TYPE].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains PAYMENT_MEDIA field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[ePAYMENT_MEDIA].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[ePAYMENT_MEDIA].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[ePAYMENT_MEDIA].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[ePAYMENT_MEDIA].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
		if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains CARD_TOKEN field
		{
			if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
			{
				szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

				iLen = strlen(szTempBuffer);

				pstSSIReqResFields[eCARD_TOKEN].elementValue = (char *)malloc(iLen+1 * sizeof(char));
				if(pstSSIReqResFields[eCARD_TOKEN].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(pstSSIReqResFields[eCARD_TOKEN].elementValue, 0x00, iLen+1);
					strncpy(pstSSIReqResFields[eCARD_TOKEN].elementValue, szTempBuffer, iLen);
				}
			}
		}
		else
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		break; //breaking from the while loop
#endif
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRecordIndex
 *
 * Description	: This function gives the record index in the data system to store
 * 				  the value from the last tran file
 *
 *
 * Input Params	: None
 *
 * Output Params: Index
 * ============================================================================
 */
static int getRecordIndex(int iFileIndex)
{
	int iRecordIndex;

	/* Format of the LAST TRAN File
	 * <RESULT><LF><RESULT_CODE><LF><TERMINATION_STATUS><LF><RESPONSE_TEXT><LF><HOST_RESPCODE><LF><CLIENTID><LF><TROUTD><LF><CTROUTD><LF><INTRN_SEQ_NUM><LF><COMMAND><LF><ACCT_NUM><LF>
	 * <CARDHOLDER><LF><TRANS_AMOUNT><LF><APPROVED_AMOUNT><LF><TRANS_DATE><LF><TRANS_TIME><LF><INVOICE><LF><PAYMENT_TYPE><LF><PAYMENT_MEDIA><LF><CARD_TOKEN><LF>AUTH_CODE<LF>
	 *
	 */

	switch(iFileIndex)
	{
	case 1:
		iRecordIndex = eRESULT;
		break;
	case 2:
		iRecordIndex = eRESULT_CODE;
		break;
	case 3:
		iRecordIndex = eTERMINATION_STATUS;
		break;
	case 4:
		iRecordIndex = eRESPONSE_TEXT;
		break;
	case 5:
		iRecordIndex = eHOST_RESPCODE;
		break;
	case 6:
		iRecordIndex = eCLIENT_ID;
		break;
	case 7:
		iRecordIndex = eTROUTD;
		break;
	case 8:
		iRecordIndex = eCTROUTD;
		break;
	case 9:
		iRecordIndex = eINTRN_SEQ_NUM;
		break;
	case 10:
		iRecordIndex = eLASTTRAN_COMMAND;
		break;
	case 11:
		iRecordIndex = eACCT_NUM;
		break;
	case 12:
		iRecordIndex = eCARDHOLDER;
		break;
	case 13:
		iRecordIndex = eTRANS_AMOUNT;
		break;
	case 14:
		iRecordIndex = eAPPROVED_AMOUNT;
		break;
	case 15:
		iRecordIndex = eTRANS_DATE;
		break;
	case 16:
		iRecordIndex = eTRANS_TIME;
		break;
	case 17:
		iRecordIndex = eINVOICE;
		break;
	case 18:
		iRecordIndex = ePAYMENT_TYPE;
		break;
	case 19:
		iRecordIndex = ePAYMENT_MEDIA;
		break;
	case 20:
		iRecordIndex = eCARD_TOKEN;
		break;
	case 21:
		iRecordIndex = eAUTH_CODE;
		break;
	default:
		iRecordIndex = FAILURE;
		break;
	}
	return iRecordIndex;
}

/*
 * ============================================================================
 * Function Name: fillErrGenRespDtls
 *
 * Description	: This function fills the general response details based on the
 * 				  error code
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int fillErrGenRespDtls(TRANDTLS_PTYPE pstSSIReqResFields, int iErrCode)
{
	int		rv				= SUCCESS;
	char *	pszTermStat		= NULL;
	char *	pszRsltCode		= NULL;
	char *	pszRslt			= NULL;
	char *	pszRspTxt		= NULL;
	int		iSetRespDtls	= 0;
	char * 	pszTempBuf		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstSSIReqResFields == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input Param is NULL !!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Error Code [%d]", __FUNCTION__, iErrCode);
		APP_TRACE(szDbgMsg);

		switch(iErrCode)
		{
		case PAYMENT_TYPE_NOT_SUPPORTED:
			pszTermStat = "NOT PROCESSED";
			pszRsltCode = "6";
			pszRslt		= "ERROR";
			pszRspTxt	= "PAYMENT_TYPE NOT SUPPORTED";
			iSetRespDtls = 1;
			break;

		case COMMAND_NOT_SUPPORTED:
			pszTermStat = "NOT PROCESSED";
			pszRsltCode = "9001";
			pszRslt		= "ERROR";
			pszRspTxt	= "COMMAND NOT SUPPORTED";
			iSetRespDtls = 1;
			break;

		case PAYMENT_TYPE_NOT_PRESENT:
			pszTermStat = "NOT PROCESSED";
			pszRsltCode = "1000";
			pszRslt		= "ERROR";
			pszRspTxt	= "PAYMENT TYPE PARAMETER MISSING";
			iSetRespDtls = 1;
			break;

		case LAST_TRAN_ERROR:
			pszTermStat = "NOT PROCESSED";
			pszRsltCode = "6";
			pszRslt		= "ERROR";
			pszRspTxt	= "LAST TRANSACTION NOT AVAILABLE";
			iSetRespDtls = 1;
			break;

		default: //If above error codes are not there then these response details would have got filled
			iSetRespDtls = 0;
			break;
		}
		if(iSetRespDtls) //Resp details are set, need to copy them to the structure
		{
			/* Preparing Termination Status */
			if(pstSSIReqResFields[eTERMINATION_STATUS].elementValue != NULL)
			{
				free(pstSSIReqResFields[eTERMINATION_STATUS].elementValue);
				pstSSIReqResFields[eTERMINATION_STATUS].elementValue = NULL;
			}

			pszTempBuf = (char *)malloc((strlen(pszTermStat) + 1)* sizeof(char));
			if(pszTempBuf == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(pszTempBuf, 0x00, (strlen(pszTermStat) + 1)* sizeof(char));
			strncpy(pszTempBuf, pszTermStat, strlen(pszTermStat));
			pstSSIReqResFields[eTERMINATION_STATUS].elementValue = pszTempBuf;
			pszTempBuf = NULL;

			/* Preparing RESULT */
			if(pstSSIReqResFields[eRESULT].elementValue != NULL)
			{
				free(pstSSIReqResFields[eRESULT].elementValue);
				pstSSIReqResFields[eRESULT].elementValue = NULL;
			}

			pszTempBuf = (char *)malloc((strlen(pszRslt) + 1)* sizeof(char));
			if(pszTempBuf == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Result!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(pszTempBuf, 0x00, (strlen(pszRslt) + 1)* sizeof(char));
			strncpy(pszTempBuf, pszRslt, strlen(pszRslt));
			pstSSIReqResFields[eRESULT].elementValue = pszTempBuf;
			pszTempBuf = NULL;

			/* Preparing Result Code */
			if(pstSSIReqResFields[eRESULT_CODE].elementValue != NULL)
			{
				free(pstSSIReqResFields[eRESULT_CODE].elementValue);
				pstSSIReqResFields[eRESULT_CODE].elementValue = NULL;
			}

			pszTempBuf = (char *)malloc((strlen(pszRsltCode) + 1)* sizeof(char));
			if(pszTempBuf == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(pszTempBuf, 0x00, (strlen(pszRsltCode) + 1)* sizeof(char));
			strncpy(pszTempBuf, pszRsltCode, strlen(pszRsltCode));
			pstSSIReqResFields[eRESULT_CODE].elementValue = pszTempBuf;
			pszTempBuf = NULL;

			/* Preparing Response Text */
			if(pstSSIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
			{
				free(pstSSIReqResFields[eRESPONSE_TEXT].elementValue);
				pstSSIReqResFields[eRESPONSE_TEXT].elementValue = NULL;
			}

			pszTempBuf = (char *)malloc((strlen(pszRspTxt) + 1)* sizeof(char));
			if(pszTempBuf == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation failed for Termination status!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(pszTempBuf, 0x00, (strlen(pszRspTxt) + 1)* sizeof(char));
			strncpy(pszTempBuf, pszRspTxt, strlen(pszRspTxt));
			pstSSIReqResFields[eRESPONSE_TEXT].elementValue = pszTempBuf;
			pszTempBuf = NULL;
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
