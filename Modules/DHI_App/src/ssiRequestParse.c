/******************************************************************
*                      ssiRequestParse.c                          *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis related to parsing of            *
*              SSI request                                        *
* 			                                                      *
*                                                                 *
* Created on: Dec 8, 2014                                         *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "DHI_App/common.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/hash.h"
#include "DHI_App/externfunc.h"
#include "DHI_App/xmlUtils.h"

static int fillSSIDataStruct(char *, char *, TRANDTLS_PTYPE );

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: parseSSIRequest
 *
 * Description	: This function parses the SSI request and fills the data fields
 * 				  in the structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int parseSSIRequest(HTTPINFO_PTYPE pstHTTPInfo, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv					= SUCCESS;
	int				iLen				= 0;
	int				iCnt				= 0;
	xmlDoc *		docPtr				= NULL;
	xmlNode *		rootPtr				= NULL;
	xmlNode *		curNode				= NULL;
	xmlNode *		tempCurNode			= NULL;
	xmlChar *		value				= NULL;
	xmlAttr * 		attribute 			= NULL;
	char			szRootName[50]		= "";
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";
	char*			pszNodeName			= NULL;
	int				iAppLogEnabled		= 0;
	long			lChildCount			= 0;
	DHI_BOOL		bVarLstFound		= DHI_FALSE;
	PTVARLST_PTYPE	pstLocalVarLst		= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	pstLocalVarLst = getDHIVarLstDtls();

	while(1)
	{
		if(pstHTTPInfo->pszRequest == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Request to Parse!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstHTTPInfo->iStatusCode = HTTP_STATUS_BAD_REQUEST;

			rv = FAILURE;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Parsing the Request from the Client");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(pstHTTPInfo->pszRequest);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		APP_TRACE(szDbgMsg);

		docPtr = xmlParseMemory(pstHTTPInfo->pszRequest, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstHTTPInfo->iStatusCode = HTTP_STATUS_BAD_REQUEST;

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "XML Parsing of Request Failed");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstHTTPInfo->iStatusCode = HTTP_STATUS_BAD_REQUEST;

			break;
		}
		strcpy(szRootName, (char *)rootPtr->name);

		debug_sprintf(szDbgMsg, "%s: The root name of the SSI XML request is %s",__FUNCTION__, szRootName);
		APP_TRACE(szDbgMsg);

		if(!( (strcasecmp(szRootName, "TRANSACTION") == 0) ||
			  (strcasecmp(szRootName, "MSH")         == 0)  ) )
		{
			debug_sprintf(szDbgMsg, "%s:Invalid root name in the request!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstHTTPInfo->iStatusCode = HTTP_STATUS_BAD_REQUEST;

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Invalid Root Name[%s] in the XML Request", szRootName);
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		/*
		 * Traverse node by node in the request and get the value of that node
		 */
		for(curNode = rootPtr->children; curNode != NULL; curNode = curNode->next)
		{
			//debug_sprintf(szDbgMsg, "%s:Node Type is %d", __FUNCTION__, curNode->type);
			//APP_TRACE(szDbgMsg);

			if(curNode->type == XML_TEXT_NODE) //No need to parse the text node
			{
				continue;
			}

			//debug_sprintf(szDbgMsg, "%s:Name of this node is %s", __FUNCTION__, (char *)curNode->name);
			//APP_TRACE(szDbgMsg);

			lChildCount = xmlChildElementCount(curNode);

			//debug_sprintf(szDbgMsg, "%s:Number of child nodes for this %s node is %ld", __FUNCTION__, (char *)curNode->name, lChildCount);
			//APP_TRACE(szDbgMsg);


			if(lChildCount > 0) //This node contains sub nodes, need to traverse those nodes
			{
				/*PRAADEEP:17-11-16: In order to support for varlist pass through fields, if there are child elements for a node then
				 * the node name will checked with all configured varlist's and if matches & if the value is not filled already then xml will parsed and
				 * the data will stored in PTVARLST_STYPE structure, it the value part of the structure will get freed at the end of the transaction.
				 * if the node node name does not match with configured varlist's then the old behavior will continue*/
				pszNodeName = strdup((char *)curNode->name);

				for(iCnt = 0; iCnt < pstLocalVarLst->iReqVARLSTCnt; iCnt++)
				{
					if((strcmp(pszNodeName, pstLocalVarLst->pstVarlistReqDtls[iCnt].key) == SUCCESS) && (pstLocalVarLst->pstVarlistReqDtls[iCnt].value == NULL))
					{
						bVarLstFound = DHI_TRUE;
						/* Parse the xml for the required data */
						/* Parse xml and store the variable linked list of data */
						rv = parseXMLForListElem(docPtr, curNode->xmlChildrenNode,
								(DHI_VARLST_INFO_PTYPE) pstLocalVarLst->pstVarlistReqDtls[iCnt].valMetaInfo,
								(DHI_VAL_LST_PTYPE *) &pstLocalVarLst->pstVarlistReqDtls[iCnt].value);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Could not add this VARLIST[%s] to the structure", __FUNCTION__, pszNodeName);
							APP_TRACE(szDbgMsg);
							rv = SUCCESS;
						}
					}

				}

				free(pszNodeName);
				pszNodeName = NULL;
				if(bVarLstFound == DHI_TRUE)
				{
					bVarLstFound = DHI_FALSE;
					continue;
				}

				debug_sprintf(szDbgMsg, "%s:Need to traverse through these child nodes of this %s node", __FUNCTION__, (char *)curNode->name);
				APP_TRACE(szDbgMsg);

				for(tempCurNode = curNode->children; tempCurNode != NULL; tempCurNode = tempCurNode->next)
				{
					value = xmlNodeListGetString(docPtr, tempCurNode->xmlChildrenNode, 1);
					if(value != NULL)
					{
					#ifdef DEVDEBUG
						if(strlen((char *)value) < 4500)
						{
							debug_sprintf(szDbgMsg, "%s:%s: Node Name [%s], Node Value[%s]",DEVDEBUGMSG,__FUNCTION__, (char *)tempCurNode->name, (char *)value);
							APP_TRACE(szDbgMsg);
						}
					#endif
						rv = fillSSIDataStruct((char *)tempCurNode->name, (char *)value, pstSSIReqResFields);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s:Could not add this value to the structure", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = SUCCESS;
						}

						/* Deallocating any allocated memory not in use now */
						xmlFree(value);
						value = NULL;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s:No Value for this node", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
			}
			else //no child nodes for this current node
			{
				/* Checking if Attributes are present for this node
				 * if present, parse the attributes
				 */
				for(attribute = curNode->properties; attribute != NULL; attribute = attribute->next)
				{
					//value = xmlNodeListGetString(curNode->doc, attribute->children, 1);

					value = xmlGetProp(curNode, attribute->name);

					debug_sprintf(szDbgMsg, "%s:Attribute Name [%s] Value [%s]", __FUNCTION__, attribute->name, value);
					APP_TRACE(szDbgMsg);

					if(value != NULL)
					{
						rv = fillSSIDataStruct((char *)attribute->name, (char *)value, pstSSIReqResFields);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s:Could not add this value to the structure", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = SUCCESS;
						}

						/* Deallocating any allocated memory not in use now */
						xmlFree(value);
						value = NULL;
					}
				}

				value = xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value != NULL)
				{
				#ifdef DEVDEBUG
					if(strlen((char *)value) < 4500)
					{
						debug_sprintf(szDbgMsg, "%s:%s: Node Name [%s], Node Value[%s]",DEVDEBUGMSG,__FUNCTION__, (char *)curNode->name, (char *)value);
						APP_TRACE(szDbgMsg);
					}
				#endif
					rv = fillSSIDataStruct((char *)curNode->name, (char *)value, pstSSIReqResFields);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:Could not add this value to the structure", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = SUCCESS;
					}

					/* Deallocating any allocated memory not in use now */
					xmlFree(value);
					value = NULL;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s:No Value for this node", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Successfully Parsed the Request from the Client");
			addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillSSIDataStruct
 *
 * Description	: This function fills/stores the value at the corresponding
 * 				  index in the data structure
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int fillSSIDataStruct(char *pszTagName, char *pszTagValue, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int					rv			= SUCCESS;
	int					iValLen		= 0;
	int					iPTReqCnt	= 0;
	int					iPTRespCnt	= 0;
	PASSTHROUGH_PTYPE	pstDHIPTLst	= NULL;
	HASHLST_PTYPE 		pstHashLst	= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pszTagName == NULL || pszTagValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Either TagName or TagValue is NULL!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Do the Hash look up for this Tag Name */
		pstHashLst = lookupHashTable(pszTagName);
		if(pstHashLst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Did not find entry for this Tag[%s] in the Hash Table!!!", __FUNCTION__, pszTagName);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Index for this Tag in the structure is %d", __FUNCTION__, pstHashLst->iTagIndexVal);
		APP_TRACE(szDbgMsg);

		pstDHIPTLst = getDHIPassThroughDtls();
		iPTReqCnt = pstDHIPTLst->iReqXMLTagsCnt;
		iPTRespCnt = pstDHIPTLst->iRespXMLTagsCnt;

		if(pstHashLst->iTagIndexVal > eELEMENT_INDEX_MAX + iPTReqCnt + iPTRespCnt) //Safety Check
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Index Value!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
			//Should not be the case, something wrong
		}

		iValLen = strlen(pszTagValue);

		//debug_sprintf(szDbgMsg, "%s: Length of the tagvalue is %d", __FUNCTION__, iValLen);
		//APP_TRACE(szDbgMsg);

		/* Allocate memory at the corresponding index in the data structure */

		pstSSIReqResFields[pstHashLst->iTagIndexVal].elementValue = (char *)malloc(iValLen + 1 * sizeof(char));
		if(pstSSIReqResFields[pstHashLst->iTagIndexVal].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory Allocation failed!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		memset(pstSSIReqResFields[pstHashLst->iTagIndexVal].elementValue, 0x00, (iValLen + 1 * sizeof(char)));

		/* Copy the tagvalue to the data structure */

		strcpy(pstSSIReqResFields[pstHashLst->iTagIndexVal].elementValue, pszTagValue);

		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDHIIndex
 *
 * Description	: This function will return the index in the DHI data structure, This is implemented to get the Index of DHI structure from Host Libraries
 *
 * Input Params	: char* (Node Name)
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getDHIIndex(char *pszTagName)
{
	int					rv			= SUCCESS;
	int					iPTReqCnt	= 0;
	int					iPTRespCnt	= 0;
	PASSTHROUGH_PTYPE	pstDHIPTLst	= NULL;
	HASHLST_PTYPE 		pstHashLst	= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pszTagName == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: TagName is NULL!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Do the Hash look up for this Tag Name */
		pstHashLst = lookupHashTable(pszTagName);
		if(pstHashLst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Did not find entry for this Tag[%s] in the Hash Table!!!", __FUNCTION__, pszTagName);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Index for this Tag in the structure is %d", __FUNCTION__, pstHashLst->iTagIndexVal);
		APP_TRACE(szDbgMsg);

		pstDHIPTLst = getDHIPassThroughDtls();
		iPTReqCnt = pstDHIPTLst->iReqXMLTagsCnt;
		iPTRespCnt = pstDHIPTLst->iRespXMLTagsCnt;

		if(pstHashLst->iTagIndexVal > eELEMENT_INDEX_MAX + iPTReqCnt + iPTRespCnt) //Safety Check
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Index Value!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
			//Should not be the case, something wrong
		}
		else
		{
			rv = pstHashLst->iTagIndexVal;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
