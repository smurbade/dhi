/******************************************************************
*                      hostInterface.c                            *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis which deals with host interface  *
* 			   libraries                                          *
*                                                                 *
* Created on: Dec 11, 2014                                        *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <errno.h>

#include "DHI_App/common.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/hostInterface.h"
#include "DHI_App/dhiCfgDef.h"
#include "DHI_App/externfunc.h"

static void getUniqueProcessorIds(char szInputList[][10], char szOutputList[][10]);
static int setCommonFuncPointers(char szProcessorList[][10]);
static int initHILibs(char szProcessorList[][10]);

static FUNC_POINTERS_STYPE stHostLibFuncPointers;

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: loadHostInterfaceLibs
 *
 * Description	: This function loads the corresponding functions from the
 * 				  host interface library depends on the configuration
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	loadHostInterfaceLibs()
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char 	szLibName[100]		= "";
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	char	szProcessorArray[MAX_PROCESSOR_NUM][10];
	char	szUniqueProcessorArray[MAX_PROCESSOR_NUM][10];

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stHostLibFuncPointers, 0x00, sizeof(FUNC_POINTERS_STYPE));

	//Filling the Processor Array
	memset(szProcessorArray, 0x00, MAX_PROCESSOR_NUM * 10 * sizeof(char));

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	/* Setting the Credit Transaction Function Pointer*/
	memset(szProcessorArray[0], 0x00, 10 * sizeof(char));
	if(strlen(dhiSettings.szCreditProcID) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Credit Processor is set [%s]", __FUNCTION__, dhiSettings.szCreditProcID);
		APP_TRACE(szDbgMsg);

		memset(szLibName, 0x00, sizeof(szLibName));

#ifdef DEBUG
		sprintf(szLibName, "libps%shiD.so", dhiSettings.szCreditProcID);
#else
		sprintf(szLibName, "libps%shi.so", dhiSettings.szCreditProcID);
#endif


		debug_sprintf(szDbgMsg, "%s: Credit Processor library path [%s]", __FUNCTION__, szLibName);
		APP_TRACE(szDbgMsg);

		rv = setDynamicFunctionPtr(szLibName, CREDIT_TRAN_FUNC_NAME, &stHostLibFuncPointers.fnpProcessCreditRequest);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, CREDIT_TRAN_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", CREDIT_TRAN_FUNC_NAME, szLibName);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, NULL);
			}
			//What to do here!!!
		}

		/*
		 * Currently SAF is allowed only for Credit transactions
		 * thats why setting this function only for the credit host processor
		 */
		rv = setDynamicFunctionPtr(szLibName, CHECK_HOST_CON_FUNC_NAME, &stHostLibFuncPointers.fnpCheckHostConnection);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, CHECK_HOST_CON_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", CHECK_HOST_CON_FUNC_NAME, szLibName);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, NULL);
			}
			//What to do here!!!
		}

		strcpy(szProcessorArray[0], dhiSettings.szCreditProcID);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Credit Processor is not set", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		stHostLibFuncPointers.fnpProcessCreditRequest = NULL;
	}

	/* Setting the Debit Transaction Function Pointer*/
	memset(szProcessorArray[1], 0x00, 10 * sizeof(char));
	if(strlen(dhiSettings.szDebitProcID) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Debit Processor is set [%s]", __FUNCTION__, dhiSettings.szDebitProcID);
		APP_TRACE(szDbgMsg);

		memset(szLibName, 0x00, sizeof(szLibName));

#ifdef DEBUG
		sprintf(szLibName, "libps%shiD.so", dhiSettings.szDebitProcID);
#else
		sprintf(szLibName, "libps%shi.so", dhiSettings.szDebitProcID);
#endif

		debug_sprintf(szDbgMsg, "%s: Debit Processor library path [%s]", __FUNCTION__, szLibName);
		APP_TRACE(szDbgMsg);

		rv = setDynamicFunctionPtr(szLibName, DEBIT_TRAN_FUNC_NAME, &stHostLibFuncPointers.fnpProcessDebitRequest);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, DEBIT_TRAN_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", DEBIT_TRAN_FUNC_NAME, szLibName);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, NULL);
			}
			//What to do here!!!
		}
		strcpy(szProcessorArray[1], dhiSettings.szDebitProcID);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Debit Processor is not set", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		stHostLibFuncPointers.fnpProcessDebitRequest = NULL;
	}

	/* Setting the Gift Transaction Function Pointer*/
	memset(szProcessorArray[2], 0x00, 10 * sizeof(char));
	if(strlen(dhiSettings.szGiftProcID) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Gift Processor is set [%s]", __FUNCTION__, dhiSettings.szGiftProcID);
		APP_TRACE(szDbgMsg);

		memset(szLibName, 0x00, sizeof(szLibName));

#ifdef DEBUG
		sprintf(szLibName, "libps%shiD.so", dhiSettings.szGiftProcID);
#else
		sprintf(szLibName, "libps%shi.so", dhiSettings.szGiftProcID);
#endif

		debug_sprintf(szDbgMsg, "%s: Gift Processor library path [%s]", __FUNCTION__, szLibName);
		APP_TRACE(szDbgMsg);

		rv = setDynamicFunctionPtr(szLibName, GIFT_TRAN_FUNC_NAME, &stHostLibFuncPointers.fnpProcessGiftRequest);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, GIFT_TRAN_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", GIFT_TRAN_FUNC_NAME, szLibName);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, NULL);
			}
			//What to do here!!!
		}
		strcpy(szProcessorArray[2], dhiSettings.szGiftProcID);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Gift Processor is not set", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		stHostLibFuncPointers.fnpProcessGiftRequest = NULL;
	}

	/* Setting the Private Label Transaction Function Pointer*/
	memset(szProcessorArray[3], 0x00, 10 * sizeof(char));
	if(strlen(dhiSettings.szPrivLblProcID) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Private Label Processor is set [%s]", __FUNCTION__, dhiSettings.szPrivLblProcID);
		APP_TRACE(szDbgMsg);

		memset(szLibName, 0x00, sizeof(szLibName));

#ifdef DEBUG
		sprintf(szLibName, "libps%shiD.so", dhiSettings.szPrivLblProcID);
#else
		sprintf(szLibName, "libps%shi.so", dhiSettings.szPrivLblProcID);
#endif

		debug_sprintf(szDbgMsg, "%s: Private Label Processor library path [%s]", __FUNCTION__, szLibName);
		APP_TRACE(szDbgMsg);

		rv = setDynamicFunctionPtr(szLibName, PRIVLBL_TRAN_FUNC_NAME, &stHostLibFuncPointers.fnpProcessPrivLblRequest);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, PRIVLBL_TRAN_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", PRIVLBL_TRAN_FUNC_NAME, szLibName);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, NULL);
			}
			//What to do here!!!
		}
		strcpy(szProcessorArray[3], dhiSettings.szPrivLblProcID);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Private Label Processor is not set", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		stHostLibFuncPointers.fnpProcessPrivLblRequest = NULL;
	}

	/* Setting the Check Transaction Function Pointer*/
	memset(szProcessorArray[4], 0x00, 10 * sizeof(char));
	if(strlen(dhiSettings.szCheckProcID) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Check Processor is set [%s]", __FUNCTION__, dhiSettings.szCheckProcID);
		APP_TRACE(szDbgMsg);

		memset(szLibName, 0x00, sizeof(szLibName));
#ifdef DEBUG
		sprintf(szLibName, "libps%shiD.so", dhiSettings.szCheckProcID);
#else
		sprintf(szLibName, "libps%shi.so", dhiSettings.szCheckProcID);
#endif

		debug_sprintf(szDbgMsg, "%s: Check Processor library path [%s]", __FUNCTION__, szLibName);
		APP_TRACE(szDbgMsg);

		rv = setDynamicFunctionPtr(szLibName, CHECK_TRAN_FUNC_NAME, &stHostLibFuncPointers.fnpProcessCheckRequest);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, CHECK_TRAN_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", CHECK_TRAN_FUNC_NAME, szLibName);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, NULL);
			}
			//What to do here!!!
		}
		strcpy(szProcessorArray[4], dhiSettings.szCheckProcID);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Check Processor is not set", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		stHostLibFuncPointers.fnpProcessCheckRequest = NULL;
	}

	/* Setting the EBT Transaction Function Pointer*/
	memset(szProcessorArray[5], 0x00, 10 * sizeof(char));
	if(strlen(dhiSettings.szEBTProcID) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: EBT Processor is set [%s]", __FUNCTION__, dhiSettings.szEBTProcID);
		APP_TRACE(szDbgMsg);

		memset(szLibName, 0x00, sizeof(szLibName));
#ifdef DEBUG
		sprintf(szLibName, "libps%shiD.so", dhiSettings.szEBTProcID);
#else
		sprintf(szLibName, "libps%shi.so", dhiSettings.szEBTProcID);
#endif

		debug_sprintf(szDbgMsg, "%s: EBT Processor library path [%s]", __FUNCTION__, szLibName);
		APP_TRACE(szDbgMsg);

		rv = setDynamicFunctionPtr(szLibName, EBT_TRAN_FUNC_NAME, &stHostLibFuncPointers.fnpProcessEBTRequest);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, EBT_TRAN_FUNC_NAME, szLibName);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", EBT_TRAN_FUNC_NAME, szLibName);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, NULL);
			}
			//What to do here!!!
		}
		strcpy(szProcessorArray[5], dhiSettings.szEBTProcID);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Check Processor is not set", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		stHostLibFuncPointers.fnpProcessEBTRequest = NULL;
	}

	/* Get the Unique Processor Names to link the Init/get version
	 * commands from host interface library
	 */
	memset(szUniqueProcessorArray, 0x00, MAX_PROCESSOR_NUM * 10 * sizeof(char));
	getUniqueProcessorIds(szProcessorArray, szUniqueProcessorArray);

	/* Setting the InitLib/GetVersion Function Pointer*/
	rv = setCommonFuncPointers(szUniqueProcessorArray);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failure while setting the common function pointers!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//What to Do here??
	}

	rv = initHILibs(szUniqueProcessorArray);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failure while initializing Host Interface Libraries!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//What to Do here??
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processReportCommand
 *
 * Description	: This function process the Report
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processReportCommand(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/*
		 * Praveen_P1: Currently under assumption that we have single host
		 * thats why checking only first index which contains pointer
		 * for the host interface library
		 */
		if(stHostLibFuncPointers.fnpRepCmdArray[0] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Host is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return HOST_NOT_CONFIGURED;
		}

		rv = stHostLibFuncPointers.fnpRepCmdArray[0](pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Report Command [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Process %s Report Command, Return Value[%d]", pstSSIReqResFields[eCOMMAND].elementValue, rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Processed Report Command", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processDeviceRegistration
 *
 * Description	: This function process the Device Registration
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processDeviceRegistration(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/*
		 * Praveen_P1: Currently under assumption that we have single host
		 * thats why checking only first index which contains pointer
		 * for the host interface library
		 */
		if(stHostLibFuncPointers.fnpDevRegArray[0] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Host is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return HOST_NOT_CONFIGURED;
		}

		rv = stHostLibFuncPointers.fnpDevRegArray[0](pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Device Registration [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Do Device Registration, Return Value[%d]", rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Processed Device Registration", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processTokenQueryCommand
 *
 * Description	: This function process the Version Command and fill the corresponding
 * 				  data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processTokenQueryCommand(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/*
		 * Praveen_P1: Currently under assumption that we have single host
		 * thats why checking only first index which contains pointer
		 * for the host interface library
		 */
		if(stHostLibFuncPointers.fnpTknQryCmdArray[0] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Host is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return HOST_NOT_CONFIGURED;
		}

		rv = stHostLibFuncPointers.fnpTknQryCmdArray[0](pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Token Query command [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed To Do Token Query Command [%d]", rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Processed Token Query Command", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: processCardRateCommand
 *
 * Description	: This function process the Card Rate Command and fill the corresponding
 * 				  data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processCardRateCommand(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(stHostLibFuncPointers.fnpCardRateCmdArray[0] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Host is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return HOST_NOT_CONFIGURED;
		}

		rv = stHostLibFuncPointers.fnpCardRateCmdArray[0](pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Card Rate command [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed To Do Card Rate Command");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Processed Card Rate Command", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkHostConnection
 *
 * Description	: This function checks the host connection status
 *
 *
 * Input Params	: None
 *
 * Output Params: NONE
 * ============================================================================
 */
void checkHostConnection(HTTPINFO_PTYPE pstHTTPInfo)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/*
		 * Praveen_P1: Currently under assumption that we have single host
		 * thats why checking only first index which contains pointer
		 * for the host interface library
		 */
		if(stHostLibFuncPointers.fnpCheckHostConnection == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Credit Processor is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Credit Processor is Not Configured");
				strcpy(szAppLogDiag, "Please Configure Credit Processor");
				addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			pstHTTPInfo->iStatusCode = HTTP_STATUS_SERVER_UNAVAILABLE;

			break;

		}

		rv = stHostLibFuncPointers.fnpCheckHostConnection(NULL);
		if(rv == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Host Connection is Available", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Host Connection is Available");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			pstHTTPInfo->iStatusCode = HTTP_STATUS_OK;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Host Connection is NOT Available", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Host Connection is Not Available");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			pstHTTPInfo->iStatusCode = HTTP_STATUS_SERVER_UNAVAILABLE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

#if 0
/*
 * ============================================================================
 * Function Name: processTokenQueryCommand
 *
 * Description	: This function process the Credit Transaction
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processTokenQueryCommand(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{

		if(pstSSIReqResFields[eHIT_HOST].elementValue != NULL)
		{
			if(strcmp(pstSSIReqResFields[eHIT_HOST].elementValue, "1") == 0)
			{
				rv = updateLastTranDtls(pstSSIReqResFields);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating Last Transaction Details to a file [%d]", __FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif



/*
 * ============================================================================
 * Function Name: getHILibDtls
 *
 * Description	: This function gets the Host Interface Library Version and Name
 *
 *
 * Input Params	: Buffers to be filled
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getHILibDtls(char *pszHILibVerion, char *pszHIName)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		/*
		 * Praveen_P1: Currently under assumption that we have single host
		 * thats why checking only first index which contains pointer
		 * for the host interface library
		 */
		if(stHostLibFuncPointers.fnpGetVerArray[0] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Host is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return HOST_NOT_CONFIGURED;
		}

		stHostLibFuncPointers.fnpGetVerArray[0](pszHILibVerion, pszHIName);

		debug_sprintf(szDbgMsg, "%s: Successfully Fetched HI Version Details- Version [%s], Name[%s]", __FUNCTION__, pszHILibVerion, pszHIName);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processCreditTransaction
 *
 * Description	: This function process the Credit Transaction
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processCreditTransaction(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(stHostLibFuncPointers.fnpProcessCreditRequest == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Credit is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Credit Processor is Not Configured");
				strcpy(szAppLogDiag, "Please Configure Credit Processor");
				addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			return PAYMENT_TYPE_NOT_SUPPORTED;
		}

		rv = stHostLibFuncPointers.fnpProcessCreditRequest(pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Credit Transaction [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error While Processing %s Credit Transaction, Return Value[%d]", pstSSIReqResFields[eCOMMAND].elementValue, rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Processed Credit Transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(pstSSIReqResFields[eHIT_HOST].elementValue != NULL)
		{
			if(strcmp(pstSSIReqResFields[eHIT_HOST].elementValue, "1") == 0)
			{
				rv = updateLastTranDtls(pstSSIReqResFields);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating Last Transaction Details to a file [%d]", __FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processDebitTransaction
 *
 * Description	: This function process the Debit Transaction
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processDebitTransaction(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(stHostLibFuncPointers.fnpProcessDebitRequest == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Debit is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Debit Processor is Not Configured");
				strcpy(szAppLogDiag, "Please Configure Debit Processor");
				addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			return PAYMENT_TYPE_NOT_SUPPORTED;
		}

		rv = stHostLibFuncPointers.fnpProcessDebitRequest(pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Debit Transaction [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error While Processing %s Debit Transaction, Return Value[%d]", pstSSIReqResFields[eCOMMAND].elementValue, rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Processed Debit Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(pstSSIReqResFields[eHIT_HOST].elementValue != NULL)
		{
			if(strcmp(pstSSIReqResFields[eHIT_HOST].elementValue, "1") == 0)
			{
				rv = updateLastTranDtls(pstSSIReqResFields);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating Last Transaction Details to a file [%d]", __FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processGiftTransaction
 *
 * Description	: This function process the Gift Transaction
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processGiftTransaction(TRANDTLS_PTYPE pstSSIReqResFields)
{
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	int		iAppLogEnabled		= 0;
	int 	rv 					= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(stHostLibFuncPointers.fnpProcessGiftRequest == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Gift is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Gift Processor is Not Configured");
				strcpy(szAppLogDiag, "Please Configure Gift Processor");
				addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			return PAYMENT_TYPE_NOT_SUPPORTED;
		}

		rv = stHostLibFuncPointers.fnpProcessGiftRequest(pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Gift Transaction [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error While Processing %s Gift Transaction, Return Value[%d]", pstSSIReqResFields[eCOMMAND].elementValue, rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Processed Gift Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(pstSSIReqResFields[eHIT_HOST].elementValue != NULL)
		{
			if(strcmp(pstSSIReqResFields[eHIT_HOST].elementValue, "1") == 0)
			{
				rv = updateLastTranDtls(pstSSIReqResFields);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating Last Transaction Details to a file [%d]", __FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processPrivLblTransaction
 *
 * Description	: This function process the Private Label Transaction
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processPrivLblTransaction(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv 					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(stHostLibFuncPointers.fnpProcessPrivLblRequest == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Private Label is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Private Label Processor is Not Configured");
				strcpy(szAppLogDiag, "Please Configure Private Label Processor");
				addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			return PAYMENT_TYPE_NOT_SUPPORTED;
		}

		rv = stHostLibFuncPointers.fnpProcessPrivLblRequest(pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Private Label Transaction [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error While Processing %s Private Label Transaction, Return Value[%d]", pstSSIReqResFields[eCOMMAND].elementValue, rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Processed Private Label Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(pstSSIReqResFields[eHIT_HOST].elementValue != NULL)
		{
			if(strcmp(pstSSIReqResFields[eHIT_HOST].elementValue, "1") == 0)
			{
				rv = updateLastTranDtls(pstSSIReqResFields);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating Last Transaction Details to a file [%d]", __FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processCheckTransaction
 *
 * Description	: This function process the Private Label Transaction
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processCheckTransaction(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv				 	= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(stHostLibFuncPointers.fnpProcessCheckRequest == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Check is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Check Processor is Not Configured");
				strcpy(szAppLogDiag, "Please Configure Check Processor");
				addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			return PAYMENT_TYPE_NOT_SUPPORTED;
		}

		rv = stHostLibFuncPointers.fnpProcessCheckRequest(pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing Check Transaction [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error While Processing %s Check Transaction, Return Value[%d]", pstSSIReqResFields[eCOMMAND].elementValue, rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Processed Check Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(pstSSIReqResFields[eHIT_HOST].elementValue != NULL)
		{
			if(strcmp(pstSSIReqResFields[eHIT_HOST].elementValue, "1") == 0)
			{
				rv = updateLastTranDtls(pstSSIReqResFields);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating Last Transaction Details to a file [%d]", __FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processEBTTransaction
 *
 * Description	: This function process the EBT Transaction
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processEBTTransaction(TRANDTLS_PTYPE pstSSIReqResFields)
{
	int 	rv				 	= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(stHostLibFuncPointers.fnpProcessEBTRequest == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: EBT is not configured!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "EBT Processor is Not Configured");
				strcpy(szAppLogDiag, "Please Configure EBT Processor");
				addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			return PAYMENT_TYPE_NOT_SUPPORTED;
		}

		rv = stHostLibFuncPointers.fnpProcessEBTRequest(pstSSIReqResFields);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while processing EBT Transaction [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error While Processing %s EBT Transaction, Return Value[%d]", pstSSIReqResFields[eCOMMAND].elementValue, rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Processed EBT Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(pstSSIReqResFields[eHIT_HOST].elementValue != NULL)
		{
			if(strcmp(pstSSIReqResFields[eHIT_HOST].elementValue, "1") == 0)
			{
				rv = updateLastTranDtls(pstSSIReqResFields);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating Last Transaction Details to a file [%d]", __FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: setDynamicFunctionPtr
 *
 * Description	: This function sets the function pointer
 * 				  host interface library depends on the configuration
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int setDynamicFunctionPtr(char *pszLibName, char *pszFuncName, int (*pFunc)(void *))
{
	int			rv				= SUCCESS;
    void 		*pFunctionLib 	= NULL;   // Handle to shared lib file
    const char 	*pdlError;        // Pointer to error string

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    // Open Dynamic Loadable Libary using LD_LIBRARY path
    pFunctionLib = dlopen(pszLibName, RTLD_NOW);
    pdlError = dlerror();
    if (pdlError)
    {
    	debug_sprintf(szDbgMsg, "%s - unable to dynamically open %s!, error=[%s]",
                						__FUNCTION__, ((pszLibName) ? pszLibName : "application"), pdlError);
    	APP_TRACE(szDbgMsg);

        pFunc = NULL;
        rv = FAILURE;
    }
    else
    {
        if (pszLibName)
        {
            debug_sprintf(szDbgMsg, "%s - loaded [%s] module!", __FUNCTION__, pszLibName);
            APP_TRACE(szDbgMsg);
        }

        // Find requested function
        pFunc = dlsym(pFunctionLib, pszFuncName);
        pdlError = dlerror();
        if (pdlError)
        {
            debug_sprintf(szDbgMsg, "%s - unable to locate function [%s] in %s, error=[%s]",
                            __FUNCTION__, pszFuncName, ((pszLibName) ? pszLibName : "application"), pdlError);
            APP_TRACE(szDbgMsg);
            pFunc = NULL;
            rv = FAILURE;
        }
        else
        {
            debug_sprintf(szDbgMsg, "%s - obtained [%s] pointer=%p", __FUNCTION__, pszFuncName, pFunc);
            APP_TRACE(szDbgMsg);
        }
    }

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: getUniqueProcessorIds
 *
 * Description	: This function deletes the duplicate processors from the list
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void getUniqueProcessorIds(char szInputList[][10], char szOutputList[][10])
{
	int iIndex 			= 0;
	int iTempIndex 		= 0;
	int iOutputIndex 	= 0;
	int	iUnique		= 1;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iIndex = 0; iIndex < MAX_PROCESSOR_NUM; iIndex++)
	{
		iUnique = 1;

		memset(szOutputList[iIndex], 0x00, 10 * sizeof(char));

		if(strlen(szInputList[iIndex]) > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Processor [%s]", __FUNCTION__, szInputList[iIndex]);
			APP_TRACE(szDbgMsg);

			for(iTempIndex = 0; iTempIndex <= iIndex - 1; iTempIndex++)
			{
				if(strcmp(szInputList[iIndex], szInputList[iTempIndex]) == 0)
				{
					iUnique = 0;
					break;
				}
			}
			if(iUnique)
			{
				strcpy(szOutputList[iOutputIndex], szInputList[iIndex]);
				debug_sprintf(szDbgMsg, "%s: Its Unique Processor [%s]", __FUNCTION__, szOutputList[iOutputIndex]);
				APP_TRACE(szDbgMsg);
				iOutputIndex++;
			}
		}

	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return ;
}

/*
 * ============================================================================
 * Function Name: setCommonFuncPointers
 *
 * Description	: This function sets the function pointers for command functions
 * 				  like Init/Get Version
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int setCommonFuncPointers(char szProcessorList[][10])
{
	int			rv				= SUCCESS;
	int			iIndex			= 0;
	int			iAppLogEnabled	= 0;
	char 		szLibName[100]	= "";
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	for(iIndex = 0; iIndex < MAX_PROCESSOR_NUM; iIndex++)
	{
		if(strlen(szProcessorList[iIndex]) > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Processor Name [%s]", __FUNCTION__, szProcessorList[iIndex]);
			APP_TRACE(szDbgMsg);

			memset(szLibName, 0x00, sizeof(szLibName));
#ifdef DEBUG
			sprintf(szLibName, "libps%shiD.so", szProcessorList[iIndex]);
#else
			sprintf(szLibName, "libps%shi.so", szProcessorList[iIndex]);
#endif
			debug_sprintf(szDbgMsg, "%s: Processor library path [%s]", __FUNCTION__, szLibName);
			APP_TRACE(szDbgMsg);

			rv = setDynamicFunctionPtr(szLibName, INIT_LIB_FUNC_NAME, &stHostLibFuncPointers.fnpInitHIArray[iIndex]);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, INIT_LIB_FUNC_NAME, szLibName);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", CHECK_TRAN_FUNC_NAME, szLibName);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				//What to do here!!!
			}

			rv = setDynamicFunctionPtr_2(szLibName, GET_VER_FUNC_NAME, &stHostLibFuncPointers.fnpGetVerArray[iIndex]);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, GET_VER_FUNC_NAME, szLibName);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", GET_VER_FUNC_NAME, szLibName);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				//What to do here!!!
			}

			rv = setDynamicFunctionPtr(szLibName, DEVICE_REG_FUNC_NAME, &stHostLibFuncPointers.fnpDevRegArray[iIndex]);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, DEVICE_REG_FUNC_NAME, szLibName);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", DEVICE_REG_FUNC_NAME, szLibName);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				//What to do here!!!
			}

			rv = setDynamicFunctionPtr(szLibName, REPORT_CMD_FUNC_NAME, &stHostLibFuncPointers.fnpRepCmdArray[iIndex]);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, DEVICE_REG_FUNC_NAME, szLibName);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", DEVICE_REG_FUNC_NAME, szLibName);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				//What to do here!!!
			}

			rv = setDynamicFunctionPtr(szLibName, TOKEN_QUERY_FUNC_NAME, &stHostLibFuncPointers.fnpTknQryCmdArray[iIndex]);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, TOKEN_QUERY_FUNC_NAME, szLibName);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", TOKEN_QUERY_FUNC_NAME, szLibName);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				//What to do here!!!
			}

			rv = setDynamicFunctionPtr(szLibName, CARD_RATE_FUNC_NAME, &stHostLibFuncPointers.fnpCardRateCmdArray[iIndex]);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to set the function [%s] in [%s] library", __FUNCTION__, CARD_RATE_FUNC_NAME, szLibName);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Unable To Set Function [%s] in [%s] Library", CARD_RATE_FUNC_NAME, szLibName);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				//What to do here!!!
			}

		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: initHILibs
 *
 * Description	: This function initializes the configured host interface libraries
 *
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int initHILibs(char szProcessorList[][10])
{
	int			rv				= SUCCESS;
	int			iIndex			= 0;
	int			iAppLogEnabled	= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	for(iIndex = 0; iIndex < MAX_PROCESSOR_NUM; iIndex++)
	{
		if(stHostLibFuncPointers.fnpInitHIArray[iIndex] != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Initializing [%s] host interface library", __FUNCTION__, szProcessorList[iIndex]);
			APP_TRACE(szDbgMsg);

			rv = stHostLibFuncPointers.fnpInitHIArray[iIndex](NULL);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Initializing [%s] host interface library", __FUNCTION__, szProcessorList[iIndex]);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Initializing [%s] Host Interface Library", szProcessorList[iIndex]);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}

				//What to To here??
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Initialized [%s] host interface library", __FUNCTION__, szProcessorList[iIndex]);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Successfully Initialized [%s] Host Interface Library", szProcessorList[iIndex]);
					addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
			}

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No Init function for [%s] processor", __FUNCTION__, szProcessorList[iIndex]);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return rv;
}
