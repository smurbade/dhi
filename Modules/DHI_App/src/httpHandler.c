/******************************************************************
*                      httpHandler.c                              *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis related to reading of            *
*              http request                                       *
* 			                                                      *
*                                                                 *
* Created on: 14-Jul-2014                                         *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "DHI_App/common.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/externfunc.h"

/* Static Function Declarations */
static int  readHTTPHeaders(int , HTTPINFO_PTYPE );
static void readLine(int , char* );
static int parseHTTPHeader(char *, HTTPINFO_PTYPE );
static int readHTTPBody(int , HTTPINFO_PTYPE );
static ssize_t readn(int fd, void *vptr, size_t n);/* Read "n" bytes from a descriptor. */

#define MAX_LINE_TO_RECEIVE 1024

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: readHTTPRequest
 *
 * Description	: This api would read the coming HTTP request from the given FD
 *
 * Input Params	: Client FD
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int readHTTPRequest(int iClientFD, HTTPINFO_PTYPE pstHTTPInfo)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		rv = readHTTPHeaders(iClientFD, pstHTTPInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while reading HTTP headers!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error While Reading HTTP Headers");
				strcpy(szAppLogDiag, "Please Check Connection Parameters of SCA and DHI");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, szAppLogDiag);
			}
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Successfully Read HTTP Header");
			addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_RECEIVE, szAppLogData, NULL);
		}

		if((pstHTTPInfo->iStatusCode != HTTP_STATUS_OK) || (pstHTTPInfo->iReqLength <= 0))
		{
			debug_sprintf(szDbgMsg, "%s: Error in the request!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(pstHTTPInfo->iReqLength <= 0)
			{
				if(pstHTTPInfo->iMethod == HEAD_METHOD)
				{
					debug_sprintf(szDbgMsg, "%s: Head Method in the HTTP Request, Check Host Connection Request", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Head Method in HTTP Request, Check Host Connection Request");
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RECEIVE, szAppLogData, NULL);
					}
				}
				else
				{
					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Request Length is Not Present in the HTTP Request");
						strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_RECEIVE, szAppLogData, szAppLogDiag);
					}
				}
#if 0
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Length Not Mentioned in the HTTP Request, ");
					strcpy(szAppLogDiag, "Check Connection Request");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RECEIVE, szAppLogData, szAppLogDiag);
				}
#endif
			}
			else
			{
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Status Code[%d] is Not OK in the HTTP Request", pstHTTPInfo->iStatusCode);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_RECEIVE, szAppLogData, szAppLogDiag);
				}
			}

			rv = FAILURE;
			break;
		}

		rv = readHTTPBody(iClientFD, pstHTTPInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to read HTTP body!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error While Reading HTTP Body");
				strcpy(szAppLogDiag, "Please Check Connection Parameters of SCA and DHI");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_RECEIVE, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Successfully Read HTTP Body");
			addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_RECEIVE, szAppLogData, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: sendHTTPErrorResponse
 *
 * Description	: This api would send the HTTP error response to the given FD
 *
 * Input Params	: Client FD
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendHTTPErrorResponse(int iClientFD, HTTPINFO_PTYPE pstHTTPInfo)
{
	int		rv					= SUCCESS;
	int		iWritten			= 0;
	int		iAppLogEnabled		= 0;
	char	szMessage[256] 		= "";
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(pstHTTPInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Status Code [%d] ", __FUNCTION__, pstHTTPInfo->iStatusCode);
		APP_TRACE(szDbgMsg);

		memset(szMessage, 0x00, sizeof(szMessage));
		sprintf(szMessage,"HTTP/1.1 %d OK\r\n"
					"\r\n", pstHTTPInfo->iStatusCode);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Error Response [%s]",DEVDEBUGMSG,__FUNCTION__, szMessage);
		APP_TRACE(szDbgMsg);
#endif

		debug_sprintf(szDbgMsg, "%s: Error Response prepared", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, szMessage, NULL);
		}

		iWritten = write(iClientFD, szMessage, strlen(szMessage));

		debug_sprintf(szDbgMsg, "%s: Written %d bytes", __FUNCTION__, iWritten);
		APP_TRACE(szDbgMsg);

		if ( iWritten < 0)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! write error", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//TODO What to do here??
			rv = FAILURE;
			break;
		}

		if(iWritten != strlen(szMessage))
		{
			debug_sprintf(szDbgMsg, "%s: Could not written all bytes!!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Could Not Write Whole Message to the Client Socket, Written Bytes[%d] Out of Total Bytes[%d]", iWritten, strlen(szMessage));
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			//TODO What to do here??
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Written successfully response to the socket", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Successfully Written Response Message to the Client Socket");
				addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_SENT, szAppLogData, szAppLogDiag);
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendHTTPSuccessResponse
 *
 * Description	: This api would send the HTTP success response to the given FD
 *
 * Input Params	: Client FD
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendHTTPSuccessResponse(int iClientFD, HTTPINFO_PTYPE pstHTTPInfo)
{
	int		rv			  		= SUCCESS;
	char	szHeader[256] 		= "";
	char 	*pszMessage	  		= NULL;
	int		iTotalMsgLen  		= 0;
	int		iWritten	  		= 0;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(pstHTTPInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		/* Preparing the Header */
		memset(szHeader, 0x00, sizeof(szHeader));
		sprintf(szHeader, "HTTP/1.1 %d OK\r\n"
					"Content-Type: text/xml\r\n"
					"Content-Length: %d\r\n"
					"\r\n", pstHTTPInfo->iStatusCode, pstHTTPInfo->iResLength);
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Header = [%s]",DEVDEBUGMSG,__FUNCTION__, szHeader);
		APP_TRACE(szDbgMsg);
#endif

		iTotalMsgLen = strlen(szHeader) + pstHTTPInfo->iResLength;

		pszMessage = (char *)malloc(sizeof(char) * iTotalMsgLen + 1);
		if(pszMessage == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc error for Message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Malloc Failed For Response Message");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}
		memset(pszMessage, 0x00, iTotalMsgLen + 1);

		/* Add the header */
		strncpy(pszMessage, szHeader, strlen(szHeader));

		/* Add the body */
		strncat(pszMessage, pstHTTPInfo->pszResponse, pstHTTPInfo->iResLength);

#ifdef DEVDEBUG
		if(strlen(pszMessage) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s:%s: Success Response [%s]",DEVDEBUGMSG,__FUNCTION__, pszMessage);
			APP_TRACE(szDbgMsg);
		}
#endif
		debug_sprintf(szDbgMsg, "%s: Success Response prepared", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iWritten = write(iClientFD, pszMessage, iTotalMsgLen);

		debug_sprintf(szDbgMsg, "%s: Written %d bytes", __FUNCTION__, iWritten);
		APP_TRACE(szDbgMsg);

		if ( iWritten < 0)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! write error", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error While Writing Response Message to the Client Scoket");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			//TODO What to do here??
			rv = FAILURE;
			break;
		}

		if(iWritten != iTotalMsgLen)
		{
			debug_sprintf(szDbgMsg, "%s: Could not written all bytes!!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Could Not Write Whole Message to the Client Socket, Written Bytes[%d] Out of Total Bytes[%d]", iWritten, iTotalMsgLen);
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			//TODO What to do here??
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Written response to the socket successfully", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Successfully Written Response Message to the Client Socket");
				addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_SENT, szAppLogData, szAppLogDiag);
			}
		}

		break;
	}

	if(pszMessage != NULL)
	{
		free(pszMessage);
		pszMessage = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: readHTTPHeaders
 *
 * Description	: This api would read the HTTP headers
 *
 * Input Params	: Client FD
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int readHTTPHeaders(int iClientFD, HTTPINFO_PTYPE pstHTTPInfo)
{
	int		rv				= SUCCESS;
	char	szHeaderLine[256]   = "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Sample HTTP Header
		 * POST /enlighten/calais.asmx HTTP/1.1\r\n
		 * Content-Type: text/xml; charset=utf-8\r\n
         * Content-Length: length\r\n
		 * \r\n
		 */
		memset(szHeaderLine, 0x00, sizeof(szHeaderLine));
		readLine(iClientFD, szHeaderLine);

		debug_sprintf(szDbgMsg, "%s: Read Line [%s] ", __FUNCTION__, szHeaderLine);
		APP_TRACE(szDbgMsg);

		if(szHeaderLine[0] == '\0' || szHeaderLine[0] == '\r')
		{
			debug_sprintf(szDbgMsg, "%s: No more lines for HTTP headers ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		rv = parseHTTPHeader(szHeaderLine, pstHTTPInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing HTTP Header!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			continue; //Read next line of the header
		}

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: readHTTPBody
 *
 * Description	: This api would read the HTTP body
 *
 * Input Params	: Client FD
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int readHTTPBody(int iClientFD, HTTPINFO_PTYPE pstHTTPInfo)
{
	int		rv								= SUCCESS;
	int		iRead							= 0;
	int		iAppLogEnabled					= 0;
	char	szAppLogData[256]				= "";
	char	szAppLogDiag[256]				= "";

#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(pstHTTPInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input param is NULL!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Allocate memory for storing the request */
		pstHTTPInfo->pszRequest = (char *)malloc(sizeof(char) * (pstHTTPInfo->iReqLength + 1));
		if(pstHTTPInfo->pszRequest == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while creating memory for XML request!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error, Memory Allocation Failed For HTTP Request");
				strcpy(szAppLogDiag, "Please Check Connection Parameters of SCA and DHI");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_RECEIVE, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		iRead = pstHTTPInfo->iReqLength;
		memset(pstHTTPInfo->pszRequest, 0x00, iRead + 1);

		readn(iClientFD, pstHTTPInfo->pszRequest, iRead);

		if(iAppLogEnabled) //Logging the Request
		{
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_REQUEST, pstHTTPInfo->pszRequest, NULL);
		}

#ifdef DEVDEBUG
		if(strlen(pstHTTPInfo->pszRequest) < 4000)
		{

				debug_sprintf(szDbgMsg, "%s:%s: SSI Request [%s]",DEVDEBUGMSG, __FUNCTION__, pstHTTPInfo->pszRequest);
				APP_TRACE(szDbgMsg);
		}
#else
		debug_sprintf(szDbgMsg, "%s: Read the SSI Request", __FUNCTION__);
		APP_TRACE(szDbgMsg);
#endif
		break;//breaking from the outer while loop
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseHTTPHeader
 *
 * Description	: This api would parse the HTTP header
 *
 * Input Params	: Buffer contains header
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int parseHTTPHeader(char *pszHeaderLine, HTTPINFO_PTYPE pstHTTPInfo)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	char	szTemp[20]			= "";
	char	*pszTemp			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(pszHeaderLine == NULL || pstHTTPInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input Param is NULL ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Received HTTP Header Line [%s]", __FUNCTION__, pszHeaderLine);
		APP_TRACE(szDbgMsg);

		if(pstHTTPInfo->iMethod == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Did not set the Method from header, so it should be first header ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if (!strncmp(pszHeaderLine, "POST ", 5)) //Currently supporting only POST method
			{
				pstHTTPInfo->iMethod = POST_METHOD;
				pstHTTPInfo->iStatusCode = HTTP_STATUS_OK;
				pszHeaderLine+=5;
			}
			else if (!strncmp(pszHeaderLine, "HEAD ", 5)) //Currently supporting only POST method
			{
				pstHTTPInfo->iMethod = HEAD_METHOD;
				pstHTTPInfo->iStatusCode = HTTP_STATUS_OK;
				pszHeaderLine+=5;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Unsupported method in the HTTP request ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pstHTTPInfo->iStatusCode = HTTP_STATUS_METHOD_NOT_ALLOWED;
				pstHTTPInfo->iMethod = UNSUPPORTED_METHOD;

				if(iAppLogEnabled)
				{
					memset(szTemp, 0x00, sizeof(szTemp));
					strncpy(szTemp, pszHeaderLine, 5);
					sprintf(szAppLogData, "Unsupported Method[%s] in the HTTP Request", szTemp);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}

			if (strncmp(pszHeaderLine, "/ ", 2))
			{
				debug_sprintf(szDbgMsg, "%s: HTTP not found in HTTP request ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pstHTTPInfo->iStatusCode = HTTP_STATUS_NOT_FOUND;
				break;
			}

			pszHeaderLine += 2;

			if (strncmp(pszHeaderLine, "HTTP/1.1", 8)) //Currently we are supporting 1.1
			{
				debug_sprintf(szDbgMsg, "%s: Unsupported version in the HTTP request ", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				pstHTTPInfo->iStatusCode = HTTP_STATUS_VERSION_NOT_SUPPORTED;

				if(iAppLogEnabled)
				{
					memset(szTemp, 0x00, sizeof(szTemp));
					strncpy(szTemp, pszHeaderLine, 8);
					sprintf(szAppLogData, "Unsupported HTTP Version[%s] in the HTTP Request", szTemp);
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Not the first header in HTTP request ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if((pstHTTPInfo->iMethod == POST_METHOD || pstHTTPInfo->iMethod == HEAD_METHOD) && pstHTTPInfo->iStatusCode == HTTP_STATUS_OK)
			{
				if(!strncasecmp(pszHeaderLine, "CONTENT-LENGTH", 14))
				{
					debug_sprintf(szDbgMsg, "%s: Header contains Content Length, parsing for it ", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pszTemp = strchr(pszHeaderLine, ':');
					if(pszTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Did not find : in the HTTP request ", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pstHTTPInfo->iStatusCode = HTTP_STATUS_BAD_REQUEST;
						break;
					}
					pszTemp++;
					if(*pszTemp == ' ')
					{
						pszTemp++;
					}
					//Now pszTemp must be pointing to header value
					if(strlen(pszTemp) <= 0)
					{
						debug_sprintf(szDbgMsg, "%s: No value for Content Length in the HTTP request ", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "HTTP Content Length is Not Found in the HTTP Request");
							strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
						}

						pstHTTPInfo->iStatusCode = HTTP_STATUS_BAD_REQUEST;
						break;
					}
					debug_sprintf(szDbgMsg, "%s: pszTemp[%s] ", __FUNCTION__, pszTemp);
					APP_TRACE(szDbgMsg);

					pstHTTPInfo->iReqLength = atoi(pszTemp);

					debug_sprintf(szDbgMsg, "%s: HTTP request length [%d] ", __FUNCTION__, pstHTTPInfo->iReqLength);
					APP_TRACE(szDbgMsg);

					if(pstHTTPInfo->iReqLength <= 0)
					{
						debug_sprintf(szDbgMsg, "%s: Length value required in the HTTP request ", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "HTTP Content Length is Not Found in the HTTP Request");
							strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
						}

						pstHTTPInfo->iStatusCode = HTTP_STATUS_LENGTH_REQUIRED;
						break;
					}

				}
				else if(!strncasecmp(pszHeaderLine, "CONTENT-TYPE", 12))
				{
					debug_sprintf(szDbgMsg, "%s: Header contains Content Type, parsing for it ", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pszTemp = strchr(pszHeaderLine, ':');
					if(pszTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Did not find : in the HTTP request ", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pstHTTPInfo->iStatusCode = HTTP_STATUS_BAD_REQUEST;
						break;
					}
					pszTemp++;
					if(*pszTemp == ' ')
					{
						pszTemp++;
					}
					//Now pszTemp must be pointing to header value
					if(strlen(pszTemp) <= 0)
					{
						debug_sprintf(szDbgMsg, "%s: No value for Content Type in the HTTP request ", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pstHTTPInfo->iStatusCode = HTTP_STATUS_BAD_REQUEST;
						break;
					}
					debug_sprintf(szDbgMsg, "%s: pszTemp[%s] ", __FUNCTION__, pszTemp);
					APP_TRACE(szDbgMsg);

					strncpy(pstHTTPInfo->szContentType, pszTemp, strlen(pszTemp));

					debug_sprintf(szDbgMsg, "%s: HTTP request content type [%s] ", __FUNCTION__, pstHTTPInfo->szContentType);
					APP_TRACE(szDbgMsg);

					if (strncmp(pstHTTPInfo->szContentType, "text/xml", 8))
					{
						debug_sprintf(szDbgMsg, "%s: Un supported content type in the HTTP request ", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Unsupported[%s] Content Type in the HTTP Request", pstHTTPInfo->szContentType);
							strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
						}

						pstHTTPInfo->iStatusCode = HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE;
						break;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Not Content Length/Type Header ", __FUNCTION__);
					APP_TRACE(szDbgMsg); //TO DO Should we parse for these headers?
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Method and Status error in HTTP request, not parsing other headers", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: readn
 *
 * Description	: Standard readn function which can be used to read n bytes
 *
 * Input Params	: Client FD, buffer to fill
 *
 * Output Params: NONE
 * ============================================================================
 */
static ssize_t						/* Read "n" bytes from a descriptor. */
readn(int fd, void *vptr, size_t n)
{
	size_t	nleft;
	ssize_t	nread;
	char	*ptr;

	ptr = vptr;
	nleft = n;
	while (nleft > 0) {
		if ( (nread = read(fd, ptr, nleft)) < 0) {
			if (errno == EINTR)
				nread = 0;		/* and call read() again */
			else
				return(-1);

		} else if (nread == 0)
			break;				/* EOF */

		nleft -= nread;
		ptr   += nread;
	}
	return(n - nleft);		/* return >= 0 */
}

/*
 * ============================================================================
 * Function Name: readLine
 *
 * Description	: This api would read the line on the socket
 *
 * Input Params	: Client FD, buffer to fill
 *
 * Output Params: NONE
 * ============================================================================
 */
static void readLine(int iClientFD, char* pszBuffer)
{
	int  rv 		= 0;
	//int	 iCount 	= 0;
	char *p  		= NULL;;
	char cBuffer;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	p = pszBuffer;
	if(p == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Param passed!!! ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

	while(1)
	{
		if ( (rv = read(iClientFD, &cBuffer, 1)) == 1 )
		{
			*p++ = cBuffer;
			if (cBuffer == '\n')
			{
				break;
			}
		}
		else
		{
			perror("read");
		}
	}
#if 0
	while(1)
	{
		if ( (rv = read(iClientFD, &cBuffer, 1)) == 1 )
		{
			*p++ = cBuffer;
			if (cBuffer == '\n')
			{
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while reading on the socket!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iCount == 20)
			{
				break;
			}
			iCount++;
			//TODO What to do here
			//break;
		}
	}
#endif
	*p = 0;
}

