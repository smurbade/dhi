/*
 * =============================================================================
 * Filename    : main.c
 *
 * Application : Mx Direct Host Interface(DHI)
 *
 * Description : This file contains the code for the beginning point of the
 * 					application.
 *
 * Modification History:
 *
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           VFI
 *  Created on: 09-Jul-2014
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved.
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>

#include <svc.h>

#include "DHI_App/common.h"
#include "DHI_App/httpHandler.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/externfunc.h"
#include "DHI_App/configusr1applib.h"

/* Static Functions */
#ifdef DEBUG
static int giSaveDebugLogs 		= 0;
static int giSaveMemDebugLogs   = 0;
static void sigHandler(int);
static void setupSigHandlers();
#endif


/*
 * ============================================================================
 * Function Name: main
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int main(int argc, char ** argv)
{
	int		rv					= SUCCESS;
	char	szErrMsg[256]   	= "";
	int		iListenerFD			= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	int		iAppLogEnabled		= 0;

#ifdef DEBUG
	int		iDbgParmsSet		= 0;
	char	szDbgMsg[256]	= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	/*
	 * ---------------------------------
	 * STEP 1: Initialize Configusr1 Lib
	 * ---------------------------------
	 */
	rv = initConfigUsr1AppLib();

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to initialize Config usr1 Lib",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to Initialize Config urs1 Lib");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

	}

	/*
	 * ---------------------------------
	 * STEP 2: Initialize the debugger.
	 * ---------------------------------
	 */
#ifdef DEBUG
	setupDebug(&giSaveDebugLogs);
	setupMemDebug(&giSaveMemDebugLogs);
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: App Ver:[%s]", __FUNCTION__, DHI_APP_VERSION);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: App Build Number:[%s]", __FUNCTION__, DHI_BUILD_NUMBER);
	APP_TRACE(szDbgMsg);

#ifdef DEBUG
	 //Setting Signal handlers for debugging purpose
	setupSigHandlers();
#endif

#ifdef DEBUG
	/*
	 * ------------------------------------
	 * STEP 3: Setting the Debug config params to environment.
	 * ------------------------------------
	 */
	rv = setDebugParamsToEnv(&iDbgParmsSet);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to set debug params to environment",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to set debug params to environment");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		while(1)
		{
			svcWait(10);
		}
	}
	else
	{
		if(iDbgParmsSet == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Debug parameters are reset to environment",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			giSaveDebugLogs = 0; //testing purpose
			giSaveMemDebugLogs = 0;

			/* Close the debug socket before updating with new debug env params */
			closeupDebug();
			closeupMemDebug();

			svcWait(3000);

			//Setting debug with new environment variables
			setupDebug(&giSaveDebugLogs);
			setupMemDebug(&giSaveMemDebugLogs);

			svcWait(3000);

			debug_sprintf(szDbgMsg, "%s: giSaveDebugLogs[%d] giSaveMemDebugLogs[%d]",__FUNCTION__, giSaveDebugLogs, giSaveMemDebugLogs);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Reseting DEBUG params to envinorment is not required",
										__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
#endif



	/*
	 * ---------------------------------
	 * STEP 4: Initialize App Logging
	 * ---------------------------------
	 */
	rv = initializeAppLog();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to initialize Application logging scheme",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to Initialize DHI Application Logging");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		while(1)
		{
			svcWait(10);
		}
	}

	iAppLogEnabled = isAppLogEnabled();

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, START_INDICATOR);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);

		sprintf(szAppLogData, "DHI Application Version:[%s], DHI Application Build Number:[%s]", DHI_APP_VERSION, DHI_BUILD_NUMBER);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 5: Read config parameters
	 * ---------------------------------
	 */
	rv = loadConfigParams();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to load config parameters",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to load DHI config parameters System");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Load DHI Config Parameters");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, NULL);

			strcpy(szAppLogData, "DHI Application Halted!!!");
			strcpy(szAppLogDiag, "Please Check The DHI Configuration");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Loaded DHI Application Configuration Parameters Successfully");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 6: Create Hash Table
	 * ---------------------------------
	 */
	rv = createHashTableForSSIFields();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create Hash Table for SSI Fields",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to create Hash Table for SSI Fields");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Create Hash Table");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, NULL);

			strcpy(szAppLogData, "DHI Application Halted!!!");
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Created Hash Table For SSI Fields Successfully");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * -------------------------------------
	 * STEP 7: Load Host Interface Libraries
	 * -------------------------------------
	 */
	rv = loadHostInterfaceLibs();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to load Host Interface Libraries",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to load Host Interface Libraries");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Load Host Interface Libraries");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, NULL);

			strcpy(szAppLogData, "DHI Application Halted!!!");
			strcpy(szAppLogDiag, "Please Check The DHI Configuration"); //TO_DO Please add more information here
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Loaded Host Interface Libraries Successfully");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 8: Create Thread Pool
	 * ---------------------------------
	 */
	rv = createThreadPool(3); //Currently creating pool of 3 threads
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create Thread Pool to service requests",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to create Thread Pool to service requests");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Create Thread Pool");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, NULL);

			strcpy(szAppLogData, "DHI Application Halted!!!");
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Created Thread Pool For Service Requests Successfully");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 9: Create DHI Server
	 * ---------------------------------
	 */
	rv = createDHIServer(&iListenerFD);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create DHI server",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to create DHI server");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Create DHI Server");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, NULL);

			strcpy(szAppLogData, "DHI Application Halted!!!");
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Created DHI Server Successfully, Started Listening For Connections");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 10: Create DHI Listener
	 * ---------------------------------
	 */
	rv = startListener(iListenerFD); /*Loop infinitely to accept and service connections/commands*/
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to start DHI listener",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to start DHI listener");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Create Listener To Accept Connections From SCA");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, NULL);

			strcpy(szAppLogData, "DHI Application Halted!!!");
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: dhiMalloc
 *
 * Description	: Wrapper for malloc, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *dhiMalloc(unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	void *		tmpPtr				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = malloc(iSize);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaMalloc", tmpPtr, iSize, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaMalloc");
		APP_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: dhistrdup
 *
 * Description	: Wrapper for strdup, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern char *dhistrdup(char *ptr , int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	char*		   		tmpPtr				= NULL;
	unsigned int        iSize 				= 0;

	iSize= strlen(ptr);

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = strdup(ptr);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaMalloc", tmpPtr, iSize, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaMalloc");
		APP_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: dhistrndup
 *
 * Description	: Wrapper for strndup, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern char *dhistrndup(char *ptr , unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	char *		tmpPtr				= NULL;
//	unsigned int         iSize 				= 0;

//	iSize= strlen(ptr);

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = strndup(ptr, iSize);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaMalloc", tmpPtr, iSize, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaMalloc");
		APP_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: dhiReAlloc
 *
 * Description	: Wrapper for realloc, will print num of bytes and starting
 * 					and original address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *dhiReAlloc(void *ptr, unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalReallocMem= 0;
	void *		tmpPtr				= NULL;
	char		szOrigAddr[16]		= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//Storing Original Address
	memset(szOrigAddr, 0x00, sizeof(szOrigAddr));
	sprintf(szOrigAddr, "%p", ptr);

	tmpPtr = realloc(ptr, iSize);

	if(tmpPtr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] OrigStartAddr[%s] NewStartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				"scaReAlloc", szOrigAddr, tmpPtr, iSize, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", "scaReAlloc");
		APP_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: dhiFree
 *
 * Description	: Wrapper for free, will print going to be free
 * 					address also and make the freed pointer points to NULL
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void dhiFree(void **pptr, int iLineNum, char * pszCallerFuncName)
{
	//size_t *sizePtr					= ((size_t *)ptr) - 1;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(*pptr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] FreedAddr[%p] called by [%s] from Line[%d]",
				"scaFree", *pptr, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
		free(*pptr);
		*pptr = NULL;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: [MEMDEBUG] NULL params Passed ", "scaFree");
		APP_MEM_TRACE(szDbgMsg);
	}
}


#ifdef DEBUG

static void catch_stop()
{
}

/*
 * ============================================================================
 * Function Name: setupSigHandlers
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void setupSigHandlers()
{
	char	szDbgMsg[256]	= "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Set up signal handlers */
	if(SIG_ERR == signal(SIGABRT, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGABRT",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGSEGV, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSEGV",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGILL, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGILL",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGKILL, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGKILL",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGSTOP, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSTOP",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGTERM, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSTOP",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}


/*
 * ============================================================================
 * Function Name: sigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void sigHandler(int iSigNo)
{
	char	szDbgMsg[256]	= "";
	struct 	sigaction 		setup_action;
	sigset_t block_mask;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pthread_t self = pthread_self();

	debug_sprintf(szDbgMsg, "%s: threadId=%lu", __FUNCTION__, (unsigned long)self);
	APP_TRACE(szDbgMsg);

	sigemptyset(&block_mask);

	sigaddset(&block_mask, SIGABRT);
	sigaddset(&block_mask, SIGSEGV);
	sigaddset(&block_mask, SIGILL);
	sigaddset(&block_mask, SIGPIPE);

	setup_action.sa_handler = catch_stop;
	setup_action.sa_mask = block_mask;
	setup_action.sa_flags = 0;
	sigaction(iSigNo, &setup_action, NULL);

	switch(iSigNo)
	{
	case SIGABRT:
		sprintf(szDbgMsg, "%s: SIGABRT signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGSEGV:
		sprintf(szDbgMsg, "%s: SIGSEGV signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGILL:
		sprintf(szDbgMsg, "%s: SIGILL signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGKILL:
		sprintf(szDbgMsg, "%s: SIGKILL signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGSTOP:
		sprintf(szDbgMsg, "%s: SIGSTOP signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGTERM:
		sprintf(szDbgMsg, "%s: SIGTERM signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		if(giSaveDebugLogs == 1 || giSaveMemDebugLogs == 1)
		{
			SaveDebugLogs();
		}

#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGPIPE:
		sprintf(szDbgMsg, "%s: SIGPIPE signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);
		break;

	default:
		sprintf(szDbgMsg, "%s: Signal no = [%d]", __FUNCTION__, iSigNo);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);
	}

	return;
}
#endif

/*
 * ============================================================================
 * End of file main.c
 * ============================================================================
 */
