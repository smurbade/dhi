/*
 * =============================================================================
 * Filename    : xmlutils.c
 *
 * Application : Mx Point SCA
 *
 * Description :
 *
 * Modification History:
 *
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved.
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "DHI_App/common.h"
#include "DHI_App/tranDef.h"
#include "DHI_App/xmlUtils.h"

static int addDataNode(DHI_VAL_LST_PTYPE, int, KEYVAL_PTYPE, int);
static int parseXMLForData(xmlDoc *, xmlNode *, KEYVAL_PTYPE, int);

/*
 * ============================================================================
 * Function Name: parseXMLForListElem
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int parseXMLForListElem(xmlDoc * docPtr, xmlNode * curNode,
								DHI_VARLST_INFO_PTYPE varLstPtr, DHI_VAL_LST_PTYPE * valNodesLst)
{
	int				rv					= SUCCESS;
	int				iCnt				= 0;
//	int				jCnt				= 0;
	int				iSize				= 0;
	int				iTotCnt				= 0;
	int				iElemRead			= 0;
	int				iTmpCnt				= 0;
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	xmlChar *		value				= NULL;
	xmlNode	*		tmpNode				= NULL;
	DHI_BOOL		bSingleXMLFieldsRead= DHI_FALSE;
	KEYVAL_PTYPE	keyLstPtr			= NULL;
	DHI_VAL_LST_PTYPE	valNodesLstPtr		= NULL;
	DHI_METADATA_STYPE	stLocMetaData;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	valNodesLstPtr = (DHI_VAL_LST_PTYPE) malloc(DHI_VAL_LST_SIZE);
	if(valNodesLstPtr == NULL)
	{
		/* If the memory is not allocated, return ERROR */
		debug_sprintf(szDbgMsg, "%s: Malloc failed for value nodes list ptr",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* Initialize the memory if allocated */
		memset(valNodesLstPtr, 0x00, DHI_VAL_LST_SIZE);

		*valNodesLst = valNodesLstPtr;
	}
	tmpNode = curNode;
	while( (tmpNode != NULL) )
	{
		if(!strcmp((char *)tmpNode->name, "text"))
		{
			tmpNode = tmpNode->next;
			continue;
		}
		else
		{
			iTmpCnt++;
			tmpNode = tmpNode->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Temp Text Node Cnt [%d]", __FUNCTION__, iTmpCnt);
	APP_TRACE(szDbgMsg);

	while( (rv == SUCCESS) && (curNode != NULL) )
	{
		if(curNode->type == 3)
		{
			curNode = curNode->next;
			continue;
		}

		while( (rv == SUCCESS) && (varLstPtr[iCnt].nodeType != -1) )
		{
			if( (((varLstPtr[iCnt].szNodeStr == NULL) && (xmlChildElementCount(curNode) <= 0)))||
				(!xmlStrcmp(curNode->name,
							(const xmlChar *)varLstPtr[iCnt].szNodeStr) )  )
			{

				if((varLstPtr[iCnt].szNodeStr == NULL) && (xmlChildElementCount(curNode) <= 0) && bSingleXMLFieldsRead == DHI_TRUE)
				{
					iTmpCnt--;
					iCnt++;
					continue;
				}

				/* Get the elements count */
				iTotCnt = varLstPtr[iCnt].iElemCnt;
				iSize = KEYVAL_SIZE * iTotCnt;
				//iMandCnt = 0;

				debug_sprintf(szDbgMsg, "%s: iTotCnt [%d], iSize [%d]", __FUNCTION__, iTotCnt, iSize);
				APP_TRACE(szDbgMsg);

				/* Allocate memory for the key list */
				keyLstPtr = (KEYVAL_PTYPE) malloc(iSize);
				if(keyLstPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed for key list",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				/* Copy the reference list here */
				memset(keyLstPtr, 0x00, iSize);
				memcpy(keyLstPtr, varLstPtr[iCnt].keyList, iSize);

				/* Hard coding the getting of attributes for now*/
				if(!xmlStrcmp(curNode->name, (const xmlChar *)"Application"))
				{
					value = xmlGetProp(curNode, (const xmlChar *) keyLstPtr[0].key);
					if(value != NULL)
					{
						keyLstPtr[0].value = strdup((char*)value);

						xmlFree(value);
					}
				}

				if(!xmlStrcmp(curNode->name, (const xmlChar *)"CapKey"))
				{
					value = xmlGetProp(curNode, (const xmlChar *) keyLstPtr[0].key);
					if(value != NULL)
					{
						keyLstPtr[0].value = strdup((char*)value);

						xmlFree(value);
					}

					value = xmlGetProp(curNode, (const xmlChar *) keyLstPtr[1].key);
					if(value != NULL)
					{
						keyLstPtr[1].value = strdup((char*)value);

						xmlFree(value);
					}
				}

//				/* Get the count of mandatory fields */
//				for(jCnt = 0; jCnt < iTotCnt; jCnt++)
//				{
//					if(keyLstPtr[jCnt].isMand == DHI_TRUE)
//					{
//						/* Increment the mandatory field count */
//						iMandCnt++;
//					}
//				}


				/* Parse the xml for the required data */
				if(varLstPtr[iCnt].szNodeStr == NULL && (xmlChildElementCount(curNode) <= 0))
				{
					iElemRead = parseXMLForData(docPtr, curNode, keyLstPtr,
																	iTotCnt);
					bSingleXMLFieldsRead = DHI_TRUE;
				}
				else
				{
					iElemRead = parseXMLForData(docPtr, curNode->xmlChildrenNode,
															keyLstPtr, iTotCnt);
				}

				if(iElemRead < 0)
				{
					rv = iElemRead;
					break;
				}


				debug_sprintf(szDbgMsg, "%s: Node Type [%d] is getting added to value list ptr", __FUNCTION__, varLstPtr[iCnt].nodeType);
				APP_TRACE(szDbgMsg);

				/* Add the data in the linked list */
				rv = addDataNode(valNodesLstPtr, varLstPtr[iCnt].nodeType,
															keyLstPtr, iTotCnt);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Addition of data node FAILED",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				/* Daivik:3/12/2015 :Once we have successfully added the node to the list we can equate this to NULL so that
				 * the we free the pointer only if node was not added in the list and if there is some error that
				 * has occurred as per the fix which we have made outside the loop.
				 */
				keyLstPtr = NULL;

				//Decrement the counter after adding the node.
				iTmpCnt--;

				break;
			}

			iCnt++;

		}
		/* Daivik:3/12/2015 : Fix to ensure that we free the KeyLstPtr , that was locally created, but not added to the
		 * main meta data list because of some error.
		 */
		if(rv != SUCCESS && keyLstPtr)
		{
			stLocMetaData.iTotCnt = iTotCnt;
			stLocMetaData.keyValList = keyLstPtr;

			freeMetaData(&stLocMetaData);
		//	free(keyLstPtr);
		}
#if 0 //ArjunU1: Don't check this condition*/
		if(varLstPtr[iCnt].nodeType == -1)
		{
			/* None of the string matched with the element */
			debug_sprintf(szDbgMsg, "%s: [%s] is invalid node string",
					__FUNCTION__, curNode->name);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
		}
#endif
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Temp Text Node Cnt While Moving to next Node [%d]", __FUNCTION__, iTmpCnt);
			APP_TRACE(szDbgMsg);

			if(iTmpCnt <= 0 )
			{
				debug_sprintf(szDbgMsg, "%s: Element in varlist are over",
													__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			iCnt = 0; //ArjunU1: We must reset this for next iteration.
			/*Checking next node*/
			curNode = curNode->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXMLForData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXMLForData(xmlDoc * docPtr, xmlNode * curNode,
										KEYVAL_PTYPE keyValList, int keyCnt)
{
	int				rv				= SUCCESS;
	int				iRetVal			= 0;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iLen			= 0;
	char *			cPtr			= NULL;
	xmlChar *		value			= NULL;
	xmlNode *		nxtNode 		= NULL;
	DHI_BOOL		bMatched		= DHI_FALSE;
	DHI_BOOL		bAlreadyParsed	= DHI_FALSE;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	while( (iCnt < keyCnt) && (rv == SUCCESS) && (curNode != NULL))
	{
		if(curNode->type == 3) /* TEXT TYPE NODE */
		{
			/* No need for checking this node */
			break;
		}

		if(curNode->type == XML_ENTITY_REF_NODE)
		{
			debug_sprintf(szDbgMsg, "%s: Node is of type XML_ENTITY_REF_NODE", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(!xmlStrcmp(curNode->name, (const xmlChar *)keyValList[iCnt].key))
		{
			switch(keyValList[iCnt].valType)
			{
			case SINGLETON:
				/* Get the correct value according to the allowable types of
				 * numerics strings present in the message */
				value = xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					iLen = xmlStrlen(value);
					if(iLen > 0)
					{
						cPtr = (char *) malloc(iLen + 1);
						memset(cPtr, 0x00, iLen + 1);

						memcpy(cPtr, value, iLen);
						keyValList[iCnt].value = cPtr;
						cPtr = NULL;
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = DHI_TRUE;
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break; /* End of SINGLETON switch case */


			case VARLIST:

				if(curNode->xmlChildrenNode == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: No child node", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Parse xml and store the variable linked list of data */
					rv = parseXMLForListElem(docPtr, curNode->xmlChildrenNode,
							(DHI_VARLST_INFO_PTYPE) keyValList[iCnt].valMetaInfo,
							(DHI_VAL_LST_PTYPE *) &keyValList[iCnt].value);

					if(keyValList[iCnt].value)
					{
						debug_sprintf(szDbgMsg, "%s: Not NULL", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: NULL", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = DHI_TRUE;
					}
					jCnt++;

					bAlreadyParsed = DHI_TRUE;
				}

				break; /* End of VARLIST switch case */

			default:
				debug_sprintf(szDbgMsg, "%s: Undefined type = [%d]",
										__FUNCTION__, keyValList[iCnt].valType);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break; /* End of default switch case */

			} /* End of SWITCH */
		} /* End of if condition*/
		/* Deallocating any allocated memory not in use now */
		if(value != NULL)
		{
			xmlFree(value);
			value = NULL;
		}
		if(bMatched == DHI_TRUE)
		{
			/*Given key matched with XML node*/
			bMatched = DHI_FALSE;
			break;
		}
		if(rv == SUCCESS)
		{
			/*Checking Next Key*/
			iCnt++;
		}
	} /* End of WHILE loop */
	if(rv == SUCCESS && (curNode != NULL))
	{
//#if 0
		//AjayS2: We already have parsed the child node for VARLIST type data, we should not parse it again
		/* Go for the child Node */
		nxtNode = curNode->xmlChildrenNode;
		if(nxtNode && bAlreadyParsed == DHI_FALSE)
		{
			iRetVal = parseXMLForData(docPtr, nxtNode, keyValList, keyCnt);
			if(iRetVal >= 0)
			{
				jCnt += iRetVal;
			}
			else
			{
				jCnt = iRetVal;
			}
		}
//#endif
		if(jCnt >= 0)
		{
			/* go for the sibling node */
			nxtNode = curNode->next;
			if(nxtNode)
			{
				iRetVal = parseXMLForData(docPtr, nxtNode, keyValList, keyCnt);
				if(iRetVal >= 0)
				{
					jCnt += iRetVal;
				}
				else
				{
					jCnt = iRetVal;
				}
			}
		}
	}
	else
	{
		jCnt = rv;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, jCnt);
	//APP_TRACE(szDbgMsg);
	return jCnt;
}



/*
 * ============================================================================
 * Function Name: addDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addDataNode(DHI_VAL_LST_PTYPE valLstPtr, int iNodeType,
										KEYVAL_PTYPE listPtr, int iElemCnt)
{
	int					rv				= SUCCESS;
	DHI_VAL_NODE_PTYPE	tmpNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	tmpNodePtr = (DHI_VAL_NODE_PTYPE) malloc(sizeof(DHI_VAL_NODE_STYPE));
	if(tmpNodePtr != NULL)
	{
		/* Initialize the allocated memory */
		memset(tmpNodePtr, 0x00, sizeof(DHI_VAL_NODE_STYPE));
		tmpNodePtr->elemCnt = iElemCnt;
		tmpNodePtr->elemList = listPtr;
		tmpNodePtr->type = iNodeType;

		/* Add the node to the list */
		if(valLstPtr->end == NULL)
		{
			/* Adding the first element */
			valLstPtr->start = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

//			debug_sprintf(szDbgMsg, "%s: Adding first node", __FUNCTION__);
//			APP_TRACE(szDbgMsg);
		}
		else
		{
			/* Adding the nth element */
			valLstPtr->end->next = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

//			debug_sprintf(szDbgMsg, "%s: Adding some node", __FUNCTION__);
//			APP_TRACE(szDbgMsg);
		}
		/* Increment the node count */
		valLstPtr->valCnt += 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: freeMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int freeMetaData(DHI_METADATA_PTYPE metaDataPtr)
{
	int					rv				= SUCCESS;
	int					iCnt			= 0;
	void *				dataPtr			= NULL;
	DHI_VAL_LST_PTYPE	valLstPtr		= NULL;
	DHI_VAL_NODE_PTYPE	valNodePtr		= NULL;
	KEYVAL_PTYPE		listPtr			= NULL;
	DHI_METADATA_STYPE	stLocMetaData;
#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stLocMetaData, 0x00, DHI_METADATA_SIZE);

	listPtr = metaDataPtr->keyValList;

	if(listPtr == NULL)
	{
		//debug_sprintf(szDbgMsg, "%s: No list to free!!!", __FUNCTION__);
		//RCHI_LIB_TRACE(szDbgMsg);
		//memset(metaDataPtr, 0x00, METADATA_SIZE);

		return rv;
	}

	/* Clean all the values */
	for(iCnt = 0; iCnt < metaDataPtr->iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}

		switch(listPtr[iCnt].valType)
		{
		case SINGLETON:
		case AMOUNT:
		case NUMERICS:
		case MASK_NUM_STR:
		case BOOLEAN:
		case STRING:
		case LIST:
		case TIME:
		case DATE:
			free(listPtr[iCnt].value);
			listPtr[iCnt].value = NULL;

			break;

		case ATTR_LKDLST: /* VDR: FIX THIS */

			valNodePtr = (DHI_VAL_NODE_PTYPE) listPtr[iCnt].value;

			while(valNodePtr)
			{
				stLocMetaData.iTotCnt = valNodePtr->elemCnt;
				stLocMetaData.keyValList = valNodePtr->elemList;

				freeMetaData(&stLocMetaData);

				if(valNodePtr->szVal != NULL)
				{
					free(valNodePtr->szVal);
				}

				dataPtr = valNodePtr;
				valNodePtr = valNodePtr->next;

				free(dataPtr);
			}

			listPtr[iCnt].value = NULL;
			break;

		case VARLIST:
		case RECLIST:
			valLstPtr = (DHI_VAL_LST_PTYPE) listPtr[iCnt].value;

			valNodePtr = valLstPtr->start;
			while(valNodePtr)
			{
				stLocMetaData.iTotCnt = valNodePtr->elemCnt;
				stLocMetaData.keyValList = valNodePtr->elemList;

				freeMetaData(&stLocMetaData);

				if(valNodePtr->szVal != NULL)
				{
					free(valNodePtr->szVal);
				}

				/* DAIVIK:16/5/2016- The absence of the below free ,would cause memory leak when freeing the metadata.
				 * Required when we are freeing a list within another list.
				 */
				if(valNodePtr->elemList != NULL)
				{
					free(valNodePtr->elemList);
					valNodePtr->elemList = NULL;
				}

				dataPtr = valNodePtr;
				valNodePtr = valNodePtr->next;
				free(dataPtr);
			}

			free(valLstPtr);

			listPtr[iCnt].value = NULL;
			break;

		case NULL_ADD:
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: [%s]SHOULD NEVER COME HERE [%d]",
												__FUNCTION__, listPtr[iCnt].key,
												listPtr[iCnt].valType);
			APP_TRACE(szDbgMsg);

			free(listPtr[iCnt].value);
			listPtr[iCnt].value = NULL;

			break;
		}
	}

	memset(metaDataPtr, 0x00, DHI_METADATA_SIZE);

	return rv;
}
