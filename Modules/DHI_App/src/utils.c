/******************************************************************
*                      utils.c                                    *
*******************************************************************
* Application: DHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis which are useful/common          *
*              across the the DHI Application                     *
* 			                                                      *
*                                                                 *
* Created on: 09-Jul-2014                                         *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <svcsec.h>
#include <sys/timeb.h>	// For struct timeb
#include <sys/stat.h>
#include <svc.h>
#include <pthread.h>
#include <dlfcn.h>

#include "DHI_App/common.h"

#define DHI_DEBUG		"DHI_DEBUG"
#define DHI_MEM_DEBUG	"DHI_MEM_DEBUG"

#ifdef DEBUG
#define MAX_DEBUG_MSG_SIZE	15+4096+2+1
char chDebug = NO_DEBUG;
char chMemDebug = NO_DEBUG;
#endif

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		dhiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         dhistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  dhistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	dhiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			dhiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: acquireMutexLock
 *
 * Description	: This function is used to acquire a mutex for sychronization
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void acquireMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_lock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to acquire %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		APP_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: releaseMutexLock
 *
 * Description	: This function is used to release an already acquired mutex
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void releaseMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_unlock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to release %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		APP_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: getPANFromTrackData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void getPANFromTrackData(char *pszTrackData, int iTrackIndicator, char *pszPAN)
{
	char *	cCurPtr				= NULL;
	char *	cNxtPtr				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTrackData == NULL || pszPAN == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param is NULL!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

	cCurPtr = pszTrackData;

	if(iTrackIndicator == 1) //Track1 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving PAN from Track1", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			* have the '^' sentinel in their track 1 information */
			strncpy(pszPAN, cCurPtr, 29);
		}
		else
		{
			memcpy(pszPAN, cCurPtr, cNxtPtr - cCurPtr);
		}
	}
	else if(iTrackIndicator == 2) //Track 2 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving PAN from Track2", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		cNxtPtr = strchr(cCurPtr, '=');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '=' sentinel in their track 2 */
			strncpy(pszPAN, cCurPtr, 29);
		}
		else
		{
			memcpy(pszPAN, cCurPtr, cNxtPtr - cCurPtr);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid track to retrieve PAN!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: PAN [%s]", DEVDEBUGMSG, __FUNCTION__, pszPAN);
#else
	debug_sprintf(szDbgMsg,"%s: Retrived PAN from the Track Data",__FUNCTION__);
#endif
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getMaskedPAN
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void getMaskedPAN(char *pszClearPAN, char *pszMaskedPAN)
{
	int  iPANLength       		= 0;
	int  iFirstDigitsInClear    = 6;
	int	 iLastDigitsClear 		= 4;
	int  iDigitsToMask    		= 0;
	int	 iIndex				    = 0;
	int  iTempIndex				= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iPANLength = strlen(pszClearPAN);

	debug_sprintf(szDbgMsg, "%s: Length of the PAN is %d", __FUNCTION__, iPANLength);
	APP_TRACE(szDbgMsg);

	iDigitsToMask = iPANLength - iLastDigitsClear - iFirstDigitsInClear; //4 from end and excluding the number of clear digits

	if(iDigitsToMask < 1)
	{
		debug_sprintf(szDbgMsg, "%s: Adjusting the Digits to Mask", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iFirstDigitsInClear = iFirstDigitsInClear - 1;
		iLastDigitsClear    = iLastDigitsClear - 1;
		iDigitsToMask = iPANLength - iLastDigitsClear - iFirstDigitsInClear;
	}

	debug_sprintf(szDbgMsg, "%s: Number of Digits to Mask is %d", __FUNCTION__, iDigitsToMask);
	APP_TRACE(szDbgMsg);

	strncpy(pszMaskedPAN, pszClearPAN, iPANLength - iDigitsToMask - iLastDigitsClear);

	iIndex = strlen(pszMaskedPAN);

	for (iTempIndex = 1 ; iTempIndex <= iDigitsToMask; iTempIndex++)
	{
		pszMaskedPAN[iIndex] = '*';
		iIndex ++;
	}

	strcat(&pszMaskedPAN[iIndex], pszClearPAN + iFirstDigitsInClear + iDigitsToMask);

	debug_sprintf(szDbgMsg, "%s: Masked PAN is [%s]", __FUNCTION__, pszMaskedPAN);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: doesFileExist
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int doesFileExist(const char *szFullFileName)
{
	int			rv			= SUCCESS;
	struct stat	fileStatus;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	memset(&fileStatus, 0x00, sizeof(fileStatus));

	rv = stat(szFullFileName, &fileStatus);
	if(rv < 0)
	{
		if(errno == EACCES)
		{
			debug_sprintf(szDbgMsg, "%s: Not enough search permissions for %s",
												__FUNCTION__, szFullFileName);
		}
		else if (errno == ENAMETOOLONG)
		{
			debug_sprintf(szDbgMsg, "%s: Name %s is too long", __FUNCTION__,
																szFullFileName);
		}
		else
		{
			/* For ENOENT/ENOTDIR/ELOOP */
			debug_sprintf(szDbgMsg,
						"%s: File - [%s] (Name not correct)/(file doesnt exist)"
												, __FUNCTION__, szFullFileName);
		}
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		/* Check if the file is a regular file */
		if(S_ISREG(fileStatus.st_mode))
		{
			/* Check if the file is not empty */
			if(fileStatus.st_size <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: File [%s] is empty", __FUNCTION__,
																szFullFileName);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error: File [%s] is not a regular file"
												, __FUNCTION__, szFullFileName);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}

	return rv;
}

 /* Function Name: setDynamicFunctionPtr
 *
 * Description	: This function sets the function pointer
 * 				  host interface library depends on the configuration
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setDynamicFunctionPtr(char *pszLibName, char *pszFuncName, int (**pFunc)(void *))
{
	int			rv				= SUCCESS;
    void 		*pFunctionLib 	= NULL;   // Handle to shared lib file
    const char 	*pdlError;        // Pointer to error string

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

    // Open Dynamic Loadable Libary using LD_LIBRARY path
    pFunctionLib = dlopen(pszLibName, RTLD_NOW);
    pdlError = dlerror();
    if (pdlError)
    {
    	debug_sprintf(szDbgMsg, "%s - unable to dynamically open %s!, error=[%s]",
                						__FUNCTION__, ((pszLibName) ? pszLibName : "application"), pdlError);
    	APP_TRACE(szDbgMsg);

        pFunc = NULL;
        rv = FAILURE;
    }
    else
    {
        if (pszLibName)
        {
            debug_sprintf(szDbgMsg, "%s - loaded [%s] module!", __FUNCTION__, pszLibName);
            APP_TRACE(szDbgMsg);
        }

        // Find requested function
        *pFunc = dlsym(pFunctionLib, pszFuncName);
        pdlError = dlerror();
        if (pdlError)
        {
            debug_sprintf(szDbgMsg, "%s - unable to locate function [%s] in %s, error=[%s]",
                            __FUNCTION__, pszFuncName, ((pszLibName) ? pszLibName : "application"), pdlError);
            APP_TRACE(szDbgMsg);
            *pFunc = NULL;
            rv = FAILURE;
        }
        else
        {
            debug_sprintf(szDbgMsg, "%s - obtained [%s] pointer=%p", __FUNCTION__, pszFuncName, pFunc);
            APP_TRACE(szDbgMsg);
        }
    }

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}

/*
 * ============================================================================
 * Function Name: setDynamicFunctionPtr_1
 *
 * Description	: This function sets the function pointer
 * 				  host interface library depends on the configuration
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setDynamicFunctionPtr_1(char *pszLibName, char *pszFuncName, void (**pFunc)(char *, char *, char *, char *, char *))
{
	int			rv				= SUCCESS;
    void 		*pFunctionLib 	= NULL;   // Handle to shared lib file
    const char 	*pdlError;        // Pointer to error string

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    // Open Dynamic Loadable Libary using LD_LIBRARY path
    pFunctionLib = dlopen(pszLibName, RTLD_NOW);
    pdlError = dlerror();
    if (pdlError)
    {
    	debug_sprintf(szDbgMsg, "%s - unable to dynamically open %s!, error=[%s]",
                						__FUNCTION__, ((pszLibName) ? pszLibName : "application"), pdlError);
    	APP_TRACE(szDbgMsg);

        pFunc = NULL;
        rv = FAILURE;
    }
    else
    {
        if (pszLibName)
        {
            debug_sprintf(szDbgMsg, "%s - loaded [%s] module!", __FUNCTION__, pszLibName);
            APP_TRACE(szDbgMsg);
        }

        // Find requested function
        *pFunc = dlsym(pFunctionLib, pszFuncName);
        pdlError = dlerror();
        if (pdlError)
        {
            debug_sprintf(szDbgMsg, "%s - unable to locate function [%s] in %s, error=[%s]",
                            __FUNCTION__, pszFuncName, ((pszLibName) ? pszLibName : "application"), pdlError);
            APP_TRACE(szDbgMsg);
            *pFunc = NULL;
            rv = FAILURE;
        }
        else
        {
            debug_sprintf(szDbgMsg, "%s - obtained [%s] pointer=%p", __FUNCTION__, pszFuncName, pFunc);
            APP_TRACE(szDbgMsg);
        }
    }

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}

/*
 * ============================================================================
 * Function Name: setDynamicFunctionPtr_2
 *
 * Description	: This function sets the function pointer
 * 				  host interface library depends on the configuration
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setDynamicFunctionPtr_2(char *pszLibName, char *pszFuncName, void (**pFunc)(char *, char *))
{
	int			rv				= SUCCESS;
    void 		*pFunctionLib 	= NULL;   // Handle to shared lib file
    const char 	*pdlError;        // Pointer to error string

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    // Open Dynamic Loadable Libary using LD_LIBRARY path
    pFunctionLib = dlopen(pszLibName, RTLD_NOW);
    pdlError = dlerror();
    if (pdlError)
    {
    	debug_sprintf(szDbgMsg, "%s - unable to dynamically open %s!, error=[%s]",
                						__FUNCTION__, ((pszLibName) ? pszLibName : "application"), pdlError);
    	APP_TRACE(szDbgMsg);

        pFunc = NULL;
        rv = FAILURE;
    }
    else
    {
        if (pszLibName)
        {
            debug_sprintf(szDbgMsg, "%s - loaded [%s] module!", __FUNCTION__, pszLibName);
            APP_TRACE(szDbgMsg);
        }

        // Find requested function
        *pFunc = dlsym(pFunctionLib, pszFuncName);
        pdlError = dlerror();
        if (pdlError)
        {
            debug_sprintf(szDbgMsg, "%s - unable to locate function [%s] in %s, error=[%s]",
                            __FUNCTION__, pszFuncName, ((pszLibName) ? pszLibName : "application"), pdlError);
            APP_TRACE(szDbgMsg);
            *pFunc = NULL;
            rv = FAILURE;
        }
        else
        {
            debug_sprintf(szDbgMsg, "%s - obtained [%s] pointer=%p", __FUNCTION__, pszFuncName, pFunc);
            APP_TRACE(szDbgMsg);
        }
    }

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}
#ifdef DEBUG
/*
 * ============================================================================
 * Function Name: setupDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(DHI_DEBUG)) == NULL)
	{
		chDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}

/*
 * ============================================================================
 * Function Name: setupMemDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupMemDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chMemDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(DHI_MEM_DEBUG)) == NULL)
	{
		chMemDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chMemDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chMemDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chMemDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chMemDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}


/*
 * ============================================================================
 * Function Name: closeupDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupDebug(void)
{
	if(chDebug == ETHERNET_DEBUG)
	{
		iCloseDebugSocket();
	}

	return;
}

/*
 * ============================================================================
 * Function Name: closeupMemDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupMemDebug(void)
{
	if(chMemDebug == ETHERNET_DEBUG)
	{
		iCloseDebugSocket();
	}

	return;
}

/*
 * ============================================================================
 * Function Name: setDebugParamsToEnv
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int setDebugParamsToEnv(int * piDbgParamSet)
{
	int 	rv 					= SUCCESS;
	char	szParamValue[20]	= "";
	int		iLen;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szParamValue);

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DHI_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DHI_DEBUG variable[%s=%s]", __FUNCTION__, "DHI_DEBUG", szParamValue);
		APP_TRACE(szDbgMsg);

		setenv("DHI_DEBUG", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DHI_MEM_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DHI_MEM_DEBUG variable[%s=%s]", __FUNCTION__, "DHI_MEM_DEBUG", szParamValue);
		APP_TRACE(szDbgMsg);

		setenv("DHI_MEM_DEBUG", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGIP", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGIP variable[%s=%s]", __FUNCTION__, "DBGIP", szParamValue);
		APP_TRACE(szDbgMsg);

		setenv("DBGIP", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGPRT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGPRT variable[%s=%s]", __FUNCTION__, "DBGPRT", szParamValue);
		APP_TRACE(szDbgMsg);

		setenv("DBGPRT", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DEBUGOPT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DEBUGOPT variable[%s=%s]", __FUNCTION__, "DEBUGOPT", szParamValue);
		APP_TRACE(szDbgMsg);

		setenv("DEBUGOPT", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: APP_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void APP_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	char *			pszMsgPrefix	= "DHI";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: APP_MEM_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void APP_MEM_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	char *			pszMsgPrefix	= "DHI";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chMemDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chMemDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chMemDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

#endif
