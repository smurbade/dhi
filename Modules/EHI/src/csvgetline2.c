// From 'The Practice of Programming' by Brian W. Kernighan and Rob Pike

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "EHI/csv.h"
#include "EHI/ehicommon.h"

enum { NOMEM = -2 };          /* out of memory signal */

static char *line    = NULL;  /* input chars */
static char *sline   = NULL;  /* line copy used by split */
static int  maxline  = 0;     /* size of line[] and sline[] */
static char **field  = NULL;  /* field pointers */
static int  maxfield = 0;     /* size of field[] */
static int  nfield   = 0;     /* number of fields in field[] */

static char fieldsep[] = "|"; /* field separator chars */

static char *advquoted(char *);
static int split(void);

/* endofline: check for and consume \r, \n, \r\n, or EOF */
static int endofline(FILE *fin, int c)
{
    int eol;

    eol = (c=='\r' || c=='\n');
    if (c == '\r')
    {
        c = getc(fin);
        if (c != '\n' && c != EOF)
            ungetc(c, fin); /* read too far; put c back */
    }
    return eol;
}

/* reset: set variables back to starting values */
static void reset(void)
{
#ifdef DEBUG
    char szDebugMsg[1024+1];
#endif

    if (line)
    {
#if defined(DEBUG) || defined(MEMALLOC)
        debug_sprintf(szDebugMsg, "%s - freeing line=%p", __FUNCTION__, line);
        EHI_LIB_TRACE(szDebugMsg);
#endif
        free(line); /* free(NULL) permitted by ANSI C */
        line = NULL;
    }

    if (sline)
    {
#if defined(DEBUG) || defined(MEMALLOC)
        debug_sprintf(szDebugMsg, "%s - freeing sline=%p", __FUNCTION__, sline);
        EHI_LIB_TRACE(szDebugMsg);
#endif
        free(sline);
        sline = NULL;
    }

    if (field)
    {
#if defined(DEBUG) || defined(MEMALLOC)
        debug_sprintf(szDebugMsg, "%s - freeing field=%p", __FUNCTION__, field);
        EHI_LIB_TRACE(szDebugMsg);
#endif
        free(field);
        field = NULL;
    }

    maxline = maxfield = nfield = 0;
}

/* csvgetline:  get one line, grow as needed */
/* sample input: "LU",86.25,"11/4/1998","2:19PM",+4.0625 */
char *csvgetline(FILE *fin)
{
    int i, c;
    char *newl, *news;
#ifdef DEBUG
    char szDebugMsg[1024+1];
#endif

    if (line == NULL)           /* allocate on first call */
    {
        maxline = maxfield = 1;
        line = (char *) malloc(maxline);
        sline = (char *) malloc(maxline);
        field = (char **) malloc(maxfield*sizeof(field[0]));
        if (line == NULL || sline == NULL || field == NULL)
        {
#if defined(DEBUG) || defined(MEMALLOC)
            debug_sprintf(szDebugMsg, "%s - OUT OF MEMORY!", __FUNCTION__);
            EHI_LIB_TRACE(szDebugMsg);
#endif
            reset();
            return NULL;        /* out of memory */
        }
#if defined(DEBUG) || defined(MEMALLOC)
        debug_sprintf(szDebugMsg, "%s - allocated line=%p (%d bytes), sline=%p (%d bytes), field=%p (%d bytes)",
                        __FUNCTION__, line, maxline, sline, maxline, field, maxfield*sizeof(field[0]));
        EHI_LIB_TRACE(szDebugMsg);
#endif
    }
    for (i=0; (c=getc(fin))!=EOF && !endofline(fin,c); i++)
    {
        if (i >= maxline-1)     /* grow line */
        {
            maxline *= 2;       /* double current size */
            newl = (char *) realloc(line, maxline);
            if (newl == NULL)
            {
                reset();
                return NULL;
            }
            line = newl;
#if defined(DEBUG) || defined(MEMALLOC)
            debug_sprintf(szDebugMsg, "%s - reallocated line=%p (%d bytes)",
                            __FUNCTION__, line, maxline);
            EHI_LIB_TRACE(szDebugMsg);
#endif
            news = (char *) realloc(sline, maxline);
            if (news == NULL)
            {
                reset();
                return NULL;
            }
            sline = news;
#if defined(DEBUG) || defined(MEMALLOC)
            debug_sprintf(szDebugMsg, "%s - reallocated sline=%p (%d bytes)",
                            __FUNCTION__, sline, maxline);
            EHI_LIB_TRACE(szDebugMsg);
#endif
        }
        line[i] = c;
    }
    line[i] = '\0';
    if (split() == NOMEM)
    {
#if defined(DEBUG) || defined(MEMALLOC)
        debug_sprintf(szDebugMsg, "%s - OUT OF MEMORY!", __FUNCTION__);
        EHI_LIB_TRACE(szDebugMsg);
#endif
        reset();
        return NULL;            /* out of memory */
    }

    return (c == EOF && i == 0) ? NULL : line;
}

/* split: split line into fields */
static int split(void)
{
    char *p, **newf;
    char *sepp; /* pointer to temporary separator character */
    int sepc;   /* temporary separator character */
#ifdef DEBUG
    char szDebugMsg[1024+1];
#endif

    nfield = 0;
    if (line[0] == '\0')
        return 0;
    strcpy(sline, line);
    p = sline;

    do
    {
        if (nfield >= maxfield)
        {
            maxfield *= 2;          /* double current size */
            newf = (char **) realloc(field, maxfield * sizeof(field[0]));
            if (newf == NULL)
                return NOMEM;
            field = newf;
#if defined(DEBUG) || defined(MEMALLOC)
            debug_sprintf(szDebugMsg, "%s - reallocated field=%p (%d bytes)",
                            __FUNCTION__, field, maxfield * sizeof(field[0]));
            EHI_LIB_TRACE(szDebugMsg);
#endif
        }
        if (*p == '"')
            sepp = advquoted(++p);  /* skip initial quote */
        else
            sepp = p + strcspn(p, fieldsep);
        sepc = sepp[0];
        sepp[0] = '\0';             /* terminate field */
        field[nfield++] = p;
        p = sepp + 1;
    } while (sepc == fieldsep[0]);

    return nfield;
}

/* advquoted: quoted field; return pointer to next separator */
static char *advquoted(char *p)
{
    int i, j;

    for (i = j = 0; p[j] != '\0'; i++, j++)
    {
        if (p[j] == '"' && p[++j] != '"')
        {
            /* copy up to next separator or \0 */
            int k = strcspn(p+j, fieldsep);
            memmove(p+i, p+j, k);
            i += k;
            j += k;
            break;
        }
        p[i] = p[j];
    }
    p[i] = '\0';
    return p + j;
}

/* csvfield:  return pointer to n-th field */
char *csvfield(int n)
{
    if (n < 0 || n >= nfield)
        return NULL;
    return field[n];
}

/* csvnfield:  return number of fields */
int csvnfield(void)
{
    return nfield;
}

/* ---
// csvtest main: test CSV library
int main(void)
{
    int i;
    char *line;

    while ((line = csvgetline(stdin)) != NULL)
    {
        printf("line = `%s'\n", line);
        for (i = 0; i < csvnfield(); i++)
            printf("field[%d] = `%s'\n", i, csvfield(i));
    }
    return 0;
}
--- */
