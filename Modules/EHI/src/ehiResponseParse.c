/*
 * ehiResponseParse.c
 *
 *  Created on: 10-March-2016
 *      Author: BLR_SCA_TEAM
 */

#include <time.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <svc.h>
#include <errno.h>
#include <libxml/parser.h>
#include <libxml/tree.h>



#include "EHI/ehicommon.h"
#include "DHI_App/appLog.h"
#include "EHI/eGroups.h"
#include "EHI/ehiCfg.h"
#include "EHI/ehiResp.h"
#include "EHI/ehiutils.h"
#include "EHI/elavonhash.h"
#include "EHI/ehiRespFields.inc"
#include "EHI/ehiRequest.h"

#define NUMBER_CONVERION 10

static int fillDataInstruct(char* , char*, TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE, int);
static int processElavonResponse(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int buildGenRespDtlsForSSI(xmlDocPtr, xmlNode *, TRANDTLS_PTYPE);
static int frameEmvTagsResponse(ELAVONTRANDTLS_PTYPE, TRANDTLS_PTYPE);


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		ehiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         ehistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  ehistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	ehiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			ehiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)


/*
 * ============================================================================
 * Function Name: parseElavonResponse
 *
 * Description	: This function parses the Elavon Response and fills the data fields
 * 				  in the structure
 *
 * Input Params	: Buffer Containg the Elavon response,
 * 				  DHI STRUCTURE,
 * 				  EHI STRUCUTURE,
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int parseElavonResponse(char* pszElavonResp, TRANDTLS_PTYPE pstDHIReqResFields, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int		 				rv 										= SUCCESS;
	int						iLen									= 0;
	int 					iValue									= 0;
	int 					iTemp									= 0;
	int 					iTotalLen								= MAX_PASSTHROUGH_SIZE;
	int 					iCurLen									= 0;
	int						iTranType								= -1;
	int						iAppLogEnabled							= 0;
	int						iPassthroughLen							= 0;
	int 					iEncodedPassthroughLen					= 0;
	char					szAppLogData[256]						= "";
	char 					szFieldNum[10+1]						= "";		// Increased size to 10 for safety purpose, if Field Number size is more than 4 is also we will accept that.
	char	                szFieldValue[MAX_SIZE]                  = "";
	char					szRootName[50]							= "";
	char					szTempPassthroughField[MAX_SIZE+10+2+1]	= "";		// MAX SIZE is for Field Value, 10 is for API field Number, 2 is for <RS>, <FS>, 1 is for Extra Buffer
	char*					szTemp									= NULL;
	char*					pszExtraAPIfieldData					= NULL;
	char*					pszEncodedExtraAPIfieldData				= NULL;
	long					lChildCount								= 0;
	xmlDoc *				docPtr									= NULL;
	xmlNode *				rootPtr									= NULL;
	xmlNode *				curNode									= NULL;
	xmlNode *				tempCurNode1							= NULL;
	xmlNode *				tempCurNode2							= NULL;
	xmlChar *				value									= NULL;

#if DEBUG
	char			szDbgMsg[4700]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if(pszElavonResp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Response to Parse!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}


		iTranType = getTransType();

		pszExtraAPIfieldData =  (char*)malloc(iTotalLen * sizeof(char));
		memset(pszExtraAPIfieldData, 0x00, iTotalLen);

		/* Parse the XML message using the libxml API */
		iLen = strlen(pszElavonResp);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		EHI_LIB_TRACE(szDbgMsg);

		docPtr = xmlParseMemory(pszElavonResp, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			break;
		}

		strcpy(szRootName, (char *)rootPtr->name);

		debug_sprintf(szDbgMsg, "%s: The root name of the Elavon XML Response is %s",__FUNCTION__, szRootName);
		EHI_LIB_TRACE(szDbgMsg);

		/*
		 * Traverse node by node in the request and get the value of that node
		 */
		for(curNode = rootPtr->children; curNode != NULL; curNode = curNode->next)
		{
			//debug_sprintf(szDbgMsg, "%s:Node Type is %d", __FUNCTION__, curNode->type);
			//EHI_LIB_TRACE(szDbgMsg);

			if(curNode->type == XML_TEXT_NODE) //No need to parse the text node
			{
				continue;
			}

			if(strcmp((char *)curNode->name, "Transaction") != 0)	// No need to Parse the nodes other than Transaction
			{
				continue;
			}
			debug_sprintf(szDbgMsg, "%s:Need to traverse through the child nodes of this %s node", __FUNCTION__, (char *)curNode->name);
			EHI_LIB_TRACE(szDbgMsg);


			for(tempCurNode1 = curNode->children; tempCurNode1 != NULL; tempCurNode1 = tempCurNode1->next)
			{

				if(strcmp((char *)tempCurNode1->name, "API_Field") != 0)	// No need to Parse the nodes other than API_Field
				{
					continue;
				}

				lChildCount = xmlChildElementCount(tempCurNode1);

				//debug_sprintf(szDbgMsg, "%s:Number of child nodes for this %s node is %ld", __FUNCTION__, (char *)tempCurNode1->name, lChildCount);
				//EHI_LIB_TRACE(szDbgMsg);


				if(lChildCount == 2) //This node contains sub nodes, need to traverse those nodes
				{

					for(tempCurNode2 = tempCurNode1->children; tempCurNode2 != NULL; tempCurNode2 = tempCurNode2->next)
					{
						value = xmlNodeListGetString(docPtr, tempCurNode2->xmlChildrenNode, 1);
						if(value != NULL)
						{
							if(strcmp((char *)tempCurNode2->name, "Field_Number") == 0)
							{
								if(strlen((char*)value) < 4)
								{
									iValue = atoi((char*)value);
									sprintf(szFieldNum, "%04d", iValue);
								}
								else
								{
									strcpy(szFieldNum, (char *)value);
								}
							}
							else if (strcmp((char *)tempCurNode2->name, "Field_Value") == 0)
							{
								strcpy(szFieldValue, (char *)value);
							}
#ifdef DEVDEBUG
							if(strlen((char *)value) < 4500)
							{
								debug_sprintf(szDbgMsg, "%s:%s: Node Name [%s], Node Value[%s]",DEVDEBUGMSG,__FUNCTION__, (char *)tempCurNode2->name, (char *)value);
								EHI_LIB_TRACE(szDbgMsg);
							}
#endif
							/* Deallocating any allocated memory not in use now */
							xmlFree(value);
							value = NULL;
						}
						else
						{
							/* Need to traverse through remaining nodes*/
//							debug_sprintf(szDbgMsg, "%s:No Value for this node", __FUNCTION__);
//							EHI_LIB_TRACE(szDbgMsg);
						}
					}

					rv = fillDataInstruct(szFieldNum, szFieldValue, pstDHIReqResFields, pstElavonReqResDetails, iTranType);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to fill the data in the structure", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						if(rv == ERR_HASH_LOOKUP_FAILED || rv == ELAVON_ADD_TO_PASSTHROUGH)
						{
							debug_sprintf(szDbgMsg, "%s: API Field [%s] with value [%s] is not a part of defined API List and Pass through API List", __FUNCTION__, szFieldNum, szFieldValue);
							EHI_LIB_TRACE(szDbgMsg);

							iCurLen += (strlen(szFieldNum) + strlen(szFieldValue) + 2);		// +2 is for <FS>, <RS>

							if(iTotalLen < iCurLen)
							{
								iTotalLen = iTotalLen*2;
								szTemp = realloc(pszExtraAPIfieldData, iTotalLen);
								pszExtraAPIfieldData = szTemp;
							}

							sprintf(szTempPassthroughField, "%s%c%s%c", szFieldNum, 0x1C, szFieldValue, 0x1E);
							strcat(pszExtraAPIfieldData, szTempPassthroughField);
//							debug_sprintf(szDbgMsg, "%s: API Field and Value with separators: [%s]", __FUNCTION__, szTempPassthroughField);
//							EHI_LIB_TRACE(szDbgMsg);
						}
						rv = SUCCESS;			// rv = SUCCESS is because to continue furthur.
					}
				}
				else //no child nodes for this current node
				{
					continue;			/*accroding to format it should contain two child nodes*/

				}
			}
		}

		if(strlen(pszExtraAPIfieldData) > 0)
		{
			pszExtraAPIfieldData[strlen(pszExtraAPIfieldData) - 1] = '\0'; // Remove the Final '<RS>'

			if(pstDHIReqResFields[ePASSTHROUGH_FIELDS].elementValue != NULL)
			{
				free(pstDHIReqResFields[ePASSTHROUGH_FIELDS].elementValue);
			}

			if(strlen(pszExtraAPIfieldData) < 4500)
			{
				debug_sprintf(szDbgMsg, "%s: All API Field and Values Before Encoding : [%s]", __FUNCTION__, pszExtraAPIfieldData);
				EHI_LIB_TRACE(szDbgMsg);
			}
			iPassthroughLen = strlen(pszExtraAPIfieldData);

			iTemp = (( iPassthroughLen / 3) * 4) + 10;				/*Added 10 for safety purpose */

			debug_sprintf(szDbgMsg, "%s:Allocating Encoded length[%d] buffer", __FUNCTION__, iTemp);
			EHI_LIB_TRACE(szDbgMsg);

			pszEncodedExtraAPIfieldData = (char*)malloc((iTemp + 1) * sizeof(char));
			if(pszEncodedExtraAPIfieldData == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed for encoded buffer!!!",__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(pszEncodedExtraAPIfieldData, 0x00, iTemp + 1);

			encdBase64((uchar *)pszExtraAPIfieldData, iPassthroughLen, (uchar *)pszEncodedExtraAPIfieldData, &iEncodedPassthroughLen);

			pstDHIReqResFields[ePASSTHROUGH_FIELDS].elementValue = strdup((char*)pszEncodedExtraAPIfieldData);
		}

		if((pstDHIReqResFields[ePASSTHROUGH_FIELDS].elementValue != NULL) && strlen(pstDHIReqResFields[ePASSTHROUGH_FIELDS].elementValue) < 4500)
		{
			debug_sprintf(szDbgMsg, "%s: Extra Passthrough API FieldData after base64 encoding[%s]", __FUNCTION__, pstDHIReqResFields[ePASSTHROUGH_FIELDS].elementValue);
			EHI_LIB_TRACE(szDbgMsg);
		}

		if(docPtr != NULL)
		{
			xmlFreeDoc(docPtr);
		}

		rv = processElavonResponse(pstDHIReqResFields, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Process the Elavon response", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}

		rv = mapElavonResultCodeToDHI(pstDHIReqResFields, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to map Elavon Result Code to DHI", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}
		break;

	}

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Parsed Elavon Response");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);


	return rv;
}

/*
 * ============================================================================
 * Function Name: fillDataInstruct
 *
 * Description	: This function fills/stores the value at the corresponding
 * 				  index in the data structure
 *
 * Input Params	: Field Number, Field Value, DHI STRUCTURE, EHI STRUCTURE
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int fillDataInstruct(char *pszFieldNum, char *pszFieldValue, TRANDTLS_PTYPE pstDHIReqResFields, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails, int iTranType)
{
	int							rv				= SUCCESS;
	int							iValLen			= 0;
	int							iEhiIndex		= -1;
	int							iDHIIndex		= -1;
	int							iPassthrough	= -1;
	int							iRespCount		= 0;
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls	= NULL;
	HASHLST_PTYPE 				pstHashLst		= NULL;


#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if(pszFieldNum == NULL || pszFieldValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Either Field Number or Field Value is NULL!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Do the Hash look up for this Tag Name */
		pstHashLst = lookupElavonHashTable(pszFieldNum);
		if(pstHashLst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Did not find entry for this Field Number[%s] in the Hash Table!!! So ignoring this tag", __FUNCTION__, pszFieldNum);
			EHI_LIB_TRACE(szDbgMsg);

			rv = ERR_HASH_LOOKUP_FAILED;

			break;
		}

		//debug_sprintf(szDbgMsg, "%s: Index for this Field Number in the structure is [%d], ", __FUNCTION__, pstHashLst->iTagIndexVal);
		//EHI_LIB_TRACE(szDbgMsg);

		pstEHIPTDtls = getElavonPassThroughDtls();
		iRespCount = pstEHIPTDtls->iRespAPICnt;

		if(pstHashLst->iTagIndexVal > (E_REQRESP_MAX_FIELDS + iRespCount))	//Safety Check
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Index Value!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
			//Should not be the case, something wrong
		}
		else if(pstHashLst->iTagIndexVal > E_REQRESP_MAX_FIELDS) //Safety Check
		{
			iDHIIndex 	= pstEHIPTDtls->pstPTAPIRespDtls[pstHashLst->iTagIndexVal - (E_REQRESP_MAX_FIELDS + 1)].iDHIIndex;
			iEhiIndex  	= pstEHIPTDtls->pstPTAPIRespDtls[pstHashLst->iTagIndexVal - (E_REQRESP_MAX_FIELDS + 1)].iEhiIndex;
		}
		else if(pstHashLst->iTagIndexVal == NO_ENUM)
		{

		}
		else
		{
			iDHIIndex 		= genElavonRespLst[pstHashLst->iTagIndexVal].iDHIIndex;
			iEhiIndex  		= genElavonRespLst[pstHashLst->iTagIndexVal].iEhiIndex;
			iPassthrough  	= genElavonRespLst[pstHashLst->iTagIndexVal].iPassthrough;
		}


		iValLen = strlen(pszFieldValue);

		debug_sprintf(szDbgMsg, "%s: DHI Index [%d] EHI Index [%d] Passthrough [%d]", __FUNCTION__, iDHIIndex, iEhiIndex, iPassthrough);
		EHI_LIB_TRACE(szDbgMsg);

		if(iDHIIndex != NO_ENUM)
		{
			if(pstDHIReqResFields[iDHIIndex].elementValue != NULL)
			{
				free(pstDHIReqResFields[iDHIIndex].elementValue);
				pstDHIReqResFields[iDHIIndex].elementValue = NULL;
			}
			pstDHIReqResFields[iDHIIndex].elementValue = strdup((char*)pszFieldValue);
		}

		if(iEhiIndex != NO_ENUM)
		{
			memset(pstElavonReqResDetails[iEhiIndex].elementValue, 0x00, sizeof(pstElavonReqResDetails[iEhiIndex].elementValue));
			strncpy(pstElavonReqResDetails[iEhiIndex].elementValue, pszFieldValue, MAX_SIZE - 1);
		}

		if(iTranType == E_COMPLETION || iTranType == E_VOID)
		{
			if(iPassthrough == EHI_TRUE)
			{
				rv = ELAVON_ADD_TO_PASSTHROUGH;
				/* In case follow on EMV Transactions we need to send all EMV related fields, currently we are mapping some of EMV fields from SCA because they may get updated after 2 gen AC step,
				 * but in case of completion SCA does not contain EMV fields, so POS may not get the fields even though they present in the Elavon Response, that's why we are sending all the EMV Feilds
				 * in the pass through field in case of Completion */
			}
		}

		break;
	}


//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processElavonResponse
 *
 * Description	: This function fills/stores the value at the corresponding
 * 				  index in the data structure
 *
 * Input Params	: DHI STRUCTURE, EHI STRUCTURE
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int processElavonResponse(TRANDTLS_PTYPE pstDHIReqResFields, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int					rv					= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{

//		/*Copying Reference Number(CTROUTD)*/
//		if(pstDHIReqResFields[eCTROUTD].elementValue != NULL)
//		{
//			free(pstDHIReqResFields[eCTROUTD].elementValue);
//			pstDHIReqResFields[eCTROUTD].elementValue = NULL;
//		}
//		if((strcmp(pstDHIReqResFields[eCOMMAND].elementValue, "VOID") == 0) || (strcmp(pstDHIReqResFields[eCOMMAND].elementValue, "COMPLETION") == 0))
//		{
//			pstDHIReqResFields[eCTROUTD].elementValue = strdup(pstElavonReqResDetails[E_FOLLOWON_REF_NUM].elementValue);
//		}
//		else
//		{
//			pstDHIReqResFields[eCTROUTD].elementValue = strdup(pstElavonReqResDetails[E_REF_NUMBER].elementValue);
//		}

		/*Copying Card Token to DHI*/
//		if(strlen(pstElavonReqResDetails[E_CARD_TOKEN].elementValue) > 0)
//		{
//			if((strlen(pstElavonReqResDetails[E_CARD_TOKEN].elementValue) > 3) && (strncmp(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, "ID:", 3) == 0)
//				&& (strcmp(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, "ID:NOT_ASSIGNED") != 0))
//			{
//				if(pstDHIReqResFields[eCARD_TOKEN].elementValue != NULL)
//				{
//					free(pstDHIReqResFields[eCARD_TOKEN].elementValue);
//					pstDHIReqResFields[eCARD_TOKEN].elementValue = NULL;
//				}
					/*T_POLISETTYG1: Card Token will be sent to SCA with out ID:, but storing with ID: in ElavonTranRecords.txt File which will be used for follow on transactions*/
//					pstDHIReqResFields[eCARD_TOKEN].elementValue = strdup(pstElavonReqResDetails[E_CARD_TOKEN].elementValue);
//			}
//			else
//			{
//				memset(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, 0x00, sizeof(pstElavonReqResDetails[E_CARD_TOKEN].elementValue));
//			}
//		}

//		/*Copying Approved Amount*/
//		if((strlen(pstElavonReqResDetails[E_GATEWAY_RESP_CODE].elementValue) > 0) && (atoi(pstElavonReqResDetails[E_GATEWAY_RESP_CODE].elementValue) == 0))
//		{
//			if(strlen(pstElavonReqResDetails[E_TOTAL_AUTH_AMOUNT].elementValue) > 0)
//			{
//				pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue = strdup(pstElavonReqResDetails[E_TOTAL_AUTH_AMOUNT].elementValue);
//			}
//			else
//			{
//				pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue = strdup(pstElavonReqResDetails[E_TRANS_AMOUNT].elementValue);
//			}
//
//		}

		if(strlen(pstElavonReqResDetails[E_TOTAL_AUTH_AMOUNT].elementValue) > 0)
		{
			pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue = strdup(pstElavonReqResDetails[E_TOTAL_AUTH_AMOUNT].elementValue);
		}
		else if(strlen(pstElavonReqResDetails[E_TRANS_AMOUNT].elementValue) > 0)
		{
			pstDHIReqResFields[eAPPROVED_AMOUNT].elementValue = strdup(pstElavonReqResDetails[E_TRANS_AMOUNT].elementValue);
		}

		/*Considering Transaction as EMV Txn, only when EMV TAGS are present*/
		if(pstDHIReqResFields[eEMV_TAGS].elementValue != NULL)
		{
			rv = frameEmvTagsResponse(pstElavonReqResDetails, pstDHIReqResFields);
			if(rv !=SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed frame the Response EMV Tags", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: mapElavonResultCodeToDHI
 *
 * Description	: This function is responsible for filling the result code to DHI.
 *
 * Input Params	: DHI STRUCTURE, EHI STRUCTURE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int mapElavonResultCodeToDHI(TRANDTLS_PTYPE pstDHIReqResFields, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int  			rv				= SUCCESS;
	int 			iResultCode		= -1;
	char 			szTemp[5+1]		= "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue = strdup("SUCCESS");

		if(strlen(pstElavonReqResDetails[E_GATEWAY_RESP_CODE].elementValue) > 0)
		{
			debug_sprintf(szDbgMsg, "%s: ELAVON GATEWAY RESULT CODE: [%s]", __FUNCTION__, pstElavonReqResDetails[E_GATEWAY_RESP_CODE].elementValue);
			EHI_LIB_TRACE(szDbgMsg);

			strcpy(szTemp, pstElavonReqResDetails[E_GATEWAY_RESP_CODE].elementValue);
			iResultCode = atoi(szTemp);
		}

		if ((iResultCode == 0) && (pstDHIReqResFields[eCOMMAND].elementValue != NULL) && (!strcmp(pstDHIReqResFields[eCOMMAND].elementValue, "VOID")))
		{
			pstDHIReqResFields[eRESULT_CODE].elementValue 	= strdup("7");
			break;
		}

		switch(iResultCode)
		{
		case ELAVON_APPROVED:
			pstDHIReqResFields[eRESULT_CODE].elementValue	= strdup("5");
			break;

		case ELAVON_DECLINED:
			pstDHIReqResFields[eRESULT_CODE].elementValue 	= strdup("6");
			break;

		case ELAVON_VERFONE_COMPLETE:
			pstDHIReqResFields[eRESULT_CODE].elementValue 	= strdup("935");
			saveLocatorValues(pstDHIReqResFields);
			break;

		default:
			pstDHIReqResFields[eRESULT_CODE].elementValue 	= strdup("6");
			break;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameEmvTagsResponse
 *
 * Description	: This function will frame the EMV Response Tags.
 *
 * Input Params	: DHI STRUCTURE, EHI STRUCTURE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int frameEmvTagsResponse(ELAVONTRANDTLS_PTYPE pstElavonReqResDetails, TRANDTLS_PTYPE pstDHIReqResFields)
{
	int  			rv						= SUCCESS;
	int 			iTotal					= 0;
	int				iIndex					= 0;
	int  			iEmvRespTags[]			= {E_EMV_RESP_CODE_8A, E_ISSUER_AUTH_DATA_91, E_PRE_ISSUER_SCRIPTS_71, E_POST_ISSUER_SCRIPTS_72, E_ISSUER_SCRIPT_RESLTS_9F5B, E_ISSUER_SCRIPT_IDNTFER_9F18};
	char 			*pszEmvTags[]			= { "8A", "91", "71", "72", "9F5B", "9F18"};
	char			szEMVtagsResp[2048]		= "";		//size needs to be checked.
	char 			szTemp[16]				= "";
	char   			szTlvTag[512]			= "";
	char			szHexLen[8+1]			= "";
	unsigned char	szLen[3+1]				= "";
	unsigned short  iLen;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	/*TODO : Currently we are sending (8A,91,71,72,9F5B,9F18) EMV tags only in the response (Under assumption that only 91, 71, 72 tags are used for 2nd GEN AC)
	 * If Require we may need to add remaining EMV Tags also in the EMV tags in the response*/

	if(strlen(pstElavonReqResDetails[E_EMV_RESP_CODE_8A].elementValue) > 0)
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		Char2Hex(pstElavonReqResDetails[E_EMV_RESP_CODE_8A].elementValue, szTemp, strlen(pstElavonReqResDetails[E_EMV_RESP_CODE_8A].elementValue));
		strcpy(pstElavonReqResDetails[E_EMV_RESP_CODE_8A].elementValue, szTemp);
	}


	iTotal = sizeof(iEmvRespTags)/sizeof(int);
	memset(szEMVtagsResp, 0x00, sizeof(szEMVtagsResp));

	for(iIndex = 0; iIndex < iTotal; iIndex++)
	{
		memset(szTlvTag, 0x00, sizeof(szTlvTag));
		memset(szLen, 0x00, sizeof(szLen));
		memset(szHexLen, 0x00, sizeof(szHexLen));
		memset(szTemp, 0x00, sizeof(szTemp));

		if(strlen(pstElavonReqResDetails[iEmvRespTags[iIndex]].elementValue) > 0)
		{
			strcpy(szTlvTag, pszEmvTags[iIndex]);

			iLen = strlen(pstElavonReqResDetails[iEmvRespTags[iIndex]].elementValue)/2;

			if (iLen < 128)
			{
				szLen[0] = iLen;
			}
			else if (iLen < 256)
			{
				szLen[0] = 1 | 0x80;
				szLen[1] = iLen;

			}
			else if(iLen < 256)
			{
				szLen[0] = 2 | 0x80;
				szLen[1] = (iLen/ 256) & 0xff;
				szLen[2] = (iLen% 256);
			}
			else
			{
				continue;
			}

			memcpy(szTemp, szLen, sizeof(szTemp));
			Char2Hex(szTemp, szHexLen, strlen(szTemp));
			strcat(szTlvTag, szHexLen);
			strcat(szTlvTag ,pstElavonReqResDetails[iEmvRespTags[iIndex]].elementValue);

		}

		if(strlen(szTlvTag))
		{
			debug_sprintf(szDbgMsg, "%s: TAG [%s]", __FUNCTION__, szTlvTag);
			EHI_LIB_TRACE(szDbgMsg);

			strcat(szEMVtagsResp, szTlvTag);
		}
	}

	/*TODO Need to Check is it ok to do like this*/
	//	if(strlen(pstElavonReqResDetails[E_EXTRA_EMV_TAGS_TLV].elementValue) > 0)
	//	{
	//
	//		if(strlen(szEMVtagsResp) > 0)
	//		{
	//			strcat(szEMVtagsResp, pstElavonReqResDetails[E_EXTRA_EMV_TAGS_TLV].elementValue);
	//		}
	//		else
	//		{
	//			strcpy(szEMVtagsResp, pstElavonReqResDetails[E_EXTRA_EMV_TAGS_TLV].elementValue);
	//		}
	//	}

	if(strlen(szEMVtagsResp) > 0)
	{
		pstDHIReqResFields[eEMV_TAGS_RESP].elementValue = strdup(szEMVtagsResp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameSSIRespForElavonReportCommand
 *
 * Description	: This Function Will frame the SSI response for the Report Commands.
 *
 * Input Params	: DHI STRUCTURE, EHI STRUCTURE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int frameSSIRespForElavonReportCommand(int iCmd, TRANDTLS_PTYPE pstDHIDetails, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int					rv				= SUCCESS;
	int					iLen			= 0;
	int					iSize			= 0;
	int					iCount			= 0;
	int					iPTRespCnt		= 0;
	int					iDHIIndex		= -1;
	PASSTHROUGH_PTYPE	pstDHIPTLst 	= getDHIPassThroughDtls();
	xmlDocPtr			docPtr			= NULL;
	xmlNodePtr			rootPtr			= NULL;
	xmlNodePtr			elemNodePtr		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if(iCmd == E_TRANS_INQUIRY)
		{
			debug_sprintf(szDbgMsg, "%s: Filling the payment type for Transaction Inquiry command ", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			/* SSi response for Elavon Transaction Inquiry Command is framed in DHI.*/
			// For Transaction Inquiry Response, Elavon sends the Transaction Qualifier(PAYMENT_TYPE) in Response, We need to honor the same and send it back to POS.
			if(strlen(pstElavonReqResDetails[E_TRANS_QUALIFIER].elementValue) > 0)
			{
				if(pstDHIDetails[ePAYMENT_TYPE].elementValue != NULL)
				{
					free(pstDHIDetails[ePAYMENT_TYPE].elementValue);
				}
				if(strcmp(pstElavonReqResDetails[E_TRANS_QUALIFIER].elementValue, "010") == SUCCESS)
				{
					pstDHIDetails[ePAYMENT_TYPE].elementValue = strdup("CREDIT");
				}
				else if(strcmp(pstElavonReqResDetails[E_TRANS_QUALIFIER].elementValue, "030") == SUCCESS)
				{
					pstDHIDetails[ePAYMENT_TYPE].elementValue = strdup("DEBIT");
				}
				else if(strcmp(pstElavonReqResDetails[E_TRANS_QUALIFIER].elementValue, "050") == SUCCESS)
				{
					pstDHIDetails[ePAYMENT_TYPE].elementValue = strdup("GIFT");
				}
			}
		}
		else
		{
			//Create the XML doc
			docPtr = xmlNewDoc(BAD_CAST "1.0");
			if(docPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED, Couldnt create doc", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			//Create the root node
			rootPtr = xmlNewNode(NULL, BAD_CAST "RESPONSE");
			if(rootPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			xmlDocSetRootElement(docPtr, rootPtr);

			rv = buildGenRespDtlsForSSI(docPtr, rootPtr, pstDHIDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt build general resp details", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			if(pstDHIDetails[eCOMMAND].elementValue != NULL)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "COMMAND", BAD_CAST pstDHIDetails[eCOMMAND].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding COMMAND field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			if(strlen(pstElavonReqResDetails[E_GATEWAY_BATCH_NUM].elementValue) > 0)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "BATCH_NUMBER", BAD_CAST pstElavonReqResDetails[E_GATEWAY_BATCH_NUM].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding Elavon Batch Number field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}


			if(strlen(pstElavonReqResDetails[E_LOCAL_BATCH_NET_AMOUNT].elementValue) > 0)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "LOCAL_BATCH_NET_SETTLE_AMT", BAD_CAST pstElavonReqResDetails[E_LOCAL_BATCH_NET_AMOUNT].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding LOCAL_BATCH_NET_SETTLE_AMT field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			if(strlen(pstElavonReqResDetails[E_LOCAL_BATCH_TRANS_COUNT].elementValue) > 0)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "LOCAL_BATCH_ITEM_COUNT", BAD_CAST pstElavonReqResDetails[E_LOCAL_BATCH_TRANS_COUNT].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding LOCAL_BATCH_ITEM_COUNT field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			if(strlen(pstElavonReqResDetails[E_HOST_BATCH_NET_AMOUNT].elementValue) > 0)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "HOST_BATCH_NET_SETTLE_AMT", BAD_CAST pstElavonReqResDetails[E_HOST_BATCH_NET_AMOUNT].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding HOST_BATCH_NET_SETTLE_AMT field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			if(strlen(pstElavonReqResDetails[E_HOST_BATCH_TRANS_COUNT].elementValue) > 0)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "HOST_BATCH_ITEM_COUNT", BAD_CAST pstElavonReqResDetails[E_HOST_BATCH_TRANS_COUNT].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding HOST_BATCH_ITEM_COUNT field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			if(strlen(pstElavonReqResDetails[E_FUNDED_BATCH_AMOUNT].elementValue) > 0)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "FUNDED_BATCH_NET_SETTLE_AMT", BAD_CAST pstElavonReqResDetails[E_FUNDED_BATCH_AMOUNT].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding FUNDED_BATCH_NET_SETTLE_AMT field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			if(strlen(pstElavonReqResDetails[E_FUNDED_BATCH_COUNT].elementValue) > 0)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "FUNDED_BATCH_ITEM_COUNT", BAD_CAST pstElavonReqResDetails[E_FUNDED_BATCH_COUNT].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding FUNDED_BATCH_ITEM_COUNT field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			if(strlen(pstElavonReqResDetails[E_UNIQUE_DEVICE_ID].elementValue) > 0)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "LANE", BAD_CAST pstElavonReqResDetails[E_UNIQUE_DEVICE_ID].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding LANE field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			if(strlen(pstElavonReqResDetails[E_TERMINAL_ID].elementValue) > 0)
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "TERMID", BAD_CAST pstElavonReqResDetails[E_TERMINAL_ID].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding TERMINAL_ID field", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			if((pstDHIDetails[ePASSTHROUGH_FIELDS].elementValue != NULL) && (strlen(pstDHIDetails[ePASSTHROUGH_FIELDS].elementValue) > 0))
			{
				elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "PASSTHROUGH_FIELDS", BAD_CAST pstDHIDetails[ePASSTHROUGH_FIELDS].elementValue);
				if(elemNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding Pass through fields", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}

			iPTRespCnt = pstDHIPTLst->iRespXMLTagsCnt;

			debug_sprintf(szDbgMsg, "%s: iPTRespCnt [%d]", __FUNCTION__, iPTRespCnt);
			EHI_LIB_TRACE(szDbgMsg);

			if(iPTRespCnt > 0)
			{
				for(iCount = 0; iCount < iPTRespCnt; iCount++)
				{
					iDHIIndex = pstDHIPTLst->pstPTXMLRespDtls[iCount].index;

					if(iDHIIndex != -1 && (pstDHIPTLst->pstPTXMLRespDtls[iCount].key != NULL) && (strlen(pstDHIPTLst->pstPTXMLRespDtls[iCount].key) > 0)
							&& (pstDHIDetails[iDHIIndex].elementValue != NULL) && (strlen(pstDHIDetails[iDHIIndex].elementValue) > 0))
					{
						elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST pstDHIPTLst->pstPTXMLRespDtls[iCount].key, BAD_CAST pstDHIDetails[iDHIIndex].elementValue);
						if(elemNodePtr == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Error while adding Pass Through field", __FUNCTION__);
							EHI_LIB_TRACE(szDbgMsg);
						}
					}
				}
			}

			xmlDocDumpFormatMemory(docPtr, (xmlChar **)&pstDHIDetails[eREPORT_RESPONSE].elementValue, &iSize, 1);

			iLen = strlen(pstDHIDetails[eREPORT_RESPONSE].elementValue);

			debug_sprintf(szDbgMsg, "%s: Length is: %d", __FUNCTION__, iLen );
			EHI_LIB_TRACE(szDbgMsg);

			if(pstDHIDetails[eREPORT_RESP_LENGTH].elementValue == NULL)
			{
				pstDHIDetails[eREPORT_RESP_LENGTH].elementValue = (char *)malloc(NUMBER_CONVERION);
				if(pstDHIDetails[eREPORT_RESP_LENGTH].elementValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc Error", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;

					return rv;
				}

				memset(pstDHIDetails[eREPORT_RESP_LENGTH].elementValue,0x00,NUMBER_CONVERION);
				sprintf(pstDHIDetails[eREPORT_RESP_LENGTH].elementValue,"%d", iSize);

			}

			EHI_LIB_TRACE(pstDHIDetails[eREPORT_RESPONSE].elementValue);

			/* Free the memory allocated for the xml document tree */
			if(docPtr != NULL)
			{
				xmlFreeDoc(docPtr);

			}

		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildGenRespDtls
 *
 * Description	: This function adds the general response details to the
 * 				  xml doc
 *
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildGenRespDtlsForSSI(xmlDocPtr docPtr, xmlNode * rootNode, TRANDTLS_PTYPE pstSSIReqResFields)
{
	int				rv				= SUCCESS;
	xmlNodePtr		elemNodePtr		= NULL;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	if(docPtr == NULL || rootNode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param (docPtr/rootNode) is NULL", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Adding TERMINATION_STATUS field */
	if(pstSSIReqResFields[eTERMINATION_STATUS].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST pstSSIReqResFields[eTERMINATION_STATUS].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding TERMINATION_STATUS field", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding RESULT field */
	if(pstSSIReqResFields[eRESULT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT", BAD_CAST pstSSIReqResFields[eRESULT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT field", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding RESULT_CODE field */
	if(pstSSIReqResFields[eRESULT_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESULT_CODE", BAD_CAST pstSSIReqResFields[eRESULT_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESULT_CODE field", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding RESPONSE_TEXT field */
	if(pstSSIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST pstSSIReqResFields[eRESPONSE_TEXT].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding RESPONSE_TEXT field", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding eHOST_RESPCODE field */
	if(pstSSIReqResFields[eHOST_RESPCODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST pstSSIReqResFields[eHOST_RESPCODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding HOST_RESPCODE field", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}
	}

	/* Adding eGATEWAY_RESULT_CODE field
	Added this specifically for Elavon Host. We usually modify the RESULT_CODE to 4, 5, 6, or 7 to SCA to understand Approved or Declined, but Elavon POS may expect the original RESULT code from Host.
	So sending original RESULT code in this field..*/
	if(pstSSIReqResFields[eGATEWAY_RESULT_CODE].elementValue != NULL)
	{
		elemNodePtr = xmlNewChild(rootNode, NULL, BAD_CAST "GATEWAY_RESULT_CODE", BAD_CAST pstSSIReqResFields[eGATEWAY_RESULT_CODE].elementValue);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding eGATEWAY_RESULT_CODE field", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillErrorDetails
 *
 * Description	: This function is responsible for filling error details to DHI on occurence
 *
 * Input Params	: Error Code, DHI STRUCTURE, Buffer to fill error response message.
 *
 * Output Params: None
 * ============================================================================
 */
void fillErrorDetails(int iErrCode, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails, TRANDTLS_PTYPE pstDHIReqResFields, char* szRespMsg)
{
	int							iCount				= 0;
	int							iEHIPTRespCount		= 0;
	int							iDHIIndex			= -1;
	char *						pszAPI				= NULL;
	char *						pszFieldValue		= NULL;
	char *						pszAPIList[]		= {"0006", "0047", "0052", "0054", "0126", "5002", "5004", "1000", "0012", "0017", "0071", "0072", NULL}; // Important : Do not alter the sequence of API and enum list below. The sequence of API and the beow index must be same and must match.
	int							pszEnumList[]		= {E_DEVICE_SERIAL_NUM, E_POS_DATA_CODE, E_PROXIMITY_IND, E_POS_ENTRY_MODE, E_TRACK_IND, E_DEVICE_SERIAL_NUM, E_ENC_PROVIDER_ID, E_CARD_TYPE, E_FORCE_FLAG,
														E_CASHBACK_AMT, E_TAX1_IND, E_TAX_AMOUNT_1, -1};
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls		= getElavonPassThroughDtls();
	HASHLST_PTYPE 				pstHashLst			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	//char 	szTmpVal[50];

	int iRetHostInfo = 0;

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	switch(iErrCode)
	{
	case ERR_UNSUPP_CMD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59006");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("59006");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Command Not Supported by Processor");
		break;

	case ERR_RESP_TO:
		//fillCardTypeAPI1000(pstElavonReqResDetails, pstDHIReqResFields);
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("23");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("3");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("*SLR COMMUNICATIONS ERROR.");
		iRetHostInfo = 1;
		break;

	case ERR_CONN_TO:
		//fillCardTypeAPI1000(pstElavonReqResDetails, pstDHIReqResFields);
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("3");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("*SLR COMMUNICATIONS ERROR.");
		iRetHostInfo = 1;
		break;

	case ERR_CONN_FAIL:
		//fillCardTypeAPI1000(pstElavonReqResDetails, pstDHIReqResFields);
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("3");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("*SLR COMMUNICATIONS ERROR.");
		iRetHostInfo = 1;
		break;

	case ERR_HOST_TRAN_REVERSED:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("22");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("22");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("COMM_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Transaction Reversed");
		iRetHostInfo = 1;
		break;

	case ERR_HOST_TRAN_TIMEOUT:
		//fillCardTypeAPI1000(pstElavonReqResDetails, pstDHIReqResFields);
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("23");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("3");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("*SLR COMMUNICATIONS ERROR.");
		iRetHostInfo = 1;
		break;

	case ERR_HOST_NOT_AVAILABLE:
		//fillCardTypeAPI1000(pstElavonReqResDetails, pstDHIReqResFields);
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("3");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("*SLR COMMUNICATIONS ERROR.");
		iRetHostInfo = 1;
		break;

	case ERR_CFG_PARAMS:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("24");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Host Unavailable: Configuration Error");
		break;

	case ERR_INV_COMBI:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59041");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("59041");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Invalid Combination");
		break;

	case ERR_FLD_REQD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59041");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("59041");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Field is Required");
		break;

	case ERR_INV_FLD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59043");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("59043");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Field is Invalid");
		break;

	case ERR_NO_FLD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59045");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("59045");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Field Does Not Exist");
		break;

	case ERR_INV_FLD_VAL:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59045");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("59045");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("FIELD_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Value Not Valid For Field");
		break;

	case ERR_INV_RESP:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("HOST_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("Invalid Response From Host");
		break;

	case ERR_HOST_ERROR:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		//pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup(szStagMsg);
		break;

	case ERR_INV_CTROUTD:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("6");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("CTROUTD Not Found in the Current Batch");
		break;

	case ERR_SYSTEM:
	case FAILURE:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("59020");
		pstDHIReqResFields[eGATEWAY_RESULT_CODE].elementValue		= strdup("59020");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("INTERNAL_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("System Error");
		break;

	case ERR_DO_VSP_REG:
		pstDHIReqResFields[eTERMINATION_STATUS].elementValue= strdup("FAILURE");
		pstDHIReqResFields[eRESULT_CODE].elementValue		= strdup("936");
		pstDHIReqResFields[eVSP_CODE].elementValue			= strdup("936");
		pstDHIReqResFields[eRESULT].elementValue			= strdup("INTERNAL_ERROR");
		pstDHIReqResFields[eRESPONSE_TEXT].elementValue		= strdup("System Error");
		break;
	default:
		debug_sprintf(szDbgMsg, "%s: Invalid error code ", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
	}

	/* below piece of code is for the response text(for the text other than text mention above)*/
	if(strlen(szRespMsg)> 0)
	{
		if(pstDHIReqResFields[eRESPONSE_TEXT].elementValue != NULL)
		{
			free(pstDHIReqResFields[eRESPONSE_TEXT].elementValue);
		}

		pstDHIReqResFields[eRESPONSE_TEXT].elementValue= strdup(szRespMsg);
	}


	iEHIPTRespCount = pstEHIPTDtls->iRespAPICnt;
	debug_sprintf(szDbgMsg, "%s: EHI Passthrough Count [%d]", __FUNCTION__, iEHIPTRespCount);
	EHI_LIB_TRACE(szDbgMsg);

	/* It is noticed from Elavon that incase of error like when the host connection is unavailable or response timesout, we need to send few fields back to the POS Response.
	 * The fields to be added in the POS aresponse are passthrough from Elavon Response.
	 * But the fields which were filled in Elavon library before hitting the host can be sent back to POS Response.
	 * Hence this below piece of code is added
	 */
	if(iEHIPTRespCount > 0)
	{
		for(iCount = 0; ((pszAPI = pszAPIList[iCount]) && (pszAPI != NULL)); iCount++)
		{
			/* Do the Hash look up for this Tag Name */
			pstHashLst = lookupElavonHashTable(pszAPI);
			if(pstHashLst == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Did not find entry for this Field Number[%s] in the Hash Table!!! So ignoring this tag", __FUNCTION__, pszAPI);
				EHI_LIB_TRACE(szDbgMsg);

				//Not sending error here
				continue;
			}

			if(pstHashLst->iTagIndexVal > (E_REQRESP_MAX_FIELDS + iEHIPTRespCount))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Index Value!!!", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				continue;
				//Should not be the case, something wrong
			}
			else if(pstHashLst->iTagIndexVal > E_REQRESP_MAX_FIELDS) //Safety Check
			{
				iDHIIndex 	= pstEHIPTDtls->pstPTAPIRespDtls[pstHashLst->iTagIndexVal - (E_REQRESP_MAX_FIELDS + 1)].iDHIIndex;
			}
			else if(pstHashLst->iTagIndexVal == NO_ENUM)
			{

			}
			else
			{
				iDHIIndex 	= genElavonRespLst[pstHashLst->iTagIndexVal].iDHIIndex;
			}

			if(pszEnumList[iCount] != -1)
			{
				pszFieldValue = pstElavonReqResDetails[pszEnumList[iCount]].elementValue;
			}

			debug_sprintf(szDbgMsg, "%s: DHI Index [%d]", __FUNCTION__, iDHIIndex);
			EHI_LIB_TRACE(szDbgMsg);

			if(iDHIIndex != NO_ENUM)
			{
				if(pstDHIReqResFields[iDHIIndex].elementValue != NULL)
				{
					free(pstDHIReqResFields[iDHIIndex].elementValue);
					pstDHIReqResFields[iDHIIndex].elementValue = NULL;
				}
				pstDHIReqResFields[iDHIIndex].elementValue = strdup((char*)pszFieldValue);
			}
		}


	}

	/* It is required to return Merchant ID and terminal ID even in case of error.
	 * Hence populating the same from the configured values.
	 */
	/* Get Terminal ID */
	/*if(iRetHostInfo)
	{
		memset(szTmpVal,0x00,sizeof(szTmpVal));
		getTerminalId(szTmpVal);
		pstDHIReqResFields[eTERMID].elementValue = strdup(szTmpVal);
	}*/

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: fillCardTypeAPI1000
 *
 * Description	: This function will fill Card Type Details in case of Connection error, Response Timeout cases. Elavon requires the captured card type back in the POS Response
 *
 * Input Params	:
 *
 * Output Params: None
 * ============================================================================
 */
int fillCardTypeAPI1000(ELAVONTRANDTLS_PTYPE pstElavonReqResDetails, TRANDTLS_PTYPE pstDHIReqResFields)
{
	int			rv					= SUCCESS;
	char 		szPaymentMedia[50]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstDHIReqResFields != NULL && pstDHIReqResFields[ePAYMENT_MEDIA].elementValue != NULL)
	{
		memset(szPaymentMedia, 0x00, sizeof(szPaymentMedia));
		strcpy(szPaymentMedia, pstDHIReqResFields[ePAYMENT_MEDIA].elementValue);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Payment Media not available for the transaction ", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		return rv;
	}

	if(strcasecmp(szPaymentMedia, "VISA") == 0)
	{
		strcpy(pstElavonReqResDetails[E_CARD_TYPE].elementValue, "VI");
	}
	else if(strcasecmp(szPaymentMedia, "AMEX") == 0)
	{
		strcpy(pstElavonReqResDetails[E_CARD_TYPE].elementValue, "AX");
	}
	else if(strcasecmp(szPaymentMedia, "MASTERCARD") == 0 || (strcasecmp(szPaymentMedia, "MC") == 0))
	{
		strcpy(pstElavonReqResDetails[E_CARD_TYPE].elementValue, "MC");
	}
	else if((strcasecmp(szPaymentMedia, "DISCOVER") == 0) || (strcasecmp(szPaymentMedia, "DISC") == 0))
	{
		strcpy(pstElavonReqResDetails[E_CARD_TYPE].elementValue, "DI");
	}
	else if(strcasecmp(szPaymentMedia, "DEBIT") == 0)
	{
		strcpy(pstElavonReqResDetails[E_CARD_TYPE].elementValue, "DB");
	}
	else if(strcasecmp(szPaymentMedia, "JCB") == 0)
	{
		strcpy(pstElavonReqResDetails[E_CARD_TYPE].elementValue, "JC");
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveLocatorValues
 *
 * Description	: This function is responsible for filling error details to DHI on occurence
 *
 * Input Params	: Error Code, DHI STRUCTURE, Buffer to fill error response message.
 *
 * Output Params: None
 * ============================================================================
 */
void saveLocatorValues(TRANDTLS_PTYPE pstDHIReqResFields)
{

#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entry ------- ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);


	if(pstDHIReqResFields[eTERMID].elementValue != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: pstDHIReqResFields[eTERMID].elementValue [%s]", __FUNCTION__, pstDHIReqResFields[eTERMID].elementValue);
		EHI_LIB_TRACE(szDbgMsg);

		updateTerminalId(pstDHIReqResFields[eTERMID].elementValue);
	}

	if(pstDHIReqResFields[eCHAIN_CODE].elementValue)
	{
		debug_sprintf(szDbgMsg, "%s: pstDHIReqResFields[eCHAIN_CODE].elementValue [%s]", __FUNCTION__, pstDHIReqResFields[eCHAIN_CODE].elementValue);
		EHI_LIB_TRACE(szDbgMsg);

		updateChainCode(pstDHIReqResFields[eCHAIN_CODE].elementValue);
	}

	if(pstDHIReqResFields[eLOCATION_NAME].elementValue != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: pstDHIReqResFields[eLOCATION_NAME].elementValue [%s]", __FUNCTION__, pstDHIReqResFields[eLOCATION_NAME].elementValue);
		EHI_LIB_TRACE(szDbgMsg);

		updateLocationName(pstDHIReqResFields[eLOCATION_NAME].elementValue);
	}

	return;

}

