/*
 * ehiComm.c
 *
 *  Created on: 03-March-2016
 *      Author: BLR_SCA_TEAM
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <time.h>
#include <sys/timeb.h>	// For struct timeb
#include <pthread.h>
#include <svc.h>
#include <setjmp.h>
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>


#include "EHI/ehicommon.h"
#include "EHI/ehiHostCfg.h"
#include "EHI/ehiCfg.h"
#include "EHI/ehiutils.h"
#include "DHI_APP/appLog.h"



#define CA_CERT_FILE	"ca-bundle.crt"

static struct curl_slist *	headers			= NULL;
static pthread_t			ptKeepAliveIntId;
static CURL *				gptElavonCurlHandle = NULL;
static pthread_mutex_t  	gptElavonCurlHandleMutex;
static sigjmp_buf			keepAliveJmpBuf;

int 			cleanCURLHandle(EHI_BOOL);

static int 		chkConn(CURL *, char *, int, char **, int *);
static int      initializeOpenSSLLocks();
static void 	keepAliveTimeSigHandler(int);
static int 		getCURLHandle(HOST_URL_ENUM);
static void * 	keepAliveIntervalTimer(void *);
static void		commonInit(CURL *, EHI_BOOL, int);
static int 		initializeRespData(RESP_DATA_PTYPE);
static int		initElavonCURLHandle(CURL **, HOST_URL_ENUM);
static size_t	saveResponse(void *, size_t, size_t, void *);
static int 		sendDataToElavonHost(CURL *, char *, int, char **, int *);


#ifdef DEBUG
/* Static functions' declarations */
static int 		curlDbgFunc(CURL *, curl_infotype, char *, size_t, void *);
#endif
/* we have this global to let the callback get easy access to it */
static pthread_mutex_t *lockarray;


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		ehiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         ehistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  ehistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	ehiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			ehiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)




/*
 * ============================================================================
 * Function Name: initEHIFace
 *
 * Description	: Below function will initialize the connection with The Elavon GateWay
 *
 * Input Params	: Nothing
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initEHIFace()
{
	int				rv				= SUCCESS;
	int				iKeepAlive		= -1;
	char *			pszCURLVer		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iKeepAlive = getKeepAliveParameter();
	/* Initialize the CURL library */
	curl_global_init(CURL_GLOBAL_ALL);

	/*
	 * Praveen_P1: We have started getting problem with the SAF and Online
	 * transactions together. To make it thread safe, we have followed the
	 * following link
	 * http://curl.haxx.se/libcurl/c/threaded-ssl.html
	 *
	 */
	rv = initializeOpenSSLLocks();

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while initializing OpenSSL Lock Callbacks", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return rv;
	}

	/* Get the CURL library version and print it in the logs */
	pszCURLVer = curl_version();

	/* VDR: TODO: Add a syslog statement */
	debug_sprintf(szDbgMsg, "%s: CURL Ver: %s", __FUNCTION__, pszCURLVer);
	EHI_LIB_TRACE(szDbgMsg);

	headers = curl_slist_append(headers, "User-Agent:SCA v2.19.x");
	headers = curl_slist_append(headers, "Content-Type:text/xml");
	//headers = curl_slist_append(headers, "Cache-Control:no-cache");

	if(pthread_mutex_init(&gptElavonCurlHandleMutex, NULL))//Initialize the Elavon curl Handle Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create Elavon Curl Handle Mutex!", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(iKeepAlive == KEEP_ALIVE_DEFINED_TIME)
	{
		//Open the keep Alive Interval Thread
		debug_sprintf(szDbgMsg, "%s - Starting the keepAliveIntervalThread ...", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		pthread_create(&ptKeepAliveIntId, NULL, keepAliveIntervalTimer, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: postDataToElavonHost
 *
 * Description	: This function initializes the curl handler and sends the data
 * 				  to the Elavon Host
 *
 * Input Params	:	char *
 * 					int
 * 					char **
 * 					int *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int postDataToElavonHost(char * req, int reqSize, char ** resp, int * respSize)
{
	int			rv					= SUCCESS;
	int			iCnt				= 0;
	int			iAppLogEnabled		= 0;
	static int	host				= PRIMARY_URL;
	char		szAppLogData[256]	= "";
	char		szAppLogDiag[256]	= "";

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: Elavon HOST REQ = [%s]", DEVDEBUGMSG,__FUNCTION__, req);
		EHI_LIB_TRACE(szDbgMsg);
	}
#endif

	while(iCnt < getNumUniqueHosts())
	{
		rv = getCURLHandle(host);

		if(rv == SUCCESS)
		{
			 /* CURL Handler was initialized successfully*/
			acquireElavonMutexLock(&gptElavonCurlHandleMutex, "Elavon Curl Handle Mutex");
			rv = sendDataToElavonHost(gptElavonCurlHandle, req, reqSize, resp, respSize);
			releaseElavonMutexLock(&gptElavonCurlHandleMutex, "Elavon Curl Handle Mutex");

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Post Data to the Elavon Host");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				if(rv == ERR_RESP_TO) //Incase of response timeout we should not try with the next url
				{
					debug_sprintf(szDbgMsg, "%s: Response Time out from the Elavon host",
											__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
					cleanCURLHandle(EHI_FALSE);
					break;
				}
			}

			if((rv == SUCCESS) && (*resp))
			{
				cleanCURLHandle(EHI_FALSE);
				break;
			}

			cleanCURLHandle(EHI_TRUE); //Closing the current curl handler before trying the next host url
		}

		/* If the first host is not working then try the next host*/
		host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trial %d..", __FUNCTION__,iCnt);
		EHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);

	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: cleanCURLHandle
 *
 * Description	: This function cleans/closes the opened CURL Handle
 *
 * Input Params	: EHI_BOOL
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int cleanCURLHandle(EHI_BOOL iForceClose)
{
	int				rv				= SUCCESS;
	static 	int		iKeepAlive		= -1;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(iKeepAlive == -1)
	{
		iKeepAlive = getKeepAliveParameter();
	}

	acquireElavonMutexLock(&gptElavonCurlHandleMutex, "Elavon Curl Handle Mutex");
	debug_sprintf(szDbgMsg, "%s: Enter, gptElavonCurlHandle [%p]", __FUNCTION__, gptElavonCurlHandle);
	EHI_LIB_TRACE(szDbgMsg);

	if((iForceClose == EHI_TRUE) || (iKeepAlive == KEEP_ALIVE_DISABLED))
	{
		if(gptElavonCurlHandle != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Cleaning up the CURL handle from if cond", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			curl_easy_cleanup(gptElavonCurlHandle);
			gptElavonCurlHandle = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No need to clean CURL Handle", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

	}

	debug_sprintf(szDbgMsg, "%s: Cleaning up the CURL handle is Done", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	releaseElavonMutexLock(&gptElavonCurlHandleMutex, "Elavon Curl Handle Mutex");

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkHostConn
 *
 * Description	: This Function is used to check the host Connection.
 *
 * Input Params	: None.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int checkHostConn(char * req, int reqSize, char ** resp, int *respSize)
{
	int			rv				= SUCCESS;
	int			iCnt			= 0;
	static int	host			= PRIMARY_URL;
	CURL *		curlHandle		= NULL;
#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(iCnt < 2)
	{
		rv = initElavonCURLHandle(&curlHandle, host);

		debug_sprintf(szDbgMsg, "%s: curlHandle [%p]", __FUNCTION__, curlHandle);
		EHI_LIB_TRACE(szDbgMsg);

		if(rv == SUCCESS)
		{
			/* Curl handler initialized successfully
			 * Checking connection */
			rv = chkConn(curlHandle, req, reqSize, resp, respSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to check Host connection",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host connection available", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
			}
		}

		if(curlHandle != NULL)
		{
			curl_easy_cleanup(curlHandle);
		}

		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Breaking from the loop", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/* If the first host is not working then try the next host */
		host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trying again..", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: chkConn
 *
 * Description	: This Function is used to check the host Connection.
 *
 * Input Params	: char *
 *
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int chkConn(CURL * curlHandle, char * req, int reqSize, char **resp, int *respSize)
{
	int				rv					= SUCCESS;
	long			lCode				= 0L;
	char			szCurlErrBuf[4096] 	= "";
	RESP_DATA_STYPE	stRespData;
#ifdef DEBUG
	char			szDbgMsg[4000]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		//curl_easy_setopt(curlHandle, CURLOPT_POST, 1L);
		//curl_easy_setopt(curlHandle, CURLOPT_NOBODY, 1);
		//		curl_easy_setopt(curlHandle, CURLOPT_VERBOSE, 1L);
		//curl_easy_setopt(curlHandle, CURLOPT_FOLLOWLOCATION, 1);

		/*		 Initialize the response data*/
		rv = initializeRespData(&stRespData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Initialization of response data FAILED"
					, __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			break;
		}
		/*Set some curl library options before sending the data*/
		curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &stRespData);

		/* Setting the req and its length only if there are existing*/
		if(req && strlen(req) > 0)
		{
#ifdef DEVDEBUG
			if(strlen(req) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: Request Msg = %s \n", __FUNCTION__,
																		req);
				EHI_LIB_TRACE(szDbgMsg);
			}
#endif

			curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, (void *) req);
			curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, reqSize);
		}

		curl_easy_setopt(curlHandle, CURLOPT_ERRORBUFFER, szCurlErrBuf);


		rv = curl_easy_perform(curlHandle);
		if(rv == CURLE_OK)
		{
			debug_sprintf(szDbgMsg, "%s: Perform SUCCESS", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			*resp = stRespData.szMsg;
			*respSize = stRespData.iWritten;
#ifdef DEVDEBUG
			if(strlen(stRespData.szMsg) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: Response Msg = %s \n", __FUNCTION__,
																		stRespData.szMsg);
				EHI_LIB_TRACE(szDbgMsg);
			}
#endif
			debug_sprintf(szDbgMsg, "%s: Response Len = [%d]", __FUNCTION__,
														stRespData.iWritten);

			EHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Return Value from PERFORM = [%d]",
					__FUNCTION__, rv);
			EHI_LIB_TRACE(szDbgMsg);
		}

		rv = curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &lCode);

		debug_sprintf(szDbgMsg, "%s: Return Value RESPONSE CODE = [%d], [%ld]",
				__FUNCTION__, rv, lCode);
		EHI_LIB_TRACE(szDbgMsg);

		if(rv == 0 && lCode == 200)
		{
			debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host",
					__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else if(rv == 0 && lCode == 404) //if connection is available we are getting this one considering good response for now
		{
			debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host",
					__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else if(rv == 0 && lCode == 412)
		{
			debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host",
					__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Didnt get the good resp code from host"
					, __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}
#if 0
	if(curlHandle != NULL)
	{
		curl_easy_cleanup(curlHandle);
	}
#endif

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: static void * keepAliveIntervalTimer(void * arg)
 *
 * Description	: Thread to run the keep Alive Interval timer
 *
 * Input Params	: Void *
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void * keepAliveIntervalTimer(void * arg)
{
	int			iKeepAliveInterval				= 0;
	int			iVal							= 0;
	ullong		curTime							= 0;
	ullong		endTime							= 0;

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	/* Add signal handler for SIG_KA_TIME SETTIME */
	if(SIG_ERR == signal(SIG_KA_TIME, keepAliveTimeSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIG_KA_TIME",
																__FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Keep Alive Interval Thread Started", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iKeepAliveInterval = getKeepAliveInterval();
	iVal = sigsetjmp(keepAliveJmpBuf, 1);
	if(iVal == 1)
	{
		debug_sprintf(szDbgMsg, "%s: Resetting the Keep Alive Interval Timer",
								__FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
	}

	curTime = svcGetSysMillisec();
	endTime = curTime + (iKeepAliveInterval * 60 * 1000L);

	while(1)
	{
		while(endTime >= curTime)
		{
			svcWait(100); // Sleep little by little in the loop for KeepAliveInterval Duration
			curTime = svcGetSysMillisec();
		}

		if(gptElavonCurlHandle != NULL)
		{
			cleanCURLHandle(EHI_TRUE);
		}
		svcWait(360000); // keep the thread to run permanently by waiting
	}

	return NULL;
}

/*
 * ============================================================================
 * Function Name: keepAliveTimeSigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void keepAliveTimeSigHandler(int iSigNo)
{
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	if(iSigNo == SIG_KA_TIME)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIG_KA_TIME delivered", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		siglongjmp(keepAliveJmpBuf, 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown signal", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
	}
	return;
}

/*
 * ===================================================================================================================
 * Function Name: resetKAIntervalUpdationTimer
 *
 * Description	: Sends the signal to Keep Alive Interval thread to reset the timer once set time command is received
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * =====================================================================================================================
 */
int resetKAIntervalUpdationTimer()
{
	int		iRetVal = FAILURE;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Keep Alive Interval Timer thread id: %d", __FUNCTION__, (int)ptKeepAliveIntId);
	EHI_LIB_TRACE(szDbgMsg);

	//Check for the thread before killing.
	if(ptKeepAliveIntId != 0)
	{
		iRetVal = pthread_kill(ptKeepAliveIntId, SIG_KA_TIME);
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, iRetVal);
	EHI_LIB_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getCURLHandle
 *
 * Description	: This function returns the curl Handle according to keep Alive configuration parameter
 *
 * Input Params	: HOST_URL_ENUM
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getCURLHandle(HOST_URL_ENUM host)
{
	int				rv 				= SUCCESS;
	static 	int		iKeepAlive		= -1;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(iKeepAlive == -1)
	{
		iKeepAlive = getKeepAliveParameter();
	}

	acquireElavonMutexLock(&gptElavonCurlHandleMutex, "Elavon Curl Handle Mutex");

	debug_sprintf(szDbgMsg, "%s: iKeepAlive [%d], gptElavonCurlHandle ---- [%p]", __FUNCTION__, iKeepAlive, gptElavonCurlHandle);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{


		if(gptElavonCurlHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: CURL Handle Not Available, Create CURL Handle", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = initElavonCURLHandle(&gptElavonCurlHandle, host);

		}

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: CURL Handle Init Failed", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		if(iKeepAlive == KEEP_ALIVE_DEFINED_TIME && (ptKeepAliveIntId != 0))
		{
			rv = pthread_kill(ptKeepAliveIntId, SIG_KA_TIME);
		}

		break;
	}

	releaseElavonMutexLock(&gptElavonCurlHandleMutex, "Elavon Curl Handle Mutex");

	return rv;
}

/*
 * ============================================================================
 * Function Name: initElavonCURLHandle
 *
 * Description	: Initializes the Elavon Curl handler with all the required curl library options.
 *
 * Input Params	:	CURL *
 * 					HOST_URL_ENUM
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */


static int initElavonCURLHandle(CURL ** handle, HOST_URL_ENUM hostType)
{
	int					rv					= SUCCESS;
	int					iAppLogEnabled		= 0;
	int 				iTimeoutFromPOS		= 0;
	char				szTmpURL[100]		= "";
	char				szAppLogData[256]	= "";
	char				szAppLogDiag[256]	= "";
	CURL *				locHandle			= NULL;
	EHI_BOOL			certValdReq 		= EHI_FALSE;
	HOSTDEF_STRUCT_TYPE	hostDef;
#ifdef DEBUG
	char				szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		memset(&hostDef, 0x00, sizeof(hostDef));

		rv = getElavonHostDetails(hostType, &hostDef);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the host details",
																__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			break;
		}

		if (strncmp(hostDef.url, "https", 5) == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Url: %s",__FUNCTION__, hostDef.url);
			EHI_LIB_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Cert Validation required",
																__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			certValdReq = EHI_TRUE;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Posting the request to the URL %s", hostDef.url);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		/* Initialize the handle using the CURL library */
		locHandle = curl_easy_init();
		if(locHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Curl initialization for handle failed",
																__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the URL */
		if(hostDef.portNum != 0)
		{
			sprintf(szTmpURL, "%s:%d", hostDef.url, hostDef.portNum);
			curl_easy_setopt(locHandle, CURLOPT_URL, szTmpURL);

			debug_sprintf(szDbgMsg, "%s: Setting Elavon URL = [%s]", __FUNCTION__,
																	szTmpURL);
			EHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			curl_easy_setopt(locHandle, CURLOPT_URL, hostDef.url);

			debug_sprintf(szDbgMsg, "%s: Setting Elavon URL = [%s]", __FUNCTION__,
																hostDef.url);
			EHI_LIB_TRACE(szDbgMsg);

		}

		iTimeoutFromPOS = getTimeoutValue();

		debug_sprintf(szDbgMsg, "%s: Timeout  [%d]", __FUNCTION__, iTimeoutFromPOS);
		EHI_LIB_TRACE(szDbgMsg);

		/* Set the connection timeout */
		if(iTimeoutFromPOS > 0)
		{
			curl_easy_setopt(locHandle, CURLOPT_CONNECTTIMEOUT, iTimeoutFromPOS);
		}
		else
		{
			curl_easy_setopt(locHandle, CURLOPT_CONNECTTIMEOUT, hostDef.conTimeOut);
		}


		/* Set the total timeout */
		if(iTimeoutFromPOS > 0)
		{
			curl_easy_setopt(locHandle, CURLOPT_TIMEOUT, iTimeoutFromPOS);
		}
		else if(hostDef.respTimeOut > 0)
		{
			curl_easy_setopt(locHandle, CURLOPT_TIMEOUT, hostDef.respTimeOut);
		}

		/* Set the other common features */
		commonInit(locHandle, certValdReq, hostType);
		*handle = locHandle;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: commonInit
 *
 * Description	: This function initializes the common curl library options
 *
 *
 * Input Params	:	CURL *
 * 					EHI_BOOL
 *
 *
 * Output Params: None
 * ============================================================================
 */

static void commonInit(CURL * handle, EHI_BOOL certValdReq, int hostType)
{

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	/*Set the NO SIGNAL option, This option is very important to set in case
	 * of multi-threaded applications like ours, because otherwise the libcurl
	 * uses signal handling for the communication and that causes the threads
	 * to go crazy and even crash. Fix done for issue 1659 (Church of LDS AmDocs
	 * Case# 130826-3983)*/
	curl_easy_setopt(handle, CURLOPT_NOSIGNAL, 1L);


	/*Need to get clarity on this TOD*/
	/*Set the HTTP post option*/
	/* Custom POST Request */
	curl_easy_setopt(handle, CURLOPT_POST, 1L);

//	if(getKeepAliveParameter())
	 /*Add the headers*/
	curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headers);

#ifdef DEBUG
	 /*Add the debug function*/
	curl_easy_setopt(handle, CURLOPT_DEBUGFUNCTION, curlDbgFunc);
#endif

	if (certValdReq == EHI_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Url starts with https. Doing certificate validation",
				__FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		curl_easy_setopt(handle, CURLOPT_SSL_VERIFYPEER, 1L);
		curl_easy_setopt(handle, CURLOPT_SSL_VERIFYHOST, 2L);
	}

/*	 Set the CA certificate*/
	curl_easy_setopt(handle, CURLOPT_CAINFO, CA_CERT_FILE);

	 /*Set the write function*/
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, saveResponse);

	 /*Set the detection as TRUE for HTTP errors*/
	curl_easy_setopt(handle, CURLOPT_FAILONERROR, EHI_TRUE);

	/* To request using TLS for the transfer */
	curl_easy_setopt(handle, CURLOPT_USE_SSL, CURLUSESSL_NONE);

	/* To set preferred TLS/SSL version */
	//curl_easy_setopt(handle, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

	debug_sprintf(szDbgMsg, "%s: --- returning ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);
	return;
}
/*
 * ============================================================================
 * Function Name: saveResponse
 *
 * Description	: This function is used to store the response
 *
 *
 * Input Params	:	void *
 * 					size_t
 * 					size_t
 * 					void *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static size_t saveResponse(void * ptr, size_t size, size_t cnt, void * des)
{
	int				rv				= SUCCESS;
	int				iSizeLeft		= 0;
	int				totLen			= 0;
	int				iReqdLen		= 0;
	char *			cTmpPtr			= NULL;
	RESP_DATA_PTYPE	respPtr			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{

		respPtr = (RESP_DATA_PTYPE) des;
		totLen = size * cnt;
		iSizeLeft = respPtr->iTotSize - respPtr->iWritten;

		if(totLen > iSizeLeft)
		{
			iReqdLen = (respPtr->iTotSize + totLen - iSizeLeft) + 1 /*one extra buffer for NULL at the end for safety purpose*/;

			cTmpPtr = (char *) realloc(respPtr->szMsg, iReqdLen);
			if(cTmpPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Reallocation of memory FAILED",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(cTmpPtr + respPtr->iWritten, 0x00, iReqdLen - respPtr->iWritten);

			respPtr->iTotSize = iReqdLen;
			respPtr->szMsg = cTmpPtr;
		}

		memcpy(respPtr->szMsg + respPtr->iWritten, ptr, totLen);
		respPtr->iWritten += totLen;

		rv = totLen;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: sendDataToElavonHost
 *
 * Description	: This function sends the XML data to the Elavon Host
 *
 *
 * Input Params	:	CURL *
 * 					char *
 * 					int
 * 					char **
 * 					int *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int sendDataToElavonHost(CURL * curlHandle, char * req, int reqSize,
												char ** resp, int * respSize)
{
	int				rv				   	= SUCCESS;
	int				iAppLogEnabled		= 0;
	char			szCurlErrBuf[4096] 	= "";
	char			szAppLogData[256]	= "";
	char			szAppLogDiag[256]	= "";
	char			szErrBuf[128]	    = "";
	RESP_DATA_STYPE	stRespData;

#ifdef DEVDEBUG
	char			szDbgMsg[4096+256]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
/*		 Initialize the response data*/
		rv = initializeRespData(&stRespData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Initialization of response data FAILED"
																, __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			break;
		}
		 /*Set some curl library options before sending the data*/
		curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &stRespData);

		/* Setting the req and its length only if there are existing*/
		if(req && strlen(req) > 0)
		{
			curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, (void *) req);
			curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, reqSize);
		}

		curl_easy_setopt(curlHandle, CURLOPT_ERRORBUFFER, szCurlErrBuf);

		debug_sprintf(szDbgMsg, "%s: Posting the data to the server", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		/*post the data to the server*/
		rv = curl_easy_perform(curlHandle);

		/* Free the headers */
	//	curl_slist_free_all(headers);

		debug_sprintf(szDbgMsg, "%s: curl_easy_perform done, rv = %d", __FUNCTION__, rv);
		EHI_LIB_TRACE(szDbgMsg);
		if(rv == CURLE_OK)
		{
			*resp = stRespData.szMsg;
			*respSize = stRespData.iWritten;
#ifdef DEVDEBUG
			if(strlen(stRespData.szMsg) < 4000)
			{
				debug_sprintf(szDbgMsg, "%s: Response Msg = %s\n", __FUNCTION__,
																		stRespData.szMsg);
				EHI_LIB_TRACE(szDbgMsg);
			}
#endif
			debug_sprintf(szDbgMsg, "%s: Response Len = [%d]", __FUNCTION__,
														stRespData.iWritten);

			EHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Request Successfully Posted and Received Response Successfully from Elavon Host");
				addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSING, szAppLogData, NULL);
			}
		}
		else
		{
			switch(rv)
			{
			case CURLE_UNSUPPORTED_PROTOCOL: /* 1 */
				/*
				 * The URL you passed to libcurl used a protocol that this libcurl does not support.
				 * The support might be a compile-time option that you didn't use,
				 * it can be a misspelled protocol string or just a protocol libcurl has no code for.
				 */
				debug_sprintf(szDbgMsg, "%s: protocol not supported by library",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_UNSUPPORTED_PROTOCOL]");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Unsupported Protocol");
				rv = FAILURE;
				break;

			case CURLE_URL_MALFORMAT: /* 3 */
				/*
				 * The URL was not properly formatted.
				 */
				debug_sprintf(szDbgMsg, "%s: URL not properly formatted",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_URL_MALFORMAT]");
					strcpy(szAppLogDiag, "Please Check the Elavon Host URLs Format, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Invalid URL");
				rv = ERR_CFG_PARAMS;
				break;

			case CURLE_COULDNT_RESOLVE_HOST: /* 6 */
				/*
				 * Couldn't resolve host. The given remote host was not resolved.
				 */
				debug_sprintf(szDbgMsg, "%s: Couldnt resolve server's address",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Resolve Host Address, Error[CURLE_COULDNT_RESOLVE_HOST]");
					strcpy(szAppLogDiag, "Please Check the Elavon Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Host not resolved");
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_COULDNT_CONNECT: /* 7 */
				/*
				 * Failed to connect() to host or proxy.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to connect server",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Connect to Elavon Host, Error[CURLE_COULDNT_CONNECT]");
					strcpy(szAppLogDiag, "Please Check The Elavon Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Connection Failed");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_HTTP_RETURNED_ERROR: /* 22 */
				/*
				 * This is returned if CURLOPT_FAILONERROR is set TRUE
				 * and the HTTP server returns an error code that is >= 400.
				 */
				debug_sprintf(szDbgMsg, "%s: Got HTTP error while posting",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_HTTP_RETURNED_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Elavon Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":HTTP Error");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_WRITE_ERROR: /* 23 */
				/*
				 * An error occurred when writing received data to a local file,
				 * or an error was returned to libcurl from a write callback.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to save the response data", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_WRITE_ERROR]");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Curl Writing Failed");

				rv = FAILURE;
				break;

			case CURLE_OUT_OF_MEMORY: /* 27 */
				/*
				 * A memory allocation request failed.
				 * This is serious badness and things are severely screwed up if this ever occurs.
				 */
				debug_sprintf(szDbgMsg, "%s: Facing memory shortage ", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_OUT_OF_MEMORY]");
					strcpy(szAppLogDiag, "Internal Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Out of Memory");
				rv = FAILURE;
				break;

			case CURLE_OPERATION_TIMEDOUT: /* 28 */
				/*
				 * Operation timeout.
				 * The specified time-out period was reached according to the conditions.
				 */
				debug_sprintf(szDbgMsg, "%s: Timeout happened while receiving", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Timed Out While Waiting For Response From Elavon Host");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				sprintf(szErrBuf, ":Curl Timed out");

				rv = ERR_RESP_TO;
				break;

			case CURLE_SSL_CONNECT_ERROR: /* 35 */
				/*
				 * A problem occurred somewhere in the SSL/TLS handshake.
				 *
				 */
				debug_sprintf(szDbgMsg, "%s: SSL/TLS Handshake FAILED", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_SSL_CONNECT_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Network, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}


				sprintf(szErrBuf, ":Connection Error");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_PEER_FAILED_VERIFICATION: /* 51 */
				/*
				 * The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK.
				 */
				debug_sprintf(szDbgMsg,
							"%s: Server's SSL certificate verification FAILED",
							__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_PEER_FAILED_VERIFICATION]");
					strcpy(szAppLogDiag, "Please Check the SSL CA Certificate on the Terminal, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":SSL certificate verification FAILED");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SEND_ERROR: /* 55 */
				/*
				 * Failed sending network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to post the request data",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_SEND_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Network, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Posting FAILED");

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_RECV_ERROR: /* 56 */
				/*
				 * Failure with receiving network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to receive the response",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_RECV_ERROR]");
					strcpy(szAppLogDiag, "Please Check the Network, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Receiving FAILED");
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SSL_CACERT: /* 60 */
				/*
				 * Peer certificate cannot be authenticated with known CA certificates.
				 */
				debug_sprintf(szDbgMsg,
								"%s: Couldnt authenticate peer certificate",
																__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_SSL_CACERT]");
					strcpy(szAppLogDiag, "Please Check the CA Ceritificate File, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Couldnt authenticate peer certificate");
				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SSL_CACERT_BADFILE: /* 77 */
				/*
				 * Problem with reading the SSL CA cert (path? access rights?)
				 */
				debug_sprintf(szDbgMsg, "%s: Cant find SSL CA certificate [%s]",
												__FUNCTION__, CA_CERT_FILE);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_SSL_CACERT_BADFILE]");
					strcpy(szAppLogDiag, "Please Check SSL CA Certificate Path, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				sprintf(szErrBuf, ":Certificate file not found");

				rv = ERR_CFG_PARAMS;
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Error [%d] occured while posting",
															__FUNCTION__, rv);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed To Post Data to Elavon Host, Error[CURLE_UNSUPPORTED_PROTOCOL]");
					strcpy(szAppLogDiag, "Please Check The Elavon Host URLs, Otherwise Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;
			}

			 /*Deallocate the allocated memory*/
			if(stRespData.szMsg != NULL)
			{
				free(stRespData.szMsg);
				stRespData.szMsg = NULL;
			}
#ifdef DEVDEBUG
			if(strlen(szDbgMsg) < 5000)
			{
				debug_sprintf(szDbgMsg, "%s: Lib Curl Error [%s]",__FUNCTION__, szCurlErrBuf);
				EHI_LIB_TRACE(szDbgMsg);
			}
#endif
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: initializeRespData
 *
 * Description	: This function initializes the structure response structure
 *
 *
 * Input Params	: RESP_DATA_PTYPE
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int initializeRespData(RESP_DATA_PTYPE stRespPtr)
{
	int		rv				= SUCCESS;
	char *	cTmpPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	cTmpPtr = (char *) malloc(8192 * sizeof(char));
	if(cTmpPtr != NULL)
	{
		/* Initialize the memory */
		memset(cTmpPtr, 0x00, 8192 * sizeof(char));
		memset(stRespPtr, 0x00, sizeof(RESP_DATA_STYPE));

		/* Assign the data */
		stRespPtr->iTotSize = 8192;
		stRespPtr->iWritten = 0;
		stRespPtr->szMsg	= cTmpPtr;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: lock_callback
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: None
 * ============================================================================
 */
static void lock_callback(int mode, int type, char *file, int line)
{
#ifdef DEBUG
	//char	szDbgMsg[512]		= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//EHI_LIB_TRACE(szDbgMsg);

  (void)file;
  (void)line;
  if (mode & CRYPTO_LOCK)
  {
	  //debug_sprintf(szDbgMsg, "%s: RC Mutex Locking", __FUNCTION__);
	  //EHI_LIB_TRACE(szDbgMsg);
	  pthread_mutex_lock(&(lockarray[type]));
  }
  else
  {
	  //debug_sprintf(szDbgMsg, "%s:  RC Mutex Un-Locking", __FUNCTION__);
	  //EHI_LIB_TRACE(szDbgMsg);

	  pthread_mutex_unlock(&(lockarray[type]));
  }

  //debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
  //EHI_LIB_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: thread_id
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static unsigned long thread_id(void)
{
  unsigned long ret;

#ifdef DEBUG
	//char	szDbgMsg[512]		= "";
#endif

  ret=(unsigned long)pthread_self();

//  debug_sprintf(szDbgMsg, "%s: Returning [%ld]", __FUNCTION__, ret);
//  EHI_LIB_TRACE(szDbgMsg);

  return(ret);
}

/*
 * ============================================================================
 * Function Name: initializeOpenSSLLocks
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static int initializeOpenSSLLocks()
{
	int		rv					= SUCCESS;
	int		iIndex				= 0;

#ifdef DEBUG
	char	szDbgMsg[512]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	lockarray=(pthread_mutex_t *)OPENSSL_malloc(CRYPTO_num_locks() *
	                                            sizeof(pthread_mutex_t));

	 for (iIndex =0; iIndex < CRYPTO_num_locks(); iIndex++)
	 {
	    pthread_mutex_init(&(lockarray[iIndex]),NULL);
	 }

	 CRYPTO_set_id_callback((unsigned long (*)())thread_id);
	 CRYPTO_set_locking_callback((void (*)())lock_callback);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

#ifdef DEBUG
/*
 * ============================================================================
 * Function Name: curlDbgFunc
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static int curlDbgFunc(CURL * handle, curl_infotype infoType, char *msg,
														size_t size, void * arg)
{
	char	szDbgMsg[8192]	= "";
	switch(infoType)
	{
	case CURLINFO_TEXT:
		sprintf(szDbgMsg, "%s: info-text: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_HEADER_IN:
		sprintf(szDbgMsg, "%s: info-header in: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_HEADER_OUT:
		sprintf(szDbgMsg, "%s: info-header out: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_DATA_IN:
		sprintf(szDbgMsg, "%s: info-data in: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_DATA_OUT:
		sprintf(szDbgMsg, "%s: info-data out: %s", __FUNCTION__, msg);
		break;

	default:
		sprintf(szDbgMsg, "%s: info-default: %s", __FUNCTION__, msg);
		break;
	}

	EHI_LIB_TRACE(szDbgMsg);

	return 0;
}
#endif

