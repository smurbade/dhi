/*
 * ehiConfigParam.c
 *
 *  Created on: 02-March-2015
 *      Author: BLR_SCA_TEAM
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <svc.h>
#include <errno.h>

#include "EHI/ehiHostCfg.h"
#include "EHI/ehicommon.h"
#include "EHI/ehiCfg.h"
#include "EHI/ehiutils.h"
#include "EHI/eGroups.h"
#include "DHI_App/appLog.h"
#include "DHI_App/tranDef.h"


static int 	getElavonHostDefinition();
//static int  setTxnTypeValue();
static int	set47Apifield();
//static int	getElavonValues();
//static int	getElavonBatchNumFromFile();

static ELAVON_PASSTHROUGH_STYPE		stEHIPTDtls;

static EHI_CFG_STYPE eSettings;
//static signed char szTransactionTypes[MAX_PAYMENT_TYPE][MAX_COMMANDS];


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		ehiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         ehistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  ehistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	ehiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			ehiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)


/*
 * ============================================================================
 * Function Name: loadEHIConfigParams
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.usr1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int loadEHIConfigParams()
{
	int		iLen 				= 0;
	int		rv 					= 0;
	int		iVal				= 0;
	int		iValLen				= 0;
	int		iAppLogEnabled		= 0;
	char    szVal[20]			= "";
	char	szTemp[50] 			= "";
	char	szAppLogData[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);
	iValLen = sizeof(szVal);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{

//		rv = getElavonValues();
//		if(rv == SUCCESS)
//		{
//			debug_sprintf(szDbgMsg, "%s: Elavon Values found",__FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//		}
//		else
//		{
//			debug_sprintf(szDbgMsg, "%s: Elavon Values not found",__FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//			break;
//		}

		/* Getting Terminal ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_EHI, TERMINAL_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Terminal ID",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			strncpy(eSettings.szTerminalId, szTemp, sizeof(eSettings.szTerminalId) - 1);

			debug_sprintf(szDbgMsg, "%s: Terminal ID = [%s]",__FUNCTION__, eSettings.szTerminalId);
			EHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Terminal ID not found", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			//rv = ERR_CFG_PARAMS;
			//break;
		}

		/* Getting Lane ID */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_EHI, LANE_ID, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Lane ID",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			strncpy(eSettings.szLaneId, szTemp, sizeof(eSettings.szLaneId) - 1);

			debug_sprintf(szDbgMsg, "%s: Lane ID = [%s]",__FUNCTION__, eSettings.szLaneId);
			EHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Lane ID is not found",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}

		/* EMV Enabled*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, EMVENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				/*Value Found*/
				debug_sprintf(szDbgMsg, "%s:EMV  Not Enabled",__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				eSettings.iEmvEnabled = 0;
			}
			else
			{
				eSettings.iEmvEnabled = 1;
			}
		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : Emv Enabled is not found",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			eSettings.iEmvEnabled = 0;

		}

		/* Contact less Enabled*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, CONTACTLESSENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				/*Value Found*/
				debug_sprintf(szDbgMsg, "%s: Setting Contact less  Not Enabled",__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				eSettings.iContactlessEnabled = 0;
			}
			else
			{
				eSettings.iContactlessEnabled = 1;
			}
		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : Contact less Enabled is not found",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			eSettings.iContactlessEnabled = 0;
		}

		/* Emv Contact less Enabled*/
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DCT, EMVCONTACTLESSENABLED, szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'N') || (*szTemp == 'n'))
			{
				/*Value Found*/
				debug_sprintf(szDbgMsg, "%s: EMV Contact less  Not Enabled",__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				eSettings.iEmvContactlessEnabled = 0;
			}
			else
			{
				eSettings.iEmvContactlessEnabled = 1;
			}
		}
		else
		{
			/*Value not found*/
			debug_sprintf(szDbgMsg,"%s : EMV Contact less Parameter is not found",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			eSettings.iEmvContactlessEnabled = 0;
		}

		/* Getting Terminal currency code */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_EHI, TERM_CURRENCY_TRIGRAPH, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Terminal currency code",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			strncpy(eSettings.szTermCurrencyTriGraph, szTemp, sizeof(eSettings.szTermCurrencyTriGraph) - 1);

			debug_sprintf(szDbgMsg, "%s: Terminal currency code  = [%s]",__FUNCTION__, eSettings.szTermCurrencyTriGraph);
			EHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Term currency code is not found",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}


		/* Getting Location Name */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_EHI, LOCATION_NAME, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Location Name",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			strncpy(eSettings.szLocationName, szTemp, sizeof(eSettings.szLocationName) - 1);

			debug_sprintf(szDbgMsg, "%s: Location Name = [%s]",__FUNCTION__, eSettings.szLocationName);
			EHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Location Name is not found",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			//break;
		}

		/* Getting Chain Code */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_EHI, CHAIN_CODE, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found*/
			debug_sprintf(szDbgMsg, "%s: Setting Chain Code",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			strncpy(eSettings.szChainCode, szTemp, sizeof(eSettings.szChainCode) - 1);

			debug_sprintf(szDbgMsg, "%s: Chain Code = [%s]",__FUNCTION__, eSettings.szChainCode);
			EHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			/* Value not found*/
			debug_sprintf(szDbgMsg, "%s: Chain Code is not found",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			//break;
		}

		/* Get EHI host urls and other communication parameters */
		rv = getElavonHostDefinition();
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in getting the Elavon host definition",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Got Elavon host definition",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

		}

		/* By Default the Number of Hosts available will be 2. But incase
		 * both the URL's are the same, then we set iNumUniqueHosts = 1.
		 * During Posting the data, if both URL's are same then we will
		 * avoid unnecessarily posting to same URL Twice.
		 */
		eSettings.iNumUniqueHosts = 2;
		if(eSettings.hostDef[0].url && eSettings.hostDef[1].url )
		{
			if(!strcmp(eSettings.hostDef[0].url,eSettings.hostDef[1].url))
			{
				debug_sprintf(szDbgMsg, "%s: Both URL's match %s , So 1 Unique Host",__FUNCTION__,eSettings.hostDef[0].url);
				EHI_LIB_TRACE(szDbgMsg);
				eSettings.iNumUniqueHosts = 1;
			}
		}
		debug_sprintf(szDbgMsg, "%s: elavon Num Hosts:%d",__FUNCTION__,eSettings.iNumUniqueHosts);
		EHI_LIB_TRACE(szDbgMsg);

		/* ------------- Check for keep Alive settings ------------------- */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_HDT, KEEP_ALIVE, szTemp, iLen);
		if(rv > 0)
		{
			iVal = atoi(szTemp);
			eSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
			eSettings.iKeepAliveInt = 0;

			if((iVal >= KEEP_ALIVE_DISABLED) && (iVal <= KEEP_ALIVE_DEFINED_TIME))
			{
				eSettings.iKeepAlive = iVal;
			}
			else
			{
				/* keep Alive parameter value not in range*/
				debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive parameter, Setting Default - Disabled", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				eSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
				memset(szVal, 0x00, iValLen);
				sprintf(szVal, "%d", KEEP_ALIVE_DISABLED);
				putEnvFile(SECTION_HDT, KEEP_ALIVE, szVal);
				rv = SUCCESS;
			}

			if(eSettings.iKeepAlive == 2)
			{
				memset(szTemp, 0x00, iLen);
				rv = getEnvFile(SECTION_HDT, KEEP_ALIVE_INTERVAL, szTemp, iLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal >= 1) && (iVal <= 60))
					{
						eSettings.iKeepAliveInt = iVal;
					}
					else
					{
						/* No value found for keep Alive Interval parameter */
						debug_sprintf(szDbgMsg,"%s: Value found for Keep Alive Interval parameter is not in range ;Setting to Default", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);

						eSettings.iKeepAliveInt = DFLT_KEEP_ALIVE_INTERVAL;
						memset(szVal, 0x00, iValLen);
						sprintf(szVal, "%d", DFLT_KEEP_ALIVE_INTERVAL);
						putEnvFile(SECTION_HDT, KEEP_ALIVE_INTERVAL, szVal);
					}
				}
				else
				{
					/* No value found for keep Alive Interval parameter */
					debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive Interval parameter, Setting to Default Value", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					eSettings.iKeepAliveInt = DFLT_KEEP_ALIVE_INTERVAL;
					memset(szVal, 0x00, iValLen);
					sprintf(szVal, "%d", DFLT_KEEP_ALIVE_INTERVAL);
					putEnvFile(SECTION_HDT, KEEP_ALIVE_INTERVAL, szVal);
				}
			}
			rv = SUCCESS;
		}
		else
		{
			/* No value found for keep Alive parameter */
			debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive parameter, Setting to Default - Disabled", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			eSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
			memset(szVal, 0x00, iValLen);
			sprintf(szVal, "%d", KEEP_ALIVE_DISABLED);
			putEnvFile(SECTION_HDT, KEEP_ALIVE, szVal);
			rv = SUCCESS;
		}

//		/* setting Transaction Code value */
//		rv = setTxnTypeValue();
//		if(rv == SUCCESS)
//		{
//			debug_sprintf(szDbgMsg, "%s: Transaction Type Values are Successfully set ",__FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//
//		}
//		else
//		{
//			debug_sprintf(szDbgMsg, "%s: Failed to set Transaction Type Values",__FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//			break;
//		}

		rv = set47Apifield();
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: 47 Field Values are Successfully set ",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to set 47 Field Values",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		rv = loadElavonPTFields();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Load Elavon Pass Through Fields", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			return rv;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Loaded Elavon Pass Through Fields", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Successfully Loaded the Elavon Pass Through Fields.");
				addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSED, szAppLogData, NULL);
			}
		}


		break;

	}

	debug_sprintf(szDbgMsg, "%s: returning [%d]",__FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);
	return rv;
}




/*
* ============================================================================
* Function Name: getTerminalId
*
*
* Description	: This function will fill the iTerminalNUM buffer
*
*
* Input Params	: TerminalNUM Buffer to be filled
*
*
* Output Params: None
* ============================================================================
*/

void getTerminalId(char *pszTerminalNum)
{
	strcpy(pszTerminalNum, eSettings.szTerminalId);
}


/*
 * ============================================================================
 * Function Name: getLaneId
 *
 *
 * Description	: This function will fill the LaneID buffer
 *
 *
 * Input Params	: LaneID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getLaneId(char *pszLaneID)
{
	strcpy(pszLaneID, eSettings.szLaneId);
}

/*
 * ============================================================================
 * Function Name: getTermCurrencyCode
 *
 *
 * Description	: This function will fill the terminal currency code buffer
 *
 *
 * Input Params	: Buffer to be filled with terminal currency code.
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getTermCurrencyCode(char *pszTermCurncyCode)
{
	strcpy(pszTermCurncyCode, eSettings.szTermCurrencyTriGraph);
}

/*
 * ============================================================================
 * Function Name: getLocationName
 *
 *
 * Description	: This function will fill the Location Name buffer
 *
 *
 * Input Params	: LaneID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getLocationName(char *pszLocationName)
{
	strcpy(pszLocationName, eSettings.szLocationName);
}

/*
 * ============================================================================
 * Function Name: getChainCode
 *
 *
 * Description	: This function will fill the Chain Code buffer
 *
 *
 * Input Params	: LaneID Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getChainCode(char *pszChainCode)
{
	strcpy(pszChainCode, eSettings.szChainCode);
}


/*
* ============================================================================
* Function Name: updateTerminalId
*
*
* Description	: This function will update the Terminal id in memory as well as in config file
*
*
* Input Params	: TerminalNUM Buffer to be filled
*
*
* Output Params: None
* ============================================================================
*/

void updateTerminalId(char *pszTerminalId)
{
	if(pszTerminalId != NULL)
	{
		memset(eSettings.szTerminalId, 0x00, sizeof(eSettings.szTerminalId));
		strncpy(eSettings.szTerminalId, pszTerminalId, sizeof(eSettings.szTerminalId) - 1);

		putEnvFile(SECTION_EHI, TERMINAL_ID, pszTerminalId);
	}
}

/*
* ============================================================================
* Function Name: updateChainCode
*
*
* Description	: This function will update the Chain Code in memory as well as in config file
*
*
* Input Params	: TerminalNUM Buffer to be filled
*
*
* Output Params: None
* ============================================================================
*/

void updateChainCode(char *pszChainCode)
{
	if(pszChainCode != NULL)
	{
		memset(eSettings.szChainCode, 0x00, sizeof(eSettings.szChainCode));
		strncpy(eSettings.szChainCode, pszChainCode, sizeof(eSettings.szChainCode) - 1);

		putEnvFile(SECTION_EHI, CHAIN_CODE, pszChainCode);
	}
}


/*
* ============================================================================
* Function Name: updateLocationName
*
*
* Description	: This function will update the Location Name in memory as well as in config file
*
*
* Input Params	: TerminalNUM Buffer to be filled
*
*
* Output Params: None
* ============================================================================
*/

void updateLocationName(char *pszLocationName)
{
	if(pszLocationName != NULL)
	{
		memset(eSettings.szLocationName, 0x00, sizeof(eSettings.szLocationName));
		strncpy(eSettings.szLocationName, pszLocationName, sizeof(eSettings.szLocationName) - 1);

		putEnvFile(SECTION_EHI, LOCATION_NAME, pszLocationName);
	}
}

/*
 * ============================================================================
 * Function Name: getKeepAliveParameter
 *
 * Description	: This function returns the keep Alive parameter value
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getKeepAliveParameter()
{
	return eSettings.iKeepAlive;
}

/*
 * ============================================================================
 * Function Name: getKeepAliveInterval
 *
 * Description	: This function returns the keep Alive parameter value
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getKeepAliveInterval()
{
	return eSettings.iKeepAliveInt;
}
/*
 * ============================================================================
 * Function Name: isEmvEnabled
 *
 * Description	: This function returns the EMV enabled(1)/disabled(0)
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int isEmvEnabled()
{
	return eSettings.iEmvEnabled;
}

/*
 * ============================================================================
 * Function Name: isContactlessEnabled
 *
 * Description	: This function returns the Contactless enabled(1)/disabled(0)
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int isContactlessEnabled()
{
	return eSettings.iContactlessEnabled;
}

/*
 * ============================================================================
 * Function Name: isEmvContactlessEnabled
 *
 * Description	: This function returns the EMV Contactless enabled(1)/disabled(0)
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int isEmvContactlessEnabled()
{
	return eSettings.iEmvContactlessEnabled;
}

/*
 * ============================================================================
 * Function Name: getElavonHostDefinition
 *
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.usr1 file and store them in the static structure.
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
 int getElavonHostDefinition()
{
	int		rv				= 0;
	int		len				= 0;
	int		iCnt			= 0;
	int		jCnt			= 0;
	int		iVal			= 0;
	int		maxLen			= 0;
	char *	tempPtr			= NULL;
	char	szTemp[100]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (iCnt < 2))
	{
		switch(iCnt)
		{
		case PRIMARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_EHI, PRIM_HOST_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Primary elavon Host URL = [%s]",
						__FUNCTION__, szTemp);
				EHI_LIB_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					eSettings.hostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing primary URL */
					debug_sprintf(szDbgMsg,
							"%s - memory allocation failed for Primary elavon URL",
							__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Primary elavon URL not found",
						__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);


			}

			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the elavon host port number ---- */
				rv = getEnvFile(SECTION_EHI, PRIM_HOST_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						eSettings.hostDef[iCnt].portNum = iVal;
						debug_sprintf(szDbgMsg, "%s: Primary host Port Number = [%d]",__FUNCTION__, iVal);
						EHI_LIB_TRACE(szDbgMsg);

					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid prim EHI port [%d]"
								, __FUNCTION__, iVal);
						eSettings.hostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Primary host port not found",
							__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					eSettings.hostDef[iCnt].portNum = 0;
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Connection Timeout ---- */
				rv = getEnvFile(SECTION_EHI, PRIM_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					eSettings.hostDef[iCnt].conTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Primary Conn TimeOut = [%d]",
							__FUNCTION__, iVal);
					EHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
							"%s: Primary conn timeout not found, setting default",
							__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					eSettings.hostDef[iCnt].conTimeOut =
							DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_EHI, PRIM_CONN_TO, szTemp);

				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_EHI, PRIM_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					eSettings.hostDef[iCnt].respTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Primary Resp TimeOut = [%d]",
							__FUNCTION__, iVal);
					EHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
							"%s: Primary resp timeout not found, setting default",
							__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					eSettings.hostDef[iCnt].respTimeOut =DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_EHI, PRIM_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		case SECONDARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_EHI, SCND_HOST_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Secondary elavon Host URL = [%s]",
						__FUNCTION__, szTemp);
				EHI_LIB_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					eSettings.hostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing secondary URL */
					debug_sprintf(szDbgMsg,
							"%s - memory allocation failed for Secondary PWC URL",
							__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Secondary PWC URL not found",
						__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

			}


			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the PWC host port number ---- */
				rv = getEnvFile(SECTION_EHI, SCND_HOST_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						eSettings.hostDef[iCnt].portNum = iVal;
						debug_sprintf(szDbgMsg, "%s: Secondary host port number = [%d]",
													__FUNCTION__, iVal);
											EHI_LIB_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid scnd elavon port [%d]"
								, __FUNCTION__, iVal);
						eSettings.hostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Secondary host port not found",
							__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					eSettings.hostDef[iCnt].portNum = 0;
				}

				/* ---- Get Connection Timeout ---- */
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				rv = getEnvFile(SECTION_EHI, SCND_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					eSettings.hostDef[iCnt].conTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Secondary Conn TimeOut = [%d]",
							__FUNCTION__, iVal);
					EHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
							"%s - Secndary conn timeout not found, setting default",
							__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					eSettings.hostDef[iCnt].conTimeOut =
							DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_EHI, SCND_CONN_TO, szTemp);
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_EHI, SCND_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					eSettings.hostDef[iCnt].respTimeOut = iVal;

					debug_sprintf(szDbgMsg, "%s: Secondary Resp TimeOut = [%d]",
							__FUNCTION__, iVal);
					EHI_LIB_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
							"%s - Secondary resp timeout not found, setting default",
							__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					eSettings.hostDef[iCnt].respTimeOut =
							DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_EHI, SCND_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		default:
			break;
		}

		iCnt++;
	}

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: elavon Host definitions load failed",
				__FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		/* Free any allocated resources */
		for(jCnt = 0; jCnt <= iCnt; jCnt++)
		{
			free(eSettings.hostDef[jCnt].url);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getelavonHostDetails
 *
 * Description	: This function fills up the elavon Host Details for a connection to
 * 				  be setup
 *
 * Input Params	:	HOST_URL_ENUM
 * 					HOSTDEF_PTR_TYPE
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getElavonHostDetails(HOST_URL_ENUM hostType, HOSTDEF_PTR_TYPE hostDefPtr)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(hostDefPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No place holder provided for host details",
				__FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		memset(hostDefPtr, 0x00, sizeof(HOSTDEF_STRUCT_TYPE));

		switch(hostType)
		{
		case PRIMARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting PRIMARY elavon Host details",
					__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(eSettings.hostDef[PRIMARY_URL]), sizeof(HOSTDEF_STRUCT_TYPE));
			break;

		case SECONDARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting SECONDARY elavon Host details",
					__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(eSettings.hostDef[SECONDARY_URL]), sizeof(HOSTDEF_STRUCT_TYPE));

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Incorrect Host Type given",
					__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNumHosts
 *
 *
 * Description	: This function will return number of unique hosts
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */

int getNumUniqueHosts()
{
	return eSettings.iNumUniqueHosts;
}


/*
 * ============================================================================
 * Function Name: updateTimeOutValue
 *
 *
 * Description	: This function will update the timeout value from POS
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: None
 * ============================================================================
 */

void updateTimeOutValue(int iPosTimeout)
{
	eSettings.iTimeoutFromPOS = iPosTimeout;
}



/*
 * ============================================================================
 * Function Name: resetTimeOutValue
 *
 *
 * Description	: This function will reset the timeout value from POS in e structure
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: None
 * ============================================================================
 */

void resetTimeOutValue()
{
	eSettings.iTimeoutFromPOS = 0;
}



/*
 * ============================================================================
 * Function Name: getTimeoutValue
 *
 *
 * Description	: This function will return the timeout value sent from POS
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: int
 * ============================================================================
 */

int getTimeoutValue()
{
	return eSettings.iTimeoutFromPOS;
}

///*
// * ============================================================================
// * Function Name: setTxnCodeValue
// *
// *
// * Description	: This function set the transaction code Value
// *
// *
// * Input Params	: None
// *
// *
// * Output Params: SUCCESS/FAILURE
// * ============================================================================
// */
//int setTxnTypeValue()
//{
//	int		rv 		= SUCCESS;
//	int 	iiCnt 	= 0;
//	int     ijCnt	= 0;
//
///*ROW0: FOR FUNCTION TYPE OTHER THAN PAYMENT EX: REPORT*/
///*ROW1: CREDIT PAYMENT TYPE*/
///*ROW2: DEBIT PAYMENT TYPE*/
///*ROW3: GIFT PAYMENT TYPE*/
///*ROW4: CHECK PAYMENT TYPE*/
///*ROW5: EBT PAYMENT TYPE*/
///*COLOUMN 0: E_AUTH, COLOUMN 1: E_SALE, COLOUMN 2: E_COMPLETION, COLOUMN 3: E_POST_AUTH,  COLOUMN 4: E_VOICE_AUTH, COLOUMN 5: E_RETURN,  COLOUMN 6: E_VOID,  COLOUMN 7: E_ACTIVATE,  COLOUMN 8: E_REDEMPTION,
// *COLOUMN 9: E_ADDVALUE,  COLOUMN 10: E_BALANCE, COLOUMN 11: E_GIFT_CLOSE , COLOUMN 12: E_DEACTIVATE, COLOUMN 13: E_SETTLE_SUMMARY,  COLOUMN 14: E_BATCH_SETTLE, COLOUMN 15: E_TOKEN_QUERY,  COLOUMN 17: E_LASTTRAN
// *COLOUMN 18: FULL REVERSAL*/
//
//	signed char szTempTransactionTypes[MAX_PAYMENT_TYPE][MAX_COMMANDS]	  =  {{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 14, 13, 37, 22, -1},
//																			  {01, 02, 07, 02, 01,  9, 11, -1, -1, -1, -1, -1, -1, -1, -1, 37, 22, 61},
//																			  {-1, 02, -1, -1, -1,  9, 11, -1, -1, -1, -1, -1, -1, -1, -1, 37, 22, 61},
//																			  {01, -1, 07, 02, 01,  9, 11,  9, 02,  9, 24, 02, 02, -1, -1, 37, 22, -1},
//																			  {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 37, -1, -1},
//																			  {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 37, -1, -1}};
//
//#ifdef DEBUG
//	char	szDbgMsg[256]	= "";
//#endif
//
//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	EHI_LIB_TRACE(szDbgMsg);
//
//
//	if(MAX_PAYMENT_TYPE != MAX_NUM_PAYMENT_TYPES || MAX_COMMANDS != MAX_NUM_COMMANDS)
//	{
//		debug_sprintf(szDbgMsg, "%s: No of Payment types or commands changed", __FUNCTION__);
//		EHI_LIB_TRACE(szDbgMsg);
//		rv = FAILURE;
//		return rv;
//	}
//
//	memcpy(szTransactionTypes, szTempTransactionTypes, MAX_NUM_PAYMENT_TYPES*MAX_NUM_COMMANDS);
//
//	for(iiCnt = 0; iiCnt < MAX_PAYMENT_TYPE; iiCnt++)
//	{
//		for(ijCnt = 0; ijCnt < MAX_COMMANDS; ijCnt++)
//		{
//			debug_sprintf(szDbgMsg, "%s: Transaction code for PAYMENT_TYPE:[%d] COMMAND :[%d] == [%d]", __FUNCTION__, iiCnt, ijCnt, szTransactionTypes[iiCnt][ijCnt]);
//			EHI_LIB_TRACE(szDbgMsg);
//		}
//	}
//
//	debug_sprintf(szDbgMsg, "%s: returning [%d]", __FUNCTION__, rv);
//	EHI_LIB_TRACE(szDbgMsg);
//
//	return rv;
//
//}

/*
 * ============================================================================
 * Function Name: set47Apifield
 *
 *
 * Description	: This function set 47 API Field
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int set47Apifield()
{
	int		rv 				= SUCCESS;
	char	szTemp[8+1] 	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);


	/*47.1 - POS Device Capability Indicator*/
	if(isEmvEnabled() == EHI_TRUE && isEmvContactlessEnabled() == EHI_TRUE)
	{
		strcpy(szTemp, "H;");
	}
	else if(isEmvEnabled() == EHI_TRUE)
	{
		strcpy(szTemp, "C;");
	}
	else if(isContactlessEnabled() == EHI_TRUE)
	{
		strcpy(szTemp, "A;");
	}
	else
	{
		strcpy(szTemp, "B;");
	}
	/*47.2 - Cardholder Authentication Capability*/
	strcat(szTemp, "1;");

	/*47.3 - Terminal Card Capture Capability*/
	strcat(szTemp, "1;");

	/*47.4 - Terminal Operating/Location Environment*/
	strcat(szTemp, "1;");

	/*Till 47.4 Fields are fixed so copying it to esetting structure*/

	strcpy(eSettings.sz47Fields1234, szTemp);

	memset(szTemp, 0x00, sizeof(szTemp));


	/*47.11 - Terminal Data Output Capability Indicator*/
	strcpy(szTemp, "3;");

	/*47.12 - PIN Capture Capability Indicator*/
	strcat(szTemp, "6;");

	/*47.13 - POS Device Attendance Indicator*/
	strcat(szTemp, "0;");

	/*47.14 - POS Device Terminal Type*/
	strcat(szTemp, "4");

	strcpy(eSettings.sz47Fields11121314, szTemp);

	debug_sprintf(szDbgMsg, "%s:  47 API Field 1;2;3;4;11;12;13;14[%s%s]", __FUNCTION__, eSettings.sz47Fields1234, eSettings.sz47Fields11121314);
	EHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;

}


/*
 * ============================================================================
 * Function Name: get47APIField1234
 *
 *
 * Description	: This function fills the API Field 47.
 *
 *
 * Input Params	: char Buffer.
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void get47APIField1234(char *szApiField)
{
	strcpy(szApiField, eSettings.sz47Fields1234);
}

/*
 * ============================================================================
 * Function Name: get47APIField11121314
 *
 *
 * Description	: This function fills the API Field 47.
 *
 *
 * Input Params	: char Buffer.
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void get47APIField11121314(char *szApiField)
{

	strcpy(szApiField, eSettings.sz47Fields11121314);
}
///*
// * ============================================================================
// * Function Name: getTxnCode
// *
// *
// * Description	: This function will give the transaction code
// *
// *
// * Input Params	: Enum value of Payment type, Enum value of Command.
// *
// *
// * Output Params: SUCCESS/FAILURE
// * ============================================================================
// */
//char getTxnType(int ipaymenttype, int icommand)
//{
//
//	return szTransactionTypes[ipaymenttype][icommand];
//
//}

/*
 * ============================================================================
 * Function Name: getTransRefNum
 *
 * Description	: This function will generate the Reference number for the current
 * 				  transaction
 *
 *
 * Input Params	: Current Stan Buffer to be filled
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getTransRefNum(char *szRefNum)
{

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	sprintf(szRefNum,"%lu", eSettings.lRefNum);

	debug_sprintf(szDbgMsg,"%s: Ref Num for current transaction is [%s]",__FUNCTION__, szRefNum);
	EHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getUpdatedRefNum
 *
 * Description	: This function will increment the Reference number.
 *
 *
 * Input Params	: Buffer to be filled up with incremented reference number.
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getUpdatedRefNum(char *szRefNum)
{

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	sprintf(szRefNum,"%lu", ++eSettings.lRefNum);

	debug_sprintf(szDbgMsg,"%s: Incremented Ref Num [%s]",__FUNCTION__, szRefNum);
	EHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getElavonBatchNum
 *
 * Description	: This function will return the current batch number.
 *
 *
 * Input Params	:
 *
 *
 * Output Params: None
 * ============================================================================
 */
void getElavonBatchNum(char *szBatchNum)
{

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	sprintf(szBatchNum,"%s", eSettings.szBatchNum);

	debug_sprintf(szDbgMsg,"%s: Batch Num [%s]",__FUNCTION__, szBatchNum);
	EHI_LIB_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: updateElavonBatchNum
 *
 * Description	: This function will update the batch number to memory.
 *
 *
 * Input Params	:
 *
 *
 * Output Params: None
 * ============================================================================
 */
void updateElavonBatchNum(char *szBatchNum)
{
	if(szBatchNum != NULL)
	{
		strcpy(eSettings.szBatchNum, szBatchNum);
	}
}

/*
 * ============================================================================
 * Function Name: getElavonValues
 *
 *
 * Description	: This function will get the Elavon params
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
//static int getElavonValues()
//{
//	int 					rv 							= SUCCESS;
//	char					szBatchNum[10+1]   			= "";
//
//#ifdef DEBUG
//	char	szDbgMsg[256]	= "";
//#endif
//
//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	EHI_LIB_TRACE(szDbgMsg);
//
//	/* Step 1: Getting values from the file ElavonParams.txt */
//	rv = doesFileExist(ELAVON_BATCH_NUM_FILE);
//	if(rv == SUCCESS)
//	{
//		rv = getElavonBatchNumFromFile();
//		if(rv == SUCCESS)
//		{
//			debug_sprintf(szDbgMsg, "%s: Successfully read from the file", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//		}
//		else
//		{
//			debug_sprintf(szDbgMsg, "%s: Unable to read from the file", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//			rv = FAILURE;
//		}
//	}
//	else
//	{
//		/* Setting Values */
//
//		/* Setting Reference Number */
//		//eSettings.lRefNum = 1L;
//		strcpy(eSettings.szBatchNum, "00000000");
//		//sprintf(szTemp, "%ld", eSettings.lRefNum);
//		//strcpy(szRefData, szTemp);
//		//strcat(szRefData,"|");
//		strcpy(szBatchNum, eSettings.szBatchNum);
//		strcat(szBatchNum,"|");
//
//		rv = writeElavonBatchNumToFile(szBatchNum);
//		if(rv == SUCCESS)
//		{
//			debug_sprintf(szDbgMsg, "%s: Successfully written",__FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//		}
//		else
//		{
//			debug_sprintf(szDbgMsg, "%s - Write unsuccessful in file [%s]!",
//					__FUNCTION__, ELAVON_BATCH_NUM_FILE);
//			EHI_LIB_TRACE(szDbgMsg);
//			rv = FAILURE;
//		}
//	}
//
//	debug_sprintf(szDbgMsg, "%s: --- Returning %d---", __FUNCTION__, rv);
//	EHI_LIB_TRACE(szDbgMsg);
//
//	return rv;
//}

/*
 * ============================================================================
 * Function Name: getElavonParamsFromFile
 *
 *
 * Description	: This function will get the Elavon details from the file
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
//static int getElavonBatchNumFromFile()
//{
//	int						rv						= SUCCESS;
////	char*					pszRefNum   			= NULL;
//	char*					pszBatchNum				= NULL;
//	char 					szElavonParams[10+1]	= "";
//	FILE*					fptr					= NULL;
//
//#ifdef DEBUG
//	char	szDbgMsg[256]	= "";
//#endif
//
//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	EHI_LIB_TRACE(szDbgMsg);
//
//
//	memset(szElavonParams,0x00,sizeof(szElavonParams));
//	fptr = fopen(ELAVON_BATCH_NUM_FILE, "r");
//	if(fptr != NULL)
//	{
//		fgets(szElavonParams, sizeof(szElavonParams), fptr);
//		debug_sprintf(szDbgMsg, "%s: szElavonParams is :%s",__FUNCTION__, szElavonParams);
//		EHI_LIB_TRACE(szDbgMsg);
//		fclose(fptr);
//
//		pszBatchNum	 = 	strtok(szElavonParams,"|");
//		//pszBatchNum	 = 	strtok(NULL,"|");
//		/* Value found*/
//
//		debug_sprintf(szDbgMsg, "%s: Checking for Elavon Reference Number", __FUNCTION__);
//		EHI_LIB_TRACE(szDbgMsg);
//
////		/* Check for the value of pszRefNum */
////		if(pszRefNum != NULL)
////		{
////			/* Value found ... Setting Reference No accordingly */
////			debug_sprintf(szDbgMsg, "%s: Setting Reference Number",__FUNCTION__);
////			EHI_LIB_TRACE(szDbgMsg);
////
////			eSettings.lRefNum = atol(pszRefNum);
////			if(eSettings.lRefNum <=  0)
////			{
////				eSettings.lRefNum = 1L;
////			}
////		}
////		else
////		{
////			debug_sprintf(szDbgMsg, "%s: Reference Number Not found",__FUNCTION__);
////			EHI_LIB_TRACE(szDbgMsg);
////		}
//
//		if(pszBatchNum != NULL)
//		{
//			/* Value found ... Setting Batch No accordingly */
//			debug_sprintf(szDbgMsg, "%s: Setting Batch Number",__FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//
//			strcpy(eSettings.szBatchNum, pszBatchNum);
//		}
//		else
//		{
//			debug_sprintf(szDbgMsg, "%s: Batch Number Not found",__FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//
//			strcpy(eSettings.szBatchNum, "00000000");
//		}
//	}
//	else
//	{
//		debug_sprintf(szDbgMsg, "%s - fopen error; file [%s] could not be opened!",
//				__FUNCTION__, ELAVON_BATCH_NUM_FILE);
//		EHI_LIB_TRACE(szDbgMsg);
//		rv = FAILURE;
//	}
//
//	debug_sprintf(szDbgMsg, "%s: returning", __FUNCTION__);
//	EHI_LIB_TRACE(szDbgMsg);
//
//	return rv;
//
//}

/*
 * ============================================================================
 * Function Name: writeElavonParamsToFile
 *
 *
 * Description	: This function will fill Elavon details in to the file
 *
 *
 * Input Params	: string to be filled in to file
 *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int writeElavonBatchNumToFile(char *pszBatchNum)
{
	int		rv 		= SUCCESS;
	FILE*	fptr	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	fptr = fopen(ELAVON_BATCH_NUM_FILE, "w");
	if(fptr != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Writing into the file",__FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		/* Writing the parameters in the file in format: ReferenceNumber| */
		fwrite(pszBatchNum, strlen(pszBatchNum) + 1, 1, fptr);
		fclose(fptr);
		rv = SUCCESS;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - fopen error; file [%s] could not be opened!",
				__FUNCTION__, ELAVON_BATCH_NUM_FILE);
		EHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: resetElavonBatchParams
 *
 * Description	: This function resets all the close batch related params
 *				  Resets the refnum,
 * Input Params	:
 * *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void resetElavonBatchParams()
{
	int			rv 						= SUCCESS;
	int			iAppLogEnabled			= isAppLogEnabled();
	char		szAppLogData[300]		= "";
	FILE*		fptr					= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	eSettings.lRefNum			= 0L;
	strcpy(eSettings.szBatchNum, "00000000");

	/* Updating config variables values in File */
	rv = updateElavonConfigParams(eSettings.szBatchNum);
	if(rv != SUCCESS && iAppLogEnabled)
	{
		strcpy(szAppLogData, "Failed to Update EHI config Parameters to File");
		addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	if(doesFileExist(ELAVON_TRAN_RECORDS) == SUCCESS)
	{
		fptr = fopen(ELAVON_TRAN_RECORDS, "w");
		if(fptr != NULL)
		{
			fclose(fptr);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);
}


/*
 * ============================================================================
 * Function Name: loadElavonPTFields
 *
 * Description	: This function is used to Elavon pass Through Request and response Fields in Memory
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
int loadElavonPTFields()
{
	int					rv					= SUCCESS;
	int					iCnt				= 1;
	int					jCnt				= 0;
	int					iReqTagCnt			= 0;
	int					iResTagCnt			= 0;
	int					iLen				= 0;
	int					iValLen				= 0;
	char				szTemp[256]			= "";
	int					iAppLogEnabled		= isAppLogEnabled();
	int					iPTIndex			= 0;
	char				szAppLogData[300]	= "";
	char				szAppLogDiag[300]	= "";
	char				szKey[256]			= "";
	char				*cPtr				= NULL;
	PASSTHROUGH_PTYPE	pstDHIPTLst		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	memset(&stEHIPTDtls, 0x00, sizeof(ELAVON_PASSTHROUGH_STYPE));
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{

		/* Check if file exist */
		rv = doesFileExist(ELAVON_PASSTHROUGH_FILE_PATH);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Elavon Pass Through File does Not Exist", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Elavon Pass Through INI File Does Not Exist. Not Loading the Fields.");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSED, szAppLogData, NULL);
			}
			rv = SUCCESS;
			break;
		}

		/* Get the total number of tags present in REQUEST section */
		iValLen = sizeof(szTemp);
		memset(szKey, 0x00, sizeof(szKey));
		memset(szTemp, 0x00, sizeof(szTemp));
		sprintf(szKey, "count");
		getEnvFilename(SECTION_ELAVONREQUEST, szKey, szTemp, iValLen, ELAVON_PASSTHROUGH_FILE_PATH);
		if(strlen(szTemp) == 0)
		{
			iReqTagCnt = 0;
		}
		else
		{
			iReqTagCnt = atoi(szTemp);
		}

		/* Get the total number of tags present in RESPONSE section */
		memset(szKey, 0x00, sizeof(szKey));
		memset(szTemp, 0x00, sizeof(szTemp));
		sprintf(szKey, "count");
		getEnvFilename(SECTION_ELAVONRESPONSE, szKey, szTemp, iValLen, ELAVON_PASSTHROUGH_FILE_PATH);
		if(strlen(szTemp) == 0)
		{
			iResTagCnt = 0;
		}
		else
		{
			iResTagCnt = atoi(szTemp);
		}

		stEHIPTDtls.iReqAPICnt = iReqTagCnt;
		stEHIPTDtls.iRespAPICnt = iResTagCnt;
		debug_sprintf(szDbgMsg, "%s: Total [%d] DHI Request fields and [%d] DHI Response fields are configured for pass through", __FUNCTION__, iReqTagCnt, iResTagCnt);
		EHI_LIB_TRACE(szDbgMsg);

		pstDHIPTLst = getDHIPassThroughDtls();

		iCnt = 1;
		if(iReqTagCnt > 0)
		{
			/* Allocate memory for request tag list*/
			stEHIPTDtls.pstPTAPIReqDtls = (ELAVON_KEYVAL_PTYPE) malloc(iReqTagCnt * sizeof(ELAVON_KEYVAL_STYPE));
			if(stEHIPTDtls.pstPTAPIReqDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory Allocation Failed",__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			// Initialize the memory
			memset(stEHIPTDtls.pstPTAPIReqDtls, 0x00, iReqTagCnt * sizeof(ELAVON_KEYVAL_STYPE));

			/* Get all the XML tag details from DHIREQUEST sections which needs to be passed along from SSI to Host Request */
			while(iCnt <= iReqTagCnt)
			{
				memset(szKey, 0x00, sizeof(szKey));
				memset(szTemp, 0x00, sizeof(szTemp));
				sprintf(szKey, "tag%d", iCnt);
				getEnvFilename(SECTION_ELAVONREQUEST, szKey, szTemp, iValLen, ELAVON_PASSTHROUGH_FILE_PATH);
				if(strlen(szTemp) == 0)
				{
					debug_sprintf(szDbgMsg, "%s: Value not Found for [%s] in [%s] section",__FUNCTION__, szKey, SECTION_ELAVONREQUEST);
					EHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				debug_sprintf(szDbgMsg, "%s: iCnt [%d], Key[%s] Value[%s]",__FUNCTION__, iCnt, szKey, szTemp);
				EHI_LIB_TRACE(szDbgMsg);

				if((cPtr = strtok(szTemp, ",")) == NULL)		//API
				{
					rv = FAILURE;
					break;
				}

				iLen = strlen(cPtr);

				stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].key = (char*) malloc(iLen + 1);
				if(stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].key == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: malloc failed",__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}

				memset(stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].key, 0x00, iLen + 1);
				strcpy(stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].key, cPtr);

				if((cPtr = strtok(NULL, ",")) == NULL)		//XML TAG
				{
					rv = FAILURE;
					break;
				}

				stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].iIndex = -1;
				for(jCnt = 0; jCnt < pstDHIPTLst->iReqXMLTagsCnt; jCnt++)
				{
					if(strcmp(pstDHIPTLst->pstPTXMLReqDtls[jCnt].key, cPtr) == 0)
					{
						stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].iIndex = pstDHIPTLst->pstPTXMLReqDtls[jCnt].index;
						break;
					}
				}

				if((cPtr = strtok(NULL, ",")) == NULL)		//MANDATORY_FLAG
				{
					rv = FAILURE;
					break;
				}

				if(! strcmp(cPtr, "TRUE"))
				{
					stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].isMand = EHI_TRUE;
				}
				else
				{
					stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].isMand = EHI_FALSE;
				}

				stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].iSource = DHI;
				stEHIPTDtls.pstPTAPIReqDtls[iCnt - 1].value = NULL;

				iCnt++;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: ELAVONREQUEST Section is Not present",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}


		if(rv != SUCCESS)
		{
			// INI file is not in correct format
			debug_sprintf(szDbgMsg, "%s: Elavon Request Section is not in correct format",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed To Parse Elavon Pass Through Request Fields.");
				strcpy(szAppLogDiag, "Please Check The Format Of DHI Pass Through INI File.");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}
			break;
		}

		iCnt = 1;
		iPTIndex = E_REQRESP_MAX_FIELDS;
		if(iResTagCnt > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Starting Response section ",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			/* Allocate memory for request tag list*/
			stEHIPTDtls.pstPTAPIRespDtls = (EHI_RESP_KEYVAL_PTYPE) malloc(iResTagCnt * sizeof(EHI_RESP_KEYVAL_STYPE));
			if(stEHIPTDtls.pstPTAPIRespDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory Allocation Failed",__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			// Initialize the memory
			memset(stEHIPTDtls.pstPTAPIRespDtls, 0x00, (iResTagCnt * sizeof(EHI_RESP_KEYVAL_STYPE)));

			/* Get all the XML tag details from DHIRESPONSE sections which needs to be passed along from Host Response to SSI */
			while(iCnt <= iResTagCnt)
			{
				memset(szKey, 0x00, sizeof(szKey));
				memset(szTemp, 0x00, sizeof(szTemp));
				sprintf(szKey, "tag%d", iCnt);
				getEnvFilename(SECTION_ELAVONRESPONSE, szKey, szTemp, iValLen, ELAVON_PASSTHROUGH_FILE_PATH);
				if(strlen(szTemp) == 0)
				{
					debug_sprintf(szDbgMsg, "%s: Value not Found for [%s] in [%s] section",__FUNCTION__, szKey, SECTION_ELAVONRESPONSE);
					EHI_LIB_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s]",__FUNCTION__, szKey, szTemp);
				EHI_LIB_TRACE(szDbgMsg);

				if((cPtr = strtok(szTemp, ",")) == NULL)		//API
				{
					rv = FAILURE;
					break;
				}
				iLen = strlen(cPtr);
				stEHIPTDtls.pstPTAPIRespDtls[iCnt - 1].key = (char*) malloc(iLen + 1);
				if(stEHIPTDtls.pstPTAPIRespDtls[iCnt - 1].key == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Memory Allocation Failed",__FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				memset(stEHIPTDtls.pstPTAPIRespDtls[iCnt - 1].key, 0x00, iLen + 1);
				strcpy(stEHIPTDtls.pstPTAPIRespDtls[iCnt - 1].key, cPtr);

				if((cPtr = strtok(NULL, ",")) == NULL)		//XML TAG
				{
					rv = FAILURE;
					break;
				}
				iLen = strlen(cPtr);
				stEHIPTDtls.pstPTAPIRespDtls[iCnt - 1].iDHIIndex= -1;
				stEHIPTDtls.pstPTAPIRespDtls[iCnt - 1].iEhiIndex= -1;
				++iPTIndex;
				for(jCnt = 0; jCnt < pstDHIPTLst->iRespXMLTagsCnt; jCnt++)
				{
					if(strcmp(pstDHIPTLst->pstPTXMLRespDtls[jCnt].key, cPtr) == 0)
					{
						stEHIPTDtls.pstPTAPIRespDtls[iCnt - 1].iDHIIndex = pstDHIPTLst->pstPTXMLRespDtls[jCnt].index;
						stEHIPTDtls.pstPTAPIRespDtls[iCnt - 1].iEhiIndex= iPTIndex;
					}
				}

				iCnt++;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: ELAVONRESPONSE Section is not present",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}

		if(rv != SUCCESS)
		{
			// INI file is not in correct format
			debug_sprintf(szDbgMsg, "%s: Elavon Response Section is not in correct format",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed To Parse Elavon Pass Through Resposne Fields.");
				strcpy(szAppLogDiag, "Please Check The Format Of DHI Pass Through INI File.");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}
			break;
		}

		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getElavonPassThroughDtls
 *
 * Description	: This Function will return the address of the global Elavon Pass through Details
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
ELAVON_PASSTHROUGH_PTYPE getElavonPassThroughDtls()
{
	return &stEHIPTDtls;
}
