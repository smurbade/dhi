/*
 * elavonhash.c
 *
 *  Created on: 09-March-2016
 *      Author: BLR_SCA_TEAM
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "EHI/elavonhash.h"
#include "EHI/ehicommon.h"
#include "EHI/eGroups.h"
#include "EHI/ehiEmvTags.inc"
#include "EHI/ehiRespFields.inc"


static HASHLST_PTYPE hashtab[HASHSIZE];  		// pointer table
static HASHLST_PTYPE Emvhashtab[EMVHASHSIZE];  	// pointer table

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		ehiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         ehistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  ehistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	ehiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			ehiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)


/*
 * ============================================================================
 * Function Name: createHashTableForElavonFields
 *
 * Description	: This Function Will Create the Hash table For ELAVON API FIELDS/ EMV TAGS.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	createHashTableForElavonFields()
{
	int				rv				= SUCCESS;
	int				iTotalCnt		= 0;
	int				iCount			= 0;
	int				iRespCount		= 0;
	HASHLST_PTYPE 	pstHashLst		= NULL;
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	/*STEP 1: Creating Hash Table for Response Fields */
	while(1)
	{
		iTotalCnt = sizeof(genElavonRespLst) / EHI_RESP_KEYVAL_SIZE;

		debug_sprintf(szDbgMsg, "%s: Total Number of Fields to be added to HashTable is %d", __FUNCTION__, iTotalCnt);
		EHI_LIB_TRACE(szDbgMsg);

		/*
		 * Adding Elavon fields to the hash table
		 * Each field number in the elavon response will be stored as KEY
		 * and its Index in the data strutcure will be stored as VALUE
		 */
		for(iCount = 0; iCount < iTotalCnt; iCount++)
		{
			//debug_sprintf(szDbgMsg,"%s: Adding [%s] key with [%d] Value to the Hash Table",__FUNCTION__, genElavonRespLst[iCount].key, iCount);
			//EHI_LIB_TRACE(szDbgMsg);

			pstHashLst = addEntryToElavonHashTable(genElavonRespLst[iCount].key, iCount);
			if(pstHashLst != NULL)
			{
				debug_sprintf(szDbgMsg,"%s: Added [%s] key with [%d] Value to the Hash Table",__FUNCTION__, pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
				EHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				//TO_DO: What to do here!!!
			}
			pstHashLst = NULL;
		}

		pstEHIPTDtls = getElavonPassThroughDtls();
		iRespCount = pstEHIPTDtls->iRespAPICnt;
		for(iCount = 0; iCount < iRespCount; iCount++)
		{
			//debug_sprintf(szDbgMsg,"%s: Adding [%s] key with [%d] Value to the Hash Table",__FUNCTION__, pstEHIPTDtls->pstPTAPIRespDtls[iCount].key, pstEHIPTDtls->pstPTAPIRespDtls[iCount].iEhiIndex);
			//EHI_LIB_TRACE(szDbgMsg);

			pstHashLst = addEntryToElavonHashTable(pstEHIPTDtls->pstPTAPIRespDtls[iCount].key, pstEHIPTDtls->pstPTAPIRespDtls[iCount].iEhiIndex);
			if(pstHashLst != NULL)
			{
				debug_sprintf(szDbgMsg,"%s: Added [%s] key with [%d] Value to the Hash Table",__FUNCTION__, pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
				EHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				//TO_DO: What to do here!!!
			}
			pstHashLst = NULL;
		}

		break;
	}

	/*STEP 2: Creating Hash Table for Emv Tags  */
	while(1)
	{
		iTotalCnt = sizeof(EmvTagLst) / EHI_EMV_KEYVAL_SIZE;

		debug_sprintf(szDbgMsg, "%s: Total Number of Fields to be added to HashTable is %d", __FUNCTION__, iTotalCnt);
		EHI_LIB_TRACE(szDbgMsg);

		/*
		 * Adding Emv Tags fields to the hash table
		 * Each Emv Tag will be stored as KEY
		 * and its Index in the data strutcure will be stored as VALUE
		 */
		for(iCount = 0; iCount < iTotalCnt; iCount++)
		{
			//debug_sprintf(szDbgMsg,"%s: Adding [%s] key with [%d] Value to the Hash Table",__FUNCTION__, EmvTagLst[iCount].key, iCount);
			//EHI_LIB_TRACE(szDbgMsg);

			pstHashLst = addEntryToEmvHashTable(EmvTagLst[iCount].key, iCount);
			if(pstHashLst != NULL)
			{
				debug_sprintf(szDbgMsg,"%s: Added [%s] key with [%d] Value to the Hash Table",__FUNCTION__, pstHashLst->pszTagName, pstHashLst->iTagIndexVal);
				EHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				//TO_DO: What to do here!!!
			}
			pstHashLst = NULL;
		}

		break;
	}



	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: generateHashVal
 *
 * Description	: This function is used to calculate the hash value for the given
 * 				  input string
 * 				  This adds each character value in the string to a scrambled
 * 				  combination of the previous ones and returns the remainder
 * 				  modulo the array size. This is not the best possible hash function,
 * 				  but it is short and effective
 *
 * Input Params	: Input String to Calculate Hash
 *
 * Output Params: Hash Value
 * ============================================================================
 */
unsigned int generateElavonHashVal(char *pszInput)
{
    unsigned int iHashVal = -1;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//EHI_LIB_TRACE(szDbgMsg);

	if(pszInput == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Input to calculate hash!!! ", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return iHashVal;
	}

    for (iHashVal = 0; *pszInput != '\0'; pszInput++)
    {
    	iHashVal = *pszInput + (31 * iHashVal);
    }

//    debug_sprintf(szDbgMsg, "%s: iHashVal [%u] ", __FUNCTION__, iHashVal);
//   EHI_LIB_TRACE(szDbgMsg);

    return iHashVal % HASHSIZE;
}

/*
 * ============================================================================
 * Function Name: lookupHashTable
 *
 * Description	: This function is used to lookup hash table for the given
 * 				  input string
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
HASHLST_PTYPE lookupElavonHashTable(char *pszInPut)
{
	HASHLST_PTYPE pstHashLst;
#if 0
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
#endif
	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//EHI_LIB_TRACE(szDbgMsg);

    for (pstHashLst = hashtab[generateElavonHashVal(pszInPut)]; pstHashLst != NULL; pstHashLst = pstHashLst->next)
    {
        if (strcmp(pszInPut, pstHashLst->pszTagName) == 0)
        {
//        	debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table", __FUNCTION__, pszInPut);
//        	EHI_LIB_TRACE(szDbgMsg);

            return pstHashLst;     // Found
        }
    }

//    debug_sprintf(szDbgMsg, "%s: Did not find Mapping for %s in hash table", __FUNCTION__, pszInPut);
//    EHI_LIB_TRACE(szDbgMsg);

    return NULL;           // NOT found
}

/*
 * ============================================================================
 * Function Name: lookupEmvHashTable
 *
 * Description	: This function is used to lookup hash table for the given
 * 				  input string
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
HASHLST_PTYPE lookupEmvHashTable(char *pszInPut)
{
	HASHLST_PTYPE pstHashLst;
#if 0
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
#endif
	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//EHI_LIB_TRACE(szDbgMsg);

    for (pstHashLst = Emvhashtab[generateElavonHashVal(pszInPut)]; pstHashLst != NULL; pstHashLst = pstHashLst->next)
    {
        if (strcmp(pszInPut, pstHashLst->pszTagName) == 0)
        {
//        	debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table", __FUNCTION__, pszInPut);
//        	EHI_LIB_TRACE(szDbgMsg);

            return pstHashLst;     // Found
        }
    }

//    debug_sprintf(szDbgMsg, "%s: Did not find Mapping for %s in hash table", __FUNCTION__, pszInPut);
//    EHI_LIB_TRACE(szDbgMsg);

    return NULL;           // NOT found
}

/*
 * ============================================================================
 * Function Name: addEntryToHashTable
 *
 * Description	: This function is used to add entry to the hash table
 *
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
HASHLST_PTYPE addEntryToElavonHashTable(char *pszTagName, int iTagIndexVal)
{
	HASHLST_PTYPE pstHashLst;
    unsigned int hashval;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
//	EHI_LIB_TRACE(szDbgMsg);

    if ((pstHashLst = lookupElavonHashTable(pszTagName)) == NULL)        // NOT found
    {
        pstHashLst = (HASHLST_PTYPE)malloc(sizeof(HASHLST_STYPE));

        if ((pstHashLst == NULL) || (pstHashLst->pszTagName = strdup(pszTagName)) == NULL)
        {
        	debug_sprintf(szDbgMsg, "%s: Error while creating memory for Hash list ", __FUNCTION__);
        	EHI_LIB_TRACE(szDbgMsg);
            return NULL;
        }

        hashval = generateElavonHashVal(pszTagName); //Generating the Hash Value

        /*for testing only, remove this log*/
       debug_sprintf(szDbgMsg, "%s: Generated Hash Value for  key [%s] is [%u] ", __FUNCTION__, pszTagName, hashval);
       EHI_LIB_TRACE(szDbgMsg);

        pstHashLst->next = hashtab[hashval];

        hashtab[hashval] = pstHashLst;
    }
    else // Already there
    {
    	debug_sprintf(szDbgMsg, "%s: Already present, need not add it again ", __FUNCTION__);
    	EHI_LIB_TRACE(szDbgMsg);
    }

    pstHashLst->iTagIndexVal = iTagIndexVal;

    return pstHashLst;
}

/*
 * ============================================================================
 * Function Name: addEntryToEmvHashTable
 *
 * Description	: This function is used to add entry to the hash table
 *
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
HASHLST_PTYPE addEntryToEmvHashTable(char *pszTagName, int iTagIndexVal)
{
	HASHLST_PTYPE pstHashLst;
    unsigned int hashval;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
//	EHI_LIB_TRACE(szDbgMsg);

    if ((pstHashLst = lookupElavonHashTable(pszTagName)) == NULL)        // NOT found
    {
        pstHashLst = (HASHLST_PTYPE)malloc(sizeof(HASHLST_STYPE));

        if ((pstHashLst == NULL) || (pstHashLst->pszTagName = strdup(pszTagName)) == NULL)
        {
        	debug_sprintf(szDbgMsg, "%s: Error while creating memory for Hash list ", __FUNCTION__);
        	EHI_LIB_TRACE(szDbgMsg);
            return NULL;
        }

        hashval = generateElavonHashVal(pszTagName); //Generating the Hash Value

        /*for testing only, remove this log*/
       debug_sprintf(szDbgMsg, "%s: Generated Hash Value for  key [%s] is [%u] ", __FUNCTION__, pszTagName, hashval);
       EHI_LIB_TRACE(szDbgMsg);

        pstHashLst->next = Emvhashtab[hashval];

        Emvhashtab[hashval] = pstHashLst;
    }
    else // Already there
    {
    	debug_sprintf(szDbgMsg, "%s: Already present, need not add it again ", __FUNCTION__);
    	EHI_LIB_TRACE(szDbgMsg);
    }

    pstHashLst->iTagIndexVal = iTagIndexVal;

    return pstHashLst;
}


