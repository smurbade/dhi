/*
 * ehiUtils.c
 *
 *  Created on: 00-March-2016
 *      Author: BLR_SCA_TEAM
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <svcsec.h>
#include <sys/timeb.h>	// For struct timeb
#include <sys/stat.h>
#include <pthread.h>
#include <svc.h>


#include "EHI/ehicommon.h"
#include "EHI/ehiutils.h"
#include "DHI_App/appLog.h"

#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		ehiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         ehistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  ehistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	ehiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			ehiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)


/* needed for Base 64 encoding/decoding */
static const char base64Char[] = {
					 /*  0    1    2    3    4    5    6    7    8    9   10 */
						'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
					 /*  11   12  13   14   15   16   17   18   19   20   21 */
						'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
					 /* 22   23   24   25 */
						'W', 'X', 'Y', 'Z',
					 /* 26   27   28   29   30   31   32   33   34   35   36 */
						'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
					 /* 37   38   39   40   41   42   43   44   45   46   47 */
						'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
					 /* 48   49   50   51 */
						'w', 'x', 'y', 'z',
					 /* 52   53   54   55   56   57   58   59   60   61 */
						'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					 /* 62   63 */
						'+', '/'
					};

#ifdef DEBUG
char chEHIDebug 	= NO_DEBUG;
char chEHIMemDebug  = NO_DEBUG;
/*
 * ============================================================================
 * Function Name: EHI_LIB_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void EHI_LIB_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char *			pszMsgPrefix	= "EHI";
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chEHIDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chEHIDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chEHIDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: EHI_LIB_MEM_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void EHI_LIB_MEM_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char *			pszMsgPrefix	= "EHI";
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chEHIMemDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chEHIMemDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chEHIMemDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: setupEHIDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupEHIDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chEHIDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(EHI_DEBUG)) == NULL)
	{
		chEHIDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chEHIDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chEHIDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chEHIDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chEHIDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}

/*
 * ============================================================================
 * Function Name: setupEHIMemDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupEHIMemDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chEHIMemDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(EHI_MEM_DEBUG)) == NULL)
	{
		chEHIMemDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chEHIMemDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chEHIMemDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chEHIMemDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chEHIMemDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}

/*
 * ============================================================================
 * Function Name: closeupDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupDebug(void)
{
	if(chEHIDebug == ETHERNET_DEBUG)
	{
		iCloseDebugSocket();
	}

	return;
}

/*
 * ============================================================================
 * Function Name: closeupMemDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupMemDebug(void)
{
	if(chEHIMemDebug == ETHERNET_DEBUG)
	{
		iCloseDebugSocket();
	}

	return;
}

/*
 * ============================================================================
 * Function Name: setEHIDebugParamsToEnv
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int setEHIDebugParamsToEnv(int * piDbgParamSet)
{
	int 	rv 					= SUCCESS;
	char	szParamValue[20]	= "";
	int		iLen;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iLen = sizeof(szParamValue);

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "EHI_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting EHI_DEBUG variable[%s=%s]", __FUNCTION__, "EHI_DEBUG", szParamValue);
		EHI_LIB_TRACE(szDbgMsg);

		setenv("EHI_DEBUG", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "EHI_MEM_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting EHI_MEM_DEBUG variable[%s=%s]", __FUNCTION__, "EHI_MEM_DEBUG", szParamValue);
		EHI_LIB_TRACE(szDbgMsg);

		setenv("EHI_MEM_DEBUG", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGIP", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGIP variable[%s=%s]", __FUNCTION__, "DBGIP", szParamValue);
		EHI_LIB_TRACE(szDbgMsg);

		setenv("DBGIP", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGPRT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGPRT variable[%s=%s]", __FUNCTION__, "DBGPRT", szParamValue);
		EHI_LIB_TRACE(szDbgMsg);

		setenv("DBGPRT", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DEBUGOPT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DEBUGOPT variable[%s=%s]", __FUNCTION__, "DEBUGOPT", szParamValue);
		EHI_LIB_TRACE(szDbgMsg);

		setenv("DEBUGOPT", szParamValue, 1);
		*piDbgParamSet = 1;
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);
	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: getHILibVersion
 *
 *
 * Description	: This function will give the Version Number and Build Number
 *
 *
 * Input Params	: Version Number and Build Number
 *
 *
 * Output Params: None
 * ============================================================================
 */

void getHILibVersion(char *szLibVersion, char *szHIName)
{
	sprintf(szLibVersion, "%s|%s", EHI_LIB_VERSION, EHI_BUILD_NUMBER);
	sprintf(szHIName, "%s", "Elavon");
}

/*
 * ============================================================================
 * Function Name: doesFileExist
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int doesFileExist(const char *szFullFileName)
{
	int			rv			= SUCCESS;
	struct stat	fileStatus;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	memset(&fileStatus, 0x00, sizeof(fileStatus));

	rv = stat(szFullFileName, &fileStatus);
	if(rv < 0)
	{
		if(errno == EACCES)
		{
			debug_sprintf(szDbgMsg, "%s: Not enough search permissions for %s",
												__FUNCTION__, szFullFileName);
		}
		else if (errno == ENAMETOOLONG)
		{
			debug_sprintf(szDbgMsg, "%s: Name %s is too long", __FUNCTION__,
																szFullFileName);
		}
		else
		{
			/* For ENOENT/ENOTDIR/ELOOP */
			debug_sprintf(szDbgMsg,
						"%s: File - [%s] (Name not correct)/(file doesnt exist)"
												, __FUNCTION__, szFullFileName);
		}
		EHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		/* Check if the file is a regular file */
		if(S_ISREG(fileStatus.st_mode))
		{
			/* Check if the file is not empty */
			if(fileStatus.st_size <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: File [%s] is empty", __FUNCTION__,
																szFullFileName);
				EHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error: File [%s] is not a regular file"
												, __FUNCTION__, szFullFileName);
			EHI_LIB_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: acquireMutexLock
 *
 * Description	: This function is used to acquire a mutex for sychronization
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void acquireElavonMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_lock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to acquire %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		EHI_LIB_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: releaseMutexLock
 *
 * Description	: This function is used to release an already acquired mutex
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void releaseElavonMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_unlock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to release %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		EHI_LIB_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: stripSpaces
 *
 * Description	: This function is responsible for striping trailing and leading
 * 					spaces
 *
 * Input Params	: Buffer, option
 *
 * Output Params: Length / FAILURE
 * ============================================================================
 */
int stripSpaces(char *pszBuffer, int iStripMask)
{
    int  i, iLength;
    char *pchTemp;

    if (iStripMask & STRIP_TRAILING_SPACES)
    {
        // Strip off trailing spaces
        iLength = strlen(pszBuffer);
        for (i = iLength-1; ((*(pszBuffer+i) == ' ') ||(*(pszBuffer+i) == 0x09) ) && (i >= 0); i--)
        {
            ;
        }
        i++;
        *(pszBuffer+i) = 0;
    }

    if (iStripMask & STRIP_LEADING_SPACES)
    {
        // Strip off leading spaces
        for (pchTemp = pszBuffer; *pchTemp == ' ' || *pchTemp == 0x09 ; pchTemp++)
        {
            ;
        }
        strcpy(pszBuffer, pchTemp);
    }

    return (strlen(pszBuffer));
}

/*
 * ============================================================================
 * Function Name: formatFieldValue
 *
 * Description	: This API formats the field value
 *
 * Input Params	: Buffer which contains the Field Value
 *
 * Output Params:
 * ============================================================================
 */
void formatFieldValue(char * pszFldVal)
{
	char	szTmp[50]		= "";
	char *	pchFirst		= NULL;
	char *	pchLast			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Old Value [%s]", __FUNCTION__, pszFldVal);
	EHI_LIB_TRACE(szDbgMsg);

	/* Copying the incoming string into a temporary buffer used for making the
	 * changes. */
	strcpy(szTmp, pszFldVal);

	/* Strip any leading and trailing spaces if present in the field value */
	stripSpaces(szTmp, STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

	memset(pszFldVal, 0x00, strlen(pszFldVal));

	pchFirst = strchr(szTmp, '"');
	if(pchFirst != NULL)
	{
		pchLast = strrchr(szTmp, '"');
		if(pchLast != NULL)
		{
			memcpy(pszFldVal, pchFirst+1, pchLast - pchFirst - 1);
		}
		else
		{
			strcpy(pszFldVal, szTmp);
		}
	}
	else
	{
		strcpy(pszFldVal, szTmp);
	}

	debug_sprintf(szDbgMsg, "%s: New Value [%s]", __FUNCTION__, pszFldVal);
	EHI_LIB_TRACE(szDbgMsg);

	return;
}

/** Encode three bytes using base64 (RFC 3548)
 * @param triple three bytes that should be encoded
 * @param result buffer of four characters where the result is stored
 * */
static void _base64_encode_triple(unsigned char triple[3], char result[4])
{
    int iTripleValue, i;
    iTripleValue = triple[0];
    iTripleValue *= 256;
    iTripleValue += triple[1];
    iTripleValue *= 256;
    iTripleValue += triple[2];

    for (i=0; i<4; i++)
    {
        result[3-i] = base64Char[iTripleValue%64];
        iTripleValue /= 64;
    }
}

/* encode an array of bytes using Base64 (RFC 3548)
* @param source the source buffer
* @param sourcelen the length of the source buffer
* @param target the target buffer
* @return total number of characters on success, 0 otherwise
*/
int encdBase64(uchar * source, int sourcelen, uchar * target, int * outLen)
//static int base64_encode(unsigned char *source, int sourcelen, char *target)
{
    int iRet = 0;

    while (sourcelen >= 3)
    {
        _base64_encode_triple(source, (char *)target);
        sourcelen -= 3;
        source += 3;
        target += 4;
        iRet = iRet + 4;
    }
    /* encode the last one or two characters */
    if (sourcelen > 0)
    {
        unsigned char temp[3];
        memset(temp, 0, sizeof(temp));
        memcpy(temp, source, sourcelen);
        _base64_encode_triple(temp, (char *)target);
        target[3] = '=';
        if (sourcelen == 1)
        target[2] = '=';
        target += 4;
        iRet = iRet + 4;
    }
    /* terminate the string */
    target[0] = 0;

    *outLen = iRet;

    return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: dcdbase64
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int dcdBase64(uchar * inpData, int inpLen, uchar * outData, int * outLen)
{
	int				iCount	= 0;
	int				jCount	= 0;
	int				kCount	= 0;
	int				twoByte	= 0;
	int				oneByte	= 0;
	unsigned char	iStr[4]	= "";
	unsigned char	oStr[3]	= "";
#ifdef DEBUG
	char			szDbgMsg[256] = "";
#endif

	if((inpLen % 4) != 0)
	{
		/* FAILURE */
		debug_sprintf(szDbgMsg, "%s: base 64 decoding failed", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		return FAILURE;
	}

	while(iCount < inpLen)
	{
		iStr[0] = inpData[iCount++];
		iStr[1] = inpData[iCount++];
		iStr[2] = inpData[iCount++];
		iStr[3] = inpData[iCount++];

		for(kCount = 0; kCount < 4; kCount++)
		{
			if((iStr[kCount] >= 'A') && (iStr[kCount] <= 'Z'))
			{
				iStr[kCount] = iStr[kCount] - 'A' + 0;
			}
			else if((iStr[kCount] >= 'a') && (iStr[kCount] <= 'z'))
			{
				iStr[kCount] = (iStr[kCount] - 'a') + 26;
			}
			else if((iStr[kCount] >= '0') && (iStr[kCount] <= '9'))
			{
				iStr[kCount] = iStr[kCount] - '0' + 52;
			}
			else if(iStr[kCount] == '+')
			{
				iStr[kCount] = 62;
			}
			else if(iStr[kCount] == '/')
			{
				iStr[kCount] = 63;
			}
			else if(iStr[kCount] == '=')
			{
				if(kCount == 2)
				{
					twoByte = 1;
				}
				else
				{
					oneByte = 1;
				}

				iStr[kCount] = 0x00;
			}
			else
			{
				return FAILURE;
			}
		}

		oStr[0] = (((iStr[0] & 0x3F) << 2) | ((iStr[1] >> 4) & 0x03)) & 0xFF;
		oStr[1] = (((iStr[1] & 0x0F) << 4) | ((iStr[2] >> 2) & 0x0F)) & 0xFF;
		oStr[2] = (((iStr[2] & 0x03) << 6) | ((iStr[3]) & 0x3F)) & 0xFF;

		if(twoByte == 1)
		{
			/* End of input string with two empty bytes */
			outData[jCount++] = oStr[0];
		}
		else if(oneByte == 1)
		{
			/* End of input string with one empty byte */
			outData[jCount++] = oStr[0];
			outData[jCount++] = oStr[1];
		}
		else
		{
			/* Normal string working */
			outData[jCount++] = oStr[0];
			outData[jCount++] = oStr[1];
			outData[jCount++] = oStr[2];
		}
	}

	* outLen = jCount;

	return SUCCESS;
}
