/*
 * ehiRequest.c
 *
 *  Created on: 04-March-2016
 *      Author: BLR_SCA_TEAM
 */

#include <time.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <svc.h>
#include <errno.h>

#include "EHI/ehicommon.h"
#include "DHI_App/appLog.h"
#include "EHI/ehiCfg.h"
#include "EHI/ehiutils.h"
#include "EHI/ehiRequest.h"
#include "EHI/eGroups.h"
#include "EHI/ehiResp.h"

static pthread_mutex_t gpConfigParamsAcessMutex;

static EHI_BOOL isHostConnected(char *, int, char **, int *);


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		ehiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         ehistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  ehistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	ehiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			ehiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)

/*
 * ============================================================================
 * Function Name: processCreditTransaction
 *
 * Description	: This Function processes the Credit Payment Type Transactions from POS for Elavon Gateway, The Elavon Structure Filling,
 * 				  Request Framing and response Parsing are done within the function.
 *
 * Input Params	: Structure TRANDTLS_PTYPE
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	processCreditTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int 						rv													= SUCCESS;
	int							iCmd												= -1;
	int							iAppLogEnabled										= 0;
	int							iReqSize											= 0;
	int							iRespSize											= 0;
	int							iVSPReg												= EHI_FALSE;
	char						szAppLogData[256]									= "";
	char						szAppLogDiag[256]									= "";
	char 						szRespMsg[256]										= "";
	char*						pszElavonRequest									= NULL;
	char*						pszElavonResp										= NULL;
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls										= getElavonPassThroughDtls();
	ELAVONTRANDTLS_STYPE		pstElavonReqResDetails[E_REQRESP_MAX_FIELDS + pstEHIPTDtls->iRespAPICnt + 1]; // The EHI Enum Index starts from 0, hence we allocate +1


#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	memset(pstElavonReqResDetails, 0x00, sizeof(pstElavonReqResDetails));

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		if(pstDHIDetails[eVSP_REG].elementValue != NULL && (atoi(pstDHIDetails[eVSP_REG].elementValue) == 1))
		{
			iVSPReg = EHI_TRUE;
		}

		if((iVSPReg == EHI_FALSE) && checkForVSPReg(pstDHIDetails) == EHI_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Terminal Id or Chanin Code or LocationName do not match with the Stored. Need to Initiate the VSP Reg Again!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = ERR_DO_VSP_REG;
			break;
		}
		/* Obtaining Transaction Type */
		iCmd = getTransType(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue);
//		if(iCmd < 0)
//		{
//			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction Type !!!", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//			rv = ERR_UNSUPP_CMD;
//			break;
//		}

		if(pstDHIDetails[eTRAN_TYPE].elementValue != NULL)
		{
			if(iCmd == E_FULL_REVERSAL)
			{
				/*For EMV HOST Approved, card rejected case we need to send Transaction type as VOID/REVERSAL*/
				if(strcmp(pstDHIDetails[eTRAN_TYPE].elementValue, "02") == 0)
				{
					strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "11");
				}
				else if(strcmp(pstDHIDetails[eTRAN_TYPE].elementValue, "01") == 0)
				{
					strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "61");
				}
				else if(strcmp(pstDHIDetails[eTRAN_TYPE].elementValue, "09") == 0)
				{
					strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "17");
				}
			}
			else
			{
				strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, pstDHIDetails[eTRAN_TYPE].elementValue);
			}
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Credit %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case E_SALE:
		case E_AUTH:
		case E_POST_AUTH:
		case E_VOICE_AUTH:
		case E_RETURN:
		case E_COMPLETION:
		case E_VOID:
		case E_FULL_REVERSAL:

				if(iVSPReg == EHI_TRUE && iCmd == E_SALE) // This is VSP registration Command
				{
					rv = setCommandForVSPReg(E_AUTH); // Need to Set the command to AUTH for VSP registration with Elavon Gateway
					/* Obtaining Transaction Type after setting/changing the command for VSP Reg*/
//					iCmd = getTransType(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue);
//					if(iCmd < 0)
//					{
//						debug_sprintf(szDbgMsg, "%s: Failed To get Transaction Type !!!", __FUNCTION__);
//						EHI_LIB_TRACE(szDbgMsg);
//						rv = ERR_UNSUPP_CMD;
//						break;
//					}
					strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "01");
				}

			/*Fill Elavon details*/
			rv = fillElavonDtls(pstDHIDetails, pstElavonReqResDetails, szRespMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill Elavon Details", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For EHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

			}
			break;

//			rv = fillElavonReqDtlsforFollowOnTran(pstDHIDetails, pstElavonReqResDetails, szRespMsg);
//			if(rv != SUCCESS)
//			{
//				debug_sprintf(szDbgMsg, "%s: Failed to Fill Elavon Values for follow on", __FUNCTION__);
//				EHI_LIB_TRACE(szDbgMsg);
//				if(iAppLogEnabled)
//				{
//					sprintf(szAppLogData, "Failed to Get Required Details For EHI : rv = [%d]", rv);
//					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
//				}
//			}
//			break;

		default:
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if (rv != SUCCESS)
		{
			break;
		}

		while(1)
		{
			rv  = frameElavonRequest(pstDHIDetails, pstElavonReqResDetails, &pszElavonRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame Elavon request", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to Frame the Request For EHI");
					strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}
			if(iAppLogEnabled)
			{
				if(strlen(pszElavonRequest) > MAX_DATA_SIZE)
				{
					// Incase of Large Elavon request, we break the the request and log it.
					char szAppLogData[MAX_DATA_SIZE + 1] 	= "";

					memset(szAppLogData, 0x00, sizeof(szAppLogData));
					strncpy(szAppLogData, pszElavonRequest, MAX_DATA_SIZE);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, szAppLogData, NULL);

					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest + MAX_DATA_SIZE, NULL);
				}
				else
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest, NULL);
				}
			}

			/* Posting data to the Elavon Host */
			iReqSize = strlen(pszElavonRequest);
			debug_sprintf(szDbgMsg, "%s: Request Len [%d]", __FUNCTION__, iReqSize);
			EHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that Elavon lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			rv = postDataToElavonHost(pszElavonRequest, iReqSize, &pszElavonResp, &iRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				EHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					if(strlen(pszElavonResp) > MAX_DATA_SIZE)
					{
						// Incase of Large Elavon request, we break the the request and log it.
						char szAppLogData[MAX_DATA_SIZE + 1] 	= "";

						memset(szAppLogData, 0x00, sizeof(szAppLogData));
						strncpy(szAppLogData, pszElavonResp, MAX_DATA_SIZE);
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, szAppLogData, NULL);

						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp + MAX_DATA_SIZE, NULL);
					}
					else
					{
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp, NULL);
					}
				}

				rv = parseElavonResponse(pszElavonResp, pstDHIDetails, pstElavonReqResDetails);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the Elavon Response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
//				else
//				{
//					/* Updating config variables values in File */
//					rv = updateElavonConfigParams(pstElavonReqResDetails[E_GATEWAY_BATCH_NUM].elementValue);
//					if(rv != SUCCESS)
//					{
//						strcpy(szAppLogData, "Failed to Update EHI config Parameters to File");
//						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
//					}
//				}
			}
			break;
		}
		break;
	}

//	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
//	{
//		if(pstDHIDetails[eEMV_TAGS].elementValue != NULL)
//		{
//			saveTxnDetailsForFollowOnTxn(pstDHIDetails, pstElavonReqResDetails);
//		}
//	}

	resetTimeOutValue();

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstElavonReqResDetails, pstDHIDetails, szRespMsg);
	}

	if(pszElavonRequest != NULL)
	{
		free(pszElavonRequest);
		pszElavonRequest = NULL;
	}

	if(pszElavonResp != NULL)
	{
		free(pszElavonResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning [%d]", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processDebitTransaction
 *
 * Description	: This Function processes the Debit Payment Type Transactions from POS for Elavon Gateway, The Elavon Structure Filling,
 * 				  Request Framing and response Parsing are done within the function.
 *
 * Input Params	: Structure TRANDTLS_PTYPE
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	processDebitTransaction(TRANDTLS_PTYPE pstDHIDetails)
{
	int 						rv													= SUCCESS;
	int							iCmd												= -1;
	int							iAppLogEnabled										= 0;
	int							iReqSize											= 0;
	int							iRespSize											= 0;
	char						szAppLogData[256]									= "";
	char						szAppLogDiag[256]									= "";
	char 						szRespMsg[256]										= "";
	char*						pszElavonRequest									= NULL;
	char*						pszElavonResp										= NULL;
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls										= getElavonPassThroughDtls();
	ELAVONTRANDTLS_STYPE		pstElavonReqResDetails[E_REQRESP_MAX_FIELDS + pstEHIPTDtls->iRespAPICnt + 1]; // The EHI Enum Index starts from 0, hence we allocate +1


#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();


	memset(pstElavonReqResDetails, 0x00, sizeof(pstElavonReqResDetails));

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		if(checkForVSPReg(pstDHIDetails) == EHI_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Terminal Id or Chanin Code or LocationName do not match with the Stored. Need to Initiate the VSP Reg Again!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = ERR_DO_VSP_REG;
			break;
		}

		/* Obtaining Transaction Type */
		iCmd = getTransType(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue);
//		if(iCmd < 0)
//		{
//			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction Type !!!", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//			rv = ERR_UNSUPP_CMD;
//			break;
//		}

		if(pstDHIDetails[eTRAN_TYPE].elementValue != NULL)
		{
			if(iCmd == E_FULL_REVERSAL)
			{
				/*For EMV HOST Approved, card rejected case we need to send Transaction type as VOID/REVERSAL*/
				if(strcmp(pstDHIDetails[eTRAN_TYPE].elementValue, "02") == 0)
				{
					strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "11");
				}
				else if(strcmp(pstDHIDetails[eTRAN_TYPE].elementValue, "01") == 0)
				{
					strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "61");
				}
				else if(strcmp(pstDHIDetails[eTRAN_TYPE].elementValue, "09") == 0)
				{
					strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "17");
				}
			}
			else
			{
				strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, pstDHIDetails[eTRAN_TYPE].elementValue);
			}
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Debit %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case E_SALE:
		case E_RETURN:
		case E_VOID:
		case E_FULL_REVERSAL:

			/*Fill Elavon details*/
			rv = fillElavonDtls(pstDHIDetails, pstElavonReqResDetails, szRespMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill Elavon Values", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For EHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

			}
			break;


//			rv = fillElavonReqDtlsforFollowOnTran(pstDHIDetails, pstElavonReqResDetails, szRespMsg);
//			if(rv != SUCCESS)
//			{
//				debug_sprintf(szDbgMsg, "%s: Failed to Fill Elavon Values for follow on", __FUNCTION__);
//				EHI_LIB_TRACE(szDbgMsg);
//				if(iAppLogEnabled)
//				{
//					sprintf(szAppLogData, "Failed to Get Required Details For EHI : rv = [%d]", rv);
//					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
//				}
//			}
//			break;

		default:
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if (rv != SUCCESS)
		{
			break;
		}

		while(1)
		{
			rv  = frameElavonRequest(pstDHIDetails, pstElavonReqResDetails, &pszElavonRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame Elavon request", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to frame the request For EHI");
					strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}

			if(iAppLogEnabled)
			{
				if(strlen(pszElavonRequest) > MAX_DATA_SIZE)
				{
					// Incase of Large Elavon request, we break the the request and log it.
					char szAppLogData[MAX_DATA_SIZE + 1] 	= "";

					memset(szAppLogData, 0x00, sizeof(szAppLogData));
					strncpy(szAppLogData, pszElavonRequest, MAX_DATA_SIZE);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, szAppLogData, NULL);

					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest + MAX_DATA_SIZE, NULL);
				}
				else
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest, NULL);
				}
			}

			/* Posting data to the Elavon Host */
			iReqSize = strlen(pszElavonRequest);
			debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
			EHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that CP lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			rv = postDataToElavonHost(pszElavonRequest, iReqSize, &pszElavonResp, &iRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				EHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					if(strlen(pszElavonResp) > MAX_DATA_SIZE)
					{
						// Incase of Large Elavon request, we break the the request and log it.
						char szAppLogData[MAX_DATA_SIZE + 1] 	= "";

						memset(szAppLogData, 0x00, sizeof(szAppLogData));
						strncpy(szAppLogData, pszElavonResp, MAX_DATA_SIZE);
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, szAppLogData, NULL);

						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp + MAX_DATA_SIZE, NULL);
					}
					else
					{
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp, NULL);
					}
				}

				rv = parseElavonResponse(pszElavonResp, pstDHIDetails, pstElavonReqResDetails);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the Elavon Response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
//				else
//				{
//					/* Updating config variables values in File */
//					rv = updateElavonConfigParams(pstElavonReqResDetails[E_GATEWAY_BATCH_NUM].elementValue);
//					if(rv != SUCCESS)
//					{
//						strcpy(szAppLogData, "Failed to Update EHI config Parameters to File");
//						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
//					}
//				}
			}
			break;
		}
		break;
	}

//	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
//	{
//		if(pstDHIDetails[eEMV_TAGS].elementValue != NULL)
//		{
//			saveTxnDetailsForFollowOnTxn(pstDHIDetails, pstElavonReqResDetails);
//		}
//	}

	resetTimeOutValue();

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstElavonReqResDetails, pstDHIDetails, szRespMsg);
	}

	if(pszElavonRequest != NULL)
	{
		free(pszElavonRequest);
		pszElavonRequest = NULL;
	}

	if(pszElavonResp != NULL)
	{
		free(pszElavonResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;

}



/*
 * ============================================================================
 * Function Name: processGiftTransaction
 *
 * Description	: This Function processes the Gift Payment Type Transactions from POS for Elavon Gateway, The Elavon Structure Filling,
 * 				  Request Framing and response Parsing are done within the function
 *
 * Input Params	: Structure TRANDTLS_PTYPE
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	processGiftTransaction(TRANDTLS_PTYPE pstDHIDetails)
{

	int 						rv													= SUCCESS;
	int							iCmd												= -1;
	int							iAppLogEnabled										= 0;
	int							iReqSize											= 0;
	int							iRespSize											= 0;
	char						szAppLogData[256]									= "";
	char						szAppLogDiag[256]									= "";
	char 						szRespMsg[256]										= "";
	char*						pszElavonRequest									= NULL;
	char*						pszElavonResp										= NULL;
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls										= getElavonPassThroughDtls();
	ELAVONTRANDTLS_STYPE		pstElavonReqResDetails[E_REQRESP_MAX_FIELDS + pstEHIPTDtls->iRespAPICnt + 1]; // The EHI Enum Index starts from 0, hence we allocate +1


#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();


	memset(pstElavonReqResDetails, 0x00, sizeof(pstElavonReqResDetails));

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		if(checkForVSPReg(pstDHIDetails) == EHI_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Terminal Id or Chanin Code or LocationName do not match with the Stored. Need to Initiate the VSP Reg Again!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = ERR_DO_VSP_REG;
			break;
		}

		/* Obtaining Transaction Type */
		iCmd = getTransType(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue);
//		if(iCmd < 0)
//		{
//			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction Type !!!", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//			rv = ERR_UNSUPP_CMD;
//			break;
//		}

		if(pstDHIDetails[eTRAN_TYPE].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, pstDHIDetails[eTRAN_TYPE].elementValue);
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Gift %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case E_REDEMPTION:
		case E_BALANCE:
		case E_RETURN:
		case E_ADDVALUE:
		case E_AUTH:
		case E_VOICE_AUTH:
		case E_POST_AUTH:
		case E_ACTIVATE:
		case E_DEACTIVATE:
		case E_GIFT_CLOSE:
		case E_COMPLETION:
		case E_VOID:

			/*Fill Elavon details*/
			rv = fillElavonDtls(pstDHIDetails, pstElavonReqResDetails, szRespMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill Elavon Values", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For EHI : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

			}
			break;


//
//			rv = fillElavonReqDtlsforFollowOnTran(pstDHIDetails, pstElavonReqResDetails, szRespMsg);
//			if(rv != SUCCESS)
//			{
//				debug_sprintf(szDbgMsg, "%s: Failed to Fill Elavon Values for follow on", __FUNCTION__);
//				EHI_LIB_TRACE(szDbgMsg);
//				if(iAppLogEnabled)
//				{
//					sprintf(szAppLogData, "Failed to Get Required Details For EHI : rv = [%d]", rv);
//					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
//				}
//			}
//			break;

		default:
			rv = ERR_UNSUPP_CMD;
			break;
		}

		if (rv != SUCCESS)
		{
			break;
		}

		while(1)
		{
			rv  = frameElavonRequest(pstDHIDetails, pstElavonReqResDetails, &pszElavonRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame Elavon request", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to frame the request For EHI");
					strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}


			if(iAppLogEnabled)
			{
				if(strlen(pszElavonRequest) > MAX_DATA_SIZE)
				{
					// Incase of Large Elavon request, we break the the request and log it.
					char szAppLogData[MAX_DATA_SIZE + 1] 	= "";

					memset(szAppLogData, 0x00, sizeof(szAppLogData));
					strncpy(szAppLogData, pszElavonRequest, MAX_DATA_SIZE);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, szAppLogData, NULL);

					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest + MAX_DATA_SIZE, NULL);
				}
				else
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest, NULL);
				}
			}

			/* Posting data to the Elavon Host */
			iReqSize = strlen(pszElavonRequest);
			debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
			EHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that CP lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			rv = postDataToElavonHost(pszElavonRequest, iReqSize, &pszElavonResp, &iRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				EHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					if(strlen(pszElavonResp) > MAX_DATA_SIZE)
					{
						// Incase of Large Elavon request, we break the the request and log it.
						char szAppLogData[MAX_DATA_SIZE + 1] 	= "";

						memset(szAppLogData, 0x00, sizeof(szAppLogData));
						strncpy(szAppLogData, pszElavonResp, MAX_DATA_SIZE);
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, szAppLogData, NULL);

						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp + MAX_DATA_SIZE, NULL);
					}
					else
					{
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp, NULL);
					}
				}
				rv = parseElavonResponse(pszElavonResp, pstDHIDetails, pstElavonReqResDetails);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the Elavon Response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
//				else
//				{
//					/* Updating config variables values in File */
//					rv = updateElavonConfigParams(pstElavonReqResDetails[E_GATEWAY_BATCH_NUM].elementValue);
//					if(rv != SUCCESS)
//					{
//						strcpy(szAppLogData, "Failed to Update EHI config Parameters to File");
//						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
//					}
//				}
			}
			break;
		}
		break;
	}

//	if(pstDHIDetails[eRESULT_CODE].elementValue != NULL && strcmp(pstDHIDetails[eRESULT_CODE].elementValue, "5") == SUCCESS)
//	{
//		saveTxnDetailsForFollowOnTxn(pstDHIDetails, pstElavonReqResDetails);
//	}

	resetTimeOutValue();

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstElavonReqResDetails, pstDHIDetails, szRespMsg);
	}

	if(pszElavonRequest != NULL)
	{
		free(pszElavonRequest);
		pszElavonRequest = NULL;
	}

	if(pszElavonResp != NULL)
	{
		free(pszElavonResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;


}

/*
 * ============================================================================
 * Function Name: processReportCommand
 *
 * Description	: This Function processes the Report Command from POS for Elavon Gateway, The Elavon Structure Filling,
 * 				  Request Framing and response Parsing are done withing the function
 *
 *
 * Input Params	: Structure TRANDTLS_PTYPE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	processReportCommand(TRANDTLS_PTYPE pstDHIDetails)
{
	int 						rv					                   			= SUCCESS;
	int							iCmd											= 0;
	int							iAppLogEnabled									= 0;
	int							iReqSize										= 0;
	int							iRespSize										= 0;
	char 						szAppLogData[MAX_DATA_SIZE + 1] 				= "";
	char						szAppLogDiag[256]								= "";
	char 						szRespMsg[256]									= "";
	char*						pszElavonRequest								= NULL;
	char*						pszElavonResp									= NULL;
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls									= getElavonPassThroughDtls();
	ELAVONTRANDTLS_STYPE		pstElavonReqResDetails[E_REQRESP_MAX_FIELDS + pstEHIPTDtls->iRespAPICnt + 1]; // The EHI Enum Index starts from 0, hence we allocate +1

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	memset(pstElavonReqResDetails, 0x00, sizeof(pstElavonReqResDetails));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		/* Obtaining Transaction Type */
		iCmd = getTransType(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue);
//		if(iCmd < 0)
//		{
//			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction Type !!!", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//			rv = ERR_UNSUPP_CMD;
//			break;
//		}

		if(pstDHIDetails[eTRAN_TYPE].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, pstDHIDetails[eTRAN_TYPE].elementValue);
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Report %s Transaction", pstDHIDetails[eCOMMAND].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		switch(iCmd)
		{
		case E_SETTLE_SUMMARY:
		case E_CUTOVER:

			rv = fillElavonDtlsForReportCommand(pstDHIDetails, pstElavonReqResDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Get Elavon Details for Report Command", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For Elavon : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
				}
			}
			break;

		case E_TRANS_INQUIRY:

			rv = fillElavonDtlsForTransInquiry(pstDHIDetails, pstElavonReqResDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill Elavon Details for Last Tran", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Get Required Details For Elavon : rv = [%d]", rv);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
				}
			}
			break;

		default:
			rv =ERR_UNSUPP_CMD;
			break;
		}

		if (rv != SUCCESS)
		{
			break;
		}

		while(1)
		{
			rv  = frameElavonRequest(pstDHIDetails, pstElavonReqResDetails, &pszElavonRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame Elavon request", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to Frame the Elavon Gateway Request For Report Command");
					strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}

			if(iAppLogEnabled)
			{
				if(strlen(pszElavonRequest) > MAX_DATA_SIZE)
				{
					memset(szAppLogData, 0x00, sizeof(szAppLogData));
					strncpy(szAppLogData, pszElavonRequest, MAX_DATA_SIZE);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, szAppLogData, NULL);

					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest + MAX_DATA_SIZE, NULL);
				}
				else
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest, NULL);
				}
			}

			/* Posting data to the Elavon Host */
			iReqSize = strlen(pszElavonRequest);
			debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
			EHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that Elavon lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			rv = postDataToElavonHost(pszElavonRequest, iReqSize, &pszElavonResp, &iRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				EHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					if(strlen(pszElavonResp) > MAX_DATA_SIZE)
					{

						memset(szAppLogData, 0x00, sizeof(szAppLogData));
						strncpy(szAppLogData, pszElavonResp, MAX_DATA_SIZE);
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, szAppLogData, NULL);

						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp + MAX_DATA_SIZE, NULL);
					}
					else
					{
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp, NULL);
					}
				}

				rv = parseElavonResponse(pszElavonResp, pstDHIDetails, pstElavonReqResDetails);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the Elavon Response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Framing Report Command Response");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				rv = frameSSIRespForElavonReportCommand(iCmd, pstDHIDetails, pstElavonReqResDetails);
//				if(strcmp(pstElavonReqResDetails[E_GATEWAY_RESP_CODE].elementValue, "0000") == SUCCESS && (iCmd == E_CUTOVER) )
//				{
//					resetElavonBatchParams();
//				}

				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to Frame SSI response for Report Command[%d]", __FUNCTION__, rv);
					EHI_LIB_TRACE(szDbgMsg);
				}
			}
			break;
		}
		break;
	}

	resetTimeOutValue();

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstElavonReqResDetails, pstDHIDetails, szRespMsg);
	}

	if(pszElavonRequest != NULL)
	{
		free(pszElavonRequest);
		pszElavonRequest = NULL;
	}

	if(pszElavonResp != NULL)
	{
		free(pszElavonResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processTokenQueryCommand
 *
 * Description	: This Function processes the Token Query Command from POS for Elavon Gateway, The Elavon Structure Filling,
 * 				  Request Framing and response Parsing are done withing the function
 *
 * Input Params	: Structure TRANDTLS_PTYPE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int	processTokenQueryCommand(TRANDTLS_PTYPE pstDHIDetails)
{
	int 						rv					                    		= SUCCESS;
	int							iCmd											= 0;
	int							iAppLogEnabled									= 0;
	int							iReqSize										= 0;
	int							iRespSize										= 0;
	char 						szRespMsg[256]									= "";
	char 						szAppLogData[MAX_DATA_SIZE + 1] 				= "";
	char						szAppLogDiag[256]								= "";
	char*						pszElavonRequest								= NULL;
	char*						pszElavonResp									= NULL;
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls									= getElavonPassThroughDtls();
	ELAVONTRANDTLS_STYPE		pstElavonReqResDetails[E_REQRESP_MAX_FIELDS + pstEHIPTDtls->iRespAPICnt + 1]; // The EHI Enum Index starts from 0, hence we allocate +1


#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
	memset(szRespMsg, 0x00, sizeof(szRespMsg));

	iAppLogEnabled = isAppLogEnabled();

	memset(pstElavonReqResDetails, 0x00, sizeof(pstElavonReqResDetails));

	while(1)
	{
		rv = setPymtnCmdValue(pstDHIDetails);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Command is not sent in the data structure or Invalid Payment type, can't process the transaction!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "COMMAND Value Not Found or Invalid Payment type");
				strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}
			break;
		}

		/* Obtaining Transaction Type */
		iCmd = getTransType(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue);
//		if(iCmd < 0)
//		{
//			debug_sprintf(szDbgMsg, "%s: Failed To get Transaction Type !!!", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//			rv = ERR_UNSUPP_CMD;
//			break;
//		}

		if(pstDHIDetails[eTRAN_TYPE].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, pstDHIDetails[eTRAN_TYPE].elementValue);
		}

		/* Step 1: Fill and the Details for Token Query Command */
		rv = fillElavonDtlsForTokenQuery(pstDHIDetails, pstElavonReqResDetails, szRespMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Get Values for Token Query", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Get Required Details For EHI : rv = [%d]", rv);
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
			}
			break;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Processing Token Query Command");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		while(1)
		{
			rv  = frameElavonRequest(pstDHIDetails, pstElavonReqResDetails, &pszElavonRequest);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame Elavon request", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "FAILED to Frame the Elavon Gateway Request For Report Command");
					strcpy(szAppLogDiag, "Internal DHI-EHI Library Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}
				break;
			}

			if(iAppLogEnabled)
			{
				if(strlen(pszElavonRequest) > MAX_DATA_SIZE)
				{
					// Incase of Large Elavon request, we break the the request and log it.
					memset(szAppLogData, 0x00, sizeof(szAppLogData));
					strncpy(szAppLogData, pszElavonRequest, MAX_DATA_SIZE);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, szAppLogData, NULL);

					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest + MAX_DATA_SIZE, NULL);
				}
				else
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_REQUEST, pszElavonRequest, NULL);
				}
			}

			/* Posting data to the Elavon Host */
			iReqSize = strlen(pszElavonRequest);
			debug_sprintf(szDbgMsg, "%s: Request Len [%d]\n", __FUNCTION__, iReqSize);
			EHI_LIB_TRACE(szDbgMsg);

			/* Setting the parameter that Elavon lib hit the host*/
			pstDHIDetails[eHIT_HOST].elementValue = strdup("1");

			rv = postDataToElavonHost(pszElavonRequest, iReqSize, &pszElavonResp, &iRespSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post the data to the host[%d]", __FUNCTION__, rv);
				EHI_LIB_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled)
				{
					if(strlen(pszElavonResp) > MAX_DATA_SIZE)
					{
						// Incase of Large Elavon request, we break the the request and log it.
						memset(szAppLogData, 0x00, sizeof(szAppLogData));
						strncpy(szAppLogData, pszElavonResp, MAX_DATA_SIZE);
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, szAppLogData, NULL);

						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp + MAX_DATA_SIZE, NULL);
					}
					else
					{
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_ELAVON_RESPONSE, pszElavonResp, NULL);
					}
				}

				rv = parseElavonResponse(pszElavonResp, pstDHIDetails, pstElavonReqResDetails);
				if(rv != SUCCESS)
				{
					switch(rv)
					{
					case ERR_INV_RESP:
						debug_sprintf(szDbgMsg, "%s: Invalid response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					case FAILURE:
					default:
						debug_sprintf(szDbgMsg, "%s: Error while parsing the Elavon Response", __FUNCTION__);
						EHI_LIB_TRACE(szDbgMsg);
						break;
					}
				}
//				else
//				{
//					/* Updating config variables values in File */
//					rv = updateElavonConfigParams(pstElavonReqResDetails[E_GATEWAY_BATCH_NUM].elementValue);
//					if(rv != SUCCESS)
//					{
//						strcpy(szAppLogData, "Failed to Update EHI config Parameters to File");
//						addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, NULL);
//					}
//				}
			}
			break;
		}

		break;
	}

	if(rv != SUCCESS)
	{
		fillErrorDetails(rv, pstElavonReqResDetails, pstDHIDetails, szRespMsg);
	}

	if(pszElavonRequest != NULL)
	{
		free(pszElavonRequest);
		pszElavonRequest = NULL;
	}

	if(pszElavonResp != NULL)
	{
		free(pszElavonResp);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: processDeviceRegistration
 *
 * Description	: This Function will do dummy device registration with EHI.
 *
 * Input Params	: Structure
 *
 * Output Params:SUCCESS
 * ============================================================================
 */
int	processDeviceRegistration(TRANDTLS_PTYPE pstDHIDetails)
{
	pstDHIDetails[eDEVICEKEY].elementValue 		= strdup("DUMMY DEVICE KEY");
	pstDHIDetails[eCLIENT_ID].elementValue 		= strdup("1234567890");
	pstDHIDetails[eCREDIT_PROC_ID].elementValue = strdup("DELVN");
	pstDHIDetails[eDEBIT_PROC_ID].elementValue 	= strdup("DELVN");
	pstDHIDetails[eGIFT_PROC_ID].elementValue 	= strdup("DELVN");

	return SUCCESS;
}

/*
* ============================================================================
* Function Name: checkHostConnection
*
* Description	: This Function will Tell whether the host is Online Or Not
*
* Input Params	: DHI STRUCTURE
*
* Output Params:  SUCCESS/FAILURE
* ============================================================================
*/
int checkHostConnection(TRANDTLS_PTYPE pstDHIDtls) //Actually nothing will be passed to this function
{
	int							rv 							= 0;
	int							iIndex 						= 0;
	int							iAppLogEnabled				= 0;
	int							iReqSize					= 0;
	int							iRespSize					= 0;
	char *						pszElavonRequest			= NULL;
	char *						pszElavonResp				= NULL;
	char						szAppLogData[256]			= "";
	char						szAppLogDiag[256]			= "";
	PASSTHROUGH_PTYPE			pstDHIPTLst 				= getDHIPassThroughDtls();
	int							iIndexCnt					= eELEMENT_INDEX_MAX + pstDHIPTLst->iReqXMLTagsCnt + pstDHIPTLst->iRespXMLTagsCnt + 1;
	TRANDTLS_STYPE   			pstDHIDetails[iIndexCnt];
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls				= getElavonPassThroughDtls();
	ELAVONTRANDTLS_STYPE		pstElavonReqResDetails[E_REQRESP_MAX_FIELDS + pstEHIPTDtls->iRespAPICnt + 1]; // The EHI Enum Index starts from 0, hence we allocate +1

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
	iAppLogEnabled = isAppLogEnabled();

	memset(&pstDHIDetails, 0x00, (sizeof(TRANDTLS_STYPE) *  (iIndexCnt)) );

	for(iIndex = 0; iIndex <= iIndexCnt; iIndex++)
	{
		pstDHIDetails[iIndex].elementValue = NULL;
	}
	memset(pstElavonReqResDetails, 0x00, sizeof(pstElavonReqResDetails));

	strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "73");
	/* Get Terminal ID */
	getTerminalId(pstElavonReqResDetails[E_TERMINAL_ID].elementValue);
	/* Get Location Name */
	getLocationName(pstElavonReqResDetails[E_LOCATION_NAME].elementValue);
	/* Get Chain Code */
	getChainCode(pstElavonReqResDetails[E_CHAIN_CODE].elementValue);

	frameElavonRequestForChkHostConn(pstDHIDetails, pstElavonReqResDetails, &pszElavonRequest);

	/* Posting data to the Elavon Host */
	iReqSize = strlen(pszElavonRequest);
	debug_sprintf(szDbgMsg, "%s: Request Len [%d]", __FUNCTION__, iReqSize);
	EHI_LIB_TRACE(szDbgMsg);

	if(isHostConnected(pszElavonRequest, iReqSize, &pszElavonResp, &iRespSize) == EHI_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Elavon Host connection is available",__FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Elavon Host Connection is Available");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		rv = 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Elavon Host connection is NOT available",__FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Elavon Host Connection is Not Available");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		rv = 0;
	}

	if(pszElavonRequest != NULL)
	{
		free(pszElavonRequest);
		pszElavonRequest = NULL;
	}

	if(pszElavonResp != NULL)
	{
		free(pszElavonResp);
		pszElavonResp = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: isHostConnected
 *
 * Description	: This Function will Tell whether the host is Online Or Not
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
static EHI_BOOL isHostConnected(char *req, int reqSize, char **resp, int *respSize)
{
	EHI_BOOL	bRv				= EHI_TRUE;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	if(SUCCESS == checkHostConn(req, reqSize, resp, respSize))
	{
		bRv = EHI_TRUE;
	}
	else
	{
		bRv = EHI_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__,
					(bRv == EHI_TRUE)? "TRUE" : "FALSE");
	EHI_LIB_TRACE(szDbgMsg);

	return  bRv;
}

/*
 * ============================================================================
 * Function Name: updateElavonConfigParams
 *
 * Description	: Updating Elavon Config Parameters in ElavonParams.txt file
 *
 * Input Params	: Null
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int updateElavonConfigParams(char *szBatchNumInResp)
{
	extern			int						errno;
	int				rv						= SUCCESS;
	char			szTransBatchData[256]	= "";
	char			szBatchNum[25]			= "";
	FILE*			fptr					= NULL;

#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	acquireElavonMutexLock(&gpConfigParamsAcessMutex, "Updating config params");

	memset(szTransBatchData, 0x00, sizeof(szTransBatchData));
//	getUpdatedRefNum(szTransBatchData);
//	strcat(szTransBatchData, "|");

	memset(szBatchNum, 0x00, sizeof(szBatchNum));
	getElavonBatchNum(szBatchNum);

	if((szBatchNumInResp != NULL) && ((strcmp(szBatchNum, szBatchNumInResp) == SUCCESS) || (strlen(szBatchNumInResp) == 0)))
	{
		strcpy(szTransBatchData, szBatchNum);
	}
	else
	{
		strcpy(szTransBatchData, szBatchNumInResp);
		updateElavonBatchNum(szBatchNumInResp);

		if(doesFileExist(ELAVON_TRAN_RECORDS) == SUCCESS)
		{
			fptr = fopen(ELAVON_TRAN_RECORDS, "w");
			if(fptr != NULL)
			{
				fclose(fptr);
			}
		}

	}
	strcat(szTransBatchData, "|");

	debug_sprintf(szDbgMsg, "%s - writing key=[%s]", __FUNCTION__, szTransBatchData);
	EHI_LIB_TRACE(szDbgMsg);


	/* Writing into the file */
	rv = writeElavonBatchNumToFile(szTransBatchData);
	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully written",__FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - Write unsuccessful in file [%s]!",
				__FUNCTION__, ELAVON_BATCH_NUM_FILE);
		EHI_LIB_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	releaseElavonMutexLock(&gpConfigParamsAcessMutex, "Updating Config params");

	debug_sprintf(szDbgMsg, "%s Returning", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkForVSPReg
 *
 * Description	: Check if we need to processVSP registration with Elavon, For change in locator values (Chain code, location name and Terminal id) with the one stored in the config file,
 * we re do the VSP Registration
 *
 * Input Params	: Null
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int checkForVSPReg(TRANDTLS_PTYPE pstDHIDetails)
{
	char		szTermId[25];
	char		szChainCode[25];
	char		szLocationName[25];
#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	memset(szTermId, 0x00, sizeof(szTermId));
	memset(szChainCode, 0x00, sizeof(szChainCode));
	memset(szLocationName, 0x00, sizeof(szLocationName));

	getTerminalId(szTermId);
	getChainCode(szChainCode);
	getLocationName(szLocationName);

	debug_sprintf(szDbgMsg, "%s: szTermId [%s] ", __FUNCTION__, szTermId);
	EHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: szChainCode [%s]", __FUNCTION__, szChainCode);
	EHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: szLocationName [%s]", __FUNCTION__, szLocationName);
	EHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDetails[eTERMID].elementValue != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: pstDHIDetails[eTERMID].elementValue [%s]", __FUNCTION__, pstDHIDetails[eTERMID].elementValue);
		EHI_LIB_TRACE(szDbgMsg);
	}

	if(pstDHIDetails[eCHAIN_CODE].elementValue)
	{
		debug_sprintf(szDbgMsg, "%s: pstDHIDetails[eCHAIN_CODE].elementValue [%s]", __FUNCTION__, pstDHIDetails[eCHAIN_CODE].elementValue);
		EHI_LIB_TRACE(szDbgMsg);
	}

	if(pstDHIDetails[eLOCATION_NAME].elementValue != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: pstDHIDetails[eLOCATION_NAME].elementValue [%s]", __FUNCTION__, pstDHIDetails[eLOCATION_NAME].elementValue);
		EHI_LIB_TRACE(szDbgMsg);
	}

	if(pstDHIDetails != NULL && (((pstDHIDetails[eTERMID].elementValue != NULL) && (strcmp(pstDHIDetails[eTERMID].elementValue, szTermId) != SUCCESS)) || ((pstDHIDetails[eCHAIN_CODE].elementValue != NULL)
			&& (strcmp(pstDHIDetails[eCHAIN_CODE].elementValue, szChainCode) != SUCCESS)) || ((pstDHIDetails[eLOCATION_NAME].elementValue != NULL) && (strcmp(pstDHIDetails[eLOCATION_NAME].elementValue, szLocationName) != SUCCESS))))
	{
		debug_sprintf(szDbgMsg, "%s: Mismatch in Locator Values Noticed. Need to Initiate the VSP Reg Again!!!", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		return EHI_TRUE;
	}

	debug_sprintf(szDbgMsg, "%s: Locator Values Match, No need for VSP Registration", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return EHI_FALSE;
}
