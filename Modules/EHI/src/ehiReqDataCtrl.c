/*
 * ehiReqDataCtrl.c
 *
 *  Created on: 09-March-2016
 *      Author: BLR_SCA_TEAM
 */

#include<stdio.h>
#include<string.h>
#include <stdlib.h>

#include "DHI_App/appLog.h"
#include "EHI/ehicommon.h"
#include "EHI/eGroups.h"
#include "EHI/ehiCfg.h"
#include "EHI/ehiReqLists.inc"
#include "EHI/ehiEmvTags.inc"
#include "EHI/elavonhash.h"
#include "EHI/xmlUtils.h"
#include "EHI/ehiutils.h"
#include "EHI/csv.h"

static  ELAVONPYMTNCMD_STYPE 	stPymtncmd;

static int	getAccountDtls(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE, char*);
static int	getTxnAmountDtls(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	getElavonGenReqDtls(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	getLevel2Dtls(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	getAVSFieldDetails(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	getMiscellaneousDtls(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	getPinDtls(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	getTransactionQualifier(char *, char *);
static int  getPOSDataCode(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static void getPOSDataCodeforFollowonTran(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
//static int	getStoredValueFunction(ELAVONTRANDTLS_PTYPE);


static int	frameCommonDtlsAPIList(xmlNodePtr, TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	frameElavonPTAPIList(xmlNodePtr, TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	frameElavonConcatanatedPTList(xmlNodePtr, TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
//static int	frameElavonRequestForReportCmd(xmlNodePtr, TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);

static int	getPresentFlag(char *);
static int	convertTrackData2Manual(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	fillEmvDetails(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
//static int  getEMVTagsFromFile(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int	parseEmvTags(char *, ELAVONTRANDTLS_PTYPE);
static int  getCardEntryMode(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int 	changeinEMVTagValLength(ELAVONTRANDTLS_PTYPE);
/*static int  getConvertedBusinessDate(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static int  getConvertedDate(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);*/
static int	Hex2Bin (unsigned char *, int , unsigned char *);
static void getCVMResults(ELAVONTRANDTLS_PTYPE);
static void	getAccnExpFromTrackData(char*, int, char*);
static void getChipConditionCode(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
static void changePosEntryModeBySrvcCodeCheck(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);


#undef strdup				// we are doing undef as we are defining our own function

#define malloc(size)		ehiMalloc(size, __LINE__, (char*)__FUNCTION__)
#define strdup(ptr)         ehistrdup((char*)ptr, __LINE__, (char*)__FUNCTION__)
#define strndup(ptr, size)  ehistrndup((char*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define realloc(ptr, size)	ehiReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__)
#define free(ptr)			ehiFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__)


/*
 * ============================================================================
 * Function Name: setPymtnCmdValue
 *
 * Description	: This function will set the command value and payment type value for every transaction
 *
 * Input Params	: DHI structure containing the details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setPymtnCmdValue(TRANDTLS_PTYPE pstDHIDetails)
{
	int					rv		       = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --------enter--------", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	memset(&stPymtncmd, 0x00, sizeof(stPymtncmd));

	/*Checking For Payment type*/
	if(pstDHIDetails[ePAYMENT_TYPE].elementValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:Payment Type Not Present", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		stPymtncmd.iPaymentType = 0;	// making it as zero for FUNCTION_TYPE other than PAYMENT
	}
	else
	{
		if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "CREDIT") == SUCCESS)
		{
			stPymtncmd.iPaymentType = CREDIT;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "DEBIT") == SUCCESS)
		{
			stPymtncmd.iPaymentType = DEBIT;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "GIFT") == SUCCESS)
		{
			stPymtncmd.iPaymentType = STOREDVALUE;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "EBT") == SUCCESS)
		{
			stPymtncmd.iPaymentType = EBT;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "CHECK") == SUCCESS)
		{
			stPymtncmd.iPaymentType = CHECK;
		}
		else if(strcmp(pstDHIDetails[ePAYMENT_TYPE].elementValue, "ADMIN") == SUCCESS)
		{
			stPymtncmd.iPaymentType = 0;		// for token query
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:Invalid Payment Type", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			rv = ERR_INV_FLD_VAL;
			return rv;
		}
	}

	/*checking for Command*/
	if(pstDHIDetails[eCOMMAND].elementValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:Command Not Present Cannot Process Further", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		rv = ERR_NO_FLD;
		return rv;
	}
	if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "PRE_AUTH") == SUCCESS)
	{
		stPymtncmd.iCommand = E_AUTH;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "COMPLETION") == SUCCESS)
	{
		stPymtncmd.iCommand = E_COMPLETION;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "SALE") == SUCCESS)
	{
		if(stPymtncmd.iPaymentType == STOREDVALUE)
		{
			stPymtncmd.iCommand = E_REDEMPTION;
		}
		else
		{
			stPymtncmd.iCommand = E_SALE;
		}
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "CREDIT") == SUCCESS)
	{
		stPymtncmd.iCommand = E_RETURN;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "VOID") == SUCCESS)
	{
		if(pstDHIDetails[eEMV_REVERSAL_TYPE].elementValue != NULL)
		{
			stPymtncmd.iCommand = E_FULL_REVERSAL;
		}
		else
		{
			stPymtncmd.iCommand = E_VOID;
		}
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "POST_AUTH") == SUCCESS)
	{
		stPymtncmd.iCommand = E_POST_AUTH;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "VOICE_AUTH") == SUCCESS)
	{
		stPymtncmd.iCommand = E_VOICE_AUTH;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "ACTIVATE") == SUCCESS)
	{
		stPymtncmd.iCommand = E_ACTIVATE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "ADD_VALUE") == SUCCESS)
	{
		stPymtncmd.iCommand = E_ADDVALUE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "BALANCE") == SUCCESS)
	{
		stPymtncmd.iCommand = E_BALANCE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "DEACTIVATE") == SUCCESS)
	{
		stPymtncmd.iCommand = E_DEACTIVATE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "GIFT_CLOSE") == SUCCESS)
	{
		stPymtncmd.iCommand = E_GIFT_CLOSE;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "SETTLESUMMARY") == SUCCESS)
	{
		stPymtncmd.iCommand = E_SETTLE_SUMMARY;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "CUTOVER") == SUCCESS)
	{
		stPymtncmd.iCommand = E_CUTOVER;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "TOKEN_QUERY") == SUCCESS)
	{
		stPymtncmd.iCommand = E_TOKEN_QUERY;
	}
	else if(strcmp(pstDHIDetails[eCOMMAND].elementValue, "LAST_TRAN") == SUCCESS)
	{
		stPymtncmd.iCommand = E_TRANS_INQUIRY;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s:Invalid Command", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		rv = ERR_UNSUPP_CMD;
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: setCommandForVSPReg
 *
 * Description	: This function will set the command sent in the argument for the transacction.
 *
 * Input Params	: Command to be set
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setCommandForVSPReg(int iCommand)
{
	int				rv		      				 = SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --------enter--------", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	stPymtncmd.iCommand = iCommand;

	debug_sprintf(szDbgMsg, "%s: The converted command is %d", __FUNCTION__, iCommand);
	EHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTransType
 *
 * Description	: This function will get the transaction code and puts in the CPTRANDTLS_STYPE structure
 *
 * Input Params	: buffer containing the transaction type
 * 				  buffer to be filled with the TransCode
 * 				  integer indicating payment type
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getTransType()
{
	int				rv		      				 = SUCCESS;
//	signed	char    cTxnType					 = 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --------enter--------", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);


//	cTxnType = getTxnType(stPymtncmd.iPaymentType, stPymtncmd.iCommand);
//
//	if(cTxnType < 0)
//	{
//		debug_sprintf(szDbgMsg, "%s: Invalid Transaction Type ", __FUNCTION__);
//		EHI_LIB_TRACE(szDbgMsg);
//		rv = ERR_INV_FLD;
//		return rv;
//	}
//	else
//	{
//		sprintf(szElavonTransType, "%02d", cTxnType);
//	}

	rv = stPymtncmd.iCommand;

//	debug_sprintf(szDbgMsg, "%s: The converted command is %s", __FUNCTION__, szElavonTransType);
//	EHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillElavonDtls
 *
 * Description	: Fills up the required Elavon details structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillElavonDtls(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails, char* szRespMsg)
{
	int		rv		       = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		/*STEP 1 : Filling Account Details*/
		rv = getAccountDtls(pstDHIDtls, pstElavonReqResDetails, szRespMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill account Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 2: Filling Transaction Amount Details*/
		rv = getTxnAmountDtls(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill Amount Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 3: Filling Transaction Qualifier*/
		rv = getTransactionQualifier(pstDHIDtls[ePAYMENT_TYPE].elementValue, pstElavonReqResDetails[E_TRANS_QUALIFIER].elementValue);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the Transaction Qualifier", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 4: Filling Stored Value Function*/
		/*if(stPymtncmd.iPaymentType == STOREDVALUE)
		{
			rv = getStoredValueFunction(pstElavonReqResDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get the stored ", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}*/

		/*STEP 5 : Filling Miscellaneous Details*/
		rv = getMiscellaneousDtls(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the Miscellaneous details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 6 : Filling General Request Details*/
		rv = getElavonGenReqDtls(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the general details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 7: Filling PIN Details*/
		rv = getPinDtls(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the PIN details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 8: Filling EMV related Details*/
		if(stPymtncmd.iPaymentType == CREDIT || stPymtncmd.iPaymentType == DEBIT)
		{
			rv = fillEmvDetails(pstDHIDtls, pstElavonReqResDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get the EMV details", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}
		/*STEP 9 :Filling AVS Related Feilds*/
		rv = getAVSFieldDetails(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get AVS Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 10: Filling Level2(Purchase Txn) Deails*/
		rv = getLevel2Dtls(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get Level 2 Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}


		/*STEP 10: Field 0047 : POS DATA Code*/
		rv = getPOSDataCode(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get POS Data Code", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s:Returning [%d]", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillElavonReqDtlsforFollowOnTran
 *
 * Description	: This Function fills the details required for follow on transaction.
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	fillElavonReqDtlsforFollowOnTran(TRANDTLS_PTYPE pstDHIDetails, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails, char* szRespMsg)
{

	int			rv									    = SUCCESS;
	int			iFound				 					= EHI_FALSE;
	int			iCount								    = 0;
	int			iIndex							        = 0;
	int			iTotal								    = 0;
	char*		pszBuffer						    	= NULL;
//	char 		szIssuerScripRes[10] 					= "";
	FILE*		fptr				 					= NULL;
	int			iEHIIndices[]						    = {E_PREVIOUS_TRAN_TYPE, E_CARD_TOKEN, E_CARD_EXP_DATE, E_TRANS_AMOUNT, E_AUTH_CODE};

	int			iDHIIndices[]							= {eEMV_TAGS, eEMV_TAGS_RESP};

#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);


	while(1)
	{
		if(pstDHIDetails[eCTROUTD].elementValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: CTROUTD not found", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_FLD_VAL;
			break;
		}

		fptr = fopen(ELAVON_TRAN_RECORDS, "r");
		if(fptr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: --- File not available --- ", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}

		while((pszBuffer = csvgetline(fptr)) != NULL)
		{
			pszBuffer = csvfield(iCount);
			if(pszBuffer != NULL)
			{
				if(strcmp(pszBuffer, pstDHIDetails[eCTROUTD].elementValue) == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Found the CTROUTD in the records", __FUNCTION__);
					EHI_LIB_TRACE(szDbgMsg);

					iFound = EHI_TRUE;
					iCount++;
					break;
				}
			}
		}

		if(iFound == EHI_FALSE)
		{
			debug_sprintf(szDbgMsg, "%s: CTROUTD not found in the records", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = ERR_INV_CTROUTD;

			break;
		}

		/*Copying the original Reference number*/
		strcpy(pstElavonReqResDetails[E_REF_NUMBER].elementValue, pszBuffer);

		iTotal = sizeof(iEHIIndices)/sizeof(int);
		for(iIndex = 0; iIndex < iTotal; iIndex++)
		{
			pszBuffer = csvfield(iCount++);
			debug_sprintf(szDbgMsg, "%s: Elavon Field %d - %s", __FUNCTION__, iIndex, pszBuffer);
			EHI_LIB_TRACE(szDbgMsg);

			if(pszBuffer && strlen(pszBuffer) > 0)
			{
				strcpy(pstElavonReqResDetails[iEHIIndices[iIndex]].elementValue, pszBuffer);
			}
		}

		if(pstDHIDetails[eEMV_TAGS_RESP].elementValue)
		{
			free(pstDHIDetails[eEMV_TAGS_RESP].elementValue);
			pstDHIDetails[eEMV_TAGS_RESP].elementValue = NULL;
		}

		/*Copying DHI Fields from the File into DHI Structure*/
		iTotal = sizeof(iDHIIndices)/sizeof(int);
		for(iIndex = 0; iIndex < iTotal; iIndex++)
		{
			pszBuffer = csvfield(iCount++);
			debug_sprintf(szDbgMsg, "%s: DHI Field %d - %s", __FUNCTION__, iIndex, pszBuffer);
			EHI_LIB_TRACE(szDbgMsg);

			if(pszBuffer && strlen(pszBuffer) > 0 && !pstDHIDetails[iDHIIndices[iIndex]].elementValue)
			{
				pstDHIDetails[iDHIIndices[iIndex]].elementValue = (char *)malloc(strlen(pszBuffer)+1);
				memset(pstDHIDetails[iDHIIndices[iIndex]].elementValue, 0x00, strlen(pszBuffer)+1);
				strcpy(pstDHIDetails[iDHIIndices[iIndex]].elementValue, pszBuffer);
			}
		}


//		/*IF E_ORIGINAL_REF_NUM is present then we are treating that as reference number, irrespective of any other condition*/
//
//		if(strlen(pstElavonReqResDetails[E_ORIGINAL_REF_NUM].elementValue) > 0)
//		{
//			strcpy(pstElavonReqResDetails[E_REF_NUMBER].elementValue, pstElavonReqResDetails[E_ORIGINAL_REF_NUM].elementValue);
//		}

		/*Changing the gateway transaction type as Void return(17) if the Previous transaction type is 09(Return)
		 * For sale(02) and Prior Authorized sale(0 7) void of sale (11) is already placed in getTxnType function */

		if(stPymtncmd.iCommand == E_VOID && strcmp(pstElavonReqResDetails[E_PREVIOUS_TRAN_TYPE].elementValue, "09") == 0)
		{
			memset(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, 0x00, sizeof(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue));
			strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "17");
		}


		/*Changing the gateway transaction type as Full Authorization reversal(61) if the Previous transaction type is 01(AUTH)
		 * For void of AUTH transactions, we need to send transaction type as 61 */
		if(stPymtncmd.iCommand == E_VOID && strcmp(pstElavonReqResDetails[E_PREVIOUS_TRAN_TYPE].elementValue, "01") == 0)
		{
			memset(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, 0x00, sizeof(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue));
			strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "61");
		}

		/*STEP 1 : Filling Account Details:
		 * Filling account details if Details come from SCA that Should be Given higher Priority if not Elavon structure would have filled from the file already*/
		if(pstDHIDetails[eACCT_NUM].elementValue != NULL || pstDHIDetails[eCARD_TOKEN].elementValue != NULL || pstDHIDetails[eTRACK_DATA].elementValue != NULL)
		{
			memset(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, 0x00, sizeof(pstElavonReqResDetails[E_CARD_TOKEN].elementValue));
			memset(pstElavonReqResDetails[E_CARD_EXP_DATE].elementValue, 0x00, sizeof(pstElavonReqResDetails[E_CARD_EXP_DATE].elementValue));

			rv = getAccountDtls(pstDHIDetails, pstElavonReqResDetails, szRespMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed To Get the Account Details For Follow On transactions", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}

		/*STEP 2: Filling Transaction Amount Details:
		 * Filling Transaction Amount Details if Details come from SCA that Should be Given higher Priority if not Elavon structure would have filled from the file already*/
		if(pstDHIDetails[eTRANS_AMOUNT].elementValue != NULL)
		{
			rv = getTxnAmountDtls(pstDHIDetails, pstElavonReqResDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed To Get the Amount Details For Follow On transactions", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}

		/*STEP 3: Filling Transaction Qualifier*/
		rv = getTransactionQualifier(pstDHIDetails[ePAYMENT_TYPE].elementValue, pstElavonReqResDetails[E_TRANS_QUALIFIER].elementValue);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the Transaction Qualifier", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 4: Filling Stored Value Function*/
		/*if(stPymtncmd.iPaymentType == STOREDVALUE)
		{
			rv = getStoredValueFunction(pstElavonReqResDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get the stored ", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}*/

		/*STEP 5 : Filling Miscellaneous Details*/
		rv = getMiscellaneousDtls(pstDHIDetails, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the Miscellaneous details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 6 : Filling General Request Details*/
		rv = getElavonGenReqDtls(pstDHIDetails, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the general details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 8: Filling EMV related Details*/
		if(pstDHIDetails[eEMV_TAGS].elementValue != NULL)
		{
			rv = fillEmvDetails(pstDHIDetails, pstElavonReqResDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get the EMV details", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				break;
			}
		}

		/*STEP 9 :Filling AVS Related Feilds*/
		rv = getAVSFieldDetails(pstDHIDetails, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get AVS Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 10: Filling Level2(Purchase Txn) Deails*/
		rv = getLevel2Dtls(pstDHIDetails, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get Level 2 Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 10: Field 0047 : POS DATA Code*/
		rv = getPOSDataCode(pstDHIDetails, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get POS Data Code", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: --- returning [%d] ---", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: fillElavonDtlsForReportCommand
 *
 * Description	: Fills up the required Elavon details structure for Report Commands
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillElavonDtlsForReportCommand(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int		rv		       = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	getElavonGenReqDtls(pstDHIDtls, pstElavonReqResDetails); // Request Details that Fetched From Config File.

	debug_sprintf(szDbgMsg, "%s:returning [%d]", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillElavonDtlsForTokenQuery
 *
 * Description	: Fills up the required Elavon details data structure for  token query
 *
 * Input Params	:structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: Returns the total number of details
 * ============================================================================
 */
int fillElavonDtlsForTokenQuery(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE	pstElavonReqResDetails, char* szRespMsg)
{
	int				rv 									= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{

		rv = getAccountDtls(pstDHIDtls, pstElavonReqResDetails, szRespMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fill account Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		strcpy(pstElavonReqResDetails[E_TRANS_AMOUNT].elementValue, "0.00");

		strcpy(pstElavonReqResDetails[E_TRANS_QUALIFIER].elementValue, "010");

		if(pstDHIDtls[ePAYMENT_TYPE].elementValue != NULL)
		{
			rv = getTransactionQualifier(pstDHIDtls[ePAYMENT_TYPE].elementValue, pstElavonReqResDetails[E_TRANS_QUALIFIER].elementValue);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get the Transaction Qualifier(Payment type is ADMIN)", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				//break;
			}
		}

		/*Filling Miscellaneous Details*/
		rv = getMiscellaneousDtls(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the Miscellaneous details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		rv = getElavonGenReqDtls(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the general details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*Filling AVS Related Feilds*/
		rv = getAVSFieldDetails(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get AVS Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*Filling PIN Details*/
		rv = getPinDtls(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the PIN details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*Filling Level2(Purchase Txn) Deails*/
		rv = getLevel2Dtls(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get Level 2 Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*STEP 10: Field 0047 : POS DATA Code*/
		rv = getPOSDataCode(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get POS Data Code", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s:Returning [%d]", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: fillElavonDtlsForTransInquiry
 *
 * Description	: Fills up the required Elavon details structure for Last Tran(Transaction Inquiry) Command. With Elavon, Last Tran command will be sent as Transaction Inquiry to hit the host
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int fillElavonDtlsForTransInquiry(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int		rv		       = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eTRANS_AMOUNT].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_TRANS_AMOUNT].elementValue, pstDHIDtls[eTRANS_AMOUNT].elementValue);
	}

	if(pstDHIDtls[eCTROUTD].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_REF_NUMBER].elementValue, pstDHIDtls[eCTROUTD].elementValue);
	}

	if(pstDHIDtls[eCARD_TOKEN].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, pstDHIDtls[eCARD_TOKEN].elementValue);
	}
	else if(pstDHIDtls[eACCT_NUM].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_ACCT_NUM].elementValue, pstDHIDtls[eACCT_NUM].elementValue);
	}

	getElavonGenReqDtls(pstDHIDtls, pstElavonReqResDetails); // Request Details that Fetched From Config File.

	/*Field 0013 : Date*/
	/*if(pstDHIDtls[eDATE].elementValue != NULL)
	{
		getConvertedDate(pstDHIDtls, pstElavonReqResDetails);
	}*/

	/*Field 0014 : Date*/
	/*if(pstDHIDtls[eTIME].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_TIME].elementValue,  pstDHIDtls[eTIME].elementValue);
	}*/

	debug_sprintf(szDbgMsg, "%s:returning %d", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getAccountDtls
 *
 * Description	: Fills up the required Elavon details structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getAccountDtls(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails, char* szRespMsg)
{
	int				rv 									= SUCCESS;
	int 			iIndicator 							= 0;
	int				iPresentFlag						= 0;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if((stPymtncmd.iCommand == E_FULL_REVERSAL) && (pstDHIDtls[eTRACK_DATA].elementValue != NULL))
		{
			/* For EMV REVERSALS, if SCA sends the track data, EHI needs to send it as Account Number and Expiry Date*/
			debug_sprintf(szDbgMsg, "%s: Received track data on EMV Reversal, converting to Acc number and Exp date ", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			convertTrackData2Manual(pstDHIDtls, pstElavonReqResDetails);
			break;
		}

		if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
		{
			iPresentFlag = getPresentFlag(pstDHIDtls[ePRESENT_FLAG].elementValue);
			if(iPresentFlag < 0)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Present Flag !! Cannot proceed further!!", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				rv = ERR_INV_FLD_VAL;
				break;
			}
		}
		else if(pstDHIDtls[eCARD_TOKEN].elementValue != NULL)
		{
			iPresentFlag    = 1;
		}
//		else
//		{
//			debug_sprintf(szDbgMsg, "%s: Present Flag / Card token missing !! Cannot proceed further!!", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//			rv = ERR_FLD_REQD;
//			break;
//		}

		/* First setting the indicator of the track if it exists
		 * IF it is a manual then setting the indicator to 0
		 * */
		if(iPresentFlag < 3)
		{
			iIndicator = 0;
		}
		else if (pstDHIDtls[eTRACK_INDICATOR].elementValue != NULL)
		{
			iIndicator = atoi(pstDHIDtls[eTRACK_INDICATOR].elementValue);
		}
		else
		{
			rv = ERR_FLD_REQD;
			break;
		}


		/* Getting Track1 or Track2 Data */
		if(pstDHIDtls[eTRACK_DATA].elementValue != NULL)
		{
//			if(iIndicator != 2 && stPymtncmd.iPaymentType == DEBIT)
//			{
//				strcpy(szRespMsg, "TRACK2 Data Required For This Transaction");	// For Debit Payment type Only Track2 is supported
//				rv = ERR_INV_FLD;
//				break;
//
//			}

			if(((pstDHIDtls[eTRACK_DATA].elementValue[0] == '%') || (pstDHIDtls[eTRACK_DATA].elementValue[0] == ';')) &&
					(pstDHIDtls[eTRACK_DATA].elementValue[strlen(pstDHIDtls[eTRACK_DATA].elementValue)-1] == '?'))
			{
				strcpy(pstElavonReqResDetails[E_TRACK_DATA].elementValue, pstDHIDtls[eTRACK_DATA].elementValue);
			}
			else
			{
				if(iIndicator == 1)
				{
					strcpy(pstElavonReqResDetails[E_TRACK_DATA].elementValue,"%");
					strcat(pstElavonReqResDetails[E_TRACK_DATA].elementValue, pstDHIDtls[eTRACK_DATA].elementValue);
					strcat(pstElavonReqResDetails[E_TRACK_DATA].elementValue, "?");

				}
				else if(iIndicator == 2)
				{
					strcpy(pstElavonReqResDetails[E_TRACK_DATA].elementValue,";");
					strcat(pstElavonReqResDetails[E_TRACK_DATA].elementValue, pstDHIDtls[eTRACK_DATA].elementValue);
					strcat(pstElavonReqResDetails[E_TRACK_DATA].elementValue, "?");
				}
			}
		}
		else
		{
			/* Copying the account number */
			if(pstDHIDtls[eACCT_NUM].elementValue != NULL)
			{
				strcpy(pstElavonReqResDetails[E_ACCT_NUM].elementValue, pstDHIDtls[eACCT_NUM].elementValue);
			}
			else if(pstDHIDtls[eCARD_TOKEN].elementValue != NULL)
			{
//				if(strncmp(pstDHIDtls[eCARD_TOKEN].elementValue, "ID:", 3) == 0)
//				{
					strcpy(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, pstDHIDtls[eCARD_TOKEN].elementValue);
//				}
//				else
//				{
//					strcpy(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, "ID:");
//					strcat(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, pstDHIDtls[eCARD_TOKEN].elementValue);
//				}
			}

			if(pstDHIDtls[eEXP_MONTH].elementValue != NULL && pstDHIDtls[eEXP_YEAR].elementValue != NULL)
			{
				strcpy(pstElavonReqResDetails[E_CARD_EXP_DATE].elementValue, pstDHIDtls[eEXP_MONTH].elementValue);
				strcat(pstElavonReqResDetails[E_CARD_EXP_DATE].elementValue, pstDHIDtls[eEXP_YEAR].elementValue);
			}

			/* Get CCV Indicator */
			if(pstDHIDtls[eCVV2].elementValue != NULL )
			{
				strcpy(pstElavonReqResDetails[E_CVV2_CID_CVC].elementValue, pstDHIDtls[eCVV2].elementValue);
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTxnAmountDtls
 *
 * Description	: Fills up the required Elavon details structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getTxnAmountDtls(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int			rv 		= 		SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[eTRANS_AMOUNT].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_TRANS_AMOUNT].elementValue, pstDHIDtls[eTRANS_AMOUNT].elementValue);
	}
	/* Not throwing error if Transaction amount is present in the incoming SSI request.
	else if(!(stPymtncmd.iCommand == E_BALANCE || stPymtncmd.iCommand == E_DEACTIVATE || stPymtncmd.iCommand == E_GIFT_CLOSE || stPymtncmd.iCommand == E_ACTIVATE))
	{
		debug_sprintf(szDbgMsg, "%s: Transaction Amount is Missing ", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		rv = ERR_FLD_REQD;
		return rv;
	}*/

	if(pstDHIDtls[eCASHBACK_AMNT].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_CASHBACK_AMT].elementValue,pstDHIDtls[eCASHBACK_AMNT].elementValue);
	}

	if(pstDHIDtls[eTIP_AMOUNT].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_TIP_AMOUNT].elementValue,pstDHIDtls[eTIP_AMOUNT].elementValue);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTransactionQualifier
 *
 * Description	: Fills the Transaction qualifier in ELavon structure.
 *
 * Input Params	: Buffer containing the Payment type, buffer to be filled with transaction Qualifier.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getTransactionQualifier(char * szDHIPymtType, char *szElavonTransQualifier)
{
	int				rv 				=	SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: Enter",__FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	if(szDHIPymtType == NULL)
	{
		return ERR_FLD_REQD;
	}

	if(stPymtncmd.iPaymentType == CREDIT)
	{
		strcpy(szElavonTransQualifier, "010");
	}
	else if(stPymtncmd.iPaymentType == DEBIT)
	{
		strcpy(szElavonTransQualifier, "030");
	}
	else if(stPymtncmd.iPaymentType == STOREDVALUE)
	{
		strcpy(szElavonTransQualifier, "050");
	}
	else if(stPymtncmd.iPaymentType == CHECK)
	{
		strcpy(szElavonTransQualifier, "020");
	}
	else
	{
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg,"%s: Returning [%d]",__FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getStoredValueFunction
 *
 * Description	: Fills the Stored Value Function in ELavon structure.
 *
 * Input Params	: structure Containing the Elavon Details.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
/*static int getStoredValueFunction(ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int				rv 				=	SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: Enter",__FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		if(stPymtncmd.iCommand == E_VOID)
		{
			if((strcmp(pstElavonReqResDetails[E_PREVIOUS_TRAN_TYPE].elementValue,"02") == 0) && (strcmp(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue,"3") == 0))
			{
				//For GIFT VOID_DEACTIVATE we Need Send Gateway Transaction Type as 17, and stored value function as 1 instead of 3
				strcpy(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue, "17");
				strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue,"1");
			}
			else
			{
				//For all other cases the respective value will be filled from file
			}
			break;
		}

		switch(stPymtncmd.iCommand)
		{
		case E_REDEMPTION:
			strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue, "5");
			break;
		case E_RETURN:
			strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue, "4");
			break;
		case E_ADDVALUE:
			strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue, "3");
			break;
		case E_POST_AUTH:
			strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue, "7");
			break;
		case E_COMPLETION:
			strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue, "7");
			break;
		case E_GIFT_CLOSE:
			strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue, "6");
			break;
		case E_ACTIVATE:
			strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue, "1");
			break;
		case E_DEACTIVATE:
			strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue, "3");
			break;
		case E_AUTH:
			break;			//this field is not applicable to Auth, voice auth, balance
		case E_VOICE_AUTH:
			break;
		case E_BALANCE:
			break;
		default:
			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg,"%s: Returning [%d]",__FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}*/

/*
 * ============================================================================
 * Function Name: getMiscellaneousDtls
 *
 * Description	: Fills up the required Elavon details structure with AVS Field Details
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getMiscellaneousDtls(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int 		rv = SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{
		/*Field 0006 : Auth Code*/
//		if(stPymtncmd.iCommand == E_POST_AUTH || stPymtncmd.iCommand == E_VOICE_AUTH)
//		{
			if(pstDHIDtls[eAUTH_CODE].elementValue != NULL)
			{
				strcpy(pstElavonReqResDetails[E_AUTH_CODE].elementValue, pstDHIDtls[eAUTH_CODE].elementValue);
			}
//			else
//			{
//				debug_sprintf(szDbgMsg, "%s: Auth Code is Missing", __FUNCTION__);
//				EHI_LIB_TRACE(szDbgMsg);
//				rv = ERR_FLD_REQD;
//				break;
//			}
//		}
//		else if(stPymtncmd.iCommand == E_COMPLETION && pstDHIDtls[eAUTH_CODE].elementValue != NULL)
//		{
//			/*If Auth Code Comes from SCA, That Should be Given Higher Priority or else it would have filled from File*/
//			strcpy(pstElavonReqResDetails[E_AUTH_CODE].elementValue, pstDHIDtls[eAUTH_CODE].elementValue);
//		}
//		else if(stPymtncmd.iCommand == E_VOID || stPymtncmd.iCommand == E_FULL_REVERSAL)
//		{
//			/*For Void Command Auth Code is not Required, so Memsetting the Auth code (Filled while loading the fields from File)*/
//			memset(pstElavonReqResDetails[E_AUTH_CODE].elementValue, 0x00, sizeof(pstElavonReqResDetails[E_AUTH_CODE].elementValue));
//		}


//		/*Field 0007 : Reference Number*/
//		if(stPymtncmd.iCommand == E_VOID || stPymtncmd.iCommand == E_COMPLETION || stPymtncmd.iCommand == E_FULL_REVERSAL)
//		{
//			/*T_POLISETTYG1 : Getting the reference number for storing the transaction details using unique Reference Number,
//			 * (Reference Number Will be incremented for all the transactions, but for Follow on transactions we will be using existing Reference number in the Host request,
//			 * the incremented reference number(see getUpdatedRefNum Function) will be used for storing the transactions details).*/
//			getTransRefNum(pstElavonReqResDetails[E_FOLLOWON_REF_NUM].elementValue);
//		}
//		else
//		{
//			getTransRefNum(pstElavonReqResDetails[E_REF_NUMBER].elementValue);
//		}

		if(pstDHIDtls[eCTROUTD].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_REF_NUMBER].elementValue,  pstDHIDtls[eCTROUTD].elementValue);
		}

		/*Field 0012 : Force Flag*/
		if(pstDHIDtls[eFORCE_FLAG].elementValue != NULL)
		{
			if(strcmp(pstDHIDtls[eFORCE_FLAG].elementValue, "TRUE") == 0)
			{
				strcpy(pstElavonReqResDetails[E_FORCE_FLAG].elementValue , "1");
			}
		}

		/*Field 0023 : Debit Account Type*/
		if(pstDHIDtls[eDEBIT_ACC_TYPE].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_DEBIT_ACCT_TYPE].elementValue, pstDHIDtls[eDEBIT_ACC_TYPE].elementValue);
		}

		/*Field 0117 : Gift/Stored Value Transaction Code*/
		if((stPymtncmd.iPaymentType == STOREDVALUE) && (pstDHIDtls[eSTORED_VALUE_FUNC].elementValue != NULL))
		{
			strcpy(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue, pstDHIDtls[eSTORED_VALUE_FUNC].elementValue);
		}

		/*Field 0025 : Business Date*/
		/*if(pstDHIDtls[eBUSINESS_DATE].elementValue != NULL)
		{
			getConvertedBusinessDate(pstDHIDtls, pstElavonReqResDetails);
		}*/

		/*Field 0013 : Date*/
		/*if(pstDHIDtls[eDATE].elementValue != NULL)
		{
			getConvertedDate(pstDHIDtls, pstElavonReqResDetails);
		}*/

		/*Field 0014 : Date*/
		/*if(pstDHIDtls[eTIME].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_TIME].elementValue,  pstDHIDtls[eTIME].elementValue);
		}*/

		/*Field 0126 : Track Indicator is not a part of Elavon request, but in case of connection error or timeout error, we need to send the field back to POS.*/
		if(pstDHIDtls[eTRACK_INDICATOR].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_TRACK_IND].elementValue,  pstDHIDtls[eTRACK_INDICATOR].elementValue);
		}

		/*Field 0052 : Transponder/Proximity Indicator*/
		/*Field 0054 : POS Entry Mode*/
		rv = getCardEntryMode(pstDHIDtls, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get Card Entry Mode", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			break;
		}

		/*Field 0110: Cashier Id */
		/*if(pstDHIDtls[eCASHIER_NUM].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_CASHIER_ID].elementValue, pstDHIDtls[eCASHIER_NUM].elementValue);
		}*/

		/*Field 0401 : Invoice Number*/
		/*if(pstDHIDtls[eINVOICE].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_INVOICE_NUM].elementValue, pstDHIDtls[eINVOICE].elementValue);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invoice is missing", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			strcpy(pstElavonReqResDetails[E_INVOICE_NUM].elementValue, "0000000000"); //Hard coding Zeros for VSP and DUMMY SALE(Need to check whether it is ok)
			rv = ERR_FLD_REQD;
			break;
		}*/

//		/*Field 0647 : Partial Authorization Acceptance*/
//		if(pstDHIDtls[ePARTIAL_AUTH].elementValue != NULL && (!(stPymtncmd.iCommand == E_VOID || stPymtncmd.iCommand == E_COMPLETION || stPymtncmd.iCommand == E_FULL_REVERSAL)))
//		{
//			strcpy(pstElavonReqResDetails[E_PARTIAL_AUTH_ACCEPTANCE].elementValue, pstDHIDtls[ePARTIAL_AUTH].elementValue);
//		}

//		/*Field 1008 : Requests Token*/
//		if(strlen(pstElavonReqResDetails[E_CARD_TOKEN].elementValue) == 0)
//		{
//			strcpy(pstElavonReqResDetails[E_MASKED_ACCT_NUM].elementValue, "ID:");
//		}

		if(pstDHIDtls[eELAVON_SERIAL_NUM].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_DEVICE_SERIAL_NUM].elementValue, pstDHIDtls[eELAVON_SERIAL_NUM].elementValue);
		}
		else
		{
			if(pstDHIDtls[eSERIAL_NUM].elementValue != NULL)
			{
				strcpy(pstElavonReqResDetails[E_DEVICE_SERIAL_NUM].elementValue, pstDHIDtls[eSERIAL_NUM].elementValue);
			}
		}

		/*Field 5004 : Encryption Provider ID*/
		if(pstDHIDtls[eELAVON_ENCRYPTION_TYPE].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_ENC_PROVIDER_ID].elementValue, pstDHIDtls[eELAVON_ENCRYPTION_TYPE].elementValue);
		}
		else
		{
			strcpy(pstElavonReqResDetails[E_ENC_PROVIDER_ID].elementValue, "V1");
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;

}


/* ============================================================================
* Function Name: getElavonGenReqDtls
*
* Description	: This function is responsible for getting and filling the gen
*				  request details in the Elavon data structure.
*
* Input Params	: structure containing the dhi details
* 				  structure to be filled with the required Elavon details
*
* Output Params: SUCCESS/FAILURE
* ============================================================================
*/
static int getElavonGenReqDtls(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int  	rv 				= SUCCESS;
	int 	iTimeout		= 0;
	char 	szTemp[3+1]		= "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	/* Get Terminal ID */
	if(pstDHIDtls[eTERMID].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_TERMINAL_ID].elementValue, pstDHIDtls[eTERMID].elementValue);
	}
//	else
//	{
//		getTerminalId(pstElavonReqResDetails[E_TERMINAL_ID].elementValue);
//	}

	/* Get Location Name */
	if(pstDHIDtls[eLOCATION_NAME].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_LOCATION_NAME].elementValue, pstDHIDtls[eLOCATION_NAME].elementValue);
	}
//	else
//	{
//		getLocationName(pstElavonReqResDetails[E_LOCATION_NAME].elementValue);
//	}

	/* Get Chain Code */
	if(pstDHIDtls[eCHAIN_CODE].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_CHAIN_CODE].elementValue, pstDHIDtls[eCHAIN_CODE].elementValue);
	}
//	else
//	{
//		getChainCode(pstElavonReqResDetails[E_CHAIN_CODE].elementValue);
//	}

	/* Get Lane Id, We Map Lane Id to Unique Device Id*/
	//if(pstDHIDtls[eLANE].elementValue != NULL)
	//{
		//strcpy(pstElavonReqResDetails[E_UNIQUE_DEVICE_ID].elementValue, pstDHIDtls[eLANE].elementValue);
	//}
	/*We get The Lane Id From Elavon request, Sending a wrong Lane Id can be a issue with certain Card brands
	else
	{
		getLaneId(pstElavonReqResDetails[E_UNIQUE_DEVICE_ID].elementValue);
	}*/

	/*if(pstDHIDtls[eCASHIER_NUM].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_CASHIER_ID].elementValue, pstDHIDtls[eCASHIER_NUM].elementValue);
	}*/
//
//	if(pstDHIDtls[eSERIAL_NUM].elementValue != NULL)
//	{
//		strcpy(pstElavonReqResDetails[E_DEVICE_SERIAL_NUM].elementValue, pstDHIDtls[eSERIAL_NUM].elementValue);
//	}

	/*Field 0011 : FuseBox Data*/
	if(pstDHIDtls[eFUSEBOX_DATA].elementValue != NULL)
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		strncpy(szTemp, pstDHIDtls[eFUSEBOX_DATA].elementValue, 3);
		iTimeout = atoi(szTemp);

		if(iTimeout > 0)
		{
			updateTimeOutValue(iTimeout);
		}
		strcpy(pstElavonReqResDetails[E_FUSEBOX_DATA].elementValue, pstDHIDtls[eFUSEBOX_DATA].elementValue);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPinDtls
 *
 * Description	: Fills up the required Elavon details structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getPinDtls(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int			rv 		= 		SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	if(pstDHIDtls[ePIN_BLOCK].elementValue == NULL || pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: PIN Details Are Not Present", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return rv;
	}

	if(pstDHIDtls[ePIN_BLOCK].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_PIN_BLOCK].elementValue, pstDHIDtls[ePIN_BLOCK].elementValue);
	}

	if(pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_KEY_SERIAL_NUM].elementValue,pstDHIDtls[eKEY_SERIAL_NUMBER].elementValue);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: fillEmvDetails
 *
 * Description	: Fills up the required Elavon details structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int fillEmvDetails(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int			rv 		= 		SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";				//size need to finalize
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

//	if(stPymtncmd.iCommand == E_COMPLETION || stPymtncmd.iCommand == E_VOID || stPymtncmd.iCommand == E_FULL_REVERSAL)
//	{
//		rv = getEMVTagsFromFile(pstDHIDtls, pstElavonReqResDetails);
//		if(rv != SUCCESS)
//		{
//			debug_sprintf(szDbgMsg, "%s: Failed To load EMV tags From File, rv= [%d]", __FUNCTION__, rv);
//			EHI_LIB_TRACE(szDbgMsg);
//			rv = SUCCESS;
//		}
//	}

	/*In Case of Completion and Void we can expect this values from POS also, if POS is sending we will honor them, if POS is not sending we won't sending these fields in the Elavon Request*/
	if(stPymtncmd.iCommand == E_COMPLETION || stPymtncmd.iCommand == E_VOID)
	{
		if(pstDHIDtls[eELAVON_CHIP_CONDITION_CODE].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_CHIP_CONDITION_CODE].elementValue, pstDHIDtls[eELAVON_CHIP_CONDITION_CODE].elementValue);
		}

		if(pstDHIDtls[eELAVON_PIN_CAPABILITIES].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_PIN_CAPABILITIES].elementValue, pstDHIDtls[eELAVON_PIN_CAPABILITIES].elementValue);
		}
	}


	if(pstDHIDtls[eEMV_TAGS].elementValue != NULL)
	{
		rv = parseEmvTags(pstDHIDtls[eEMV_TAGS].elementValue, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of EMV data Failed", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			return rv;
		}

		getChipConditionCode(pstDHIDtls, pstElavonReqResDetails);

		getCVMResults(pstElavonReqResDetails);

		//getTermCurrencyCode(pstElavonReqResDetails[E_TERM_CURRENCY_TRI_GRAPH].elementValue);   not required let pos drive this
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No EMV Tags Present for this transaction", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getAVSFieldDetails
 *
 * Description	: Fills up the required Elavon details structure with AVS Field Details
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getAVSFieldDetails(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int 		rv = SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	//if(pstDHIDtls[eCUSTOMER_ZIP].elementValue != NULL)
	//{
		//strcpy(pstElavonReqResDetails[E_BILLING_ZIP_CODE].elementValue, pstDHIDtls[eCUSTOMER_ZIP].elementValue);
	//}

	if(pstDHIDtls[eCUSTOMER_STREET].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_BILLING_STREET_ADDRESS].elementValue, pstDHIDtls[eCUSTOMER_STREET].elementValue);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTransTaxDtls
 *
 * Description	: Fills up the required Elavon details structure
 *
 * Input Params	: structure containing the dhi details
 * 				  structure to be filled with the required Elavon details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getLevel2Dtls(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int      rv 	   = SUCCESS;
//	EHI_BOOL iDatareqd = EHI_FALSE;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	//	if(pstDHIDtls[eCUSTOMER_CODE].elementValue != NULL || pstDHIDtls[eTAX_IND].elementValue != NULL || pstDHIDtls[eTAX_AMOUNT].elementValue != NULL)
	//	{
	//		iDatareqd = EHI_TRUE;
	//	}
	//
	//	if(iDatareqd == EHI_TRUE)
	//	{
	//		if(pstDHIDtls[eCUSTOMER_CODE].elementValue != NULL)
	//		{
	//			strcpy(pstElavonReqResDetails[E_CUSTOMER_CODE].elementValue, pstDHIDtls[eCUSTOMER_CODE].elementValue);
	//		}
	//		else
	//		{
	//			strcpy(pstElavonReqResDetails[E_CUSTOMER_CODE].elementValue, pstElavonReqResDetails[E_REF_NUMBER].elementValue);
	//		}
	//
	//		if(pstDHIDtls[eTAX_IND].elementValue != NULL)
	//		{
	//			strcpy(pstElavonReqResDetails[E_TAX1_IND].elementValue, pstDHIDtls[eTAX_IND].elementValue);
	//		}
	//
	//		if(pstDHIDtls[eTAX_AMOUNT].elementValue == NULL && pstDHIDtls[eTAX_IND].elementValue != NULL)
	//		{
	//			if(atoi(pstDHIDtls[eTAX_IND].elementValue) == 1)
	//			{
	//				debug_sprintf(szDbgMsg, "%s: Tax Amount is required", __FUNCTION__);
	//				EHI_LIB_TRACE(szDbgMsg);
	//				rv = ERR_FLD_REQD;
	//				return rv;
	//			}
	//			else if(atoi(pstDHIDtls[eTAX_IND].elementValue) == 2 || atoi(pstDHIDtls[eTAX_IND].elementValue) == 0)
	//			{
	//				// for tax Non-taxable transaction
	//				strcpy(pstElavonReqResDetails[E_TAX_AMOUNT_1].elementValue, "0.00");
	//			}
	//		}
	//		if(pstDHIDtls[eTAX_AMOUNT].elementValue != NULL)
	//		{
	//			strcpy(pstElavonReqResDetails[E_TAX_AMOUNT_1].elementValue, pstDHIDtls[eTAX_AMOUNT].elementValue);
	//		}
	//	}

	if(pstDHIDtls[eCUSTOMER_CODE].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_CUSTOMER_CODE].elementValue, pstDHIDtls[eCUSTOMER_CODE].elementValue);
	}
//	else
//	{
//		strcpy(pstElavonReqResDetails[E_CUSTOMER_CODE].elementValue, pstElavonReqResDetails[E_REF_NUMBER].elementValue);
//	}

	if(pstDHIDtls[eTAX_IND].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_TAX1_IND].elementValue, pstDHIDtls[eTAX_IND].elementValue);
	}

	if(pstDHIDtls[eTAX_AMOUNT].elementValue != NULL)
	{
		strcpy(pstElavonReqResDetails[E_TAX_AMOUNT_1].elementValue, pstDHIDtls[eTAX_AMOUNT].elementValue);
	}


	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameElavonRequest
 *
 * Description	:
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	frameElavonRequest(TRANDTLS_PTYPE pstDHIDetails, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails, char ** pszElavonRequest)
{
	int 			rv	 				= SUCCESS;
	int				iSize				= 0;
	int				iCmd				= -1;
	int				iPymtType			= -1;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	xmlNodePtr		elemNodePtr			= NULL;
	xmlNodePtr		transNodePtr		= NULL;
	xmlAttrPtr 		newAttr				= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[9400]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{

		/* Create the xml document */
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldn't create doc", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Create the root node for the xml document */
		rootPtr = xmlNewNode(NULL, BAD_CAST "ProtoBase_Transaction_Batch");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		newAttr = xmlNewProp (rootPtr, BAD_CAST "xmlns:xsi", BAD_CAST "http://www.w3.org/2001/XMLSchema-instance");

		newAttr = xmlNewProp (rootPtr, BAD_CAST "xsi:noNamespaceSchemaLocation", BAD_CAST "http://www.protobase.com/XML/PBAPI1.xsd");

		/* Set the root element for the doc */
		xmlDocSetRootElement(docPtr, rootPtr);


		elemNodePtr = xmlNewDocText(docPtr, BAD_CAST "!--Copyright Elavon., 2009 --");
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to add comment Text", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}


		iCmd = stPymtncmd.iCommand;
		iPymtType = stPymtncmd.iPaymentType;

		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "Settlement_Batch", BAD_CAST "false");
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to add New Settlement Batch Child", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		transNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "Transaction", NULL);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to add Transaction Node Child", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//		if(iCmd == E_SETTLE_SUMMARY || iCmd == E_CUTOVER)
		//		{
		//			rv = frameElavonRequestForReportCmd(transNodePtr, pstDHIDetails, pstElavonReqResDetails);
		//			if(rv != SUCCESS)
		//			{
		//				debug_sprintf(szDbgMsg, "%s: Failed to Fill the Request for Report Command", __FUNCTION__);
		//				EHI_LIB_TRACE(szDbgMsg);
		//
		//				break;
		//			}
		//		}
		//		else
		//		{
		rv = frameCommonDtlsAPIList(transNodePtr, pstDHIDetails, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Fill the Common API Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			break;
		}

		rv = frameElavonPTAPIList(transNodePtr, pstDHIDetails, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Frame Elavon PT Req Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			break;
		}
		//		}

		if(pstDHIDetails[ePASSTHROUGH_FIELDS].elementValue != NULL && (strlen(pstDHIDetails[ePASSTHROUGH_FIELDS].elementValue) > 0))
		{
			rv = frameElavonConcatanatedPTList(transNodePtr, pstDHIDetails, pstElavonReqResDetails);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Frame Elavon Concatanated PT Req Details", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);

				break;
			}
		}


		/* If the XML message tree has formed correctly, dump this message into
		 * the string provided as parameter */
		//xmlDocDumpFormatMemory(docPtr, (xmlChar **) pszElavonRequest, &iSize, 0);
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) pszElavonRequest, &iSize, "UTF-8", 0);

#ifdef DEVDEBUG

		debug_sprintf(szDbgMsg, "%s: Total Bytes = [%d]", __FUNCTION__, iSize);
		EHI_LIB_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Elavon Request Message = [%s]", __FUNCTION__, *pszElavonRequest);
		EHI_LIB_TRACE(szDbgMsg);
#endif

		break;
	}

	/* Free the memory allocated for the xml document tree */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameElavonRequestForChkHostConn
 *
 * Description	:
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int	frameElavonRequestForChkHostConn(TRANDTLS_PTYPE pstDHIDetails, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails, char ** pszElavonRequest)
{
	int 			rv	 				= SUCCESS;
	int				iSize				= 0;
	int				iCmd				= -1;
	int				iPymtType			= -1;
	xmlDocPtr		docPtr				= NULL;
	xmlNodePtr		rootPtr				= NULL;
	xmlNodePtr		elemNodePtr			= NULL;
	xmlNodePtr		transNodePtr		= NULL;
	xmlAttrPtr 		newAttr				= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[4700]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	while(1)
	{

		/* Create the xml document */
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED; couldn't create doc", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Create the root node for the xml document */
		rootPtr = xmlNewNode(NULL, BAD_CAST "ProtoBase_Transaction_Batch");
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		newAttr = xmlNewProp (rootPtr, BAD_CAST "xmlns:xsi", BAD_CAST "http://www.w3.org/2001/XMLSchema-instance");

		newAttr = xmlNewProp (rootPtr, BAD_CAST "xsi:noNamespaceSchemaLocation", BAD_CAST "http://www.protobase.com/XML/PBAPI1.xsd");

		/* Set the root element for the doc */
		xmlDocSetRootElement(docPtr, rootPtr);


		elemNodePtr = xmlNewDocText(docPtr, BAD_CAST "!--Copyright Elavon., 2009 --");
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to add comment Text", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}


		iCmd = stPymtncmd.iCommand;
		iPymtType = stPymtncmd.iPaymentType;

		elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "Settlement_Batch", BAD_CAST "false");
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to add New Settlement Batch Child", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		transNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "Transaction", NULL);
		if(elemNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to add Transaction Node Child", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = frameCommonDtlsAPIList(transNodePtr, pstDHIDetails, pstElavonReqResDetails);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Fill the Common API Details", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);

			break;
		}


		/* If the XML message tree has formed correctly, dump this message into
		 * the string provided as parameter */
		//xmlDocDumpFormatMemory(docPtr, (xmlChar **) pszElavonRequest, &iSize, 0);
		xmlDocDumpFormatMemoryEnc(docPtr, (xmlChar **) pszElavonRequest, &iSize, "UTF-8", 0);

#ifdef DEVDEBUG

		debug_sprintf(szDbgMsg, "%s: Total Bytes = [%d]", __FUNCTION__, iSize);
		EHI_LIB_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Elavon Request Message = [%s]", __FUNCTION__, *pszElavonRequest);
		EHI_LIB_TRACE(szDbgMsg);
#endif

		break;
	}

	/* Free the memory allocated for the xml document tree */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: returning %d", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameElavonRequestForReportCmd
 *
 * Description	: FUnction Frames the Elavon Request for Report COmmand such as CUTOVER and SETTLE SUMMARY.
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
//static int	frameElavonRequestForReportCmd(xmlNodePtr	transNodePtr, TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
//{
//	int				rv						=	SUCCESS;
//	int				iCnt					=	0;
//	int				iTotCnt					=	0;
//	int				iIndex					=	0;
//	int				iAppLogEnabled			=	0;
//	char			szAppLogData[256]		= 	"";
//	char			szAppLogDiag[256]		= 	"";
//	char			*pszValBuffer			=	NULL;
//	xmlNodePtr		APINodePtr				= NULL;
//	xmlNodePtr		childNode				= NULL;
//
//#ifdef DEBUG
//	char	szDbgMsg[256]		= "";
//#endif
//
//	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
//	EHI_LIB_TRACE(szDbgMsg);
//
//	iAppLogEnabled = isAppLogEnabled();
//	memset(szAppLogData, 0x00, sizeof(szAppLogData));
//	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
//
//	iTotCnt = sizeof(stReportCmdAPILst) / ELAVON_KEYVAL_SIZE;
//
//	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
//	{
//
//		iIndex = stReportCmdAPILst[iCnt].iIndex;
//		if(stReportCmdAPILst[iCnt].iSource == DHI)
//		{
//			pszValBuffer = pstDHIDtls[iIndex].elementValue;
//		}
//		else if(stReportCmdAPILst[iCnt].iSource == ELAVON)
//		{
//			pszValBuffer = pstElavonReqResDetails[iIndex].elementValue;
//		}
//
//		if( (stReportCmdAPILst[iCnt].isMand == EHI_TRUE) && (pszValBuffer == NULL || (strlen(pszValBuffer) == 0)) )
//		{
//			debug_sprintf(szDbgMsg, "%s: Mandatory Field [%s] Not present", __FUNCTION__, stReportCmdAPILst[iCnt].key);
//			EHI_LIB_TRACE(szDbgMsg);
//
//			if(iAppLogEnabled)
//			{
//				sprintf(szAppLogData, "Mandatory API Field [%s] Not Present To Fill in Elavon Request", stReportCmdAPILst[iCnt].key);
//				sprintf(szAppLogDiag, "Please Provide Value For API Field [%s] in Elavon Request", stReportCmdAPILst[iCnt].key);
//				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
//			}
//
//			rv = FAILURE;
//			break;
//		}
//
//		if(pszValBuffer != NULL && strlen(pszValBuffer) > 0)	// Add new API_Field Xml Child only when the Value for the API Field Exists
//		{
//			APINodePtr = xmlNewNode(NULL, BAD_CAST "API_Field");
//			xmlAddChild(transNodePtr, APINodePtr);
//
//			childNode = xmlNewChild(APINodePtr, NULL, BAD_CAST "Field_Number", BAD_CAST stReportCmdAPILst[iCnt].key);
//			childNode = xmlNewChild(APINodePtr, NULL, BAD_CAST "Field_Value", BAD_CAST pszValBuffer);
//		}
//
//		pszValBuffer = NULL;
//		APINodePtr = NULL;
//		childNode = NULL;
//	}
//
//	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__,rv);
//	EHI_LIB_TRACE(szDbgMsg);
//
//	return rv;
//}

/*
 * ============================================================================
 * Function Name: frameCommonDtlsAPIList
 *
 * Description	:
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int	frameCommonDtlsAPIList(xmlNodePtr	transNodePtr, TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int				rv						=	SUCCESS;
	int				iCnt					=	0;
	int				iTotCnt					=	0;
	int				iIndex					=	0;
	int				iAppLogEnabled			=	0;
	char			szAppLogData[256]		= 	"";
	char			szAppLogDiag[256]		= 	"";
	char			*pszValBuffer			=	NULL;
	xmlNodePtr		APINodePtr				= NULL;
	xmlNodePtr		childNode				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iTotCnt = sizeof(stCommonAPILst) / ELAVON_KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{

		iIndex = stCommonAPILst[iCnt].iIndex;
		if(stCommonAPILst[iCnt].iSource == DHI)
		{
			pszValBuffer = pstDHIDtls[iIndex].elementValue;
		}
		else if(stCommonAPILst[iCnt].iSource == ELAVON)
		{
			pszValBuffer = pstElavonReqResDetails[iIndex].elementValue;
		}

		if( (stCommonAPILst[iCnt].isMand == EHI_TRUE) && (pszValBuffer == NULL || (strlen(pszValBuffer) == 0)) )
		{
			debug_sprintf(szDbgMsg, "%s: Mandatory Field [%s] Not present", __FUNCTION__, stCommonAPILst[iCnt].key);
			EHI_LIB_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Mandatory API Field [%s] Not Present To Fill in Elavon Request", stCommonAPILst[iCnt].key);
				sprintf(szAppLogDiag, "Please Provide Value For API Field [%s] in Elavon Request", stCommonAPILst[iCnt].key);
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		if(pszValBuffer != NULL && strlen(pszValBuffer) > 0)	// Add new API_Field Xml Child only when the Value for the API Field Exists
		{
			APINodePtr = xmlNewNode(NULL, BAD_CAST "API_Field");
			xmlAddChild(transNodePtr, APINodePtr);

			childNode = xmlNewChild(APINodePtr, NULL, BAD_CAST "Field_Number", BAD_CAST stCommonAPILst[iCnt].key);
			childNode = xmlNewChild(APINodePtr, NULL, BAD_CAST "Field_Value", BAD_CAST pszValBuffer);
		}

		pszValBuffer = NULL;
		APINodePtr = NULL;
		childNode = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameElavonPTAPIList
 *
 * Description	:
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int	frameElavonPTAPIList(xmlNodePtr	transNodePtr, TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int				rv						=	SUCCESS;
	int				iCnt					=	0;
	int				iTotCnt					=	0;
	int				iIndex					=	0;
	int				iAppLogEnabled			=	0;
	char			szAppLogData[256]		= 	"";
	char			szAppLogDiag[256]		= 	"";
	char			*pszValBuffer			=	NULL;
	xmlNodePtr		APINodePtr				= 	NULL;
	xmlNodePtr		childNode				= 	NULL;
	ELAVON_PASSTHROUGH_PTYPE	pstEHIPTDtls	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	pstEHIPTDtls = getElavonPassThroughDtls();
	if(pstEHIPTDtls != NULL)
	{
		iTotCnt = pstEHIPTDtls->iReqAPICnt;

		debug_sprintf(szDbgMsg, "%s: EHI Pass Through Request Count [%d]", __FUNCTION__, iTotCnt);
		EHI_LIB_TRACE(szDbgMsg);


		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			iIndex = pstEHIPTDtls->pstPTAPIReqDtls[iCnt].iIndex;

			if(pstEHIPTDtls->pstPTAPIReqDtls[iCnt].iSource == DHI)
			{
				pszValBuffer = pstDHIDtls[iIndex].elementValue;
			}
			else if(pstEHIPTDtls->pstPTAPIReqDtls[iCnt].iSource == ELAVON)
			{
				pszValBuffer = pstElavonReqResDetails[iIndex].elementValue;
			}

			if( (pstEHIPTDtls->pstPTAPIReqDtls[iCnt].isMand == EHI_TRUE) && (pszValBuffer == NULL || (strlen(pszValBuffer) == 0)) )
			{
				debug_sprintf(szDbgMsg, "%s: Mandatory Field [%s] Not present", __FUNCTION__, pstEHIPTDtls->pstPTAPIReqDtls[iCnt].key);
				EHI_LIB_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Mandatory API Field [%s] Not Present To Fill in Elavon Request", pstEHIPTDtls->pstPTAPIReqDtls[iCnt].key);
					sprintf(szAppLogDiag, "Please Provide Value For API Field [%s] in Elavon Request", pstEHIPTDtls->pstPTAPIReqDtls[iCnt].key);
					addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_FLD_REQD;
				break;
			}

			if(pszValBuffer != NULL && strlen(pszValBuffer) > 0)	// Add new API_Field Xml Child only when the Value for the API Field Exists
			{
				APINodePtr = xmlNewNode(NULL, BAD_CAST "API_Field");
				xmlAddChild(transNodePtr, APINodePtr);

				childNode = xmlNewChild(APINodePtr, NULL, BAD_CAST "Field_Number", BAD_CAST pstEHIPTDtls->pstPTAPIReqDtls[iCnt].key);
				childNode = xmlNewChild(APINodePtr, NULL, BAD_CAST "Field_Value", BAD_CAST pszValBuffer);
			}

			pszValBuffer = NULL;
			APINodePtr = NULL;
			childNode = NULL;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__,rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: frameElavonConcatanatedPTList
 *
 * Description	:
 *
 *
 * Input Params	: Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int	frameElavonConcatanatedPTList(xmlNodePtr	transNodePtr, TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int				rv								=	SUCCESS;
	int				iAppLogEnabled					=	0;
//	int				iLenParsed						=   0;
	int 			iTemp							= 	0;
	int 			iEncodedFieldLen				=	0;
	int 			iLenAfterDecoding				=   0;
	char 			szAPIFieldNumnValue[MAX_SIZE+6]	= 	"";
	char			szAppLogData[256]				= 	"";
	char			szAppLogDiag[256]				= 	"";
	char			szAPI[4+1]						=	"";
	char 			szFieldValue[MAX_SIZE]			=	"";
	char			*pszTemp						=	NULL;
	char			*pszAPIFieldnValue				=	NULL;
	char			*pszCurrPtr						=	NULL;
	char 			*pszDecodedAPIFieldsData		= 	NULL;
	xmlNodePtr		APINodePtr						= 	NULL;
	xmlNodePtr		childNode						= 	NULL;

#ifdef DEBUG
	char			szDbgMsg[2048]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	if(pstDHIDtls[ePASSTHROUGH_FIELDS].elementValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Passthrough Fields to Add in FuseBox Request", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		rv = SUCCESS;
		return rv;
	}


	iEncodedFieldLen = strlen(pstDHIDtls[ePASSTHROUGH_FIELDS].elementValue);

	pszDecodedAPIFieldsData = (char*)malloc((iEncodedFieldLen + 1) * sizeof(char));
	memset(pszDecodedAPIFieldsData, 0x00, (iEncodedFieldLen + 1) * sizeof(char));

	dcdBase64((uchar*)pstDHIDtls[ePASSTHROUGH_FIELDS].elementValue, iEncodedFieldLen, (uchar*)pszDecodedAPIFieldsData, &iLenAfterDecoding);

	if(iLenAfterDecoding > iEncodedFieldLen)
	{
		debug_sprintf(szDbgMsg, "%s: Enough Memory is not allocated", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		if(pszDecodedAPIFieldsData != NULL)
		{
			free(pszDecodedAPIFieldsData);
		}

		rv = FAILURE;
		return rv;

	}

	if(strlen(pszDecodedAPIFieldsData) < 2048)
	{
		debug_sprintf(szDbgMsg, "%s: Passthrough Fields [%s]", __FUNCTION__, pszDecodedAPIFieldsData);
		EHI_LIB_TRACE(szDbgMsg);
	}


	//pszDecodedAPIFieldsData[strlen(pszDecodedAPIFieldsData)] = 0x1E;

	pszCurrPtr = pszDecodedAPIFieldsData;

	while(*pszCurrPtr != '\0')
	{
		memset(szAPI, 0x00, sizeof(szAPI));
		memset(szFieldValue, 0x00, sizeof(szFieldValue));
		memset(szAPIFieldNumnValue, 0x00, sizeof(szAPIFieldNumnValue));


//		if(iLenAfterDecoding == iLenParsed)
//		{
//			debug_sprintf(szDbgMsg, "%s:Pass through field is parsed Fully ", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//
//			break;
//		}
//		else
//		{
			if((pszAPIFieldnValue = strchr(pszCurrPtr, 0x1E)) != NULL)
			{
				iTemp = (pszAPIFieldnValue - pszCurrPtr);
				strncpy(szAPIFieldNumnValue, pszCurrPtr, iTemp);

				iTemp = iTemp + 1;		// to skip <RS>
			}
			else
			{
				iTemp = strlen(pszCurrPtr);
				strncpy(szAPIFieldNumnValue, pszCurrPtr, iTemp);
			}

		//	iLenParsed = iLenParsed + iTemp;
			pszCurrPtr = pszCurrPtr + iTemp;


			debug_sprintf(szDbgMsg, "%s:Api field number and value : [%s] ", __FUNCTION__, szAPIFieldNumnValue);
			EHI_LIB_TRACE(szDbgMsg);

			if((pszTemp = strchr(szAPIFieldNumnValue, 0x1C)) != NULL)
			{
				strncpy(szAPI, szAPIFieldNumnValue, pszTemp - szAPIFieldNumnValue);
				pszTemp = pszTemp + 1;
				if(pszTemp != NULL)
				{
					strcpy(szFieldValue, pszTemp);
				}
			}

			if(strlen(szFieldValue) > 0)
			{
//				debug_sprintf(szDbgMsg, "%s: API [%s],  API value [%s]", __FUNCTION__, szAPI, szFieldValue);
//				EHI_LIB_TRACE(szDbgMsg);

				APINodePtr = xmlNewNode(NULL, BAD_CAST "API_Field");
				xmlAddChild(transNodePtr, APINodePtr);

				childNode = xmlNewChild(APINodePtr, NULL, BAD_CAST "Field_Number", BAD_CAST szAPI);
				childNode = xmlNewChild(APINodePtr, NULL, BAD_CAST "Field_Value", BAD_CAST szFieldValue);
			}

			APINodePtr = NULL;
			childNode = NULL;
	//	}

	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPresentFlag
 *
 *
 * Description	: This function will fill the Present Flag in the DHI request
 *
 *
 * Input Params	: Terminal Entry Capability Buffer to be filled
 *
 *
 * Output Params: value of Present flag.
 * ============================================================================
 */
static int getPresentFlag(char *szPresentFlag)
{
	int iPresentFlag = 0;

	if(szPresentFlag == NULL)
	{
		return ERR_FLD_REQD;
	}

	iPresentFlag = atoi(szPresentFlag);
	switch(iPresentFlag)
	{
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
	case 7:
		return iPresentFlag;
	default:
		return ERR_INV_FLD;
	}
}

/*
 * ============================================================================
 * Function Name  : saveTxnDetailsForFollowOnTxn
 *
 * Description	 :  This function will fills the Transaction Details required for follow on in a file
 *
 * Input Params	 :  structure containing the DHI details
 * 					structure containing the EHI details
 *
 * Output Params   : None
 * ============================================================================
 */
void saveTxnDetailsForFollowOnTxn(TRANDTLS_PTYPE pstDHIDetails, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	char	szTranRecord[2048] 	= "";
	char	szBuffer[1024]		= "";
	FILE* 	fptr 				= NULL;


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --------enter--------", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	fptr = fopen(ELAVON_TRAN_RECORDS, "a+");
	if(fptr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Could not open the file could not save the record", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return;
	}

	/*
	 * Order in which the data is going to get stored in the file:
	 * Ref_Num|EMV_TAGS|
	 */


	if(strlen(pstElavonReqResDetails[E_REF_NUMBER].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_REF_NUMBER].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}

	/*storing EMV_TAGS Sent in The Request to Host*/
	if((pstDHIDetails[eEMV_TAGS].elementValue != NULL) && (strlen(pstDHIDetails[eEMV_TAGS].elementValue) > 0))
	{
		sprintf(szBuffer, "%s|", pstDHIDetails[eEMV_TAGS].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}


	/*storing card token */
	if((strlen(pstElavonReqResDetails[E_CARD_TOKEN].elementValue) > 3) && (strncmp(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, "ID:", 3) == 0)
			&& (strcmp(pstElavonReqResDetails[E_CARD_TOKEN].elementValue, "ID:NOT_ASSIGNED") != 0))
	{
		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_CARD_TOKEN].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else if(strlen(pstElavonReqResDetails[E_ACCT_NUM].elementValue) > 0)
	{
		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_ACCT_NUM].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else if(strlen(pstElavonReqResDetails[E_TRACK_DATA].elementValue) > 0)
	{
		convertTrackData2Manual(pstDHIDetails, pstElavonReqResDetails);
		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_ACCT_NUM].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}


	/*storing transaction amount (APPROVED AMOUNT) */
	if(pstDHIDetails[eAPPROVED_AMOUNT].elementValue != NULL)
	{
		sprintf(szBuffer, "%s|", pstDHIDetails[eAPPROVED_AMOUNT].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else if(pstDHIDetails[eTRANS_AMOUNT].elementValue != NULL)
	{
		sprintf(szBuffer, "%s|", pstDHIDetails[eTRANS_AMOUNT].elementValue);
		strcat(szTranRecord, szBuffer);
	}
	else
	{
		strcat(szTranRecord, "|");
	}


	strcat(szTranRecord, "\n");

	fprintf(fptr, "%s", szTranRecord);

	fclose(fptr);



	/*Storing all the fields which are required for the follow on transactions*/

	/*storing Reference Number*/
//	if(stPymtncmd.iCommand == E_VOID || stPymtncmd.iCommand == E_COMPLETION || stPymtncmd.iCommand == E_FULL_REVERSAL)
//	{
//		if(strlen(pstElavonReqResDetails[E_FOLLOWON_REF_NUM].elementValue) > 0)
//		{
//			sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_FOLLOWON_REF_NUM].elementValue);
//			strcat(szTranRecord, szBuffer);
//		}
//		else
//		{
//			strcat(szTranRecord, "|");
//		}
//
//		if(strlen(pstElavonReqResDetails[E_REF_NUMBER].elementValue) > 0)
//		{
//			sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_REF_NUMBER].elementValue);
//			strcat(szTranRecord, szBuffer);
//		}
//		else
//		{
//			strcat(szTranRecord, "|");
//		}
//	}
//	else
//	{
//		if(strlen(pstElavonReqResDetails[E_REF_NUMBER].elementValue) > 0)
//		{
//			sprintf(szBuffer, "%s||", pstElavonReqResDetails[E_REF_NUMBER].elementValue);
//			strcat(szTranRecord, szBuffer);
//		}
//		else
//		{
//			strcat(szTranRecord, "||");
//		}
//	}

//	/*storing transaction type */
//	if(strlen(pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue) > 0)
//	{
//		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_GATEWAY_TRANS_TYPE].elementValue);
//		strcat(szTranRecord, szBuffer);
//	}
//	else
//	{
//		strcat(szTranRecord, "|");
//	}
//
//	/*storing card token */
//	if(strlen(pstElavonReqResDetails[E_CARD_TOKEN].elementValue) > 0)
//	{
//		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_CARD_TOKEN].elementValue);
//		strcat(szTranRecord, szBuffer);
//	}
//	else if(strlen(pstElavonReqResDetails[E_ACCT_NUM].elementValue) > 0)
//	{
//		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_ACCT_NUM].elementValue);
//		strcat(szTranRecord, szBuffer);
//	}
//	else if(strlen(pstElavonReqResDetails[E_TRACK_DATA].elementValue) > 0)
//	{
//		convertTrackData2Manual(pstDHIDetails, pstElavonReqResDetails);
//		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_ACCT_NUM].elementValue);
//		strcat(szTranRecord, szBuffer);
//	}
//	else
//	{
//		strcat(szTranRecord, "|");
//	}
//
//	/*storing Expiry Details*/
//	if(strlen(pstElavonReqResDetails[E_CARD_EXP_DATE].elementValue) > 0)
//	{
//		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_CARD_EXP_DATE].elementValue);
//		strcat(szTranRecord, szBuffer);
//	}
//	else
//	{
//		strcat(szTranRecord, "|");
//	}
//
//	/*storing transaction amount (APPROVED AMOUNT) */
//	if(pstDHIDetails[eAPPROVED_AMOUNT].elementValue != NULL)
//	{
//		sprintf(szBuffer, "%s|", pstDHIDetails[eAPPROVED_AMOUNT].elementValue);
//		strcat(szTranRecord, szBuffer);
//	}
//	else
//	{
//		strcat(szTranRecord, "|");
//	}
//
//	/*storing auth code given by the host*/
//	if(strlen(pstElavonReqResDetails[E_AUTH_CODE].elementValue) > 0)
//	{
//		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_AUTH_CODE].elementValue);
//		strcat(szTranRecord, szBuffer);
//	}
//	else
//	{
//		strcat(szTranRecord, "|");
//	}
//
//	/*storing stored Value Function */
///*	if(strlen(pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue) > 0)
//	{
//		sprintf(szBuffer, "%s|", pstElavonReqResDetails[E_STORED_VALUE_FUNCTION].elementValue);
//		strcat(szTranRecord, szBuffer);
//	}
//	else
//	{
//		strcat(szTranRecord, "|");
//	}*/

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: convertTrackData2Manual
 *
 * Description	: it will copy the account number and exp date from the track data to respective feilds.
 *
 * Input Params	: structre containing dhi details,
 * 				  EHI structre to be filled
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int convertTrackData2Manual(TRANDTLS_PTYPE pstDHIDetails, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
		int				rv 									= SUCCESS;
		int 			iLen								= 0;
		int             iTrackIndicator						= 0;
		char  			szBuffer[25+1]						= "";
		char*			pszTemp								= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	iTrackIndicator = atoi(pstDHIDetails[eTRACK_INDICATOR].elementValue);
	getAccnExpFromTrackData(pstDHIDetails[eTRACK_DATA].elementValue, iTrackIndicator, szBuffer);
	pszTemp = strchr(szBuffer, '|');
	strncpy(pstElavonReqResDetails[E_ACCT_NUM].elementValue, szBuffer, pszTemp-szBuffer);
	pszTemp = pszTemp + 1;
	iLen = pszTemp-szBuffer;
	pszTemp = strchr(pszTemp, '|');
	strncpy(pstElavonReqResDetails[E_CARD_EXP_DATE].elementValue, szBuffer + iLen, 4);			// 4 because MMYY


	/*As the track data is going as the manual, changing POS ENTRY MODE and Proximity indicator as manual Entry*/
	strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "01");
	strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");

	debug_sprintf(szDbgMsg, "%s:returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;

}


/*
 * ============================================================================
 * Function Name: getAccnExpFromTrackData
 *
 * Description	: it will copy the account number and exp date from the track data to buffer
 *
 * Input Params	: buffer containing track data,
 * 				  track indicator,
 *				  buffer to be filled with the account num and exp date
 * Output Params:
 * ============================================================================
 */
void getAccnExpFromTrackData(char *pszTrackData, int iTrackIndicator, char *pszAccnExp)
{
	char *	cCurPtr				    = NULL;
	char *	cNxtPtr				    = NULL;
	char    szAccn[19+1]	        = "";
	char    szExp[4+1]	            = "";
	char    szTempExp[4+1]	        = "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	if(pszTrackData == NULL || pszAccnExp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param is NULL!!!", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return;
	}

	cCurPtr = pszTrackData;

	if(iTrackIndicator == 1) //Track1 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving Account Number and exp from Track1", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		if(!(cCurPtr[0] >= '0' && cCurPtr[0] <= '9')) //If it is not digit then skip that character
		{
			cCurPtr = cCurPtr + 1;
		}

		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '^' sentinel in their track 1 information */
			strcpy(szAccn, cCurPtr);
		}
		else
		{
			memcpy(szAccn, cCurPtr, cNxtPtr - cCurPtr);
		}

		/*skipping card holder name*/
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid track; no Name", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}

		/*copying the exp date*/
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, '\0');
		if((cNxtPtr - cCurPtr) < 4)
		{
			debug_sprintf(szDbgMsg,"%s: Invalid track; no exp dt",__FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			memcpy(szTempExp, cCurPtr, 4);
		}
	}
	else if(iTrackIndicator == 2) //Track 2 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving Account Number from Track2", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);

		cNxtPtr = strchr(cCurPtr, '=');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '=' sentinel in their track 2 */
			strcpy(szAccn, cCurPtr);
		}
		else
		{
			memcpy(szAccn, cCurPtr, cNxtPtr - cCurPtr);
		}

		/* copying exp Date*/
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cNxtPtr, '\0');
		if(cNxtPtr - cCurPtr < 4)
		{
			debug_sprintf(szDbgMsg, "%s: Expiry Date is Missing!!!", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
		}
		else
		{
			memcpy(szTempExp, cCurPtr, 4);
		}

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid track to retrieve Account Number And Exp Date!!!", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return;
	}

	/*below operations are to store in MMYY format*/
	strncpy(szExp, szTempExp+2, 2);
	strncat(szExp, szTempExp, 2);

	sprintf(pszAccnExp, "%s|%s|", szAccn, szExp);

	debug_sprintf(szDbgMsg, "%s:returning ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getEMVTagsFromFile
 *
 * Description	: This Function will get EMV tags from File and Fills in to DHI structure.
 *
 * Input Params	: DHI STRUCTURE,
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
//int getEMVTagsFromFile(TRANDTLS_PTYPE pstDHIDetails, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
//{
//	int			rv									    = SUCCESS;
//	int			iFound				 					= EHI_FALSE;
//	int			iCount								    = 0;
//	int			iIndex							        = 0;
//	int			iTotal								    = 0;
//	char*		pszBuffer						    	= NULL;
//	FILE*		fptr				 					= NULL;
//	int			iEHIIndices[]						    = {E_CARD_TOKEN, E_TRANS_AMOUNT};
//	int			iDHIIndices[]							= {eEMV_TAGS};
//
//#ifdef DEBUG
//	char			szDbgMsg[1024]	= "";
//#endif
//
//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	EHI_LIB_TRACE(szDbgMsg);
//
//
//	while(1)
//	{
//		if(strlen(pstElavonReqResDetails[E_REF_NUMBER].elementValue) == 0)
//		{
//			debug_sprintf(szDbgMsg, "%s: CTROUTD not Present", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//
//			rv = ERR_INV_FLD_VAL;
//			break;
//		}
//
//		fptr = fopen(ELAVON_TRAN_RECORDS, "r");
//		if(fptr == NULL)
//		{
//			debug_sprintf(szDbgMsg, "%s: --- File not available --- ", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//
//			rv = FAILURE;
//
//			break;
//		}
//
//		while((pszBuffer = csvgetline(fptr)) != NULL)
//		{
//			pszBuffer = csvfield(iCount);
//			if(pszBuffer != NULL)
//			{
//				if(strcmp(pszBuffer, pstElavonReqResDetails[E_REF_NUMBER].elementValue) == SUCCESS)
//				{
//					debug_sprintf(szDbgMsg, "%s: Found the CTROUTD in the records", __FUNCTION__);
//					EHI_LIB_TRACE(szDbgMsg);
//
//					iFound = EHI_TRUE;
//					iCount++;
//					break;
//				}
//			}
//		}
//
//		if(iFound == EHI_FALSE)
//		{
//			debug_sprintf(szDbgMsg, "%s: CTROUTD not found in the records", __FUNCTION__);
//			EHI_LIB_TRACE(szDbgMsg);
//
//			rv = ERR_INV_CTROUTD;
//			break;
//		}
//
//		/*Copying DHI Fields from the File into DHI Structure*/
//		iTotal = sizeof(iDHIIndices)/sizeof(int);
//		for(iIndex = 0; iIndex < iTotal; iIndex++)
//		{
//			pszBuffer = csvfield(iCount++);
//			debug_sprintf(szDbgMsg, "%s: DHI Field %d - %s", __FUNCTION__, iIndex, pszBuffer);
//			EHI_LIB_TRACE(szDbgMsg);
//
//			if(pszBuffer && strlen(pszBuffer) > 0 && !pstDHIDetails[iDHIIndices[iIndex]].elementValue)
//			{
//				pstDHIDetails[iDHIIndices[iIndex]].elementValue = (char *)malloc(strlen(pszBuffer)+1);
//				memset(pstDHIDetails[iDHIIndices[iIndex]].elementValue, 0x00, strlen(pszBuffer)+1);
//				strcpy(pstDHIDetails[iDHIIndices[iIndex]].elementValue, pszBuffer);
//			}
//		}
//
//		/*Copying EHI Fields from the File into EHI Structure*/
//		if(stPymtncmd.iCommand == E_FULL_REVERSAL)
//		{
//			iTotal = sizeof(iEHIIndices)/sizeof(int);
//			for(iIndex = 0; iIndex < iTotal; iIndex++)
//			{
//				pszBuffer = csvfield(iCount++);
//				debug_sprintf(szDbgMsg, "%s: Elavon Field %d - %s", __FUNCTION__, iIndex, pszBuffer);
//				EHI_LIB_TRACE(szDbgMsg);
//
//				if(pszBuffer && strlen(pszBuffer) > 0)
//				{
//					strcpy(pstElavonReqResDetails[iEHIIndices[iIndex]].elementValue, pszBuffer);
//				}
//			}
//
//			/*we are filling the card token as the account information so using POS entry mode as manual Entry*/
//			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "01");
//			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");
//		}
//		break;
//	}
//
//	if(fptr != NULL)
//	{
//		fclose(fptr);
//	}
//
//	debug_sprintf(szDbgMsg, "%s: --- returning [%d] ---", __FUNCTION__, rv);
//	EHI_LIB_TRACE(szDbgMsg);
//
//	return rv;
//}

/*
 * ============================================================================
 * Function Name: parseEmvTags
 *
 * Description	: This Function will parse EMV tags from DHI structure and fills the Elavon structure.
 *
 * Input Params	: Buffer containing EMV tags,
 * 				  EHI Structure.
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int parseEmvTags(char* pszEMVtags, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int						rv						= SUCCESS;
//	int						iValLen					= 0;
	int						iEhiIndex				= -1;
	int						iLengthParsed 			= 0;
	int						iTotallength			= 0;
	int						iAscIIBuffSize			= 0;
	char					szTag[7]				= "";
	char					szTempchar[128+1]		= "";
	char					szTempHex[128+1]		= "";
	char					szExtraEmvTags[1024]	= "";
	char					*ascIIBuffer			= NULL;
	char					*pszStartTemp			= NULL;
	char					*pszEndTemp				= NULL;
	char					*pszStartEmvList		= NULL;
	char					*ptrTagValue			= NULL;
	EHI_BOOL				bConvert2Ascii			= EHI_FALSE;
	HASHLST_PTYPE			pstHashLst				= NULL;
	unsigned int			tag;
	unsigned short			Temp;
	unsigned short			len;


#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	ascIIBuffer = (char *) malloc (sizeof(char)* 2048);
	if( ascIIBuffer == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(ascIIBuffer, 0x00, 2048);
	iAscIIBuffSize = Hex2Bin((unsigned char *)pszEMVtags, strlen(pszEMVtags)/2, (unsigned char *)ascIIBuffer);
	if(iAscIIBuffSize <= 0)
	{
		debug_sprintf(szDbgMsg, "%s:No tags are present adding default tags ", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		//return ERR_INV_FLD;
	}

	memset(szExtraEmvTags, 0x00, sizeof(szExtraEmvTags));

	iTotallength = iAscIIBuffSize;
	//storing start for freeing later
	pszStartEmvList = ascIIBuffer;

	while(iLengthParsed < iAscIIBuffSize)
	{
		pszStartTemp = ascIIBuffer;
		tag = 0;
		tag = *ascIIBuffer++;
		iLengthParsed++;

		if( (tag & 0x1F) == 0x1F )// tag value is atleast 2 bytes
		{
			do {
				tag = tag << 8;
				tag = tag | *ascIIBuffer++;
				iLengthParsed++;
			} while((tag & 0x80) == 0x80);
		}

		//Getting Length
		len= *ascIIBuffer++;
		iLengthParsed += 1;
		if (len & 0x80)
		{
			Temp= len & 0x7f;
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *ascIIBuffer++;
				iLengthParsed += 1;
				Temp--; // Temp should decremented for exiting from the closed loop.
			}
		}

		ptrTagValue = NULL;
		ptrTagValue = ascIIBuffer;

		iLengthParsed += len;
		ascIIBuffer += len;
		pszEndTemp = ascIIBuffer;
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);

		/* Do the Hash look up for this Tag Name */
		pstHashLst = lookupEmvHashTable(szTag);
		if(pstHashLst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Did not find entry for this EMV Tag [%s] in the Hash Table!!! So ignoring this tag", __FUNCTION__, szTag);
			EHI_LIB_TRACE(szDbgMsg);

			memset(szTempchar, 0X00, sizeof(szTempchar));
			memset(szTempHex, 0X00, sizeof(szTempHex));

			memcpy(szTempchar, pszStartTemp, pszEndTemp - pszStartTemp);
			Char2Hex(szTempchar, szTempHex, pszEndTemp - pszStartTemp);

			strcat(szExtraEmvTags, szTempHex);
		}
		else
		{

//			debug_sprintf(szDbgMsg, "%s: Index for this EMV Tag in the structure is %d", __FUNCTION__, pstHashLst->iTagIndexVal);
//			EHI_LIB_TRACE(szDbgMsg);

			if(pstHashLst->iTagIndexVal > E_REQRESP_MAX_FIELDS) //Safety Check
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Index Value!!!", __FUNCTION__);
				EHI_LIB_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
				//Should not be the case, something wrong
			}

			iEhiIndex  		= EmvTagLst[pstHashLst->iTagIndexVal].iEhiIndex;
			bConvert2Ascii	= EmvTagLst[pstHashLst->iTagIndexVal].bConvert2ascii;

			//iValLen = strlen(pszTagValue);

			//debug_sprintf(szDbgMsg, "%s: Length of the pszTagValue is %d", __FUNCTION__, iValLen);
			//EHI_LIB_TRACE(szDbgMsg);


			memset(szTempchar, 0X00, sizeof(szTempchar));
			memset(szTempHex, 0X00, sizeof(szTempHex));
			memcpy(szTempchar, ptrTagValue, len);


			if(iEhiIndex != NO_ENUM)
			{
				if(bConvert2Ascii == EHI_TRUE)
				{
					memset(pstElavonReqResDetails[iEhiIndex].elementValue, 0x00, sizeof(pstElavonReqResDetails[iEhiIndex].elementValue));
					memcpy(pstElavonReqResDetails[iEhiIndex].elementValue, szTempchar, sizeof(szTempchar));
				}
				else
				{
					Char2Hex(szTempchar, szTempHex, len);
					memset(pstElavonReqResDetails[iEhiIndex].elementValue, 0x00, sizeof(pstElavonReqResDetails[iEhiIndex].elementValue));
					memcpy(pstElavonReqResDetails[iEhiIndex].elementValue, szTempHex, sizeof(szTempHex));
				}
			}

			//break;
		}

		rv = changeinEMVTagValLength(pstElavonReqResDetails);
	}

	strcpy(pstElavonReqResDetails[E_EXTRA_EMV_TAGS_TLV].elementValue, szExtraEmvTags);
	free(pszStartEmvList);

	debug_sprintf(szDbgMsg, "%s:returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;

}


/*============================================================================
Hex2Bin - This is utility function to perform Hex to Binary conversion
on input data
@param - data. Pointer to input hex data
@param - length. number of bytes to convert
@param - dest. Buffer to hold converted data buffer
@return - length.
=============================================================================*/
int Hex2Bin (unsigned char *data, int length, unsigned char *dest)
{
    int c;
    unsigned char ch;
    unsigned char byte;
    for (c = 0; c < length; c++)
    {
        byte = 0;
        ch = *data++;

        if (ch >= '0' && ch <= '9')
        {
            ch -= '0';
        }
        else
        {
            if (ch >= 'A' && ch <= 'F')
            {
                ch -= 'A';
                ch += 10;
            }
            else if (ch >= 'a' && ch <= 'f')
            {
            	ch -= 'a';
            	ch += 10;
            }
            else
                return (-1);
        }

        byte = ch << 4;
        ch = *data++;

        if (ch >= '0' && ch <= '9')
        {
            ch -= '0';
        }
        else
        {
            if (ch >= 'A' && ch <= 'F')
            {
                ch -= 'A';
                ch += 10;
            }
            else if (ch >= 'a' && ch <= 'f')
            {
            	ch -= 'a';
            	ch += 10;
            }
            else
                return (-1);
        }

        byte |= ch;
        *dest = byte;
        dest++;
    }
    return (length);
}


/*This function will convert a character buffer into two byte Hex buff*/
void  Char2Hex(char *inBuff, char *outBuff, int iLen)
{
	char temp[2 + 1];
	char chHex[] = "0123456789ABCDEF\0";
	int i = 0, iDec;
	while(i < iLen)
	{
		iDec = inBuff[i++];
		memset(temp, 0x00, sizeof(temp));

		temp[1] = chHex[iDec%16];
		iDec = iDec/16;
		temp[0] = chHex[iDec%16];

		strcat(outBuff, temp);
	}
}

/*
 * ============================================================================
 * Function Name: changeinEMVTagValLength
 *
 * Description	: This function fills/stores the value at the corresponding
 * 				  index in the data structure
 *
 * Input Params	: Structure containing Elavon Details.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int changeinEMVTagValLength(ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int					rv				= SUCCESS;
	int					iCnt			= 0;
	int 				iIndex[]		= {E_TERM_CURRENCY_CODE_9F3C, E_APPL_CURR_CODE_9F42, E_TERM_CONTRY_CODE_9F1A, E_TRAN_CUUR_CODE_5F2A};
	char 				szTemp[256]		= "";


	for(iCnt = 0; iCnt < sizeof(iIndex)/sizeof(int); iCnt++)
	{

		if(strlen(pstElavonReqResDetails[iIndex[iCnt]].elementValue) == 4)
		{
			memset(szTemp, 0x00, sizeof(szTemp));
			strcpy(szTemp, pstElavonReqResDetails[iIndex[iCnt]].elementValue);
			memset(pstElavonReqResDetails[iIndex[iCnt]].elementValue, 0x00, sizeof(pstElavonReqResDetails[iIndex[iCnt]].elementValue));
			strcpy(pstElavonReqResDetails[iIndex[iCnt]].elementValue, szTemp+1);
		}

	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCVMResults
 *
 * Description	: This function CVM Results in ELAVON STRUCTURE
 *
 * Input Params	: Structure containing Elavon Details.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void getCVMResults(ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	char				cTmp 			= 0;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	/*	Field to indicate to the POS the cardholder verification methods. Valid values are:
	*	0 - Failed CVM
	*	1 - Signature required
	*	2 - PIN required
	*	3 - PIN and signature required*/

	if(strlen(pstElavonReqResDetails[E_CVM_REULTS_9F34].elementValue) > 0)
	{
		cTmp = pstElavonReqResDetails[E_CVM_REULTS_9F34].elementValue[1];
	}
	else
	{
		return;
	}

	if(cTmp == '0')
	{
		strcpy(pstElavonReqResDetails[E_EMV_CVM_VERCATN_IND].elementValue, "0");
	}
	else if(cTmp == 'E')
	{
		strcpy(pstElavonReqResDetails[E_EMV_CVM_VERCATN_IND].elementValue, "1");
	}
	else if(cTmp == '1' || cTmp == '2' || cTmp == '4')
	{
		strcpy(pstElavonReqResDetails[E_EMV_CVM_VERCATN_IND].elementValue, "2");
	}
	else if(cTmp == '3' || cTmp == '5')
	{
		strcpy(pstElavonReqResDetails[E_EMV_CVM_VERCATN_IND].elementValue, "3");
	}


	/*	Identifies if the PIN Pad is capable of a user entering a PIN. Valid Values:
		0 - Unknown
		1 - Can accept PIN, online PIN
		2 - Cannot accept PIN (Valid Value for Transponders)
		8 - PIN Pad is down
		9 - PIN verified offline by the chip*/

	if(cTmp == '1' || cTmp == '3' || cTmp == '4' || cTmp == '5')
	{
		strcpy(pstElavonReqResDetails[E_PIN_CAPABILITIES].elementValue, "9");
	}
	else
	{
		strcpy(pstElavonReqResDetails[E_PIN_CAPABILITIES].elementValue, "1");
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getChipConditionCode
 *
 * Description	: This function ICC Chip Condition Code in ELAVON STRUCTURE
 *
 * Input Params	: DHI structure,  ELAVON structure.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void getChipConditionCode(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int			iPresentFlag	=	0;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	/* Identifies the condition of the ICC chip. Values:
	 *	0 � ICC enabled card chip read
	 *	1 � ICC enabled card first magnetic stripe Fallback
	 *	2 � ICC enabled card not first magnetic stripe Fallback for this chip
	 *	3 � ICC enabled card that is manually entered
	 */

	if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
	{
		iPresentFlag = atoi(pstDHIDtls[ePRESENT_FLAG].elementValue);

		if(iPresentFlag == 5)
		{
			strcpy(pstElavonReqResDetails[E_CHIP_CONDITION_CODE].elementValue, "0");
		}
		else if(iPresentFlag == 7)
		{
			/*TODO : Technical Fallback will also ask directly for swipe after the first insert, Need clarity*/
			strcpy(pstElavonReqResDetails[E_CHIP_CONDITION_CODE].elementValue, "2");
		}
		else if((iPresentFlag == 3) && (strcmp(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "80")	== 0))
		{
			/*Under Assumption that Empty candidate list will ask directly for swipe after the first insert*/
			strcpy(pstElavonReqResDetails[E_CHIP_CONDITION_CODE].elementValue, "1");
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getPOSDataCode
 *
 * Description	: This function fills POS ENTRY MODE and Proximity Indicator in to ELAVON STRUCTURE
 *
 * Input Params	: DHI structure,  ELAVON structure.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getPOSDataCode(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int         rv             			 = 	SUCCESS;
	char		sz47APIField[30]		 =	"";
	char		szTemp[8+1]				 =  "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);


	if(stPymtncmd.iCommand == E_COMPLETION || stPymtncmd.iCommand == E_VOID)
	{
		if(pstDHIDtls[eELAVON_POS_DATA_CODE].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_POS_DATA_CODE].elementValue, pstDHIDtls[eELAVON_POS_DATA_CODE].elementValue);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: POS Data Code is not present in the POS request, filling it by EHI Module", __FUNCTION__);
			EHI_LIB_TRACE(szDbgMsg);
			getPOSDataCodeforFollowonTran(pstDHIDtls, pstElavonReqResDetails);
		}
	}
	else
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		get47APIField1234(szTemp);
		strcpy(sz47APIField, szTemp);

		if((strlen(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue) == 0)||strcmp(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "01") == 0)
		{
			/*47.5 - Cardholder Present Indicator*/
			strcat(sz47APIField, "1;");

			/*47.6 - Card Present Indicator*/
			strcat(sz47APIField, "0;");
		}
		else
		{
			/*47.5 - Cardholder Present Indicator*/
			strcat(sz47APIField, "0;");

			/*47.6 - Card Present Indicator*/
			strcat(sz47APIField, "1;");
		}

		/*47.7 - Card Data Input Mode Indicator*/
		if(strcmp(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "90") == 0)
		{
			strcat(sz47APIField, "2;");

		}
		else if(strcmp(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "91") == 0)
		{
			strcat(sz47APIField, "A;");
		}
		else if(strcmp(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "01") == 0)
		{
			strcat(sz47APIField, "6;");
		}
		else if(strcmp(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "05") == 0)
		{
			strcat(sz47APIField, "5;");
		}
		else if(strcmp(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "07") == 0)
		{
			strcat(sz47APIField, "M;");
		}
		else if(strcmp(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "80") == 0)
		{
			/*Will come here in two cases 1. Technical fall back, 2.Empty Candidate list(EMV is enabled but present flag is 3 for service code 2xx,6xx)*/
			if((pstDHIDtls[ePRESENT_FLAG].elementValue != NULL) && (atoi(pstDHIDtls[ePRESENT_FLAG].elementValue) == 3))
			{
				strcat(sz47APIField, "P;");			// if Present flag is 3 when EMV is enabled and service code is 2xx,6xx, we are treating as Empty candidate list fall back
			}
			else
			{
				strcat(sz47APIField, "9;");			// Technical fall back
			}
		}
		else
		{
			strcat(sz47APIField, "6;");
		}

		/*47.8 - Card Data Authentication Mode Indicator*/
		if((pstDHIDtls[ePIN_BLOCK].elementValue != NULL) || (strcmp(pstElavonReqResDetails[E_PIN_CAPABILITIES].elementValue, "9") == 0))
		{
			strcat(sz47APIField, "1;");
		}
		else
		{
			strcat(sz47APIField, "0;");
		}

		/*47.9 - Cardholder Authentication Mode Indicator*/
		if(strcmp(pstElavonReqResDetails[E_PIN_CAPABILITIES].elementValue, "9") == 0)
		{
			strcat(sz47APIField, "1;");
		}
		else if((pstDHIDtls[ePIN_BLOCK].elementValue != NULL))
		{
			strcat(sz47APIField, "3;");
		}
		else
		{
			strcat(sz47APIField, "0;");
		}


		/*47.10 - Card Data Output Capability Indicator*/
		if(strcmp(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "05") == 0)
		{
			strcat(sz47APIField, "3;");
		}
		else
		{
			strcat(sz47APIField, "1;");
		}


		memset(szTemp, 0x00, sizeof(szTemp));
		get47APIField11121314(szTemp);
		strcat(sz47APIField, szTemp);

		strcpy(pstElavonReqResDetails[E_POS_DATA_CODE].elementValue, sz47APIField);
	}

	debug_sprintf(szDbgMsg, "%s: POS DATA CODE: [%s]", __FUNCTION__, pstElavonReqResDetails[E_POS_DATA_CODE].elementValue);
	EHI_LIB_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPOSDataCodeforFollowonTran
 *
 * Description	: This function fills POS DATA CODE for follow on tran in to ELAVON STRUCTURE
 *
 * Input Params	: DHI structure,  ELAVON structure.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void getPOSDataCodeforFollowonTran(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int 		iPresentFlag			 =  0;
	char		sz47APIField[30]		 =	"";
	char		szTemp[8+1]				 =  "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	memset(szTemp, 0x00, sizeof(szTemp));
	get47APIField1234(szTemp);
	strcpy(sz47APIField, szTemp);

	/*In Case of Debit Void, we can capture the card details, in that case it will go to first case, if not it will take default values*/
	if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
	{
		iPresentFlag = atoi(pstDHIDtls[ePRESENT_FLAG].elementValue);

		switch(iPresentFlag)
		{
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			/*47.5 - Cardholder Present Indicator*/
			strcat(sz47APIField, "0;");

			/*47.6 - Card Present Indicator*/
			strcat(sz47APIField, "1;");
			break;
		default:
			/*47.5 - Cardholder Present Indicator*/
			strcat(sz47APIField, "1;");

			/*47.6 - Card Present Indicator*/
			strcat(sz47APIField, "0;");
			break;
		}

		switch(iPresentFlag)
		{
		case 1:
		case 2:
			strcat(sz47APIField, "6;");
			break;
		case 3:
			strcat(sz47APIField, "2;");
			break;
		case 4:
			strcat(sz47APIField, "A;");
			break;
		case 5:
			strcat(sz47APIField, "5;");
			break;
		case 6:
			strcat(sz47APIField, "9;");
			break;
		case 7:
			strcat(sz47APIField, ";");
			break;
		default:
			strcat(sz47APIField, "6;");
			break;
		}


		/*47.8 - Card Data Authentication Mode Indicator*/
		if(pstDHIDtls[ePIN_BLOCK].elementValue != NULL)
		{
			strcat(sz47APIField, "1;");
		}
		else
		{
			strcat(sz47APIField, "0;");
		}

		/*47.9 - Cardholder Authentication Mode Indicator*/
		if(pstDHIDtls[ePIN_BLOCK].elementValue != NULL)
		{
			strcat(sz47APIField, "3;");
		}
		else
		{
			strcat(sz47APIField, "0;");
		}

		/*47.10 - Card Data Output Capability Indicator*/
		/*Completion, Void we won't consider them as a EMV transactions, so using the value of MSR below*/
		strcat(sz47APIField, "1;");
	}
	else
	{
		/*For Completion we will never capture card details, so giving 47.5 as card holder not present, 47.6 as card not present*/
		/*47.5 - Cardholder Present Indicator*/
		strcat(sz47APIField, "1;");

		/*47.6 - Card Present Indicator*/
		strcat(sz47APIField, "0;");

		/*47.7 - Card Data Input Mode Indicator*/
		/*For Completion or Void , we won't capture card details so filling 47.7 as Key Entry Only(for Card token present scenario), terminal not capable of reading card(for no card details scenario)*/
		strcat(sz47APIField, "6;");


		/*47.8 - Card Data Authentication Mode Indicator*/
		/* In case of Completion and Void, we will never capture PIN details*/
		strcat(sz47APIField, "0;");

		/*47.9 - Cardholder Authentication Mode Indicator*/
		strcat(sz47APIField, "0;");


		/*47.10 - Card Data Output Capability Indicator*/
		/*Completion, Void we won't consider them as a EMV transactions, so using the value of MSR below*/
		strcat(sz47APIField, "1;");
	}

	memset(szTemp, 0x00, sizeof(szTemp));
	get47APIField11121314(szTemp);
	strcat(sz47APIField, szTemp);

	strcpy(pstElavonReqResDetails[E_POS_DATA_CODE].elementValue, sz47APIField);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getChipConditionCode
 *
 * Description	: This function fills POS ENTRY MODE and Proximity Indicator in to ELAVON STRUCTURE
 *
 * Input Params	: DHI structure,  ELAVON structure.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getCardEntryMode(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int         rv              = 	SUCCESS;
	int			iPresentFlag	=	0;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	/* 0054:POS ENTRY MODE : Identifies how the card data was entered. Values:
	 *	00 - Unknown
	 *	01 - Manual/Key Entry
	 *	02 - Key Entry Card Present
	 *	04 - OCR code read
	 *	05 - Integrated circuit card read - CVV data reliable (smart card)
	 *	07 - Contactless M/Chip or Visa Smart Card read
	 *	10 - Scanned MICR/check
	 *	80 - Chip Card capable - unaltered track data read (used for EMV fall back to swipe)
	 *	82 - Contactless Mobile Commerce device
	 *	90 - Magnetic Stripe - CVV/CVC certified, unaltered Track Data (1 or 2) included in Authorization Request. Required to participate in PS/2000 or CPS/2000
	 *	91 - Contactless chip magnetic stripe read
	 *	95 - Integrated circuit card read - CVV data unreliable
	 *	85 - Internet (not an ISO value)
	 *	For follow on transactions, this field will be 01 as we are sending the card token and expiry date
	 */


	/*0052 : TRANSPONDER/PROXIMITY IND :Identifies if the card was read contactless and whether it supports EMV or not. Valid values:
	 *	0 - no transponder
	 *	1 - transponder initiated transaction,
	 *	2 - Proximity using qVSDC rules,
	 *	3 - Proximity using magstripe rules,
	 *	4 - Chip/Smart Card (i.e. PayPass M/Chip),
	 *	5 - Transponder equipped using magstripe or key entry
	 */

	/*In Case of Completion and Void transactions we will honor 0052,0054 from the POS, if POS does not send EHI will fill those values*/
	if(stPymtncmd.iCommand == E_COMPLETION || stPymtncmd.iCommand == E_VOID)
	{
		if(pstDHIDtls[eELAVON_POS_ENTRY_MODE].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, pstDHIDtls[eELAVON_POS_ENTRY_MODE].elementValue);
		}

		if(pstDHIDtls[eELAVON_PROXIMITY_IND].elementValue != NULL)
		{
			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, pstDHIDtls[eELAVON_PROXIMITY_IND].elementValue);
		}
	}


	/*0054:POS ENTRY MODE*/
	if(strlen(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue) > 0)
	{
		/*If it is already filled do nothing, its possible in case of EMV REVERSALS, COMPLETION, VOID*/
	}
	else if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
	{
		iPresentFlag = atoi(pstDHIDtls[ePRESENT_FLAG].elementValue);

		switch(iPresentFlag)
		{
		case 1:
		case 2:
			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "01");
			break;
		case 3:
			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "90");
			if(isEmvEnabled() == EHI_TRUE)
			{
				/*When EMV is enabled and present flag is 3 and service code is 2xx,6xx, then the transaction is treated as EMPTY candidate list fallback*/
				changePosEntryModeBySrvcCodeCheck(pstDHIDtls,pstElavonReqResDetails);
			}
			break;
		case 4:
			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "91");
			break;
		case 5:
			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "05");
			break;
		case 6:
			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "07");
			break;
		case 7:
			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "80");
			break;
		default:
			rv = ERR_INV_FLD;
			break;
		}
	}
	else if(strlen(pstElavonReqResDetails[E_CARD_TOKEN].elementValue) > 0)
	{
		strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "01");
	}


	/*0052 : TRANSPONDER/PROXIMITY IND*/
	if(strlen(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue) > 0)
	{
		/*If it is already filled do nothing, its possible in case of EMV REVERSALS, COMPLETION, VOID*/
	}
	else if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
	{
		iPresentFlag = atoi(pstDHIDtls[ePRESENT_FLAG].elementValue);

		switch(iPresentFlag)
		{
		case 1:
		case 2:
			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");
			break;
		case 3:
			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");
			break;
		case 4:
			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "3");
			break;
		case 5:
			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "0");
			break;
		case 6:
			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "4");
			break;
		case 7:
			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");
			break;
		default:
			rv = ERR_INV_FLD;
			break;
		}
	}
	else if(strlen(pstElavonReqResDetails[E_CARD_TOKEN].elementValue) > 0)
	{
		strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");
	}



	//
	//	if((strlen(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue) > 0) && (strlen(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue) > 0))
	//	{
	//		/*If it is already filled do nothing, its possible in case of EMV REVERSALS*/
	//	}
	//	else if(pstDHIDtls[ePRESENT_FLAG].elementValue != NULL)
	//	{
	//		iPresentFlag = atoi(pstDHIDtls[ePRESENT_FLAG].elementValue);
	//
	//		switch(iPresentFlag)
	//		{
	//		case 1:
	//		case 2:
	//			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "01");
	//			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");
	//			break;
	//		case 3:
	//			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "90");
	//			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");
	//			if(isEmvEnabled() == EHI_TRUE)
	//			{
	//				/*When EMV is enabled and present flag is 3 and service code is 2xx,6xx, then the transaction is treated as EMPTY candidate list fallback*/
	//				changePosEntryModeBySrvcCodeCheck(pstDHIDtls,pstElavonReqResDetails);
	//			}
	//			break;
	//		case 4:
	//			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "91");
	//			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "3");
	//			break;
	//		case 5:
	//			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "05");
	//			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "0");
	//			break;
	//		case 6:
	//			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "07");
	//			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "4");
	//			break;
	//		case 7:
	//			strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "80");
	//			strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");
	//			break;
	//		default:
	//			rv = ERR_INV_FLD;
	//			break;
	//		}
	//	}
	//	else if(strlen(pstElavonReqResDetails[E_CARD_TOKEN].elementValue) > 0)
	//	{
	//		strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "01");
	//		strcpy(pstElavonReqResDetails[E_PROXIMITY_IND].elementValue, "5");
	//	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getConvertedBusinessDate
 *
 * Description	: This function Converts the Business date to required format
 *
 * Input Params	: DHI structure,  ELAVON structure.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
/*static int getConvertedBusinessDate(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int			rv				= 0;
	char 		szDate[6+1]		= "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);


	if((pstDHIDtls[eBUSINESS_DATE].elementValue != NULL) && (strlen(pstDHIDtls[eBUSINESS_DATE].elementValue) == 8))
	{
		memset(szDate, 0x00 , sizeof(szDate));
		strncpy(szDate, pstDHIDtls[eBUSINESS_DATE].elementValue +4, 2);
		strncat(szDate, pstDHIDtls[eBUSINESS_DATE].elementValue +6, 2);
		strncat(szDate, pstDHIDtls[eBUSINESS_DATE].elementValue +2, 2);

		debug_sprintf(szDbgMsg, "%s: Business Date [%s]", __FUNCTION__, szDate);
		EHI_LIB_TRACE(szDbgMsg);

		strcpy(pstElavonReqResDetails[E_BUSINESS_DATE].elementValue, szDate);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Business Date is Invalid", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
	}


	debug_sprintf(szDbgMsg, "%s: Returning:[%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}*/

/*
 * ============================================================================
 * Function Name: getConvertedDate
 *
 * Description	: This function Converts the date API 0013 to required format
 *
 * Input Params	: DHI structure,  ELAVON structure.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
/*static int getConvertedDate(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{
	int			rv				= 0;
	char 		szDate[6+1]		= "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);


	if((pstDHIDtls[eDATE].elementValue != NULL) && (strlen(pstDHIDtls[eDATE].elementValue) == 8))
	{
		memset(szDate, 0x00 , sizeof(szDate));
		strncpy(szDate, pstDHIDtls[eDATE].elementValue , 4);
		strncat(szDate, pstDHIDtls[eDATE].elementValue +6, 2);

		debug_sprintf(szDbgMsg, "%s: Business Date [%s]", __FUNCTION__, szDate);
		EHI_LIB_TRACE(szDbgMsg);

		strcpy(pstElavonReqResDetails[E_DATE].elementValue, szDate);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Date is Invalid", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
	}


	debug_sprintf(szDbgMsg, "%s: Returning:[%d]", __FUNCTION__, rv);
	EHI_LIB_TRACE(szDbgMsg);

	return rv;
}*/

/*
 * ============================================================================
 * Function Name:  changePosEntryModeBySrvcCodeCheck
 *
 * Description	:  This function will change POS Entry Mode Value for Empty Candidate List.
 *
 * Input Params	:  DHI structure, ELAVON structure.
 *
 * Output Params: None
 * ============================================================================
 */
void changePosEntryModeBySrvcCodeCheck(TRANDTLS_PTYPE pstDHIDtls, ELAVONTRANDTLS_PTYPE pstElavonReqResDetails)
{

	int 	iTrackIndicator 		= 0;
	int		iAppLogEnabled			= 0;
	char    cServicecode        	= 0;
	char	szTrackData[79+1]		= "";
	char    szExpnSrvcCode[5+1]		= "";
	char	szAppLogData[256]		= "";
	char*	pszName					= NULL;
	char*	pszExp					= NULL;

#ifdef DEBUG
	char	  szDbgMsg[256]      = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);

	iAppLogEnabled = isAppLogEnabled();

	if(pstDHIDtls[eTRACK_DATA].elementValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Track Data, Returning ", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return;
	}

	if(pstDHIDtls[eTRACK_INDICATOR].elementValue != NULL)
	{
		iTrackIndicator= atoi(pstDHIDtls[eTRACK_INDICATOR].elementValue);
	}

	memset(szTrackData, 0x00, sizeof(szTrackData));
	memset(szExpnSrvcCode, 0x00, sizeof(szExpnSrvcCode));
	strcpy(szTrackData, pstDHIDtls[eTRACK_DATA].elementValue);

	if (iTrackIndicator == 1)
	{
		if((pszName = strchr(szTrackData, '^')) != NULL)
		{
			if((pszExp = strchr(pszName + 1, '^')) != NULL)
			{
				strncpy(szExpnSrvcCode, pszExp + 1, 5);

				if(szExpnSrvcCode[0] != '^')
				{
					if(szExpnSrvcCode[4] != '^')
					{
						cServicecode = szExpnSrvcCode[4];
					}
				}
				else if (szExpnSrvcCode[0] == '^')
				{
					if(szExpnSrvcCode[1] != '^')
					{
						cServicecode = szExpnSrvcCode[1];
					}
				}
			}
		}

	}
	else if (iTrackIndicator == 2)
	{
		if((pszExp = strchr(szTrackData, '=')) != NULL)
		{
			strncpy(szExpnSrvcCode, pszExp + 1, 5);

			if(szExpnSrvcCode[0] != '=')
			{
				if(szExpnSrvcCode[4] != '=')
				{
					cServicecode = szExpnSrvcCode[4];
				}
			}
			else if (szExpnSrvcCode[0] == '=')
			{
				if(szExpnSrvcCode[1] != '=')
				{
					cServicecode = szExpnSrvcCode[1];
				}
			}

		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Track indicator, Returning", __FUNCTION__);
		EHI_LIB_TRACE(szDbgMsg);
		return;
	}


	if((cServicecode == '2') || (cServicecode == '6'))
	{
		/*As per Elavon Mail on JUNE/08/2016 with subject :Outstanding Issues With Elavon, we should send POS Entry Mode as 80 for Empty candidate List*/
		strcpy(pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue, "80");

		debug_sprintf(szDbgMsg, "%s: Service code [%c] ,changed POS Entry Mode to [%s]", __FUNCTION__, cServicecode, pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue);
		EHI_LIB_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Service Code [%c] POS Entry Mode Changed to [%s]", cServicecode, pstElavonReqResDetails[E_POS_ENTRY_MODE].elementValue);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Service code [%c] , No change in POS Entry Mode", __FUNCTION__, cServicecode);
		EHI_LIB_TRACE(szDbgMsg);
	}


	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	EHI_LIB_TRACE(szDbgMsg);
	return ;
}
