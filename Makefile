# ----------------------------------------------------------------------------
# Project: Point Mx SCA
#
# Purpose: For running recursive make to create the application libraries and
# 			then application executables using those libraries
#
# Author : Vikram Datt Rana
#
# Version History:
# ------------------------------------------
# Version	Date			Author
# -------	---------		----------------
# 1.0.0		12Aug2013		Vikram Datt Rana
# ----------------------------------------------------------------------------

MODULE_DIR := ./Modules

module_DHIApp := $(MODULE_DIR)/DHI_App
module_rchi   := $(MODULE_DIR)/RCHI
module_cphi   := $(MODULE_DIR)/CPHI
module_ehi    := $(MODULE_DIR)/EHI

modules := $(module_DHIApp) $(module_rchi) $(module_cphi) $(module_ehi)
executables := ./appExecData

.PHONY: all $(executables) $(modules) clean

all: $(executables)

clean:
	@$(MAKE) TARGET=clean

REL:
	@$(MAKE) TARGET=REL

DEB:
	@$(MAKE) TARGET=DEB

$(executables) $(modules):
	$(MAKE) --directory=$@ $(TARGET)

$(executables): $(modules)

#TODO: Define dependencies for the modules also
