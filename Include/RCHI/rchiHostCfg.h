/*
 * rccurl.h
 *
 *  Created on: Dec 8, 2014
 *      Author: BLR_SCA_TEAM
 */

#ifndef RCHOSTCFG_H_
#define RCHOSTCFG_H_

typedef struct
{
	char *		url;
	int			portNum;
	int			conTimeOut;
	int			respTimeOut;
}
HOSTDEF_STRUCT_TYPE, * HOSTDEF_PTR_TYPE;

typedef struct
{
	char *szMsg;
	int iWritten;
	int iTotSize;
}
RESP_DATA_STYPE, *RESP_DATA_PTYPE;

typedef enum
{
	PRIMARY_URL = 0,
	SECONDARY_URL,
	SERVICEDISCOVERY_URL,
	FEXCO_URL,
	REGISTRATION_URL
}
HOST_URL_ENUM;

#define DFLT_KEEP_ALIVE_INTERVAL	10
#define KEEP_ALIVE_DISABLED			0
#define KEEP_ALIVE_PERMANENT		1
#define KEEP_ALIVE_DEFINED_TIME		2

#define DATAWIRE_TID_LENGTH			8

extern int setRCHIHostUrl(HOST_URL_ENUM, char*);
extern int getKeepAliveParameter();
extern int getKeepAliveInterval();
#endif /* RCHOSTCFG_H_ */
