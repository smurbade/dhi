#ifndef __UTILS_H
#define __UTILS_H

typedef enum
{
	LOCALTIME = 1,
	GMTTIME
}
TIME_FORMAT;

typedef unsigned char 		uchar;

extern void getTime(char*, int);
extern int validateNumericField(char*);
extern int createHashTableForRCFields();
extern int doesFileExist(const char *);
extern int isFGNCard(char* pszPan);

extern void acquireRCMutexLock(pthread_mutex_t * , const char * );
extern void releaseRCMutexLock(pthread_mutex_t * , const char * );
#endif
