/* 
 * --------------------------------------------------------
 * -------------- SSI DATA FIELDS ------------------------
 * --------------------------------------------------------
 */
 
 #include "DHI_App/tranDef.h"
 #include "RCHI/rchiResp.h"
 
static RCHI_RESP_KEYVAL_STYPE genRespLst[] =
{	
	{ "PymtType"			, NO_ENUM			  , NO_ENUM},
	{ "TxnType"				, NO_ENUM			  , NO_ENUM},
	{ "LocalDateTime"		, NO_ENUM			  , eTRANS_DATE},
	{ "TrnmsnDateTime"		, NO_ENUM			  , eTRANS_TIME},
	{ "STAN"      			, NO_ENUM			  , eTROUTD},
	{ "RefNum" 				, NO_ENUM			  , eCTROUTD},
	{ "OrderNum"   	 		, NO_ENUM			  , NO_ENUM},
	{ "TermID"  	 		, NO_ENUM			  , eTERMID},
	{ "MerchID"  	 		, NO_ENUM			  , eMERCHID},
	{ "AltMerchID"  		, NO_ENUM			  , NO_ENUM},
	{ "MerchCatCode"   		, NO_ENUM		 	  , NO_ENUM},
	{ "POSEntryMode"   		, NO_ENUM			  , NO_ENUM},
	{ "POSCondCode"   		, NO_ENUM			  , NO_ENUM},
	{ "TermCatCode"   		, NO_ENUM			  , NO_ENUM},
	{ "TermEntryCapablt"	, NO_ENUM			  , NO_ENUM},
	{ "TxnAmt"  			, RC_CARD_TRANS_AMOUNT, eAPPROVED_AMOUNT},
	{ "TxnCrncy"  			, NO_ENUM			  , NO_ENUM},
	{ "TermLocInd"			, NO_ENUM			  , NO_ENUM},
	{ "CardCaptCap"			, NO_ENUM			  , NO_ENUM},
	{ "POSID"		  		, NO_ENUM		      , NO_ENUM},
	{ "PLPOSDebitFlg"  		, RC_PLESS_POS_DEB_FLG, NO_ENUM},

	{ "AcctNum" 			, RC_ACCT_NUM		  , eEMBOSSED_ACCT_NUM},
	{ "CardExpiryDate"  	, NO_ENUM  			  , NO_ENUM},
		
	{ "AddAmt"		  		, RC_ADDTL_BAL_AMT			, NO_ENUM},
	{ "AddAmtCrncy"  		, RC_ADDTL_BAL_AMT_CRNCY	, NO_ENUM},
	{ "AddAmtType"	  		, RC_ADDTL_BAL_AMT_TYPE		, NO_ENUM},
	{ "AddAmtAcctType"		, RC_ADDTL_BAL_AMT_ACC_TYPE	, NO_ENUM},
	
	{ "AddAmt"		  		, RC_ADDTL_BAL_AMT_1			, NO_ENUM},
	{ "AddAmtCrncy"  		, RC_ADDTL_BAL_AMT_CRNCY_1		, NO_ENUM},
	{ "AddAmtType"	  		, RC_ADDTL_BAL_AMT_TYPE_1		, NO_ENUM},
	{ "AddAmtAcctType"		, RC_ADDTL_BAL_AMT_ACC_TYPE_1	, NO_ENUM},
		
	{ "RespCode"			, RC_RESP_CODE	  	, eHOST_RESPCODE},
	{ "AuthID"  			, RC_AUTH_ID 	  	, eAUTH_CODE	},
	{ "AddtlRespData"  		, RC_RESPONSE_TEXT	, eRESPONSE_TEXT},
	{ "SttlmDate"   		, RC_STTLMT_DATE  	, NO_ENUM		},
	{ "AthNtwkID"    		, RC_ATH_NTWK_ID  	, eAUTH_NWID	},
	{ "ErrorData"        	, RC_ERROR_DATA	  	, eRESPONSE_TEXT},
	
	{ "Tkn"  				, RC_CARD_TOKEN		, eCARD_TOKEN	},
	{ "HoldInfo"			, RC_HOLD_INFO		, NO_ENUM},
	
	{ "ACI"					, RC_AUTH_CHARACTERISTICS_IND, NO_ENUM},
	{ "CardLevelResult"		, RC_CARD_LEVEL_RESULT		 , NO_ENUM},
	{ "TransID"				, RC_TRAN_ID				 , NO_ENUM},
	
	{ "BanknetData"			, RC_BANK_NET_DATA		, NO_ENUM},
	{ "CCVErrorCode"		, RC_CCV_ERROR_CODE		, NO_ENUM},
	{ "POSEntryModeChg"		, RC_POSENTRYMODE_CHG	, NO_ENUM},
	
	{ "DiscProcCode"		, RC_DISC_PROC_CODE	 , NO_ENUM},
	{ "DiscPOSEntry"		, RC_DISC_POS_ENTRY  , NO_ENUM},
	{ "DiscRespCode"		, RC_DISC_RESP_CODE  , NO_ENUM},
	{ "DiscPOSData"			, RC_DISC_POS_DATA	 , NO_ENUM},
	{ "DiscTransQualifier"	, RC_DISC_TRAN_QUALIF, NO_ENUM},
	{ "DiscNRID"			, RC_DISC_NRID 		 , NO_ENUM},
	
	{ "AmExPOSData"			, RC_AMEX_POS_DATA	, NO_ENUM},
	{ "AmExTranID"			, RC_AMEX_TRAN_ID 	, NO_ENUM},
	{ "EMVData"				, NO_ENUM			, eEMV_TAGS_RESP},
	{ "XCodeResp"			, RC_X_CODE_RESP	, NO_ENUM},
	{ "FunCode"				, RC_FUNC_CODE		, NO_ENUM},
	{ "NextFileDLOffset"	, RC_REQ_FL_OFF		, NO_ENUM},
	{ "CurrFileCreationDt"  , RC_FL_CREAT_DATE	, NO_ENUM},
	{ "FileSize"			, RC_FL_SZ			, NO_ENUM},
	{ "FileCRC16"			, RC_FL_CRC			, NO_ENUM},
	{ "RtInd"  				, RC_PLESS_POS_DEB_FLG, NO_ENUM},
	
	{ "REFERENCE"					, RC_REFERENCE						, eREF						},
	{ "EXCHANGERATE"				, RC_EXCHANGERATE					, eEXGN_RATE 				},
	{ "FGNCURCODE" 					, RC_FGNCURCODE						, eFGN_CUR_CODE				},
	{ "FGNAMOUNT"       			, RC_FGNAMOUNT						, eFGN_AMT					},
	{ "DCCOFFERED"					, RC_DCCOFFERED						, eDCC_OFFD					},		
	{ "VALIDHOURS"					, RC_VALIDHOURS						, eVALID_HRS				},
	{ "MARGINRATEPERCENTAGE"		, RC_MARGINRATEPERCENTAGE			, eMRGN_RATE_PERCENT		},
	{ "EXCHANGERATESOURCENAME"		, RC_EXCHANGERATESOURCENAME			, eEXGN_RATE_SRC_NAME		},
	{ "COMMISSIONPERCENTAGE"		, RC_COMMISSIONPERCENTAGE			, eCOMM_PERCENT				},
	{ "EXCHANGERATESOURCETIMESTAMP" , RC_EXCHANGERATESOURCETIMESTAMP	, eEXGN_RATE_SRCTIMESTAMP	},
	{ "MINORUNITS"					, RC_MINORUNITS						, eMINR_UNT					},
	{ "RESPONSECODE"				, RC_RESPONSE_CODE					, eRESPONSE_CODE			},
	{ "TAExpDate"  			, RC_TA_EXP_DATE	, NO_ENUM}
};