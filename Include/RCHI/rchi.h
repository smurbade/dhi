/*
 * rchi.h
 *
 *  Created on: 09-Dec-2014
 *  Author: BLR_SCA_TEAM
 */

#ifndef RC_H_
#define RC_H_

#define DW_REQUEST			"REQUEST"
#define CREDIT_REQUEST		"CreditRequest"
#define DEBIT_REQUEST		"DebitRequest"
#define	REVERSAL_REQUEST	"ReversalRequest"
#define ADMIN_REQUEST		"AdminRequest"

extern int 	initHILib();
extern int 	getMetaDataForRCReq(METADATA_PTYPE, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE, int);
extern int 	postDataToRCHost(char *, int, char **, int *);
extern int postDataToFEXCOHost(char *, int, char **, int *);
extern int 	processReportCommand(TRANDTLS_PTYPE);
extern int 	postRegReqToDWHost(char *, int, char **, int *, int);
extern int 	createTimeoutReversalThread();
extern int 	createEMVKeyStatThread();
extern void retryTimeoutReversal(char*, int);

#endif /* RC_H_ */
