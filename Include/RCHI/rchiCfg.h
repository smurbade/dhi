#ifndef __RC_CFG_H
#define __RC_CFG_H

/* Section in the config file */
#define SECTION_REG 			"REG"
#define SECTION_DHI 			"DHI"
#define SECTION_RCHI 			"RCHI"
#define SECTION_DCT				"DCT"
#define SECTION_HDT				"HDT"
#define SECTION_RCHIGROUPS		"RCHIGROUPS"
#define SECTION_RCHIRESPONSE	"RCHIRESPONSE"


/* DHI Params in DHI Section */
#define STAN					"stan"
#define REFERENCE_NUM			"refnum"
#define TPP_ID 					"tppid"
#define TERMINAL_ID				"tid"
#define MERCHANT_ID				"mid"
#define	MERCHANT_ID_DCC			"dccmerchid"
#define ACQUIRER				"dccacquirer"
#define MERCH_CAT_CODE			"merchantcatcode"
#define TRANSACTION_CURRENCY	"trancrncy"
#define GROUP_ID				"groupid"
#define POS_ID					"laneid"			/* POSSIBLY LANE ID IS SAME AS POS ID */
#define ADDTL_AMT_CURRENCY		"addamtcrncy"
#define ALTERNATE_MERCH_ID		"altmerchid"
#define KEY_ID					"keyid"
#define DOMAIN					"domain"
#define BRAND					"brand"
#define RC_DATAWIRE_ID			"datawareid"
#define RC_SERVICE_ID			"serviceid"
#define RC_APP_ID				"appid"
#define CLIENT_REF_NUM			"clientrefnum"
#define PRIM_HOST_URL			"primurl"
#define PRIM_HOST_PORT			"primport"
#define SCND_HOST_URL			"scndurl"
#define SCND_HOST_PORT			"scndport"
#define PRIM_CONN_TO			"primcontimeout"
#define SCND_CONN_TO			"scndcontimeout"
#define PRIM_RESP_TO			"primrestimeout"
#define SCND_RESP_TO			"scndresptimeout"
#define EMBOSSED_NUM_CALC_REQD	"embossednumcalcreqd"
#define STRIP_MID_REQD			"stripMIDreqd"
#define TOKEN_TYPE				"tokentype"
#define EMV_KEY_DUR				"emvkeydur"
#define EMV_SETUP_REQ			"emvsetupreqd"
#define FGN_CARD_BIN_DATA_FILE	"fgncardbindatafile"
#define CURRENCY_DATA_FILE		"currencydatafile"
#define DCC_ENABLED				"dccenabled"
#define RC_VERSION				"rcschemaver"


/* Under Reg Section*/
#define	TIME_ZONE				"timezone"

#define REG_HOST_URL			"reghosturl"
#define REG_HOST_PORT			"reghostport"
#define REG_CONN_TO				"regcontimeout"
#define REG_RESP_TO				"regresptimeout"

#define	FEXCO_HOST_URL			"fexcohosturl"
#define FEXCO_HOST_PORT			"fexcohostport"
#define	FEXCO_CONN_TO			"fexcocontimeout"
#define	FEXCO_RESP_TO			"fexcoresptimeout"

/* Keep Alive Params in HDT Section */
#define KEEP_ALIVE				"keepalive"
#define KEEP_ALIVE_INTERVAL		"keepaliveinterval"

#define VSP_ENABLED				"vspenabled"
#define EMV_ENABLED				"emvenabled"
#define CTLS_EMV_ENABLED		"contactlessemvenabled"
#define FORCE_PL_DEBIT_FLAG		"forcepldebitflag"

#define DFLT_HOSTCONN_TIMEOUT	10
#define DFLT_HOSTRESP_TIMEOUT	10

/* RC Constant parameters */
#define TERM_CAT_CODE			"01"				/* 2bytes: 01-POS */
#define TERM_LOCATION_IND		"0"					/* 1byte: 0: On Premises and 1: Off Premises */
#define CARD_CAPTURE_CAPABLT	"1"					/* 1byte: Card Capture Capability is enabled */
#define SECURITY_LVL			"EncrptTknizatn"	/* 15bytes: Encryption and Tokenization */
#define ENCRPT_TYPE				"Verifone"			/* 8bytes */

typedef struct
{
	int					iKeepAlive;			// Implemented to keep the connection alive if needed
	int					iKeepAliveInt;		// Duration/Interval to keep the connection alive
	unsigned 			iStan;
	unsigned long		iRefNum;
	unsigned long		iClientRefNum;
	unsigned long		iCardRateRefNum;
	char 				szTPPID[7];
	char 				szTerminalID[9];
	char 				szMerchantID[17];
	char				szDCCMerchantID[17];		// DCC Merchant ID
	char				szDCCAcquirer[4];				// DCC Acquirer
	char 				szMerchCatCode[5];
	char 				szTranCurrency[4];
	char 				szGroupID[14];
	char 				szPOSID[5];
	char 				szAddtlAmtCurrency[4];
	char 				szAlternateMerchID[16];
	char 				szKeyID[13]; //Key Id = DOMAIN;BRAND
	char				szDomin[6];
	char				szBrand[6];
	char 				szServiceID[4];
	char 				szAppID[16];
	char 				szDatawireID[33];
	char 				szAuth[42];
	char				szPassword[7];
	char				szTokenType[5];
	char				szFgnBinDataFile[128];
	char				szCurrencyDataFile[128];
	char				szRCVersion[10];
	RCHI_BOOL			vspEnabled;
	RCHI_BOOL			emvEnabled;
	RCHI_BOOL			ctlsEmvEnabled;
	RCHI_BOOL			bEmbossedNumCalReqd;
	RCHI_BOOL			bStripMIDReqd;
	RCHI_BOOL			bDCCEnabled;
	HOSTDEF_STRUCT_TYPE	hostDef[5];
	int					iNumUniqueHosts;
	int 				emvkeydur;
	RCHI_BOOL 			forcePLDebit;
}
RC_CFG_STYPE, *RC_CFG_PTYPE;

typedef struct
{
	int iStan;
	int iRefNum;
	int iClntRefNum;
	int	iCardRateRefNum;
}
RC_FILE_PARAMS_STYPE, *RC_FILE_PARAMS_PTYPE ;


extern void getTime(char*, int);
extern void getSTAN(char*);
extern int 	getEmvKeyStatdur();
extern void getTPPID(char*);
extern RCHI_BOOL isEMVEnabledInDevice();
extern RCHI_BOOL isEMVCTLSEnabledInDevice();
extern RCHI_BOOL isDCCEnabledInDevice();
extern void getTerminalID(char*);
extern void getMerchantID(char*);
extern void getMerchCatCode(char*);
extern void getTranCurrency(char*);
extern void getGroupID(char*);
extern void getAddtlAmtCurrency(char*);
extern void getAlternateMerchID(char*);
extern void getServiceID(char*);
extern void getClientTimeOut(char*);
extern void getClientTimeOutForFEXCO(char *);
extern void getAppID(char*);
extern void getDatawireID(char*);
extern void getAuth(char*);
extern void getTermCatCode(char*);
extern void getTermLocationIndicator(char*);
extern void getCardCaptureCapablt(char*);
extern void getSecurityLvl(char*);
extern void getEncrptType(char*);
extern void getKeyID(char*);
extern void getTokenType(char*);
extern void getRefNum(char *);
extern int	getRCHostDetails(HOST_URL_ENUM, HOSTDEF_PTR_TYPE);
extern void getClientRefNum(char*);
extern void getHostTotalsPassword(char*);
extern void getCurrentClientRefNum(char *);
extern void getCurrentRefNum(char *);
extern void getCurrentSTAN(char *);
extern void setDatawireID(char*);
extern void getDCCMerchantId(char *);
extern void getDCCAcquirer(char *);
extern void getRefNumForCardRate(char*);
extern RCHI_BOOL isVSPEnabledInDevice();
extern RCHI_BOOL isEmbossedCardNumCalcRequired();
extern RCHI_BOOL isForcePLDebitFlagSet();
extern RCHI_BOOL isDCCEnabledInDevice();
extern int getNumUniqueHosts();
extern int writeRCParamsToFile(char*);
extern char* getFGNCardBinFileName();
extern char* getCurrencyDataFileName();
extern void getRCVersion(char*);
extern int  getTotalGrpCount();
#endif


