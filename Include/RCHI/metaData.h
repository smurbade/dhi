#ifndef __METADATA_H
#define __METADATA_H

#include "rchiCommon.h"
#include "DHI_App/tranDef.h"

/* Structure definitions for storing/getting the values present in the messages
 * sent by external agents */
//
//typedef enum
//{
//	SINGLETON = 1,
//	STRING,
//	AMOUNT,
//	BOOLEAN,
//	TIME,
//	DATE,
//	NUMERICS,
//	LIST,
//	MULTILIST,
//	VARLIST,
//	RECLIST,
//	ATTR_LKDLST,
//	MASK_NUM_STR,
//	NULL_ADD,
//	KEYLIST
//}
//VAL_TYPE_ENUM;




typedef enum
{
	INTRN_SEQ_NUM,
	PROCESSOR_ID,
	BATCH_SEQ_NUM,
	TRANS_SEQ_NUM,
	INVOICE,
	COMMAND,
	ACCT_NUM,
	EXP_MONTH,
	EXP_YEAR,
	CARDHOLDER,
	TRANS_AMOUNT,
	REFERENCE,
	TRANS_DATE,
	TRANS_TIME,
	ORIG_SEQ_NUM,
	STATUS_CODE,
	TROUTD,
	CTROUTD,
	PAYMENT_TYPE,
	PAYMENT_MEDIA,
	RESULT_CODE,
	AUTH_CODE,
	TRACE_CODE,
	AVS_CODE,
	CVV2_CODE,
	USERID,
	CASHBACK_AMNT,
	TIP_AMOUNT,
	RESPONSE_REFERENCE,
	R_AUTH_CODE,
	COL_1,
	COL_2,
	COL_3,
	COL_4,
	COL_5,
	COL_6,
	COL_7,
	COL_8,
	COL_9,
	COL_10
}
GENRESP_FLD_ENUM;

typedef enum
{
	STL_BATCH_SEQ_NUM,
	ERR_TRANS_IN_BATCH,
	SETTLE_DATE,
	SETTLE_CODE,
	CRDT_SALE_AMT,
	CRDT_SALE_CNT,
	CRDT_CRDT_AMT,
	CRDT_CRDT_CNT,
	CRDT_VOID_AMT,
	CRDT_VOID_CNT
}
STLRESP_FLD_ENUM;

typedef struct
{
	char *			key;
	int				iIndex;
	int				iSource;
	char			cPaymentType;
	void *			value;
	RCHI_BOOL		isMand;
	VAL_TYPE_ENUM	valType;
	void *			valMetaInfo;
	char*			varlistSource;
}
RCKEYVAL_STYPE, * RCKEYVAL_PTYPE;

#define RCKEYVAL_SIZE	sizeof(RCKEYVAL_STYPE)

typedef struct
{
	int					iMandCnt;
	int					iTotCnt;
	RCKEYVAL_PTYPE		keyValList;
}
METADATA_STYPE, * METADATA_PTYPE;

#define METADATA_SIZE	sizeof(METADATA_STYPE)
typedef METADATA_STYPE	META_STYPE;
typedef METADATA_PTYPE	META_PTYPE;

typedef struct
{
	int			iListVal;
	char *		szListVal;
}
LIST_INFO_STYPE, * LIST_INFO_PTYPE;

typedef struct
{
	int				nodeType;
	char *			szNodeStr;
	int				iElemCnt;
	RCKEYVAL_PTYPE	keyList;
}
VARLST_INFO_STYPE, * VARLST_INFO_PTYPE;

typedef struct
{
	char *			szNodeStr;
	int				iElemCnt;
	RCKEYVAL_PTYPE	keyList;
}
PARAM_INFO_STYPE, * PARAM_INFO_PTYPE;

typedef struct
{
	int				nodeType;
	char *			szNodeStr;
	int				iElemCnt;
	RCKEYVAL_PTYPE	keyList;
}
ATTRLST_INFO_STYPE, * ATTRLST_INFO_PTYPE;

typedef struct __stNode
{
	int					type;
	int					elemCnt;
	char *				szVal;
	RCKEYVAL_PTYPE		elemList;
	struct __stNode *	next;
}
VAL_NODE_STYPE, * VAL_NODE_PTYPE;

typedef struct
{
	int					valCnt;
	VAL_NODE_PTYPE		start;
	VAL_NODE_PTYPE		end;
}
VAL_LST_STYPE, * VAL_LST_PTYPE;

#define VAL_LST_SIZE sizeof(VAL_LST_STYPE)

typedef struct __stVal
{
	int					iLstVal;
	struct __stVal *	next;
}
MLVAL_STYPE, * MLVAL_PTYPE;
#define MLVAL_SIZE sizeof(MLVAL_STYPE)

typedef struct
{
	int		minLen;
	int		maxLen;
}
STR_DEF_STYPE, * STR_DEF_PTYPE;

typedef struct
{
	int		minLen;
	int		maxLen;
	int		prefixLen; /* chars before masked chars */
	int		suffixLen; /* chars after masked chars */
}
MASK_STR_DEF_STYPE, * MASK_STR_DEF_PTYPE;

typedef struct
{
	RCHI_BOOL	    isNegAmtAllwd;
	int				minDecLen;
	int				maxDecLen;
	int				minManLen;
	int				maxManLen;
}
AMT_DEF_STYPE, * AMT_DEF_PTYPE;

typedef struct
{
	METADATA_STYPE	stMetaData;
	char *			szRespData;
}
PASSTHRU_DATA_STYPE, * PASSTHRU_DATA_PTYPE;

/* EMV Admin nodes with Public Key Parameters
 * which are used while creating the SSI response
 */
typedef struct __gstEMVAdminNode
{
	char*					pszPkCkSum;
	char*					pszPkExpLen;
	char*					pszPkExpDate;
	char*					pszRID;
	char*					pszPkMod;
	char*					pszPkCkSumLen;
	char*					pszPkModLen;
	char*					pszPkExp;
	char*					pszPkIndex;
	struct __gstEMVAdminNode* next;
}
VAL_EMVADMINNODE_STYPE, * VAL_EMVADMINNODE_PTYPE;

#define EMVADMINNODE_SIZE sizeof(VAL_EMVADMINNODE_STYPE)

typedef struct __gstEMVAdminHeadNode
{
	VAL_EMVADMINNODE_PTYPE pstKeysLst;
	char					*pszEMVKeyLst;
	int 					iMaxPublicKeys;
	char 					szDownOffset[6];
	char 					szFileBlk[5];
	char					szDwnldStat[2];
	int						iKeyUpdAvl;
}
VAL_EMVADMINHEADNODE_STYPE, * VAL_EMVADMINHEADNODE_PTYPE;

typedef struct __stDCCFields
{
	char szCurrCode[4+1];
	char szCountryName[50 + 1];
	char szCurrName[30+1];
	char szAlphaCurrCode[10+1];
	char szCountryAlpha2Code[2+1];
	char szCurrSym[3+1];
	char szMinorCurrUnit[5+1];
}
FEXCORESPDTLS_STYPE, * FEXCORESPDTLS_PTYPE;

typedef struct __gstnode
{
	 int 					iHeight;
	 int 					iKey;
	 struct __gstnode 		*left;
	 struct __gstnode 		*right;
	FEXCORESPDTLS_PTYPE		pszValue;
}
AVL_STYPE, *AVL_PTYPE;

typedef struct __stHostTtlNode
{
	char*					pszCardTag;
	char*					pszTransCnt;
	char*					pszTansAmt;
	struct __stHostTtlNode* next;
}
VAL_HOSTTOTALNODE_STYPE, * VAL_HOSTTOTALNODE_PTYPE;
#define HOSTTOTALNODE_SIZE sizeof(VAL_HOSTTOTALNODE_STYPE)

typedef struct __stHostTtlHeadNode
{
	VAL_HOSTTOTALNODE_PTYPE pstHead;
	int iMaxHostTotalGroup;
	char szNetSettlemntAmt[256];
}
VAL_HOSTTOTALHEADNODE_STYPE, * VAL_HOSTTOTALHEADNODE_PTYPE;

/* ------------ Extern API's ------------- */

/* GENERIC API's */
extern int	freeMetaData(METADATA_PTYPE);
extern int	freeMetaDataEx(METADATA_PTYPE);

#endif
