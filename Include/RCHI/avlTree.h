/*
 * avlTree.h
 *
 *  Created on: 13-May-2016
 *      Author: T_AkshayaM1
 */

#ifndef AVLTREE_H_
#define AVLTREE_H_

static int getBalance(AVL_PTYPE);
static int calculateHeight(AVL_PTYPE);
static int maxOfTwoNodes(int  , int );
AVL_PTYPE rightRotation(AVL_PTYPE);
AVL_PTYPE leftRotation(AVL_PTYPE);

AVL_PTYPE searchElement(AVL_PTYPE, int);
AVL_PTYPE insertElement(AVL_PTYPE, int, FEXCORESPDTLS_PTYPE);
AVL_PTYPE createNewNode(int, FEXCORESPDTLS_PTYPE);


#endif /* AVLTREE_H_ */
