/*
 * hash.h
 *
 *  Created on: Dec 5, 2014
 *      Author: BLR_SCA_TEAM
 */

#ifndef HASH_H_
#define HASH_H_

#define HASHSIZE 101


typedef struct __stHash /* table entry: */
{
	struct __stHash * next;     		/* Next entry in chain */
    char 		    *pszTagName;       	/* Defined Tag Name */
    int  			iTagIndexVal;    	/* Index Value of the Tag */
}
HASHLST_STYPE, * HASHLST_PTYPE;

extern HASHLST_PTYPE addEntryToRCHashTable(char *, int );
extern HASHLST_PTYPE lookupRCHashTable(char *);

#endif /* HASH_H_ */
