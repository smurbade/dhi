/*
 * rchiMsgQueue.h
 *
 *  Created on: 05-Jan-2015
 *      Author: KranthiK1
 */

#ifndef RCHIMSGQUEUE_H_
#define RCHIMSGQUEUE_H_


extern int 		displayMsgQueueInfo(unsigned);
extern void 	flushMsgQueue(unsigned);
extern unsigned	createMsgQueue(key_t);

#endif /* RCHIMSGQUEUE_H_ */
