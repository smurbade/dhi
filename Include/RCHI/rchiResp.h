/*
 * rchiResp.h
 *
 *  Created on: 16-Dec-2014
 *      Author: BLR_SCA_TEAM
 */

#ifndef RCHIRESP_H_
#define RCHIRESP_H_

typedef struct
{
	char *			key;
	int			 	iRCIndex;
	int			 	iDHIIndex;
}
RCHI_RESP_KEYVAL_STYPE, * RCHI_RESP_KEYVAL_PTYPE;

#define RCHI_RESP_KEYVAL_SIZE	sizeof(RCHI_RESP_KEYVAL_STYPE)

extern int 	getNodeValFromDWResp(char*, char**, char**, int, char*);
extern int 	getNodeValFromDWRegResp(char*, char**, char**, int, char*);
extern int 	parseRCResponse(char*, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE);
extern int 	parseRCResponseForHostTotals(char*, TRANDTLS_PTYPE, RCTRANDTLS_PTYPE, VAL_HOSTTOTALHEADNODE_PTYPE);
extern int  parseFEXCOResponse(char* , TRANDTLS_PTYPE , RCTRANDTLS_PTYPE);
extern int 	getTotalsRequestDate(char *, char *);
extern void fillErrorDtls(int, TRANDTLS_PTYPE, char*);

#endif /* RCHIRESP_H_ */
