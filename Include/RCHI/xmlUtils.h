#ifndef __XML_UTILS_H
#define __XML_UTILS_H

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "metaData.h"

typedef struct __genKeyVal
{
	char *					key;
	char *					value;
	struct __genKeyVal *	next;
}
GENKEYVAL_STYPE, * GENKEYVAL_PTYPE;
#define GENKEYVAL_SIZE sizeof(GENKEYVAL_STYPE)

/* Extern functions declarations */
extern int parseXMLMsg(xmlDoc *, METADATA_PTYPE);
extern int parseXMLMsg_1(char *, char *, char *, METADATA_PTYPE);
extern int parseXMLFile(char *, char *, char *, METADATA_PTYPE);
extern int parseXMLFileForConfigData(char *, GENKEYVAL_PTYPE *);
extern int parseXMLFileForConfigIniData(char *, GENKEYVAL_PTYPE *);
extern int getXMLRootName(char * , char * );

extern int buildXMLMsgFromMetaData(char **, int *, char *, METADATA_PTYPE);
extern int buildXMLAdminReqMsgFromMetaData(char **, int *, char *, METADATA_PTYPE);
extern xmlNode * findNodeInXML(char *, xmlNode *);

extern int buildReqClientIdForDWReq(xmlNodePtr, int);
extern int buildXMLReqForDW(char**, int*, char*, int, int);
extern int buildXMLReqForFEXCO(char** , int*, RCTRANDTLS_PTYPE);
extern int buildReqCardRateForFEXCORequest(xmlNodePtr, RCTRANDTLS_PTYPE);

#endif
