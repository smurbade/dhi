/*
 * rcCommon.h
 *
 *  Created on: Dec 8, 2014
 *      Author: BLR_SCA
 */

#ifndef RCCOMMON_H_
#define RCCOMMON_H_


typedef unsigned long long	ullong;

#define	SIG_KA_TIME SIGRTMIN+2	//To reset the keep alive interval timer as get the CURL Handle or whenever we receive SetTime command

/* ERROR CONSTANTS */

#define ERR_INV_XML_MSG			-100
#define ERR_UNSUPP_CMD			-101
#define ERR_SYSTEM				-102
#define ERR_RESP_TO				-103
#define ERR_CONN_TO				-104
#define ERR_CONN_FAIL			-105
#define ERR_CFG_PARAMS			-106
#define ERR_INV_COMBI			-107
#define ERR_FLD_REQD			-108
#define ERR_INV_FLD				-109
#define ERR_NO_FLD				-110
#define ERR_INV_FLD_VAL			-111
#define ERR_INV_RESP			-112
#define ERR_HOST_ERROR			-113
#define ERR_HOST_TRAN_REVERSED	-114
#define ERR_HOST_TRAN_TIMEOUT 	-115
#define ERR_HOST_NOT_AVAILABLE 	-116
#define ERR_INV_CTROUTD			-117
#define ERR_TOKEN_NOT_FOUND		-118
#define	ERR_FILE_NOT_AVAILABLE	-119
#define ERR_NO_REQUEST_DATA		-120

#define ERR_DW_HOST_BUSY		 200
#define ERR_DW_HOST_UNAVAILABLE	 201
#define ERR_DW_CONN_ERROR		 202
#define ERR_DW_HOST_DROP		 203
#define ERR_DW_HOST_COMM_ERROR	 204
#define ERR_DW_NO_RESPONSE		 205
#define ERR_DW_HOST_SEND_ERROR	 206
#define ERR_DW_TIME_OUT			 405
#define ERR_DW_NETWORK_ERROR1	 505
#define ERR_DW_NETWORK_ERROR2	 8

#define RCHI_LIB_VERSION	"V_1.00.07"
#define RCHI_BUILD_NUMBER   "1"

#ifdef DEBUG
	extern char chRCHIDebug;
	extern char chRCHIMemDebug;
	#include "tcpipdbg.h"
	//#define debug_sprintf(...) sprintf(__VA_ARGS__)
	#define debug_sprintf(...) \
			{ \
		if (chRCHIDebug != NO_DEBUG) \
		{ \
			sprintf(__VA_ARGS__) ; \
		} \
			}
	#define debug_mem_sprintf(...) \
		{ \
	if (chRCHIMemDebug != NO_DEBUG) \
	{ \
		sprintf(__VA_ARGS__) ; \
	} \
		}
	extern void setupRCHIDebug(int *);
	extern void setupRCHIMemDebug(int *);
	extern void closeupDebug(void);
	extern void closeupMemDebug(void);
	extern void RCHI_LIB_TRACE(char *pszMessage);
	extern void RCHI_LIB_MEM_TRACE(char *pszMessage);
	extern void RCHI_LIB_TRACE_EX(char *pszMessage, int iLen);
	extern int setRCHIDebugParamsToEnv(int * );
#else
	#define debug_sprintf(...) ;
	#define debug_mem_sprintf(...) ;
	#define RCHI_LIB_TRACE(s) ;
	#define RCHI_LIB_MEM_TRACE(s) ;
	#define RCHI_LIB_TRACE_EX(s, i) ;
#endif

#define SUCCESS 		0
#define FAILURE		-1403

#define DEVDEBUGMSG 		"DEVDEBUGMSG"
#define RCHI_DEBUG			"RCHI_DEBUG"
#define RCHI_MEM_DEBUG		"RCHI_MEM_DEBUG"

#define TRAN_RECORDS 					"./flash/tranRecords.txt"
#define RC_PARAMS_FILE					"./flash/rcparams.txt"
#define CARDRATE_REFNUM_FILE			"./flash/cardraterefnum.txt"
#define RCHI_PASSTHROUGH_FILE_PATH1 	"./flash/rchiPassThrgFields.INI"
#define RCHI_PASSTHROUGH_FILE_PATH2 	"./flash/rchiPassThrgFields.ini"

#define MAX_DEBUG_MSG_SIZE	15+4096+2+1


typedef enum
{
	NO_ENUM = -1,
	RCHI_FALSE,
	RCHI_TRUE
}
RCHI_BOOL;

extern void getHILibVersion(char *, char *);
extern int updateConfigParams();
extern int updateCardRateRefNumToFile();

extern int 	checkEMVKeysStat();
extern int 	cleanCURLHandle(RCHI_BOOL);
/* Application Logging #defines */

// App Entry for Transactional App Logging

#define	APP_NAME "RCHI"

// Entry Tyrp for Transactional App Logging
#define ENTRYTYPE_INFO 		"INFO"
#define ENTRYTYPE_ERROR 	"ERROR"
#define ENTRYTYPE_FAILURE 	"FAILURE"
#define ENTRYTYPE_WARNING 	"WARNING"
#define ENTRYTYPE_SUCCESS 	"SUCCESS"


// App Entry Id for Transactional App Logging
#define ENTRYID_START_UP			"START_UP"
#define	ENTRYID_START				"START"
#define	ENTRYID_COMMAND_END			"END"
#define ENTRYID_RECEIVE				"RECEIVE"
#define ENTRYID_PROCESSING			"PROCESSING"
#define ENTRYID_PROCESSED			"PROCESSED"
#define ENTRYID_SENT				"SENT"
#define ENTRYID_PROCESSING			"PROCESSING"
#define ENTRYID_RC_REQUEST			"RC_REQUEST"
#define ENTRYID_RC_RESPONSE			"RC_RESPONSE"
#define ENTRYID_RESPONDED			"RESPONDED"
#define	ENTRYID_FEXCO_REQUEST		"FEXCO_REQUEST"
#define ENTRYID_FEXCO_RESPONSE		"FEXCO_RESPONSE"

#define EMV_KEY_FL_DTLS		"./flash/rc_emvdtls.dat"

typedef enum
{
	TRANSACTION,
	REGISTRATION,
	ACTIVATION
}
TRAN_TYPE_ENUM;

typedef enum
{
	RC_AUTH = 0,
	RC_SALE,
	RC_COMPLETION,
	RC_VOICE_COMPLETION,
	RC_REFUND,
	RC_VOID,
	RC_ACTIVATE,
	RC_BALANCE_LOCK,
	RC_REDEMPTION_UNLOCK,
	RC_REDEMPTION,
	RC_RELOAD,
	RC_BALANCE,
	RC_CASH_OUT,
	RC_HOST_TOTALS,
	RC_TOKEN_QUERY,
	RC_EMV_DOWNLOAD
}
COMMAND_ENUM;

typedef enum
{
	RC_CLOSE_BATCH = 0,
	RC_SITE_CUR_DAY
}
HOSTOTAL_ENUM;

typedef struct
{
	char elementValue[256];
}
RCTRANDTLS_STYPE, * RCTRANDTLS_PTYPE;

extern void *rchiMalloc(unsigned int, int, char* );
extern char *rchistrdup(char*, int , char *);
extern char *rchistrndup(char *, unsigned int , int, char * );
extern void *rchiReAlloc(void *, unsigned int, int, char*);
extern void  rchiFree(void **, int, char*);

#endif /* RCCOMMON_H_ */
