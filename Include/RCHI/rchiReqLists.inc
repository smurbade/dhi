/* 
 * --------------------------------------------------------
 * -------------- RC REQUEST DATA ------------------------
 * --------------------------------------------------------
 */

/* Common Group List */
static RCKEYVAL_STYPE	stCommonGrpLst[] =
{
	{ "PymtType"		, RC_PAYMENT_TYPE		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "ReversalInd"		, RC_REVERSL_RSN_CODE	, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },	/* ONLY FOR REVERSALS */
	{ "TxnType"			, RC_TRAN_TYPE			, RC	, 0, NULL, RCHI_TRUE,	SINGLETON, NULL, NULL },
	{ "LocalDateTime"	, RC_LOCAL_DATE_TIME	, RC	, 0, NULL, RCHI_TRUE, 	SINGLETON, NULL, NULL },
	{ "TrnmsnDateTime"	, RC_TRANSM_DATE_TIME	, RC	, 0, NULL, RCHI_TRUE, 	SINGLETON, NULL, NULL },
	{ "STAN"      		, RC_STAN				, RC	, 0, NULL, RCHI_TRUE, 	SINGLETON, NULL, NULL },
	{ "RefNum" 			, RC_REFERENCE_NUMBER	, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "OrderNum"   	 	, RC_ORDER_NUMBER		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "TPPID"  	 		, RC_TPP_ID				, RC	, 0, NULL, RCHI_TRUE, 	SINGLETON, NULL, NULL },
	{ "TermID"  	 	, RC_TERMINAL_ID		, RC 	, 0, NULL, RCHI_TRUE, 	SINGLETON, NULL, NULL },
	{ "MerchID"  	 	, RC_MERCHANT_ID		, RC	, 0, NULL, RCHI_TRUE, 	SINGLETON, NULL, NULL },
	{ "AltMerchID"  	, RC_ALT_MERCH_ID		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL }, /* ONLY FOR PREPAID REQUESTS */
	{ "MerchCatCode"   	, RC_MERCH_CAT_CODE		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "POSEntryMode"   	, RC_POS_ENTRY_MODE		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "POSCondCode"  	, RC_POS_COND_CODE		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "TermCatCode"  	, RC_TERM_CAT_CODE		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "TermEntryCapablt", RC_TERM_ENTRY_CAPABLT	, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "TxnAmt"  		, RC_TRAN_AMOUNT		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "TxnCrncy"  		, RC_TRANS_CRNCY		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "TermLocInd" 		, RC_TERM_LOC_IND		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "CardCaptCap"  	, RC_CARD_CAPT_CAPABLT	, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "ProgramID"	  	, RC_PROGRAM_ID			, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL }, /* ONLY FOR PREPAID REQUESTS */
	{ "GroupID"  		, RC_GROUP_ID			, RC	, 0, NULL, RCHI_TRUE, 	SINGLETON, NULL, NULL },
	{ "POSID"		  	, RC_POS_ID				, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "PLPOSDebitFlg"  	, ePINLESSDEBIT 		, DHI	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE commonGroupItems[] =
{
	{ 0 , NULL, sizeof(stCommonGrpLst) / RCKEYVAL_SIZE, stCommonGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Card Group List */ 
static RCKEYVAL_STYPE	stCardGrpLst[] =
{
	{ "AcctNum"				, RC_ACCT_NUM		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "CardExpiryDate"		, RC_CARD_EXP_DATE	, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "Track1Data"			, RC_TRACK1_DATA	, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "Track2Data"			, RC_TRACK2_DATA	, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "CardType"			, RC_CARD_TYPE		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "CCVInd"				, RC_CCV_IND		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "CCVData"				, RC_CCV			, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE cardGroupItems[] =
{
	{ 0 , NULL, sizeof(stCardGrpLst) / RCKEYVAL_SIZE, stCardGrpLst},
	{ -1, NULL    , 0 , NULL }
};


/* Additional Amount Group Details List */
static RCKEYVAL_STYPE	stAddtlTaxAmtGrpLst[] = 
{
	{ "AddAmt"						, RC_ADDTL_AMT_TAXAMT		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, /* RCHI_TRUE FOR TIMEOUT REVERSAL IN CREDIT AND DEBIT REQUESTS. RCHI_TRUE FOR VOID REVERSALS IN CREDIT REQUEST */
	{ "AddAmtCrncy"					, RC_ADDTL_AMT_CRNCY_TAXAMT	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, /* RCHI_TRUE FOR TIMEOUT REVERSAL IN CREDIT AND DEBIT REQUESTS. RCHI_TRUE FOR VOID REVERSALS IN CREDIT REQUEST */
	{ "AddAmtType"					, RC_ADDTL_AMT_TYPE_TAXAMT	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "HoldInfo"  					, RC_HOLD_INFO				, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, /* ONLY FOR PREPAID TRANSACTION */
	{ "PartAuthrztnApprvlCapablt"	, RC_PARTIAL_AUTH			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE addtlTaxAmtGroupItems[] =
{
	{ 0 , NULL, sizeof(stAddtlTaxAmtGrpLst) / RCKEYVAL_SIZE, stAddtlTaxAmtGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Additional Amount Group Details List */
static RCKEYVAL_STYPE	stAddtlCashBakAmtGrpLst[] = 
{
	{ "AddAmt"						, RC_ADDTL_AMT_CASHBK		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, /* RCHI_TRUE FOR TIMEOUT REVERSAL IN CREDIT AND DEBIT REQUESTS. RCHI_TRUE FOR VOID REVERSALS IN CREDIT REQUEST */
	{ "AddAmtCrncy"					, RC_ADDTL_AMT_CRNCY_CASHBK	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, /* RCHI_TRUE FOR TIMEOUT REVERSAL IN CREDIT AND DEBIT REQUESTS. RCHI_TRUE FOR VOID REVERSALS IN CREDIT REQUEST */
	{ "AddAmtType"					, RC_ADDTL_AMT_TYPE_CASHBK	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE addtlCashBakAmtGroupItems[] =
{
	{ 0 , NULL, sizeof(stAddtlCashBakAmtGrpLst) / RCKEYVAL_SIZE, stAddtlCashBakAmtGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Additional Amount Group Details List */
static RCKEYVAL_STYPE	stAddtlFAAmtGrpLst[] = 
{	
	{ "AddAmt"						, RC_ADDTL_FA_AMT			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, /* RCHI_TRUE FOR TIMEOUT REVERSAL IN CREDIT AND DEBIT REQUESTS. RCHI_TRUE FOR VOID REVERSALS IN CREDIT REQUEST */
	{ "AddAmtCrncy"					, RC_ADDTL_FA_AMT_CRNCY		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, /* RCHI_TRUE FOR TIMEOUT REVERSAL IN CREDIT AND DEBIT REQUESTS. RCHI_TRUE FOR VOID REVERSALS IN CREDIT REQUEST */
	{ "AddAmtType"					, RC_ADDTL_FA_AMT_TYPE		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },	
};

static VARLST_INFO_STYPE addtlFAAmtGroupItems[] =
{
	{ 0 , NULL, sizeof(stAddtlFAAmtGrpLst) / RCKEYVAL_SIZE, stAddtlFAAmtGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Additional Amount Group Details List */
static RCKEYVAL_STYPE	stAddtlTAAmtGrpLst[] = 
{	
	{ "AddAmt"						, RC_ADDTL_TA_AMT			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, /* RCHI_TRUE FOR TIMEOUT REVERSAL IN CREDIT AND DEBIT REQUESTS. RCHI_TRUE FOR VOID REVERSALS IN CREDIT REQUEST */
	{ "AddAmtCrncy"					, RC_ADDTL_TA_AMT_CRNCY		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, /* RCHI_TRUE FOR TIMEOUT REVERSAL IN CREDIT AND DEBIT REQUESTS. RCHI_TRUE FOR VOID REVERSALS IN CREDIT REQUEST */
	{ "AddAmtType"					, RC_ADDTL_TA_AMT_TYPE		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
};

static VARLST_INFO_STYPE addtlTAAmtGroupItems[] =
{
	{ 0 , NULL, sizeof(stAddtlTAAmtGrpLst) / RCKEYVAL_SIZE, stAddtlTAAmtGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Additional Amount Group for Healthcare Details List (FSA) */
static RCKEYVAL_STYPE	stAddtlHCAmtGrpLst[] = 
{	
	{ "AddAmt"						, RC_ADDTL_AMT_HLTCARE			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, 
	{ "AddAmtCrncy"					, RC_ADDTL_AMT_CRNCY_HLTCARE	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "AddAmtType"					, RC_ADDTL_AMT_TYPE_HLTCARE		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }	
};

static VARLST_INFO_STYPE addtlHCAmtGroupItems[] =
{
	{ 0 , NULL, sizeof(stAddtlHCAmtGrpLst) / RCKEYVAL_SIZE, stAddtlHCAmtGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Additional Amount Group for Prescription Details List (FSA) */
static RCKEYVAL_STYPE	stAddtlPRAmtGrpLst[] = 
{	
	{ "AddAmt"						, RC_ADDTL_AMT_PRESCRIPION			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, 
	{ "AddAmtCrncy"					, RC_ADDTL_AMT_CRNCY_PRESCRIPION	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, 
	{ "AddAmtType"					, RC_ADDTL_AMT_TYPE_PRESCRIPION		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }	
};

static VARLST_INFO_STYPE addtlPRAmtGroupItems[] =
{
	{ 0 , NULL, sizeof(stAddtlPRAmtGrpLst) / RCKEYVAL_SIZE, stAddtlPRAmtGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Additional Amount Group for Vision Details List (FSA) */
static RCKEYVAL_STYPE	stAddtlVIAmtGrpLst[] = 
{	
	{ "AddAmt"						, RC_ADDTL_AMT_VISION			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, 
	{ "AddAmtCrncy"					, RC_ADDTL_AMT_CRNCY_VISION		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, 
	{ "AddAmtType"					, RC_ADDTL_AMT_TYPE_VISION		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }	
};

static VARLST_INFO_STYPE addtlVIAmtGroupItems[] =
{
	{ 0 , NULL, sizeof(stAddtlVIAmtGrpLst) / RCKEYVAL_SIZE, stAddtlVIAmtGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Additional Amount Group for Clinic Details List (FSA) */
static RCKEYVAL_STYPE	stAddtlCLAmtGrpLst[] = 
{	
	{ "AddAmt"						, RC_ADDTL_AMT_CLINIC			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, 
	{ "AddAmtCrncy"					, RC_ADDTL_AMT_CRNCY_CLINIC		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, 
	{ "AddAmtType"					, RC_ADDTL_AMT_TYPE_CLINIC		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }	
};

static VARLST_INFO_STYPE addtlCLAmtGroupItems[] =
{
	{ 0 , NULL, sizeof(stAddtlCLAmtGrpLst) / RCKEYVAL_SIZE, stAddtlCLAmtGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Additional Amount Group for Prescription Details List (FSA) */
static RCKEYVAL_STYPE	stAddtlDEAmtGrpLst[] = 
{	
	{ "AddAmt"						, RC_ADDTL_AMT_DENTAL			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }, 
	{ "AddAmtCrncy"					, RC_ADDTL_AMT_CRNCY_DENTAL		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "AddAmtType"					, RC_ADDTL_AMT_TYPE_DENTAL		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }	
};

static VARLST_INFO_STYPE addtlDEAmtGroupItems[] =
{
	{ 0 , NULL, sizeof(stAddtlDEAmtGrpLst) / RCKEYVAL_SIZE, stAddtlDEAmtGrpLst},
	{ -1, NULL    , 0 , NULL }
};
/* Visa Group List */ 
static RCKEYVAL_STYPE	stVisaGrpLst[] =
{
	{ "ACI"					, RC_AUTH_CHARACTERISTICS_IND, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "MrktSpecificDataInd"	, RC_VISA_MRKT_SPEC_IND		 , RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "CardLevelResult"		, RC_CARD_LEVEL_RESULT		 , RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "TransID"				, RC_TRAN_ID				 , RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};	
	
static VARLST_INFO_STYPE visGroupItems[] =
{
	{ 0 , NULL, sizeof(stVisaGrpLst) / RCKEYVAL_SIZE, stVisaGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Master Card Group List */ 
static RCKEYVAL_STYPE	stMasterGrpLst[] =
{
	{ "BanknetData"		, RC_BANK_NET_DATA		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "MCMSDI"		    , RC_MC_MRKT_SPEC_IND	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "CCVErrorCode"	, RC_CCV_ERROR_CODE		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "POSEntryModeChg"	, RC_POSENTRYMODE_CHG	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DevTypeInd"		, RC_DEV_TYPE_IND		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};	
	
static VARLST_INFO_STYPE mcGroupItems[] =
{
	{ 0 , NULL, sizeof(stMasterGrpLst) / RCKEYVAL_SIZE, stMasterGrpLst},
	{ -1, NULL    , 0 , NULL }
};


/* Discover Card Group List */ 
static RCKEYVAL_STYPE	stDiscoverGrpLst[] =
{
	{ "DiscProcCode"		, RC_DISC_PROC_CODE	 , RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DiscPOSEntry"		, RC_DISC_POS_ENTRY  , RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DiscRespCode"		, RC_DISC_RESP_CODE  , RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DiscPOSData"			, RC_DISC_POS_DATA	 , RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DiscTransQualifier"	, RC_DISC_TRAN_QUALIF, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DiscNRID"			, RC_DISC_NRID 		 , RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};	
	
static VARLST_INFO_STYPE discGroupItems[] =
{
	{ 0 , NULL, sizeof(stDiscoverGrpLst) / RCKEYVAL_SIZE, stDiscoverGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Amex Card Group List */ 	
static RCKEYVAL_STYPE	stAmexGrpLst[] =
{
	{ "AmExPOSData"	, RC_AMEX_POS_DATA, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "AmExTranID"	, RC_AMEX_TRAN_ID , RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
};	
	
static VARLST_INFO_STYPE amexGroupItems[] =
{
	{ 0 , NULL, sizeof(stAmexGrpLst) / RCKEYVAL_SIZE, stAmexGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Purchase Card Lvl2 Group List */
static RCKEYVAL_STYPE	stPurchCardLvl2GrpLst[] = 
{
	{ "TaxAmt"				, RC_TAX_AMOUNT			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "TaxInd"				, RC_TAX_IND			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "PurchIdfr"			, RC_PURCHASE_IDFR 		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "PCOrderNum"			, RC_PC_ORDER_NUM		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DiscntAmt"			, RC_DISC_AMOUNT		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "FrghtAmt"			, RC_FREIGHT_AMOUNT		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DutyAmt"				, RC_DUTY_AMOUNT		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DestPostalCode"		, RC_DEST_POSTAL_CODE	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "ShipFromPostalCode"	, RC_SHIP_POSTAL_CODE	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "DestCtryCode"		, RC_DEST_COUNTRY_CODE	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "MerchTaxID"			, RC_MERCH_TAX_ID 		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "ProdDesc"			, RC_PROD_DESC			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "PC3Add"				, RC_PC3_ADD			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE purchCardLvl2GroupItems[] =
{
	{ 0 , NULL, sizeof(stPurchCardLvl2GrpLst) / RCKEYVAL_SIZE, stPurchCardLvl2GrpLst},
	{ -1, NULL    , 0 , NULL }
};

/*PRADEEP: 17-11-2016 : Changed All Feilds in stOrigAuthGrpLst fields as optional because we are not cheking for the mandatory flag while filling the data for this Group*/
/* Original Authorization Group List */
static RCKEYVAL_STYPE	stOrigAuthGrpLst[] =
{
	{ "OrigAuthID"			, RC_ORIG_AUTH_ID			, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },	/* RCHI_TRUE FOR ALL REQUESTS EXCEPT TIMEOUT AND VOID REVERSALS */	
	{ "OrigLocalDateTime"  	, RC_ORIG_LOCAL_DATE_TIME	, RC	, 0, NULL, RCHI_FALSE, 	SINGLETON, NULL, NULL },
	{ "OrigTranDateTime"    , RC_ORIG_TRANSM_DATE_TIME	, RC	, 0, NULL, RCHI_FALSE, 	SINGLETON, NULL, NULL },
	{ "OrigSTAN"   			, RC_ORIG_STAN				, RC	, 0, NULL, RCHI_FALSE, 	SINGLETON, NULL, NULL },
	{ "OrigRespCode"    	, RC_ORIG_RESP_CODE			, RC	, 0, NULL, RCHI_FALSE, 	SINGLETON, NULL, NULL }		/* RCHI_TRUE FOR COMPLETION AND VOID REQUESTS */
};

static VARLST_INFO_STYPE origAuthGroupItems[] =
{
	{ 0 , NULL, sizeof(stOrigAuthGrpLst) / RCKEYVAL_SIZE, stOrigAuthGrpLst},
	{ -1, NULL    , 0 , NULL }
};


/* FOLLOWING THREE GROUPS ARE ONLY FOR DEBIT REQUESTS */
/* PIN Group List */
/* In Case of EMV Transactions with Offline/No Pin Verification- PIN Block will not be recieved in SSI request,
 * but RC Host has a mandatory requirement for PinGrp for Debit Transactions, hence these fields have been moved to
 * RCHI to populate it as required.
 */
static RCKEYVAL_STYPE	stPinGrpLst[] =
{
	{ "PINData"				, RC_PIN_BLOCK	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "KeySerialNumData"	, RC_KSN 		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE pinGroupItems[] =
{
	{ 0 , NULL, sizeof(stPinGrpLst) / RCKEYVAL_SIZE, stPinGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* TransArmor Group */
static RCKEYVAL_STYPE stTransArmorGrpLst[] =
{
	{ "SctyLvl"		, RC_SECURITY_LVL	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "EncrptType"	, RC_ENCRPT_TYPE	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "EncrptTrgt"	, RC_ENCRPT_TARGET	, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "KeyID"		, RC_KEY_ID			, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "EncrptBlock"	, RC_ECRYPT_BLK		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "TknType"		, RC_TOKEN_TYPE		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "Tkn"			, RC_CARD_TOKEN		, RC	, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE transArmorGrpItems[] =
{
	{ 0 , NULL, sizeof(stTransArmorGrpLst) / RCKEYVAL_SIZE, stTransArmorGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* FOLLOWING GROUPS IS ONLY FOR HOST TOTALS REQUESTS */
/* Host Totals Group List */
static RCKEYVAL_STYPE	stHostTotalsGrpLst[] =
{
	{ "TotReqDate"	, RC_TOT_REQ_DATE	, RC, 0, NULL, RCHI_TRUE, SINGLETON, NULL, NULL },
	{ "Password"	, RC_PASSWORD		, RC, 0, NULL, RCHI_TRUE, SINGLETON, NULL, NULL },
	{ "NetSettleAmt", RC_NET_STLMNT_AMT	, RC, 0, NULL, RCHI_TRUE, SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE hostTotalsGrpItems[] =
{
	{ 0 , NULL, sizeof(stHostTotalsGrpLst) / RCKEYVAL_SIZE, stHostTotalsGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Host Totals Details Group List  */
static RCKEYVAL_STYPE	stHostTotalsDtlsGrpLst[] =
{
	{ "CardTag"     , RC_CARD_TAG			, RC , 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "TxnCt"		, RC_TRANS_COUNT		, RC , 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "CardTxnAmt"	, RC_CARD_TRANS_AMOUNT	, RC , 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE hostTotalsDtlsGrpItems[] =
{
	{ 0 , NULL, sizeof(stHostTotalsDtlsGrpLst) / RCKEYVAL_SIZE, stHostTotalsDtlsGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* Check Group List */ 
static RCKEYVAL_STYPE	stCheckGrpLst[] =
{
	{ "MICR"			, RC_MICR				, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL }, /* Mandatory for Sale and Reversals, for Authorization it is Conditional */
	{ "DrvLic"			, eDL_NUMBER			, DHI	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "StateCode"		, eDL_STATE				, DHI	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "DLDateOfBirth"	, eDL_DOB				, DHI	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "ChkSvcPvdr"		, RC_CHK_SERVCE_PROVIDER, RC	, 0, NULL, RCHI_TRUE,	SINGLETON, NULL, NULL },
	{ "ChkEntryMethod"	, RC_CHK_ENTRY_METHD	, RC	, 0, NULL, RCHI_TRUE,	SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE checkGrpItems[] =
{
	{ 0 , NULL, sizeof(stCheckGrpLst) / RCKEYVAL_SIZE, stCheckGrpLst},
	{ -1, NULL    , 0 , NULL }
};

/* EBT Group List */ 
static RCKEYVAL_STYPE	stEBTGrpLst[] =
{
	{ "EBTType"			, RC_EBT_TYPE			, RC	, 0, NULL, RCHI_TRUE,	SINGLETON, NULL, NULL }, 
	{ "MerchFNSNum"		, RC_MERCH_FNS_NUM		, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
	{ "EBTCardSeqNum"	, RC_EBT_CARD_SEQ_NUM	, RC	, 0, NULL, RCHI_FALSE,	SINGLETON, NULL, NULL },
};

static VARLST_INFO_STYPE eBTGrpItems[] =
{
	{ 0 , NULL, sizeof(stEBTGrpLst) / RCKEYVAL_SIZE, stEBTGrpLst},
	{ -1, NULL    , 0 , NULL }
};


static RCKEYVAL_STYPE	stEmvGrpLst[] =
{
	{ "EMVData"		, eEMV_TAGS				, DHI, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "CardSeqNum"	, RC_CARD_SEQ_NUM		, RC , 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "XCodeResp"	, RC_X_CODE_RESP		, RC , 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE emvGrpItems[] =
{
	{ 0 , NULL, sizeof(stEmvGrpLst) / RCKEYVAL_SIZE, stEmvGrpLst},
	{ -1, NULL    , 0 , NULL }
};
/* The first 5 fields are what we will currently be using with the File Download Group while downloading a file (EMV Key Data in our case)
 * The Last 3 Fields are used only when we want to read the status of the File and send funcCode as R.
 */
static RCKEYVAL_STYPE	stFileDLGrpLst[] =
{
    { "FileType"			, RC_FL_TYPE			, RC, 0, NULL, RCHI_TRUE,  SINGLETON, NULL, NULL },
	{ "FunCode"				, RC_FUNC_CODE			, RC, 0, NULL, RCHI_TRUE,  SINGLETON, NULL, NULL },
	{ "FBSeq"				, RC_FL_BLK_SEQ			, RC, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "ReqFBMaxSize"		, RC_FL_BLK_MAX			, RC, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "ReqFileOffset"		, RC_REQ_FL_OFF			, RC, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "CurrFileCreationDt"	, RC_FL_CREAT_DATE		, RC, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "FileSize"			, RC_FL_SZ				, RC, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL },
	{ "FileCRC16"			, RC_FL_CRC				, RC, 0, NULL, RCHI_FALSE, SINGLETON, NULL, NULL }

};

static VARLST_INFO_STYPE fileDLGrpItems[] =
{
	{ 0 , NULL, sizeof(stFileDLGrpLst) / RCKEYVAL_SIZE, stFileDLGrpLst},
	{ -1, NULL    , 0 , NULL }
};

static	RCKEYVAL_STYPE  stDCCFields[] = 
{	
	{	"DCCInd"				, RC_DCC_IND					, RC , 0, NULL, RCHI_FALSE, SINGLETON , NULL, NULL },
	{	"DCCTimeZn"				, RC_DCC_TRAN_TIME_ZONE			, RC , 0, NULL, RCHI_FALSE, SINGLETON , NULL, NULL },
	{	"DCCAmt"				, RC_DCC_CARDHOLDER_CURR_AMT    , RC , 0, NULL , RCHI_FALSE, SINGLETON, NULL, NULL },
	{	"DCCRate"				, RC_DCC_CARDHOLDER_CONV_RATE	, RC , 0, NULL , RCHI_FALSE, SINGLETON, NULL, NULL },
	{ 	"DCCCrncy"				, RC_DCC_CARDHOLDER_CURR_CODE	, RC , 0, NULL , RCHI_FALSE, SINGLETON, NULL, NULL }
};

static VARLST_INFO_STYPE dccGrpItems[] =
{
	{ 0 , NULL, sizeof(stDCCFields) / RCKEYVAL_SIZE, stDCCFields},
	{ -1, NULL    , 0 , NULL }
};

/*T_POLISETTYG1: PAYMENT TYPE CALICULATION:
* Currently RCHI Supports CREDIT,DEBIT,PREPAID,ADMIN,CHECK,EBT Payment types, Each Payment type corresponds to individual bit of a byte.
* CREDIT - 0 bit
* DEBIT  - 1 bit
* PREPAID- 2 bit
* ADMIN  - 3 bit
* CHECK  - 4 bit
* EBT    - 5 bit
* 6 & 7th bits are reserved 
* Based on the Payment type the corresponding bit can be set, and calculate the value accordingly.
*/
/* RC Host Request Details Group List  */
static RCKEYVAL_STYPE	stRCHostRequestLst[] =
{
	{ "CommonGrp"   	, NO_ENUM, NO_ENUM, 63, NULL, RCHI_TRUE,	VARLIST, commonGroupItems			, NULL},
	{ "CheckGrp"   		, NO_ENUM, NO_ENUM, 16, NULL, RCHI_FALSE,	VARLIST, checkGrpItems				, NULL},
	{ "CardGrp"			, NO_ENUM, NO_ENUM, 39, NULL, RCHI_FALSE,	VARLIST, cardGroupItems				, NULL},
	{ "PINGrp"			, NO_ENUM, NO_ENUM, 35, NULL, RCHI_FALSE, 	VARLIST, pinGroupItems 				, NULL},
	{ "AddtlAmtGrp"		, NO_ENUM, NO_ENUM, 55, NULL, RCHI_FALSE, 	VARLIST, addtlTaxAmtGroupItems		, NULL},
	{ "AddtlAmtGrp"		, NO_ENUM, NO_ENUM, 55, NULL, RCHI_FALSE, 	VARLIST, addtlCashBakAmtGroupItems	, NULL},
	{ "AddtlAmtGrp"		, NO_ENUM, NO_ENUM, 55, NULL, RCHI_FALSE, 	VARLIST, addtlFAAmtGroupItems		, NULL},
	{ "AddtlAmtGrp"		, NO_ENUM, NO_ENUM, 55, NULL, RCHI_FALSE, 	VARLIST, addtlTAAmtGroupItems		, NULL},
	{ "AddtlAmtGrp"		, NO_ENUM, NO_ENUM, 55, NULL, RCHI_FALSE, 	VARLIST, addtlHCAmtGroupItems		, NULL},
	{ "AddtlAmtGrp"		, NO_ENUM, NO_ENUM, 55, NULL, RCHI_FALSE, 	VARLIST, addtlPRAmtGroupItems		, NULL},
	{ "AddtlAmtGrp"		, NO_ENUM, NO_ENUM, 55, NULL, RCHI_FALSE, 	VARLIST, addtlVIAmtGroupItems		, NULL},
	{ "AddtlAmtGrp"		, NO_ENUM, NO_ENUM, 55, NULL, RCHI_FALSE, 	VARLIST, addtlDEAmtGroupItems		, NULL},
	{ "AddtlAmtGrp"		, NO_ENUM, NO_ENUM, 55, NULL, RCHI_FALSE, 	VARLIST, addtlCLAmtGroupItems		, NULL},
	{ "EMVGrp"			, NO_ENUM, NO_ENUM, 63, NULL, RCHI_FALSE,   VARLIST, emvGrpItems				, NULL},
	{ "TAGrp"			, NO_ENUM, NO_ENUM, 35, NULL, RCHI_FALSE, 	VARLIST, transArmorGrpItems 		, NULL},
	{ "VisaGrp"			, NO_ENUM, NO_ENUM,  1, NULL, RCHI_FALSE, 	VARLIST, visGroupItems				, NULL},
	{ "MCGrp"			, NO_ENUM, NO_ENUM,  1, NULL, RCHI_FALSE, 	VARLIST, mcGroupItems				, NULL},
	{ "DSGrp"			, NO_ENUM, NO_ENUM,  1, NULL, RCHI_FALSE, 	VARLIST, discGroupItems				, NULL},
	{ "AmexGrp"			, NO_ENUM, NO_ENUM,  1, NULL, RCHI_FALSE, 	VARLIST, amexGroupItems				, NULL},
	{ "EbtGrp"			, NO_ENUM, NO_ENUM, 32, NULL, RCHI_FALSE,	VARLIST, eBTGrpItems				, NULL},
	{ "PurchCardlvl2Grp", NO_ENUM, NO_ENUM,  1, NULL, RCHI_FALSE, 	VARLIST, purchCardLvl2GroupItems	, NULL},
	{ "OrigAuthGrp"		, NO_ENUM, NO_ENUM, 63, NULL, RCHI_FALSE, 	VARLIST, origAuthGroupItems			, NULL},
	{ "HostTotGrp"		, NO_ENUM, NO_ENUM,  8, NULL, RCHI_FALSE, 	VARLIST, hostTotalsGrpItems 		, NULL},
	{ "HostTotDetGrp"	, NO_ENUM, NO_ENUM,  8, NULL, RCHI_FALSE, 	VARLIST, hostTotalsDtlsGrpItems		, NULL},
	{ "FileDLGrp"		, NO_ENUM, NO_ENUM, 63, NULL, RCHI_FALSE,   VARLIST, fileDLGrpItems				, NULL},
	{ "DCCGrp"			, NO_ENUM, NO_ENUM, 63, NULL, RCHI_FALSE,	VARLIST, dccGrpItems				, NULL}
};

/*
 * ============================================================================
 * End of file rcReqLists.inc
 * ============================================================================
 */
