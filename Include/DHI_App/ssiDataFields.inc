/* 
 * --------------------------------------------------------
 * -------------- SSI DATA FIELDS ------------------------
 * --------------------------------------------------------
 */
 
 #include "tranDef.h"
 
 static KEYVAL_STYPE genReqLst[] =
{
	{ "FUNCTION_TYPE"		, eFUNCTION_TYPE 				},
	{ "COMMAND"      		, eCOMMAND 						},
	{ "CLIENT_ID"    		, eCLIENT_ID 					},
	{ "DEVICEKEY"    		, eDEVICEKEY					},
	{ "SERIAL_NUM"   		, eSERIAL_NUM					},
	{ "DEVTYPE"      		, eDEVTYPE 						},
	{ "USER_ID"   	 		, eUSER_ID 						},
	
	{ "INVOICE"      		, eINVOICE						},
	{ "STORE_NUM"    		, eSTORE_NUM					},
	{ "LANE"         		, eLANE  						},
	{ "CASHIER_NUM"  		, eCASHIER_NUM  				},
	{ "SERVER_ID"    		, eSERVER_ID  					},
	{ "SHIFT_ID"     		, eSHIFT_ID  					},
	{ "TABLE_NUM"    		, eTABLE_NUM  					},
	{ "PURCHASE_ID"  		, ePURCHASE_ID  				},
	
	{ "TRANS_AMOUNT" 		, eTRANS_AMOUNT					},
	{ "TIP_AMOUNT"   		, eTIP_AMOUNT  					},
	{ "CASHBACK_AMNT"		, eCASHBACK_AMNT  				},
	
	{ "PRESENT_FLAG"		, ePRESENT_FLAG 				},
	{ "CARDHOLDER"  		, eCARDHOLDER 					},
	{ "ACCT_NUM"    		, eACCT_NUM 					},
	{ "EXP_MONTH"   		, eEXP_MONTH 					},
	{ "EXP_YEAR"    		, eEXP_YEAR 					},
	{ "CVV2"        		, eCVV2 						},
	{ "TRACK_DATA"  		, eTRACK_DATA 					},
	{ "TRACK_INDICATOR"  	, eTRACK_INDICATOR 				},
	{ "ENCRYPTION_TYPE" 	, eENCRYPTION_TYPE 				},
	{ "ENCRYPTION_PAYLOAD"  , eENCRYPTION_PAYLOAD 			},
	{ "PINLESSDEBIT"  		, ePINLESSDEBIT 				},
	
	{ "PIN_BLOCK"        	, ePIN_BLOCK 					},
	{ "KEY_SERIAL_NUMBER"	, eKEY_SERIAL_NUMBER			},
	
	{ "MIMETYPE"     		, eMIMETYPE 					},
	{ "SIGNATUREDATA"		, eSIGNATUREDATA 				},
	
	{ "TAX_AMOUNT"			, eTAX_AMOUNT 					},
	{ "TAX_IND"   			, eTAX_IND 						},
	{ "CMRCL_TYPE"			, eCMRCL_TYPE 					},
	
	{ "CUSTOMER_STREET"		, eCUSTOMER_STREET 				},
	{ "CUSTOMER_ZIP"   		, eCUSTOMER_ZIP					},
	
	{ "REF_TROUTD"			, eREF_TROUTD 					},
	{ "CTROUTD"  			, eCTROUTD 						},
	{ "AUTH_CODE"			, eAUTH_CODE 					},
	{ "CARD_TOKEN"			, eCARD_TOKEN 					},
	
	{ "PAYMENT_TYPE"           , ePAYMENT_TYPE 				},
	{ "PAYMENT_MEDIA"		   , ePAYMENT_MEDIA				},
	{ "FORCE_FLAG"             , eFORCE_FLAG 				},
	{ "PARTIAL_REDEMPTION_FLAG", ePARTIAL_REDEMPTION_FLAG 	},
	{ "PARTIAL_AUTH"           , ePARTIAL_AUTH 				},
	{ "SAF_TRAN"	           , eSAF_TRAN 					},
	{ "VSP_REG"		           , eVSP_REG 					},
	
	{ "MICR"       			    , eMICR 					},
	{ "ABA_NUM"    				, eABA_NUM 					},
	{ "CHECK_ACCT_NUM"   		, eCHECK_ACCT_NUM 			},
	{ "CHECK_NUM"  				, eCHECK_NUM 				},
	{ "CHECK_TYPE" 				, eCHECK_TYPE 				},
	{ "DL_STATE"   				, eDL_STATE 				},
	{ "DL_NUMBER"  				, eDL_NUMBER 				},
	
	{ "TERMINATION_SUCCESS"		, eTERMINATION_STATUS		},
	{ "RESULT"					, eRESULT					},
	{ "RESULT_CODE"				, eRESULT_CODE				},
	{ "RESPONSE_TEXT"			, eRESPONSE_TEXT			},
	{ "HOST_RESPCODE"		    , eHOST_RESPCODE			},
	
	{ "VSP_CODE"				, eVSP_CODE					},
	{ "VSP_TRXID"				, eVSP_TRXID				},
	{ "VSP_RESULTDESC"			, eVSP_RESULTDESC			},
	
	{ "APPROVED_AMOUNT"			, eAPPROVED_AMOUNT			},
	{ "AUTH_AMOUNT"				, eAUTH_AMOUNT				},
	{ "AVAIL_BALANCE"			, eAVAIL_BALANCE			},
	{ "AMOUNT_BALANCE"			, eAMOUNT_BALANCE			},
	{ "ORIG_TRANS_AMOUNT"		, eORIG_TRANS_AMOUNT		},
	{ "DIFF_AMOUNT_DUE"			, eDIFF_AMOUNT_DUE			},
	{ "DIFF_DUE_AMOUNT"			, eDIFF_DUE_AMOUNT			},
	{ "DIFF_AUTH_AMOUNT"		, eDIFF_AUTH_AMOUNT			},
	{ "PREVIOUS_BALANCE"		, ePREVIOUS_BALANCE			},
	{ "FS_AVAIL_BALANCE"		, eFS_AVAIL_BALANCE			},
	{ "CB_AVAIL_BALANCE"		, eCB_AVAIL_BALANCE			},
	
	{ "CVV2_CODE"				, eCVV2_CODE				},
	{ "EMBOSSED_ACCT_NUM"		, eEMBOSSED_ACCT_NUM		},
	
	{ "TRANS_SEQ_NUM"			, eTRANS_SEQ_NUM			},
	{ "INTRN_SEQ_NUM"			, eINTRN_SEQ_NUM			},
	{ "TROUTD"					, eTROUTD					},
	{ "LPTOKEN"					, eLPTOKEN					},
	{ "TRACE_NUM"				, eTRACE_NUM				},
	{ "RETURN_CHECK_FEE"		, eRETURN_CHECK_FEE			},
	{ "RETURN_CHECK_NOTE"		, eRETURN_CHECK_NOTE		},
	{ "ACK_REQUIRED"			, eACK_REQUIRED				},
	{ "BANK_USERDATA"			, eBANK_USERDATA			},
	{ "AUTH_RESP_CODE"			, eAUTH_RESP_CODE			},
	{ "TXN_POSENTRYMODE"		, eTXN_POSENTRYMODE			},
	{ "MERCHID"					, eMERCHID					},
	{ "TERMID"					, eTERMID					},
	
	{ "VERSION_INFO"			, eVERSION_INFO				},
	
	{ "TRANS_DATE" 				, eTRANS_DATE				},
	{ "TRANS_TIME"				, eTRANS_TIME				},
	
	{ "TERMINAL"				, eTERMINAL					},
	{ "SERIALNUM"				, eSERIALNUM				},
	
	{ "CREDIT_PROC_ID"			, eCREDIT_PROC_ID			}, //This is used by Host library for device registration command
	{ "DEBIT_PROC_ID"			, eDEBIT_PROC_ID			}, //This is used by Host library for device registration command
	{ "GIFT_PROC_ID"			, eGIFT_PROC_ID				}, //This is used by Host library for device registration command
	{ "PRIVLBL_PROC_ID"			, ePRIVLBL_PROC_ID			}, //This is used by Host library for device registration command
	{ "CHECK_PROC_ID"			, eCHECK_PROC_ID			}, //This is used by Host library for device registration command
	
	{ "REPORT_RESPONSE"			, eREPORT_RESPONSE			}, //This is used by Host library for Report commands
	{ "REPORT_RESP_LENGTH"		, eREPORT_RESP_LENGTH		}, //This is used by Host library for Report commands
	
	{ "HIT_HOST"				, eHIT_HOST					}, //This is used by Host library to inform that it tried to hit the host
	{ "LASTTRAN_COMMAND"		, eLASTTRAN_COMMAND			}, //This is used by Host library for Report last tran commands
	
	{ "SAF_APPROVALCODE"		, eSAF_APPROVALCODE			},
	
	{ "DISCOUNT_AMOUNT"			, eDISC_AMOUNT				},
	{ "FREIGHT_AMOUNT"			, eFREIGHT_AMOUNT			},
	{ "DUTY_AMOUNT"				, eDUTY_AMOUNT				},
	{ "SHIP_FROM_ZIP_CODE"		, eSHIP_POSTAL_CODE			},
	{ "RETAIL_ITEM_DESC_1"		, ePROD_DESC				},
	{ "DEST_POSTAL_CODE"		, eDEST_POSTAL_CODE			},
	{ "ALT_TAX_ID"				, eALT_TAX_ID				},
	{ "DEST_COUNTRY_CODE"		, eDEST_COUNTRY_CODE		},
	{ "CUSTOMER_CODE"           , eCUSTOMER_CODE			},
	
	{ "AMOUNT_HEALTHCARE"       , eAMT_HLTCARE				},
	{ "AMOUNT_PRESCRIPTION"     , eAMT_PRESCRIPION			},
	{ "AMOUNT_VISION"           , eAMT_VISION				},
	{ "AMOUNT_CLINIC"           , eAMT_CLINIC				},
	{ "AMOUNT_DENTAL"           , eAMT_DENTAL				},
	
	{ "EBT_TYPE"           		, eEBT_TYPE					},
		
	{ "EMV_TAGS"           		, eEMV_TAGS					},
	{ "EMV_TAGS_RESPONSE"       , eEMV_TAGS_RESP			},
	{ "DOWNLOAD_STATUS"			, eEMV_DOWNLOAD_STAT		},
	{ "EMV_UPDATE"				, eEMV_UPDATE				},
	{ "EMV_REVERSAL_TYPE"	    , eEMV_REVERSAL_TYPE		},
	{ "AVS_CODE"				, eAVS_CODE					},
	{ "PAYPASS_TYPE"			, ePAYPASS_TYPE				},
	
	{ "BUSINESSDATE"			, eBUSINESS_DATE			},
	
	{ "CARD_ID"						, eCARDID					},
	{ "REF_NUM"						, eREF_NUM					},
	{ "REFERENCE"					, eREF						},
	{ "EXCHANGE_RATE"				, eEXGN_RATE				},
	{ "FGN_CURR_CODE" 				, eFGN_CUR_CODE				},
	{ "FGNAMOUNT"       			, eFGN_AMT					},
	{ "DCC_OFFERED"					, eDCC_OFFD					},
	{ "VALIDHOURS"					, eVALID_HRS				},
	{ "MARGINRATE_PERCENTAGE"		, eMRGN_RATE_PERCENT		},
	{ "EXCHANGE_RATE_SOURCENAME"	, eEXGN_RATE_SRC_NAME		},
	{ "COMMISSION_PERCENTAGE"		, eCOMM_PERCENT				},
	{ "EXCHANGERATESOURCETIMESTAMP" , eEXGN_RATE_SRCTIMESTAMP	},
	{ "MINOR_UNITS"					, eMINR_UNT					},
	
	{ "DCC_IND"						, eDCCInd					},
	{ "DCC_TRANS_AMOUNT"			, eDCCAmt					},
	{ "DCC_CONV_RATE"				, eDCCRate					},
	{ "DCC_CURR_CODE"				, eDCCCrncy					},
	
	{ "AUTHNWID"					, eAUTH_NWID				},
	
	{ "DATE" 						, eDATE						},
	{ "TIME"						, eTIME						}, 
	{ "USER_DEFINED1"				, eUSER_DEFINED1			},
	{ "CHAIN_CODE"					, eCHAIN_CODE				},
	{ "LOCATION_NAME"				, eLOCATION_NAME			},
	{ "API_FIELD_0001" 				, eTRAN_TYPE				},
	{ "API_FIELD_0006" 				, eAUTH_CODE				},
	{ "API_FIELD_0007"   			, eCTROUTD					},
	{ "API_FIELD_0011"				, eFUSEBOX_DATA				},
	{ "STORED_VALUE_FUNC"			, eSTORED_VALUE_FUNC		},
	{ "API_FIELD_0023"				, eDEBIT_ACC_TYPE			},
	{ "API_FIELD_5002"				, eELAVON_SERIAL_NUM		},
	{ "API_FIELD_5004"				, eELAVON_ENCRYPTION_TYPE	},
	{ "API_FIELD_0052"				, eELAVON_PROXIMITY_IND		},
	{ "API_FIELD_0054"				, eELAVON_POS_ENTRY_MODE	},
	{ "API_FIELD_0055"				, eELAVON_PIN_CAPABILITIES	},
	{ "API_FIELD_0057"				, eELAVON_CHIP_CONDITION_CODE	},
	{ "API_FIELD_0047"				, eELAVON_POS_DATA_CODE		},
	{ "PASSTHROUGH_FIELDS"			, ePASSTHROUGH_FIELDS		}	//T_RaghavendranR1 : Currently used by Elavon Host. Can be used to passthrough a xml tag in SSI response for any Host
};