#ifndef __XML_UTILS_H
#define __XML_UTILS_H

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "tranDef.h"


extern int parseXMLForListElem(xmlDoc *, xmlNode *, DHI_VARLST_INFO_PTYPE,
															DHI_VAL_LST_PTYPE *);
extern int freeMetaData(DHI_METADATA_PTYPE);


#endif


