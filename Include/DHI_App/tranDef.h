/*
 * tranDef.h
 *
 *  Created on: 10-Nov-2014
 *      Author: Praveen_P1
 */

#ifndef TRANDEF_H_
#define TRANDEF_H_


typedef enum
{
	SINGLETON = 1,
	STRING,
	AMOUNT,
	BOOLEAN,
	TIME,
	DATE,
	NUMERICS,
	LIST,
	MULTILIST,
	VARLIST,
	RECLIST,
	ATTR_LKDLST,
	MASK_NUM_STR,
	NULL_ADD,
	KEYLIST,
	NUM_RANGE,
	CHAR_SET,
	ATTRIBUTE
}
VAL_TYPE_ENUM;


typedef struct
{
	char *				key;
	int			 		index;
	void *				value;
//	DHI_BOOL			isMand;
	VAL_TYPE_ENUM		valType;
	void *				valMetaInfo;
}
KEYVAL_STYPE, * KEYVAL_PTYPE;

#define KEYVAL_SIZE	sizeof(KEYVAL_STYPE)

typedef struct
{
	int				nodeType;
	char *			szNodeStr;
	int				iElemCnt;
	KEYVAL_PTYPE	keyList;
}
DHI_VARLST_INFO_STYPE, * DHI_VARLST_INFO_PTYPE;


typedef struct __dhistNode
{
	int						type;
	int						elemCnt;
	char *					szVal;
	KEYVAL_PTYPE			elemList;
	struct __dhistNode *	next;
}
DHI_VAL_NODE_STYPE, * DHI_VAL_NODE_PTYPE;

typedef struct
{
	int						valCnt;
	DHI_VAL_NODE_PTYPE		start;
	DHI_VAL_NODE_PTYPE		end;
}
DHI_VAL_LST_STYPE, * DHI_VAL_LST_PTYPE;

#define DHI_VAL_LST_SIZE sizeof(DHI_VAL_LST_STYPE)

/*
 * **********************************************
 * DATA FOR ELEMENT VALUE
 * **********************************************
 */

typedef struct
{
	char * elementValue;
}
TRANDTLS_STYPE, * TRANDTLS_PTYPE;

#define	PASSTHROUGH_XML_TAGS_FILE	"./flash/dhiPassThrgFields.INI"

typedef	struct
{
	int					iReqXMLTagsCnt;
	int					iRespXMLTagsCnt;
	KEYVAL_PTYPE		pstPTXMLRespDtls;
	KEYVAL_PTYPE		pstPTXMLReqDtls;
}
PASSTHROUGH_STYPE, *PASSTHROUGH_PTYPE;


typedef	struct
{
	int					iReqVARLSTCnt;
	KEYVAL_PTYPE		pstVarlistReqDtls;
}
PTVARLST_STYPE, *PTVARLST_PTYPE;


typedef struct
{
	int					iTotCnt;
	KEYVAL_PTYPE		keyValList;
}
DHI_METADATA_STYPE, * DHI_METADATA_PTYPE;

#define DHI_METADATA_SIZE	sizeof(DHI_METADATA_STYPE)


typedef enum
{
	eFUNCTION_TYPE = 0,   		// 0 - FUNCTION_TYPE
	eCOMMAND,             		// 1 - COMMAND

	eCLIENT_ID,           		// 2- CLIENT_ID
	eSERIAL_NUM,          		// 3- SERIAL_NUM
	eDEVICEKEY,           		// 4- DEVICEKEY
	eDEVTYPE,             		// 5- DEVTYPE
	eUSER_ID,			 		// 6 - USER_ID

	eINVOICE,			  		// 7 - INVOICE
	eSTORE_NUM,					// 8 - STORE_NUM
	eLANE,						// 9 - LANE
	eCASHIER_NUM,				//10 - CASHIER_NUM
	eSERVER_ID,					//11 - SERVER_ID
	eSHIFT_ID,					//12 - SHIFT_ID
	eTABLE_NUM,					//13 - TABLE_NUM
	ePURCHASE_ID,				//14 - PURCHASE_ID

	eTRANS_AMOUNT,				//15 - TRANS_AMOUNT
	eTIP_AMOUNT,				//16 - TIP_AMOUNT
	eCASHBACK_AMNT,				//17 - CASHBACK_AMNT

	ePRESENT_FLAG,				//18 - PRESENT_FLAG
	eCARDHOLDER,				//19 - CARDHOLDER
	eACCT_NUM,					//20 - ACCT_NUM
	eEXP_MONTH,					//21 - EXP_MONTH
	eEXP_YEAR,					//22 - EXP_YEAR
	eCVV2,						//23 - CVV2
	eTRACK_DATA,				//24 - TRACK_DATA
	eTRACK_INDICATOR,			//25 - TRACK_INDICATOR
	eENCRYPTION_TYPE,			//26 - ENCRYPTION_TYPE
	eENCRYPTION_PAYLOAD,		//27 - ENCRYPTION_PAYLOAD
	ePINLESSDEBIT,				//28 - PINLESSDEBIT

	ePIN_BLOCK,					//29 - PIN_BLOCK
	eKEY_SERIAL_NUMBER,			//30 - KEY_SERIAL_NUMBER

	eMIMETYPE,					//31 - MIMETYPE
	eSIGNATUREDATA,				//32 - SIGNATUREDATA

	eTAX_AMOUNT,				//33 - TAX_AMOUNT
	eTAX_IND,					//34 - TAX_IND
	eCMRCL_TYPE,				//35 - CMRCL_TYPE

	eCUSTOMER_STREET,			//36 - CUSTOMER_STREET
	eCUSTOMER_ZIP,				//37 - CUSTOMER_ZIP

	eREF_TROUTD,				//38 - REF_TROUTD
	eCTROUTD,					//39 - CTROUTD
	eAUTH_CODE,					//40 - AUTH_CODE
	eCARD_TOKEN,				//41 - CARD_TOKEN

	ePAYMENT_TYPE,				//42 - PAYMENT_TYPE
	ePAYMENT_MEDIA,				//43 - PAYMENT_MEDIA
	eFORCE_FLAG,				//44 - FORCE_FLAG
	ePARTIAL_REDEMPTION_FLAG, 	//45 - PARTIAL_REDEMPTION_FLAG
	ePARTIAL_AUTH,				//46 - PARTIAL_AUTH
	eSAF_TRAN,					//47 - SAF_TRAN
	eVSP_REG,					//48 - VSP_REG

	eMICR,						//49 - MICR
	eABA_NUM,					//50 - ABA_NUM
	eCHECK_ACCT_NUM,			//51 - ACCT_NUM
	eCHECK_NUM,					//52 - CHECK_NUM
	eCHECK_TYPE,				//53 - CHECK_TYPE
	eDL_STATE,					//54 - DL_STATE
	eDL_NUMBER,					//55 - DL_NUMBER

	eTERMINATION_STATUS,		//56 - TERMINATION_SUCCESS
	eRESULT,					//57 - RESULT
	eRESULT_CODE,				//58 - RESULT_CODE
	eRESPONSE_TEXT,				//59 - RESPONSE_TEXT
	eHOST_RESPCODE,			    //60 - HOST_RESPCODE

	eVSP_CODE,					//61 - VSP_CODE
	eVSP_TRXID,					//62 - VSP_TRXID
	eVSP_RESULTDESC,			//63 - VSP_RESULTDESC

	eAPPROVED_AMOUNT,			//64 - APPROVED_AMOUNT
	eAUTH_AMOUNT,				//65 - AUTH_AMOUNT
	eAVAIL_BALANCE,				//66 - AVAIL_BALANCE
	eAMOUNT_BALANCE,			//67 - AMOUNT_BALANCE
	eORIG_TRANS_AMOUNT,			//68 - ORIG_TRANS_AMOUNT
	eDIFF_AMOUNT_DUE,			//69 - DIFF_AMOUNT_DUE
	eDIFF_DUE_AMOUNT,			//70 - DIFF_DUE_AMOUNT
	eDIFF_AUTH_AMOUNT,			//71 - DIFF_AUTH_AMOUNT
	ePREVIOUS_BALANCE,			//72 - PREVIOUS_BALANCE

	eCVV2_CODE,					//73 - CVV2_CODE
	eEMBOSSED_ACCT_NUM,			//74 - EMBOSSED_ACCT_NUM

	eTRANS_SEQ_NUM,				//75 - TRANS_SEQ_NUM
	eINTRN_SEQ_NUM,				//76 - INTRN_SEQ_NUM
	eTROUTD,					//77 - TROUTD
	eLPTOKEN,					//78 - LPTOKEN
	eTRACE_NUM,					//79 - TRACE_NUM
	eRETURN_CHECK_FEE,			//80 - RETURN_CHECK_FEE
	eRETURN_CHECK_NOTE,			//81 - RETURN_CHECK_NOTE
	eACK_REQUIRED,				//82 - ACK_REQUIRED
	eBANK_USERDATA,				//83 - BANK_USERDATA
	eAUTH_RESP_CODE,			//84 - AUTH_RESP_CODE
	eTXN_POSENTRYMODE,			//85 - TXN_POSENTRYMODE
	eMERCHID,					//86 - MERCHID
	eTERMID,					//87 - TERMID

	eVERSION_INFO,				//88 - VERSION_INFO

	eTRANS_DATE,				//89 - TRANS_DATE
	eTRANS_TIME,				//90 - TRANS_TIME

	eTERMINAL,					//91 - TERMINAL
	eSERIALNUM,					//92 - SERIALNUM

//From 91 to 97 are not correspond to tag/field names in the SSI response/request, these are used for DHI-Host Library communication purpose
	eCREDIT_PROC_ID,			//93 - CREDIT_PROCESSOR_ID
	eDEBIT_PROC_ID,				//94 - DEBIT_PROCESSOR_ID
	eGIFT_PROC_ID,				//95 - GIFT_PROCESSOR_ID
	ePRIVLBL_PROC_ID,			//96 - PRIVATELABEL_PROCESSOR_ID
	eCHECK_PROC_ID,				//97 - CHECK_PROCESSOR_ID

	eREPORT_RESPONSE,			//98 - REPORT_RESPONSE
	eREPORT_RESP_LENGTH,		//99 - REPORT_RESPONSE_LENGTH

//Following tag/field name does not correspond in the SSI response/request, it is used for DHI-Host Library communication purpose
	eHIT_HOST,					//100 - HIT_HOST

//This is used to store the last tran command name
	eLASTTRAN_COMMAND,			//101 - LASTTRAN_COMMAND

	eSAF_APPROVALCODE,			//102 - SAF_APPROVALCODE

	eDISC_AMOUNT,				//103 - DISC_AMOUNT
	eFREIGHT_AMOUNT,			//104 - FREIGHT_AMOUNT
	eDUTY_AMOUNT,				//105 - DUTY_AMOUNT
	eSHIP_POSTAL_CODE,			//106 - SHIP_POSTAL_CODE
	ePROD_DESC,					//107 - PROD_DESC
	eDEST_POSTAL_CODE,			//108 - DEST_POSTAL_CODE
	eALT_TAX_ID,				//109 - ALT_TAX_ID
	eDEST_COUNTRY_CODE,			//110 - DEST_COUNTRY_CODE
	eCUSTOMER_CODE,				//111 - CUSTOMER_CODE

	eAMT_HLTCARE,               //112 - AMOUNT_HEALTHCARE
	eAMT_PRESCRIPION,           //113 - AMOUNT_PRESCRIPTION
	eAMT_VISION,                //114 - AMOUNT_VISION
	eAMT_CLINIC,                //115 - AMOUNT_CLINIC
	eAMT_DENTAL,                //116 - AMOUNT_DENTAL

	eEBT_TYPE,					//117 - EBT_TYPE
	eDL_DOB,					//118 - DATE_OF_BIRTH

	eEMV_TAGS,					//119 - EMV_TAGS
	eEMV_TAGS_RESP,				//120 - EMV_TAGS_RESPONSE
	eEMV_DOWNLOAD_STAT,			//121 - EMV_DOWNLOAD_STATUS
	eEMVADMIN_RESPONSE,			//122 - REPORT_RESPONSE
	eEMVADMIN_RESP_LENGTH,		//123-  REPORT_RESPONSE_LENGTH
	eEMV_UPDATE,				//124 - EMV_UPDATE
	eEMV_REVERSAL_TYPE,			//125 - EMV_REVERSAL_TYPE

	eFS_AVAIL_BALANCE,			//126 - EBT_FS_BALANCE
	eCB_AVAIL_BALANCE,			//127 - EBT_CB_BALANCE

	eAVS_CODE,					//128- 	AVS_CODE
	ePAYPASS_TYPE,				//129 - PAYPASS_TYPE

	eBUSINESS_DATE,

	eCARDID,					//130 - CARDID for FEXCO
	eREF_NUM,					//131 - Reference Number for FEXCO

	eREF,
	eEXGN_RATE,					//132
	eFGN_CUR_CODE,				//133
	eFGN_AMT,					//134
	eDCC_OFFD,					//135
	eVALID_HRS,					//136
	eMRGN_RATE_PERCENT,			//137
	eEXGN_RATE_SRC_NAME,		//138
	eCOMM_PERCENT,				//139
	eEXGN_RATE_SRCTIMESTAMP,	//140
	eMINR_UNT,					//141
	eRESPONSE_CODE,				//143

	eCURR_CODE,					//144
	eCOUN_NAME,					//145
	eCURR_NAME,					//146
	eALPHA_CURR_CODE,			//147
	eCOUN_ALPHA_2_CODE,			//148
	eCURR_SYMBOL,				//149
	eMINOR_CURR_UNIT,			//150
	eHTML_CODE,					//151

	eDCCInd,					//152
	eDCCAmt,					//153
	eDCCRate,					//154
	eDCCCrncy,					//155
	eAUTH_NWID,					//156
	eBATCH_NUM,					//157

	eDATE,						//158 - DATE
	eTIME,						//159 - TIME
	eUSER_DEFINED1 ,			//160 - USER DEFINED DATA(PASS THROUGH FROM SCA)
	eGATEWAY_RESULT_CODE,		//161 - Added this specifically for Elavon Host. We usually modify the RESULT_CODE to 4, 5, 6, or 7 to SCA to understand Approved or Declined, but Elavon POS may expect the original RESULT code from Host. So sending original RESULT code in this field..
	eTRAN_TYPE, 				//162 - Used By Elavon Gateway, It will be pass through in SCA
	eFUSEBOX_DATA,				//163 - Used By Elavon Gateway, It will be Pass through in SCA
	eSTORED_VALUE_FUNC,			//164 - Used By Elavon Gateway, It will be Pass through in SCA
	eCHAIN_CODE,				//165 - Used By Elavon Gateway, It will be Pass through in SCA
	eLOCATION_NAME,				//166 - Used By Elavon Gateway, It will be Pass through in SCA
	ePASSTHROUGH_FIELDS,		//167 - T_RaghavendranR1 : Currently used ny Elavon Host. Can be used to passthrough a xml tag in SSI response for any Host
	eDEBIT_ACC_TYPE,			//168 - Used By Elavon Gateway, It will be Pass through in SCA
	eISSUER_AUTH_DATA_91TAG,		//169 - Used By Elavon Gateway, It will be Pass through in SCA
	ePRE_AC_GEN_ISSUE_SCRIPTS_71,	//170 - Used By Elavon Gateway, It will be Pass through in SCA
	ePRE_AC_GEN_ISSUE_SCRIPTS_72,	//171 - Used By Elavon Gateway, It will be Pass through in SCA
	eISSUER_SCRIPT_DENTIFIER_9F18,	//172 - Used By Elavon Gateway, It will be Pass through in SCA
	eELAVON_TRANS_AMOUNT,			//173 - Used By Elavon Gateway, It will be Pass through in SCA
	eELAVON_APPROVED_AMOUNT,		//174 - Used By Elavon Gateway, It will be Pass through in SCA
	eELAVON_SERIAL_NUM,				//175 - Used By Elavon Gateway, It will be Pass through in SCA
	eELAVON_ENCRYPTION_TYPE,		//176 - Used By Elavon Gateway, It will be Pass through in SCA
	eELAVON_POS_ENTRY_MODE,			//177 - Used By Elavon Gateway, It will be Pass through in SCA
	eELAVON_PROXIMITY_IND,			//178 - Used By Elavon Gateway, It will be Pass through in SCA
	eELAVON_CHIP_CONDITION_CODE,	//179 - Used By Elavon Gateway, It will be Pass through in SCA
	eELAVON_PIN_CAPABILITIES,		//180 - Used By Elavon Gateway, It will be Pass through in SCA
	eELAVON_POS_DATA_CODE,			//181 - Used By Elavon Gateway, It will be Pass through in SCA

	eELEMENT_INDEX_MAX
}
ELEMENT_INDEX;

/*FUNCTION_TYPE struct*/
typedef struct  {
	char PAYMENT[7+1];
	char BATCH[5+1];
	char REPORT[6+1];
}sFUNCTION_TYPE;


/*COMMAND struct*/
typedef struct  {
	char PRE_AUTH[8+1];
	char SALE[4+1];
	char CREDIT[6+1];
	char VOID[4+1];
	char POST_AUTH[9+1];
	char ADD_TIP[7+1];
	char COMPLETION[10+1];
	char ADD_VALUE[9+1];
	char REMOVE_VALUE[12+1];
	char ACTIVATE[8+1];
	char DEACTIVATE[10+1];
	char BALANCE[7+1];
	char SIGNATURE[9+1];
	char TOKEN_QUERY[11+1];
	char SETTLE[6+1];
	char DAYSUMMARY[10+1];
	char VERSION[7+1];
	char DEVADMIN[16+1];
	char TOR_REQ[7+1];
	char VOICE_AUTH[10+1];
	char CUTOVER[7+1];
	char HOSTTOTALS[10+1];
	char LAST_TRAN[9+1];
	char GIFT_CLOSE[10+1];
	char CARD_RATE[9+1];
}sCOMMAND;

/*PAYMENT_TYPE struct*/
typedef struct  {
	char CREDIT[6+1];
	char DEBIT[5+1];
	char GIFT[4+1];
	char PRIVLBL[8+1];
	char CHECK[5+1];
	char EBT[3+1];
}sPAYMENT_TYPE;

extern PASSTHROUGH_PTYPE getDHIPassThroughDtls();
extern PTVARLST_PTYPE getDHIVarLstDtls();
extern int getDHIIndex(char* );

#endif /* TRANDEF_H_ */
