/*
 * httpHandler.h
 *
 *  Created on: 24-Jul-2014
 *      Author: Praveen_P1
 */

#ifndef HTTPHANDLER_H_
#define HTTPHANDLER_H_

#define HTTP_STATUS_PROCESSING				102
#define HTTP_STATUS_OK						200
#define HTTP_STATUS_BAD_REQUEST				400
#define HTTP_STATUS_NOT_FOUND				404
#define HTTP_STATUS_METHOD_NOT_ALLOWED		405
#define HTTP_STATUS_NOT_ACCEPTABLE			406
#define HTTP_STATUS_REQUEST_TIMEOUT			408
#define HTTP_STATUS_LENGTH_REQUIRED			411
#define HTTP_STATUS_REQUEST_ENTITY_LARGE	413
#define HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE	415
#define HTTP_STATUS_INTERNAL_SERVER_ERROR	500
#define HTTP_STATUS_SERVER_UNAVAILABLE		503
#define HTTP_STATUS_GATEWAY_TIMEOUT			504
#define HTTP_STATUS_VERSION_NOT_SUPPORTED	505

typedef enum
{
	POST_METHOD = 1,
	GET_METHOD,
	HEAD_METHOD,
	UNSUPPORTED_METHOD
}
HTTP_METHOD;

typedef struct
{
	int  iMethod;
	int  iStatusCode;
	int	 iReqLength;
	int  iResLength;
	char szContentType[30];
	char *pszRequest;
	char *pszResponse;
}
HTTPINFO_STYPE, * HTTPINFO_PTYPE;;


#endif /* HTTPHANDLER_H_ */
