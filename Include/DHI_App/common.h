/*
 * utils.h
 *
 *  Created on: 09-Jul-2014
 *      Author: Praveen_P1
 */

#ifndef __COMMON_H
#define __COMMON_H

#define DHI_APP_VERSION	"V_1.00.07"
#define DHI_BUILD_NUMBER "1"

extern char chDebug;
extern char chMemDebug;

#ifdef DEBUG
	#include "tcpipdbg.h"
	//#define debug_sprintf(...) sprintf(__VA_ARGS__)
	#define DEVDEBUGMSG	"DEVDEBUG"
	#define debug_sprintf(...) \
	{ \
	    if (chDebug != NO_DEBUG) \
	    { \
	        sprintf(__VA_ARGS__) ; \
	    } \
	}
	#define debug_mem_sprintf(...) \
			{ \
		if (chMemDebug != NO_DEBUG) \
		{ \
			sprintf(__VA_ARGS__) ; \
		} \
			}
	extern void setupDebug(int *);
	extern void setupMemDebug(int *);
	extern void closeupDebug(void);
	extern void closeupMemDebug(void);
	extern void APP_TRACE(char *pszMessage);
	extern void APP_MEM_TRACE(char *pszMessage);
	extern int setDebugParamsToEnv(int * );
#else
	#define debug_sprintf(...) ;
	#define debug_mem_sprintf(...);
	#define APP_TRACE(s) ;
	#define APP_MEM_TRACE(s);
	#define APP_TRACE_EX(s, i) ;
#endif

#define SUCCESS 		0
#define FAILURE			-1403

#define PAYMENT_TYPE_NOT_PRESENT   -1
#define PAYMENT_TYPE_NOT_SUPPORTED -2
#define COMMAND_NOT_SUPPORTED	   -3
#define HOST_NOT_CONFIGURED		   -4
#define LAST_TRAN_ERROR			   -5

typedef enum
{
	DHI_FALSE = 0,
	DHI_TRUE
}
DHI_BOOL;

typedef int	SOCK;

/* Application Logging #defines */

// App Entry for Transactional App Logging

#define	APP_NAME "DHI"

// Entry Tyrp for Transactional App Logging
#define ENTRYTYPE_INFO 		"INFO"
#define ENTRYTYPE_ERROR 	"ERROR"
#define ENTRYTYPE_FAILURE 	"FAILURE"
#define ENTRYTYPE_WARNING 	"WARNING"
#define ENTRYTYPE_SUCCESS 	"SUCCESS"

#define START_INDICATOR 			"######################### START #########################"
#define END_INDICATOR 			    "#########################  END  #########################"

// App Entry Id for Transactional App Logging
#define ENTRYID_START_UP			"START_UP"
#define	ENTRYID_START				"START"
#define	ENTRYID_COMMAND_END			"END"
#define ENTRYID_RECEIVE				"RECEIVE"
#define ENTRYID_PROCESSING			"PROCESSING"
#define ENTRYID_PROCESSED			"PROCESSED"
#define ENTRYID_SENT				"SENT"
#define ENTRYID_PROCESSING			"PROCESSING"
#define ENTRYID_DHI_REQUEST			"DHI_REQUEST"
#define ENTRYID_DHI_RESPONSE		"DHI_RESPONSE"
#define ENTRYID_RESPONDED			"RESPONDED"

#define LAST_TRAN_FILE_NAME         "./flash/lasttran.txt"

#define PIPE    	0x7C
#define SEMI_COLON 	0x3B

extern void *dhiMalloc(unsigned int, int, char* );
extern char *dhistrdup(char*, int , char *);
extern char *dhistrndup(char *, unsigned int , int, char * );
extern void *dhiReAlloc(void *, unsigned int, int, char*);
extern void  dhiFree(void **, int, char*);


#endif /* COMMON_H */
