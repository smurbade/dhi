/*
 * hostInterface.h
 *
 *  Created on: Dec 11, 2014
 *      Author: Praveen_p1
 */

#ifndef HOSTINTERFACE_H_
#define HOSTINTERFACE_H_

#define CREDIT_TRAN_FUNC_NAME  		"processCreditTransaction"
#define DEBIT_TRAN_FUNC_NAME   		"processDebitTransaction"
#define GIFT_TRAN_FUNC_NAME  		"processGiftTransaction"
#define PRIVLBL_TRAN_FUNC_NAME  	"processPrivLabelTransaction"
#define CHECK_TRAN_FUNC_NAME  		"processCheckTransaction"
#define EBT_TRAN_FUNC_NAME  		"processEBTTransaction"
#define INIT_LIB_FUNC_NAME			"initHILib"
#define GET_VER_FUNC_NAME			"getHILibVersion"
#define	DEVICE_REG_FUNC_NAME		"processDeviceRegistration"
#define REPORT_CMD_FUNC_NAME		"processReportCommand"
#define TOKEN_QUERY_FUNC_NAME       "processTokenQueryCommand"
#define CHECK_HOST_CON_FUNC_NAME 	"checkHostConnection"
#define	CARD_RATE_FUNC_NAME			"processCardRateCommand"

typedef int (*fnpInitHI)( void * );
typedef void (*fnpGetVersion)( char * , char * );
typedef int (*fnpProcessDeviceReg)( void * );
typedef int (*fnpProcessReportCmd)( void * );
typedef int (*fnpProcessTokenQueryCmd)(void *);
typedef int (*fnpProcessCardRateCmd)(void *);

#define MAX_PROCESSOR_NUM 6 //Maximum five processors can be there at a time

typedef struct
{
	int (*fnpProcessCreditRequest)(void * );
	int (*fnpProcessDebitRequest)(void * );
	int (*fnpProcessGiftRequest)(void * );
	int (*fnpProcessPrivLblRequest)(void * );
	int (*fnpProcessCheckRequest)(void * );
	int	(*fnpProcessEBTRequest)(void *);
	int (*fnpCheckHostConnection)(void *);
	fnpInitHI 	  			 fnpInitHIArray[MAX_PROCESSOR_NUM];
	fnpGetVersion 			 fnpGetVerArray[MAX_PROCESSOR_NUM];
	fnpProcessDeviceReg		 fnpDevRegArray[MAX_PROCESSOR_NUM];
	fnpProcessReportCmd 	 fnpRepCmdArray[MAX_PROCESSOR_NUM];
	fnpProcessTokenQueryCmd  fnpTknQryCmdArray[MAX_PROCESSOR_NUM];
	fnpProcessCardRateCmd	 fnpCardRateCmdArray[MAX_PROCESSOR_NUM];
}
FUNC_POINTERS_STYPE, * FUNC_POINTERS_PTYPE;


#endif /* HOSTINTERFACE_H_ */
