/*
 * dhiCfgDef.h
 *
 *  Created on: Dec 11, 2014
 *      Author: Praveen_p1
 */

#ifndef DHICFGDEF_H_
#define DHICFGDEF_H_

#define SECTION_DHI			"DHI"
#define SECTION_RCHI		"RCHI"
#define	SECTION_CPHI        "CPHI"
#define	SECTION_EHI         "EHI"
#define CREDIT_PROCESSOR	"creditprocessor"
#define DEBIT_PROCESSOR		"debitprocessor"
#define GIFT_PROCESSOR		"giftprocessor"
#define PRIVLBL_PROCESSOR	"privlblprocessor"
#define CHECK_PROCESSOR		"checkprocessor"
#define EBT_PROCESSOR		"ebtprocessor"
#define TERMINAL_ID			"tid"
#define MERCHANT_ID			"mid"
#define LOCATION			"location"
#define CHAIN_CODE			"chaincode"
#define DHI_PROCESSOR		"processor"
#define SECTION_DHIREQUEST	"DHIREQUEST"
#define SECTION_DHIRESPONSE	"DHIRESPONSE"

typedef struct
{
	char					szCreditProcID[10];
	char					szDebitProcID[10];
	char					szGiftProcID[10];
	char					szPrivLblProcID[10];
	char					szCheckProcID[10];
	char					szEBTProcID[10];
	char					szMID[50];
	char					szTID[10];
	char					szDHIProcessor[15];
}
DHI_CFG_STYPE, * DHI_CFG_PTYPE;

extern DHI_CFG_STYPE dhiSettings;

#endif /* DHICFGDEF_H_ */
