/*
 * appLog.h
 *
 *  Created on: Dec 22, 2014
 *      Author: Praveen_p1
 */

#ifndef APPLOG_H_
#define APPLOG_H_

#define MAX_DATA_SIZE				5100 //Please don't change it to higher value since msg queue allows max 8192 bytes in single msg.

extern int initializeAppLog();
extern int closeAppLog();
extern int isAppLogEnabled();
extern void addAppEventLog(char*, char* , char* , char* , char* );

#endif /* APPLOG_H_ */
