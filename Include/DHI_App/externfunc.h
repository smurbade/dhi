/*
 * externfunc.h
 *
 *  Created on: Dec 10, 2014
 *      Author: Praveen_p1
 */

#ifndef EXTERNFUNC_H_
#define EXTERNFUNC_H_

/* Extern functions */
extern int SaveDebugLogs();
extern int loadConfigParams();
extern int createDHIServer(int *);
extern int startListener(int );
extern int createHashTableForSSIFields();
extern int createThreadPool(int );
extern int loadHostInterfaceLibs();

extern int addJobToThreadPool(void *(*function_p)(int ), int);
extern int readHTTPRequest(int , HTTPINFO_PTYPE );
extern int sendHTTPSuccessResponse(int , HTTPINFO_PTYPE );
extern int sendHTTPErrorResponse(int , HTTPINFO_PTYPE );
extern int parseSSIRequest(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
extern int processSSIRequest(HTTPINFO_PTYPE , TRANDTLS_PTYPE );
extern int frameSSIResponse(HTTPINFO_PTYPE , TRANDTLS_PTYPE );

extern void getCreditProcessorName(char *);
extern void getDebitProcessorName(char *);
extern void getGiftProcessorName(char *);
extern void getPrivLblProcessorName(char *);
extern void getCheckProcessorName(char *);
extern void getDHIProcessorField(char *);

extern int processCreditTransaction(TRANDTLS_PTYPE );
extern int processDebitTransaction(TRANDTLS_PTYPE );
extern int processGiftTransaction(TRANDTLS_PTYPE );
extern int processPrivLblTransaction(TRANDTLS_PTYPE );
extern int processCheckTransaction(TRANDTLS_PTYPE );
extern int processEBTTransaction(TRANDTLS_PTYPE );
extern int processDeviceRegistration(TRANDTLS_PTYPE );
extern int processReportCommand(TRANDTLS_PTYPE );
extern int processTokenQueryCommand(TRANDTLS_PTYPE );
extern int processCardRateCommand(TRANDTLS_PTYPE );
extern int getHILibDtls(char *, char *);
extern void checkHostConnection(HTTPINFO_PTYPE );

extern int  setDynamicFunctionPtr(char *, char *, int (**pFunc)(void *));
extern int setDynamicFunctionPtr_1(char *, char *, void (**pFunc)(char *, char *, char *, char *, char *));
extern int setDynamicFunctionPtr_2(char *, char *, void (**pFunc)(char *, char *));
extern void acquireMutexLock(pthread_mutex_t * , const char * );
extern void releaseMutexLock(pthread_mutex_t * , const char * );
extern void getMaskedPAN(char *, char *);
extern void getPANFromTrackData(char *, int , char *);

extern int updateLastTranDtls(TRANDTLS_PTYPE );

extern int initializeAppLog();
extern int closeAppLog();
extern int isAppLogEnabled();
extern void addAppEventLog(char*, char* , char* , char* , char* );

extern int	loadDHIPassThroughXMLDtls();
extern int	doesFileExist(const char *);
#endif /* EXTERNFUNC_H_ */
