
/*
 * cphicommon.h
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */

#ifndef CPHICOMMOM_H_
#define CPHICOMMOM_H_

#include "DHI_App/tranDef.h"

/* Application Logging #defines */

// App Entry for Transactional App Logging

#define	APP_NAME "CPHI"

#define	SIG_KA_TIME SIGRTMIN+2

typedef unsigned long long ullong;
typedef unsigned char 		uchar;

#define CP_TRAN_RECORDS			 "./flash/CPtranRecords.txt"
#define CP_PARAMS_FILE			 "./flash/CPParams.txt"
#define EMV_TABLES_FILE_PATH	 "./flash/EMVTables.ini"
#define CP_USERNAME_PASSWORD     "./flash/CPUsernameAndPassword.txt"

#define ERR_UNSUPP_CMD			-101
#define ERR_NO_FLD				-110
#define ERR_INV_COMBI			-107
#define ERR_FLD_REQD			-108
#define ERR_INV_FLD_VAL			-111
#define ERR_INV_FLD				-109

#define SUCCESS 		0
#define FAILURE		-1403


// Entry Tyrp for Transactional App Logging
#define ENTRYTYPE_INFO 		"INFO"
#define ENTRYTYPE_ERROR 	"ERROR"
#define ENTRYTYPE_FAILURE 	"FAILURE"
#define ENTRYTYPE_WARNING 	"WARNING"
#define ENTRYTYPE_SUCCESS 	"SUCCESS"

// App Entry Id for Transactional App Logging
#define ENTRYID_START_UP			"START_UP"
#define	ENTRYID_START				"START"
#define	ENTRYID_COMMAND_END			"END"
#define ENTRYID_RECEIVE				"RECEIVE"
#define ENTRYID_PROCESSING			"PROCESSING"
#define ENTRYID_PROCESSED			"PROCESSED"
#define ENTRYID_SENT				"SENT"
#define ENTRYID_PROCESSING			"PROCESSING"
#define ENTRYID_CP_REQUEST			"CP_REQUEST"
#define ENTRYID_CP_RESPONSE			"CP_RESPONSE"
#define ENTRYID_RESPONDED			"RESPONDED"

/* ERROR CONSTANTS (NEED TO CHANGE) */

#define ERR_UNSUPP_CMD			-101
#define ERR_SYSTEM				-102
#define ERR_RESP_TO				-103
#define ERR_CONN_TO				-104
#define ERR_CONN_FAIL			-105
#define ERR_CFG_PARAMS			-106
#define ERR_INV_COMBI			-107
#define ERR_FLD_REQD			-108
#define ERR_INV_FLD				-109
#define ERR_NO_FLD				-110
#define ERR_INV_FLD_VAL			-111
#define ERR_INV_RESP			-112
#define ERR_HOST_ERROR			-113
#define ERR_HOST_TRAN_REVERSED	-114
#define ERR_HOST_TRAN_TIMEOUT 	-115
#define ERR_HOST_NOT_AVAILABLE 	-116
#define ERR_INV_CTROUTD			-117

#define CPHI_LIB_VERSION	"V_1.00.00"
#define CPHI_BUILD_NUMBER   "8"
#define CPHI_RELEASE_DATE   "051016" //MMDDYY

#ifdef DEBUG
	extern char chCPHIDebug;
	extern char chCPHIMemDebug;
	#include "tcpipdbg.h"
	//#define debug_sprintf(...) sprintf(__VA_ARGS__)
	#define debug_sprintf(...) \
			{ \
		if (chCPHIDebug != NO_DEBUG) \
		{ \
			sprintf(__VA_ARGS__) ; \
		} \
			}
	#define debug_mem_sprintf(...) \
			{ \
		if (chCPHIMemDebug != NO_DEBUG) \
		{ \
			sprintf(__VA_ARGS__) ; \
		} \
			}

	extern void setupCPHIDebug(int *);
	extern void setupCPHIMemDebug(int *);
	extern void closeupDebug(void);
	extern void closeupMemDebug(void);
	extern void CPHI_LIB_TRACE(char *pszMessage);
	extern void CPHI_LIB_MEM_TRACE(char *pszMessage);
	extern void CPHI_LIB_TRACE_EX(char *pszMessage, int iLen);
	extern int setCPHIDebugParamsToEnv(int * );
#else
	#define debug_sprintf(...) ;
	#define debug_mem_sprintf(...);
	#define CPHI_LIB_TRACE(s) ;
	#define CPHI_LIB_MEM_TRACE(s) ;
	#define CPHI_LIB_TRACE_EX(s, i) ;
#endif

#define DEVDEBUGMSG 	"DEVDEBUGMSG"
#define CPHI_DEBUG		"CPHI_DEBUG"
#define CPHI_MEM_DEBUG  "CPHI_MEM_DEBUG"

#define MAX_DEBUG_MSG_SIZE	15+4096+2+1

extern void getHILibVersion(char *, char *);
extern void initEMVDwnloadParams();

typedef enum
{
	NO_ENUM = -1,
	CPHI_FALSE,
	CPHI_TRUE
}
CPHI_BOOL;


typedef enum
{
	CP_AUTH = 0,
	CP_SALE,
	CP_COMPLETION,
	CP_POST_AUTH,
	CP_RETURN,
	CP_VOID,
	CP_ACTIVATE,
	CP_REDEMPTION,
	CP_ADDVALUE,
	CP_BALANCE,
	CP_DEACTIVATE,
	CP_EMV_DOWNLOAD,
	CP_BATCH_INQUIRY,
	CP_BATCH_CLOSE,
	CP_TOKEN_QUERY,
	CP_REVERSAL,
	MAX_COMMANDS
}
COMMAND_ENUM;

#define CPHI_EMVAID_LEN 10
#define CPHI_EMVFLOOR_LMT_LEN 9

/* EMV Fallback Back Indicator Nodes */
typedef struct __gstEMVFINode
{
	char					szAID[CPHI_EMVAID_LEN+1];
	char					szPkFbInd[1+1];
	struct __gstEMVFINode* next;
}
VAL_EMVFI_STYPE, * VAL_EMVFI_PTYPE;

/* EMV Fallback Back Indicator List */
typedef struct __gstEMVFILstHeadNode
{
	int iNumFbNodes;
	VAL_EMVFI_PTYPE pstFILst;
}
VAL_EMVFIHEADNODE_STYPE, * VAL_EMVFIHEADNODE_PTYPE;

/* EMV Floor Limit Nodes */
typedef struct __gstEMVFLNode
{
	char					szAID[CPHI_EMVAID_LEN+1];
	char					szOfflineFlrLmt[CPHI_EMVFLOOR_LMT_LEN+1];
	char					szRandSelThresh[CPHI_EMVFLOOR_LMT_LEN+1];
	struct __gstEMVFLNode* next;
}
VAL_EMVFL_STYPE, * VAL_EMVFL_PTYPE;

/* EMV Floor Limit List */
typedef struct __gstEMVFLLstHeadNode
{
	int iNumFlrLmtNodes;
	VAL_EMVFL_PTYPE pstFLLst;
}
VAL_EMVFLHEADNODE_STYPE, * VAL_EMVFLHEADNODE_PTYPE;

/* EMV Public Key Nodes */
typedef struct __gstEMVPkNode
{
	char					szAID[CPHI_EMVAID_LEN+1];
	char					szPkIndex[3];
	char					szPkModLen[5];
	char*					pszPkMod;
	char					szPkCkSumLen[3];
	char*					pszPkCkSum;
	char					szPkExpLen[3];
	char*					pszPkExp;
	//char*					pszPkExpDate;
	struct __gstEMVPkNode* next;
}
VAL_EMVPK_STYPE, * VAL_EMVPK_PTYPE;

/* EMV Public Keys List */
typedef struct __gstEMVPkLstHeadNode
{
	VAL_EMVPK_PTYPE pstKeysLst;
	int 					iMaxPublicKeys;
}
VAL_EMVPKLSTHEADNODE_STYPE, * VAL_EMVPKLSTHEADNODE_PTYPE;

typedef struct __gstEMVParamDwnldNode
{
	char 					szLastRetRef[9];
	char					szDwnldStat[2];
	int						iKeyUpdAvl;
}
VAL_EMVPARAMDWNLDNODE_STYPE, * VAL_EMVPARAMDWNLDNODE_PTYPE;


extern VAL_EMVPKLSTHEADNODE_STYPE gstEMVPkLst;
extern VAL_EMVPARAMDWNLDNODE_STYPE gstEMVParamDwnld;
extern VAL_EMVFLHEADNODE_STYPE gstEMVFLlst;
extern VAL_EMVFIHEADNODE_STYPE gstEMVFIlst;

typedef struct
{
	char elementValue[1024];
	char fieldSeparator[12+1];
}
CPTRANDTLS_STYPE, * CPTRANDTLS_PTYPE;

typedef struct
{
	int iPaymentType;
	int iCommand;
}
CPPYMTNCMD_STYPE, * CPPYMTNCMD_PTYPE;


extern void *cphiMalloc(unsigned int, int, char* );
extern char *cphistrdup(char*, int , char *);
extern char *cphistrndup(char *, unsigned int , int, char * );
extern void *cphiReAlloc(void *, unsigned int, int, char*);
extern void  cphiFree(void **, int, char*);

#endif /* CPHICOMMOM_H_ */
