
/*
 * cphiGroups.h
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */

#ifndef __CP_GROUPS_H
#define __CP_GROUPS_H

#include "DHI_APP/tranDef.h"
#include "CPHI/cphicommon.h"

typedef enum
{
	CP_A_SYSTEM_IND	= 0,
	CP_A_ROUTING_IND,			// 1: Routing Indicator
	CP_A_CLIENT_NUM,			// 2: Client Number
	CP_A_MERCHANT_NUM,			// 3: Merchant Number
	CP_A_TERMINAL_NUM,			// 4: Terminal Number
	CP_A_TXN_SQN_FLAG,			// 5: Txn Sequence Flag
	CP_A_SEQ_NUM,				// 6: Sequence Number
	CP_A_TXN_CLASS,				// 7: Txn Class
	CP_A_TXN_CODE,				// 8: Txn code
	CP_A_BATCH_NUM,				// 9: Batch Number
	CP_A_BATCH_SEQ_NUM,			// 10: Batch Sequence Number
	CP_A_BATCH_OFFSET,			// 11: Batch Offset
	CP_A_TRAN_COUNT,			// 12: Transaction Count
	CP_A_NET_AMT,				// 13: Net Amount
	CP_A_APP_NAME,              // 14: Application Name
	CP_A_RELEASE_DATE,          // 15: Release Date
	CP_A_EPROM_VER_NUM,			// 16: EPROM/Version number
	CP_A_FILLER,				// 17: Filler
	CP_A_PIN_CAPBLTY_CODE,		// 18: Pin Capability Code
	CP_A_DATA_ENTRY_SRC,		// 19: Data Entry Source
	CP_A_LAST_REF_NUM,			// 20: last retrieval reference number
	CP_A_DWNLD_STATUS,			// 21: Download Status
	CP_B_FULL_MAG_STRIPE_INFO,	// 22: Mag. Stripe
	CP_B_ACC_NUM,				// 23: Acc. Number
	CP_B_EXP_MMYY,				// 24: Expiry Month and Year
	CP_C_TXN_AMT,				// 25: Txn Amount
	CP_C_TXN_AMT_TOR,			// 26: Txn Ampunt for TOR
	CP_C_TXN_FILLER,			// 27: Txn Filler
	CP_D_MARKET_DATA_IND,		// 28: Market Data Indicator
	CP_D_DUKPT_KSN,				// 29: Key Serial Number
	CP_D_DUKPT_PIN_BLOCK,		// 30: PIN_BLOCK
	CP_D_CASHBACK_AMT,			// 31: Cash back Amount
	CP_F_INDUSTRY_CODE,			// 32: Industry Code
	CP_F_INVOICE_NUMBER,		// 33: Invoice Number
	CP_F_ITEM_CODE,				// 34: Item Code
	CP_F_EXT_TRANS_ID,			// 35: External Transaction Identifier
	CP_F_EMP_NUM,				// 36: Employee Number
	CP_F_CASHOUT_IND,			// 37: Cashout Indicator
	CP_F_SEQ_NUM_CARD,			// 38: Sequence Number of Cards being Issued
	CP_F_TOT_NUM_CARD,			// 39: Total Number of Cards being Issued
	CP_G_FIELD_SEP,				// 40: Field Separator Required in Miscellaneous info
	CP_G_AUTH_CODE,				// 41: Auth code
	CP_G_REF_NUM,				// 42: Ref. Num
	CP_G_EBT_TYPE,				// 43: EBT type
    CP_G_ACC_NUM,				// 44: Account Number for Void
	CP_H_CRDHLDR_ST_ADD,		// 45: Cardholder Street Address
	CP_H_EXTD_CRDHLDR_ST_ADD,	// 46: extended Cardholder Street Address
	CP_H_CARDHLDR_ZIP_CODE,		// 47: card holder zip code
	CP_I_CUST_REFERENCE_NUM,	// 48: customer reference number(invoice)
	CP_I_LOCAL_TAX_FLAG,		// 49: Local Tax Falg
	CP_I_PURCHASE_FILLER,		// 50: Purchase filler
	CP_I_DEST_ZIP,				// 51: Destination Zip
	CP_I_SALES_TAX_AMT,			// 52: Sales Tax Amount
	CP_J_TOKEN_START,			// 53: Token Start indicator
	CP_J_CUST_DATA,				// 54: Customer defined data
	CP_J_DUP_CHECK,				// 55: Duplicate Transaction Checking
	CP_J_CVV,					// 56: CVV
	CP_J_ENHANCED_AUTH,			// 57: enhanced authorization indicator
	CP_J_HEALTH_CARE,			// 58: health care details
	CP_J_POS_CAPBLTY1,			// 59: pos capability details 1
	CP_J_POS_CAPBLTY2,			// 60: pos capability details 2
	CP_J_PAN_ENCRYPT,			// 61: encryption details
	CP_J_REF_NUM,				// 62: pos retrival reference number
	CP_J_PYMNT_METHOD,			// 63: payment media
	CP_J_CREDIT_CASHBACK,       // 64: Credit Cash Back
	CP_J_INSTORE_FLAG,			// 64: Instore payment Flag
	CP_J_PIN_BLOCK,				// 65: Pin block
	CP_J_KEY_SERIAL_NUM,		// 66: key serial number
	CP_J_CARD_TOKEN,			// 67: card token
	CP_J_TRANS_CODE,			// 68: Reversal Advice Transaction Code Indicator
	CP_J_REVERSL_REASON,		// 69: Reversal Reason code indicator
	CP_J_APP_IDENTIFIER,		// 70: EMV Application Identifier
	CP_J_CARD_AUTH_RESLT_CODE,  // 71: Card authentication result code.
	CP_J_CHIP_CARD_DATA,		// 72: EMV Tags
	CP_J_PAN_SEQ_NUMBER,		// 73: EMV PAN sequence Number
	CP_J_TRAN_SEQ_COUNTER,		// 74: EMV Transaction Sequence Counter
	CP_J_SV_TRANS_INFO,			// 75:Third Party Stored Value Transaction Information
	CP_J_TOKEN_ALLOWED,			// 76: token allowed indicator
	CP_LRC,						// 77: LRC Value
	CP_MAX_REQ_ELEMENTS,		// 78: maximum number of request elements 	/* any field belongs to request place above this field */
	CP_TXN_CODE_FOR_REVERSAL,	// 79: transaction code used for original transaction
	CP_MAX_ELEMENTS,
}CP_FIELDS;


typedef enum
{
	CREDIT = 1,
	DEBIT,
	DEBIT_CASHBACK,
	STOREDVALUE,
	CHECK,
	EBT,
	MAX_PAYMENT_TYPE,
}
CPPAYMENT_TYPE;

typedef enum
{
	REQUEST_EMVTAGS = 1,
	RESPONSE_EMVTAGS
}
EMVPARSE_ENUMS;

extern int  setPymtnCmdValue(TRANDTLS_PTYPE);
extern int  getTransCode(char *);
extern int  fillCPReqDtls(TRANDTLS_PTYPE,CPTRANDTLS_PTYPE, char*);
extern int  fillCPDtlsForTOR(TRANDTLS_PTYPE,CPTRANDTLS_PTYPE);
extern int  fillCPReqDtlsforFollowOnTran(TRANDTLS_PTYPE,CPTRANDTLS_PTYPE);
extern int  fillCPDtlsForVSPReg(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
extern int  fillCPDtlsForTokenQuery(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
extern int  fillCPDtlsForBatchMgmt(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
extern int  fillCPDtlsforEMVParamDwnld(TRANDTLS_PTYPE,CPTRANDTLS_PTYPE);
extern int  updateCPConfigParams(char*);
extern void saveTranDetailsForFollowOnTxn(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE, CPTRANDTLS_PTYPE);

#endif
