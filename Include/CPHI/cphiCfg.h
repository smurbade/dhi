/*
 * cphiCfg.h
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */


#ifndef CPHICFG_H_
#define CPHICFG_H_
#include "cphiHostCfg.h"
#include "iniparser.h"

/* Section in the config file */
#define SECTION_REG 			"REG"
#define SECTION_DHI 			"DHI"
#define SECTION_CPHI			"CPHI"
#define SECTION_DCT				"DCT"
#define SECTION_HDT				"HDT"


/* CPHI Params in CPHI Section */

#define ROUTING_IND					"routingind"
#define CLIENT_NUM					"clientnum"
#define THIRDPARTYGIFT_ENABLED		"thirdpartygiftenabled"
#define TOKEN_MODE					"tokenmode"
#define TERMINAL_NUM				"tid"
#define MERCHANT_NUM				"mid"
#define LANE_ID						"laneid"
#define SW_IDENTIFIER				"swidentifier"
#define HW_IDENTIFIER				"hwidentifier"
#define PRIM_HOST_URL				"primurl"
#define PRIM_HOST_PORT				"primport"
#define SCND_HOST_URL				"scndurl"
#define SCND_HOST_PORT				"scndport"
#define PRIM_CONN_TO				"primcontimeout"
#define SCND_CONN_TO				"scndcontimeout"
#define PRIM_RESP_TO				"primrestimeout"
#define SCND_RESP_TO				"scndresptimeout"

/*DCT in CPHI Section*/
#define EMVENABLED					"emvenabled"
#define	CONTACTLESSENABLED			"contactlessenabled"
#define VSPENABLED					"vspenabled"
#define EMV_SETUP_REQ				"emvsetupreqd"


/* Under Reg Section*/
#define	TIME_ZONE				"timezone"

#define REG_HOST_URL			"reghosturl"
#define REG_HOST_PORT			"reghostport"
#define REG_CONN_TO				"regcontimeout"
#define REG_RESP_TO				"regresptimeout"


/* Keep Alive Params in HDT Section */
#define KEEP_ALIVE				"keepalive"
#define KEEP_ALIVE_INTERVAL		"keepaliveinterval"


#define VSP_ENABLED				"vspenabled"


#define DFLT_HOSTCONN_TIMEOUT	10
#define DFLT_HOSTRESP_TIMEOUT	10


/* Constant Parameter CPHI*/
#define STX					     0x02			// start of message
#define	ETX						 0x03			// end  of message
#define FS						 0x1C			// field separator
#define LF                       0x0A           // line feed
#define SYSTEM_IND				"L."   			// Host Capture Indicator
#define TXN_SQN_FLAG			"1"				//Single Transaction
#define TXN_CLASS				"F"				//Financial Transaction
#define PIN_CAPBLTY_CODE		"1"				//1-Terminal Device Accepts PIN Capability Code
#define TXN_FILLER		 		"00000000"  		//Filler Data
#define INDUSTRY_CODE			"004"			//Industry Code For Retail
#define SV_INDUSTRY_CODE		"014"					//Industry Code for Stored Value
#define ITEM_CODE				"00000000000000000000"	//Harded Coded to Zero's
#define EXT_TRANS_ID			"               "		//Hard Coded to spaces's
#define	SEQ_NUM_CARDS			"01"					//Sequence Number of Cards being issued
#define	TOTAL_NUM_CARDS			"01"					//Total Number of Cards being issued

typedef struct
{
		int					iKeepAlive;			// Implemented to keep the connection alive if needed
		int					iKeepAliveInt;		// Duration/Interval to keep the connection alive
		int					iNumUniqueHosts;
		int					iSequenceNum;
		int     			iTokenRefNum;
		int 				iCusDefData;
		int 				iLastRefNum;
		int					iemvEnabled;
		int					iContactessEnabled;
		int 				ivspenabled;
		int 				iThirdpartygiftenabled;
		int 				iTokenMode;
		char 				szRoutingInd[6+1];
		char				szClientNum[4+1];
		char 				szMerchantNum[12+1];
		char 				szTerminalNum[3+1];
		char				szLaneId[2+1];
		char				szUsername[32+1];
		char				szPassword[32+1];
		char				szSWIdentifier[4+1];
		char				szHWIdentifier[4+1];
		char				szP2TokenDetailsEncrypted[32+1];
		char				szP2TokenDetailsNonEncrypted[32+1];
	HOSTDEF_STRUCT_TYPE		hostDef[2];
}
CPHI_CFG_STYPE, *CPHI_CFG_PTYPE;

extern void getClientNum(char *);
extern void getTerminalNum(char *);
extern void getMerchantNum(char *);
extern void getConfigLaneId(char *);
extern void getRoutingInd(char *);
extern void getSystemInd(char *);
extern void getTxnClass(char *);
extern void getTxnSqnFlag(char *);
extern void getPinCapabCode(char *);
extern void getTransInfoFiller(char *, char*);
extern void getIndusCode(char *, char *);
extern void getItemCode(char *);
extern void getPosCapblty1Dtls(char *,char *);
extern void getUserName(char *);
extern void getPassword(char *);
extern void getSequencenum(char *);
extern void getCurrentSequenceNum(char *);
extern void getCurrentTknRefNum(char *);
extern void getCurrentCustDefData(TRANDTLS_PTYPE, char *);
extern void getTokenRefNum(char *);
extern void getCusDefData(char *);
extern void getSWIdentifier(char *);
extern void getHWIdentifier(char *);
extern void getLastRefNum(char *);
extern void getExtTransId(char*, char*);
extern void getCashOutInd(char*);
extern void getSeqNumCards(char*, char*);
extern void getTotalNumCards(char*, char*);
extern void getPosCapblty2Dtls(char *, TRANDTLS_PTYPE);
extern void putLastRefNum(char *);
extern void resetBatchParams();
extern int  getEmployeeNum(char* ,char*);
extern int  getKeepAliveParameter();
extern int  getKeepAliveInterval();
extern int  getCPHostDetails();
extern int  getNumUniqueHosts();
extern int  isThirdPartyGiftEnabled();
extern int  isTokenModeEnabled();
extern int  writeCPParamsToFile(char*);
extern char getDESValue(int, int);
extern char getTxnCode(int, int);

extern int checkHostConn();

#endif     /* CPHICFG_H_ */
