/* 
 * --------------------------------------------------------
 * -------------- CPHI RESPONSE FIELDS ------------------------
 * --------------------------------------------------------
 */
 
 #include "DHI_App/tranDef.h"
 #include "CPHI/cphiResp.h"
 
static CPHI_RESP_KEYVAL_STYPE genCPRespLst[] =
{	
	/*multiple tokens*/
	{"AT" , CP_R_TRANS_IDENTIFIER 	, NO_ENUM 			} ,
	{"B6" , CP_R_AVAL_BAL_1			, NO_ENUM			} ,
	{"B7" , CP_R_AVAL_BAL_2 		, NO_ENUM			} ,
	{"CP" , CP_R_PREPAID  			, NO_ENUM			} ,
	{"CV" , CP_R_ENHANCED_CARD 		, NO_ENUM			} ,
	{"ER" , CP_R_ERR_RESP 			, NO_ENUM			} ,
	{"SR" , CP_R_SEMTEK_CODE 		, NO_ENUM			} ,
	{"Q8" , CP_R_ENHANCED_AUTH_RESP , NO_ENUM			} ,
	{"RN" , CP_R_POS_RET_REF_NUM	, NO_ENUM			} ,
	{"TA" , CP_R_TOKEN_DATA			, NO_ENUM			} ,
	{"TK" , CP_R_CUSTOMER_PAYMENT	, NO_ENUM			} ,
	
	/*credit */
	{"AC" , CP_R_AMEX  				, eCVV2_CODE 		} ,
	{"CM" , CP_R_CVV   				, NO_ENUM			} ,		
	{"DA",  CP_R_DISCOVER_AUTH  	, NO_ENUM			} ,
	{"DC" , CP_R_DYNA_CURR_CON   	, NO_ENUM			} ,
	{"PD" , CP_R_AUTH_POS_DATA  	, NO_ENUM			} ,
	{"VC" , CP_R_CHASE_VISA_RESU    , NO_ENUM			} ,
	{"VS" , CP_R_SPEND_RESU_CODE  	, NO_ENUM			} ,
	
	/*stored value*/
	{"B1" , CP_R_CURR_BAL_INFO   	, eAMOUNT_BALANCE	} ,
	{"B3" , CP_R_PREV_APP_AMT  	 	, ePREVIOUS_BALANCE	} ,
	{"B4" , CP_R_APPV_AMT   		, eAPPROVED_AMOUNT	} ,
	{"B5",  CP_R_CASH_OUT_AMT  		, NO_ENUM			} ,
	{"FB" , CP_R_FAILED_BLOCK   	, NO_ENUM			} ,
	
	/*check*/
	{"EC" , CP_R_ECP_RESP_CODE 		, eCHECK_TYPE		} ,
	
	/*batch*/
	{"MD" , CP_R_DATA_FLAG   		, NO_ENUM			} ,
	{"TT" , CP_R_TOTALS_CARD_TYPE   , NO_ENUM			} ,
	{"TD" , CP_R_TRANS_DET   		, NO_ENUM			} ,
	

	/*EMV*/
	{"CR" , CP_R_CARD_AUTH			, NO_ENUM			} ,
	{"DL" , CP_R_DOWNLOAD_IND		, NO_ENUM			} ,
	{"DS" , CP_R_PARA_DOWNLOAD_STAT	, NO_ENUM			} ,
	{"EM" ,	CP_R_CHIP_CARD_DATA		, NO_ENUM			} ,
	
	/* CARD TOKEN*/
	{"T4", CP_R_TOKEN_ID 			, NO_ENUM			},
};
