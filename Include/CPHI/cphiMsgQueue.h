
/*
 * cphiMsgQueue.h
 *
 *  Created on: 30-June-2015
 *      Author: BLR_SCA_TEAM
 */
#ifndef CPHIMSGQUEUE_H_
#define CPHIMSGQUEUE_H_


extern int 		displayMsgQueueInfo(unsigned);
extern void 	flushMsgQueue(unsigned);
extern unsigned	createMsgQueue(key_t);

#endif /* CPHIMSGQUEUE_H_ */
