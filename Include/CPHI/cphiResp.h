/*
 * cphiResp.h
 *
 *  Created on: 03-July-2015
 *      Author: BLR_SCA_TEAM
 */

#ifndef CPHIRESP_H_
#define CPHIRESP_H_

typedef struct
{
	char *			key;
	int			 	iCPIndex;
	int			 	iDHIIndex;
}
CPHI_RESP_KEYVAL_STYPE, * CPHI_RESP_KEYVAL_PTYPE;

#define CPHI_RESP_KEYVAL_SIZE	sizeof(CPHI_RESP_KEYVAL_STYPE)

typedef enum
{
	CP_ACTION_CODE	=	0,
	CP_ADDR_VER_RESP_CODE,		// 1:Address Verification Response Code
	CP_AUTH_ERROR_CODE,			// 2:Authorization/Error Code
	CP_BATCH_NUMBER,			// 3:Batch Number
	CP_RET_REF_NUM,				// 4:Reterival Reference Number
	CP_SEQ_NUM,					// 5:Sequence Number
	CP_RESP_MSG,				// 6:Response Message
	CP_CARD_TYPE,				// 7:Card Type
	CP_INTER_COMPLI,			// 8:Interchange Compliance
	CP_AUTH_NETW_ID,			// 9:Authorization Network ID
	CP_AUTH_SOURCE,				// 10:Authorization Source
	CP_OPTIONAL_DATA,			// 11:Optional Data

	CP_TRACE_NUM,				// 12:Trace Number
	CP_SURCH_FEE_AMT,			// 13:Surcharge Fee Amount


	CP_FS_LEG_BAL,				// 14:FS Ledger Balance
	CP_FS_AVAL_BAL,				// 15:FS Avaliable Balance
	CP_FS_BEGI_BAL,				// 16:Begining Balance
	CP_CASH_LEG_BAL,			// 17:Cash Ledger Balance
	CP_CASH_AVAL_BAL,			// 18:Cash Avaliable Balance
	CP_CASH_BEGI_BAL,			// 19:Cash Begining Balance
	CP_MERCH_CONTROL_IND,		// 20:Merchant Control Indicator
	CP_DOWNLD_FLG,				// 21:Download Flag
	CP_MUL_MSG_FLG,				// 22:Multi Message Flag
	CP_BATCH_OPEN_DATE_TIME,	// 23:Batch Open Date/Time
	CP_BATCH_CLOSE_DATE_TIME,	// 24:Batch Close Date/Time
	CP_BATCH_TRAN_COUNT,		// 25:Batch Transaction Count
	CP_BATCH_NET_AMT,			// 26:Batch Net Amount
	CP_ADDTL_DATA,				// 27:Additional Data

	//tokens
	//mutiple tokens
	CP_R_TRANS_IDENTIFIER,		// 28:Transaction Identifier(AT)
	CP_R_AVAL_BAL_1,			// 29:Avaliable Balance 1(B6)
	CP_R_AVAL_BAL_2,			// 30:Avaliable Balance 2(B7)
	CP_R_PREPAID,				// 31:Prepaid(CP)
	CP_R_ENHANCED_CARD,			// 32:Enhanced Card Verification Data(CV)
	CP_R_ERR_RESP,				// 33:Error Response Token(ER)
	CP_R_PAN_ENCRYP,			// 34:PAN Encryption(PE)
	CP_R_SEMTEK_CODE,			// 35:Semtek Code(SR)
	CP_R_ENHANCED_AUTH_RESP,	// 36:Enhanced Authorization Response(Q8)
	CP_R_PARTIAL_PREAUTH,		// 37:Partial Pre-Authorization(PA)
	CP_R_POS_RET_REF_NUM,		// 38:POS Reterival Refernce Number(RN)
	CP_R_TOKEN_DATA,			// 39:Token Data(TA)
	CP_R_CUSTOMER_PAYMENT,		// 40:Customer Digital Payment Token Response Data(TK)

	//check tokens
	CP_R_ECP_RESP_CODE,			// 41:ECP Response Reason Code(EC)

	//credit token
	CP_R_AMEX,					// 42:American Express CID(AC)
	CP_R_CRE_CASH_BACK,			// 43:Credit Cash Back/Cash Over(CB)
	CP_R_CVV,					// 44:ChaseNet and Visa CVV Results Code(CM)
	CP_R_DISCOVER_AUTH,			// 45:Discover Authoraization Data(DA)
	CP_R_DYNA_CURR_CON,			// 46:Dynamic Currecncy Code(DC)
	CP_R_AUTH_POS_DATA,			// 47:Authorization POS Data Code(PD)
	CP_R_CHASE_VISA_RESU,		// 48:ChaseNet and Visa Card Level Results Indicator(VC)
	CP_R_SPEND_RESU_CODE,		// 49:Spend Qualified Results Code Indicator(VS)

	//EMV token
	CP_R_CARD_AUTH,
	CP_R_DOWNLOAD_IND,
	CP_R_PARA_DOWNLOAD_STAT,
	CP_R_CHIP_CARD_DATA,
	CP_R_FALL_BACK_IND,
	CP_R_OFFLINE_FLOOR_LIM,
	CP_R_PUBLIC_KEY,

	//Batch
	CP_R_DATA_FLAG,
	CP_R_TOTALS_CARD_TYPE,
	CP_R_TRANS_DET,

	//SAF token
	CP_R_TRANS_IND,
	CP_R_TRACE_AUTH_IND,
	CP_R_SUPP_DATA,

	//Stored Value Token
	CP_R_CURR_BAL_INFO,
	CP_R_PREV_APP_AMT,
	CP_R_APPV_AMT,
	CP_R_CASH_OUT_AMT,
	CP_R_FAILED_BLOCK,

	//device
	CP_SR_TOKEN,

	//Token
	CP_R_TOKEN_ID,

	CP_R_LRC,
	CP_MAX_RESP_FIELDS
}CP_RESP_FIELDS;

typedef struct __stPaymentTypeNode
{
	char					szPayType[4];
	char					szTransCnt[10];
	char					szNetAmt[20];
	struct __stPaymentTypeNode* next;
}
VAL_PAYMENTTYPENODE_STYPE, * VAL_PAYMENTTYPENODE_PTYPE;
#define HOSTTOTALNODE_SIZE sizeof(VAL_HOSTTOTALNODE_STYPE)

typedef struct __stPaymentTypeHeadNode
{
	VAL_PAYMENTTYPENODE_PTYPE pstHead;
	int iMaxHostTotalGroup;
	char szNetSettlemntAmt[256];
}
VAL_PAYMENTTYPEHEADNODE_STYPE, * VAL_PAYMENTTYPEHEADNODE_PTYPE;

extern int parseCPResponse(char *, TRANDTLS_PTYPE , CPTRANDTLS_PTYPE , int);
extern int parseCreditDetails(TRANDTLS_PTYPE ,CPTRANDTLS_PTYPE, char**);
extern int parseDebitDetails( TRANDTLS_PTYPE ,CPTRANDTLS_PTYPE,char**);
extern int parseStoredValueDetails(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE,char**);
extern int parseEBTDetails(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE ,char**);
extern int parseCheckDetails(TRANDTLS_PTYPE, CPTRANDTLS_PTYPE, char**);

extern int parseTokenDetails(TRANDTLS_PTYPE , CPTRANDTLS_PTYPE ,char**);
extern int frameSSIRespForReportCommand(TRANDTLS_PTYPE, VAL_PAYMENTTYPEHEADNODE_PTYPE);
extern int correctDHIResponseFields(TRANDTLS_PTYPE pstDHIReqResFields);
extern int parseCPResponseForBatchMgmt(char *, TRANDTLS_PTYPE, CPTRANDTLS_PTYPE ,VAL_PAYMENTTYPEHEADNODE_PTYPE );
extern void fillErrorDetails(int, TRANDTLS_PTYPE, char*);
extern int frameSSIRespForEMVAdmin(TRANDTLS_PTYPE);
extern int parseCPEMVDwldResp(char *, TRANDTLS_PTYPE , CPTRANDTLS_PTYPE);

#endif /* CPHIRESP_H_ */
