

#ifndef UTILS_H_
#define UTILS_H_

extern void acquireCPMutexLock(pthread_mutex_t * , const char * );
extern void releaseCPMutexLock(pthread_mutex_t * , const char * );
extern uchar findLrc(char *, int);

extern int doesFileExist(const char *);
#endif /* UTILS_H_ */
