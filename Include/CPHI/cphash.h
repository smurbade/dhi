/*
 * cphash.h
 *
 *  Created on: 14-July-2015
 *      Author: BLR_SCA_TEAM
 */

#ifndef CPHASH_H_
#define CPHASH_H_

#define HASHSIZE 50
typedef struct __stHash /* table entry: */
{
	struct __stHash * next;     		/* Next entry in chain */
    char 		    *pszTagName;       	/* Defined Tag Name */
    int  			iTagIndexVal;    	/* Index Value of the Tag */
}
HASHLST_STYPE, * HASHLST_PTYPE;

extern HASHLST_PTYPE addEntryToCPHashTable(char *, int );
extern HASHLST_PTYPE lookupCPHashTable(char *);

#endif /* CPHASH_H_ */
