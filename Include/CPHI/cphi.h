
#ifndef CPHI_H_
#define CPHI_H_

extern int initHILib();
extern int postDataToCPHost(char *, int, char **, int *);
//extern int processDeviceRegistration(TRANDTLS_PTYPE);
extern int parseCPResponseForDevAndVSPReg(char *, TRANDTLS_PTYPE, CPTRANDTLS_PTYPE);
extern void retryTimeoutReversal(char*, int);
extern int 	createTimeoutReversalThread();

#endif /* CPHI_H_ */
