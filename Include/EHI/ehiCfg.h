/*
 * ehiCfg.h
 *
 *  Created on: 02-March-2016
 *      Author: BLR_SCA_TEAM
 */


#ifndef EHICFG_H_
#define EHICFG_H_
#include "ehiHostCfg.h"

/* Section in the config file */
#define SECTION_REG 			"REG"
#define SECTION_DHI 			"DHI"
#define SECTION_EHI			    "EHI"
#define SECTION_DCT				"DCT"
#define SECTION_HDT				"HDT"
#define SECTION_ELAVONREQUEST	"ELAVONREQUEST"
#define SECTION_ELAVONRESPONSE	"ELAVONRESPONSE"


/* EHI Params in EHI Section */

#define TERMINAL_ID 				"tid"
#define LANE_ID						"laneid"
#define LOCATION_NAME				"location"
#define CHAIN_CODE					"chaincode"
#define TERM_CURRENCY_TRIGRAPH		"termcurncytrigraph"
#define PRIM_HOST_URL				"primurl"
#define PRIM_HOST_PORT				"primport"
#define SCND_HOST_URL				"scndurl"
#define SCND_HOST_PORT				"scndport"
#define PRIM_CONN_TO				"primcontimeout"
#define SCND_CONN_TO				"scndcontimeout"
#define PRIM_RESP_TO				"primrestimeout"
#define SCND_RESP_TO				"scndresptimeout"


#define EMVENABLED					"emvenabled"
#define	CONTACTLESSENABLED			"contactlessenabled"
#define	EMVCONTACTLESSENABLED		"contactlessemvenabled"


/* Keep Alive Params in HDT Section */
#define KEEP_ALIVE				"keepalive"
#define KEEP_ALIVE_INTERVAL		"keepaliveinterval"


#define DFLT_HOSTCONN_TIMEOUT	10
#define DFLT_HOSTRESP_TIMEOUT	10


#define MAX_NUM_PAYMENT_TYPES   6
#define MAX_NUM_COMMANDS        18

typedef struct
{
	int						iKeepAlive;				// Implemented to keep the connection alive if needed
	int						iKeepAliveInt;			// Duration/Interval to keep the connection alive
	int						iNumUniqueHosts;
	int 					iEmvEnabled;
	int 					iContactlessEnabled;
	int 					iEmvContactlessEnabled;
	int 					iTimeoutFromPOS;
	char					szLaneId[8+1];
	char 					szTerminalId[10+1];
	char					szChainCode[15+1];
	char					szLocationName[16+1];
	char					szTermCurrencyTriGraph[3+1];
	unsigned long			lRefNum;
	char					szBatchNum[10+1];
	char					sz47Fields1234[8+1];
	char					sz47Fields11121314[8+1];

	HOSTDEF_STRUCT_TYPE		hostDef[2];
}
EHI_CFG_STYPE, *EHI_CFG_PTYPE;


extern int	loadElavonPTFields();
extern ELAVON_PASSTHROUGH_PTYPE getElavonPassThroughDtls();
extern int  getKeepAliveParameter();
extern int  getKeepAliveInterval();
extern int 	isEmvEnabled();
extern int 	isContactlessEnabled();
extern int 	isEmvContactlessEnabled();
extern int  getNumUniqueHosts();
extern int	checkHostConn(char *, int, char **, int *);
extern int  getElavonHostDetails();
extern int  writeElavonBatchNumToFile(char *);
extern int  getTimeoutValue();
//extern char getTxnType(int, int);
extern void getTermCurrencyCode(char *);
extern void getTransRefNum(char *);
extern void getUpdatedRefNum(char *);
extern void resetElavonBatchParams();
extern void getTerminalId(char *);
extern void getLaneId(char *);
extern void getLocationName(char *);
extern void getChainCode(char *);
extern void getElavonBatchNum(char *);
extern void updateTimeOutValue(int);
extern void resetTimeOutValue();
extern void updateElavonBatchNum(char *);
extern void get47APIField1234(char *);
extern void get47APIField11121314(char *);
extern void updateTerminalId(char *);
extern void updateChainCode(char *);
extern void updateLocationName(char *);

#endif     /* CPHICFG_H_ */
