
/* 
 * --------------------------------------------------------
 * -------------- EMV TAGS ------------------------
 * --------------------------------------------------------
 */
 
 #include "EHI/eGroups.h"
 
static EHI_EMV_KEYVAL_STYPE EmvTagLst[] =
{
	{ "5F57"			, E_DEBIT_ACCT_TYPE						, EHI_FALSE},
	{ "9F3C"			, E_TERM_CURRENCY_CODE_9F3C				, EHI_FALSE},
	{ "5F2D"			, E_PREFERRED_LANG_5F2D					, EHI_TRUE },
	{ "9F26"			, E_APPL_CRYPTGRAM_9F26					, EHI_FALSE},
	{ "91"				, E_ISSUER_AUTH_DATA_91					, EHI_FALSE},
	{ "5F24"			, E_APPL_EXP_DATE_5F24					, EHI_FALSE},
	{ "9F34"			, E_CVM_REULTS_9F34						, EHI_FALSE},
	{ "9F1E"			, E_DEVICE_SERIAL_NUM_9F1E				, EHI_TRUE },
	{ "9F10"			, E_ISSUER_APPL_DATA_9F10				, EHI_FALSE},
	{ "9F33"			, E_TERM_CAPBLTY_9F33					, EHI_FALSE},
	{ "95"				, E_TVR_95								, EHI_FALSE},
	{ "9F42"			, E_APPL_CURR_CODE_9F42					, EHI_FALSE},
	{ "9F1A"			, E_TERM_CONTRY_CODE_9F1A				, EHI_FALSE},
	{ "5F34"			, E_APPL_SEQ_NUM_5F34					, EHI_FALSE},
	{ "84"				, E_DED_FILE_NAME_84					, EHI_FALSE},
	{ "9F08"			, E_ICC_APPL_VERSON_NUM_9F08			, EHI_FALSE},
	{ "9F1C"			, E_EMV_DEVICE_ID_9F1C					, EHI_TRUE },
	{ "9F09"			, E_TERM_APPL_VERSON_NUM_9F09			, EHI_FALSE},
	{ "9F41"			, E_TRAN_SEQ_COUNTER_9F41				, EHI_FALSE},
	{ "82"				, E_APPL_INTERCHNG_PROFILE_82			, EHI_FALSE},
	{ "9F36"			, E_APPL_TRAN_COUNTER_9F36				, EHI_FALSE},
	{ "9F27"			, E_CRYPT_INFO_DATA_9F27				, EHI_FALSE},
	{ "9F35"			, E_TERM_TYPE_9F35						, EHI_FALSE},
	{ "9F37"			, E_UNPREDICT_NUM_9F37					, EHI_FALSE},
	{ "4F"				, E_ICC_APP_IDNTIFIER_4F				, EHI_FALSE},
	{ "9F12"			, E_ICC_APPL_PREFRD_NAME_9F12			, EHI_TRUE },
	{ "50"				, E_APPL_LABEL_50						, EHI_TRUE },
	{ "9F06"			, E_TERM_APP_IDENTIFIER_9F06			, EHI_FALSE},
	{ "9F0D"			, E_DEFLT_ISSUER_ACTN_CODE_9F0D			, EHI_FALSE},
	{ "9F0E"			, E_DENAIL_ISSUER_ACTN_CODE_9F0E		, EHI_FALSE},
	{ "9F0F"			, E_ONLINE_ISSUER_ACTN_CODE_9F0F		, EHI_FALSE},
	{ "9F07"			, E_APPL_USAGE_CONTROL_9F07				, EHI_FALSE},
	{ "9B"				, E_TRANSACTION_STATU_INFO_9B			, EHI_FALSE},
	{ "DF04"			, E_SMART_CARD_SCHEME_DF04				, EHI_FALSE},
	{ "8A"				, E_EMV_RESP_CODE_8A					, EHI_FALSE},
	{ "9F6E"			, E_ICC_FORM_FACT_IND_9F6E				, EHI_FALSE},
	{ "9F7C"			, E_ICC_CUST_DATA_9F7C					, EHI_FALSE},
	{ "5F2A"			, E_TRAN_CUUR_CODE_5F2A					, EHI_FALSE},
	{ "9C"				, E_CRYPT_TRAN_TYPE_9C					, EHI_FALSE},
	{ "5F56"			, E_ISSUER_COUNTRY_CODE_5F56			, EHI_TRUE }
};

