/*
 * ehicommon.h
 *
 *  Created on: 02-March-2015
 *      Author: BLR_SCA_TEAM
 */

#ifndef EHICOMMOM_H_
#define EHICOMMOM_H_

#include "DHI_App/tranDef.h"

/* Application Logging #defines */

// App Entry for Transactional App Logging

#define	APP_NAME "EHI"

#define	SIG_KA_TIME SIGRTMIN+2

typedef unsigned long long ullong;
typedef unsigned char 		uchar;


#define SUCCESS 		0
#define FAILURE		-1403

#define ELAVON_BATCH_NUM_FILE		 "./flash/ElavonBatchNum.txt"
#define ELAVON_TRAN_RECORDS			 "./flash/ElavontranRecords.txt"

// Entry Tyrp for Transactional App Logging
#define ENTRYTYPE_INFO 		"INFO"
#define ENTRYTYPE_ERROR 	"ERROR"
#define ENTRYTYPE_FAILURE 	"FAILURE"
#define ENTRYTYPE_WARNING 	"WARNING"
#define ENTRYTYPE_SUCCESS 	"SUCCESS"

// App Entry Id for Transactional App Logging
#define ENTRYID_START_UP			    "START_UP"
#define	ENTRYID_START					"START"
#define	ENTRYID_COMMAND_END				"END"
#define ENTRYID_RECEIVE					"RECEIVE"
#define ENTRYID_PROCESSING				"PROCESSING"
#define ENTRYID_PROCESSED				"PROCESSED"
#define ENTRYID_SENT					"SENT"
#define ENTRYID_PROCESSING				"PROCESSING"
#define ENTRYID_ELAVON_REQUEST			"ELAVON_REQUEST"
#define ENTRYID_ELAVON_RESPONSE			"ELAVON_RESPONSE"
#define ENTRYID_RESPONDED				"RESPONDED"

/* ERROR CONSTANTS (NEED TO CHANGE) */

#define ERR_UNSUPP_CMD				-101
#define ERR_SYSTEM					-102
#define ERR_RESP_TO					-103
#define ERR_CONN_TO					-104
#define ERR_CONN_FAIL				-105
#define ERR_CFG_PARAMS				-106
#define ERR_INV_COMBI				-107
#define ERR_FLD_REQD				-108
#define ERR_INV_FLD					-109
#define ERR_NO_FLD					-110
#define ERR_INV_FLD_VAL				-111
#define ERR_INV_RESP				-112
#define ERR_HOST_ERROR				-113
#define ERR_HOST_TRAN_REVERSED		-114
#define ERR_HOST_TRAN_TIMEOUT 		-115
#define ERR_HOST_NOT_AVAILABLE 		-116
#define ERR_INV_CTROUTD				-117
#define	ERR_DO_VSP_REG				-118
#define	ERR_HASH_LOOKUP_FAILED		-119
#define	ELAVON_ADD_TO_PASSTHROUGH	-120

/*ELAVON GATEWAY RESPONSE CODE*/

#define		ELAVON_APPROVED						0
#define		ELAVON_DECLINED						60
#define		ELAVON_EMPTY_BATCH					22
#define		ELAVON_CANNOT_READ					37
#define		ELAVON_BAD_ACCT_NUM					41
#define		ELAVON_BAD_TRAN_AMT					43
#define		ELAVON_BAD_AUTH_AMT					44
#define		ELAVON_BAD_CARD INFO				46
#define		ELAVON_BAD_CARD_TYPE				47
#define		ELAVON_INVALID_DATE_ENTERED			48
#define		ELAVON_BAD_EXPIRATION				49
#define		ELAVON_INVALID_MAGSTRIPE			53
#define		ELAVON_BAD_REF_NBR					54
#define		ELAVON_INVALID_TERM_ID				57
#define		ELAVON_INVALID_TRAN_CODE			59
#define		ELAVON_MISSING_REQ_FIELD			67
#define		ELAVON_SWITCH_TIMEOUT				88
#define		ELAVON_ERROR                		99
#define		ELAVON_ETE_SWITCH_LINE_DOWN			158
#define		ELAVON_ETE_SERVICE_TERMINAL_ISSUE	160
#define		ELAVON_E2E_SERVICE_TIMEOUT			166
#define		ELAVON_TRAN_NOT_ALLOWED				173
#define		ELAVON_HOST_NOT_FOUND				182
#define		ELAVON_CARD_DATA_NOT_ENCRYPTED		197
#define		ELAVON_INVALID_REQUEST				237
#define		ELAVON_TOKEN_NOT_FOUND				244
#define		ELAVON_VOLTAGE_ERROR				280
#define		ELAVON_VERIFONE_ERROR				281
#define		ELAVON_TOKEN_VAULT_ERROR			282
#define		ELAVON_SAFETY_ERROR					283
#define		ELAVON_VERFONE_COMPLETE				285
#define		ELAVON_CARD_NOT_ENCRYPT				286
#define		ELAVON_VOLTAGE_SPECIFIC_ERROR_TEXT	1004
#define		ELAVON_VOLTAGE_SPECIFIC_ERROR_CODE	1009


#define		ELAVON_DUPLICATE					-5
#define		ELAVON_AMOUNT_TOO_LARGE				-16
#define		ELAVON_NO_RECORDS_FOUND				-7


#define EHI_LIB_VERSION			"V_1.00.00"
#define EHI_BUILD_NUMBER  		"5"
#define EHI_RELEASE_DATE   		"082916"			//MMDDYY

#ifdef DEBUG
	extern char chEHIDebug;
	extern char chEHIMemDebug;
	#include "tcpipdbg.h"
	//#define debug_sprintf(...) sprintf(__VA_ARGS__)
	#define debug_sprintf(...) \
			{ \
		if (chEHIDebug != NO_DEBUG) \
		{ \
			sprintf(__VA_ARGS__) ; \
		} \
			}
	#define debug_mem_sprintf(...) \
			{ \
		if (chEHIMemDebug != NO_DEBUG) \
		{ \
			sprintf(__VA_ARGS__) ; \
		} \
			}

	extern void setupEHIDebug(int *);
	extern void setupEHIMemDebug(int *);
	extern void closeupDebug(void);
	extern void closeupMemDebug(void);
	extern void EHI_LIB_TRACE(char *pszMessage);
	extern void EHI_LIB_MEM_TRACE(char *pszMessage);
	extern void EHI_LIB_TRACE_EX(char *pszMessage, int iLen);
	extern int setEHIDebugParamsToEnv(int * );
#else
	#define debug_sprintf(...) ;
	#define debug_mem_sprintf(...);
	#define EHI_LIB_TRACE(s) ;
	#define EHI_LIB_MEM_TRACE(s) ;
	#define EHI_LIB_TRACE_EX(s, i) ;
#endif

#define DEVDEBUGMSG 	"DEVDEBUGMSG"
#define EHI_DEBUG		"EHI_DEBUG"
#define EHI_MEM_DEBUG   "EHI_MEM_DEBUG"

#define MAX_DEBUG_MSG_SIZE			15+4096+2+1
#define MAX_SIZE					768
#define	MAX_PASSTHROUGH_SIZE		1024

#define CR      0x0D
#define LF      0x0A
#define EOT     0x04


extern void getHILibVersion(char *, char *);

typedef enum
{
	NO_ENUM = -1,
	EHI_FALSE,
	EHI_TRUE
}
EHI_BOOL;

//typedef enum
//{
//	SINGLETON = 1,
//	STRING,
//	AMOUNT,
//	BOOLEAN,
//	TIME,
//	DATE,
//	NUMERICS,
//	LIST,
//	MULTILIST,
//	VARLIST,
//	RECLIST,
//	ATTR_LKDLST,
//	MASK_NUM_STR,
//	NULL_ADD,
//	KEYLIST
//}
//VAL_TYPE_ENUM;

typedef struct
{
	char *			key;
	int			 	iEhiIndex;
	int			 	iDHIIndex;
	int				iPassthrough;
}
EHI_RESP_KEYVAL_STYPE, * EHI_RESP_KEYVAL_PTYPE;

#define EHI_RESP_KEYVAL_SIZE	sizeof(EHI_RESP_KEYVAL_STYPE)

typedef struct
{
	char *			key;
	int				iIndex;
	int				iSource;
	void *			value;
	EHI_BOOL		isMand;
}
ELAVON_KEYVAL_STYPE, * ELAVON_KEYVAL_PTYPE;

#define ELAVON_KEYVAL_SIZE	sizeof(ELAVON_KEYVAL_STYPE)

#define ELAVON_PASSTHROUGH_FILE_PATH "./flash/dhiPassThrgFields.INI"

typedef struct
{
	int						iReqAPICnt;
	int						iRespAPICnt;
	ELAVON_KEYVAL_PTYPE		pstPTAPIReqDtls;
	EHI_RESP_KEYVAL_PTYPE	pstPTAPIRespDtls;
}
ELAVON_PASSTHROUGH_STYPE, *ELAVON_PASSTHROUGH_PTYPE;

typedef struct
{
	int						iMandCnt;
	int						iTotCnt;
	ELAVON_KEYVAL_PTYPE		keyValList;
}
METADATA_STYPE, * METADATA_PTYPE;

typedef enum
{
	E_AUTH = 0,
	E_SALE,
	E_COMPLETION,
	E_POST_AUTH,
	E_VOICE_AUTH,
	E_RETURN,
	E_VOID,
	E_ACTIVATE,
	E_REDEMPTION,
	E_ADDVALUE,
	E_BALANCE,
	E_GIFT_CLOSE,
	E_DEACTIVATE,
	E_SETTLE_SUMMARY,
	E_CUTOVER,
	E_TOKEN_QUERY,
	E_TRANS_INQUIRY,
	E_FULL_REVERSAL,
	MAX_COMMANDS
}
COMMAND_ENUM;

typedef struct
{
	int iPaymentType;
	int iCommand;
}
ELAVONPYMTNCMD_STYPE, * ELAVONPYMTNCMD_PTYPE;

typedef struct
{
	char elementValue[768];
}
ELAVONTRANDTLS_STYPE, * ELAVONTRANDTLS_PTYPE;

extern ELAVON_PASSTHROUGH_PTYPE getElavonPassThroughDtls();
extern int 	loadEHIConfigParams();
extern int 	initEHIFace();
extern int	SaveDebugLogs();
extern int	cleanCURLHandle(EHI_BOOL);
extern void Char2Hex(char *, char *, int );
extern int  updateElavonConfigParams(char *);

extern void *ehiMalloc(unsigned int, int, char* );
extern char *ehistrdup(char*, int , char *);
extern char *ehistrndup(char *, unsigned int , int, char * );
extern void *ehiReAlloc(void *, unsigned int, int, char*);
extern void  ehiFree(void **, int, char*);

#endif /* EHICOMMOM_H_ */
