/*
 * ehiRequest.h
 *
 *  Created on: 02-March-2016
 *      Author: BLR_SCA_TEAM
 */

#ifndef EHIREQUEST_H_
#define EHIREQUEST_H_

extern int  getTransType();
extern int	setCommandForVSPReg(int);
extern int  setPymtnCmdValue(TRANDTLS_PTYPE);
extern int	postDataToElavonHost(char *, int, char **, int *);
extern int	frameElavonRequest(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE, char **);
extern int	frameElavonRequestForChkHostConn(TRANDTLS_PTYPE , ELAVONTRANDTLS_PTYPE , char ** );
extern int 	fillElavonDtls(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE, char *);
extern int 	fillElavonDtlsForBatchMgmt(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
extern int 	fillElavonDtlsForTokenQuery(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE, char *);
extern int  fillElavonReqDtlsforFollowOnTran(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE, char *);
extern void saveTxnDetailsForFollowOnTxn(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
extern int 	fillElavonDtlsForReportCommand(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
extern int 	fillElavonDtlsForTransInquiry(TRANDTLS_PTYPE, ELAVONTRANDTLS_PTYPE);
extern int	checkForVSPReg(TRANDTLS_PTYPE );

#endif /* EHIREQUEST_H_ */
