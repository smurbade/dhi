/*
 * elavonhash.h
 *
 *  Created on: 09-March-2016
 *      Author: BLR_SCA_TEAM
 */

#ifndef HASH_H_
#define HASH_H_

#define HASHSIZE 		135			// needs to be changed
#define	EMVHASHSIZE		35			// needs to be changed

typedef struct __stHash /* table entry: */
{
	struct __stHash * next;     		/* Next entry in chain */
    char 		    *pszTagName;       	/* Defined Tag Name */
    int  			iTagIndexVal;    	/* Index Value of the Tag */
}
HASHLST_STYPE, * HASHLST_PTYPE;

extern HASHLST_PTYPE addEntryToElavonHashTable(char *, int );
extern HASHLST_PTYPE addEntryToEmvHashTable(char *, int );
extern HASHLST_PTYPE lookupElavonHashTable(char *);
extern HASHLST_PTYPE lookupEmvHashTable(char *);

#endif /* HASH_H_ */
