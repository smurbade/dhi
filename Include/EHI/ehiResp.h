/*
 * ehiResp.h
 *
 *  Created on: 09-March-2016
 *      Author: BLR_SCA_TEAM
 */

#ifndef EHIRESP_H_
#define EHIRESP_H_

extern int		parseElavonResponse(char *, TRANDTLS_PTYPE , ELAVONTRANDTLS_PTYPE);
extern int		frameSSIRespForElavonReportCommand(int iCmd, TRANDTLS_PTYPE , ELAVONTRANDTLS_PTYPE);
extern int 		mapElavonResultCodeToDHI(TRANDTLS_PTYPE , ELAVONTRANDTLS_PTYPE);
extern void		fillErrorDetails(int, ELAVONTRANDTLS_PTYPE, TRANDTLS_PTYPE, char*);
extern void 	saveLocatorValues(TRANDTLS_PTYPE );
extern int 		fillCardTypeAPI1000(ELAVONTRANDTLS_PTYPE, TRANDTLS_PTYPE );

#endif /* EHIRESP_H_ */
