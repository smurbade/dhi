#ifndef __CSV_H
#define __CSV_H

char *csvgetline(FILE *fin);
char *csvfield(int n);
int csvnfield(void);

#endif // __CSV_H
