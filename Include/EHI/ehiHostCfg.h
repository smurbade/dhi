/*
 * ehiHostCfg.h
 *
 *  Created on: 02-March-2016
 *      Author: BLR_SCA_TEAM
 */


#ifndef EHIHOSTCFG_H_
#define EHIHOSTCFG_H_

typedef struct
{
	char *		url;
	int			portNum;
	int			conTimeOut;
	int			respTimeOut;
}
HOSTDEF_STRUCT_TYPE, * HOSTDEF_PTR_TYPE;


typedef struct
{
	char *szMsg;
	int iWritten;
	int iTotSize;
}
RESP_DATA_STYPE, *RESP_DATA_PTYPE;

typedef enum
{
	PRIMARY_URL = 0,
	SECONDARY_URL
}
HOST_URL_ENUM;

#define DFLT_KEEP_ALIVE_INTERVAL	10
#define KEEP_ALIVE_DISABLED			0
#define KEEP_ALIVE_PERMANENT		1
#define KEEP_ALIVE_DEFINED_TIME		2


extern int getKeepAliveParameter();
extern int getKeepAliveInterval();

#endif /* EHIHOSTCFG_H_ */
