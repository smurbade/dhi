/*
 * ehiutils.h
 *
 *  Created on: 09-March-2016
 *      Author: BLR_SCA_TEAM
 */

#ifndef EHIUTILS_H_
#define EHIUTILS_H_

#define STRIP_LEADING_SPACES	0x01
#define STRIP_TRAILING_SPACES	0x02

extern void acquireElavonMutexLock(pthread_mutex_t * , const char * );
extern void releaseElavonMutexLock(pthread_mutex_t * , const char * );
extern int doesFileExist(const char *);
extern void formatFieldValue(char *);
extern int createHashTableForElavonFields();
extern int encdBase64(uchar *, int, uchar *, int *);
extern int dcdBase64(uchar *, int, uchar *, int *);
#endif /* EHIUTILS_H_ */
