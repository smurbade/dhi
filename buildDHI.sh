# --------------------------------
# Author: BLR SCA Team
# --------------------------------

ANS=""
DATA=0
COMPILE=0
BUNDLE=0
HOST=0
RCHI=1
CPHI=2
EHI=3

VER=""

# Defining the path for the application package data
cd appExecData
APP_PATH=`pwd`
cd -

# Defining the path for the DHIFiles 
#cd DHIFiles
#DHIFiles_PATH=`pwd`
#cd -

# Defining the path where the application package will be created
DHI_PKG_DIR=./package

echo ""
# Get Compilation option
if [ "$1" = "y" ] || [ "$1" = "Y" ]
then
	echo "Compilation Required"
	COMPILE=1
elif [ "$1" = "n" ] || [ "$1" = "N" ]
then
	echo "Compilation Not Required"
	COMPILE=0
else
	while [ $DATA = 0 ]
	do
		echo "Want to Compile DHI Application?[Y/N]"
		read ANS
		case $ANS in
			"Y" | "y" )
			COMPILE=1
			DATA=1
			;;

			"N" | "n" )
			COMPILE=0
			DATA=1
			;;
		esac
	done
fi

echo ""
# Get the bundling option
if [ "$2" = "y" ] || [ "$2" = "Y" ]
then
	echo "DHI Bundling Enabled"
	BUNDLE=1
elif [ "$2" = "n" ] || [ "$2" = "N" ]
then
	echo "DHI Bundling Not Required"
	BUNDLE=0
else
	DATA=0
	while [ $DATA = 0 ]
	do
		echo "Want to create DHI App bundle?[Y/N]"
		read ANS
		case $ANS in
			"Y" | "y" )
			BUNDLE=1
			DATA=1
			;;

			"N" | "n" )
			BUNDLE=0
			DATA=1
			;;
		esac
	done
fi

echo ""
# if the bundling is YES, ask for version
if [ $BUNDLE = 1 ]
then
	if [ "$3" = "" ]
	then
		while [ "$VER" = "" ]
		do
			echo "Please enter the version no for applicatioon"
			read VER
		done
	else
		VER=$3
	fi
fi

# if the bundling is YES, ask for 
if [ $BUNDLE = 1 ]
then
	if [ "$4" = "1" ]
	then
		HOST=RCHI
		echo "Making RCHI Bundle"
	elif [ "$4" = "2" ]
	then
		HOST=CPHI
		echo "Making CPHI Bundle"
	elif [ "$4" = "3" ]
	then
		HOST=EHI
		echo "Making EHI Bundle"
	else
		HOST=0
		while [ $HOST = 0 ]
		do
			echo "Which Host Interface Do you want to Create"
			echo "1. RCHI"
			echo "2. CPHI"
			echo "3. EHI"
			read ANS
			case $ANS in
				"1" )
				HOST=RCHI
				;;

				"2" )
				HOST=CPHI
				;;
				
				"3" )
				HOST=EHI
				;;
			esac
		done
	fi
fi

#Defining the path for the Host specific files
cd "$HOST"Files
DHIFiles_PATH=`pwd`
cd - 


# Start the compilation if needed
if [ $COMPILE = 1 ]
then
	echo ""
	echo " ------- Running Make ---------"
	export CYGPATH=cygpath
	make
	if [ $? = 0 ]
	then
		echo ""
		echo " ------- Make is SUCCESSFUL ------- "
	else
		echo ""
		echo " ------- Make FAILED --------- "
		return $?
	fi
fi

# Start the bundling if needed
if [ $BUNDLE = 1 ]
then
	echo "Bundling the DHI Application"
	cd $DHI_PKG_DIR
	./mkpkg.sh $VER $APP_PATH $DHIFiles_PATH $HOST
	if [ $? = 0 ]
	then
		echo ""
		echo " ----------- DHI App Bundling SUCCESSFUL ----------"
	else
		echo ""
		echo " ----------- DHI App Bundling FAILED -------------- "
		return $?
	fi
	cd -
fi